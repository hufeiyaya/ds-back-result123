package com.daoshu.mds.message.config;

import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ThreadPoolConfig {

	@Bean("taskExecutor")
	public Executor taskExecutor () {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(20);
		executor.setMaxPoolSize(100);
		executor.setQueueCapacity(100);
		executor.setKeepAliveSeconds(30);
		executor.setThreadNamePrefix("TaskExecutorPool-");
		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		return executor;
	}
	@Bean("scheduTaskExecutor")
	public ScheduledThreadPoolExecutor scheduledThread() {
		ThreadPoolTaskExecutor executor = (ThreadPoolTaskExecutor) taskExecutor();
		ScheduledThreadPoolExecutor schedu = new ScheduledThreadPoolExecutor(20, executor);
		return schedu;
	}
}
