package com.daoshu.mds.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.mds.message.entity.PersonGroupEntity;

public interface PersonActiveGroupMapper extends BaseMapper<PersonGroupEntity> {

}
