package com.daoshu.mds.message.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.component.exception.BusinessException;
import com.daoshu.mds.constants.MessageType;
import com.daoshu.mds.dispatch.entity.MicroGroupMemberEntity;
import com.daoshu.mds.dispatch.entity.MicroGroupMessageEntity;
import com.daoshu.mds.dispatch.qo.MicroGroupQO;
import com.daoshu.mds.message.mapper.MicroGroupMemberMapper;
import com.daoshu.mds.message.service.IMessageService;
import com.daoshu.mds.message.service.IMicroGroupMemberService;
import com.daoshu.mds.message.service.IMicroGroupMessageService;
import com.daoshu.system.bo.AccountBo;
import com.daoshu.system.utils.RequestUtils;
import io.jsonwebtoken.lang.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  微群成员服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Service
public class MicroGroupMemberServiceImpl extends ServiceImpl<MicroGroupMemberMapper, MicroGroupMemberEntity> implements IMicroGroupMemberService {

	@Autowired
	private RequestUtils userUtil;

	@Autowired
	private StringRedisTemplate redisTemplate;

	@Autowired
	@Lazy
	private IMessageService messageService;

	@Autowired
	@Lazy
	private IMicroGroupMessageService microGroupMessageService;

	@Override
	public void setReadTime(String groupId, String personId,boolean importMessageFalg) {
		MicroGroupMemberEntity entity = new MicroGroupMemberEntity();
		if(importMessageFalg) {
			MicroGroupMessageEntity message = microGroupMessageService.getById(groupId);
			if(null != message) {
				Date time = message.getCreateTime();
				entity.setReadImportTime(time);
				groupId = message.getMicroGroupId();
			}else {
				throw new BusinessException(500, "不存在的消息，更新失败");
			}
		}else {
			entity.setReadedTime(new Date());
		}
		this.update(entity, new UpdateWrapper<MicroGroupMemberEntity>().eq("person_id_",personId).eq("micro_group_id_", groupId));
	}

	@Override
	public void addMembers(List<MicroGroupMemberEntity> members) {
		if (!Collections.isEmpty(members)) {
			redisTemplate.delete("MicroGroup-" + members.get(0).getMicroGroupId());
			
			// 发送消息给群里所有人
			AccountBo user = userUtil.getCurrentUser();
			MicroGroupMessageEntity message = new MicroGroupMessageEntity();
			StringBuffer sb = new StringBuffer(user.getPersonBo().getName() + " 邀请 ");
			Date date = new Date();
			List<String> membersString = new ArrayList<>(members.size());
			for(MicroGroupMemberEntity entity : members) {
				entity.setReadedTime(date);
				entity.setReadImportTime(date);
				entity.setDelete(0);
				entity.setCreateTime(date);
				entity.setCreatorId(user.getPersonId());
				sb.append(entity.getPersonName()).append(",");
				membersString.add(entity.getPersonId());
			}
			List<MicroGroupMemberEntity> list = this.list(new QueryWrapper<MicroGroupMemberEntity>().lambda()
					.eq(MicroGroupMemberEntity::getMicroGroupId, members.get(0).getMicroGroupId())
					.in(MicroGroupMemberEntity::getPersonId, membersString));
			if(list!=null&&!list.isEmpty())
				throw new BusinessException(500, "不能重复添加成员");
			sb.setLength(sb.length() - 1);
			sb.append(" 加入微群");
			this.saveBatch(members);
			message.setType(MessageType.SYSTEM_INFO.getType());
			message.setCreateId(user.getPersonId());
			message.setCreateName(user.getPersonBo().getName());
			message.setContent(sb.toString());
			message.setMicroGroupId(members.get(0).getMicroGroupId());
			messageService.sendMessage(message);
		}
	}
	@Override
	public IPage<MicroGroupMemberEntity> queryMember(MicroGroupQO qo) {
		IPage<MicroGroupMemberEntity> page = this.page(new Page<MicroGroupMemberEntity>(qo.getStart(), qo.getSize()), new QueryWrapper<MicroGroupMemberEntity>().eq("micro_group_id_", qo.getMicroGroupId()));
		if(page.getRecords()!=null&&!page.getRecords().isEmpty()) {
			List<MicroGroupMemberEntity> list = page.getRecords();
			List<String> personIds = new ArrayList<>(list.size());
			list.forEach(a->personIds.add(a.getPersonId()));
		}
		return page;
	}
	
}
