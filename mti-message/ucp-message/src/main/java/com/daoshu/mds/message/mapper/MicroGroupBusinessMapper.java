package com.daoshu.mds.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.mds.dispatch.entity.MicroGroupBusiness;

/**
 * <p>
  *  微群业务关联Mapper 接口
 * </p>
 *
 * @author zhaichen
 * @since 2019-06-04
 */
public interface MicroGroupBusinessMapper extends BaseMapper<MicroGroupBusiness> {

}