package com.daoshu.mds.message.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.mds.dispatch.entity.MicroGroupMessageEntity;
import com.daoshu.mds.dispatch.qo.MicroMessageQO;

/**
 * <p>
 *  微群消息服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface IMicroGroupMessageService extends IService<MicroGroupMessageEntity> {
	
	/**
	 * 查询微群消息记录
	 * @param qo
	 * @return
	 */
	IPage<MicroGroupMessageEntity> queryMicroMessage(MicroMessageQO qo);

	/**
	 * 获取微群下未读消息数量
	 * @param groupId
	 * @return
	 */
	Integer getUnreadMessage(String groupId, String personId);
	
	/***
	 * 发送微群消息
	 * @param message
	 */
	boolean sendMicroGroupMessage(MicroGroupMessageEntity message);
	/**
	 * 查询会议室中聊天消息
	 * @param qo
	 * @return
	 */
	IPage<MicroGroupMessageEntity> queryConfienceMessage(MicroMessageQO qo);
	/**
	 * 查询重要消息
	 * @param qo
	 * @return
	 */
	IPage<MicroGroupMessageEntity> queryImportMessage(MicroMessageQO qo);
	/**
	 * 设置重要消息
	 * @param messageId
	 */
	void setupImportMessage(String messageId);

	/**
	 * 查询微群重要消息记录历史记录
	 *
	 * @param qo
	 * @return
	 */
	IPage<MicroGroupMessageEntity> queryImportHistoryMicroMessage(MicroMessageQO qo);

	/**
	 * 取消重要消息设置
	 *
	 * @param messageId 重要消息id
	 * @return
	 */
	boolean cancalSettingImportant(String messageId);
}
