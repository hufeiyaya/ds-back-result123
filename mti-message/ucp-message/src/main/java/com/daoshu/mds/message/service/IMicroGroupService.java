package com.daoshu.mds.message.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.mds.dispatch.bo.MicroGroupBO;
import com.daoshu.mds.dispatch.entity.MicroGroupEntity;
import com.daoshu.mds.dispatch.qo.MicroGroupQO;

/**
 * <p>
 *  微群服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface IMicroGroupService extends IService<MicroGroupEntity> {
	
	/**
	 * 添加微群
	 * @param bo
	 * @return
	 */
	MicroGroupEntity addMicroGroup(MicroGroupBO bo);
	
	/**
	 * 查询某人参与的微群
	 * @param bo
	 * @return
	 */
	IPage<MicroGroupBO> queryMicroGroupInPerson(MicroGroupQO qo);
	/**
	 * 删除微群
	 * @param groupId
	 */
	void deleteMicroGroup(String groupId);

	/**
	 * 查询微群id和人员id对应关系
	 * @Title: getAllMGToMember
	 * @Description: TODO
	 * @return
	 * @return: List<Map<String,String>>
	 */
	public List<Map<String, String>> getAllMGToMember();
	
	/**
	 * 查询某人参与的所有未读消息数量 
	 * @param personId
	 * @return
	 */
	Integer getUnreadCount(String personId);
	
}
