package com.daoshu.mds.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.mds.message.entity.PersonGroupEntity;

public interface IPersonGroupService extends IService<PersonGroupEntity> {

	void saveOrUpd(String microGroupId, String personId);
	
	void updateReadTimeOffline(String personId);

}
