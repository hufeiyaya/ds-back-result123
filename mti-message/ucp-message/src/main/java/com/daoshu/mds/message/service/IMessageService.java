package com.daoshu.mds.message.service;

import com.daoshu.mds.dispatch.entity.MicroGroupMessageEntity;

public interface IMessageService {

	/**
	 * 发送微群消息
	 * @Title: sendMessage
	 * @Description: TODO
	 * @param message
	 * @return: void
	 */
	void sendMessage(MicroGroupMessageEntity message);

	/**
	 * 发送警情消息
	 * @param alarms
	 */
	void sendAlarmMessage(String alarms);

	/**
	 * 发送通知消息到个人
	 */
	void sendNoticeMessage(MicroGroupMessageEntity message);

}
