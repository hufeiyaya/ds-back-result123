package com.daoshu.mds.message.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.WebAsyncTask;

import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.dispatch.entity.MicroGroupMessageEntity;
import com.daoshu.mds.message.service.IMessageService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@RestController
@RequestMapping("/mds")
public class MessageController {
	
	@Autowired
    private IMessageService messageService;
	
	@ApiOperation(value = "用户发送点对点消息", notes = "用于通过http发送消息到特定用户")
	@PostMapping(value = "/sendMessage")
	public  WebAsyncTask<ResponseInfo>  sendToUsersMsg(@ApiParam @RequestBody MicroGroupMessageEntity message) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				messageService.sendMessage(message);
				criteria.addSingleResult(message);
			}
		}.sendAsyncRequest();
	}

	@ApiOperation(value = "发送警情消息", notes = "发送警情消息到警情队列")
	@PostMapping(value = "/sendAlarmMessage")
	public WebAsyncTask<ResponseInfo> sendAlarmMessage(@RequestBody String alarms) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				messageService.sendAlarmMessage(alarms);
				criteria.addSingleResult(alarms);
			}
		}.sendAsyncRequest();
	}

	@ApiOperation(value = "发送通知消息", notes = "发送通知消息")
	@PostMapping(value = "/sendNoticeMessage")
	public WebAsyncTask<ResponseInfo> sendNotice(@RequestBody MicroGroupMessageEntity message) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				messageService.sendNoticeMessage(message);
				criteria.addSingleResult(message);
			}
		}.sendAsyncRequest();
	}
	
	@MessageExceptionHandler
	public String handleException(Throwable exception) {
		return exception.getMessage();
	}
	
	@MessageMapping("/sendMessage")
    public void sendMessage(@Payload MicroGroupMessageEntity message) throws Exception {
        messageService.sendMessage(message);
    }
}
