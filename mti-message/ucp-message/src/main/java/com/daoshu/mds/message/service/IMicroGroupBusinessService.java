package com.daoshu.mds.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.mds.dispatch.entity.MicroGroupBusiness;

/**
 * <p>
 *  微群业务关联服务类
 * </p>
 *
 * @author zhaichen
 * @since 2019-06-04
 */
public interface IMicroGroupBusinessService extends IService<MicroGroupBusiness> {

}
