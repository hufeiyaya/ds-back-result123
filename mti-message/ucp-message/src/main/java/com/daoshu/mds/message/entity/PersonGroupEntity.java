package com.daoshu.mds.message.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@TableName("t_mds_person_active_group")
public class PersonGroupEntity implements Serializable {
    private static final long serialVersionUID = -3061427998659725826L;

    @TableId("id_")
    protected String id;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @TableField("person_id_")
    private String personId;
    @TableField("active_group_id_")
    private String activeGroupId;

    public PersonGroupEntity() {
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public void setActiveGroupId(String activeGroupId) {
        this.activeGroupId = activeGroupId;
    }

    public String getPersonId() {
        return this.personId;
    }

    public String getActiveGroupId() {
        return this.activeGroupId;
    }

    public String toString() {
        return "PersonGroupEntity(personId=" + this.getPersonId() + ", activeGroupId=" + this.getActiveGroupId() + ")";
    }
}