package com.daoshu.mds.message.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.dispatch.entity.MicroGroupBusiness;
import com.daoshu.mds.message.mapper.MicroGroupBusinessMapper;
import com.daoshu.mds.message.service.IMicroGroupBusinessService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  微群业务关联实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-06-04
 */
@Service
public class MicroGroupBusinessServiceImpl extends ServiceImpl<MicroGroupBusinessMapper, MicroGroupBusiness> implements IMicroGroupBusinessService {

}
