package com.daoshu.mds.message.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.mds.dispatch.entity.MicroGroupMemberEntity;
import com.daoshu.mds.dispatch.qo.MicroGroupQO;

/**
 * <p>
 *  微群成员服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface IMicroGroupMemberService extends IService<MicroGroupMemberEntity> {
	/**
	 * 设置读取时间
	 * @param groupId
	 * @param personId
	 */
	void setReadTime(String groupId, String personId,boolean importMessageFlag);
	
	/**
	 * 添加微群成员 
	 * @param members
	 */
	void addMembers(List<MicroGroupMemberEntity> members);
	
	/**
	 * 查询群成员
	 * @param qo
	 * @return
	 */
	IPage<MicroGroupMemberEntity> queryMember(MicroGroupQO qo);
}
