package com.daoshu.mds.message.service.impl;

import com.daoshu.mds.message.entity.PersonGroupEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.message.mapper.PersonActiveGroupMapper;
import com.daoshu.mds.message.service.IMicroGroupMemberService;
import com.daoshu.mds.message.service.IPersonGroupService;

@Service
public class PersonGroupServiceImpl extends ServiceImpl<PersonActiveGroupMapper, PersonGroupEntity> implements IPersonGroupService {

	@Autowired
	private IMicroGroupMemberService microGroupMemberService;
	
	@Override
	public void saveOrUpd(String microGroupId, String personId) {
		PersonGroupEntity entity = this.getOne(new QueryWrapper<PersonGroupEntity>().eq("person_id_", personId));
		if (null == entity) {
			entity = new PersonGroupEntity();
		} else {
			microGroupMemberService.setReadTime(entity.getActiveGroupId(), personId,false);
		}
		entity.setActiveGroupId(microGroupId);
		entity.setPersonId(personId);
		this.saveOrUpdate(entity);
	}

	@Override
	@Async
	public void updateReadTimeOffline(String personId) {
		PersonGroupEntity entity = this.getOne(new QueryWrapper<PersonGroupEntity>().eq("person_id_", personId));
		if (null != entity) {
			microGroupMemberService.setReadTime(entity.getActiveGroupId(), personId,false);
			this.removeById(entity.getId());
		}
	}
}
