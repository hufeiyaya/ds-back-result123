package com.daoshu.mds.message.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.mds.dispatch.entity.MicroGroupMemberEntity;

/**
 * <p>
  *  微群成员Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface MicroGroupMemberMapper extends BaseMapper<MicroGroupMemberEntity> {

	/**
	 * 查询单聊成员及组群
	 * @param creatorId
	 * @param memberId
	 * @return
	 */
	List<MicroGroupMemberEntity> querySingleMember(String creatorId,String memberId);
}