package com.daoshu.mds.message.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.mds.dispatch.entity.MicroGroupEntity;

/**
 * <p>
  *  微群Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface MicroGroupMapper extends BaseMapper<MicroGroupEntity> {

	@Select("select \n" +
			"			g.id_ groupids, listagg(m.person_id_, ',') within group(order by g.id_) memberids \n" +
			"		from t_mds_micro_group g left join t_mds_micro_group_member m on m.micro_group_id_ = g.id_ \n" +
			"		where g.delete_ = 0 \n" +
			"		group by g.id_")
	List<Map<String, String>> getAllMGToMembers();
	/**
	 * 查询某人参与微群未读消息总数
	 * @param personId
	 * @return
	 */
	Integer getUnreadCount(String personId);
}