package com.daoshu.mds.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.mds.dispatch.entity.MicroGroupMessageEntity;

/**
 * <p>
  * 微群消息 Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface MicroGroupMessageMapper extends BaseMapper<MicroGroupMessageEntity> {

}