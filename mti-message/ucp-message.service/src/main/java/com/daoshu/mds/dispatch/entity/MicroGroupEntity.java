package com.daoshu.mds.dispatch.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 微群
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@EqualsAndHashCode(callSuper=false)
@Data
@TableName("t_mds_micro_group")
@ApiModel(value = "微群对象", description = "微群对象")
public class MicroGroupEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5172486426047687340L;

	@TableId("id_")
	@ApiModelProperty(value = "主键ID", dataType = "String")
	private String id;

	/** 创建时间 */
    @TableField("create_time_")
	@ApiModelProperty(value = "创建时间", dataType = "Date", hidden = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	/** 创建人id */
    @TableField("creator_id_")
	@ApiModelProperty(value = "创建人id", dataType = "String", hidden = true)
	private String creatorId;

	/** 标题 */
    @TableField("title_")
	@ApiModelProperty(value = "标题", dataType = "String")
	private String title;

	/** 描述 */
    @TableField("description_")
	@ApiModelProperty(value = "描述", dataType = "String")
	private String description;

	/** 删除标记 */
	@TableLogic
	@TableField("delete_")
	@ApiModelProperty(value = "删除标记", dataType = "Integer", hidden = true)
	private Integer delete;


	@TableField("group_type")
	private String groupType;

	@TableField("creator_name_")
	private String creatorName;

	/**
	 * 最后一条消息更新时间
	 */
	@TableField("last_message_update_time")
	@ApiModelProperty(value = "最后一条消息更新时间", dataType = "Date", hidden = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date lastMessageUpdateTime;


	@Override
	public String toString() {
		return "TMdsMicroGroup{" +
			"id=" + id +
			", createTime=" + createTime +
			", creatorId=" + creatorId +
			", title=" + title +
			", description=" + description +
			", delete=" + delete +
			"}";
	}
}
