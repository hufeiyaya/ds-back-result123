package com.daoshu.mds.dispatch.bo;

import java.util.List;

import com.daoshu.mds.dispatch.entity.MicroGroupEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class MicroGroupMessageBO extends MicroGroupEntity{
	
	/**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 7353029007423785006L;
	
	private List<String> members;
}
