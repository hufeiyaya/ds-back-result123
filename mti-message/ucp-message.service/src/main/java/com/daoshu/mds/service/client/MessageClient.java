package com.daoshu.mds.service.client;

import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.dispatch.entity.MicroGroupMessageEntity;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(name = "UCP-MESSAGE")
public interface MessageClient {

    @RequestMapping(value="/mds/sendNoticeMessage",method= RequestMethod.POST)
    ResponseInfo sendNoticeMessage(@RequestBody final MicroGroupMessageEntity message);

    @RequestMapping(value="/mds/sendAlarmMessage",method= RequestMethod.POST)
    ResponseInfo sendAlarmMessage(@RequestBody final String alarms);
}
