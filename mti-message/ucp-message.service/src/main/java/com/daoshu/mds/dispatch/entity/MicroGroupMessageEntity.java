package com.daoshu.mds.dispatch.entity;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 微群消息
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@EqualsAndHashCode(callSuper=false)
@Data
@TableName("t_mds_micro_group_message")
public class MicroGroupMessageEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -847848949024110065L;

	@TableId("id_")
	@ApiModelProperty(value = "主键ID", dataType = "String")
	private String id;

	/** 消息类型 */
    @TableField("type_")
	private Integer type;
	/**  发送人id*/
    @TableField("create_id_")
	private String createId;
	/**发送时间  */
    @TableField("create_time_")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;
	/** 内容 */
    @TableField("content_")
	private String content;
	/** 文件类型地址 */
    @TableField("path_")
	private String path;
    @TableField("create_name_")
	private String createName;
    @TableField("micro_group_id_")
    private String microGroupId;
    @TableField("obj_")
    private String obj;

	/**
	 * 是否重要消息：0.否 1.是
	 */
	@TableField("import_message_flag_")
    private Integer importMessageFlag;
    @TableField(exist=false)
    private String headUrl;
    @TableField(exist=false)
    private Boolean isOther;
	@TableField(exist = false)
	private String microGroupName;

    public String toJsonString() {
    	String json = JSONObject.toJSONString(this);
    	return json;
    }
    
	@Override
	public String toString() {
		return "TMdsMicroGroupMessage{" +
			"id=" + id +
			", type=" + type +
			", createId=" + createId +
			", createTime=" + createTime +
			", content=" + content +
			", path=" + path +
			"}";
	}
}
