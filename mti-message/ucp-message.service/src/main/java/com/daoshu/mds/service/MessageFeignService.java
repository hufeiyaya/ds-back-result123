package com.daoshu.mds.service;

import com.daoshu.mds.dispatch.entity.MicroGroupMessageEntity;
import com.daoshu.mds.service.client.MessageClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName MessageFeignService
 * @Description TODO
 * @Author zhaoyj
 * @Date 2019/4/10 12:56
 */
@Service
public class MessageFeignService {

    @Autowired private MessageClient messageClient;

    public void sendNoticeMessage(MicroGroupMessageEntity message){
        messageClient.sendNoticeMessage(message);
    }

    public void sendAlarmMessage(String alarms){
        messageClient.sendAlarmMessage(alarms);
    }
}
