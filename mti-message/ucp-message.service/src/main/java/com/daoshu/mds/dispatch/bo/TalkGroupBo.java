package com.daoshu.mds.dispatch.bo;

import com.daoshu.mds.dispatch.qo.PageQO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TalkGroupBo extends PageQO {

	private String id;
	
	private String name;
	
	private String orgId;
	
	private String creatorId;
	
	private List<String> persons;

}
