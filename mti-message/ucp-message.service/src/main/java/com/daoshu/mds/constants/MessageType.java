package com.daoshu.mds.constants;

public enum MessageType {
	NOTICE_OFFLINE(-10000, "强制退出"),
	SYSTEM_INFO(-1, "系统消息"),
	TEXT_MESSAGE(0, "普通文本消息"),
	PICTURE_MESSAGE(1, "图片"),
	AUDIO_MESSAGE(2, "音频"),
	VIDEO_MESSAGE(3, "视频"),
	ATTACH_MESSAGE(4, "附件消息");

	private int type;
	
	private String title;
	
	MessageType(int type, String title) {
		this.type = type;
		this.title = title;
	}
	
	public int getType() {
		return this.type;
	}
	public String getTitle() {
		return this.title;
	}
	
}
