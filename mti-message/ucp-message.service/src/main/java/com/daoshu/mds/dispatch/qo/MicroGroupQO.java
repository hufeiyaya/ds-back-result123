package com.daoshu.mds.dispatch.qo;

import lombok.Data;
import lombok.EqualsAndHashCode;
@EqualsAndHashCode(callSuper=false)
@Data
public class MicroGroupQO extends PageQO{

	/**
	 * 微群ID
	 */
	private String microGroupId;
	
	private String personId;
	
	private String title;
	
}
