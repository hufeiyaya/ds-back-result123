package com.daoshu.mds.constants;

/**
 * 微群类型
 * @author zhaoyj
 *
 */
public enum MicroGroupType {

    /** 单聊 */
    SINGLE_GROUP("1","单聊"),
    /** 群聊  */
    GROUP_GROUP("2","群聊");
    /**
     * 获取微群类型
     *
     * @param status
     *
     * @return
     */
    public static MicroGroupType getMicroGroupType(String status){
        MicroGroupType[] array = MicroGroupType.values();
        for(MicroGroupType item:array){
            if(item.status == status){
                return item;
            }
        }
        return MicroGroupType.GROUP_GROUP;
    }

    MicroGroupType(String status,String title){
        this.status = status;
        this.title = title;
    }
    /** 类型 */
    private String status;
    /** 类型标题 */
    private String title;
    /**
     * 获取类成员type
     * @return {@link #type}
     */
    public String getStatus() {
        return this.status;
    }
    /**
     * 获取类成员title
     * @return {@link #title}
     */
    public String getTitle() {
        return this.title;
    }
}
