package com.daoshu.mds.dispatch.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 微群关联业务
 * </p>
 *
 * @author zhaichen
 * @since 2019-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_mds_micro_group_business")
@ApiModel(value = "微群关联业务对象", description = "微群关联业务对象")
public class MicroGroupBusiness implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5480084229829062084L;

	@TableId("id_")
	@ApiModelProperty(value = "主键ID", dataType = "String")
	private String id;

	@ApiModelProperty(value = "微群id", dataType = "String", required = true)
    @TableField("micro_group_id")
	private String microGroupId;

	@ApiModelProperty(value = "业务id", dataType = "String")
    @TableField("business_id")
	private String businessId;

	@ApiModelProperty(value = "业务类型", dataType = "Integer", notes = "1.重点人员 2.警情", example = "1", required = true)
	@TableField("business_type")
	private Integer businessType;

	@ApiModelProperty(value = "创建人id", dataType = "String", hidden = true)
	@TableField("create_id")
	private String createId;

	@ApiModelProperty(value = "创建时间", dataType = "Date", hidden = true)
	@TableField("create_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

}
