package com.daoshu.mds.dispatch.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

import javax.persistence.Entity;

@Setter
@Getter
@ToString
@Entity
@TableName(value = "t_mds_person_active_group")
public class PersonGroupEntity implements Serializable{

    /**
     * @fieldName: serialVersionUID
     * @fieldType: long
     * @Description: TODO
     */
    private static final long serialVersionUID = -3061427998659725826L;

    @TableId("id_")
    @ApiModelProperty(value = "主键ID", dataType = "String")
    private String id;

    @TableField(value = "person_id_")
    private String personId;

    @TableField(value = "active_group_id_")
    private String activeGroupId;

}
