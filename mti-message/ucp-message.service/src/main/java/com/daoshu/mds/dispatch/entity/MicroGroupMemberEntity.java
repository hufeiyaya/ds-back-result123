package com.daoshu.mds.dispatch.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 微群成员
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@EqualsAndHashCode(callSuper=false)
@Data
@TableName("t_mds_micro_group_member")
public class MicroGroupMemberEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7109094270362598626L;

	@TableId("id_")
	@ApiModelProperty(value = "主键ID", dataType = "String")
	private String id;

	/** 加入时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time_")
	private Date createTime;
	/** 邀请人id */
    @TableField("creator_id_")
	private String creatorId;
	/** 成员id */
    @TableField("person_id_")
	private String personId;
	/** 成员姓名 */
    @TableField("person_name_")
	private String personName;
    /** 成员姓名 */
    @TableField("main_duty_")
	private String mainDuty;
	/** 联系方式Id */
    @TableField("concat_id_")
	private String concatId;
	/** 联系号码 */
    @TableField("concat_")
	private String concat;
	/** 职务名称 */
    @TableField("position_name_")
	private String positionName;
	/** 微群id */
    @TableField("micro_group_id_")
	private String microGroupId;
	/** 已读时间 */
    @TableField("readed_time_")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date readedTime;
    @TableField("person_type_")
	private Integer personType;
    @TableField("person_org_id_")
	private String personOrgId;
    @TableField("person_org_name_")
	private String personOrgName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("readed_import_time_")
	private Date readImportTime;
    /** 删除标记 */
	@TableLogic
	@TableField("delete_")
	private Integer delete = 0;
	
	@TableField(exist = false)
	private String defaultConcat;

	@Override
	public String toString() {
		return "TMdsMicroGroupMember{" +
			"id=" + id +
			", createTime=" + createTime +
			", creatorId=" + creatorId +
			", personId=" + personId +
			", personName=" + personName +
			", concatId=" + concatId +
			", concat=" + concat +
			", positionName=" + positionName +
			", microGroupId=" + microGroupId +
			", readedTime=" + readedTime +
			"}";
	}
}
