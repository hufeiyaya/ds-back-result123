package com.daoshu.mds.dispatch.bo;

import com.daoshu.mds.dispatch.entity.MicroGroupBusiness;
import com.daoshu.mds.dispatch.entity.MicroGroupEntity;
import com.daoshu.mds.dispatch.entity.MicroGroupMessageEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Data@EqualsAndHashCode(callSuper=false)
public class MicroGroupBO extends MicroGroupEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 599475834825332788L;

	private List<String> members;
	
	private Integer unReadCount;
	
	private MicroGroupMessageEntity latestMessage;

	/**
	 * 是否关联业务
	 */
	private boolean isBusiness = false;

	/**
	 * 微群关联业务
	 */
	private MicroGroupBusiness microGroupBusiness;
}
