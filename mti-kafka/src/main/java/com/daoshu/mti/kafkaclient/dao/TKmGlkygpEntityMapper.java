package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmGlkygpEntity;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface TKmGlkygpEntityMapper {
    int deleteByPrimaryKey(String pgPkid);

    int insert(TKmGlkygpEntity record);

    int insertSelective(TKmGlkygpEntity record);

    TKmGlkygpEntity selectByPrimaryKey(String pgPkid);

    int updateByPrimaryKeySelective(TKmGlkygpEntity record);

    int updateByPrimaryKey(TKmGlkygpEntity record);

    /**
     * �жϹ�·�ÿ� ��-��
     * @param tKmGlkygpEntity
     * @return
     */
    Integer judgeInOutType(TKmGlkygpEntity tKmGlkygpEntity);

    /**
     * 根据客运站名称查询 城市信息
     * @param kyz
     * @return
     */
    Map<String, Object> queryKyzInfoByName(@Param("kyz") String kyz);

    /**
     * 根据身份证号  和 预警时间判断是否重复
     * @param tKmGlkygpEntity
     * @return
     */
    Integer judgeRepeatData(TKmGlkygpEntity tKmGlkygpEntity);
}