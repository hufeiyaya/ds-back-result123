package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TLfZdryBaseEntity;
import com.daoshu.mti.kafkaclient.dao.TLfZdryBaseEntityMapper;
import com.daoshu.mti.kafkaclient.service.TLfZdryBaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TKmWbswServiceImpl
 * @Description TODO
 * @Date 2020/1/17 23:22
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TLfZdryBaseServiceImpl implements TLfZdryBaseService {
    @Resource
    private TLfZdryBaseEntityMapper tLfZdryBaseEntityMapper;

    @Override
    public TLfZdryBaseEntity getBaseInfoByPassportNo(String passportNo) {
        return tLfZdryBaseEntityMapper.getBaseInfoByPassportNo(passportNo);
    }
}
