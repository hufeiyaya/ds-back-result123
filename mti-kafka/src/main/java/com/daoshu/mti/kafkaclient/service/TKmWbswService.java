package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmWbswEntity;

/**
 * @Classname TKmWbswService
 * @Description TODO
 * @Date 2020/1/17 23:22
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmWbswService {
    Integer insert(TKmWbswEntity tKmWbswEntity);

    /**
     * 根据身份证号 和 预警时间判断重复数据
     * @param tKmWbswEntity
     * @return
     */
    Boolean judgeRepeatData(TKmWbswEntity tKmWbswEntity);
}
