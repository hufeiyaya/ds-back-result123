package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmHczEntity;
import com.daoshu.mti.kafkaclient.dao.TKmHczEntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmHczService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TKmHczServiceImpl
 * @Description TODO
 * @Date 2020/5/11 20:14
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmHczServiceImpl implements TKmHczService {
    @Resource
    private TKmHczEntityMapper tKmHczEntityMapper;

    @Override
    public TKmHczEntity getHczByStationName(String sendStation) {
        return tKmHczEntityMapper.getHczByStationName(sendStation);
    }
}
