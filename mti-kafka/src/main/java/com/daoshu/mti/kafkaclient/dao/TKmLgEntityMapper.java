package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmLgEntity;
import com.daoshu.mti.kafkaclient.bean.TKmLgzsEntity;
import org.apache.ibatis.annotations.Param;

public interface TKmLgEntityMapper {
    int deleteByPrimaryKey(String lgDm);

    int insert(TKmLgEntity record);

    int insertSelective(TKmLgEntity record);

    TKmLgEntity selectByPrimaryKey(@Param("lgDm") String lgDm);

    int updateByPrimaryKeySelective(TKmLgEntity record);

    int updateByPrimaryKey(TKmLgEntity record);

    Integer judgeRepeatData(TKmLgzsEntity kmLgzsEntity);
}