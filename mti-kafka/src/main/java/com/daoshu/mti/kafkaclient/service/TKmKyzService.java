package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmKyzEntity;

/**
 * @Classname TKmKyzService
 * @Description TODO
 * @Date 2020/5/5 9:54
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmKyzService {
    /**
     * 根据起点查询客运站名称
     * @param gaKyzid
     * @return
     */
    TKmKyzEntity getKyzXxByGaId(String gaKyzid);

    /**
     * 通过线路名称查询客运站信息
     * @param xlmc
     * @return
     */
    TKmKyzEntity getKyzXxByXlmc(String xlmc);

    /**
     * 通过起点站查询客运站的信息
     * @param sendStation
     * @return
     */
    TKmKyzEntity getKyzByKyzMc(String sendStation);
}
