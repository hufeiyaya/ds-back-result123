package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmWbswEntity;

public interface TKmWbswEntityMapper {
    int deleteByPrimaryKey(String xxzjbh);

    int insert(TKmWbswEntity record);

    int insertSelective(TKmWbswEntity record);

    TKmWbswEntity selectByPrimaryKey(String xxzjbh);

    int updateByPrimaryKeySelective(TKmWbswEntity record);

    int updateByPrimaryKey(TKmWbswEntity record);

    Integer judgeRepeatData(TKmWbswEntity tKmWbswEntity);
}