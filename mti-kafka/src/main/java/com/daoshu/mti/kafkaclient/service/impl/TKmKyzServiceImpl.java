package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmKyzEntity;
import com.daoshu.mti.kafkaclient.dao.TKmKyzEntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmKyzService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TKmKyzServiceImpl
 * @Description TODO
 * @Date 2020/5/5 9:54
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmKyzServiceImpl implements TKmKyzService {

    @Resource
    private TKmKyzEntityMapper tKmKyzEntityMapper;
    @Override
    public TKmKyzEntity getKyzXxByGaId(String gaKyzid) {
        return tKmKyzEntityMapper.getKyzXxByGaId(gaKyzid);
    }

    @Override
    public TKmKyzEntity getKyzXxByXlmc(String xlmc) {
        return tKmKyzEntityMapper.getKyzXxByXlmc(xlmc);
    }

    @Override
    public TKmKyzEntity getKyzByKyzMc(String sendStation) {
        return tKmKyzEntityMapper.getKyzByKyzMc(sendStation);
    }
}
