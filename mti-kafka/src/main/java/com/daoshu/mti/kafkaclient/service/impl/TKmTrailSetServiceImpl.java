package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmTrailEntity;
import com.daoshu.mti.kafkaclient.dao.TKmTrailSetEntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmTrailSetService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TKmTldpServiceImpl
 * @Description TODO
 * @Date 2020/1/17 16:14
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmTrailSetServiceImpl  implements TKmTrailSetService {
    @Resource
    private TKmTrailSetEntityMapper tkmapper;

	@Override
	public Integer insert(TKmTrailEntity entity) {
		return tkmapper.insert(entity);
	}

	@Override
	public Boolean judgeRepeatData(String id) {
		Integer count = tkmapper.judgeRepeatData(id);
        if(count > 0) return true;
        return false;
	}

	@Override
	public Integer update(String setId) {
		 return  tkmapper.update(setId);
	}
}
