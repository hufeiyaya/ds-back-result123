package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmMhdzEntity;

import java.util.Map;

/**
 * @Classname TKmMhdzService
 * @Description TODO
 * @Date 2020/1/17 23:20
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmMhdzService {
    Integer insert(TKmMhdzEntity tKmMhdzEntity);

    /**
     * @param tKmMhdzEntity
     * @return
     */
    Integer judgeInOutType(TKmMhdzEntity tKmMhdzEntity);

    /**
     * 根据民航三字码查询城市信息
     * @param airSegDptAirptCd
     * @return
     */
    Map<String, Object> queryCityByMhCode(String airSegDptAirptCd);

    /**
     * 根据身份证号 和 预警时间判断重复数据
     * @param tKmMhdzEntity
     * @return
     */
    Boolean judgeRepeatData(TKmMhdzEntity tKmMhdzEntity);
}
