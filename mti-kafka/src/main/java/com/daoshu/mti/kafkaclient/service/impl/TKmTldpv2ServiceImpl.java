package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmTldpv2Entity;
import com.daoshu.mti.kafkaclient.dao.TKmTldpv2EntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmTldpv2Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Classname TKmTldpv2ServiceImpl
 * @Description TODO
 * @Date 2020/3/5 14:05
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmTldpv2ServiceImpl implements TKmTldpv2Service {
    @Resource
    private TKmTldpv2EntityMapper tKmTldpv2EntityMapper;

    @Override
    public Integer insert(TKmTldpv2Entity tKmTldpv2Entity) {
        return tKmTldpv2EntityMapper.insert(tKmTldpv2Entity);
    }

    @Override
    public Integer judgeInOutType(TKmTldpv2Entity tKmTldpv2Entity) {
        return tKmTldpv2EntityMapper.judgeInOutType(tKmTldpv2Entity);
    }

    @Override
    public Map<String, Object> queryCityByHcz(String cfHcczmc) {
        if(null ==cfHcczmc || cfHcczmc.length()==0) return null;
        return tKmTldpv2EntityMapper.queryCityByHcz(cfHcczmc);
    }

    @Override
    public Boolean judgeRepeatData(TKmTldpv2Entity tKmTldpv2Entity) {
        Integer count = tKmTldpv2EntityMapper.judgeRepeatData(tKmTldpv2Entity);
        if(count>0) return true;
        return false;
    }
}
