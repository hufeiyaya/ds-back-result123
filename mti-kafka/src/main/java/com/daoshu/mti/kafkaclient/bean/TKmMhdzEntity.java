package com.daoshu.mti.kafkaclient.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.daoshu.mti.kafkaclient.constant.ZdryYjLx;
import com.daoshu.mti.kafkaclient.utils.DateUtils;
import com.daoshu.mti.kafkaclient.utils.KeyWorker;
import com.daoshu.mti.kafkaclient.utils.ZdryYjUtilBean;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;
@Data
public class TKmMhdzEntity {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String xxzjbh;

    private String pnrRef;

    private String pnrCrDt;

    private String airCarrCd;

    private String airSegFltNbrSfx;

    private String airSegFltNbr;

    private String airSegDptDtLcl;

    private String airSegDptTmLcl;

    private String airSegArrvTmLcl;

    private String airSegArrvDtLcl;

    private String airSegDptAirptCd;

    private String airSegArrvAirptCd;

    private String subClsCd;

    private String offcCd;

    private String oprStatCd;

    private String rspAirlnCd;

    private String rspOfcCd;

    private String pasId;

    private String pasLstNm;

    private String pasFstNm;

    private String pasChnNm;

    private String pasIdType;

    private String pasIdNbr;

    private String ffpIdNbr;

    private String grpInd;

    private String grpNm;

    private String vipInd;

    private String pnSeat;

    private String operatedate;

    private String operatetime;

    private String filename;

    private String ctDt;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgRksj;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgGxsj;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date jlSccjsj;
    /**
     * 等机机场名称
     */
    private String airSegDptAirptCdCn;
    /**
     * 到达机场名称
     */
    private String airSegArrvAirptCdCn;

    public ZdryYjUtilBean transtoZdryYjUtilBean(){
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.getPasChnNm());
        zdryYjUtilBean.setSfzh(this.getPasIdNbr());
        zdryYjUtilBean.setYjlx(ZdryYjLx.MHDZ);
        zdryYjUtilBean.setYjdz(this.airCarrCd+this.airSegFltNbr);
        zdryYjUtilBean.setYjsj(DateUtils.convertStrDateToOtherStr(this.getOperatedate()+this.getOperatetime(),
                "yyyyMMddHH:mm:ss","yyyy-MM-dd HH:mm:ss"));
        return zdryYjUtilBean;
    }

    public TKmZdryYjEntity transtoZdryYjEntity(){
        TKmZdryYjEntity tKmZdryYjEntity = new TKmZdryYjEntity();
        tKmZdryYjEntity.setWybs(String.valueOf(KeyWorker.nextId()));
        tKmZdryYjEntity.setYjlx(ZdryYjLx.MHDZ);
        tKmZdryYjEntity.setRyxm(this.getPasChnNm());
        tKmZdryYjEntity.setGlId(this.getXxzjbh());
        tKmZdryYjEntity.setYjbs(this.getPasIdNbr());
        tKmZdryYjEntity.setYjdd(this.airCarrCd+this.airSegFltNbr);
        tKmZdryYjEntity.setYjsj(DateUtils.convertStrDateToOtherStr(this.getOperatedate()+this.getOperatetime(),
                "yyyyMMddHH:mm:ss","yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setTbsj(DateUtils.formatDateToStr(new Date(),"yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setHandleFlag(1);
        tKmZdryYjEntity.setSendStation(this.airSegDptAirptCdCn);
        tKmZdryYjEntity.setSendtime(DateUtils.convertStrDateToOtherStr(this.airSegDptDtLcl+this.airSegDptTmLcl,
                "yyyyMMddHH:mm:ss","yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setSendStationIden(this.airSegDptAirptCd);
        tKmZdryYjEntity.setEndname(this.airSegArrvAirptCdCn);
        tKmZdryYjEntity.setEndStationIden(this.airSegArrvAirptCd);
        tKmZdryYjEntity.setEndStationTime(DateUtils.strToDateOrTime(this.airSegArrvDtLcl+this.airSegArrvTmLcl,"yyyyMMddHH:mm:ss"));
        tKmZdryYjEntity.setSendFlag(0);
        return tKmZdryYjEntity;
    }
}