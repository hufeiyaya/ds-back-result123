package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmLgzsEntity;

public interface TKmLgzsEntityMapper {
    int deleteByPrimaryKey(String xxzjbh);

    int insert(TKmLgzsEntity record);

    int insertSelective(TKmLgzsEntity record);

    TKmLgzsEntity selectByPrimaryKey(String xxzjbh);

    int updateByPrimaryKeySelective(TKmLgzsEntity record);

    int updateByPrimaryKey(TKmLgzsEntity record);
}