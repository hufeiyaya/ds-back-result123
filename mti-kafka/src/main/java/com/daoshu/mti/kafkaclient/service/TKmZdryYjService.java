package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmZdryYjEntity;

import java.util.List;
import java.util.Map;

/**
 * @Classname TKmZdryYjService
 * @Description TODO
 * @Date 2020/1/18 18:43
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmZdryYjService {
    void insert(TKmZdryYjEntity tKmZdryYjEntity);

    /**
     * 根据身份证号查询 派出所，分局，情报中心
     * @param sfzh
     * @return
     */
    Map<String, String> getZdryInfoBySfzh(String sfzh);

    /**
     * 根据身份证号查询websocket推送的token
     * @param zjhm
     * @return
     */
    List<String> findWebsocketPsuhUserListBySfzh(String zjhm);

    /**
     * 根据身份证号查询重点人员的管控级别，确定是否推送预警信息
     * @param sfzh
     * @return
     */
    Boolean judgeInsertFlag(String sfzh);

    /**
     * @Author hf
     * @Title: authGkjbAndWkzt
     * @Description: TODO (过滤掉 已稳控   和  灰名单数据 )
     * @Param [sfzh] 身份证号
     * @return TKmZdryYjEntity
     * @Date 2020/11/26 17:46
     */
    TKmZdryYjEntity authGkjbAndWkzt(String sfzh);
}
