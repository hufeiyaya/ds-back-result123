package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmJurisdictionEntity;

/**
 * @Classname TKmJurisdictionService
 * @Description TODO
 * @Date 2020/5/3 15:41
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmJurisdictionService {
    /**
     * 根据预警发生地经纬度查询预警发生地的派出所信息
     * @param x
     * @param y
     * @return
     */
    TKmJurisdictionEntity queryYjdPcsByLngAndLat(String x, String y);
}
