package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmLgDiztEntity;

/**
 * @Classname TKmLgDiztService
 * @Description TODO
 * @Date 2020/5/13 16:25
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmLgDiztService {
    /**
     * 根据旅馆代码查询旅馆的相关信息
     * @param lvguandaima
     * @return
     */
    TKmLgDiztEntity getLgDictInfoByLgDm(String lvguandaima);
}
