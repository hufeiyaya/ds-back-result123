package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmWbEntity;
import org.apache.ibatis.annotations.Param;

public interface TKmWbEntityMapper {
    int deleteByPrimaryKey(String id);

    int insert(TKmWbEntity record);

    int insertSelective(TKmWbEntity record);

    TKmWbEntity selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TKmWbEntity record);

    int updateByPrimaryKey(TKmWbEntity record);

    /**
     * 根据网吧
     * @param serviceCode
     * @return
     */
    TKmWbEntity getWbXxByCode(@Param("code") String serviceCode);
}