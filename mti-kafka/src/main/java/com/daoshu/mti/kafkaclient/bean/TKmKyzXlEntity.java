package com.daoshu.mti.kafkaclient.bean;

import lombok.Data;

@Data
public class TKmKyzXlEntity {
    private String gaKyzid;

    private String czid;

    private String xlmc;

    private String kmKyzMc;

    private String kmGaKyzid;

    private String kmKyzDz;

    private String lng;

    private String lat;

}