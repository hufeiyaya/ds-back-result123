package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmMhlgEntity;

/**
 * @Classname TKmMhlgService
 * @Description TODO
 * @Date 2020/1/18 11:48
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmMhlgService {
    Integer insert(TKmMhlgEntity tKmTldpEntity);

    /**
     * 根据身份证号  和  预警时间判断数据重复
     * @param tKmMhlgEntity
     * @return
     */
    Boolean judgeRepeatData(TKmMhlgEntity tKmMhlgEntity);
}
