package com.daoshu.mti.kafkaclient.dao;

import org.apache.ibatis.annotations.Select;

import com.daoshu.mti.kafkaclient.bean.TKmTrailEntity;

public interface TKmTrailSetEntityMapper  {
	
	Integer insert(TKmTrailEntity entity);
    @Select("select count(*) from t_km_trail t where t.id = #{id}")
	Integer judgeRepeatData(String id);
    
    Integer update(String setId);
       
}