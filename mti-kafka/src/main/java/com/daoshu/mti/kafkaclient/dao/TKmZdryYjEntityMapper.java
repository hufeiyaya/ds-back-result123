package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmZdryYjEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TKmZdryYjEntityMapper {

    int insert(TKmZdryYjEntity record);

    int insertSelective(TKmZdryYjEntity record);

    /**
     * 根据身份证查询 管控民警  派出所  所属分局
     * @param sfzh
     * @return
     */
    Map<String, String> getZdryInfoBySfzh(@Param("sfzh") String sfzh);

    /**
     * 根据重点人员的id查询
     * @param id
     * @return
     */
    List<String> getZrbmByZdryId(@Param("id") String id);

    /**
     * 根据身份证号查询重点人员的预警信息
     * @param sfzh
     * @return
     */
    Map<String, String> judgeInsertFlag(@Param("sfzh") String sfzh);

    /**
     * @Author hf
     * @Title: authGkjbAndWkzt
     * @Description: TODO (根据身份证号查询并过滤已稳控 和 灰名单用户)
     * @Param [sfzh]
     * @return TKmZdryYjEntity
     * @Date 2020/11/26 17:55
     */
    TKmZdryYjEntity authGkjbAndWkzt(@Param("sfzh") String sfzh);
}