package com.daoshu.mti.kafkaclient.handle;

import com.alibaba.fastjson.JSON;
import com.daoshu.mti.kafkaclient.bean.*;
import com.daoshu.mti.kafkaclient.constant.Topic;
import com.daoshu.mti.kafkaclient.consumer.TKmTldpBaseConsumer;
import com.daoshu.mti.kafkaclient.service.*;
import com.daoshu.mti.kafkaclient.utils.ZdryYjUtilBean;
import com.daoshu.mti.kafkaclient.websocket.SocketType;
import com.daoshu.mti.kafkaclient.websocket.WebSocketUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @Classname TKmTldpHandle
 * @Description TODO
 * @Date 2020/1/17 21:15
 * @Created by duchaof
 * @Version 1.0
 */
/*@Slf4j
@Component
public class TKmTljpHandle extends TKmTljpBaseConsumer {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

   *//* @Autowired
    private TKmTljpService tKmTljpService;

    @Autowired
    private TKmZdryYjService tKmZdryYjService;

    @Autowired
    private WebSocketUtils webSocketUtils;

    @Autowired
    private TKmJurisdictionService tKmJurisdictionService;
    @Autowired
    private TKmHczService tKmHczService;*//*


   *//* @Override
    @Transactional
    protected void onDealMessage(String message)  {
        try {

            log.info("接收到topic："+ Topic.TLJP +"的消息"+",线程："+Thread.currentThread().getName()+"在"+sdf.format(new Date())+"开始处理接受到的消息："+message);
            TKmTljpEntity tljp = JSON.parseObject(message, TKmTljpEntity.class);

            *//**//*Boolean insertFlag = tKmZdryYjService.judgeInsertFlag(tKmTldpv2Entity.getZjhm());
            if(!insertFlag) return;*//**//*

            Integer res =tKmTljpService.insert(tljp);
            if(res>0) {
                //log.info("数据插入成功，插入成功的数据为：" + message);
                TKmZdryYjEntity tKmZdryYjEntity = tljp.transtoZdryEntity();
                // 判断 进-出 昆明
                //Integer inOutType = tKmTldpv2Service.judgeInOutType(tKmTldpv2Entity);
                //tKmZdryYjEntity.setInOut(inOutType);

                TKmHczEntity fromHcz = tKmHczService.getHczByStationName(tKmZdryYjEntity.getSendStation());
                if(null != fromHcz){
                    tKmZdryYjEntity.setSendPro(fromHcz.getProvince());
                    tKmZdryYjEntity.setSendCity(fromHcz.getCity());
                    tKmZdryYjEntity.setSendLng(fromHcz.getLng());
                    tKmZdryYjEntity.setSendLat(fromHcz.getLat());
                    if(fromHcz.getCity().equals("昆明")||fromHcz.getCity().equals("昆明市")){
                        tKmZdryYjEntity.setInOut(2);
                        tKmZdryYjEntity.setX(fromHcz.getLng());
                        tKmZdryYjEntity.setY(fromHcz.getLat());
                    }
                }

                TKmHczEntity toHcz = tKmHczService.getHczByStationName(tKmZdryYjEntity.getEndname());
                if(null != toHcz){
                    tKmZdryYjEntity.setEndPro(toHcz.getProvince());
                    tKmZdryYjEntity.setEndCity(toHcz.getCity());
                    tKmZdryYjEntity.setEndLng(toHcz.getLng());
                    tKmZdryYjEntity.setEndLat(toHcz.getLat());
                    if(toHcz.getCity().equals("昆明")||toHcz.getCity().equals("昆明市")){
                        tKmZdryYjEntity.setInOut(1);
                        tKmZdryYjEntity.setX(toHcz.getLng());
                        tKmZdryYjEntity.setY(toHcz.getLat());
                    }
                }
                // 查询预警发生地的派出所代码和名称
                TKmJurisdictionEntity tKmJurisdictionEntity = tKmJurisdictionService.queryYjdPcsByLngAndLat(tKmZdryYjEntity.getX(),tKmZdryYjEntity.getY());
                if(null != tKmJurisdictionEntity) {
                    tKmZdryYjEntity.setYjdPcs(tKmJurisdictionEntity.getMc());
                    tKmZdryYjEntity.setYjdPcsCode(String.format("%.0f",Double.parseDouble(tKmJurisdictionEntity.getDm())));
                    tKmZdryYjEntity.setYjdSsfj(tKmJurisdictionEntity.getSsfj());
                }
                tKmZdryYjService.insert(tKmZdryYjEntity);

                ZdryYjUtilBean zdryYjUtilBean = tljp.transtoZdryYjUtilBean();
                zdryYjUtilBean.setYjwybs(tKmZdryYjEntity.getWybs());

                if(null != tKmJurisdictionEntity){
                    StringBuilder sb = new StringBuilder("重点人员【");
                    sb.append(tKmZdryYjEntity.getRyxm()).append("】在").append(tKmJurisdictionEntity.getMc()).append("的管辖范围内乘坐火车。");
                    zdryYjUtilBean.setYjMsg(sb.toString());
                }

                if(null!=tljp.getIdno()) {
                    List<String> tokens = tKmZdryYjService.findWebsocketPsuhUserListBySfzh(tljp.getIdno());
                    if(null != tokens)
                    webSocketUtils.sendWebsockMessageToUser(tokens,
                                        Arrays.asList("tljp", JSON.toJSONString(zdryYjUtilBean)), SocketType.ZDRRYJXX.getCode());
                }
                //webSocketUtils.sendWebsockMessageToAll(Arrays.asList("tldp",JSON.toJSONString(zdryYjUtilBean)), SocketType.ZDRRYJXX.getName());
            }else
                log.error("数据插入失败，插入失败的数据为："+message);
        } catch (Exception e) {
            e.printStackTrace();
           log.error("插入数据库发生异常=====>{}",e.getMessage());
        }
    }*//*
}*/
