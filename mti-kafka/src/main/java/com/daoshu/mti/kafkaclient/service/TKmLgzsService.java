package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmLgzsEntity;

/**
 * @Classname TKmLgzsService
 * @Description TODO
 * @Date 2020/1/17 23:18
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmLgzsService {
    Integer insert(TKmLgzsEntity kmLgzsEntity);
}
