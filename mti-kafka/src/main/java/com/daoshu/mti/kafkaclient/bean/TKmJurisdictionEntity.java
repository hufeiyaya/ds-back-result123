package com.daoshu.mti.kafkaclient.bean;

import lombok.Data;

@Data
public class TKmJurisdictionEntity {
    private String id;

    private String dm;

    private String mc;

    private String ssfj;

    private Object xqfw;

}