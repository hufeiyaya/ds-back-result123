package com.daoshu.mti.kafkaclient.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.daoshu.mti.kafkaclient.constant.ZdryYjLx;
import com.daoshu.mti.kafkaclient.utils.DateUtils;
import com.daoshu.mti.kafkaclient.utils.KeyWorker;
import com.daoshu.mti.kafkaclient.utils.ZdryYjUtilBean;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;
@Data
public class TKmWbswEntity {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String xxzjbh;

    private String id;

    private String loginAt;

    private String idType;

    private String idCode;

    private String idName;

    private String idSex;

    private String serviceCode;

    private String xzqh;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date rksj;

    @JSONField(format = "yyyy/MM/dd HH:mm:ss")
    private Date inserttime;

    @JSONField(format = "yyyy/MM/dd HH:mm:ss")
    private Date loginTm;

    @JSONField(format = "yyyy/MM/dd HH:mm:ss")
    private Date logoutAt;

    private String clientIp;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date bzRksj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date bzGxsj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgRksj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgGxsj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date jlSccjsj;

    /**
     * 网吧名称
     */
    private String wbmc;
    /**
     * 网吧经度
     */
    private String wbjd;

    /**
     * 网吧维度
     */
    private String wbwd;


    public ZdryYjUtilBean transtoZdryYjUtilBean(){
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.idName);
        zdryYjUtilBean.setSfzh(this.idCode);
        zdryYjUtilBean.setYjlx(ZdryYjLx.WBSW);
        zdryYjUtilBean.setYjdz(this.wbmc);
        zdryYjUtilBean.setYjsj(DateUtils.convertStrDateToOtherStr(this.getLoginAt(),"yyyy/MM/dd HH:mm:ss","yyyy-MM-dd HH:mm:ss"));
        zdryYjUtilBean.setLng(this.wbjd);
        zdryYjUtilBean.setLat(this.wbwd);
        return zdryYjUtilBean;
    }

    public TKmZdryYjEntity transtoZdryEntity(){
        TKmZdryYjEntity tKmZdryYjEntity = new TKmZdryYjEntity();
        tKmZdryYjEntity.setWybs(String.valueOf(KeyWorker.nextId()));
        tKmZdryYjEntity.setTbsj(DateUtils.formatDateToStr(new Date(),"yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setGlId(this.getXxzjbh());
        tKmZdryYjEntity.setYjlx(ZdryYjLx.WBSW);
        tKmZdryYjEntity.setRyxm(this.idName);
        tKmZdryYjEntity.setYjbs(this.getIdCode());
        tKmZdryYjEntity.setYjsj(DateUtils.convertStrDateToOtherStr(this.getLoginAt(),"yyyy/MM/dd HH:mm:ss","yyyy-MM-dd HH:mm:ss"));
        if(null != this.wbmc && this.wbmc.length()>0 && !this.wbmc.equals("null"))
        tKmZdryYjEntity.setYjdd(this.wbmc);
        else tKmZdryYjEntity.setYjdd(this.serviceCode);
        tKmZdryYjEntity.setHandleFlag(1);
        tKmZdryYjEntity.setServiceName(this.wbmc);
        tKmZdryYjEntity.setServiceCode(this.serviceCode);
        tKmZdryYjEntity.setX(this.wbjd);
        tKmZdryYjEntity.setY(this.wbwd);
        tKmZdryYjEntity.setSendFlag(0);
        return tKmZdryYjEntity;
    }


}