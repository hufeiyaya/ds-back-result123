package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmLgEntity;
import com.daoshu.mti.kafkaclient.bean.TKmLgzsEntity;
import com.daoshu.mti.kafkaclient.dao.TKmLgEntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmLgService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TKmLgServiceImpl
 * @Description TODO
 * @Date 2020/5/5 9:57
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmLgServiceImpl implements TKmLgService {
    @Resource
    private TKmLgEntityMapper tKmLgEntityMapper;

    @Override
    public TKmLgEntity getTkmlgByCode(String lvguandaima) {
        return tKmLgEntityMapper.selectByPrimaryKey(lvguandaima);
    }

    @Override
    public Boolean judgeRepeatData(TKmLgzsEntity kmLgzsEntity) {
        Integer count = tKmLgEntityMapper.judgeRepeatData(kmLgzsEntity);
        if(count>0) return true;
        return false;
    }
}
