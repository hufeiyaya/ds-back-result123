package com.daoshu.mti.kafkaclient.bean;

public class TKmLgDiztEntity {
    private String zdbh;

    private String lvguandaima;

    private String lvguanmingcheng;

    private String leixing;

    private String xiangzhi;

    private String kaiyeshijian;

    private String jingjixingzhi;

    private String tehangxukezh;

    private String farenmingcheng;

    private String lvguanzongjingli;

    private String lvguanzhianzerenren;

    private String lvguanlianxidianhua;

    private String baoanbudianhua;

    private String lvguanxingji;

    private String lvguandengji;

    private String loucengshu;

    private String fangjianshu;

    private String chuangweishu;

    private String zongtongtaofangshu;

    private String haohuataofangshu;

    private String biaojianshu;

    private String danrenjianshu;

    private String sanrenjianshu;

    private String qitafangjianshu;

    private String yuangongrenshu;

    private String baoanrenshu;

    private String insection;

    private String countyseat;

    private String station;

    private String stationid;

    private String zhuangtai;

    private String zhuangtaigaibianriqi;

    private String transfertime;

    private String receivetime;

    private String bz;

    private String packname;

    private String gxsj;

    public String getZdbh() {
        return zdbh;
    }

    public void setZdbh(String zdbh) {
        this.zdbh = zdbh == null ? null : zdbh.trim();
    }

    public String getLvguandaima() {
        return lvguandaima;
    }

    public void setLvguandaima(String lvguandaima) {
        this.lvguandaima = lvguandaima == null ? null : lvguandaima.trim();
    }

    public String getLvguanmingcheng() {
        return lvguanmingcheng;
    }

    public void setLvguanmingcheng(String lvguanmingcheng) {
        this.lvguanmingcheng = lvguanmingcheng == null ? null : lvguanmingcheng.trim();
    }

    public String getLeixing() {
        return leixing;
    }

    public void setLeixing(String leixing) {
        this.leixing = leixing == null ? null : leixing.trim();
    }

    public String getXiangzhi() {
        return xiangzhi;
    }

    public void setXiangzhi(String xiangzhi) {
        this.xiangzhi = xiangzhi == null ? null : xiangzhi.trim();
    }

    public String getKaiyeshijian() {
        return kaiyeshijian;
    }

    public void setKaiyeshijian(String kaiyeshijian) {
        this.kaiyeshijian = kaiyeshijian == null ? null : kaiyeshijian.trim();
    }

    public String getJingjixingzhi() {
        return jingjixingzhi;
    }

    public void setJingjixingzhi(String jingjixingzhi) {
        this.jingjixingzhi = jingjixingzhi == null ? null : jingjixingzhi.trim();
    }

    public String getTehangxukezh() {
        return tehangxukezh;
    }

    public void setTehangxukezh(String tehangxukezh) {
        this.tehangxukezh = tehangxukezh == null ? null : tehangxukezh.trim();
    }

    public String getFarenmingcheng() {
        return farenmingcheng;
    }

    public void setFarenmingcheng(String farenmingcheng) {
        this.farenmingcheng = farenmingcheng == null ? null : farenmingcheng.trim();
    }

    public String getLvguanzongjingli() {
        return lvguanzongjingli;
    }

    public void setLvguanzongjingli(String lvguanzongjingli) {
        this.lvguanzongjingli = lvguanzongjingli == null ? null : lvguanzongjingli.trim();
    }

    public String getLvguanzhianzerenren() {
        return lvguanzhianzerenren;
    }

    public void setLvguanzhianzerenren(String lvguanzhianzerenren) {
        this.lvguanzhianzerenren = lvguanzhianzerenren == null ? null : lvguanzhianzerenren.trim();
    }

    public String getLvguanlianxidianhua() {
        return lvguanlianxidianhua;
    }

    public void setLvguanlianxidianhua(String lvguanlianxidianhua) {
        this.lvguanlianxidianhua = lvguanlianxidianhua == null ? null : lvguanlianxidianhua.trim();
    }

    public String getBaoanbudianhua() {
        return baoanbudianhua;
    }

    public void setBaoanbudianhua(String baoanbudianhua) {
        this.baoanbudianhua = baoanbudianhua == null ? null : baoanbudianhua.trim();
    }

    public String getLvguanxingji() {
        return lvguanxingji;
    }

    public void setLvguanxingji(String lvguanxingji) {
        this.lvguanxingji = lvguanxingji == null ? null : lvguanxingji.trim();
    }

    public String getLvguandengji() {
        return lvguandengji;
    }

    public void setLvguandengji(String lvguandengji) {
        this.lvguandengji = lvguandengji == null ? null : lvguandengji.trim();
    }

    public String getLoucengshu() {
        return loucengshu;
    }

    public void setLoucengshu(String loucengshu) {
        this.loucengshu = loucengshu == null ? null : loucengshu.trim();
    }

    public String getFangjianshu() {
        return fangjianshu;
    }

    public void setFangjianshu(String fangjianshu) {
        this.fangjianshu = fangjianshu == null ? null : fangjianshu.trim();
    }

    public String getChuangweishu() {
        return chuangweishu;
    }

    public void setChuangweishu(String chuangweishu) {
        this.chuangweishu = chuangweishu == null ? null : chuangweishu.trim();
    }

    public String getZongtongtaofangshu() {
        return zongtongtaofangshu;
    }

    public void setZongtongtaofangshu(String zongtongtaofangshu) {
        this.zongtongtaofangshu = zongtongtaofangshu == null ? null : zongtongtaofangshu.trim();
    }

    public String getHaohuataofangshu() {
        return haohuataofangshu;
    }

    public void setHaohuataofangshu(String haohuataofangshu) {
        this.haohuataofangshu = haohuataofangshu == null ? null : haohuataofangshu.trim();
    }

    public String getBiaojianshu() {
        return biaojianshu;
    }

    public void setBiaojianshu(String biaojianshu) {
        this.biaojianshu = biaojianshu == null ? null : biaojianshu.trim();
    }

    public String getDanrenjianshu() {
        return danrenjianshu;
    }

    public void setDanrenjianshu(String danrenjianshu) {
        this.danrenjianshu = danrenjianshu == null ? null : danrenjianshu.trim();
    }

    public String getSanrenjianshu() {
        return sanrenjianshu;
    }

    public void setSanrenjianshu(String sanrenjianshu) {
        this.sanrenjianshu = sanrenjianshu == null ? null : sanrenjianshu.trim();
    }

    public String getQitafangjianshu() {
        return qitafangjianshu;
    }

    public void setQitafangjianshu(String qitafangjianshu) {
        this.qitafangjianshu = qitafangjianshu == null ? null : qitafangjianshu.trim();
    }

    public String getYuangongrenshu() {
        return yuangongrenshu;
    }

    public void setYuangongrenshu(String yuangongrenshu) {
        this.yuangongrenshu = yuangongrenshu == null ? null : yuangongrenshu.trim();
    }

    public String getBaoanrenshu() {
        return baoanrenshu;
    }

    public void setBaoanrenshu(String baoanrenshu) {
        this.baoanrenshu = baoanrenshu == null ? null : baoanrenshu.trim();
    }

    public String getInsection() {
        return insection;
    }

    public void setInsection(String insection) {
        this.insection = insection == null ? null : insection.trim();
    }

    public String getCountyseat() {
        return countyseat;
    }

    public void setCountyseat(String countyseat) {
        this.countyseat = countyseat == null ? null : countyseat.trim();
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station == null ? null : station.trim();
    }

    public String getStationid() {
        return stationid;
    }

    public void setStationid(String stationid) {
        this.stationid = stationid == null ? null : stationid.trim();
    }

    public String getZhuangtai() {
        return zhuangtai;
    }

    public void setZhuangtai(String zhuangtai) {
        this.zhuangtai = zhuangtai == null ? null : zhuangtai.trim();
    }

    public String getZhuangtaigaibianriqi() {
        return zhuangtaigaibianriqi;
    }

    public void setZhuangtaigaibianriqi(String zhuangtaigaibianriqi) {
        this.zhuangtaigaibianriqi = zhuangtaigaibianriqi == null ? null : zhuangtaigaibianriqi.trim();
    }

    public String getTransfertime() {
        return transfertime;
    }

    public void setTransfertime(String transfertime) {
        this.transfertime = transfertime == null ? null : transfertime.trim();
    }

    public String getReceivetime() {
        return receivetime;
    }

    public void setReceivetime(String receivetime) {
        this.receivetime = receivetime == null ? null : receivetime.trim();
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    public String getPackname() {
        return packname;
    }

    public void setPackname(String packname) {
        this.packname = packname == null ? null : packname.trim();
    }

    public String getGxsj() {
        return gxsj;
    }

    public void setGxsj(String gxsj) {
        this.gxsj = gxsj == null ? null : gxsj.trim();
    }
}