package com.daoshu.mti.kafkaclient.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.daoshu.mti.kafkaclient.constant.ZdryYjLx;
import com.daoshu.mti.kafkaclient.utils.DateUtils;
import com.daoshu.mti.kafkaclient.utils.KeyWorker;
import com.daoshu.mti.kafkaclient.utils.ZdryYjUtilBean;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;
@Data
public class TKmGlkygpEntity {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private String pgPkid;

    private String zjlb;

    private String zjhm;

    private String xm;

    private String cpbh;

    private String zwh;

    private String gpsj;

    private String gaKyzid;

    private String czid;

    private String bc;

    private String xlmc;

    private String qdzMc;

    private String zdzMc;

    private String zdzXzqhbh;

    private String fbsj;

    private String gsmc;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date jssj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgRksj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgGxsj;

    public ZdryYjUtilBean transtoZdryYjUtilBean(){
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.getXm());
        zdryYjUtilBean.setSfzh(this.getZjhm());
        zdryYjUtilBean.setYjlx(ZdryYjLx.GLKYGP);
        zdryYjUtilBean.setYjdz(this.xlmc);
        zdryYjUtilBean.setYjsj(DateUtils.convertStrDateToOtherStr(this.getGpsj(),"yyyyMMddHHmmss","yyyy-MM-dd HH:mm:ss"));
        return zdryYjUtilBean;
    }

    public TKmZdryYjEntity transtoZdryYjEntiy(){
        TKmZdryYjEntity tKmZdryYjEntity = new TKmZdryYjEntity();
        tKmZdryYjEntity.setWybs(String.valueOf(KeyWorker.nextId()));
        tKmZdryYjEntity.setYjlx(ZdryYjLx.GLKYGP);
        tKmZdryYjEntity.setGlId(this.getPgPkid());
        tKmZdryYjEntity.setRyxm(this.getXm());
        tKmZdryYjEntity.setYjbs(this.getZjhm());
        tKmZdryYjEntity.setYjdd(this.xlmc);
        tKmZdryYjEntity.setYjsj(DateUtils.convertStrDateToOtherStr(this.getGpsj(),"yyyyMMddHHmmss","yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setTbsj(DateUtils.formatDateToStr(new Date(),"yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setHandleFlag(1);
        tKmZdryYjEntity.setSendtime(DateUtils.convertStrDateToOtherStr(this.fbsj,"yyyyMMddHHmmss","yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setSendStation(this.qdzMc);
        tKmZdryYjEntity.setEndname(this.zdzMc);
        tKmZdryYjEntity.setSendFlag(0);
        return tKmZdryYjEntity;
    }

}