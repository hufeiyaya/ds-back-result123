package com.daoshu.mti.kafkaclient.service.impl;

import com.alibaba.fastjson.JSON;
import com.daoshu.mti.kafkaclient.bean.TKmZdryYjEntity;
import com.daoshu.mti.kafkaclient.bean.TLfZdryBaseEntity;
import com.daoshu.mti.kafkaclient.bean.common.UserTypeInfo;
import com.daoshu.mti.kafkaclient.dao.TKmZdryYjEntityMapper;
import com.daoshu.mti.kafkaclient.dao.TLfZdryBaseEntityMapper;
import com.daoshu.mti.kafkaclient.redis.RedisUtil;
import com.daoshu.mti.kafkaclient.service.TKmZdryYjService;
import com.daoshu.mti.kafkaclient.utils.IdCardAuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.*;

/**
 * @Classname TKmZdryYjServiceImpl
 * @Description TODO
 * @Date 2020/1/18 18:43
 * @Created by duchaof
 * @Version 1.0
 */
@Slf4j
@Service
public class TKmZdryYjServiceImpl implements TKmZdryYjService {
    @Resource
    private TKmZdryYjEntityMapper tKmZdryYjEntityMapper;

    @Resource
    private TLfZdryBaseEntityMapper tLfZdryBaseEntityMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public void insert(TKmZdryYjEntity tKmZdryYjEntity) {
        // 判断tKmZdryYjEntity.getYjbs() 是身份证号还是护照号
        if (!StringUtils.isEmpty(tKmZdryYjEntity) && !IdCardAuthUtil.authorIdCard(tKmZdryYjEntity.getYjbs())) {
            TLfZdryBaseEntity tLfZdryBaseEntity = tLfZdryBaseEntityMapper.getBaseInfoByPassportNo(tKmZdryYjEntity.getYjbs());
            //根据护照号查找身份证号
            tKmZdryYjEntity.setYjbs(tLfZdryBaseEntity.getSfzh());
        }
        tKmZdryYjEntityMapper.insert(tKmZdryYjEntity);
    }

    @Override
    public Map<String, String> getZdryInfoBySfzh(String sfzh) {
            Map<String,String> result = Optional.ofNullable(tKmZdryYjEntityMapper.getZdryInfoBySfzh(sfzh)).orElse(new HashMap<>());
            return result;
    }

    @Override
    public List<String> findWebsocketPsuhUserListBySfzh(String zjhm) {
        try {
            List<String> gkjbs = Arrays.asList("高","中","低");
            List<String> resultList = new ArrayList<>();
            List<UserTypeInfo> users = new ArrayList<>();  //当前链接websocket的所有用户
            Set<String> keies = redisUtil.keys("kmzdry_websocket_*");
            for(String key : keies){
                String s = redisUtil.get(key).toString();
                users.add(JSON.parseObject(s, UserTypeInfo.class));
            }
            Map<String,String> result = this.getZdryInfoBySfzh(zjhm);
            List<String> zrbms = null;
            if(result.containsKey("id")&& null != result.get("id")){
                 zrbms = this.getZrbmByZdryId(result.get("id"));
            }
            //角色消息分发规则：
            // 1 2 5 警种
            // 3 6   分局用户
            // 4     派出所用户
            for(UserTypeInfo u : users){
                if(u.getRole()==null) continue;
                // 判断是不是情报中心
                switch (u.getRole()){
                    case "1":resultList.add(u.getToken());break; // 情报中心
                    case "2":{ //警种
                        if(result.containsKey("gkjb") && gkjbs.contains(result.get("gkjb"))){
                            if(null != zrbms && zrbms.size()>0) {
                               if(zrbms.equals(result.get("zrbm")))
                                   resultList.add(u.getToken());
                            }
                        }
                    }break;
                    case "3":{ // 分局
                        if(result.containsKey("gkjb") && gkjbs.contains(result.get("gkjb"))){
                            if(null != result.get("ssxq")){
                                if(result.get("ssxq").equals(u.getName())) resultList.add(u.getToken());
                            }
                        }
                    }break;
                    case "4":{ //派出所
                        if(result.containsKey("gkjb") && gkjbs.contains(result.get("gkjb"))){
                            if(null != result.get("sspcs")){
                                if(result.get("sspcs").equals(u.getName())) resultList.add(u.getToken());
                            }
                        }
                    }break;
                    case "5":{ //警种(查阅)
                        if(result.containsKey("gkjb") && gkjbs.contains(result.get("gkjb"))){
                            if(null != zrbms && zrbms.size()>0) {
                                if(zrbms.equals(result.get("zrbm")))
                                {
                                    resultList.add(u.getToken());
                                }
                            }
                        }
                    }break;
                    case "6":{ // 分局
                        if(result.containsKey("gkjb") && gkjbs.contains(result.get("gkjb"))){
                            if(null != result.get("ssxq")){
                                if(result.get("ssxq").equals(u.getName())) resultList.add(u.getToken());
                            }
                        }
                    }break;

                    default: resultList.add(u.getToken());break;
                }
            }
            return resultList;
        } catch (Exception e) {
            log.info("查询推送token时发生异常：{}",e.getMessage());
            return null;
        }
    }

    /**
     * 根据重点人员的id查询相关的责任部门
     * @param id
     * @return
     */
    private List<String> getZrbmByZdryId(String id) {
        return tKmZdryYjEntityMapper.getZrbmByZdryId(id);
    }

    /**
     * 根据重点人员的身份证号判断是否需要推送预警信息
     * @param sfzh
     * @return
     */
    @Override
    public Boolean judgeInsertFlag(String sfzh) {
        try {
            Map<String,String> map = tKmZdryYjEntityMapper.judgeInsertFlag(sfzh);
            if(null == map){
                return false;
            }
            return true;
        } catch (Exception e) {
            log.error("判断预警推送时发生异常：====》{}",e.getMessage());
            return false;
        }
    }

    @Override
    public TKmZdryYjEntity authGkjbAndWkzt(String sfzh) {
        return tKmZdryYjEntityMapper.authGkjbAndWkzt(sfzh);
    }
}
