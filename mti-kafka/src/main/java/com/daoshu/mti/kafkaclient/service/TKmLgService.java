package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmLgEntity;
import com.daoshu.mti.kafkaclient.bean.TKmLgzsEntity;

/**
 * @Classname TKmLgService
 * @Description TODO
 * @Date 2020/5/5 9:57
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmLgService {
    /**
     * 通过旅馆代码查询旅馆信息
     * @param lvguandaima
     * @return
     */
    TKmLgEntity getTkmlgByCode(String lvguandaima);

    /**
     * 通过身份证号 和 预警时间判断重复数据
     * @param kmLgzsEntity
     * @return
     */
    Boolean judgeRepeatData(TKmLgzsEntity kmLgzsEntity);
}
