package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmWbswEntity;
import com.daoshu.mti.kafkaclient.dao.TKmWbswEntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmWbswService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TKmWbswServiceImpl
 * @Description TODO
 * @Date 2020/1/17 23:22
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmWbswServiceImpl implements TKmWbswService {
    @Resource
    private TKmWbswEntityMapper tKmWbswEntityMapper;

    @Override
    public Integer insert(TKmWbswEntity tKmWbswEntity) {
        return tKmWbswEntityMapper.insert(tKmWbswEntity);
    }

    @Override
    public Boolean judgeRepeatData(TKmWbswEntity tKmWbswEntity) {
        Integer count = tKmWbswEntityMapper.judgeRepeatData(tKmWbswEntity);
        if(count>0) return true;
        return false;
    }
}
