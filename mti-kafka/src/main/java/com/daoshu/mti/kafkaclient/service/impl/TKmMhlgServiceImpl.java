package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmMhlgEntity;
import com.daoshu.mti.kafkaclient.dao.TKmMhlgEntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmMhlgService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TKmMhlgServiceImpl
 * @Description TODO
 * @Date 2020/1/18 11:48
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmMhlgServiceImpl implements TKmMhlgService {
    @Resource
    private TKmMhlgEntityMapper tKmMhlgEntityMapper;

    @Override
    public Integer insert(TKmMhlgEntity tKmMhlgEntity) {
        return tKmMhlgEntityMapper.insert(tKmMhlgEntity);
    }

    @Override
    public Boolean judgeRepeatData(TKmMhlgEntity tKmMhlgEntity) {
        Integer count = tKmMhlgEntityMapper.judgeRepeatData(tKmMhlgEntity);
        if(count>0) return true;
        return false;
    }
}
