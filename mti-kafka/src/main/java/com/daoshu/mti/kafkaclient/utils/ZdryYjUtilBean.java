package com.daoshu.mti.kafkaclient.utils;

import lombok.Data;

import java.util.Date;

/**
 * @Classname ZdryYjUtilBean
 * @Description TODO
 * @Date 2020/1/19 17:26
 * @Created by duchaof
 * @Version 1.0
 */
@Data
public class ZdryYjUtilBean {

    private String yjwybs;

    private String name;

    private String sfzh;

    private String yjsj;

    private String yjlx;

    private String yjdz;

    private String lng;

    private String lat;

    private String yjMsg;

}
