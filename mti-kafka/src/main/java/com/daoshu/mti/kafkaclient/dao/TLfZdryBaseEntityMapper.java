package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TLfZdryBaseEntity;
import org.apache.ibatis.annotations.Param;

public interface TLfZdryBaseEntityMapper {
    TLfZdryBaseEntity getBaseInfoByPassportNo(@Param("passportNo") String passportNo);
}