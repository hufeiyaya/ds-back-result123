package com.daoshu.mti.kafkaclient.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @Classname ResttemplateConfig
 * @Description TODO
 * @Date 2020/1/19 18:47
 * @Created by duchaof
 * @Version 1.0
 */
@Configuration
public class ResttemplateConfig {
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
