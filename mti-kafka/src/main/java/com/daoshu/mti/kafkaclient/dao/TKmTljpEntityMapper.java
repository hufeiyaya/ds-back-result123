package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmTljpEntity;

public interface TKmTljpEntityMapper {
    int deleteByPrimaryKey(String id);

    int insert(TKmTljpEntity record);

    int insertSelective(TKmTljpEntity record);

    TKmTljpEntity selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TKmTljpEntity record);

    int updateByPrimaryKey(TKmTljpEntity record);
}