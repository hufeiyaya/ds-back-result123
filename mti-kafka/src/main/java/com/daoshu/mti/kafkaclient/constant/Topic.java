package com.daoshu.mti.kafkaclient.constant;

/**
 * @author liBai
 * @Classname Topic
 * @Description TODO
 * @Date 2019-05-14 10:53
 */

public class Topic {
    /**
     * 铁路订票
     */
    public static final String TLDP="tldpv2";
    /**
     * 网吧上网
     */
    public static final String WBSW="wbsw";
    /**
     * 旅馆住宿
     */
    public static final String LGZS="lgzs";
    /**
     * 民航订座
     */
    public static final String MHDZ="mhdz";
    /**
     * 民航离岗
     */
    public static final String MHLG="mhlg";
    /**
     * 公路客运-购票
     */
    public static final String GLKYGP="glky-gp";
    /**
     * 铁路检票
     */
    public static final String TLJP="tljp-new";
    /**
     * 预警消息
     */
    public static final String WARN="warnMassage";

}
