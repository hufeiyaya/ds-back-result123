package com.daoshu.mti.kafkaclient.bean;

import lombok.Data;

@Data
public class TKmWbEntity {
    private String id;

    private String code;

    private String name;

    private String ssfj;

    private String pcs;

    private String lng;

    private String lat;

    private String address;

}