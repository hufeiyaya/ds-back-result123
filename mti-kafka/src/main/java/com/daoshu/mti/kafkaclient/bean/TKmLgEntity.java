package com.daoshu.mti.kafkaclient.bean;

import lombok.Data;

@Data
public class TKmLgEntity {
    private String lgDm;

    private String lgMc;

    private String lgDz;

    private String lgXzqhmc;

    private String lng;

    private String lat;


}