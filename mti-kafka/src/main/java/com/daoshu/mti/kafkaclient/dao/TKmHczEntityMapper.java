package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmHczEntity;
import org.apache.ibatis.annotations.Param;

public interface TKmHczEntityMapper {
    int insert(TKmHczEntity record);

    int insertSelective(TKmHczEntity record);

    TKmHczEntity getHczByStationName(@Param("stationName") String stationName);
}