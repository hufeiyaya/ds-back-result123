package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmTljpEntity;

/**
 * @Classname TKmTljpService
 * @Description TODO
 * @Date 2020/5/11 17:57
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmTljpService {
    /**
     * 插入铁路检票数据
     * @param tljp
     * @return
     */
    Integer insert(TKmTljpEntity tljp);
}
