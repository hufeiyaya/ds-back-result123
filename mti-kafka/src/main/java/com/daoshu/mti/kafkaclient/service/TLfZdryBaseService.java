package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TLfZdryBaseEntity;

/**
 * @Classname TLfZdryBaseService
 * @Description TODO
 * @Date 2020/1/17 23:22
 * @Created by duchaof
 * @Version 1.0
 */
public interface TLfZdryBaseService {
    /**
    * 方法描述 根据护照号查询身份证号
    * @author hf
    * @date 2020/11/10
    * @param passportNo 护照号
    * @return
    */
    TLfZdryBaseEntity getBaseInfoByPassportNo(String passportNo);
}
