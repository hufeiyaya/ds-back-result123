package com.daoshu.mti.kafkaclient.constant;

/**
 * @Classname ZdryYjLx
 * @Description TODO
 * @Date 2020/1/18 19:09
 * @Created by duchaof
 * @Version 1.0
 */
public interface ZdryYjLx {
    /**
     * 铁路订票
     */
    String TLDP="tldp";
    /**
     * 网吧上网
     */
     String WBSW="wbsw";
    /**
     * 旅馆住宿
     */
     String LGZS="lgzs";
    /**
     * 民航订座
     */
     String MHDZ="mhdz";
    /**
     * 民航离岗
     */
     String MHLG="mhlg";
    /**
     * 公路客运-购票
     */
     String GLKYGP="glky-gp";
    /**
     * 铁路检票
     */
    String TLJP="tljp";
}
