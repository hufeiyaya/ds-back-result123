package com.daoshu.mti.kafkaclient.bean;


import java.util.Date;

import lombok.Data;


/**
 * @ClaaaName TKmTrailEntity
 * @Description TODO
 * @Author Liuhy
 * @Date 2020/9/316:23
 * @Version 1.0
 */
@Data
public class TKmTrailEntity {
    private String id;
    /**
     * @1：聚集，2：伴随
     */
    private String trailType;
    private Date startTime;
    private Date endTime;
    private String personCount;
    private String setId;
    private String setName;
    private Date createTime;
    private String isNew;
    private String trailInfo;
    private String setPersonType;
    private String areaId;
    private String isRead;  //是否已读
    private long sort;  
}

