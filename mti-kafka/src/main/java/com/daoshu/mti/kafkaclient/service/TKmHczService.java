package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmHczEntity;

/**
 * @Classname TKmHczService
 * @Description TODO
 * @Date 2020/5/11 20:14
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmHczService {
    /**
     * 通过火车站名称查询火车站信息
     * @param sendStation
     * @return
     */
    TKmHczEntity getHczByStationName(String sendStation);
}
