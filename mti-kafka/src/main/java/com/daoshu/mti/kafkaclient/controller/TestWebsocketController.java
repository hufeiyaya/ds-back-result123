package com.daoshu.mti.kafkaclient.controller;

import com.daoshu.mti.kafkaclient.websocket.SocketType;
import com.daoshu.mti.kafkaclient.websocket.WebSocketUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * @Classname TestWebsocket
 * @Description TODO
 * @Date 2020/1/2 9:10
 * @Created by duchaof
 * @Version 1.0
 */
@RestController
@RequestMapping("/front/group")
@Validated
public class TestWebsocketController {
    @Resource
    private WebSocketUtils webSocketUtils;
    @GetMapping("/sendToUsers")
    public Object sendToUsers(){
        ResponseEntity<String> data = (ResponseEntity<String>)webSocketUtils.sendWebsockMessageToUser(Arrays.asList("34"),
                Arrays.asList("Hello World"), SocketType.NORMAL.getCode());
        return data;
    }

    @GetMapping("/sendToGroup")
    public Object sendToGroup(){
        ResponseEntity<String> data = (ResponseEntity<String>)webSocketUtils.sendWebsockMessageToGroup(Arrays.asList("1"),
                Arrays.asList("Hello World"), SocketType.NORMAL.getCode());
        return data;
    }

    @GetMapping("/sendToAll")
    public Object sendToAll(){
        ResponseEntity<String> data = (ResponseEntity<String>)webSocketUtils.sendWebsockMessageToAll(
                Arrays.asList("Hello World"), SocketType.NORMAL.getCode());
        return data;
    }
}
