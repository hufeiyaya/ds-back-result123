package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmWbEntity;
import com.daoshu.mti.kafkaclient.dao.TKmWbEntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmWbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TKmWbServiceImpl
 * @Description TODO
 * @Date 2020/5/5 9:53
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmWbServiceImpl implements TKmWbService {
    @Resource
    private TKmWbEntityMapper tKmWbEntityMapper;

    @Override
    public TKmWbEntity getWbXxByCode(String serviceCode) {
        return tKmWbEntityMapper.getWbXxByCode(serviceCode);
    }
}
