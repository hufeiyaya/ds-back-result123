package com.daoshu.mti.kafkaclient.bean;

import lombok.Data;

@Data
public class TKmKyzEntity {
    private String gaKyzid;

    private String gaKyzmc;

    private String xzqh;

    private String city;

    private String country;

    private String czid;

    private String czmc;

    private String lng;

    private String lat;


}