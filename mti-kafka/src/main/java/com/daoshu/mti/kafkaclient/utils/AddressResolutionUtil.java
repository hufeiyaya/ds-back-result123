package com.daoshu.mti.kafkaclient.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Classname AddressResolutionUtil
 * @Description TODO
 * @Date 2020/3/17 18:12
 * @Created by duchaof
 * @Version 1.0
 */
@Slf4j
public class AddressResolutionUtil {

    /**
     *  地址坐标转换
     * @param address
     * @return
     */
    public static Map<String,String> addressResolution(String address){
        String regex="(?<province>[^省]+自治区|.*?省|.*?行政区|.*?市)(?<city>[^市]+自治州|.*?地区|.*?行政单位|.+盟|市辖区|.*?市|.*?县)(?<county>[^县]+县|.+区|.+市|.+旗|.+海域|.+岛)?(?<town>[^区]+区|.+镇)?(?<village>.*)";
        try {
                Matcher m= Pattern.compile(regex).matcher(address);
                String province= null,city=null,county=null,town=null,village=null;
                Map<String,String> row= null;
                while(m.find()){
                    row=new LinkedHashMap<String,String>();
                    province=m.group("province");
                    row.put("province", province==null?"":province.trim());
                    city=m.group("city");
                    row.put("city", city==null?"":city.trim());
                    county=m.group("county");
                    row.put("county", county==null?"":county.trim());
                    town=m.group("town");
                    row.put("town", town==null?"":town.trim());
                    village=m.group("village");
                    row.put("village", village==null?"":village.trim());
                    row.put("detail",row.get("town")+row.get("village"));
                }
            return row;
        } catch (Exception e) {
            log.error("地址截取时发生异常：=====>{}",e.getMessage());
            return null;
        }
    }


}
