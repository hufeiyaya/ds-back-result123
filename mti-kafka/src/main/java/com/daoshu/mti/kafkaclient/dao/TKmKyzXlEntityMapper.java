package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmKyzXlEntity;

public interface TKmKyzXlEntityMapper {
    int insert(TKmKyzXlEntity record);

    int insertSelective(TKmKyzXlEntity record);
}