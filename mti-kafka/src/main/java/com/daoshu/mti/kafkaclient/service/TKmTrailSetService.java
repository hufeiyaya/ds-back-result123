package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmTrailEntity;

/**
 * @Classname TKmTldpService
 * @Description TODO
 * @Date 2020/1/17 16:14
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmTrailSetService{
	
	Integer insert(TKmTrailEntity entity);
	Boolean judgeRepeatData(String id);
	Integer update(String setId);
	
}
