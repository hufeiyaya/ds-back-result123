package com.daoshu.mti.kafkaclient.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class DateUtils {
	public static String createDate()
    {
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    	String date = sdf.format(new Date());
    	return date;
    }

	/*
	 * 将时间转换为时间戳
	 */
	public static String dateToStamp(String s) throws ParseException {
		String res;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = simpleDateFormat.parse(s);
		long ts = date.getTime();
		res = String.valueOf(ts);
		return res;
	}

	/*
	 * 将时间戳转换为时间
	 */
	public static String stampToDate(String s){
		String res;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long lt = new Long(s);
		Date date = new Date(lt);
		res = simpleDateFormat.format(date);
		return res;
	}

	/*
	 * 将时间戳转换为时间
	 */
	public static Date stampToDate2(String s){
		long lt = new Long(s);
		Date date = new Date(lt);
		return date;
	}

	/**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd HH:mm:ss
     *
     * @param strDate
     * @return
     */
	public static Date strToDateLong(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	/**
	 * 时间转换
	 * @param date
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	public static Date strToDateOrTime(String date,String format)  {
		try {
			if(StringUtils.isEmpty(date))
				throw new RuntimeException("date cannot be null!");
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(StringUtils.isEmpty(format)?"yyyy-MM-dd HH:mm:ss":format);
			return simpleDateFormat.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Date formatDate(Date date,String format) {
		try {
			if(date==null || StringUtils.isEmpty(format))
				throw new RuntimeException("date or format cannot be null!");
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			String newStr = dateFormat.format(date);
			return dateFormat.parse(newStr);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String formatDateToStr(Date date,String format) {
		try {
			if(date==null || StringUtils.isEmpty(format))
				throw new RuntimeException("date or format cannot be null!");
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			String newStr = dateFormat.format(date);
			return newStr;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Date parseStrToDate(String str,String format) {
		try {
			if(str==null || StringUtils.isEmpty(format))
				throw new RuntimeException("date or format cannot be null!");
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			Date date = dateFormat.parse(str);
			return date;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public static String parseStrDateToChineseDate(String date){
		SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return format.format(format1.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String convertStrDateToOtherStr(String oriStrDate,String oriPattren,String tarPattern){
		try {
			if(StringUtils.isEmpty(oriStrDate))
				throw new RuntimeException("date cannot be null!");
			SimpleDateFormat format = new SimpleDateFormat(oriPattren);
			SimpleDateFormat format1 = new SimpleDateFormat(tarPattern);
			return format1.format(format.parse(oriStrDate));
		} catch (ParseException e) {
			e.printStackTrace();
			log.info("时间转换是发生异常："+e.getMessage());
		}
		return null;
	}
}
