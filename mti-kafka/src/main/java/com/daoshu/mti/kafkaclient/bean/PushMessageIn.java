package com.daoshu.mti.kafkaclient.bean;

import com.daoshu.mti.kafkaclient.utils.GeneratorIdCode;
import lombok.Data;
import lombok.ToString;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName: PushMessageIn
 * @description:
 * @author: Allen
 * @create: 2019-06-19 14:30
 **/
@Data
@ToString
public class PushMessageIn {

    //消息ID
    private String id;

    //要推送的用户列表
    private List<String> to;

    // 消息类型
    private String type;

    /***
     * 推送内容
     */
    //推送内容
    private List<String> body;

    /***
     * 分组ID 连接websocket时传入的groupId作为分组ID
     */

    // 分组ID
    private List<String> groupId;


    // 排序
    private String sort;


     // 推送消息类型
    private String contentType;

    /**
     * 发送给指定的某个人或者某几个人
     * @param toUserIds
     * @param param
     */
    public PushMessageIn (List<String> toUserIds, List<String> param,String contentType){
        String sendMsgId  = GeneratorIdCode.getGeneratorById("ID","01");
        this.id=sendMsgId;
        this.to=toUserIds;
        this.body=param;
        this.sort="0";
        this.contentType=contentType;
        this.type="message";
    }

    /**
     * 发送给某各组或者某几个组
     * @param to
     * @param groupIds
     * @param param
     * @param contentType
     */
    public PushMessageIn (String to,List<String> groupIds, List<String> param,String contentType){
        String sendMsgId  = GeneratorIdCode.getGeneratorById("ID","02");
        this.id=sendMsgId;
        this.to= Arrays.asList(to);
        this.groupId=groupIds;
        this.body=param;
        this.sort="0";
        this.contentType=contentType;
        this.type="message";
    }

    /**
     * 发送给所有人
     * @param param
     * @param contentType
     */
    public PushMessageIn ( List<String> param,String contentType){
        String sendMsgId  = GeneratorIdCode.getGeneratorById("ID","03");
        this.id=sendMsgId;
        this.to= Arrays.asList("*");
        this.body=param;
        this.sort="0";
        this.contentType=contentType;
        this.type="message";
    }
}
