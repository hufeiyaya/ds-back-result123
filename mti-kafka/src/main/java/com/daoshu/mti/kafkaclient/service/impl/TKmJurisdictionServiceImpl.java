package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmJurisdictionEntity;
import com.daoshu.mti.kafkaclient.dao.TKmJurisdictionMapper;
import com.daoshu.mti.kafkaclient.service.TKmJurisdictionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TKmJurisdictionServiceImpl
 * @Description TODO
 * @Date 2020/5/3 15:42
 * @Created by duchaof
 * @Version 1.0
 */
@Slf4j
@Service
public class TKmJurisdictionServiceImpl implements TKmJurisdictionService {
    @Resource
    private TKmJurisdictionMapper tKmJurisdictionMapper;

    @Override
    public TKmJurisdictionEntity queryYjdPcsByLngAndLat(String x, String y) {
        try {
            return tKmJurisdictionMapper.queryYjdPcsByLngAndLat(x,y);
        } catch (Exception e) {
            log.error("查询预警发生地辖区时发生异常："+e.getMessage());
            return null;
        }
    }
}
