package com.daoshu.mti.kafkaclient.utils;

import lombok.extern.slf4j.Slf4j;

/**
 * 方法描述 验证是否是身份证号
 *
 * @param
 * @author hf
 * @date 2020/11/10
 * @return
 */
@Slf4j
public class IdCardAuthUtil {

    /**
     * 身份证号验证
     *
     * @param idNumber
     * @return
     */
    public static boolean authorIdCard(String idNumber) {
        String regex = "\\d{15}(\\d{2}[0-9xX])?";
        if (idNumber.matches(regex)) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {

        System.out.println(authorIdCard("110101200603073772"));

    }


}
