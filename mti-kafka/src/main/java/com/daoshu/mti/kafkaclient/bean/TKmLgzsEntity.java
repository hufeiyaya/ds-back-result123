package com.daoshu.mti.kafkaclient.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.daoshu.mti.kafkaclient.constant.ZdryYjLx;
import com.daoshu.mti.kafkaclient.utils.DateUtils;
import com.daoshu.mti.kafkaclient.utils.KeyWorker;
import com.daoshu.mti.kafkaclient.utils.ZdryYjUtilBean;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class TKmLgzsEntity {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String xxzjbh;

    private String zdbh;

    private String lvkedaima;

    private String xingming;

    private String xingbie;

    private String minzu;

    private String chushengriqi;

    private String zhengjianmingcheng;

    private String zhengjianhaoma;

    private String yuanji;

    private String shengshixian;

    private String xiangzhi;

    private String timao;

    private String ruzhufanghao;

    private String ruzhushijian;

    private String tuifangshijian;

    private String leixing;

    private String tuanduidm;

    private String tuanduimc;

    private String lvguandaima;

    private String paichusuo;

    private String transfertime;

    private String receivetime;

    private String bz;

    private String hfbz;

    private String packname;

    private String rzbz;

    private String chcl;

    private String toLog;

    private String sfzh;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date rksj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date rzsj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date bzRksj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date bzGxsj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgRksj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgGxsj;

    /**
     * 旅馆名称
     */
    private String lgmc;
    /**
     * 旅馆经度
     */
    private String lgjd;

    /**
     * 旅馆维度
     */
    private String lgwd;


    public ZdryYjUtilBean transtoZdryYjUtilBean() {
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.getXingming());
        zdryYjUtilBean.setSfzh(this.getSfzh());
        zdryYjUtilBean.setYjlx(ZdryYjLx.LGZS);
        if (null != this.getRzsj()) {
            zdryYjUtilBean.setYjsj(DateUtils.formatDateToStr(this.getRzsj(), "yyyy-MM-dd HH:mm:ss"));
        }
        zdryYjUtilBean.setYjdz(this.lgmc);
        zdryYjUtilBean.setLng(this.lgjd);
        zdryYjUtilBean.setLat(this.lgwd);
        return zdryYjUtilBean;
    }

    public TKmZdryYjEntity transtoZdryYjEntity() {
        TKmZdryYjEntity tKmZdryYjEntity = new TKmZdryYjEntity();
        tKmZdryYjEntity.setWybs(String.valueOf(KeyWorker.nextId()));
        tKmZdryYjEntity.setYjlx(ZdryYjLx.LGZS);
        tKmZdryYjEntity.setGlId(this.getXxzjbh());
        tKmZdryYjEntity.setRyxm(this.getXingming());
        tKmZdryYjEntity.setYjbs(this.getSfzh());
        if (null != this.lgmc && this.lgmc.length() > 0)
            tKmZdryYjEntity.setYjdd(this.lgmc + "-" + this.getRuzhufanghao());
        else tKmZdryYjEntity.setYjdd(this.lvguandaima + "-" + this.getRuzhufanghao());
        tKmZdryYjEntity.setYjsj(DateUtils.formatDateToStr(this.getRzsj(), "yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setTbsj(DateUtils.formatDateToStr(new Date(), "yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setHandleFlag(1);
        tKmZdryYjEntity.setServiceName(this.lgmc);
        tKmZdryYjEntity.setServiceCode(this.lvguandaima);
        tKmZdryYjEntity.setX(this.getLgjd());
        tKmZdryYjEntity.setY(this.getLgwd());
        tKmZdryYjEntity.setSendFlag(0);
        return tKmZdryYjEntity;
    }
}