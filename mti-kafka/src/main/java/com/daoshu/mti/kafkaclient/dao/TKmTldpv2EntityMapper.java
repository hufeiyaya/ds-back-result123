package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmTldpv2Entity;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface TKmTldpv2EntityMapper {
    int insert(TKmTldpv2Entity record);

    int insertSelective(TKmTldpv2Entity record);

    /**
     * �ж���·  ��-��
     * @param tKmTldpv2Entity
     * @return
     */
    Integer judgeInOutType(TKmTldpv2Entity tKmTldpv2Entity);

    /**
     *  根据火车站 查询 城市信息
     * @param hcz
     * @return
     */
    Map<String, Object> queryCityByHcz(@Param("hcz") String hcz);

    /**
     * 根据身份证号 和 预警时间判断是否为重复数据
     * @param tKmTldpv2Entity
     * @return
     */
    Integer judgeRepeatData(TKmTldpv2Entity tKmTldpv2Entity);
}