package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmLgzsEntity;
import com.daoshu.mti.kafkaclient.dao.TKmLgzsEntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmLgzsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TKmLgzsServiceImpl
 * @Description TODO
 * @Date 2020/1/17 23:18
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmLgzsServiceImpl implements TKmLgzsService {
    @Resource
    private TKmLgzsEntityMapper tKmLgzsEntityMapper;

    @Override
    public Integer insert(TKmLgzsEntity kmLgzsEntity) {
        return tKmLgzsEntityMapper.insert(kmLgzsEntity);
    }
}
