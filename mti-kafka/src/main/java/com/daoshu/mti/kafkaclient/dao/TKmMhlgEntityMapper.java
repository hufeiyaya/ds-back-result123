package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmMhlgEntity;

public interface TKmMhlgEntityMapper {
    int deleteByPrimaryKey(String xxzjbh);

    int insert(TKmMhlgEntity record);

    int insertSelective(TKmMhlgEntity record);

    TKmMhlgEntity selectByPrimaryKey(String xxzjbh);

    int updateByPrimaryKeySelective(TKmMhlgEntity record);

    int updateByPrimaryKey(TKmMhlgEntity record);

    Integer judgeRepeatData(TKmMhlgEntity tKmMhlgEntity);
}