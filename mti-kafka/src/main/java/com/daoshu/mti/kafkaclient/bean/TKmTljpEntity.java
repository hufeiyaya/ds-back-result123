package com.daoshu.mti.kafkaclient.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.daoshu.mti.kafkaclient.constant.ZdryYjLx;
import com.daoshu.mti.kafkaclient.utils.DateUtils;
import com.daoshu.mti.kafkaclient.utils.KeyWorker;
import com.daoshu.mti.kafkaclient.utils.ZdryYjUtilBean;
import lombok.Data;

import java.util.Date;

@Data
public class TKmTljpEntity {
    private String id;

    private String channel;

    private String checkintime;

    private String checkinwindow;

    private String checkinwindowip;

    private String coachno;

    private String endcheckintime;

    private String folk;

    private String fromtelecode;

    private String idaddress;

    private String idbirthday;

    private String idname;

    private String idno;

    private String idsex;

    private String idtype;

    private String idvalidtimeend;

    private String idvalidtimestart;

    private String ipdescribe;

    private String isisseingauthority;

    private String orgcode;

    private String queryfile;

    private String saleoffice;

    private String salewindow;

    private String seatno;

    private String seattype;

    private String sendtime;

    private String startcheckintime;

    private String starttime;

    private String stationcode;

    private String statisticsdate;

    private String ticketno;

    private String totelecode;

    private String traincode;

    private String traindate;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date jl_sccjsj;

    public TKmZdryYjEntity transtoZdryEntity(){
        TKmZdryYjEntity tKmZdryYjEntity = new TKmZdryYjEntity();
        tKmZdryYjEntity.setWybs(String.valueOf(KeyWorker.nextId()));
        tKmZdryYjEntity.setYjlx(ZdryYjLx.TLJP);
        tKmZdryYjEntity.setRyxm(this.idname);
        tKmZdryYjEntity.setYjbs(this.idno);
        tKmZdryYjEntity.setYjsj(this.checkintime);
        tKmZdryYjEntity.setYjdd(this.stationcode);
        // x,y
        tKmZdryYjEntity.setTbsj(DateUtils.formatDateToStr(new Date(),"yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setSendtime(this.getSendtime());
        tKmZdryYjEntity.setEndname(this.totelecode);
        tKmZdryYjEntity.setGlId(this.id);
        // in_out,handle_flag
        tKmZdryYjEntity.setSendStation(this.fromtelecode);
        tKmZdryYjEntity.setSendFlag(0);
        return tKmZdryYjEntity;
    }

    public ZdryYjUtilBean transtoZdryYjUtilBean() {
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.idname);
        zdryYjUtilBean.setSfzh(this.idno);
        zdryYjUtilBean.setYjsj(this.checkintime);
        zdryYjUtilBean.setYjlx(ZdryYjLx.TLJP);
        zdryYjUtilBean.setYjdz(this.stationcode);
        return zdryYjUtilBean;
    }
}