package com.daoshu.mti.kafkaclient.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.daoshu.mti.kafkaclient.constant.ZdryYjLx;
import com.daoshu.mti.kafkaclient.utils.DateUtils;
import com.daoshu.mti.kafkaclient.utils.KeyWorker;
import com.daoshu.mti.kafkaclient.utils.ZdryYjUtilBean;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Date;
@Data
public class TKmMhlgEntity {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String xxzjbh;

    private String snId;

    private String borderType;

    private String segFltid;

    private String fltAirlcode;

    private String fltNumber;

    private String fltSuffix;

    private String fltDate;

    private String segDeptno;

    private String segDestno;

    private String segDeptCode;

    private String segDestCode;

    private String staDepttm;

    private String staArvetm;

    private String pdtDept;

    private String pdtDest;

    private String psrHostnbr;

    private String psrName;

    private String psrChnname;

    private String pdtLastname;

    private String pdtMidname;

    private String pdtFirstname;

    private String psrStatus;

    private String pdtBirthday;

    private String pdtBirAdress;

    private String psrGender;

    private String certType;

    private String certNo;

    private String pdtExprirydate;

    private String pdtIssuedate;

    private String pdtIssueCountry;

    private String pdtCountry;

    private String psrType;

    private String psrBrdno;

    private String psrGroup;

    private String psrIcs;

    private String psrCrs;

    private String psrSeatnbr;

    private String psrFffr;

    private String psrEfmn;

    private String psrBags;

    private String psrBagwgt;

    private String psrClass;

    private String psrCkipid;

    private String psrOffice;

    private String psrAgent;

    private String psrCkitime;

    private String psrSip;

    private String psrFoidNonet;

    private String psrSegSeatnbr;

    private String ffqno;

    private String datasource;

    private String telno;

    private String paxclass;

    private String adress;

    private String city;

    private String province;

    private String icaoCode;

    private String postcode;

    private String edi;

    private String sby;

    private String psrInf;

    private String psrInfname;

    private String stcr;

    private String wl;

    private String jmp;

    private String specialSeat;

    private String specialBg;

    private String svc;

    private String ckiChg;

    private String cmt;

    private String vip;

    private String ures;

    private String psrSbyno;

    private String psrStdrs;

    private String psrAcc;

    private String psrSpmlId;

    private String psrSpml;

    private String psrMsg;

    private String psrPsm;

    private String psrPil;

    private String psrPctc;

    private String psrPspt;

    private String psrNotify;

    private String psrBlnd;

    private String psrDeaf;

    private String psrEmig;

    private String psrInad;

    private String psrWtype;

    private String psrWcbd;

    private String psrWcbw;

    private String psrWcmd;

    private String psrWcob;

    private String psrCtca;

    private String psrRush;

    private String psrHbprbg;

    private String psrAvih;

    private String psrPetc;

    private String psrBaggage;

    private String psrUnattach;

    private String psrArst;

    private String psrXasr;

    private String psrXabp;

    private String psrExstseat;

    private String psrRst;

    private String psrSpe;

    private String psrAsoboi;

    private String psrOsr;

    private String psrAec;

    private String psrSea;

    private String psrRea;

    private String psrBsct;

    private String psrUdgrade;

    private String psrOfl;

    private String psrDep;

    private String psrRtfi;

    private String psrRffi;

    private String psrTsi;

    private String psrXres;

    private String psrXt;

    private String psrCitycanc;

    private String psrVudgrade;

    private String psrEdiWarn;

    private String psrInbound;

    private String psrEdii;

    private String psrOutbound;

    private String psrEdio;

    private String psrEdiDept;

    private String psrEdiDest;

    private String pnrRef;

    private String deleteFlag;

    private String deleteFltId;

    private String psrArriveTime;

    private String dupcode;

    private String flightNo;

    private String filename;


    private String ctDt;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date rksj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date importTime;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgRksj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgGxsj;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date jlSccjsj;
    /**
     * 起飞航站名称
     */
    private String segDeptCodeCn;
    /**
     * 到达航站名称
     */
    private String segDestCodeCn;


    public ZdryYjUtilBean transtoZdryYjUtilBean(){
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.getPsrChnname());
        zdryYjUtilBean.setSfzh(this.getCertNo());
        zdryYjUtilBean.setYjlx(ZdryYjLx.MHLG);
        zdryYjUtilBean.setYjdz(this.fltAirlcode+this.fltNumber);
        zdryYjUtilBean.setYjsj(this.getStaDepttm());
        return zdryYjUtilBean;
    }

    public TKmZdryYjEntity transtoZdryEntity(){
        TKmZdryYjEntity tKmZdryYjEntity = new TKmZdryYjEntity();
        tKmZdryYjEntity.setWybs(String.valueOf(KeyWorker.nextId()));
        tKmZdryYjEntity.setYjlx(ZdryYjLx.MHLG);
        tKmZdryYjEntity.setTbsj(DateUtils.formatDateToStr(new Date(),"yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setGlId(this.getXxzjbh());
        tKmZdryYjEntity.setRyxm(this.getPsrChnname());
        tKmZdryYjEntity.setYjbs(this.getCertNo());
        tKmZdryYjEntity.setYjsj(this.getStaDepttm());
        tKmZdryYjEntity.setYjdd(this.fltAirlcode+this.fltNumber);
        tKmZdryYjEntity.setHandleFlag(1);
        tKmZdryYjEntity.setSendStation(this.segDeptCodeCn);
        tKmZdryYjEntity.setSendStationIden(this.segDeptCode);
        tKmZdryYjEntity.setSendtime(this.staDepttm);
        tKmZdryYjEntity.setEndname(this.segDestCodeCn);
        tKmZdryYjEntity.setEndStationIden(this.segDestCode);
        tKmZdryYjEntity.setEndStationTime(DateUtils.strToDateOrTime(this.staDepttm,"yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setSendFlag(0);
        return tKmZdryYjEntity;
    }
}