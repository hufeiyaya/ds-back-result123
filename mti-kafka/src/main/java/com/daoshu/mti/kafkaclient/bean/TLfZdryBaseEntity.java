package com.daoshu.mti.kafkaclient.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class TLfZdryBaseEntity {

    /**
     * 主键
     */
    private String id;

    /**
     * 姓名
     */
    private String xm;

    /**
     * 性别
     */
    private String xb;

    /**
     * 民族
     */
    private String mz;

    /**
     * 身份证号
     */
    private String sfzh;

    /**
     * 户籍地
     */
    private String hjd;

    /**
     * 手机号
     */
    private String sjh;

    /**
     * 是否落实管控措施
     */
    private String sflsgkcs;

    /**
     * 具体管控措施或未控制原因
     */
    private String jtgkcshwkzyy;

    /**
     * 责任单位
     */
    private String zrdw;

    /**
     * 责任民警
     */
    private String gkmj;

    /**
     * 民警联系电话
     */
    private String mjlxdh;

    /**
     * update_time
     */
    private String updateTime;

    /**
     * 备注
     */
    private String bz;

    /**
     * job_id
     */
    private String jobId;

    /**
     * 是否在控
     */
    private String sfzk;

    /**
     * 责任部门--对应警种
     */
    private String zrbm;

    /**
     * 所属辖区--对应昆明管控责任分局
     */
    private String ssxq;

    /**
     * 是否布控
     */
    private String sfbk;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTimex;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTimex;

    /**
     * 头像
     */
    private String tx;

    /**
     * 责任派出所
     */
    private String sspcs;

    /**
     * 派出所ID
     */
    private String sspcsId;

    /**
     * 民警ID
     */
    private String mjid;

    /**
     * 是否自排(0:默认，1：自排)
     */
    private String sfzp;

    /**
     * 所属辖区ID
     */
    private String ssxqId;

    /**
     * 重点人员审批状态(0：待分局审批，1：待警种审核，2：已驳回，4：已完成)
     */
    private String approveStatus;

    /**
     * 户籍省
     */
    private String hjs;

    /**
     * 户籍市
     */
    private String hjss;

    /**
     * 户籍县
     */
    private String hjx;

    /**
     * 户籍所在地经度
     */
    private String hjdX;

    /**
     * 户籍所在地维度
     */
    private String hjdY;

    /**
     * 实际住地省
     */
    private String xzzs;

    /**
     * 实际住地市
     */
    private String xzzss;

    /**
     * 实际住地县
     */
    private String xzzx;

    /**
     * 实际居住地址
     */
    private String xzz;

    /**
     * 实际居住地址经度
     */
    private String x;

    /**
     * 实际居住地址纬度
     */
    private String y;

    /**
     * 管控级别 字典表的key
     */
    private String gkjb;

    /**
     * 管控级别（新）  对应风险等级的字典数据
     */
    private String gkjbCode;


    /**
     * 字典表key，对应人员属性类型
     */
    private Integer rysx;

    /**
     * 预警处置类别，字典表key
     */
    private Integer yjczlb;


    /**
     * 稳控状态,字典表key
     */
    private String wkzt;

    /**
     * 流程类型(0：数据导入、基本信息变更，1:新增，2:风险等级变更,3:移交至分局，30：移交分局驳回，31：分局指定派出所，321：市局警种驳回，322市局警种指定分局，323市局警种上报
     */
    private Integer processType;

    /**
     * 上报状态 0 未上报  1已上报
     */
    private Integer upStatus;

    /**
     * 提交人：account_id账户ID即警号
     */
    private String submitter;

    /**
     * 最后一次操作： 驳回， 通过
     */
    private String lastOperate;

    /**
     * 提交人角色Id
     */
    private String submitterRoleId;
    /**
     * 是否移交市外
     */
    @TableField(exist = false)
    private Integer yjsw;


    private String appeal;

    private Integer receiveFlag;

    private String sszsmc;

    private String yjbz;

    private String modifier;


    private String reviewState;


    private String approvalOpinion;


    private String rysxName;


    private String yjczlbName;


    private Integer resubmitFlag;


    private String rid;

    private Integer isFollow;

    private Integer isDelete;

    /**
     * 默认派出所
     */
    private String createOrg;

    /**
     * 默认派出所ID
     */
    private String createOrgId;

}