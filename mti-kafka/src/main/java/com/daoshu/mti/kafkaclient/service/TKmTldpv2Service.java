package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmTldpv2Entity;

import java.util.Map;

/**
 * @Classname TKmTldpv2Service
 * @Description TODO
 * @Date 2020/3/5 14:05
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmTldpv2Service {
    /**
     * @param tKmTldpv2Entity
     * @return
     */
    Integer insert(TKmTldpv2Entity tKmTldpv2Entity);

    /**
     * �ж���·��Ʊ ��-��
     * @param tKmTldpv2Entity
     * @return
     */
    Integer judgeInOutType(TKmTldpv2Entity tKmTldpv2Entity);

    /**
     * 根据火车站名称查询 城市信息
     * @param cfHcczmc
     * @return
     */
    Map<String, Object> queryCityByHcz(String cfHcczmc);

    /**
     * 根据身份证号 和 预警时间判断重复数据
     * @param tKmTldpv2Entity
     * @return
     */
    Boolean judgeRepeatData(TKmTldpv2Entity tKmTldpv2Entity);
}
