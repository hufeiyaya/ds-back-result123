package com.daoshu.mti.kafkaclient.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.daoshu.mti.kafkaclient.constant.ZdryYjLx;
import com.daoshu.mti.kafkaclient.utils.DateUtils;
import com.daoshu.mti.kafkaclient.utils.KeyWorker;
import com.daoshu.mti.kafkaclient.utils.ZdryYjUtilBean;
import lombok.Data;

import java.util.Date;
@Data
public class TKmTldpEntity {

    private String kind;

    private String sex;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgGxsj;

    private String idNo;

    private String realName;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date reserveTime;

    private String seatTypeName;

    private String passengerIdNo;

    private String userName;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date trainDate;

    private String country;

    private String seatNo;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date jlSccjsj;

    private String coachNo;

    private String dpbh;

    private String ticketTypeName;

    private String state;

    private String xxzjbh;

    private Date bornDate;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgRksj;

    private String mobileNo;

    private String fromStationName;

    private String passengerName;

    private String toStationName;

    private String ticketPrice;

    private String boardTrainCode;

    private String idName;

    private String email;

    private String phoneNo;

    public ZdryYjUtilBean transtoZdryYjUtilBean(){
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.realName);
        zdryYjUtilBean.setSfzh(this.getIdNo());
        zdryYjUtilBean.setYjlx(ZdryYjLx.TLDP);
        if(null !=this.getTrainDate())
        zdryYjUtilBean.setYjsj(DateUtils.formatDateToStr(this.getTrainDate(),"yyyy-MM-dd HH:mm:ss"));
        zdryYjUtilBean.setYjdz(this.getFromStationName());
        return zdryYjUtilBean;
    }

    public TKmZdryYjEntity transtoZdryEntity(){
        TKmZdryYjEntity tKmZdryYjEntity = new TKmZdryYjEntity();
        tKmZdryYjEntity.setWybs(String.valueOf(KeyWorker.nextId()));
        tKmZdryYjEntity.setTbsj(DateUtils.formatDateToStr(new Date(),"yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setGlId(this.getXxzjbh());
        tKmZdryYjEntity.setYjlx(ZdryYjLx.TLDP);
        tKmZdryYjEntity.setRyxm(this.getRealName());
        tKmZdryYjEntity.setYjbs(this.getIdNo());
        if(null!=this.getTrainDate())
        tKmZdryYjEntity.setYjsj(DateUtils.formatDateToStr(this.getTrainDate(),"yyyy-MM-dd HH:mm:ss"));
        return tKmZdryYjEntity;
    }

}