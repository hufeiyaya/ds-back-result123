package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmGlkygpEntity;

import java.util.Map;

/**
 * @Classname TKmGlkygpService
 * @Description TODO
 * @Date 2020/1/17 23:15
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmGlkygpService {
    Integer insert( TKmGlkygpEntity tKmGlkygpEntity);

    /**
     * 判断进出
     * @param tKmGlkygpEntity
     * @return
     */
    Integer judgeInOutType(TKmGlkygpEntity tKmGlkygpEntity);

    /**
     * 根据客运站名称查询 城市名称
     * @param qdzMc
     * @return
     */
    Map<String, Object> queryKyzInfoByName(String qdzMc);

    /**
     * 根据身份证号  和 预警时间判断重复数据
     * @param tKmGlkygpEntity
     * @return
     */
    Boolean judgeRepeatData(TKmGlkygpEntity tKmGlkygpEntity);
}
