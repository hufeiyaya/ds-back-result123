package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmJurisdictionEntity;
import org.apache.ibatis.annotations.Param;

public interface TKmJurisdictionMapper {
    int deleteByPrimaryKey(String id);

    int insert(TKmJurisdictionEntity record);

    int insertSelective(TKmJurisdictionEntity record);

    TKmJurisdictionEntity selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TKmJurisdictionEntity record);

    int updateByPrimaryKey(TKmJurisdictionEntity record);

    /**
     * 根据预警发生地经纬度查询派出所信息
     * @param x
     * @param y
     * @return
     */
    TKmJurisdictionEntity queryYjdPcsByLngAndLat(@Param("x") String x, @Param("y") String y);
}