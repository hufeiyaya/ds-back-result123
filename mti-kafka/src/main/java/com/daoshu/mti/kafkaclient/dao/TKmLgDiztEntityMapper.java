package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmLgDiztEntity;
import org.apache.ibatis.annotations.Param;

public interface TKmLgDiztEntityMapper {
    int insert(TKmLgDiztEntity record);

    int insertSelective(TKmLgDiztEntity record);

    /**
     * 根据旅馆代码查询旅馆的相关信息
     * @param lvguandaima
     * @return
     */
    TKmLgDiztEntity getLgDictInfoByLgDm(@Param("lvguandaima") String lvguandaima);
}