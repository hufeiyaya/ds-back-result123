package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmLgDiztEntity;
import com.daoshu.mti.kafkaclient.dao.TKmLgDiztEntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmLgDiztService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TKmLgDiztServiceImpl
 * @Description TODO
 * @Date 2020/5/13 16:25
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmLgDiztServiceImpl implements TKmLgDiztService {
    @Resource
    private TKmLgDiztEntityMapper tKmLgDiztEntityMapper;

    @Override
    public TKmLgDiztEntity getLgDictInfoByLgDm(String lvguandaima) {
        return tKmLgDiztEntityMapper.getLgDictInfoByLgDm(lvguandaima);
    }
}
