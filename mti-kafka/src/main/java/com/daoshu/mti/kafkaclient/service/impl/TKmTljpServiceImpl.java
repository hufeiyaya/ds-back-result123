package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmTljpEntity;
import com.daoshu.mti.kafkaclient.dao.TKmTljpEntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmTljpService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TKmTljpServiceImpl
 * @Description TODO
 * @Date 2020/5/11 17:57
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmTljpServiceImpl implements TKmTljpService {

    @Resource
    private TKmTljpEntityMapper tKmTljpEntityMapper;

    @Override
    public Integer insert(TKmTljpEntity tljp) {
        return tKmTljpEntityMapper.insert(tljp);
    }
}
