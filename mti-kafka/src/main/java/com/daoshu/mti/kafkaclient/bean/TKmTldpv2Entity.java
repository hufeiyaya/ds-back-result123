package com.daoshu.mti.kafkaclient.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.daoshu.mti.kafkaclient.constant.ZdryYjLx;
import com.daoshu.mti.kafkaclient.utils.DateUtils;
import com.daoshu.mti.kafkaclient.utils.KeyWorker;
import com.daoshu.mti.kafkaclient.utils.ZdryYjUtilBean;
import lombok.Data;

import java.util.Date;
@Data
public class TKmTldpv2Entity {
    private String systemid;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date bzkGxsj;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date bzkRksj;

    private String bzkScbz;

    private String bzkZzjg;

    private String ccBc;

    private String cfHcczmc;

    private String cxh;

    private String czLx;

    private String ddzMc;

    private String fcrqRqsj;

    private String hckSystemid;

    private String hiveRksj;

    private String lkcpzt;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgRksj;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgGxsj;

    private String xm;

    private String ywkSsxtmc;

    private String zjhm;

    private String zjlx;

    private String zjlxYwxt;

    private String zjlxZw;

    private String zwh;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date jlSccjsj;


    public ZdryYjUtilBean transtoZdryYjUtilBean() {
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.xm);
        zdryYjUtilBean.setSfzh(this.zjhm);
        zdryYjUtilBean.setYjlx(ZdryYjLx.TLDP);
        zdryYjUtilBean.setYjsj(this.fcrqRqsj);
        zdryYjUtilBean.setYjdz(this.cfHcczmc);
        return zdryYjUtilBean;
    }

    public TKmZdryYjEntity transtoZdryEntity(){
        TKmZdryYjEntity tKmZdryYjEntity = new TKmZdryYjEntity();
        tKmZdryYjEntity.setWybs(String.valueOf(KeyWorker.nextId()));
        tKmZdryYjEntity.setTbsj(DateUtils.formatDateToStr(new Date(),"yyyy-MM-dd HH:mm:ss"));
        tKmZdryYjEntity.setGlId(this.systemid);
        tKmZdryYjEntity.setYjlx(ZdryYjLx.TLDP);
        tKmZdryYjEntity.setRyxm(this.xm);
        tKmZdryYjEntity.setYjbs(this.zjhm);
        tKmZdryYjEntity.setYjsj(this.fcrqRqsj);
        tKmZdryYjEntity.setYjdd(this.ccBc);
        tKmZdryYjEntity.setHandleFlag(1);
        tKmZdryYjEntity.setSendStation(this.cfHcczmc);
        tKmZdryYjEntity.setSendtime(this.fcrqRqsj);
        tKmZdryYjEntity.setEndname(this.ddzMc);
        tKmZdryYjEntity.setSendFlag(0);
        return tKmZdryYjEntity;
    }
}