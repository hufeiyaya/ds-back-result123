package com.daoshu.mti.kafkaclient.bean;

import lombok.Data;

@Data
public class TKmHczEntity {
    private Long id;

    private String name;

    private String province;

    private String city;

    private String county;

    private Short deleted;

    private String lng;

    private String lat;
}