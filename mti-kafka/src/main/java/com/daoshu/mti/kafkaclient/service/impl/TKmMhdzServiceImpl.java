package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmMhdzEntity;
import com.daoshu.mti.kafkaclient.dao.TKmMhdzEntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmMhdzService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Classname TKmMhdzServiceImpl
 * @Description TODO
 * @Date 2020/1/17 23:20
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmMhdzServiceImpl implements TKmMhdzService {
    @Resource
    private TKmMhdzEntityMapper tKmMhdzEntityMapper;

    @Override
    public Integer insert(TKmMhdzEntity tKmMhdzEntity) {
        return tKmMhdzEntityMapper.insert(tKmMhdzEntity);
    }

    @Override
    public Integer judgeInOutType(TKmMhdzEntity tKmMhdzEntity) {
        return tKmMhdzEntityMapper.judgeInOutType(tKmMhdzEntity);
    }

    @Override
    public Map<String, Object> queryCityByMhCode(String airSegDptAirptCd) {
        if(null == airSegDptAirptCd || airSegDptAirptCd.length()==0) return null;
        return tKmMhdzEntityMapper.queryCityByMhCode(airSegDptAirptCd);
    }

    @Override
    public Boolean judgeRepeatData(TKmMhdzEntity tKmMhdzEntity) {
        Integer count = tKmMhdzEntityMapper.judgeRepeatData(tKmMhdzEntity);
        if(count>0) return true;
        return false;
    }
}
