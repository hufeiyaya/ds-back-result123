package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmMhdzEntity;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface TKmMhdzEntityMapper {
    int deleteByPrimaryKey(String xxzjbh);

    int insert(TKmMhdzEntity record);

    int insertSelective(TKmMhdzEntity record);

    TKmMhdzEntity selectByPrimaryKey(String xxzjbh);

    int updateByPrimaryKeySelective(TKmMhdzEntity record);

    int updateByPrimaryKey(TKmMhdzEntity record);

    /**
     * �ж�����Ϣ ��-��
     * @param tKmMhdzEntity
     * @return
     */
    Integer judgeInOutType(TKmMhdzEntity tKmMhdzEntity);

    /**
     * 根据民航三字码 参数城市信息
     * @param code
     * @return
     */
    Map<String, Object> queryCityByMhCode(@Param("code") String code);

    /**
     * 根据身份证号  和  预警时间判断是否为重复数据
     * @param tKmMhdzEntity
     * @return
     */
    Integer judgeRepeatData(TKmMhdzEntity tKmMhdzEntity);
}