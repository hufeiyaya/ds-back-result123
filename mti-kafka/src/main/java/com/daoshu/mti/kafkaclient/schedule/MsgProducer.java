package com.daoshu.mti.kafkaclient.schedule;

import com.alibaba.fastjson.JSON;
import com.daoshu.mti.kafkaclient.bean.*;
import com.daoshu.mti.kafkaclient.constant.Topic;
import com.daoshu.mti.kafkaclient.utils.DateUtils;
import com.daoshu.mti.kafkaclient.utils.KeyWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author liBai
 * @Classname MsgProducer
 * @Description TODO
 * @Date 2019-05-14 17:08
 */
@Slf4j
@Component
@EnableScheduling
public class MsgProducer {
    @Autowired
    private KafkaTemplate kafkaTemplate;

    private AtomicInteger flag =new AtomicInteger(90420);

    private static CopyOnWriteArrayList<String> sfzhes = new CopyOnWriteArrayList(Arrays.asList("132825197208290216",
            "132827194803150620","132828194503283241"));

    //@Scheduled(cron = "0/30 * * * * ?")
    public void send() {
        /*String flagMsg =String.valueOf(flag.getAndIncrement());
        TKmTldpEntity tKmTldpEntity = new TKmTldpEntity();
        tKmTldpEntity.setXxzjbh(flagMsg);
        Random r = new Random();
        tKmTldpEntity.setIdNo(sfzhes.get(r.nextInt(3)));
        tKmTldpEntity.setRealName("张三");
        tKmTldpEntity.setTrainDate(new Date());
        //tKmTldpEntity.setFromStationName("西安");
        //tKmTldpEntity.setToStationName("昆明北");*/
        String s = "{\"bzk_gxsj\":\"2019-12-10 20:41:18\",\"bzk_rksj\":\"2019-12-10 20:41:18\",\"bzk_scbz\":\"0\",\"bzk_zzjg\":\" \",\"cc_bc\":\"D3928\",\"cf_hcczmc\":\"昆明南\",\"cxh\":\"04\",\"cz_lx\":\"I\",\"ddz_mc\":\"石林西\",\"fcrq_rqsj\":\"2020-07-14 14:00:00.0\",\"hck_systemid\":\"203412054877830649009739\",\"hive_rksj\":\"2019-12-10\",\"jl_sccjsj\":\"2020-01-28 17:15:47\",\"lkcpzt\":\"Z\",\"pg_gxsj\":\"2019-12-10 20:41:18\",\"pg_rksj\":\"2019-12-10 20:41:18\",\"rule_reply_code\":\"\",\"systemid\":\"IUUC16759816781593544568493\",\"xm\":\"张玉珍\",\"ywk_ssxtmc\":\"公安部资源服务平台\",\"zdr_sf\":\"true\",\"zjhm\":\"532628199302012919\",\"zjlx\":\"111\",\"zjlx_ywxt\":\"1\",\"zjlx_zw\":\"居民身份证\",\"zwh\":\"002B\"}";
        TKmTldpv2Entity tKmTldpv2Entity = JSON.parseObject(s, TKmTldpv2Entity.class);
        tKmTldpv2Entity.setSystemid(String.valueOf(KeyWorker.nextId()));
        tKmTldpv2Entity.setZjhm("13282819711020469X");
        tKmTldpv2Entity.setXm("陈报国");
        ListenableFuture listenableFuture = kafkaTemplate.send(Topic.TLDP,JSON.toJSON(tKmTldpv2Entity).toString());
        listenableFuture.addCallback(
                o -> log.info("tkmtldp==>消息发送成功,{}", o.toString()),
                throwable -> log.info("消息发送失败,{}" + throwable.getMessage())
        );
    }

    //@Scheduled(cron = "0/30 * * * * ?")
    public void sendLgzs() {
        /*String flagMsg =String.valueOf(flag.getAndIncrement());
        TKmTldpEntity tKmTldpEntity = new TKmTldpEntity();
        tKmTldpEntity.setXxzjbh(flagMsg);
        Random r = new Random();
        tKmTldpEntity.setIdNo(sfzhes.get(r.nextInt(3)));
        tKmTldpEntity.setRealName("张三");
        tKmTldpEntity.setTrainDate(new Date());
        //tKmTldpEntity.setFromStationName("西安");
        //tKmTldpEntity.setToStationName("昆明北");*/
        String s = "{\"bz\":\"K\",\"bz_gxsj\":null,\"bz_rksj\":\"2020-07-14 14:14:50\",\"chcl\":null,\"chushengriqi\":\"1989/06/18 00:00:00.000000000\",\"hfbz\":\"M\",\"leixing\":null,\"lgjd\":\"\",\"lgmc\":\"\",\"lgwd\":\"\",\"lvguandaima\":\"530100310700082\",\"lvkedaima\":\"530111600000347201912152024300003041\",\"minzu\":\"01\",\"packname\":\"H530328200100035_2019121520252568_299.Rec\",\"paichusuo\":\"大板桥派出所\",\"pg_gxsj\":\"2020-02-26 01:29:42\",\"pg_rksj\":\"2020-02-26 01:12:50\",\"receivetime\":\"2019/12/15 20:24:50.000000000\",\"rksj\":\"2020-07-14 14:14:50\",\"ruzhufanghao\":\"304\",\"ruzhushijian\":\"2020/07/14 14:25:26.000000000\",\"rzbz\":null,\"rzsj\":\"2020-07-14 14:09:26\",\"sfzh\":\"53012919760305132X\",\"shengshixian\":\"云南省昭通市\",\"timao\":\"01\",\"to_log\":null,\"transfertime\":null,\"tuanduidm\":null,\"tuanduimc\":null,\"tuifangshijian\":null,\"xiangzhi\":\"云南省昭通市镇雄县赤水源镇布丈村民委员会和平村民小组55号\",\"xingbie\":\"男\",\"xingming\":\"潘金莲\",\"xxzjbh\":\"530111600000347201912152024300003041\",\"yuanji\":\"532128\",\"zdbh\":\"530111600000347\",\"zhengjianhaoma\":\"53012919760305132X\",\"zhengjianmingcheng\":\"111\"}";
        TKmLgzsEntity tKmLgzsEntity = JSON.parseObject(s,TKmLgzsEntity.class);
        tKmLgzsEntity.setXxzjbh(String.valueOf(KeyWorker.nextId()));
        tKmLgzsEntity.setSfzh("142402199501021538");
        ListenableFuture listenableFuture = kafkaTemplate.send(Topic.LGZS,JSON.toJSON(tKmLgzsEntity).toString());
        listenableFuture.addCallback(
                o -> log.info("tkmtldp==>消息发送成功,{}", o.toString()),
                throwable -> log.info("消息发送失败,{}" + throwable.getMessage())
        );
    }

    //@Scheduled(cron = "0/30 * * * * ?")
    public void sendWbsw() {
        /*String flagMsg =String.valueOf(flag.getAndIncrement());
        TKmTldpEntity tKmTldpEntity = new TKmTldpEntity();
        tKmTldpEntity.setXxzjbh(flagMsg);
        Random r = new Random();
        tKmTldpEntity.setIdNo(sfzhes.get(r.nextInt(3)));
        tKmTldpEntity.setRealName("张三");
        tKmTldpEntity.setTrainDate(new Date());
        //tKmTldpEntity.setFromStationName("西安");
        //tKmTldpEntity.setToStationName("昆明北");*/
        String s = "{\n" +
                "            \"xxzjbh\": \"1\",\n" +
                "            \"id\": \"1\",\n" +
                "            \"login_at\": \"2020/07/14 15:01:20 \",\n" +
                "            \"id_type\": \"\",\n" +
                "            \"id_code\": \"13282819711020469X\",\n" +
                "            \"id_name\": \"秦子涵\",\n" +
                "            \"id_sex\": \"\",\n" +
                "            \"service_code\": \"53212410000039\",\n" +
                "            \"xzqh\": \"\",\n" +
                "            \"rksj\": \"\",\n" +
                "            \"inserttime\": \"2020/07/14 15:01:42\",\n" +
                "            \"login_tm\": \"2020/07/14 15:01:34\",\n" +
                "            \"logout_at\": \"2020/07/14 15:02:34\",\n" +
                "            \"client_ip\": \"\",\n" +
                "            \"bz_rksj\": \"\",\n" +
                "            \"bz_gxsj\": \"\",\n" +
                "            \"pg_rksj\": \"2020-07-14 15:02:46\",\n" +
                "            \"pg_gxsj\": \"2020-07-14 15:01:46\",\n" +
                "            \"jl_sccjsj\": \"2020-07-14 15:02:46\",\n" +
                "            \"wbmc\": \"\",\n" +
                "            \"wbjd\": \"\",\n" +
                "            \"wbwd\": \"\"\n" +
                "        }";
        TKmWbswEntity wbsw = JSON.parseObject(s,TKmWbswEntity.class);
        wbsw.setXxzjbh(wbsw.getXxzjbh()+flag.getAndIncrement());
        ListenableFuture listenableFuture = kafkaTemplate.send(Topic.WBSW,JSON.toJSON(wbsw).toString());
        listenableFuture.addCallback(
                o -> log.info("wbsw==>消息发送成功,{}", o.toString()),
                throwable -> log.info("消息发送失败,{}" + throwable.getMessage())
        );
    }

    //@Scheduled(cron = "0/40 * * * * ?")
    public void sendMhdz() {
       /* String flagMsg =String.valueOf(flag.getAndIncrement());
        TKmMhdzEntity tKmMhdzEntity = new TKmMhdzEntity();
        tKmMhdzEntity.setXxzjbh(flagMsg);
        Random r = new Random();
        tKmMhdzEntity.setPasIdNbr(sfzhes.get(r.nextInt(3)));
        //tKmMhdzEntity.setAirSegDptAirptCd("PVG");
        //tKmMhdzEntity.setAirSegArrvAirptCd("KMG");
        tKmMhdzEntity.setPasChnNm("李四");
        tKmMhdzEntity.setOperatedate("20191225");
        tKmMhdzEntity.setOperatetime("12:11:10");
        tKmMhdzEntity.setAirSegDptAirptCdCn("浦东机场");
        tKmMhdzEntity.setAirSegArrvAirptCdCn("长水机场");*/
        String s1 = "{\"air_carr_cd\":\"MU\",\"air_seg_arrv_airpt_cd\":\"KMG\",\"air_seg_arrv_airpt_cd_cn\":\"长水\",\"air_seg_arrv_dt_lcl\":\"0\",\"air_seg_arrv_tm_lcl\":\"13:05:00\",\"air_seg_dpt_airpt_cd\":\"ZAT\",\"air_seg_dpt_airpt_cd_cn\":\"昭通机场\",\"air_seg_dpt_dt_lcl\":\"20200304\",\"air_seg_dpt_tm_lcl\":\"12:00:00\",\"air_seg_flt_nbr\":\"5865\",\"air_seg_flt_nbr_sfx\":null,\"ct_dt\":\"20200302 03:21:42\",\"ffp_id_nbr\":null,\"filename\":\"FuturePNR_20200302.txt.Z\",\"grp_ind\":\"N\",\"grp_nm\":null,\"offc_cd\":\"SHA777\",\"operatedate\":\"20200714\",\"operatetime\":\"14:23:00\",\"opr_stat_cd\":\"RR\",\"pas_chn_nm\":\"玉井莲\",\"pas_fst_nm\":null,\"pas_id\":\"1\",\"pas_id_nbr\":\"53012919760305132X\",\"pas_id_type\":\"I\",\"pas_lst_nm\":\"WANGXIANGJIE\",\"pg_gxsj\":\"2020-07-14 14:24:54\",\"pg_rksj\":\"2020-07-14 14:25:54\",\"pnr_cr_dt\":\"20200714\",\"pnr_ref\":\"MBGJT\",\"pn_seat\":\"1\",\"rsp_airln_cd\":\"MU\",\"rsp_ofc_cd\":\"SHA777\",\"sub_cls_cd\":\"Y\",\"vip_ind\":null,\"xxzjbh\":\"3eda82429dc5f33033b5fb0f82d0edba\"}";
        TKmMhdzEntity tKmMhdzEntity = JSON.parseObject(s1,TKmMhdzEntity.class);
        tKmMhdzEntity.setXxzjbh(String.valueOf(KeyWorker.nextId()));
        tKmMhdzEntity.setPasIdNbr("110101199003073212");
        ListenableFuture listenableFuture1 = kafkaTemplate.send(Topic.MHDZ,JSON.toJSON(tKmMhdzEntity).toString());
        listenableFuture1.addCallback(
                o -> log.info("tkmglkygp==>消息发送成功,{}", o.toString()),
                throwable -> log.info("消息发送失败,{}" + throwable.getMessage())
        );
    }

    //@Scheduled(cron = "0/30 * * * * ?")
    public void sendGlkygp() {
       /* String flagMsg =String.valueOf(flag.getAndIncrement());
        TKmGlkygpEntity tKmGlkygpEntity = new TKmGlkygpEntity();
        tKmGlkygpEntity.setPgPkid(flagMsg);
        Random r = new Random();
        tKmGlkygpEntity.setZjhm(sfzhes.get(r.nextInt(3)));
        tKmGlkygpEntity.setXm("王五");
        tKmGlkygpEntity.setGpsj("20200304040355");
        //tKmGlkygpEntity.setQdzMc("昆明市北部客运站");
        //tKmGlkygpEntity.setZdzMc("西安市三府湾客运站");*/
       //String s = "{\"bc\":\"1028\",\"cpbh\":\"SB:2Q0660\",\"czid\":\"740701\",\"fbsj\":\"20200226091800\",\"ga_kyzid\":\"530113000001\",\"gpsj\":\"20200713133805\",\"gsmc\":\"DaRui\",\"jssj\":\"2020-02-26 15:27:41\",\"pg_gxsj\":\"2020-02-26 15:32:33\",\"pg_pkid\":\"31f7eba1-7804-4ba8-9aef-b1c3952e091a\",\"pg_rksj\":\"2020-02-26 15:32:33\",\"qdz_mc\":\"陆良\",\"xlmc\":\"晋宁->昆明(新南站)\",\"xm\":\"杨义成\",\"zdz_mc\":\"昆明\",\"zdz_xzqhbh\":null,\"zjhm\":\"14010519790307399X\",\"zjlb\":\"01\",\"zwh\":\"27\"}";

        String s ="{\n" +
                "            \"pg_pkid\": \"06041c47-be05-4251-ab10-f969d55cca98\",\n" +
                "            \"zjlb\": \"01\",\n" +
                "            \"zjhm\": \"530127195905041715\",\n" +
                "            \"xm\": \"陈友华1\",\n" +
                "            \"cpbh\": \"917227390\",\n" +
                "            \"zwh\": \"1\",\n" +
                "            \"gpsj\": \"20200714135334\",\n" +
                "            \"ga_kyzid\": \"530103000001\",\n" +
                "            \"czid\": \"530103002\",\n" +
                "            \"bc\": \"6072\",\n" +
                "            \"xlmc\": \"昆明->小街\",\n" +
                "            \"qdz_mc\": \"昆明\",\n" +
                "            \"zdz_mc\": \"小街\",\n" +
                "            \"zdz_xzqhbh\": \"\",\n" +
                "            \"fbsj\": \"20200714135100\",\n" +
                "            \"gsmc\": \"TuAn\",\n" +
                "            \"jssj\": \"2020-06-04 08:51:42\",\n" +
                "            \"pg_rksj\": \"2020-06-04 08:55:02\",\n" +
                "            \"pg_gxsj\": \"2020-06-04 08:55:02\"\n" +
                "        }";

        TKmGlkygpEntity tKmGlkygpEntity = JSON.parseObject(s,TKmGlkygpEntity.class);
        tKmGlkygpEntity.setPgPkid("glky"+KeyWorker.nextId());
        tKmGlkygpEntity.setZjhm("530127195905041715");
        ListenableFuture listenableFuture1 = kafkaTemplate.send(Topic.GLKYGP,JSON.toJSON(tKmGlkygpEntity).toString());
        listenableFuture1.addCallback(
                o -> log.info("tkmglkygp==>消息发送成功,{}", o.toString()),
                throwable -> log.info("消息发送失败,{}" + throwable.getMessage())
        );
    }

    //@Scheduled(cron = "0/30 * * * * ?")
    public void sendMhlg() {
       /* String flagMsg = String.valueOf(flag.getAndIncrement());
        TKmMhlgEntity tKmMhlgEntity = new TKmMhlgEntity();
        tKmMhlgEntity.setXxzjbh(flagMsg);
        tKmMhlgEntity.setPsrChnname("张三");
        tKmMhlgEntity.setImportTime(new Date());
        Random r = new Random();
        tKmMhlgEntity.setCertNo(sfzhes.get(r.nextInt(3)));*/
        String s1 = "{\"adress\":null,\"border_type\":\"0\",\"cert_no\":\"53012919760305132X\",\"cert_type\":\"NI\"," +
                "\"city\":null,\"cki_chg\":null,\"cmt\":null,\"ct_dt\":\"20200223 12:43:00\",\"datasource\":\"1\"," +
                "\"delete_flag\":\"0\",\"delete_flt_id\":null,\"dupcode\":null,\"edi\":null,\"ffqno\":null,\"filename\":\"DEST_20200223123729.txt.Z\"," +
                "\"flight_no\":null,\"flt_airlcode\":\"MU\",\"flt_date\":\"2020-07-14\",\"flt_number\":\"5749\"," +
                "\"flt_suffix\":null,\"icao_code\":null,\"import_time\":null,\"jmp\":null,\"paxclass\":null,\"pdt_bir_adress\":null," +
                "\"pdt_birthday\":null,\"pdt_country\":null,\"pdt_dept\":null,\"pdt_dest\":null,\"pdt_exprirydate\":null," +
                "\"pdt_firstname\":null,\"pdt_issue_country\":null,\"pdt_issuedate\":null,\"pdt_lastname\":null," +
                "\"pdt_midname\":null,\"pg_gxsj\":\"2020-07-14 14:48:20.150365\",\"pg_rksj\":\"2020-07-14 14:48:20.150365\"," +
                "\"pnr_ref\":null,\"postcode\":null,\"province\":null,\"psr_acc\":null,\"psr_aec\":null,\"psr_agent\":\"33926\"," +
                "\"psr_arrive_time\":\"2020-07-14 14:48:44\",\"psr_arst\":null,\"psr_asoboi\":null,\"psr_avih\":null,\"psr_baggage\":null," +
                "\"psr_bags\":null,\"psr_bagwgt\":null,\"psr_blnd\":null,\"psr_brdno\":\"35\",\"psr_bsct\":null,\"psr_chnname\":\"于连\"," +
                "\"psr_citycanc\":null,\"psr_ckipid\":\"39044\",\"psr_ckitime\":\"2020-07-14 14:50:00\",\"psr_class\":\"R\",\"psr_crs\":null," +
                "\"psr_ctca\":null,\"psr_deaf\":null,\"psr_dep\":null,\"psr_edi_dept\":null,\"psr_edi_dest\":null,\"psr_edii\":null," +
                "\"psr_edio\":null,\"psr_edi_warn\":null,\"psr_efmn\":\"7812449982739\",\"psr_emig\":null,\"psr_exstseat\":null," +
                "\"psr_fffr\":null,\"psr_foid_nonet\":null,\"psr_gender\":\"M\",\"psr_group\":\"G55\",\"psr_hbprbg\":null," +
                "\"psr_hostnbr\":\"434\",\"psr_ics\":\"NG5N4G\",\"psr_inad\":null,\"psr_inbound\":null,\"psr_inf\":null," +
                "\"psr_infname\":null,\"psr_msg\":null,\"psr_name\":\"WANGWEI\",\"psr_notify\":null,\"psr_office\":\"KMG099\"," +
                "\"psr_ofl\":null,\"psr_osr\":null,\"psr_outbound\":null,\"psr_pctc\":null,\"psr_petc\":null,\"psr_pil\":null," +
                "\"psr_psm\":null,\"psr_pspt\":null,\"psr_rea\":null,\"psr_rffi\":null,\"psr_rst\":null,\"psr_rtfi\":null," +
                "\"psr_rush\":null,\"psr_sbyno\":null,\"psr_sea\":null,\"psr_seatnbr\":\"43C\",\"psr_seg_seatnbr\":null," +
                "\"psr_sip\":null,\"psr_spe\":null,\"psr_spml\":null,\"psr_spml_id\":null,\"psr_status\":\"AC\",\"psr_stdrs\":null," +
                "\"psr_tsi\":null,\"psr_type\":null,\"psr_udgrade\":null,\"psr_unattach\":null,\"psr_vudgrade\":null,\"psr_wcbd\":null," +
                "\"psr_wcbw\":null,\"psr_wcmd\":null,\"psr_wcob\":null,\"psr_wtype\":null,\"psr_xabp\":null,\"psr_xasr\":null," +
                "\"psr_xres\":null,\"psr_xt\":null,\"rksj\":null,\"sby\":null,\"seg_dept_code\":\"KMG\",\"seg_dept_code_cn\":\"长水国际机场\"," +
                "\"seg_deptno\":\"1\",\"seg_dest_code\":\"YNZ\",\"seg_dest_code_cn\":\"南洋机场\",\"seg_destno\":\"3\",\"seg_fltid\":\"90997698\"," +
                "\"sn_id\":\"10062816465\",\"special_bg\":null,\"special_seat\":null,\"sta_arvetm\":\"2020-02-23 18:00:00\"," +
                "\"sta_depttm\":\"2020-07-14 14:45:00\",\"stcr\":null,\"svc\":null,\"telno\":\"13888198660\",\"ures\":null," +
                "\"vip\":null,\"wl\":null,\"xxzjbh\":\"da204a4a-588a-45e0-9bea-842f8d24d1f9\"}";
        TKmMhlgEntity tKmMhlgEntity = JSON.parseObject(s1,TKmMhlgEntity.class);
        tKmMhlgEntity.setXxzjbh(String.valueOf(KeyWorker.nextId()));
        tKmMhlgEntity.setCertNo("142402199501021538");
        ListenableFuture listenableFuture1 = kafkaTemplate.send(Topic.MHLG, JSON.toJSON(tKmMhlgEntity).toString());
        listenableFuture1.addCallback(
                o -> log.info("tkmglkygp==>消息发送成功,{}", o.toString()),
                throwable -> log.info("消息发送失败,{}" + throwable.getMessage())
        );
    }

    //@Scheduled(cron = "0/30 * * * * ?")
    public void sendTljp() {
        TKmTljpEntity tljp = new TKmTljpEntity();
        tljp.setChannel("");
        tljp.setCheckintime("2020-05-10 20:16:03");
        tljp.setCheckinwindow("16");
        tljp.setCheckinwindowip("198.181.89.142");
        tljp.setCoachno("15");
        tljp.setEndcheckintime(null);
        tljp.setFolk("01");
        tljp.setFromtelecode("昆明");
        tljp.setId(String.valueOf(KeyWorker.nextId()));
        tljp.setIdaddress("云南省昆明市");
        tljp.setIdbirthday("19720402");
        tljp.setIdname("吴秀梅");
        tljp.setIdno("13282819711020469X");
        tljp.setIdsex("");
        tljp.setIdtype("01");
        tljp.setIdvalidtimeend("20280311");
        tljp.setIdvalidtimestart("20080311");
        tljp.setIpdescribe(null);
        tljp.setIsisseingauthority(null);
        tljp.setJl_sccjsj(DateUtils.parseStrToDate("2020-05-10 21:11:41","yyyy-MM-dd HH:mm:ss"));
        tljp.setOrgcode(null);
        tljp.setQueryfile(null);
        tljp.setSaleoffice("4819756");
        tljp.setSalewindow("086");
        tljp.setSeatno("0055");
        tljp.setSeattype("1");
        tljp.setSendtime("2020-05-10 20:16:17:883");
        tljp.setStartcheckintime(null);
        tljp.setStarttime("2018");
        tljp.setStationcode("昆明");
        tljp.setStatisticsdate("20200510");
        tljp.setTicketno("9079850");
        tljp.setTotelecode("河口北");
        tljp.setTraincode("C8339");
        tljp.setTraindate("20200510");
        log.info("发送铁路检票的数据");
        ListenableFuture listenableFuture1 = kafkaTemplate.send(Topic.TLJP, JSON.toJSON(tljp).toString());
        listenableFuture1.addCallback(
                o -> log.info("tkmglkygp==>消息发送成功,{}", o.toString()),
                throwable -> log.info("消息发送失败,{}" + throwable.getMessage())
        );
    }

}
