package com.daoshu.mti.kafkaclient.dao;

import com.daoshu.mti.kafkaclient.bean.TKmKyzEntity;
import org.apache.ibatis.annotations.Param;

public interface TKmKyzEntityMapper {
    int insert(TKmKyzEntity record);

    int insertSelective(TKmKyzEntity record);

    /**
     * 根据公安客运站名称查询客运站信息
     * @param gaKyzid
     * @return
     */
    TKmKyzEntity getKyzXxByGaId(@Param("gaKyzid") String gaKyzid);

    /**
     *  通过线路名车获取客运站信息
     * @param xlmc
     * @return
     */
    TKmKyzEntity getKyzXxByXlmc(@Param("xlmc") String xlmc);

    /**
     * 通过客运站名称查询客运站信息
     * @param station
     * @return
     */
    TKmKyzEntity getKyzByKyzMc(@Param("station") String station);
}