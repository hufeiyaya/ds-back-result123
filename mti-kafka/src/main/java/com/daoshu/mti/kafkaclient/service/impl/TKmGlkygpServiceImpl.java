package com.daoshu.mti.kafkaclient.service.impl;

import com.daoshu.mti.kafkaclient.bean.TKmGlkygpEntity;
import com.daoshu.mti.kafkaclient.dao.TKmGlkygpEntityMapper;
import com.daoshu.mti.kafkaclient.service.TKmGlkygpService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Classname TKmGlkygpServiceImpl
 * @Description TODO
 * @Date 2020/1/17 23:15
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmGlkygpServiceImpl implements TKmGlkygpService {

    @Resource
    private TKmGlkygpEntityMapper tKmGlkygpEntityMapper;

    @Override
    public Integer insert(TKmGlkygpEntity tKmGlkygpEntity) {
        return tKmGlkygpEntityMapper.insert(tKmGlkygpEntity);
    }

    @Override
    public Integer judgeInOutType(TKmGlkygpEntity tKmGlkygpEntity) {
        return tKmGlkygpEntityMapper.judgeInOutType(tKmGlkygpEntity);
    }

    @Override
    public Map<String, Object> queryKyzInfoByName(String qdzMc) {
        if(null==qdzMc || qdzMc.length()==0) return null;
        return tKmGlkygpEntityMapper.queryKyzInfoByName(qdzMc);
    }

    @Override
    public Boolean judgeRepeatData(TKmGlkygpEntity tKmGlkygpEntity) {
        Integer count = tKmGlkygpEntityMapper.judgeRepeatData(tKmGlkygpEntity);
        if(count>0) return true;
        return false;
    }


}
