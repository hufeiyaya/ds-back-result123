package com.daoshu.mti.kafkaclient.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
@Data
public class TKmZdryYjEntity {
    private String wybs;

    private String yjlx;

    private String ryxm;

    private String yjbs;

    private String sjbs;

    private String yjsj;

    private String yjdd;

    private String ddmc;

    private String gxsj;

    private String x;

    private String y;

    private String tbsj;

    private String xsd;

    private String rlzp;

    private String zjzp;

    private String zjzp2;

    private String zjzp3;

    private String pcmjid;

    private String pcmjname;

    private String pcmjdh;

    private String pcpcs;

    private String pcpcsdh;

    private String ticketno;

    private String seatno;

    private String busno;

    private String prtname;
    /**
     * 发车时间
     */
    private String sendtime;

    /**
     * 终点站
     */
    private String endname;

    /**
     * 关联预警主键
     */
    private String glId;
    /**
     * 民航进出判断标志
     */
    private Integer inOut;

    /**
     * 预警处置标志：1 为处置，2  超时，3 已处置
     */
    private Integer handleFlag;

    /**
     * 发车点
     */
    private String sendStation;
    /**
     * 发车点唯一标识
     */
    private String sendStationIden;

    /**
     * 终点点唯一标识
     */
    private String endStationIden;

    /**
     * 到站时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endStationTime;

    /**
     * 网吧/旅馆名称
     */
    private String serviceName;

    /**
     * 网吧/旅馆标识
     */
    private String serviceCode;
    /**
     * 出发城市
     */
    private String sendCity;
    /**
     * 目的地城市
     */
    private String endCity;

    /**
     * 出发省份
     */
    private String sendPro;

    /**
     * 到达省份
     */
    private String endPro;

    /**
     * 目前辖区
     */
    private String currentJur;

    /**
     * 出发站经度
     */
    private String sendLng;

    /**
     * 出发站维度
     */
    private String sendLat;

    /**
     * 终点站经度
     */
    private String endLng;

    /**
     * 终点站维度
     */
    private String endLat;

    /**
     * 预警发生地派出所
     */
    private String yjdPcs;

    /**
     * 预警发生地派出所编码
     */
    private String yjdPcsCode;

    /**
     * 预警发送标志：0 未发送，1 已发送
     */
    private Integer sendFlag;

    /**
     * 预警发生地分局
     */
    private String yjdSsfj;

    /**
     *  预警发生的分局编码
     */
    private String yjdSsfjCode;
}