package com.daoshu.mti.kafkaclient.service;

import com.daoshu.mti.kafkaclient.bean.TKmWbEntity;

/**
 * @Classname TKmWbService
 * @Description TODO
 * @Date 2020/5/5 9:52
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmWbService {
    /**
     * 通过网吧code获取网吧信息
     * @param serviceCode
     * @return
     */
    TKmWbEntity getWbXxByCode(String serviceCode);
}
