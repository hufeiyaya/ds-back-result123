package com.daoshu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.jmx.support.RegistrationPolicy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@MapperScan(basePackages = "com.daoshu.**.mapper")
@EnableTransactionManagement
@EnableCaching
@EnableAsync
/**
 *  解决jmx重复注册bean的问题
 */
@EnableMBeanExport(registration = RegistrationPolicy.IGNORE_EXISTING)
@ServletComponentScan
@SpringBootApplication(scanBasePackages = "com.daoshu")
@EnableDiscoveryClient
@EnableEurekaClient
@EnableFeignClients
@EnableScheduling
/**
 * MdsMessageApplication class
 *
 * @author zhaoyj
 * @date 2019/4/5
 */
public class UcpSystemApplication {
	
		public static void main(String[] args) {
		SpringApplication.run(UcpSystemApplication.class, args);
	}

}
