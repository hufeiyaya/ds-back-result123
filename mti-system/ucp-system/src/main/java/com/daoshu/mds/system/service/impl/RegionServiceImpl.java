package com.daoshu.mds.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.system.mapper.RegionMapper;
import com.daoshu.mds.system.service.IRegionService;
import com.daoshu.system.entity.RegionEntity;

/**
 * <p>
 *  区域控制类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Service
@CacheConfig(cacheNames = "regionCacheable")
public class RegionServiceImpl extends ServiceImpl<RegionMapper, RegionEntity> implements IRegionService {
	
	@Resource
	private RegionMapper regionMapper;

	public List<RegionEntity> getRegionByName(String region_name) {
//		        QueryWrapper<RegionEntity> queryWrapper = new QueryWrapper<>();
//		        queryWrapper.select("region_name ,region_code");
//		        queryWrapper.eq("parent_code (select region_code from t_pf_region t where t.region_name='", ""+region_name+"')");
		        List<RegionEntity> region = regionMapper.getRegionByName(region_name);
		        		//selectList(queryWrapper);
		        
				return region;
		        
	}

	@Override
	public List<RegionEntity> getAllcity(Integer level) {
		QueryWrapper<RegionEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("level",level);
		List<RegionEntity> region = regionMapper.selectList(queryWrapper);
		return region;
	}

	@Override
	@Cacheable(key = "#root.method+'('+#pid+')'") 
	public List<RegionEntity> getRegionByPid(Integer pid) {
		List<RegionEntity> regionEntities = regionMapper.getRegionByPid(pid);
		if(null!=regionEntities){
			return regionEntities;
		}
		return null;
	}
	
	@Override
	@Cacheable(key = "#root.method+'('+#all+')'") 
	public List<RegionEntity> getList(Integer all) {
		List<RegionEntity> result = this.list(new QueryWrapper<RegionEntity>().isNull("parent_code").orderByAsc("sort"));
		return result;
	}

	
	public List<RegionEntity> getAllProvince() {
		        QueryWrapper<RegionEntity> queryWrapper = new QueryWrapper<>();
		        queryWrapper.select("region_name ,region_code");
		        queryWrapper.isNull("parent_code");
		        List<RegionEntity> region = regionMapper.selectList(queryWrapper);
		        return region;
	}

	@Override
	public List<List<RegionEntity>> getRegionByKeyword(String keyword) {
        QueryWrapper<RegionEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("region_name", keyword);
        queryWrapper.lambda().last("LIMIT 10");
        List<RegionEntity> regions = regionMapper.selectList(queryWrapper);
        List<List<RegionEntity>> list = new ArrayList<List<RegionEntity>>();
        for (RegionEntity region : regions) {
        	//String[] ssx= new String[3];
        	List<RegionEntity> tempRegion =new ArrayList<RegionEntity>();
        	if(2==region.getLevel()) {
        		RegionEntity sRegion = this.getRegionByCode(region.getParentCode());
        		tempRegion.add(sRegion);
        		tempRegion.add(region);
//        		ssx[0]=sRegion.getId();
//        		ssx[1]=region.getId();
        		list.add(tempRegion);
        	}else if(3==region.getLevel()){
        		RegionEntity cityRegion = this.getRegionByCode(region.getParentCode());
        		RegionEntity sRegion = this.getRegionByCode(cityRegion.getParentCode());
        		tempRegion.add(sRegion);
        		tempRegion.add(cityRegion);
        		tempRegion.add(region);
//        		ssx[0]=sRegion.getId();
//        		ssx[1]=cityRegion.getId();
//        		ssx[2]=region.getId();
        		list.add(tempRegion);
        	}else {
        		tempRegion.add(region);
        		list.add(tempRegion);
//        		ssx[0]=region.getId();
//        		list.add(ssx);
        	}
        	
		}
        return list;
}

	@Override
	public RegionEntity getRegionByCode(String code) {
		QueryWrapper<RegionEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("region_code", code);
        RegionEntity region = regionMapper.selectOne(queryWrapper);
        return region;
	}
	
}
