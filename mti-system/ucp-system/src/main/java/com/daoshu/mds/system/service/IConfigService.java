package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.entity.ConfigEntity;
import com.daoshu.system.entity.SysDictEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface IConfigService extends IService<ConfigEntity> {

    List<SysDictEntity> selectPersonDXL();

    boolean savePersonDXL(SysDictEntity sysDictEntity);

    boolean updatePersonDXL(SysDictEntity sysDictEntity);

    boolean deletePersonDXL(SysDictEntity sysDictEntity);

    boolean onOffDXL(SysDictEntity sysDictEntity);

}
