package com.daoshu.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.system.entity.AccountRoleEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface AccountRoleMapper extends BaseMapper<AccountRoleEntity> {

    /**
     * 根据人员ID 删除角色
     * @param accountId
     */
    void deletePersonRole(@Param("accountId") String accountId);

    /**
     * 根据账户ID获取所有角色
     * @param accountId accountId
     * @return
     */
    List<Map<String,String>> getRoleListByAccountId(@Param(value = "accountId")String accountId);

    List<Map<String,String>> getPersonInfoByAccountId(@Param(value = "accountId")String accountId);
}
