package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.entity.DictionaryEntity;

/**
*@Description: 字典信息接口
*@Param: 
*@return: 
*@Author: Williams Guo
*@date: 2018-12-12
*/
public interface IDictionaryService extends IService<DictionaryEntity> {

    DictionaryEntity getCacheDict(String key, String type);
}
