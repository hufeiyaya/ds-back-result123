package com.daoshu.mds.system.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.bo.RoleBo;
import com.daoshu.system.entity.RoleEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface IRoleService extends IService<RoleEntity> {

	/**
	 * 分页查询角色信息
	 * @Title: getPage
	 * @Description: TODO
	 * @param roleBo
	 * @return
	 * @return: IPage<RoleEntity>
	 */
	IPage<RoleEntity> getPage(RoleBo roleBo);
	
	/**
	 * 更新或者保存角色信息
	 * @Title: saveOrUpd
	 * @Description: TODO
	 * @param entity
	 * @return
	 * @return: RoleEntity
	 */
	RoleEntity saveOrUpd(RoleEntity entity);
	
	/**
	 * 通过ids删除角色信息
	 * @Title: removeByIds
	 * @Description: TODO
	 * @param ids
	 * @return
	 * @return: int
	 */
	int removeByIds(String ids);
	/**
	 * 根据种类获取属于自己的角色
	 * @param orgSort
	 * @return
	 */
	List<RoleEntity> getOrgsByType (Integer orgSort) ;

	/**
	 * 根据组织id查询角色列表
	 * @param regionId
	 * @return
	 */
	List<RoleEntity> getListByRegionId(String regionId);
}
