package com.daoshu.mds.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.system.mapper.AccountMenuMapper;
import com.daoshu.mds.system.service.IAccountMenuService;
import com.daoshu.system.entity.AccountMenuEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Service
public class AccountMenuServiceImpl extends ServiceImpl<AccountMenuMapper, AccountMenuEntity> implements IAccountMenuService {

	@Override
	public Set<String> getMenuIdsByAccountId(String accountId) {
		AccountMenuEntity accountMenuEntity = this.getOne(new QueryWrapper<AccountMenuEntity>().eq("account_id_", accountId));
		if (null == accountMenuEntity) {
			return new HashSet<String>();
		}
		return new HashSet<String>(Arrays.asList(accountMenuEntity.getMenuIds().split(",")));
	}
}
