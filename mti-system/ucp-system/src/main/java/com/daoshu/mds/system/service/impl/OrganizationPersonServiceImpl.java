package com.daoshu.mds.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.system.mapper.OrganizationPersonMapper;
import com.daoshu.mds.system.service.IOrganizationPersonService;
import com.daoshu.system.bo.OrganizationPersonBo;
import com.daoshu.system.entity.OrganizationPersonEntity;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Service
public class OrganizationPersonServiceImpl extends ServiceImpl<OrganizationPersonMapper, OrganizationPersonEntity> implements IOrganizationPersonService {

	@Override
	public IPage<OrganizationPersonEntity> getPage(OrganizationPersonBo organizationPersonBo) {
		OrganizationPersonEntity condition = new OrganizationPersonEntity();
		BeanUtils.copyProperties(organizationPersonBo, condition);
		Wrapper<OrganizationPersonEntity> queryWrapper = new QueryWrapper<OrganizationPersonEntity>(condition);
		return this.page(new Page<>(organizationPersonBo.getStart(), organizationPersonBo.getSize()), queryWrapper);
	}

	@Override
	public boolean sortPosition(List<Map<String, Object>> sorts) {
		if (CollectionUtils.isNotEmpty(sorts)) {
			OrganizationPersonEntity entity = new OrganizationPersonEntity();
			sorts.forEach(item -> {
				entity.setPersonId(item.get("pertonId").toString());
				entity.setPositionSort(Integer.parseInt(item.get("sort").toString()));
				this.update(entity, new QueryWrapper<OrganizationPersonEntity>().eq("person_id_", entity.getPersonId()));
			});
			return true;
		}
		return false;
	}
}
