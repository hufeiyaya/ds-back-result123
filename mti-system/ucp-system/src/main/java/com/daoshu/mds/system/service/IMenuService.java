package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.bo.MenuBo;
import com.daoshu.system.entity.MenuEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface IMenuService extends IService<MenuEntity> {

	/**
	 * 分页获取菜单信息
	 * @Title: getPage
	 * @Description: TODO
	 * @param menuBo
	 * @return
	 * @return: IPage<MenuEntity>
	 */
	IPage<MenuEntity> getPage(MenuBo menuBo);
}
