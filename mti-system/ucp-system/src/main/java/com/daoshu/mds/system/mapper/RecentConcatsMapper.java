package com.daoshu.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daoshu.system.entity.RecentConcatsEntity;
import com.daoshu.system.qo.RecentConcatsQO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  *  最近联系人Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface RecentConcatsMapper extends BaseMapper<RecentConcatsEntity> {

	IPage<RecentConcatsEntity> queryRecent(Page<RecentConcatsEntity> page, @Param("qo") RecentConcatsQO qo);
}