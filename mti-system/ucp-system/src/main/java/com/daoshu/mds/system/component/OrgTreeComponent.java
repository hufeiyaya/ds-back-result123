package com.daoshu.mds.system.component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.daoshu.mds.system.service.IOrganizationService;
@Component
public class OrgTreeComponent implements CommandLineRunner{

	@Autowired private IOrganizationService organizationService;
	@Override
	public void run(String... args) throws Exception {
		organizationService.getOrgTree(1, 1, false, "0");
	}

}
