package com.daoshu.mds.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.system.mapper.LoginLogMapper;
import com.daoshu.mds.system.service.ILoginLogService;
import com.daoshu.system.entity.CpLoginLog;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class LoginLogServiceImpl extends ServiceImpl<LoginLogMapper, CpLoginLog> implements ILoginLogService {
}
