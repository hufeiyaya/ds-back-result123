package com.daoshu.mds.system.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.daoshu.component.page.PageConstants;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.system.service.IRecentConcatsService;
import com.daoshu.system.bo.RecentConcatBO;
import com.daoshu.system.entity.RecentConcatsEntity;
import com.daoshu.system.qo.RecentConcatsQO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 *  最近联系人前端控制器
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Api(value="最近联系人接口",description="最近联系人接口")
@RestController
@RequestMapping("/mds")
public class RecentConcatsController {
	
	@Autowired private IRecentConcatsService recentConcatsService;
	
	@ApiOperation(value="最近通讯录查询",notes="最近通讯录查询")
	@RequestMapping(value="/recentConcats/page",method=RequestMethod.POST)
	public ResponseInfo queryLocalUserGeneralConcats(@RequestBody final RecentConcatsQO qo){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				IPage<RecentConcatBO> page = recentConcatsService.queryRecent(qo);
				criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
				criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
			}
		}.sendRequest();
	}
	@ApiOperation(value="最近通讯录查询(简单列表)",notes="最近通讯录查询")
	@RequestMapping(value="/recentConcats/simplePage",method=RequestMethod.POST)
	public ResponseInfo queryRecentSimple(@RequestBody final RecentConcatsQO qo){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				IPage<RecentConcatBO> page = recentConcatsService.queryRecentSimple(qo);
				criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
				criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
			}
		}.sendRequest();
	}
	@ApiOperation(value="最近通讯录添加（被叫添加入口）",notes="concat,createTime,endTime,connectType(1语音，2音视频)为必须字段")
	@RequestMapping(value="/recentConcats",method=RequestMethod.POST)
	public ResponseInfo add(@RequestBody final RecentConcatsEntity entity){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(recentConcatsService.addRecent(entity));
			}
		}.sendRequest();
	}
	@ApiOperation(value="统计",notes="统计")
	@RequestMapping(value="/recentConcats/statistics",method=RequestMethod.GET)
	public ResponseInfo statistics(){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(recentConcatsService.statistics());
			}
		}.sendRequest();
	}
}
