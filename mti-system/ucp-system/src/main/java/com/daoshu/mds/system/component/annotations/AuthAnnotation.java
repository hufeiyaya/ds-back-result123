package com.daoshu.mds.system.component.annotations;

import java.lang.annotation.*;

/**
 * @author zhangmingxin
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthAnnotation {
    /**
     * 操作类型
     * @return 类型值
     */
    String opVal();
}
