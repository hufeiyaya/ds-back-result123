package com.daoshu.mds.system.threadhandle;

import com.daoshu.system.entity.CpAuthLog;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @Classname ZdryExportHandle
 * @Description TODO
 * @Date 2020/5/14 16:08
 * @Created by duchaof
 * @Version 1.0
 */
@Data
@Slf4j
@AllArgsConstructor
public class AuthLogExportHandle implements Runnable {

    private SXSSFSheet sh;

    private CellStyle cellStyleCenter;

    private CellStyle cellStyleLeft;

    private List<CpAuthLog> list;

    private String exportName;

    private CountDownLatch downLatch;

    @Override
    public void run() {
        try {
            int totalSize = 2 + list.size();
            for (int rowNum = 0; rowNum < totalSize; rowNum++) {
                Row row = sh.createRow(rowNum);
                for (int colNum = 0; colNum < 8; colNum++) {
                    Cell cell1 = row.createCell(colNum);
                    //设置单元格样式
                    cell1.setCellStyle(cellStyleLeft);
                    String title = "";
                    if (rowNum == 1) {
                        switch (colNum) {
                            case 0:
                                title = "序号";
                                break;
                            case 1:
                                title = "警号";
                                break;
                            case 2:
                                title = "姓名";
                                break;
                            case 3:
                                title = "单位";
                                break;
                            case 4:
                                title = "授权类型";
                                break;
                            case 5:
                                title = "授权角色";
                                break;
                            case 6:
                                title = "授权时间";
                                break;
                            case 7:
                                title = "授权IP";
                                break;
                        }
                        cell1.setCellValue(title);
                    } else {
                        if (rowNum > 1) {
                            //数据总的条数
                            CpAuthLog entity = list.get(rowNum - 2);
                            //设置列号
                            if (colNum == 0)
                                cell1.setCellValue(rowNum - 1);
                            else {
                                switch (colNum) {
                                    case 1:
                                        cell1.setCellValue(entity.getNumber());
                                        break;
                                    case 2:
                                        cell1.setCellValue(entity.getName());
                                        break;
                                    case 3:
                                        cell1.setCellValue(entity.getOrg());
                                        break;
                                    case 4:
                                        cell1.setCellValue(entity.getAuthRole());
                                        break;
                                    case 5:
                                        cell1.setCellValue(entity.getAuthRoleName());
                                        break;
                                    case 6:
                                        cell1.setCellValue(entity.getAuthDateTime());
                                        break;
                                    case 7:
                                        cell1.setCellValue(entity.getIp());
                                        break;
                                }
                            }
                        } else

                            cell1.setCellValue("");
                    }

                }

                if (rowNum == 0) {
                    sh.addMergedRegion(new CellRangeAddress(0, 0, 0, 8));
                    sh.getRow(rowNum).getCell(0).setCellValue("授权日志导出列表");
                    //设置单元格样式
                    sh.getRow(rowNum).getCell(0).setCellStyle(cellStyleCenter);
                }
            }
            //设置列宽默认自适应
            for (int i = 0; i < totalSize; i++) {
                sh.autoSizeColumn(i, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("给execl写入数据时发生异常：========》{}", e.getMessage());
        } finally {
            downLatch.countDown();
        }
    }
}
