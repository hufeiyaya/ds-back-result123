package com.daoshu.mds.system.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.bo.AccountBo;
import com.daoshu.system.bo.OrganizationBo;
import com.daoshu.system.entity.OrganizationEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface IOrganizationService extends IService<OrganizationEntity> {

	/**
	 * 分页查询组织机构列表信息
	 * @Title: getPage
	 * @Description: TODO
	 * @param organizationBo
	 * @return
	 * @return: IPage<OrganizationEntity>
	 */
	IPage<OrganizationEntity> getPage(OrganizationBo organizationBo);

	/**
	 * 获取组织机构树
	 * @Title: getOrgTree
	 * @Description: TODO
	 * @param type
	 * @param isBusiness true: 
	 * @return
	 * @return: List<OrganizationEntity>
	 */
	List<OrganizationEntity> getOrgTree(Integer type, Integer contain, boolean isBusiness, String rootId);

	/**
	 * 获取其他州市责任分局
	 */
	List<OrganizationEntity> getTreeZrfj(Integer type, Integer contain, boolean isBusiness, String rootId,String ssxq,String sspcs);
	/**
	 * 
	 * @Title: getDetailByPId
	 * @Description: 通过父ID获取所有儿子和孙子节点
	 * @param isBusiness
	 * @param pId
	 * @return
	 * @return: List<OrganizationEntity>
	 */
	List<OrganizationEntity> getDetailByPId(boolean isBusiness, String pId, Boolean isInternal,boolean systemUserFlag);
	/**
	 * 
	 * @Title: getDetailByPId
	 * @Description: 通过父ID获取 派出所儿子 节点
	 * @param orgType
	 * @param pId
	 * @return keyword
	 * @return: List<OrganizationEntity>
	 */
	List<OrganizationEntity> getOrgByPId(String orgType, String pId, String keyword);
	
	/**
	 * 
	 * @Title: getOrgByOrgtype
	 * @Description: 通过组织类型获取所有分局
	 * @param isBusiness
	 * @param pId
	 * @return
	 * @return: List<OrganizationEntity>
	 */
	public List<OrganizationEntity> getOrgByOrgtype(String type);
	
	/**
	 * 通过组织id 获取子节点id
	 * @Title: getAllChild
	 * @Description: TODO
	 * @param pIds 父节点IDS
	 * @param containsAll 是否包含孙子节点以及更下级节点
	 * @param isBusiness 是否为业务分层
	 * @return
	 * @return: List<String>
	 */
	List<String> getAllChild(String pIds, boolean containsAll, boolean isBusiness);

	/**
	 * 通过当前用户获取对应组织机构
	 *
	 * @return
	 */
	List<OrganizationEntity> getOrgByCurrentUserId();

	/**
	 * 通过当前用户获取对应组织机构
	 * @param accountBo
	 * @return
	 */
	List<OrganizationEntity> getOrgByCurrentUserId(final AccountBo accountBo, final int control);

	/**
	 * 根据当父id查询组织机构
	 *
	 * @param parentId 父id
	 * @param type 过滤类型
	 * @return
	 */
	List<OrganizationEntity> getOrgByParentId(String parentId, Integer type);
	/**
	 * 根据当 id查询组织机构
	 *
	 * @param    id
	 * @param type 过滤类型
	 * @return
	 */
	OrganizationEntity getOrgById(boolean isBusiness, String id, Boolean isInternal, boolean systemUserFlag);

	/**
	 * 根据组织id查询所有的父级结点
	 * @param regionId
	 * @return
	 */
    List<OrganizationEntity> getListByRegionId(String regionId);
}
