package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.bo.ContentBO;
import com.daoshu.system.bo.DutyBO;
import com.daoshu.system.entity.GeneralConcatsEntity;
import com.daoshu.system.qo.GeneralConcatsQO;

import java.util.List;

/**
 * <p>
 *  常用联系人服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface IGeneralConcatsService extends IService<GeneralConcatsEntity> {
	
	/**
	 * 查询常用联系人
	 * @param qo
	 * @return
	 */
	IPage<ContentBO> queryGeneralConcats(GeneralConcatsQO qo);
	/**
	 * 添加常用联系人
	 * @param entity
	 */
	void addGeneralConcats(GeneralConcatsEntity entity);
	/**
	 * 查询单位值守信息
	 * @param orgId
	 * @return
	 */
	DutyBO queryOrgDuty(String orgId);
	
	void batchAddGeneralConcats(List<GeneralConcatsEntity> entitys);
}
