package com.daoshu.mds.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.system.entity.CpLoginLog;

public interface LoginLogMapper extends BaseMapper<CpLoginLog> {

}
