package com.daoshu.mds.system.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.system.mapper.RoleDataMapper;
import com.daoshu.mds.system.service.IRoleDataService;
import com.daoshu.system.entity.RoleDataEntity;

@Service
public class RoleDataServiceImpl extends ServiceImpl<RoleDataMapper, RoleDataEntity> implements IRoleDataService{

}
