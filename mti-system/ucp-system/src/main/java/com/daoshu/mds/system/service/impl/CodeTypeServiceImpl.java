package com.daoshu.mds.system.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.system.mapper.CodeTypeMapper;
import com.daoshu.mds.system.service.ICodeTypeService;
import com.daoshu.system.entity.CodeTypeEntity;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Service
public class CodeTypeServiceImpl extends ServiceImpl<CodeTypeMapper, CodeTypeEntity> implements ICodeTypeService {

}
