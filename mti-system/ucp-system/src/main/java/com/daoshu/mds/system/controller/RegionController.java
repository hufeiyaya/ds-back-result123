package com.daoshu.mds.system.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.system.mapper.RegionMapper;
import com.daoshu.mds.system.service.IRegionService;
import com.daoshu.system.entity.RegionEntity;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/region")
public class RegionController {
	
	@Autowired
	private IRegionService regionService;
	
	
	@Resource
	private RegionMapper regionMapper;
	
	@GetMapping(value = "/getByPId")
	@ApiOperation(value = "通过父级id获取行政区划代码", notes = "通过父级ID获取行政区划代码")
	public ResponseInfo getByPId(@RequestParam(required = false) Integer pid) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				List<RegionEntity> result = null;
				if(null ==pid){ //查省份
					result = regionService.getList(0);
				}else{
					result = regionService.getRegionByPid(pid);
				}
				criteria.addSingleResult(result);
			}
		}.sendRequest();
	}
	
	@GetMapping(value = "/getByKeyword")
	@ApiOperation(value = "通过父级id获取行政区划代码", notes = "通过父级ID获取行政区划代码")
	public ResponseInfo getByKeyword(@RequestParam(required = false) String keyword) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				List<List<RegionEntity>> result = regionService.getRegionByKeyword(keyword);
				criteria.addSingleResult(result);
			}
		}.sendRequest();
	}
	
	@GetMapping(value = "/getRegionByCode")
	@ApiOperation(value = "通过code获取行政区划代码", notes = "通过code获取行政区划代码")
	public ResponseInfo getRegionByCode(@RequestParam(required = false) String code) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				RegionEntity result = regionService.getRegionByCode(code);
				criteria.addSingleResult(result);
			}
		}.sendRequest();
	}
	
	@GetMapping(value = "/getAllProvince")
	@ApiOperation(value = "查询所有的省份", notes = "查询所有的省份")
	public ResponseInfo getAllProvince() {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
		        List<RegionEntity> region = regionService.getAllProvince();
				criteria.addSingleResult(region);
			}
		}.sendRequest();
	}

	@GetMapping(value = "/getRegionByType")
	@ApiOperation(value = "查询省市县", notes = "查询省市县")
	public ResponseInfo getAllProvice(@RequestParam Integer level){
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				List<RegionEntity> region = regionService.getAllcity(level);
				List<String> result = region.stream().map(RegionEntity::getRegionName).collect(Collectors.toList());
				criteria.addSingleResult(result);
			}
		}.sendRequest();
	}
	
	
	@GetMapping(value = "/getRegionByName")
	@ApiOperation(value = "通过区划名称查询子区域", notes = "通过区划名称查询子区域")
	public ResponseInfo getRegionByName(@RequestParam final String region_name) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
		        List<RegionEntity> region = regionService.getRegionByName(region_name);
		        
				criteria.addSingleResult(region);
			}
		}.sendRequest();
	}

	@GetMapping(value = "/getAllRegionInfo")
	@ApiOperation(value = "获取所有的区域信息", notes = "获取所有的区域信息")
	public ResponseInfo getAllRegionInfo(){
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				List<RegionEntity> regiones = regionService.list(new QueryWrapper<RegionEntity>());
				criteria.addSingleResult(regiones);
			}
		}.sendRequest();
	}


	/*@GetMapping(value = "/getCountyByCity")
	@ApiOperation(value = "通过市名称获取所有县", notes = "通过市名称获取所有县")
	public ResponseInfo getCountyByCity(@RequestParam final String parent_code) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
		        QueryWrapper<RegionEntity> queryWrapper = new QueryWrapper<>();
		        queryWrapper.select("DISTINCT  region_name");
		        queryWrapper.eq("parent_code", parent_code);
		        List<RegionEntity> region = regionMapper.selectList(queryWrapper);
		        
				criteria.addSingleResult(region);
			}
		}.sendRequest();
	}*/
}
