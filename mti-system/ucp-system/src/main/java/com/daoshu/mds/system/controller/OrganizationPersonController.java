package com.daoshu.mds.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.daoshu.component.page.PageConstants;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.system.service.IOrganizationPersonService;
import com.daoshu.system.bo.OrganizationPersonBo;
import com.daoshu.system.entity.OrganizationPersonEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Api(value = "机构成员管理接口", description = "机构成员管理接口")
@RestController
@RequestMapping(value = "/organization/person")
public class OrganizationPersonController {

	@Autowired
	private IOrganizationPersonService organizationPersonService;
	
	@PostMapping(value = "/saveOrUpdate")
	@ApiOperation(value = "添加或者修改机构成员信息", notes = "添加或修改机构成员信息")
	public ResponseInfo saveOrUpdate(@RequestBody final OrganizationPersonBo organizationPersonBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				OrganizationPersonEntity organizationPerson = new OrganizationPersonEntity();
				BeanUtils.copyProperties(organizationPersonBo, organizationPerson);
				criteria.addSingleResult(organizationPersonService.saveOrUpdate(organizationPerson));
			}
		}.sendRequest();
	}
	@PutMapping(value = "")
	@ApiOperation(value = "修改机构成员信息", notes = "修改机构成员信息")
	public ResponseInfo upate(@RequestBody final OrganizationPersonEntity entity) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationPersonService.updateById(entity));
			}
		}.sendRequest();
	}
	@PostMapping(value = "/save")
	@ApiOperation(value = "添加机构成员信息", notes = "添加机构成员信息")
	public ResponseInfo save(@RequestBody final OrganizationPersonEntity entity) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationPersonService.save(entity));
			}
		}.sendRequest();
	}
	@PostMapping(value = "/list")
	@ApiOperation(value = "查询机构人员信息列表", notes = "查询机构人员信息列表")
	public ResponseInfo list(@RequestBody final OrganizationPersonBo organizationPersonBo) {
		// 临时处理  前端传来的分页参数为10 为了处理责任民警列表大于10的时候 后边数据无法显示
		organizationPersonBo.setSize(20);
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				IPage<OrganizationPersonEntity> page = organizationPersonService.getPage(organizationPersonBo);
				criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
				criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/querylist")
	@ApiOperation(value = "模糊查询检索条件", notes = "模糊查询检索条件")
	public ResponseInfo querylist(@RequestBody final OrganizationPersonBo personBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				List<OrganizationPersonEntity> org = organizationPersonService.list(new QueryWrapper<OrganizationPersonEntity>().lambda().eq(OrganizationPersonEntity::getOrgId, personBo.getOrgId()).like(OrganizationPersonEntity::getPersonName, personBo.getPersonName()));
				criteria.addSingleResult(org);
			}
		}.sendRequest();
	}
	
	@GetMapping(value = "/delete")
	@ApiOperation(value = "删除组织人员信息", notes = "删除组织机构人员信息")
	public ResponseInfo delete(@RequestParam final String ids) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationPersonService.removeByIds(Arrays.asList(ids.split(","))));
			}
		}.sendRequest();
	}

	@PostMapping(value = "/positionSort")
	@ApiOperation(value = "组织人员职位排序", notes = "组织人员职位排序")
	public ResponseInfo positionSort(@RequestBody final List<Map<String, Object>> sorts) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationPersonService.sortPosition(sorts));
			}
		}.sendRequest();
	}

	@PostMapping(value = "/params")
	@ApiOperation(value = "条件查询一个", notes = "条件查询一个")
	public ResponseInfo getOne(@RequestBody final OrganizationPersonEntity entity) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				OrganizationPersonEntity org = organizationPersonService.getOne(new QueryWrapper<>(entity).orderByAsc("sort_"));
				criteria.addSingleResult(org);
			}
		}.sendRequest();
	}
	@GetMapping(value = "/listByPerson")
	@ApiOperation(value = "人员查询", notes = "人员查询")
	public ResponseInfo getOne(@RequestParam final String personId) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				List<OrganizationPersonEntity> org = organizationPersonService.list(new QueryWrapper<OrganizationPersonEntity>().lambda().eq(OrganizationPersonEntity::getPersonId, personId));
				criteria.addSingleResult(org);
			}
		}.sendRequest();
	}
	@PostMapping(value = "/listByOrgIds")
	@ApiOperation(value = "条件查询一个", notes = "条件查询一个")
	public ResponseInfo listByOrgIds(@RequestBody final List<String> ids) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				List<OrganizationPersonEntity> orgs = organizationPersonService.list(new QueryWrapper<OrganizationPersonEntity>().lambda().in(OrganizationPersonEntity::getOrgId,ids));
				criteria.addSingleResult(orgs);
			}
		}.sendRequest();
	}
}
