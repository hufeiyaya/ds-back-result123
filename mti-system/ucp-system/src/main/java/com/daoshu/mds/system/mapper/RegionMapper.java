package com.daoshu.mds.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.system.entity.RegionEntity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface RegionMapper extends BaseMapper<RegionEntity> {

	  /**
     * 根据type 与key 查询字典信息
     *
     * @param type,key
     * @return
     */
//    @Select("SELECT region_name ,region_code FROM t_pf_region WHERE parent_code = (select region_code from t_pf_region t where t.region_name = #{value})")
//    @ResultMap("BaseResultMap")
     List<RegionEntity> getRegionByName(String value);

    /**
     * 通过父级Id获取自己
     * @param pid
     * @return
     */
    List<RegionEntity> getRegionByPid(@Param("pid") Integer pid);
}
