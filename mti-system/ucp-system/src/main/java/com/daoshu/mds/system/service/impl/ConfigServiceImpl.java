package com.daoshu.mds.system.service.impl;

import com.daoshu.component.exception.BusinessException;
import com.daoshu.mds.system.utils.ChineseCharToEn;
import com.daoshu.system.entity.KmZdryZrbmEntity;
import com.daoshu.system.entity.SysDictEntity;
import com.daoshu.system.entity.TKmZdryType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.system.mapper.ConfigMapper;
import com.daoshu.mds.system.service.IConfigService;
import com.daoshu.system.entity.ConfigEntity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper, ConfigEntity> implements IConfigService {
    @Autowired
    private ConfigMapper configMapper;
    @Autowired
    private IConfigService iConfigService;


    @Override
    public List<SysDictEntity> selectPersonDXL() {
        //先查询所有大类
        List<SysDictEntity> sysDictEntities = configMapper.selectPersonDXL("kp_rylb");
        //封装小类
        if(null != sysDictEntities && sysDictEntities.size() > 0){
            for(SysDictEntity sysDictEntity:sysDictEntities){
                List<SysDictEntity> sysDictEntities1 = configMapper.selectPersonDXL(sysDictEntity.getKey());
                //警种
                if(null != sysDictEntities1 && sysDictEntities1.size() > 0){
                    for(SysDictEntity sysDictEntity1 : sysDictEntities1){
                        KmZdryZrbmEntity kmZdryZrbmEntity = configMapper.selectPersonPoliceName(sysDictEntity1.getKey(),sysDictEntity1.getType());
                        if(null != kmZdryZrbmEntity){
                            sysDictEntity1.setPoliceName(kmZdryZrbmEntity.getZrbm());
                        }
                    }
                }
                sysDictEntity.setChildren(sysDictEntities1);
            }
        }
        return sysDictEntities;
    }

    @Override
    public boolean savePersonDXL(SysDictEntity sysDictEntity) {
        if(null != sysDictEntity){
            if("1".equals(sysDictEntity.getType())){
                /**
                 * 大类入库
                 */
                //根据名称自动生成对应的key
                if(null != sysDictEntity.getDlName() && sysDictEntity.getDlName() != ""){
                    ChineseCharToEn cte = new ChineseCharToEn();
                    String key ="zdry_" + cte.getAllFirstLetter(sysDictEntity.getDlName());
                    sysDictEntity.setKey(key);
                    sysDictEntity.setValue(sysDictEntity.getDlName());
                    sysDictEntity.setType("kp_rylb");
                    sysDictEntity.setCreateTime(Timestamp.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
                    sysDictEntity.setUpdateTime(Timestamp.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
                    int idNumber = (int) ((Math.random() * 9 + 1) * 100000);
                    sysDictEntity.setId(String.valueOf(idNumber));
                    sysDictEntity.setSort(idNumber);
                    int i = configMapper.selectCountKey(key);
                    if(i > 0){
                        throw new BusinessException(500,"人员类别已经存在，请重新输入！");
                    }else {
                        //入库字典表
                        configMapper.insertPersonDXL(sysDictEntity);
                    }

                }
            }else {
                /**
                 * 小类入库
                 */
                //根据名称自动生成对应的key
                if(null != sysDictEntity.getXlName() && sysDictEntity.getXlName() != ""){
                    int key = (int) ((Math.random() * 9 + 1) * 1000);
                    sysDictEntity.setKey(String.valueOf(key));
                    sysDictEntity.setValue(sysDictEntity.getXlName());
                    sysDictEntity.setType(sysDictEntity.getDlCode());
                    sysDictEntity.setCreateTime(Timestamp.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
                    sysDictEntity.setUpdateTime(Timestamp.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
                    int idNumber = (int) ((Math.random() * 9 + 1) * 100000);
                    sysDictEntity.setId(String.valueOf(idNumber));
                    sysDictEntity.setSort(idNumber);
                        //入库字典表
                        configMapper.insertPersonDXL(sysDictEntity);
                        //入库警种表
                    KmZdryZrbmEntity kmZdryZrbmEntity = new KmZdryZrbmEntity();
                    kmZdryZrbmEntity.setId(String.valueOf(idNumber));
                    kmZdryZrbmEntity.setRydl(sysDictEntity.getDlName());
                    kmZdryZrbmEntity.setZrbm(sysDictEntity.getPoliceName());
                    kmZdryZrbmEntity.setRyxl(sysDictEntity.getXlName());
                    kmZdryZrbmEntity.setRyxlCode(String.valueOf(key));
                    kmZdryZrbmEntity.setRydlCode(sysDictEntity.getDlCode());
                    configMapper.insertPersonPolice(kmZdryZrbmEntity);
                }
            }
        }
        return true;
    }

    @Override
    public boolean updatePersonDXL(SysDictEntity sysDictEntity) {
        if(null != sysDictEntity){
            if("1".equals(sysDictEntity.getType())){
                SysDictEntity sysDictEntity1 = configMapper.selectOnePersonDXL(sysDictEntity.getId());
                //大类名称（原始）
                String originalDlName = sysDictEntity1.getValue();
                String originalDlKey = sysDictEntity1.getKey();
                if(null != sysDictEntity1){
                    String dlKey = sysDictEntity1.getKey();
                    ChineseCharToEn cte = new ChineseCharToEn();
                    String key ="zdry_" + cte.getAllFirstLetter(sysDictEntity.getDlName());
                    sysDictEntity1.setKey(key);
                    sysDictEntity1.setValue(sysDictEntity.getDlName());
                    sysDictEntity1.setDelete(sysDictEntity.getDelete());
                    sysDictEntity1.setUpdateTime(Timestamp.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
                    //更新大类
                    configMapper.updatePersonDXL(sysDictEntity1);
                    //大类名称(新)
                    String newDlName = sysDictEntity.getDlName();
                    //更新重点人员对应的大小类
                    TKmZdryType tKmZdryType = new TKmZdryType();
                    tKmZdryType.setRylb(originalDlName);
                    tKmZdryType.setRylbCode(originalDlKey);
                    tKmZdryType.setRylbNew(newDlName);
                    tKmZdryType.setRylbCodeNew(key);
                    configMapper.updatePersonType(tKmZdryType);


                    //根据原始大类编码查询所对应的小类
                    List<SysDictEntity> sysDictEntities = configMapper.selectPersonDXL(dlKey);
                    if(null != sysDictEntities && sysDictEntities.size() > 0){
                        for(SysDictEntity sysDictEntity2: sysDictEntities){
                            sysDictEntity2.setType(key);
                            configMapper.updatePersonDXL(sysDictEntity2);

                            //更新警种表
                            KmZdryZrbmEntity kmZdryZrbmEntity = new KmZdryZrbmEntity();
                            kmZdryZrbmEntity.setId(sysDictEntity2.getId());
                            kmZdryZrbmEntity.setRydl(sysDictEntity.getDlName());
//                            kmZdryZrbmEntity.setZrbm(sysDictEntity.getPoliceName());
                            kmZdryZrbmEntity.setRyxl(sysDictEntity2.getValue());
                            kmZdryZrbmEntity.setRyxlCode(sysDictEntity2.getKey());
                            kmZdryZrbmEntity.setRydlCode(key);
                            configMapper.updatePersonPolice(kmZdryZrbmEntity);
                        }
                    }

                }else {
                    throw new BusinessException(500,"修改的大小类不存在！");
                }

            }else {
                SysDictEntity sysDictEntity1 = configMapper.selectOnePersonDXL(sysDictEntity.getId());
                //小类名称（原始）
                String originalXlName = sysDictEntity1.getValue();
                String originalXlKey = sysDictEntity1.getKey();
                if(null != sysDictEntity1){
                    sysDictEntity1.setValue(sysDictEntity.getXlName());
                    sysDictEntity1.setDelete(sysDictEntity.getDelete());
                    sysDictEntity1.setType(sysDictEntity.getDlCode());
                    sysDictEntity1.setUpdateTime(Timestamp.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
                    configMapper.updatePersonDXL(sysDictEntity1);

                    //大类名称(新)
                    String newXlName = sysDictEntity.getXlName();

                    //更新重点人员对应的大小类
                    TKmZdryType tKmZdryType = new TKmZdryType();
                    tKmZdryType.setXl(originalXlName);
                    tKmZdryType.setXlCode(originalXlKey);
                    tKmZdryType.setXlNew(newXlName);
                    tKmZdryType.setXlCodeNew(originalXlKey);


                    KmZdryZrbmEntity kmZdryZrbmEntity1 = configMapper.selectPersonPoliceName(sysDictEntity1.getKey(), sysDictEntity.getDlCode());
                    //更新警种表
                    KmZdryZrbmEntity kmZdryZrbmEntity = new KmZdryZrbmEntity();
                    if(null != kmZdryZrbmEntity1){
                        kmZdryZrbmEntity.setId(kmZdryZrbmEntity1.getId());
                    }else {
                        kmZdryZrbmEntity.setId(sysDictEntity.getId());
                    }
                    kmZdryZrbmEntity.setRydl(sysDictEntity.getDlName());
                    kmZdryZrbmEntity.setZrbm(sysDictEntity.getPoliceName());
                    kmZdryZrbmEntity.setRyxl(sysDictEntity.getXlName());
                    kmZdryZrbmEntity.setRyxlCode(sysDictEntity1.getKey());
                    kmZdryZrbmEntity.setRydlCode(sysDictEntity.getDlCode());
                    configMapper.updatePersonPolice(kmZdryZrbmEntity);

                    if(null != kmZdryZrbmEntity1){
                        if(!kmZdryZrbmEntity1.getZrbm().equals(sysDictEntity.getPoliceName())){
                            tKmZdryType.setZrbm(kmZdryZrbmEntity1.getZrbm());
                            tKmZdryType.setZrbmNew(sysDictEntity.getPoliceName());
                        }
                    }
                    configMapper.updatePersonType(tKmZdryType);
                }else {
                    throw new BusinessException(500,"修改的大小类不存在！");
                }
            }
        }
        return true;
    }

    @Override
    public boolean deletePersonDXL(SysDictEntity sysDictEntity) {
            if(null != sysDictEntity){
                //删除大类
                if(null != sysDictEntity.getId() && sysDictEntity.getId() != ""){
                    configMapper.deletePersonDXL(sysDictEntity.getId());
                }
                if(!"1".equals(sysDictEntity.getType())){
                    //删除警种对应的大小类
                    if(null != sysDictEntity.getId() && sysDictEntity.getId() != ""){
                        configMapper.deletePersonPolice(sysDictEntity.getId());
                    }
                }
            }
            return true;
    }

    @Override
    public boolean onOffDXL(SysDictEntity sysDictEntity) {
        if(null != sysDictEntity){
            if(null != sysDictEntity.getId() && sysDictEntity.getId() != ""){
                sysDictEntity.setUpdateTime(Timestamp.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
                configMapper.updatePersonDXL(sysDictEntity);

                SysDictEntity sysDictEntity1 = configMapper.selectOnePersonDXL(sysDictEntity.getId());
                if(null != sysDictEntity1){
                    if("kp_rylb".equals(sysDictEntity1.getType())){
                        //更新大类及所有小类
                        List<SysDictEntity> sysDictEntities1 = configMapper.selectPersonDXL(sysDictEntity1.getKey());
                        if(null != sysDictEntities1 && sysDictEntities1.size() > 0){
                            for(SysDictEntity sdn : sysDictEntities1){
                                sysDictEntity.setId(sdn.getId());
                                sysDictEntity.setUpdateTime(Timestamp.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
                                configMapper.updatePersonDXL(sysDictEntity);
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}
