package com.daoshu.mds.system.controller;

import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.system.service.IAuthLogService;
import com.daoshu.mds.system.utils.FileUtils;
import com.daoshu.system.bo.AuthLogBo;
import com.daoshu.system.entity.CpAuthLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhangmingxin
 */
@Api(value = "授权日志模块", tags = "授权日志模块")
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping(value = "/authLog")
public class AuthLogController {
    private final IAuthLogService authLogService;

    @PostMapping(value = "/list")
    @ApiOperation(value = "授权日志列表")
    public ResponseInfo list(@RequestBody  AuthLogBo authLogBo) {
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria responseCriteria, Object... objects) {
                List<CpAuthLog> cpAuthLogs = authLogService.listLogs(authLogBo);
                Long aLong = authLogService.listCount(authLogBo);
                List<Map<String, Object>> list = new ArrayList<>();
                Map<String, Object> map = new HashMap<>();
                map.put("data", cpAuthLogs);
                map.put("total", aLong);
                list.add(map);
                responseCriteria.addSingleResult(list);
            }
        }.sendRequest();
    }


    @ApiOperation(value = "授权日志为Excel")
    @PostMapping("/excel/export")
    public HttpServletResponse export(@RequestBody  AuthLogBo authLogBo, HttpServletResponse response) {
        try {

            // path是指欲下载的文件的路径。
            File file = new File(FileUtils.getLogFilePath() + "授权日志" + System.currentTimeMillis() + ".xlsx");
            // 取得文件名。
            String filename = file.getName();
            List<CpAuthLog> list = authLogService.export(file, authLogBo);
            // 以流的形式下载文件。
            InputStream fis = new BufferedInputStream(new FileInputStream(file.getAbsolutePath()));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes()));
            response.addHeader("Content-Length", "" + file.length());
            response.addHeader("Data-size", "" + list.size());
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException | InterruptedException ex) {
            ex.printStackTrace();
        }
        return response;

    }


}
