package com.daoshu.mds.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.entity.RegionEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface IRegionService extends IService<RegionEntity> {
	public List<RegionEntity> getAllProvince();
	
	
	public List<RegionEntity> getRegionByName(String name) ;

	/**
	 * 根据类别查询省市县
	 * @param level
	 * @return
	 */
	List<RegionEntity> getAllcity(Integer level);

	/**
	 * 通过父Id获取自己
	 * @param pid
	 * @return
	 */
	List<RegionEntity> getRegionByPid(Integer pid);

	/**
	 * 模糊查询所有的省市县数据
	 * @param keyword
	 * @return
	 */
	List<List<RegionEntity>> getRegionByKeyword(String keyword);
	
	/**
	 * 通过code查询region
	 * @param code
	 * @return
	 */
	RegionEntity getRegionByCode(String code);

	/**
	 * 查省份
	 * @return
	 */
	List<RegionEntity> getList(Integer all);
}
