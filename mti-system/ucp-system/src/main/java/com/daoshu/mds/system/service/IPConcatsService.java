package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.bo.ConcatsBO;
import com.daoshu.system.bo.ContentBO;
import com.daoshu.system.entity.PConcatsEntity;
import com.daoshu.system.qo.PersonConcatsQO;

import java.util.List;

/**
 * <p>
 *  人员通讯录服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface IPConcatsService extends IService<PConcatsEntity> {
	
	/**
	 * 新增
	 *
	 * @param entity
	 * @return
	 */
	boolean insert(PConcatsEntity entity);
	
	/**
	 * 修改
	 *
	 * @param entity
	 * @return
	 */
	boolean update(PConcatsEntity entity);

	/**
	 * 设置默认通讯方式
	 *
	 * @param id
	 */
	void defaultConcat(String id, String personId);

	/**
	 * 获取人员所有联系方式
	 *
	 * @param personId
	 * @return
	 */
	List<PConcatsEntity> getConcat(String personId);

	/**
	 * 根据联系方式与联系方式内容获取通讯录信息
	 *
	 * @param content
	 * @param type
	 * @return
	 */
	PConcatsEntity getByContentAndType(String content, Integer type);

	/**
	 * 分页查询人员联系方式
	 *
	 * @param qo
	 * @return
	 */
	IPage<ContentBO> queryPersonConcat(PersonConcatsQO qo);

	/**
	 * 分页查询人员联系方式
	 *
	 * @param personIds
	 * @return
	 */
	List<ContentBO> queryPersonConcat(List<String> personIds);

	/**
	 * 根据联系方式查询联系人
	 *
	 * @param content
	 * @return
	 */
	ConcatsBO getConcats(String content);

	/**
	 * 根据联系方式查询联系人
	 *
	 * @param content
	 * @return
	 */
	ConcatsBO getByObjIdAndContent(String content, String objId);
}
