package com.daoshu.mds.system.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daoshu.component.page.PageConstants;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.component.util.MD5Util;
import com.daoshu.mds.system.component.annotations.AuthAnnotation;
import com.daoshu.mds.system.service.IAccountRoleService;
import com.daoshu.mds.system.service.IAccountService;
import com.daoshu.system.bo.AccountBo;
import com.daoshu.system.entity.AccountEntity;
import com.daoshu.system.entity.AccountRoleEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(value = "账户管理接口", description = "账户管理接口")
@RestController
@RequestMapping(value = "/account")
public class AccountController {

	@Autowired
	private IAccountService accountService;

	@Autowired
	private IAccountRoleService accountRoleService;

	@Value("${Init.Password}")
	private String initPassword;
	
	@GetMapping(value = "/getById")
	@ApiOperation(value = "通过id获取账户信息", notes = "通过id获取账户信息")
	public ResponseInfo getAccount(@RequestParam(value = "id", required = true) final String id) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(accountService.getById(id));
			}
		}.sendRequest();
	}

	@PostMapping(value = "/saveOrUpdate")
	@ApiOperation(value = "修改账户信息", notes = "修改账户信息")
	public ResponseInfo saveOrUpdateAccount(@RequestBody final AccountBo accountBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				accountBo.setPassword(MD5Util.MD5(StringUtils.isNotBlank(accountBo.getPassword()) ? accountBo.getPassword() : initPassword));
				criteria.addSingleResult(accountService.saveOrUpdateBo(accountBo));
			}
		}.sendRequest();
	}
	@PostMapping(value = "/save")
	@ApiOperation(value = "修改账户信息", notes = "修改账户信息")
	public ResponseInfo save(@RequestBody final AccountEntity entity) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(accountService.save(entity));
			}
		}.sendRequest();
	}
	@GetMapping(value = "/resetPassword")
	@ApiOperation(value = "重置密码", notes = "重置密码")
	public ResponseInfo resetPassword(@RequestParam final String id) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				AccountEntity entity = new AccountEntity();
				entity.setPassword(MD5Util.MD5(initPassword));
				Wrapper<AccountEntity> updateWrapper = new UpdateWrapper<AccountEntity>().eq("id_", id);
				criteria.addSingleResult(accountService.update(entity, updateWrapper));
			}
		}.sendRequest();
	}

	@PostMapping(value = "/updatePassword")
	@ApiOperation(value = "更新密码", notes = "更新密码")
	public ResponseInfo updatePassword(@RequestBody final AccountBo accountBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(accountService.updatePassword(accountBo));
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/list")
	@ApiOperation(value = "获取账户列表信息", notes = "获取账户列表信息")
	public ResponseInfo listAccount(@RequestBody final AccountBo accountBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				Wrapper<AccountEntity> queryWrapper = new QueryWrapper<AccountEntity>();
				IPage<AccountEntity> page = accountService.page(new Page<>(accountBo.getStart(), accountBo.getSize()), queryWrapper);
				criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
				criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
			}
		}.sendRequest();
	}

	@PostMapping(value = "/setAccountRole")
	@ApiOperation(value = "设置账户角色", notes = "设置账户角色")
	@AuthAnnotation(opVal = "setAccountRole")
	public ResponseInfo setAccountRole(@RequestBody final List<AccountRoleEntity> accountRoleEntitys) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				if (CollectionUtils.isNotEmpty(accountRoleEntitys)) {
					String accountId = accountRoleEntitys.get(0).getAccountId();
					accountRoleService.remove(new QueryWrapper<AccountRoleEntity>().lambda()
							.eq(AccountRoleEntity::getAccountId, accountId));
					accountRoleService.saveBatch(accountRoleEntitys);
				}
			}
		}.sendRequest();
	}

	@GetMapping(value = "/getAccountRoleIds")
	@ApiOperation(value = "通过账户id 获取账户的角色ids", notes = "通过账户的id 获取账户的角色ids")
	public ResponseInfo getAccountRoleIds (@RequestParam final String accountId) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria  criteria, Object... obj) {
				List<AccountRoleEntity> list = accountRoleService.list(new QueryWrapper<AccountRoleEntity>()
				.lambda().eq(AccountRoleEntity::getAccountId, accountId));
				if (CollectionUtils.isNotEmpty(list)) {
					List<String> result = new ArrayList<>();
					list.stream().forEach(item -> result.add(item.getRoleId()));
					criteria.addSingleResult(result);
				}
			}
		}.sendRequest();
	}

}
