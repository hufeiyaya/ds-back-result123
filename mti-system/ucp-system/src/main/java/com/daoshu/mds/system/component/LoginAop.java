package com.daoshu.mds.system.component;

import com.daoshu.component.exception.BusinessException;
import com.daoshu.component.snowflake.SnowFlake;
import com.daoshu.mds.system.service.ILoginLogService;
import com.daoshu.mds.system.utils.RequestKit;
import com.daoshu.system.bo.AccountBo;
import com.daoshu.system.entity.CpLoginLog;
import com.daoshu.system.utils.RequestUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * @author zhangmingxin
 */
@Slf4j
@Aspect
@Component
@AllArgsConstructor
public class LoginAop {
    private final ILoginLogService loginLogService;
    private final SnowFlake snowFlake = new SnowFlake();
    private final RequestUtils requestUtils;
    private final RequestKit kit;

    @Pointcut(value = "execution(* com.daoshu.mds.system.service.impl.AccountServiceImpl.log*(..))")
    public void pointCut(){
    }

    @Around("pointCut()")
    public Object around(ProceedingJoinPoint joinPoint){
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();
        CpLoginLog loginLog = new CpLoginLog();
        loginLog.setId(snowFlake.nextId());
        String ip = kit.getIp(request);
        loginLog.setIp(ip);
        loginLog.setLoginType(request.getRequestURI().endsWith("login")?"登录":"登出");
        loginLog.setCreateTime(LocalDateTime.now());
        AccountBo accountBo = null;
        boolean isLogin = request.getRequestURI().endsWith("login");
        boolean success = true;
        try {
            if(request.getRequestURI().endsWith("login")){
                accountBo = ((AccountBo) joinPoint.proceed());
            }else{
                accountBo = requestUtils.getCurrentUser();
                success = (boolean) joinPoint.proceed();
            }
        } catch (Throwable throwable) {
            log.error("error==>{}", throwable.getMessage());
            if("用户名和密码不匹配".equals(throwable.getMessage())){
                throw new BusinessException(500, "用户名和密码不匹配");
            }

        }
        if(accountBo!=null){
            loginLog.setName(accountBo.getPersonBo()==null?"":accountBo.getPersonBo().getName());
            loginLog.setNumber(accountBo.getPersonBo()==null?"":accountBo.getPersonBo().getNumber());
            loginLog.setOrg(accountBo.getOrganizationBo()==null?"":accountBo.getOrganizationBo().getName());
            loginLog.setOrgId(accountBo.getOrganizationBo()==null?"":accountBo.getOrganizationBo().getId());
        }
        loginLogService.save(loginLog);
        return isLogin?accountBo:success;
    }
}
