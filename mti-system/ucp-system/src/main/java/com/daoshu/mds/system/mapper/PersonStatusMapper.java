package com.daoshu.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.system.entity.PersonStatusEntity;

public interface PersonStatusMapper extends BaseMapper<PersonStatusEntity> {
}
