package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.entity.CpLoginLog;

public interface ILoginLogService extends IService<CpLoginLog> {
}
