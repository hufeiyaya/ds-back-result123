package com.daoshu.mds.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.daoshu.component.page.PageConstants;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.system.service.IOrganizationPersonService;
import com.daoshu.mds.system.service.IPersonService;
import com.daoshu.mds.system.service.IPersonStatuService;
import com.daoshu.system.bo.PersonBo;
import com.daoshu.system.bo.PersonContentBO;
import com.daoshu.system.entity.PersonEntity;
import com.daoshu.system.entity.PersonStatusEntity;
import com.daoshu.system.qo.PersonQO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Api(value = "人员信息管理接口", description = "人员信息管理接口")
@RestController
@RequestMapping(value = "/person")
public class PersonController {

	@Autowired
	private IPersonService personService;
	@Autowired
	private IPersonStatuService personStatuService;
	@SuppressWarnings("unused")
	@Autowired
	private IOrganizationPersonService organizationPersonService;
	
	@PostMapping(value = "/saveOrUpdate")
	@ApiOperation(value = "修改人员信息", notes = "修改人员信息")
	public ResponseInfo saveOrUpdate(@RequestBody final PersonBo personBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				PersonEntity person = new PersonEntity();
				BeanUtils.copyProperties(personBo, person);
				criteria.addSingleResult(personService.saveOrUpdate(person));
			}
		}.sendRequest();
	}
	@PostMapping(value = "/save")
	@ApiOperation(value = "添加人员信息", notes = "添加人员信息")
	public ResponseInfo save(@RequestBody final PersonEntity entity) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(personService.save(entity));
			}
		}.sendRequest();
	}
	@PostMapping(value = "/detaillPage")
	@ApiOperation(value = "人员详情查询", notes = "人员详情查询")
	public ResponseInfo queryPersonPage(@RequestBody final PersonQO qo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				IPage<PersonContentBO> page = personService.queryPersonPage(qo);
				criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
				criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
			}
		}.sendRequest();
	}
	@PostMapping(value = "/page")
	@ApiOperation(value = "查询人员信息列表", notes = "查询人员信息列表")
	public ResponseInfo list(@RequestBody final PersonBo personBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(personService.getPage(personBo));
			}
		}.sendRequest();
	}
	
	@GetMapping(value = "/delete")
	@ApiOperation(value = "删除人员信息", notes = "删除人员信息")
	public ResponseInfo delete(@RequestParam final String ids) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(personService.removeByIds(Arrays.asList(ids.split(","))));
			}
		}.sendRequest();
	}
	@GetMapping(value = "/{id}")
	@ApiOperation(value = "获取人员信息", notes = "获取人员信息")
	public ResponseInfo getById(@PathVariable final String id) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(personService.getById(id));
			}
		}.sendRequest();
	}
	@PostMapping(value = "/list")
	@ApiOperation(value = "获取人员信息", notes = "获取人员信息")
	public ResponseInfo getById(@RequestBody final List<String> ids) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				if(ids.isEmpty()) {
					criteria.addSingleResult(new ArrayList<PersonEntity>());
				}else {
					criteria.addSingleResult(personService.listByIds(ids));
				}
			}
		}.sendRequest();
	}
	@PostMapping(value = "/lists")
	@ApiOperation(value = "获取所有人员信息", notes = "获取所有人员信息")
	public ResponseInfo queryAll() {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(personService.list(new QueryWrapper<PersonEntity>()));
			}
		}.sendRequest();
	}
	@PostMapping(value = "/status")
	@ApiOperation(value = "获取人员设备类型信息", notes = "获取人员设备类型信息")
	public ResponseInfo getPersonStatus(@RequestBody(required = false) final List<String> ids) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				if(ids.isEmpty()) {
					criteria.addSingleResult(criteria.addSingleResult(new ArrayList<>()));
					return;
				}
				List<PersonStatusEntity> list = personStatuService.list(new QueryWrapper<PersonStatusEntity>().lambda().in(PersonStatusEntity::getPersonId, ids));
				criteria.addSingleResult(list);
			}
		}.sendRequest();
	}

	/**
	 * 根据组织机构查询人员列表
	 *
	 * @param orgId 组织机构编号
	 * @return
	 */
	@GetMapping(value = "/getPersonsByOrgId")
	@ApiOperation(value = "根据组织机构查询人员列表", notes = "根据组织机构查询人员列表")
	@ApiImplicitParam(name = "orgId", value = "组织机构编号", required = true, dataType = "String", paramType = "query")
	public ResponseInfo getPersonsByOrgId(@RequestParam(required = true) String orgId) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria responseCriteria, Object... objects) {
				List<PersonEntity> personEntityList = personService.getPersonsByOrgId(orgId);
				responseCriteria.addSingleResult(personEntityList);
			}
		}.sendRequest();
	}

    /**
     * 根据人员信息对象查询对应人员
     *
     * @param personEntity 人员信息对象
     * @return
     */
    @GetMapping(value = "/getByPerson")
    @ApiOperation(value = "根据人员信息对象查询对应人员", notes = "根据人员信息对象查询对应人员")
    @ApiImplicitParam(name = "personEntity", value = "人员信息对象", required = false, dataType = "PersonEntity", paramType = "query")
    public ResponseInfo getByPerson(PersonEntity personEntity) {
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria responseCriteria, Object... objects) {
                QueryWrapper<PersonEntity> queryWrapper = new QueryWrapper<PersonEntity>();
                queryWrapper.setEntity(personEntity);
                PersonEntity personEntityResult = personService.getOne(queryWrapper);
                responseCriteria.addSingleResult(personEntityResult);
            }
        }.sendRequest();
    }

}
