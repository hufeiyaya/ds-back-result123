package com.daoshu.mds.system.controller;

import java.util.Arrays;

import com.daoshu.system.entity.SysDictEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.system.service.IConfigService;
import com.daoshu.system.bo.ConfigBo;
import com.daoshu.system.entity.ConfigEntity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "config")
@Api(value = "系统配置控制器", description = "系统配置控制实体 ")
public class ConfigController {

	@Autowired
	private IConfigService configService;
	
	@PostMapping(value = "/list")
	@ApiOperation(value = "查询配置信息列表", notes = "查询配置信息列表")
	public ResponseInfo list(@RequestBody final ConfigBo configBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(configService.page(new Page<>(configBo.getStart(), configBo.getSize()), new QueryWrapper<ConfigEntity>()));
			}
		}.sendRequest();
	}
	
	@DeleteMapping(value = "/delete")
	@ApiOperation(value = "删除配置信息", notes = "删除配置信息")
	public ResponseInfo delete(@RequestParam final String ids) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(configService.removeByIds(Arrays.asList(ids.split(","))));
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/saveOrUpdate")
	@ApiOperation(value = "新增或修改配置信息", notes = "新增或修改配置信息")
	public ResponseInfo saveOrUpdate(@RequestBody final ConfigBo configBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				ConfigEntity entity = new ConfigEntity();
				BeanUtils.copyProperties(configBo, entity);
				criteria.addSingleResult(configService.saveOrUpdate(entity));
			}
		}.sendRequest();
	}

	@PostMapping(value = "/selectPersonDXL")
	@ApiOperation(value = "大小类列表查询", notes = "大小类列表查询")
	public ResponseInfo selectPersonDXL() {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(configService.selectPersonDXL());
			}
		}.sendRequest();
	}
	@PostMapping(value = "/savePersonDXL")
	@ApiOperation(value = "新增大小类", notes = "新增大小类")
	public ResponseInfo savePersonDXL(@RequestBody SysDictEntity sysDictEntity) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(configService.savePersonDXL(sysDictEntity));
			}
		}.sendRequest();
	}

	@PostMapping(value = "/updatePersonDXL")
	@ApiOperation(value = "修改大小类", notes = "修改大小类")
	public ResponseInfo updatePersonDXL(@RequestBody SysDictEntity sysDictEntity) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(configService.updatePersonDXL(sysDictEntity));
			}
		}.sendRequest();
	}

	@PostMapping(value = "/deletePersonDXL")
	@ApiOperation(value = "删除大小类", notes = "删除大小类")
	public ResponseInfo deletePersonDXL(@RequestBody SysDictEntity sysDictEntity) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(configService.deletePersonDXL(sysDictEntity));
			}
		}.sendRequest();
	}

	@PostMapping(value = "/onOffDXL")
	@ApiOperation(value = "设置显隐", notes = "设置显隐")
	public ResponseInfo onOffDXL(@RequestBody SysDictEntity sysDictEntity) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(configService.onOffDXL(sysDictEntity));
			}
		}.sendRequest();
	}


}
