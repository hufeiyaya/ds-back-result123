package com.daoshu.mds.system.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.system.mapper.BaseCodeMapper;
import com.daoshu.mds.system.service.IBaseCodeService;
import com.daoshu.system.entity.BaseCodeEntity;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Service
public class BaseCodeServiceImpl extends ServiceImpl<BaseCodeMapper, BaseCodeEntity> implements IBaseCodeService {

}
