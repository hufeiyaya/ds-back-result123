package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.bo.OrganizationPersonBo;
import com.daoshu.system.entity.OrganizationPersonEntity;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface IOrganizationPersonService extends IService<OrganizationPersonEntity> {
	
	/**
	 * 分页查询组织机构人员信息
	 * @Title: getPage
	 * @Description: TODO
	 * @param organizationPersonBo
	 * @return
	 * @return: IPage<OrganizationEntity>
	 */
	IPage<OrganizationPersonEntity> getPage(OrganizationPersonBo organizationPersonBo);

	boolean sortPosition(List<Map<String, Object>> sorts);
}
