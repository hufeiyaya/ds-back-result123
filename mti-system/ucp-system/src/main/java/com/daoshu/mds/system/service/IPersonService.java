package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.bo.PersonBo;
import com.daoshu.system.bo.PersonContentBO;
import com.daoshu.system.entity.PersonEntity;
import com.daoshu.system.qo.PersonQO;

import java.util.List;

/**
 * <p>
 *  人员信息 服务类
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-20
 */
public interface IPersonService extends IService<PersonEntity> {

	/**
	 * 分页查询人员信息列表
	 *
	 * @param personBo
	 * @return
	 */
	IPage<PersonEntity> getPage(PersonBo personBo);

	/**
	 * 人员详情查询
	 *
	 * @param qo
	 * @return
	 */
	IPage<PersonContentBO> queryPersonPage(PersonQO qo);

	/**
	 * 人员详情查询
	 *
	 * @param personIds
	 * @return
	 */
	List<PersonContentBO> queryPersonList(List<String> personIds);

	/**
	 * 根据账号id查询人员信息
	 *
	 * @param id
	 * @return
	 */
	PersonEntity getByAccountId(String id);

	/**
	 * 根据组织机构查询人员列表
	 *
	 * @param orgId 组织机构编号
	 * @return
	 */
	List<PersonEntity> getPersonsByOrgId(String orgId);
}
