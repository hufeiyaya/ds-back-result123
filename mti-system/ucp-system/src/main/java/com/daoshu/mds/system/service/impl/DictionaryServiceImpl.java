package com.daoshu.mds.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.system.mapper.DictionaryMapper;
import com.daoshu.mds.system.service.IDictionaryService;
import com.daoshu.system.entity.DictionaryEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: mds.pom
 * @description: 字典信息接口实现类
 * @author: Williams Guo
 * @create: 2018-12-12 11:55
 */
@Service
public class DictionaryServiceImpl extends ServiceImpl<DictionaryMapper, DictionaryEntity> implements IDictionaryService {
    @SuppressWarnings("rawtypes")
	private static final Map<String, Map> types = new HashMap<>();


    @SuppressWarnings("unchecked")
	@Override
    public DictionaryEntity getCacheDict(String key, String type) {
        List<DictionaryEntity> list = null;
        Map<String, DictionaryEntity> tMap = types.get(type);
        DictionaryEntity dictionaryEntity = null;
        if(null == tMap) {
            list = list(new QueryWrapper<DictionaryEntity>().eq("type_", type));
            tMap = new HashMap<>();
            for(DictionaryEntity dict : list) {
                tMap.put(dict.getKey(), dict);
                if (dict.getKey().equals(key)) {
                    dictionaryEntity = dict;
                }
            }
            types.put(type, tMap);
            return dictionaryEntity;
        } else {
            return tMap.get(key);
        }
    }
}