package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.bo.AccountBo;
import com.daoshu.system.entity.AccountEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface IAccountService extends IService<AccountEntity> {
	
	/**
	 * 账户登陆
	 * @Title: login
	 * @Description: TODO
	 * @param accountBo
	 * @return
	 * @return: AccountBo
	 */
	 AccountBo login(AccountBo accountBo);

	/**
	 * 根据personId获取用户
	 * @param personId
	 * @return
	 */
	AccountBo getByPersonId(String personId);

	/**
	 * 账户退出登陆
	 * @Title: logout
	 * @Description: TODO
	 * @return: void
	 */
	boolean logout();

	/**
	 * 账户退出登陆
	 * @Title: logout
	 * @Description: TODO
	 * @return: void
	 */
	boolean logout(AccountBo accountBo);

	/**
	 * 创建或者修改账户信息
	 * @Title: saveOrUpdateBo
	 * @Description: TODO
	 * @param accountBo
	 * @return
	 * @return: Object
	 */
	boolean saveOrUpdateBo(AccountBo accountBo);


	AccountBo getAccount(AccountBo accountBo);


	/**
	 *@Description: 根据用户id获取账户信息，含Sip信息
	 *@Param:
	 *@return:
	 *@Author: Williams Guo
	 *@date: 2018/11/12
	 */
	AccountBo getPersonByPersonId(String personId);


	/**
	 *@Description: 根据Sip账号获取人员账号信息
	 *@Param:
	 *@return:
	 *@Author: Williams Guo
	 *@date: 2018/11/12
	 */
	List<AccountBo> getPersonsByAccount(String[] accounts);

	/**
	 *@Description: 根据Sip账号获取人员账号信息
	 *@Param:
	 *@return:
	 *@Author: Williams Guo
	 *@date: 2018/11/12
	 */
	AccountBo getPersonsByAccount(String account);

	Boolean updatePassword(final AccountBo accountBo);

}
