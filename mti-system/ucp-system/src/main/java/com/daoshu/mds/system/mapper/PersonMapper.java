package com.daoshu.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daoshu.system.bo.PersonBo;
import com.daoshu.system.bo.PersonContentBO;
import com.daoshu.system.entity.PersonEntity;
import com.daoshu.system.qo.PersonQO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface PersonMapper extends BaseMapper<PersonEntity> {

	/**
	 * 人员查询
	 * @param qo
	 * @return
	 */
	IPage<PersonContentBO> queryPerson(Page<?> page, @Param("qo") PersonQO qo);

	/**
	 * 根据组织机构编号查询人员列表
	 *
	 * @param orgId 组织机构编号
	 * @return
	 */
	@Select("select tpp.* from t_pf_organization_person tfop, t_pf_person tpp where tfop.person_id_ = tpp.id_ and tfop.delete_ = 0 and tpp.delete_ = 0 and tfop.org_id_ = #{orgid}")
	List<PersonEntity> getPersonsByOrgId(String orgId);

	List<PersonEntity> pagePerson(@Param(value = "person")PersonBo person,@Param(value = "page")Integer page,@Param(value = "size")Integer size);
	long countPagePerson(@Param(value = "person")PersonBo person);
}
