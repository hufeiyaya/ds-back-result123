package com.daoshu.mds.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.component.exception.BusinessException;
import com.daoshu.mds.constants.ConcatType;
import com.daoshu.mds.constants.LinkType;
import com.daoshu.mds.system.mapper.OConcatsMapper;
import com.daoshu.mds.system.service.IGeneralConcatsService;
import com.daoshu.mds.system.service.IOConcatsService;
import com.daoshu.system.bo.AccountBo;
import com.daoshu.system.bo.ContentBO;
import com.daoshu.system.entity.GeneralConcatsEntity;
import com.daoshu.system.entity.OConcatsEntity;
import com.daoshu.system.entity.OrganizationEntity;
import com.daoshu.system.service.OrgFeignService;
import com.daoshu.system.utils.ListUtils;
import com.daoshu.system.utils.RequestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 *  组织通讯录服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Service
public class OConcatsServiceImpl extends ServiceImpl<OConcatsMapper, OConcatsEntity> implements IOConcatsService {
	@Autowired private OrgFeignService orgService;
	@Autowired private IGeneralConcatsService generalConcatsService;
	@Autowired private RequestUtils userUtil;
	@Override
	public boolean insert(OConcatsEntity entity) {
		return this.save(entity);
	}

	@Override
	public boolean update(OConcatsEntity entity) {
		return this.updateById(entity);
	}

	
	@Override
	public List<OConcatsEntity> getConcat(String orgId) {
		if(StringUtils.isBlank(orgId)) {
			throw new BusinessException(500, "联系人不能为空");
		}
		return this.list(new QueryWrapper<OConcatsEntity>().eq("organization_id_", orgId).eq("available_", 1));
	}
	@Override
	public OConcatsEntity getByContentAndType(String content, Integer type) {
		return this.getOne(new QueryWrapper<OConcatsEntity>().eq("concat_number_", content).eq("concat_type_", type).eq("available_", 1));
	}

	@Override
	public List<ContentBO> queryOrgConcat(List<String> orgIds) {
		List<ContentBO> orgContents = new ArrayList<>();
		List<String> orgIdsTemp = ListUtils.removeNullElement(orgIds);
		if(!orgIdsTemp.isEmpty()) {
			Collection<OrganizationEntity> orgs = Optional.ofNullable(orgService.listByIds(orgIdsTemp)).orElse(new ArrayList<>());
			for(OrganizationEntity o: orgs) {
				ContentBO bo = new ContentBO();
				bo.setOrgId(o.getId());
				bo.setOrgName(o.getName());
				orgContents.add(bo);
			}
			Collection<OConcatsEntity> oConcats = Optional.ofNullable(this.list(new QueryWrapper<OConcatsEntity>().in("organization_id_", orgIds).eq("delete_", 0).orderByAsc("sort_"))).orElse(new ArrayList<>());
			// 设置是否常用联系人
			AccountBo user = userUtil.getCurrentUser();
			List<GeneralConcatsEntity> generalConcat = Optional.ofNullable(generalConcatsService.list(new QueryWrapper<GeneralConcatsEntity>().eq("person_id_", user.getPersonBo().getId()).in("link_person_id_", orgIdsTemp))).orElse(new ArrayList<>());
			orgContents.forEach(bo->{
				bo.setIsGeneralconcat(false);
				generalConcat.forEach(general->{
					if(general.getLinkPersonId()!=null&&general.getLinkPersonId().equals(bo.getId())) {
						bo.setGeneralId(general.getId());
						bo.setIsGeneralconcat(true);
					}
				});
			});
			for(ContentBO org: orgContents) {
				List<OConcatsEntity> temp = new ArrayList<>();
				
				List<OConcatsEntity> colonyCall = new ArrayList<>();
				List<OConcatsEntity> phone = new ArrayList<>();
				List<OConcatsEntity> restaCall = new ArrayList<>();
				List<OConcatsEntity> specicalCall = new ArrayList<>();
				List<OConcatsEntity> steatsCall = new ArrayList<>();
				for(OConcatsEntity concat: oConcats) {
					if(concat.getOrganizationId()!=null && concat.getOrganizationId().equals(org.getOrgId())) {
						temp.add(concat);
						if(concat.getConcatType()!=null&&concat.getConcatType().equals(ConcatType.COLONY_CALL.getStatus())) {
							colonyCall.add(concat);
						}else if(concat.getConcatType()!=null&&concat.getConcatType().equals(ConcatType.PHONE.getStatus())) {
							phone.add(concat);
						}else if(concat.getConcatType()!=null&&concat.getConcatType().equals(ConcatType.RESTA_CALL.getStatus())) {
							restaCall.add(concat);
						}else if(concat.getConcatType()!=null&&concat.getConcatType().equals(ConcatType.SPECICAL_CALL.getStatus())) {
							specicalCall.add(concat);
						}else if(concat.getConcatType()!=null&&concat.getConcatType().equals(ConcatType.STEATS_CALL.getStatus())) {
							steatsCall.add(concat);
						}
						if(concat.getDefaultValue()!=null&&concat.getDefaultValue().equals(1)) {
							org.setDefaultConcat(concat.getConcatNumber());
							org.setDefaultConcatType(concat.getConcatType());
						}
					}
				}
				Map<String,List<OConcatsEntity>> map = new HashMap<>();
				map.put("colonyCall", colonyCall.isEmpty()?null:colonyCall);
				map.put("phone", phone.isEmpty()?null:phone);
				map.put("restaCall", restaCall.isEmpty()?null:restaCall);
				map.put("specicalCall", specicalCall.isEmpty()?null:specicalCall);
				map.put("steatsCall", steatsCall.isEmpty()?null:steatsCall);
				org.setOrgConcats(temp);
				org.setConcats(map);
				org.setType(LinkType.ORG.getStatus());
				org.setTypeName(LinkType.ORG.getTitle());
			}
		}
		return orgContents;
	}
}
