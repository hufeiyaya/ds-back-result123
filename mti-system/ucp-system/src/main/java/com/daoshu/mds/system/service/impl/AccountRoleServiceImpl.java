package com.daoshu.mds.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.system.component.annotations.AuthAnnotation;
import com.daoshu.mds.system.mapper.AccountRoleMapper;
import com.daoshu.mds.system.service.IAccountRoleService;
import com.daoshu.mds.system.service.IRoleService;
import com.daoshu.system.entity.AccountRoleEntity;
import com.daoshu.system.entity.RoleEntity;
import io.jsonwebtoken.lang.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Service
public class AccountRoleServiceImpl extends ServiceImpl<AccountRoleMapper, AccountRoleEntity> implements IAccountRoleService {

	@Autowired
	private IRoleService roleService;

	@Override
	public Set<String> getMenuIdsByAccountId(String accountId) {
		Set<String> menuIds = new HashSet<String>();
		Set<String> roleIds = new HashSet<String>();
		AccountRoleEntity condition = new AccountRoleEntity();
		condition.setAccountId(accountId);
		Wrapper<AccountRoleEntity> queryWrapper = new QueryWrapper<AccountRoleEntity>(condition);
		List<AccountRoleEntity> entitys = this.list(queryWrapper);
		if (!Collections.isEmpty(entitys)) {
			for(AccountRoleEntity entity : entitys) {
				roleIds.add(entity.getRoleId());
			}
		}
		if (!Collections.isEmpty(roleIds)) {
			Collection<RoleEntity> roles = roleService.listByIds(roleIds);
			if (!Collections.isEmpty(roles)) {
				for (RoleEntity role : roles) {
					menuIds.addAll(Arrays.asList(role.getMenuIds().split(",")));
				}
			}
		}
		return menuIds;
	}

    @Override
	@Transactional
    public void deletePersonRole(String accountId) {
        this.baseMapper.deletePersonRole(accountId);
    }

}
