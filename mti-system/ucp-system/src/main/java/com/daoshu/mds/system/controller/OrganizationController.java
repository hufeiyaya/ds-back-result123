package com.daoshu.mds.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.daoshu.component.exception.BusinessException;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.system.service.IOrganizationService;
import com.daoshu.system.bo.AccountBo;
import com.daoshu.system.bo.OrganizationBo;
import com.daoshu.system.entity.OrganizationEntity;
import com.daoshu.system.utils.RequestUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@Api(value = "组织机构管理接口", description = "组织机构管理接口")
@RestController
@RequestMapping(value = "/organization")
public class OrganizationController {

	@Autowired
	private IOrganizationService organizationService;
	
	@GetMapping(value = "/getDetailByPId")
	@ApiOperation(value = "通过父ID查询组织详情")
	public ResponseInfo getDetailByPId(
			@RequestParam(value = "isBusiness", defaultValue = "false") final boolean isBusiness, 
			@RequestParam(value = "pId", defaultValue = "0") final String pId, 
			@RequestParam(value = "isInternal", required = false) final Boolean isInternal,
			@RequestParam(value = "systemFlag", required = false,defaultValue="false") final boolean systemFlag) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.getDetailByPId(isBusiness, pId, isInternal,systemFlag));
			}
		}.sendRequest();
	}
	
	@GetMapping(value = "/getOrgByPId")
	@ApiOperation(value = "通过父ID查询派出所详情")
	public ResponseInfo getOrgByPId(
			@RequestParam(value = "orgType", defaultValue = "3") final String orgType, 
			@RequestParam(value = "pId", defaultValue = "0") final String pId, 
			@RequestParam(value = "keyword",required = false) final String keyword) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.getOrgByPId(orgType, pId, keyword));
			}
		}.sendRequest();
	}
	
	@GetMapping(value = "/getOrgById")
	@ApiOperation(value = "通过父ID查询派出所详情")
	public ResponseInfo getOrgById(
			@RequestParam(value = "isBusiness", defaultValue = "false") final boolean isBusiness, 
			@RequestParam(value = "id", defaultValue = "0") final String id, 
			@RequestParam(value = "isInternal", required = false) final Boolean isInternal,
			@RequestParam(value = "systemFlag", required = false,defaultValue="false") final boolean systemFlag) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.getOrgById(isBusiness, id, isInternal,systemFlag));
			}
		}.sendRequest();
	}
	
	@GetMapping(value = "/getOrgByOrgtype")
	@ApiOperation(value = "通过Orgtype查询分局")
	public ResponseInfo getOrgByOrgtype( 
			@RequestParam(value = "orgType", required = false,defaultValue="2") final String orgType) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.getOrgByOrgtype(orgType));
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/saveOrUpdate")
	@ApiOperation(value = "修改组织机构信息", notes = "修改组织机构信息")
	public ResponseInfo saveOrUpdate(@RequestBody final OrganizationBo organizationBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				OrganizationEntity organization = new OrganizationEntity();
				BeanUtils.copyProperties(organizationBo, organization);
				criteria.addSingleResult(organizationService.saveOrUpdate(organization));
			}
		}.sendRequest();
	}
	@PostMapping(value = "/save")
	@ApiOperation(value = "修改组织机构信息", notes = "修改组织机构信息")
	public ResponseInfo save(@RequestBody final OrganizationEntity entity) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.save(entity));
			}
		}.sendRequest();
	}
	@PutMapping(value = "/update")
	@ApiOperation(value = "修改组织机构信息", notes = "修改组织机构信息")
	public ResponseInfo update(@RequestBody final OrganizationEntity entity) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.updateById(entity));
			}
		}.sendRequest();
	}
	@PostMapping(value = "/list")
	@ApiOperation(value = "查询组织机构信息列表", notes = "查询组织机构信息列表")
	public ResponseInfo list(@RequestBody final OrganizationBo organizationBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.getPage(organizationBo));
			}
		}.sendRequest();
	}
	@PostMapping(value = "/listByIds")
	@ApiOperation(value = "查询组织机构信息列表", notes = "查询组织机构信息列表")
	public ResponseInfo listByIds(@RequestBody final List<String> ids) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.listByIds(ids));
			}
		}.sendRequest();
	}
	@GetMapping(value = "/getTree")
	@ApiOperation(value = "获取组织机构树", notes = "获取组织机构树")
	public ResponseInfo getTree(@RequestParam(value = "type", defaultValue = "0", required = false) final Integer type,
								@RequestParam(value = "contain", defaultValue = "0", required = false) final Integer contain,
								@RequestParam(value = "isBusiness", defaultValue = "false", required = false) final boolean isBusiness,
								@RequestParam(value = "rootId",defaultValue = "",required = false)String rootId) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.getOrgTree(type, contain, isBusiness, rootId));
			}
		}.sendRequest();
	}

	/**
	 * 获取其他州市责任分局
	 * @param ids
	 * @return
	 */
	@GetMapping(value = "/getTreeZrfj")
	@ApiOperation(value = "获取组织机构树", notes = "获取组织机构树")
	public ResponseInfo getTreeZrfj(@RequestParam(value = "type", defaultValue = "0", required = false) final Integer type,
								@RequestParam(value = "contain", defaultValue = "0", required = false) final Integer contain,
								@RequestParam(value = "isBusiness", defaultValue = "false", required = false) final boolean isBusiness,
								@RequestParam(value = "rootId",defaultValue = "",required = false)String rootId,
									@RequestParam(value = "ssxq",defaultValue = "",required = false)String ssxq,
									@RequestParam(value = "sspcs",defaultValue = "",required = false)String sspcs) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.getTreeZrfj(type, contain, isBusiness, rootId,ssxq,sspcs));
			}
		}.sendRequest();
	}

	
	@GetMapping(value = "/delete")
	@ApiOperation(value = "删除组织机构信息", notes = "删除组织机构信息")
	public ResponseInfo delete(@RequestParam final String ids) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.removeByIds(Arrays.asList(ids.split(","))));
			}
		}.sendRequest();
	}

	@GetMapping(value = "/{id}")
	@ApiOperation(value = "删除组织机构信息", notes = "删除组织机构信息")
	public ResponseInfo getByid(@PathVariable("id") final String id) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.getById(id));
			}
		}.sendRequest();
	}

	@GetMapping(value = "/allChild")
	@ApiOperation(value = "获取子节点id", notes = "获取子节点id")
	public ResponseInfo getAllChild(@RequestParam String pIds, @RequestParam boolean containsAll, @RequestParam boolean isBusiness) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.getAllChild(pIds,containsAll,isBusiness));
			}
		}.sendRequest();
	}

	@GetMapping(value = "/listAll")
	@ApiOperation(value = "获取所有", notes = "获取所有")
	public ResponseInfo getAllChild() {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(organizationService.list(new QueryWrapper<OrganizationEntity>()));
			}
		}.sendRequest();
	}

	@GetMapping(value = "/getOrgByCurrentUserId")
	@ApiOperation(value = "根据当前用户权限查询组织机构", notes = "根据当前用户权限查询的组织机构")
	public ResponseInfo getOrgByCurrentUserId() {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria responseCriteria, Object... objects) {
				responseCriteria.addSingleResult(organizationService.getOrgByCurrentUserId());
			}
		}.sendRequest();
	}

	@PostMapping(value = "/getOrgByCurrentUserIdV2")
	@ApiOperation(value = "根据当前用户权限查询组织机构", notes = "根据当前用户权限查询的组织机构")
	public ResponseInfo getOrgByCurrentUserIdV2(@RequestBody final AccountBo accountBo, @RequestParam(value="type",required = false,defaultValue = "0")int type) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria responseCriteria, Object... objects) {
				responseCriteria.addSingleResult(organizationService.getOrgByCurrentUserId(accountBo, type));
			}
		}.sendRequest();
	}

	@GetMapping(value = "/getOrgByParentId")
	@ApiOperation(value = "根据当父id查询组织机构", notes = "根据父id查询组织机构")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "parentId", value = "父id", required = true, dataType = "String", paramType = "query", defaultValue = "0"),
		@ApiImplicitParam(name = "type", value = "过滤类型", required = false, dataType = "String", paramType = "query")
	})
	public ResponseInfo getOrgByParentId(@RequestParam String parentId, @RequestParam(value = "type", defaultValue = "0", required = false) final Integer type) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria responseCriteria, Object... objects) {
				responseCriteria.addSingleResult(organizationService.getOrgByParentId(parentId, type));
			}
		}.sendRequest();
	}

	@ApiImplicitParam(name = "id",value = "组织id",required = true,dataType = "String",paramType = "query")
	@GetMapping("/getAllUpOrganization")
	public ResponseInfo getAllUpOrganization(@RequestParam String id){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria responseCriteria, Object... objects) {
				responseCriteria.addSingleResult(organizationService.getListByRegionId(id));
			}
		}.sendRequest();
	}
}
