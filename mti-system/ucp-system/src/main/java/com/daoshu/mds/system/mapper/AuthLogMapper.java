package com.daoshu.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.system.bo.AuthLogBo;
import com.daoshu.system.entity.CpAuthLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AuthLogMapper extends BaseMapper<CpAuthLog> {
    List<CpAuthLog> listAuthLog(@Param(value = "authRole") List authRole, @Param(value = "searchTerm") String searchTerm, @Param(value = "page") Integer page, @Param(value = "size") Integer size);

    Long listCount(AuthLogBo authLogBo);

    List<CpAuthLog> export(AuthLogBo authLogBo);
}
