package com.daoshu.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.system.entity.ConfigEntity;
import com.daoshu.system.entity.KmZdryZrbmEntity;
import com.daoshu.system.entity.SysDictEntity;
import com.daoshu.system.entity.TKmZdryType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface ConfigMapper extends BaseMapper<ConfigEntity> {

    List<SysDictEntity> selectPersonDXL(@Param("type") String type);

    int selectCountKey(@Param("key") String key);

    void insertPersonDXL(@Param("sysDictEntity") SysDictEntity sysDictEntity);

    void insertPersonPolice(@Param("kmZdryZrbmEntity") KmZdryZrbmEntity kmZdryZrbmEntity);

    SysDictEntity selectOnePersonDXL(@Param("id") String id);

    Boolean updatePersonDXL(@Param("sysDictEntity") SysDictEntity sysDictEntity);

    Boolean updatePersonPolice(@Param("kmZdryZrbmEntity") KmZdryZrbmEntity kmZdryZrbmEntity);

    Boolean deletePersonDXL(@Param("id") String id);

    Boolean deletePersonPolice(@Param("id") String id);

    KmZdryZrbmEntity selectPersonPoliceName(@Param("xlCode") String xlCode,@Param("dlCode") String dlCode);

    Boolean updatePersonType(@Param("tKmZdryType") TKmZdryType tKmZdryType);
}
