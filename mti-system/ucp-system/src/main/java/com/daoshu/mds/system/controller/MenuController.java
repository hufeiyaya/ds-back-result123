package com.daoshu.mds.system.controller;

import java.util.Arrays;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.daoshu.component.page.PageConstants;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.system.service.IMenuService;
import com.daoshu.system.bo.MenuBo;
import com.daoshu.system.entity.MenuEntity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "权限菜单管理接口", description = "权限菜单管理接口")
@RestController
@RequestMapping(value = "/menu")
public class MenuController {

	@Autowired
	private IMenuService menuService;
	
	@GetMapping(value = "/getById")
	@ApiOperation(value = "通过id权限菜单信息", notes = "通过id获取权限菜单信息")
	public ResponseInfo getMenu(@RequestParam(value = "id", required = true) final String id) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(menuService.getById(id));
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/saveOrUpdate")
	@ApiOperation(value = "添加或修改权限菜单信息", notes = "添加或修改权限菜单信息")
	public ResponseInfo saveOrUpdate(@RequestBody final MenuBo menuBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				MenuEntity menu = new MenuEntity();
				BeanUtils.copyProperties(menuBo, menu);
				criteria.addSingleResult(menuService.saveOrUpdate(menu));
			}
		}.sendRequest();
	}

	
	@PostMapping(value = "/list")
	@ApiOperation(value = "获取权限菜单列表信息", notes = "获取权限菜单列表信息")
	public ResponseInfo list(@RequestBody final MenuBo menuBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				IPage<MenuEntity> page = menuService.getPage(menuBo);
				criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
				criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
				criteria.addSingleResult(menuService.getPage(menuBo));
			}
		}.sendRequest();
	}
	
	@GetMapping(value = "/delete")
	@ApiOperation(value = "根据id删除权限菜单信息", notes = "根据id删除权限菜单信息")
	public ResponseInfo delete(@RequestParam final String ids) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(menuService.removeByIds(Arrays.asList(ids.split(","))));
			}
		}.sendRequest();
	}
	
}
