package com.daoshu.mds.system.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.system.mapper.MenuMapper;
import com.daoshu.mds.system.service.IMenuService;
import com.daoshu.system.bo.MenuBo;
import com.daoshu.system.entity.MenuEntity;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, MenuEntity> implements IMenuService {

	@Override
	public IPage<MenuEntity> getPage(MenuBo menuBo) {
		MenuEntity condition = new MenuEntity();
		BeanUtils.copyProperties(menuBo, condition);
		Wrapper<MenuEntity> queryWrapper = new QueryWrapper<MenuEntity>(condition);
		return this.page(new Page<>(menuBo.getStart(), menuBo.getSize()), queryWrapper);
	}

}
