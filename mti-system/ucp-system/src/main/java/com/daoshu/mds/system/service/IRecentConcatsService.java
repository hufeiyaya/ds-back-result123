package com.daoshu.mds.system.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.bo.ContentBO;
import com.daoshu.system.bo.RecentConcatBO;
import com.daoshu.system.entity.RecentConcatsEntity;
import com.daoshu.system.qo.RecentConcatsQO;

/**
 * <p>
 *  最近联系人服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface IRecentConcatsService extends IService<RecentConcatsEntity> {
	
	/**
	 * 最近联系人查询
	 *
	 * @param qo
	 * @return
	 */
	IPage<RecentConcatBO> queryRecent(RecentConcatsQO qo);

	/**
	 * 最近联系人查询
	 *
	 * @param qo
	 * @return
	 */
	IPage<RecentConcatBO> queryRecentSimple(RecentConcatsQO qo);

	/**
	 * 添加最近联系人
	 *
	 * @param entity
	 * @return 
	 */
	RecentConcatsEntity addRecent(RecentConcatsEntity entity);

	/**
	 * 根据人员或者单位ID及类型查询详情
	 *
	 * @param id
	 * @param type
	 * @return
	 */
	ContentBO getByIdAndType(String id, Integer type);

	JSONObject statistics();
}
