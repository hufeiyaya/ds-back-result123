package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.bo.AuthLogBo;
import com.daoshu.system.entity.CpAuthLog;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author zhangmingxin
 */
public interface IAuthLogService extends IService<CpAuthLog> {
    List<CpAuthLog> listLogs(AuthLogBo authLogBo);

    Long listCount(AuthLogBo authLogBo);

    List<CpAuthLog> export(File file, AuthLogBo authLogBo) throws InterruptedException, IOException;

}
