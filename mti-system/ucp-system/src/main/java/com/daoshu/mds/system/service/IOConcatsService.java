package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.bo.ContentBO;
import com.daoshu.system.entity.OConcatsEntity;

import java.util.List;

/**
 * <p>
 *  组织通讯录服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface IOConcatsService extends IService<OConcatsEntity> {
	/**
	 * 新增
	 * @param entity
	 * @return
	 */
	boolean insert(OConcatsEntity entity);
	
	/**
	 * 修改
	 * @param entity
	 * @return
	 */
	boolean update(OConcatsEntity entity);
	/**
	 * 获取单位通讯录
	 * @param orgId
	 * @return
	 */
	List<OConcatsEntity> getConcat(String orgId);
	/**
	 * 根据联系方式与联系方式内容获取通讯录信息
	 * @param content
	 * @param type
	 * @return
	 */
	OConcatsEntity getByContentAndType(String content, Integer type);
	/**
	 * 分页查询单位联系方式
	 * @param orgIds
	 * @return
	 */
	List<ContentBO> queryOrgConcat(List<String> orgIds);
}
