package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.entity.AccountRoleEntity;

import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface IAccountRoleService extends IService<AccountRoleEntity> {

	/**
	 * 通过账户ID 获取账户的权限IDS
	 * @Title: getMenuIdsByAccountId
	 * @Description: TODO
	 * @param accountId
	 * @return
	 * @return: Set<String>
	 */
	Set<String> getMenuIdsByAccountId(String accountId);

	/**
	 * 根据人员id删除人员角色
	 * @param accountId
	 */
    void deletePersonRole(String accountId);
}
