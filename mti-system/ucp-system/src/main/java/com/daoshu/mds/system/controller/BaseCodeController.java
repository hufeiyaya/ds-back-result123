package com.daoshu.mds.system.controller;

import java.util.Arrays;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.system.service.IBaseCodeService;
import com.daoshu.system.bo.BaseCodeBo;
import com.daoshu.system.entity.BaseCodeEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName: CodeController
 * @Description: TODO
 * @author: Meifeng Tian
 * @date: 2018年9月20日 下午2:58:55
 */
@ApiModel(value = "基础代码控制器")
@RestController
@RequestMapping("/basecode")
public class BaseCodeController {

	@Autowired
	private IBaseCodeService baseCodeService;
	
	@PostMapping(value = "/list")
	@ApiOperation(value = "查询基础代码列表", notes = "查询基础代码列表")
	public ResponseInfo list(@RequestBody final BaseCodeBo baseCodeBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(baseCodeService.page(new Page<>(baseCodeBo.getStart(), baseCodeBo.getSize()), new QueryWrapper<BaseCodeEntity>()));
			}
		}.sendRequest();
	}
	
	@DeleteMapping(value = "/delete")
	@ApiOperation(value = "删除基础代码信息", notes = "删除基础代码信息")
	public ResponseInfo delete(@RequestParam final String ids) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(baseCodeService.removeByIds(Arrays.asList(ids.split(","))));
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/saveOrUpdate")
	@ApiOperation(value = "新增或修改基础代码", notes = "新增或修改基础代码信息")
	public ResponseInfo saveOrUpdate(@RequestBody final BaseCodeBo baseBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				BaseCodeEntity entity = new BaseCodeEntity();
				BeanUtils.copyProperties(baseBo, entity);
				criteria.addSingleResult(baseCodeService.saveOrUpdate(entity));
			}
		}.sendRequest();
	}
}
