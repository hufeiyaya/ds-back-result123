package com.daoshu.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.system.entity.GeneralConcatsEntity;

/**
 * <p>
  *  常用联系人Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface GeneralConcatsMapper extends BaseMapper<GeneralConcatsEntity> {

}