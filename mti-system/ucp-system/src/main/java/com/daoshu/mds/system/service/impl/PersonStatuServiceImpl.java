package com.daoshu.mds.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.system.mapper.PersonStatusMapper;
import com.daoshu.mds.system.service.IPersonStatuService;
import com.daoshu.system.entity.PersonStatusEntity;
import org.springframework.stereotype.Service;

@Service
public class PersonStatuServiceImpl extends ServiceImpl<PersonStatusMapper, PersonStatusEntity> implements IPersonStatuService {
}
