package com.daoshu.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.system.entity.OrganizationEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface OrganizationMapper extends BaseMapper<OrganizationEntity> {
    /**
     * 通过组织id查询所有的父节点
     * @param regionId
     * @return
     */
    List<OrganizationEntity> getListByRegionId(@Param("regionId") String regionId);
}
