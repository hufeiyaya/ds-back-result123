package com.daoshu.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.system.entity.OConcatsEntity;

/**
 * <p>
  *  组织通讯录Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface OConcatsMapper extends BaseMapper<OConcatsEntity> {

}