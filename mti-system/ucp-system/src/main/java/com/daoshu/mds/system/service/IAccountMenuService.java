package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.entity.AccountMenuEntity;

import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface IAccountMenuService extends IService<AccountMenuEntity> {

	/**
	 * 通过账户ID 获取菜单的IDS
	 * @Title: getMenuIdsByAccountId
	 * @Description: TODO
	 * @param id
	 * @return
	 * @return: Set<String>
	 */
	Set<String> getMenuIdsByAccountId(String id);

}
