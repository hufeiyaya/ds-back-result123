package com.daoshu.mds.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.system.service.IAccountService;
import com.daoshu.system.bo.AccountBo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "登陆登出管理接口", description = "登陆登出管理接口")
@RestController
@RequestMapping(value = "/login")
public class LoginController {

	@Autowired
	private IAccountService accountService;
	
	@PostMapping(value = "/login")
	@ApiOperation(value = "登陆", notes = "账户登陆")
	public ResponseInfo login(@RequestBody final AccountBo accountBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(accountService.login(accountBo));
			}
		}.sendRequest();
	}
	
	@GetMapping(value = "logout")
	@ApiOperation(value = "退出", notes = "退出登陆")
	public ResponseInfo logout() {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(accountService.logout());
			}
		}.sendRequest();
	}
}
