package com.daoshu.mds.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.component.exception.BusinessException;
import com.daoshu.mds.system.mapper.AuthLogMapper;
import com.daoshu.mds.system.service.IAuthLogService;
import com.daoshu.mds.system.threadhandle.AuthLogExportHandle;
import com.daoshu.mds.system.utils.ListUtils;
import com.daoshu.system.bo.AuthLogBo;
import com.daoshu.system.entity.CpAuthLog;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author zhangmingxin
 */
@Service
@AllArgsConstructor
@Slf4j
public class AuthLogServiceImpl extends ServiceImpl<AuthLogMapper, CpAuthLog> implements IAuthLogService {
    private final AuthLogMapper authLogMapper;

    @Override
    public List<CpAuthLog> listLogs(AuthLogBo authLogBo) {
        return authLogMapper.listAuthLog(authLogBo.getAuthRole(), authLogBo.getSearchTerm(), authLogBo.getPage() == null || authLogBo.getPage() < 0 ? 0 : authLogBo.getPage() - 1, authLogBo.getSize());
    }

    @Override
    public Long listCount(AuthLogBo authLogBo) {
        return authLogMapper.listCount(authLogBo);
    }

    @Override
    public List<CpAuthLog> export(File file, AuthLogBo authLogBo) throws InterruptedException, IOException {
        List<CpAuthLog> list = authLogMapper.export(authLogBo);
        if (null == list || list.size() == 0) throw new BusinessException(500, "未查询到导出数据");

        OutputStream outputStream = new FileOutputStream(file);
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        CellStyle cellStyleCenter = wb.createCellStyle();
        CellStyle cellStyleLeft = wb.createCellStyle();
        cellStyleCenter.setAlignment(HorizontalAlignment.CENTER); // 居中
        cellStyleCenter.setVerticalAlignment(VerticalAlignment.CENTER); // 居中
        cellStyleLeft.setAlignment(HorizontalAlignment.LEFT);
        cellStyleLeft.setVerticalAlignment(VerticalAlignment.CENTER);

        List<List<CpAuthLog>> exportData = ListUtils.splitList(list, 15000);

        ExecutorService pool = Executors.newFixedThreadPool(exportData.size());

        CountDownLatch countDownLatch = new CountDownLatch(exportData.size());
        for (int i = 0; i < exportData.size(); i++) {
            StringBuilder sb = null;
            if (null == file.getName() || file.getName().length() <= 0) sb = new StringBuilder("授权日志导出列表");
            else sb = new StringBuilder(file.getName());
            sb.append("-").append(i);
            SXSSFSheet sh = wb.createSheet(sb.toString());
            sh.trackAllColumnsForAutoSizing();
            pool.execute(new AuthLogExportHandle(sh, cellStyleCenter, cellStyleLeft, exportData.get(i), file.getName(), countDownLatch));
        }
        countDownLatch.await();
        try {
            wb.write(outputStream);
        } catch (IOException e) {
            log.error("error == >{}", e.getMessage());
        } finally {
            try {
                wb.close();
                outputStream.flush();
                outputStream.close();
            } catch (IOException e) {
                log.error("error == >{}", e.getMessage());
            }
            pool.shutdown();
        }
        return list;
    }


}
