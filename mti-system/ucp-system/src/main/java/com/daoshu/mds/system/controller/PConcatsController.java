package com.daoshu.mds.system.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daoshu.component.page.PageConstants;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.system.service.IPConcatsService;
import com.daoshu.system.bo.ContentBO;
import com.daoshu.system.entity.PConcatsEntity;
import com.daoshu.system.qo.GeneralConcatsQO;
import com.daoshu.system.qo.PersonConcatsQO;
import com.daoshu.system.utils.ListUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  人员通讯录前端控制器
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Api(value="人员通讯录接口", description="人员通讯录接口")
@RestController
@RequestMapping("/mds")
public class PConcatsController {
	
	@Autowired
	IPConcatsService pConcatsService;
	
	@ApiOperation(value="查询人员通讯录",notes="查询人员通讯录")
	@RequestMapping(value="/personConcats/page",method=RequestMethod.POST)
	public ResponseInfo queryLocalUserGeneralConcats(@RequestBody final GeneralConcatsQO qo){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				PConcatsEntity entity = new PConcatsEntity();
				BeanUtils.copyProperties(qo, entity);
				QueryWrapper<PConcatsEntity> queryWrapper = new QueryWrapper<>(entity);
				queryWrapper.orderBy(true, true, "sort_");
				IPage<PConcatsEntity> page = pConcatsService.page(new Page<PConcatsEntity>(qo.getStart(), qo.getSize()), queryWrapper);
				criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
				criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
			}
		}.sendRequest();
	}
	@ApiOperation(value="查询人员通讯录，不分页",notes="查询人员通讯录")
	@RequestMapping(value="/personConcats/list",method=RequestMethod.POST)
	public ResponseInfo queryLocalUserGeneralConcatsList(@RequestBody final PersonConcatsQO qo){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				qo.setSize(100000000);
				qo.setStart(1);
				IPage<ContentBO> page = pConcatsService.queryPersonConcat(qo);
				criteria.addSingleResult(page.getRecords());
			}
		}.sendRequest();
	}
	@ApiOperation(value="查询联系人列表及详情",notes="查询联系人列表及详情")
	@RequestMapping(value="/concats/persons/page",method=RequestMethod.POST)
	public ResponseInfo queryLocalUserGeneralConcats(@RequestBody final PersonConcatsQO qo){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				IPage<ContentBO> page = pConcatsService.queryPersonConcat(qo);
				criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
				criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
			}
		}.sendRequest();
	}
	@ApiOperation(value="新增人员通讯录",notes="新增人员通讯录")
	@RequestMapping(value="/personConcats",method=RequestMethod.POST)
	public ResponseInfo addUserGeneralConcats(@RequestBody final PConcatsEntity entity){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				boolean flag = pConcatsService.insert(entity);
				criteria.addSingleResult(flag?"新增成功":"新增失败,请联系管理员处理");
			}
		}.sendRequest();
	}
	@ApiOperation(value="修改人员通讯录",notes="修改人员通讯录")
	@RequestMapping(value="/personConcats",method=RequestMethod.PUT)
	public ResponseInfo updateUserGeneralConcats(@RequestBody final PConcatsEntity entity){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				boolean flag = pConcatsService.update(entity);
				criteria.addSingleResult(flag?"修改成功":"修改失败,请联系管理员处理");
			}
		}.sendRequest();
	}
	@ApiOperation(value="删除通讯录",notes="删除通讯录")
	@RequestMapping(value="/personConcats/{id}",method=RequestMethod.DELETE)
	public ResponseInfo updateUserGeneralConcats(@PathVariable final String id){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				boolean flag = pConcatsService.removeById(id);
				criteria.addSingleResult(flag?"删除成功":"删除失败,请联系管理员处理");
			}
		}.sendRequest();
	}
	@ApiOperation(value="设置默认联系方式",notes="设置默认联系方式")
	@RequestMapping(value="/personConcats/defaultConcat",method=RequestMethod.GET)
	public ResponseInfo defaultConcat(@RequestParam final String id,@RequestParam final String personId){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				pConcatsService.defaultConcat(id,personId);
				criteria.addSingleResult("设置成功");
			}
		}.sendRequest();
	}
	
	@ApiOperation(value="获取人员所有联系方式",notes="获取人员所有联系方式")
	@RequestMapping(value="/personConcats/{personId}",method=RequestMethod.GET)
	public ResponseInfo getConcat(@RequestParam final String personId){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(pConcatsService.getConcat(personId));
			}
		}.sendRequest();
	}
	@ApiOperation(value="根据联系方式查询第一条",notes="根据联系方式查询第一条")
	@RequestMapping(value="/personConcats",method=RequestMethod.GET)
	public ResponseInfo getByConcatsAndPersonId(@RequestParam final String personId,@RequestParam final Integer concatType){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(pConcatsService.getOne(new QueryWrapper<PConcatsEntity>().eq("person_id_", personId).eq("concat_type_", concatType).orderByAsc("sort_")));
			}
		}.sendRequest();
	}
	@ApiOperation(value="根据联系方式删除",notes="根据联系方式删除")
	@RequestMapping(value="/personConcats",method=RequestMethod.DELETE)
	public ResponseInfo deleteByConcatsAndPersonId(@RequestParam final String personId,@RequestParam final Integer concatType){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(pConcatsService.remove(new QueryWrapper<PConcatsEntity>().eq("person_id_", personId).eq("concat_type_", concatType)));
			}
		}.sendRequest();
	}
	@ApiOperation(value="排除某人删除联系方式",notes="排除某人删除联系方式")
	@RequestMapping(value="/personConcats/exist",method=RequestMethod.DELETE)
	public ResponseInfo deleteByConcatsAndExistPersonId(@RequestParam final String personId,@RequestParam final Integer concatType,@RequestParam final String number){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				List<String> persons = new ArrayList<>(1);
				persons.add(personId);
				pConcatsService.remove(new QueryWrapper<PConcatsEntity>()
						.notIn("person_id_", persons)
						.eq("concat_type_", concatType)
						.eq("concat_number_", number));
				criteria.addSingleResult("删除成功");
			}
		}.sendRequest();
	}
	@ApiOperation(value="查询默认联系方式",notes="查询默认联系方式")
	@RequestMapping(value="/personConcats/queryDefaultConcat",method=RequestMethod.POST)
	public ResponseInfo queryDefaultConcats(@RequestBody final List<String> personIds){
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				if(!personIds.isEmpty()) {
					List<PConcatsEntity> list = pConcatsService.list(new QueryWrapper<PConcatsEntity>().lambda()
							.in(PConcatsEntity::getPersonId, ListUtils.removeNullElement(personIds))
							.eq(PConcatsEntity::getDefaultValue, 1));
					criteria.addSingleResult(list);
				}
			}
		}.sendRequest();
	}
}
