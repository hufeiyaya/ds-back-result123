package com.daoshu.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.system.entity.RoleDataEntity;

public interface IRoleDataService extends IService<RoleDataEntity>  {

}
