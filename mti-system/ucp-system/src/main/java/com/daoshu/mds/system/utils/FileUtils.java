package com.daoshu.mds.system.utils;

import java.io.File;

/**
 * <p>文件操作工具类<p>
 *
 * @author hf
 * @version 1.0
 * @date 2017年1月18日
 */
public class FileUtils {

    static {
        mkDir(getConTextPath() + File.separator + "authLog");
    }


    /**
     * 获取windows/linux的项目根目录
     *
     * @return
     */
    public static String getConTextPath() {
        String path = System.getProperty("user.dir");
        return path;
    }


    public static String getLogFilePath() {
        File logPath = new File(getConTextPath() + File.separator + "authLog");
        try {
            if (!logPath.exists()) {
                logPath.mkdir();
            }
        } catch (Exception e) {
            System.out.println("新建目录操作出错");
            e.printStackTrace();
        }
        return logPath.getAbsolutePath() + File.separator;
    }

    /**
     * 创建目录
     *
     * @param dir_path
     */
    public static void mkDir(String dir_path) {
        File myFolderPath = new File(dir_path);
        try {
            if (!myFolderPath.exists()) {
                myFolderPath.mkdir();
            }
        } catch (Exception e) {
            System.out.println("新建目录操作出错");
            e.printStackTrace();
        }
    }


}