package com.daoshu.mds.system.controller;

import com.daoshu.mds.system.component.annotations.AuthAnnotation;
import com.daoshu.mds.system.service.IAccountRoleService;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.system.service.IRoleService;
import com.daoshu.system.bo.RoleBo;
import com.daoshu.system.entity.RoleEntity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "角色管理接口", description = "角色管理接口")
@RestController
@RequestMapping(value = "/role")
public class RoleController {

	@Autowired
	private IRoleService roleService;
	@Autowired
	private IAccountRoleService accountRoleService;
	
	@PostMapping(value = "/list")
	@ApiOperation(value = "查询角色列表", notes = "查询角色列表")
	public ResponseInfo list(@RequestBody final RoleBo roleBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
			criteria.addSingleResult(roleService.getPage(roleBo));
			}
		}.sendRequest();
	}
	
	@GetMapping(value = "/getListByType")
	@ApiOperation(value = "查询角色列表", notes = "查询角色列表")
	public ResponseInfo getListByType(@RequestParam Integer orgSort) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
			criteria.addSingleResult(roleService.getOrgsByType(orgSort));
			}
		}.sendRequest();
	}

	@GetMapping(value = "/getListByRegionId")
	@ApiOperation(value = "查询根据机构Id查询角色列表", notes = "查询根据机构Id查询角色列表")
	public ResponseInfo getListByRegionId(@RequestParam String regionId) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(roleService.getListByRegionId(regionId));
			}
		}.sendRequest();
	}
	
	@DeleteMapping(value = "/delete")
	@ApiOperation(value = "删除角色信息", notes = "删除角色信息")
	public ResponseInfo delete(@RequestParam final String ids) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(roleService.removeByIds(ids));
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/saveOrUpdate")
	@ApiOperation(value = "新增或修改角色信息", notes = "新增或修改角色信息")
	public ResponseInfo saveOrUpdate(@RequestBody final RoleBo roleBo) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				RoleEntity roleEntity = new RoleEntity();
				BeanUtils.copyProperties(roleBo, roleEntity);
				criteria.addSingleResult(roleService.saveOrUpd(roleEntity));
			}
		}.sendRequest();
	}

	@GetMapping("/deletePersonRole")
	@ApiImplicitParam(name = "accountId",value = "账号",dataType = "String",paramType = "query")
	@AuthAnnotation(opVal = "deletePersonRole")
	public ResponseInfo deletePersonRole(@RequestParam String accountId){
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				accountRoleService.deletePersonRole(accountId);
			}
		}.sendRequest();
	}
}
