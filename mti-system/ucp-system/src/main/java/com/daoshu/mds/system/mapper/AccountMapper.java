package com.daoshu.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.system.entity.AccountEntity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
public interface AccountMapper extends BaseMapper<AccountEntity> {

}
