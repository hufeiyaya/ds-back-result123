package com.daoshu.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.system.entity.DictionaryEntity;

/**
 *
 */
public interface DictionaryMapper extends BaseMapper<DictionaryEntity> {
}
