package com.daoshu.system.bo;

import com.daoshu.system.entity.OConcatsEntity;
import com.daoshu.system.entity.PConcatsEntity;
import lombok.Data;

import java.util.List;

@Data
public class ConcatsBO {

	private String id;
	/** 组织或者人员ID */
	private String objId;
	/** 组织或者人员名称 */
	private String objName;
	
	private Integer concatType;
	
	private String concatNumber;
	
	private Integer defaultValue;
	/** 1，人员，2。组织 */
	private Integer type;
	
	private List<PConcatsEntity> pConcats;
	
	private List<OConcatsEntity> oConcats;
	
	private List<String> deleteConcats;
}
