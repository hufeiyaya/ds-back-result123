package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@Entity
@ToString
@ApiModel(description = "基础代码实体")
@TableName(value = "t_pf_base_code")
public class BaseCodeEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id_")
    private String id;

    @TableField(value = "type_id_")
    private String typeId;
    
    @TableField(value = "code_")
    private String code;

    @TableField(value = "name_")
    private String name;

    @TableField(value = "short_name_")
    private String shortName;

    @TableField(value = "sort_")
    private Integer sort;

    @TableField(value = "available_")
    private Integer available;

    @TableLogic
    @TableField(value = "delete_")
    private Integer delete;
}
