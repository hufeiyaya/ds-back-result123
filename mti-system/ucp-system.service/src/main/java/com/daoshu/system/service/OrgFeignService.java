package com.daoshu.system.service;

import com.daoshu.component.response.ResponseInfo;
import com.daoshu.component.util.TransReponseUtil;
import com.daoshu.system.entity.OrganizationEntity;
import com.daoshu.system.service.client.OrgClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName OrgService
 * @Description OrgService
 * @Author zhaoyj
 * @Date 2019/4/8 14:37
 */
@Service
public class OrgFeignService {

    @Autowired private OrgClient orgClient;

    public OrganizationEntity getById(String id){
        ResponseInfo info = orgClient.getById(id);
        return TransReponseUtil.transFormToClass(info,OrganizationEntity.class);
    }
    public List<OrganizationEntity> listByIds(List<String> ids){
        ResponseInfo info = orgClient.listByIds(ids);
        return TransReponseUtil.transFormToListClass(info,OrganizationEntity.class);
    }

    public List<String> getAllChild(String pIds, boolean containsAll, boolean isBusiness){
        ResponseInfo info = orgClient.getAllChild(pIds,containsAll,isBusiness);
        return TransReponseUtil.transFormToListClass(info,String.class);
    }
    public List<OrganizationEntity> listAll(){
        ResponseInfo info = orgClient.listAll();
        return TransReponseUtil.transFormToListClass(info,OrganizationEntity.class);
    }
    public void save(OrganizationEntity entity){
        orgClient.save(entity);
    }
    public void updateById(OrganizationEntity entity){
        orgClient.updateById(entity);
    }
}
