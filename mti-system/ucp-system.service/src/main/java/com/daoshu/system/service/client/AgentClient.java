package com.daoshu.system.service.client;


import com.daoshu.component.response.ResponseInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "UCP-SYSTEM")
public interface AgentClient {

    @RequestMapping(value="/agent/ip",method= RequestMethod.GET)
    ResponseInfo getByIP(@RequestParam(value = "ip") String ip);

    /**
     * 根据分机号获取agent
     */
    @RequestMapping(value="/agent/ext",method= RequestMethod.GET)
    ResponseInfo getByExt(@RequestParam(value = "ext") final String ext);
}
