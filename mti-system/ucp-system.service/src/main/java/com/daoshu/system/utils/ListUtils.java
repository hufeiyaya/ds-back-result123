package com.daoshu.system.utils;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {

	/**
	 * 删除列表中元素为null 的元素
	 * @param list
	 */
	public static <T> List<T> removeNullElement(List<T> list) {
		List<T> result =  new ArrayList<T>();
		if(list == null || list.isEmpty())
			return result;
		for(int i = list.size() - 1; i >= 0; i--) {
			if(list.get(i) != null && !"null".equalsIgnoreCase(list.get(i).toString().trim())) {
				result.add(list.get(i));
			}
		}
		return result;
	}
}
