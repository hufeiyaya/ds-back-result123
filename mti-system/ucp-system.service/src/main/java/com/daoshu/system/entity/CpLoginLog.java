package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author zhangmingxin
 */
@Data
@ApiModel(value = "登陆信息日志")
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "cp_login_log")
public class CpLoginLog implements Serializable {
    private static final long serialVersionUID = 2424149135140927843L;
    @ApiModelProperty(value = "id")
    @TableId
    private String id;
    @ApiModelProperty(value = "警号")
    @TableField("number")
    private String number;
    @ApiModelProperty(value = "姓名")
    @TableField("name")
    private String name;
    @ApiModelProperty(value = "ip")
    @TableField("ip")
    private String ip;
    @ApiModelProperty(value = "组织单位")
    @TableField("org")
    private String org;
    @ApiModelProperty(value = "登陆类型(1：登陆，0：登出)")
    @TableField("login_type")
    private String loginType;
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-mm-dd HH:MM:SS")
    private LocalDateTime createTime;
    @ApiModelProperty(value = "操作时间")
    @TableField(exist = false)
    private String loginDateTime;
    @ApiModelProperty(value = "组织名称")
    @TableField("org_id")
    private String orgId;
}
