package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@ToString
@Entity
@ApiModel(description = "区域信息实体类")
@TableName(value = "t_pf_region")
public class RegionEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id")
    private String id;

    @TableField(value = "region_name")
    private String regionName;

    @TableField(value = "region_code")
    private String regionCode;

    @TableField(value = "parent_code")
    private String parentCode;

    /**
     * 等级：1 代表省；2 代表市； 3 代表县
     */
    @TableField(value = "level")
    private Integer level;

}
