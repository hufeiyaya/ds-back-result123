package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 常用联系人
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@EqualsAndHashCode(callSuper=false)
@Data
@TableName("t_mds_general_concats")
public class GeneralConcatsEntity implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@TableId(value = "id_")
	private String id;

	/** 用户ID */
    @TableField("person_id_")
	private String personId;
	/** 联系人ID */
    @TableField("link_person_id_")
	private String linkPersonId;
	/** 序号 */
    @TableField("sort_")
	private Integer sort;
	/** 有效标志 */
    @TableField("is_available_")
	private Integer isAvailable;
	/** 类型 1.人员; 2.单位*/
    @TableField("link_type_")
	private Integer linkType;


	@Override
	public String toString() {
		return "TMdsGeneralConcats{" +
			"id=" + id +
			", personId=" + personId +
			", linkPersonId=" + linkPersonId +
			", sort=" + sort +
			", isAvailable=" + isAvailable +
			", linkType=" + linkType +
			"}";
	}
}
