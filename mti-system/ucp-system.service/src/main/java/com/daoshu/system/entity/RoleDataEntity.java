package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;

@Getter
@Setter
@ToString
@Entity
@TableName(value = "t_pf_role_data")
public class RoleDataEntity implements Serializable {

	@TableId("id_")
	protected String id;
	/**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -2446499163698456771L;
	
	@TableField(value = "role_id_")
	private String roleId;
	
	@TableField(value = "org_ids_")
	private String orgIds;

}
