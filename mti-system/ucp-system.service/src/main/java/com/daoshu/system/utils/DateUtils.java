package com.daoshu.system.utils;

import java.util.Date;

public class DateUtils {

	public static String getSubTimeString(Date start,Date end) {
		if(start == null || end == null) {
			return "";
		}
		long temp = end.getTime()-start.getTime();
		long hour = temp/1000/60/60;
		long min = (temp/1000)%3600/60;
		long sen = (temp/1000)%3600%60;
        StringBuilder sb = new StringBuilder("");
        if(hour > 0)
        	sb.append(hour).append("时");
        if(min > 0)
        	sb.append(min).append("分");
        if(sen > 0)
        	sb.append(sen).append("秒");
        return sb.toString();
	}
	
	public static String getSubTimeString(int sec) {
		long temp = sec*1000;
		long hour = temp/1000/60/60;
		long min = (temp/1000)%3600/60;
		long sen = (temp/1000)%3600%60;
        StringBuilder sb = new StringBuilder("");
        if(hour > 0)
        	sb.append(hour).append("时");
        if(min > 0)
        	sb.append(min).append("分");
        if(sen > 0)
        	sb.append(sen).append("秒");
        return sb.toString();
	}
}
