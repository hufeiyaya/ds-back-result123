package com.daoshu.system.service.client;

import com.daoshu.component.response.ResponseInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @ClassName PersonClient
 * @Description PersonClient
 * @Author zhaoyj
 * @Date 2019/4/8 13:57
 */
@FeignClient(name = "UCP-SYSTEM")
public interface SipUserAccountClient {

    /**
     * 获取所有关联列表
     * @param ids
     * @return
     */
    @RequestMapping(value="/sip/user/account/list",method= RequestMethod.GET)
    ResponseInfo getAll();
//    /**
//     * 获取人员列表
//     * @param qo
//     * @return
//     */
//    @RequestMapping(value="/person/detaillPage",method= RequestMethod.POST)
//    ResponseInfo queryPersonPage(@RequestBody PersonConcatsQO qo);
}
