package com.daoshu.system.service;

import com.daoshu.component.response.ResponseInfo;
import com.daoshu.component.util.TransReponseUtil;
import com.daoshu.system.bo.AccountBo;
import com.daoshu.system.entity.AccountEntity;
import com.daoshu.system.service.client.AccountClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName PersonService
 * @Description AccountFeignService
 * @Author zhaoyj
 * @Date 2019/4/8 14:31
 */
@Service
public class AccountFeignService {

    @Autowired private AccountClient accountClient;

    public List<AccountBo> getPersonsByAccount(String[] ids){
        ResponseInfo info = accountClient.getPersonsByAccount(ids);
        return TransReponseUtil.transFormToListClass(info, AccountBo.class);
    }

    public void save(AccountEntity entity){
        accountClient.save(entity);
    }
}
