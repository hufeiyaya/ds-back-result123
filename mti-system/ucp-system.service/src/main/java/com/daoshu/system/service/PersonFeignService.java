package com.daoshu.system.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daoshu.component.page.PageConstants;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.component.util.TransReponseUtil;
import com.daoshu.system.bo.PersonContentBO;
import com.daoshu.system.entity.PersonEntity;
import com.daoshu.system.qo.PersonQO;
import com.daoshu.system.service.client.PersonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @ClassName PersonService
 * @Description TODO
 * @Author zhaoyj
 * @Date 2019/4/8 14:31
 */
@Service
public class PersonFeignService {

    @Autowired private PersonClient personClient;

    public void save(PersonEntity entity){
        personClient.save(entity);
    }
    public List<PersonEntity> listByIds(List<String> ids){
        ResponseInfo info = personClient.getByIds(ids);
        return TransReponseUtil.transFormToListClass(info,PersonEntity.class);
    }
    public List<PersonEntity> getPersonByIds(List<String> ids){
        ResponseInfo info = personClient.getByIds(ids);
        return JSON.parseArray(JSON.toJSONString(info.getData()), PersonEntity.class);
    }
    public PersonEntity getById(String id){
        ResponseInfo info = personClient.getById(id);
        return TransReponseUtil.transFormToClass(info,PersonEntity.class);
    }
    public IPage<PersonContentBO> queryPersonPage(PersonQO qo){
        ResponseInfo info = personClient.queryPersonPage(qo);
        IPage<PersonContentBO> page = new Page<>();
        if(info.getData() instanceof Map){
            @SuppressWarnings("unchecked")
            Map<String,Object> map = (Map<String, Object>) info.getData();
            page.setTotal(Long.valueOf(map.get(PageConstants.PAGE_RESULT_COUNT).toString()));
        }
        List<PersonContentBO> list = TransReponseUtil.transFormToListClass(info, PersonContentBO.class);
        page.setRecords(list);
        return page;
    }
}
