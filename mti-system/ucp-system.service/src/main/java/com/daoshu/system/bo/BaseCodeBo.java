package com.daoshu.system.bo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class BaseCodeBo extends BaseBo implements Serializable {
	
	/**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 4166453812868758529L;
	
    private String id;

    private String typeId;
    
    private String code;

    private String name;

    private String shortName;

    private Integer sort;

    private Integer available;
}
