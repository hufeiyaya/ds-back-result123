package com.daoshu.system.service.client;

import com.daoshu.component.response.ResponseInfo;
import com.daoshu.system.entity.AccountEntity;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @ClassName PersonClient
 * @Description PersonClient
 * @Author zhaoyj
 * @Date 2019/4/8 13:57
 */
@FeignClient(name = "UCP-SYSTEM")
public interface AccountClient {

    /**
     * 获取人员根据SIP列表
     * @param ids
     * @return
     */
    @RequestMapping(value="/account/personBySip",method= RequestMethod.POST)
    ResponseInfo getPersonsByAccount(@RequestBody String[] ids);

    @RequestMapping(value="/account/save",method= RequestMethod.POST)
    ResponseInfo save(@RequestBody AccountEntity entity);
}
