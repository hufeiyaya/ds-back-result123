package com.daoshu.system.service;

import com.daoshu.component.response.ResponseInfo;
import com.daoshu.component.util.TransReponseUtil;
import com.daoshu.system.entity.PersonStatusEntity;
import com.daoshu.system.service.client.PersonStatusClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName PersonStatusFeignService
 * @Description TODO
 * @Author zhaoyj
 * @Date 2019/4/10 16:12
 */
@Service
public class PersonStatusFeignService {

    @Autowired private PersonStatusClient personStatusClient;

    public List<PersonStatusEntity> getPersonStatus(List<String> ids){
        ResponseInfo info = personStatusClient.getPersonStatus(ids);
        return TransReponseUtil.transFormToListClass(info,PersonStatusEntity.class);
    }
}
