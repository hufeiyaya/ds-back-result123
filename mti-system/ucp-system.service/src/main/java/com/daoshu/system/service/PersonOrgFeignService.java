package com.daoshu.system.service;

import com.daoshu.component.response.ResponseInfo;
import com.daoshu.component.util.TransReponseUtil;
import com.daoshu.system.entity.OrganizationPersonEntity;
import com.daoshu.system.service.client.PersonOrgClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName PersonOrgFeignService
 * @Description TODO
 * @Author zhaoyj
 * @Date 2019/4/8 14:41
 */
@Service
public class PersonOrgFeignService {

    @Autowired private PersonOrgClient personOrgClient;

    public OrganizationPersonEntity getOne(OrganizationPersonEntity entity){
        return TransReponseUtil.transFormToClass(personOrgClient.getOne(entity), OrganizationPersonEntity.class);
    }

    public OrganizationPersonEntity getOne(String psersonId){
        OrganizationPersonEntity entity = new OrganizationPersonEntity();
        entity.setPersonId(psersonId);
        return this.getOne(entity);
    }

    public OrganizationPersonEntity getOneByOrgId(String orgId){
        OrganizationPersonEntity entity = new OrganizationPersonEntity();
        entity.setOrgId(orgId);
        return this.getOne(entity);
    }

    public List<OrganizationPersonEntity> listByOrgIds(List<String> orgIds){
        ResponseInfo info = personOrgClient.listByOrgIds(orgIds);
        return TransReponseUtil.transFormToListClass(info,OrganizationPersonEntity.class);
    }

    public List<OrganizationPersonEntity> listByPerson(String personId){
        ResponseInfo info = personOrgClient.listByPerson(personId);
        return TransReponseUtil.transFormToListClass(info,OrganizationPersonEntity.class);
    }

    public void updateById(OrganizationPersonEntity entity){
        personOrgClient.updateById(entity);
    }

    public void save(OrganizationPersonEntity entity){
        personOrgClient.save(entity);
    }
}
