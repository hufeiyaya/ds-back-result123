package com.daoshu.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 组织通讯录
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@EqualsAndHashCode(callSuper=false)
@Data@TableName("t_mds_o_concats")
public class OConcatsEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1441410048651131114L;

	/**
	 * ID
	 */
	@TableId(value = "id_")
	private String id;

	/** 组织ID	 */
    @TableField("organization_id_")
	private String organizationId;
	/**  通信方式类型*/
    @TableField("concat_type_")
	private Integer concatType;
	/**联系信息  */
    @TableField("concat_number_")
	private String concatNumber;
	/** 序号 */
    @TableField("sort_")
	private Integer sort;
	/** 有效标记 */
    @TableField("available_")
	private Integer available;
	/** 是否默认 */
    @TableField("default_")
	private Integer defaultValue;
	/** 是否删除 */
    @TableField("delete_")
	@TableLogic
	private Integer delete;

	@Override
	public String toString() {
		return "TMdsOConcats{" +
			"id=" + id +
			", organizationId=" + organizationId +
			", concatType=" + concatType +
			", concatNumber=" + concatNumber +
			", sort=" + sort +
			", available=" + available +
			", defaultValue=" + defaultValue +
			", delete=" + delete +
			"}";
	}
}
