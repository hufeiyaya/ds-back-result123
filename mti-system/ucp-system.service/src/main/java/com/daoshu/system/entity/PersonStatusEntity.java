package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@Entity
@TableName("t_pf_person_record_status")
@ToString
public class PersonStatusEntity implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3561046822112850817L;
	@TableId("id_")
    protected String id;
    @TableField(value = "account_id_")
    private String accountId;

    @TableField(value = "person_id_")
    private String personId;

    @TableField(value = "modify_time_")
    private Date modifyTime;

    @TableField(value = "client_type_")
    private String clientType;
}
