package com.daoshu.system.bo;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@ToString
@ApiModel(description = "组织人员实体类")
public class OrganizationPersonBo extends BaseBo implements Serializable {

    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -7601650306220643026L;

    private String id;

    private String orgId;

    private String personId;

    private String personName;
    
    private String positionName;

    private Integer mainPosition;

    private Integer leader;

    private Integer available;

    private Integer sort;

    private Integer positionSort;
}
