package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@ToString
@Entity
@ApiModel(description = "编码类型实体")
@TableName(value = "t_pf_code_type")
public class CodeTypeEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id_")
    private String id;

    @TableField(value = "name_")
    private String name;
    
    @TableField(value = "available_")
    private Integer available;
    
    @TableLogic
    @TableField(value = "delete_")
    private Integer delete;

}
