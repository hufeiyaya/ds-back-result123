package com.daoshu.system.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@Entity
@ToString
@TableName(value = "t_pf_person")
@ApiModel(description = "人员信息实体类")
public class PersonEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id_")
    private String id;

    @TableField(value = "number_")
    private String number;

    @TableField(value = "name_")
    private String name;

    @TableField(value = "police_code_")
    private String policeCode;

    @TableField(value = "credentials_type_")
    private Integer credentialsType;

    @TableField(value = "credentials_number_")
    private String credentialsNumber;

    @TableField(value = "sex_")
    private Integer sex;

    @TableField(value = "birth_date_")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date birthDate;

    @TableField(value = "address_")
    private String address;

    @TableField(value = "security_")
    private Integer security;

    @TableField(value = "version_")
    private String version;

    @TableField(value = "available_")
    private Integer available;

    @TableField(value = "phone_")
    private String phone;

    @TableField(value = "email_")
    private String email;

    @TableField(value = "creator_id_")
    private String creatorId;

    @TableField(value = "main_duty_")
    private String mainDuty;

    @TableField(value = "avator_")
    private String avator;
    @TableLogic
    @TableField(value = "delete_")
    private Integer delete;


    @TableField(exist = false)
    private AccountEntity accountEntity;

    @TableField(exist = false)
    private String orgName;

    /**
     * 组织机构表
     */
    @TableField(exist = false)
    private OrganizationEntity organizationEntity;
}
