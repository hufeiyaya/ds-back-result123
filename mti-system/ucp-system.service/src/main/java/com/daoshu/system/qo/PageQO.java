package com.daoshu.system.qo;

import com.daoshu.component.page.PageConstants;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
public class PageQO {

	private Integer start = 1;
	
	private Integer size = PageConstants.PAGE_NUM_SIZE;
}
