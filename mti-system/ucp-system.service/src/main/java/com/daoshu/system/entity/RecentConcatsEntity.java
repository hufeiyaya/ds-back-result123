package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 最近联系人
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@EqualsAndHashCode(callSuper=false)
@Data@TableName("t_mds_recent_concats")
public class RecentConcatsEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@TableId(value = "id_")
	private String id;

	/** 联系人ID */
    @TableField("person_id_")
	private String personId;
	/** 联系人姓名 */
    @TableField("person_name_")
	private String personName;
	/** 联系方式id */
    @TableField("concat_id_")
	private String concatId;
	/** 联系方式类型 */
    @TableField("concat_type_")
	private Integer concatType;
	/** 联系方式内容 */
    @TableField("concat_")
	private String concat;
	/** 联系时间 */
    @TableField("create_time_")
	private Date createTime;
	/** 创建人id */
    @TableField("create_id_")
	private String createId;

	/**
	 * 联系人类型
	 */
	@TableField("link_type_")
    private Integer linkType;

	/**
	 * 通话结束时间
	 */
	@TableField("end_time_")
    private Date endTime;

    /**
	 * 通话类型
	 */
    @TableField("connect_type_")
    private Integer connectType;

    /** 创建者号码 */
    @TableField("creater_content_")
	private String createrContent;

    /** 设备状态 */
    @TableField("device_status_")
	private Integer deviceStatus;

	/**
	 * 通讯类型:1.单呼 2.会议
	 */
	@TableField("call_type_")
    private Integer callType;

	@Override
	public String toString() {
		return "TMdsRecentConcats{" +
			"id=" + id +
			", personId=" + personId +
			", personName=" + personName +
			", concatId=" + concatId +
			", concatType=" + concatType +
			", concat=" + concat +
			", createTime=" + createTime +
			", createId=" + createId +
			"}";
	}
}
