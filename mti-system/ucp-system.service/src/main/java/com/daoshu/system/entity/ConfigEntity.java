package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@Entity
@ToString
@ApiModel(description = "系统配置实体类")
@TableName(value = "t_pf_config")
public class ConfigEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id_")
    private String id;

    @TableField(value = "code_")
    private String code;

    @TableField(value = "name_")
    private String name;

    @TableField(value = "description_")
    private String description;

    @TableField(value = "relation_dict_")
    private String relationDict;

    @TableField(value = "relation_dict_detail_")
    private String relationDictDetail;

    @TableField(value = "value_type_")
    private Integer valueType;

    @TableField(value = "value_")
    private String value;

    @TableField(value = "default_value_")
    private String defaultValue;

    @TableField(value = "current_value_")
    private String currentValue;

    @TableField(value = "required_")
    private Integer required;

    @TableField(value = "level_")
    private Integer level;

    @TableField(value = "sort_")
    private Integer sort;
}
