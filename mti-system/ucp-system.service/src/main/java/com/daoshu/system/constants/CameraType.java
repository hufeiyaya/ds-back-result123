package com.daoshu.system.constants;


/**
 * 通讯类型
 * @author zhaoyj
 *
 */
public enum CameraType {

	/** 正常 */
	NORMAL(1,""),
	/** 故障  */
	ERROR(0,"集群 ");
	/**
	 * 获取待办事项状态
	 * 
	 * @param status
	 * 
	 * @return
	 */
	public static CameraType getConcatType(int status){
		CameraType[] array = CameraType.values();
		for(CameraType item:array){
			if(item.status == status){
				return item;
			}
		}
		return null;
	}
	
	CameraType(int status, String title){
		this.status = status;
		this.title = title;
	}
	/** 类型 */
	private int status;
	/** 类型标题 */
	private String title;
	/**
	 * 获取类成员type
	 * @return {@link #type}
	 */
	public int getStatus() {
		return this.status;
	}
	/**
	 * 获取类成员title
	 * @return {@link #title}
	 */
	public String getTitle() {
		return this.title;
	}
}
