package com.daoshu.system.qo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper=false)
@Data
public class PersonQO extends PageQO{

	private String personId;
	
	private String orgid;
	/* 是否领导 */
	private Integer isLeader;
	/**
	 * 人员ID列表
	 */
	private List<String> personIds;
	
	private String name;
	/** 警号 */
	private String policeCode;
}
