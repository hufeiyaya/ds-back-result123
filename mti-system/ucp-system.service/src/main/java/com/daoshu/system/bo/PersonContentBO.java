package com.daoshu.system.bo;

import com.daoshu.system.entity.PersonEntity;
import lombok.Getter;
import lombok.Setter;
/**
 * @ClassName PersonContentBO
 * @Description TODO
 * @Author zhaoyj
 * @Date 2019/4/10 15:37
 */
@Getter
@Setter

public class PersonContentBO extends PersonEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 单位ID
     */
    private String orgId;
    /**
     * 单位名称
     */
    private String orgName;
    private String positionName;
    private String mainPosition;
    private Integer leader;
    private Integer positionSort;
    /** 常用联系ID */
    private String generalId;

    private Object concats;
    /** 默认联系内容 */
    private String defaultConcat;

    private Integer defaultConcatType;

    private String personId;
    /**
     * 类型
     */
    private Integer type;
    private String typeName;
    /**
     * 是否常用联系人
     *
     */
    private Boolean isGeneralconcat;

}
