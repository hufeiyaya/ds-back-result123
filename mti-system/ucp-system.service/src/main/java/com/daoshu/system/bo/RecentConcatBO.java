package com.daoshu.system.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RecentConcatBO extends ContentBO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 联系方式
	 */
	private String concat;
	
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;
	/** 通话时长 */
	private String connectTimeString;
	/** 通话类型名称 */
	private Integer connectType;

	/** 通话类型名称 */
	private String connectTypeName;

	/**
	 * 通讯类型:1.单呼 2.会议
	 */
	private Integer callType;

	/**
	 * 通讯类型名称
	 */
	private String callTypeName;

}
