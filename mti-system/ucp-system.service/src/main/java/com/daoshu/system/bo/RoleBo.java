package com.daoshu.system.bo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@ApiModel(description = "角色信息实体类")
@ToString
public class RoleBo extends BaseBo implements Serializable {

    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 8372142672672335247L;

	private String id;

    private String name;

    private String menuIds;
    
    /**
     * 角色类型
     */
    private Integer roleType;

    private String clientType;
    
    private List<String> orgIds;

    private String creatorId;

    private Date createTime;
}
