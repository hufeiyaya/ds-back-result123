package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_sys_dict")
public class SysDictEntity implements Serializable {
    /**
     * 序列化id
     */
    private static final long serialVersionUID = -1142114169896502387L;
    /**
     * 流水编号
     */
    @TableId("id_")
    private String id;

    @TableField("key_")
    private String key;

    @TableField("value_")
    private String value;

    /**
     * 数据字典类型
     *      重点人员分类	kp_category
     *     重点人员负责部门	kp_department
     *     重点时期	kp_period
     *     重点人员管控状态	kp_control_state
     *     重点人员管控手段	kp_control_type
     *     重点人员级别	kp_level
     *     重点人员报警方式	kp_alarm_mode
     *     辖区	kp_area
     */
    @TableField("type_")
    private String type;

    @TableField("sort_")
    private Integer sort;

    @TableField("description_")
    private String description;

    @TableField("delete_")
    private String delete;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time_")
    private Timestamp createTime;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("update_time_")
    private Timestamp updateTime;


    /**
     * 小类
     */
    @TableField(exist = false)
    private List<SysDictEntity> children;

    /**
     * 大类编码
     */
    @TableField(exist = false)
    private String dlCode;

    /**
     * 大类名称
     */
    @TableField(exist = false)
    private String dlName;

    /**
     * 小类类名称
     */
    @TableField(exist = false)
    private String xlName;

    /**
     * 警种
     */
    @TableField(exist = false)
    private String policeName;


}
