package com.daoshu.system.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * Created by admin on 2020/11/24.
 */
@Data
@Accessors(chain = true)
public class TKmZdryType implements Serializable {

    /**
     * 大类名称（旧）
     */
    private String rylb;

    /**
     * 大类编码（旧）
     */
    private String rylbCode;


    /**
     * 大类名称（新）
     */
    private String rylbNew;

    /**
     * 大类编码（新）
     */
    private String rylbCodeNew;

    /**
     * 责任部门--对应警种（旧）
     */
    private String zrbm;

    /**
     * 责任部门--对应警种（新）
     */
    private String zrbmNew;

    /**
     * 小类（旧）
     */
    private String xl;

    /**
     * 小类编码（旧）
     */
    private String xlCode;

    /**
     * 小类（新）
     */
    private String xlNew;

    /**
     * 小类编码（新）
     */
    private String xlCodeNew;



}
