package com.daoshu.system.bo;

import com.daoshu.resource.bo.AgentBo;
import com.daoshu.system.entity.MenuEntity;
import com.daoshu.system.entity.RoleEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountBo extends BaseBo implements Serializable {

    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 1690891663910437828L;

	private String id;

    private String loginName;

    private String password;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    
    private String creatorId;

    private String personId;
    
    private String description;

    private String orgId;

    private Integer available;

    private Integer statu;
    /**
     * 人员信息
     */
    private PersonBo personBo;
    
    /**
     * 组织信息
     */
    private OrganizationBo organizationBo;
    
    /**
     * 角色权限(包含临时权限)
     */
    private List<MenuEntity> menus;
    
    /**
     * 菜单IDS
     */
    private List<String> menuIds;
    
    /**
     * 角色IDS
     */
    private List<String> roleIds;
    
    /**
     * token
     */
    private String token;
    
    /**
     * 坐席信息
     */
    private AgentBo agentBo;

    /**
     * 客户端类型
     * web/android ...
     */
    private String clientType;

    /**
     * 角色IDS
     */
    private List<RoleEntity> roleList;

    /**
     * new password
     */
    private String newPassword;
}
