package com.daoshu.system.bo;

import com.alibaba.fastjson.JSONObject;

import lombok.Data;

@Data
public class ConsumerBO {

	
	private Integer msgId;
	
	/** 登陆用户（分机号） */
	private String userName;
	
	private String commId;
	
	private AccountBo accountBo;
	
	private JSONObject msgContent;
	/** 1，结果事件；2。状态事件 */
	private Integer messageType;
}
