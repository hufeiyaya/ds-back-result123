package com.daoshu.system.bo;

import com.daoshu.system.entity.OConcatsEntity;
import com.daoshu.system.entity.PConcatsEntity;
import com.daoshu.system.entity.PersonEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ContentBO extends PersonEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 单位ID
     */
    private String orgId;
    /**
     * 单位名称
     */
    private String orgName;
    private String positionName;
    private String mainPosition;
    private Integer leader;
    private Integer positionSort;
    /** 常用联系ID */
    private String generalId;
    /**
     * 联系方式列表
     */
    private List<PConcatsEntity> personConcats;

    private List<OConcatsEntity> orgConcats;

    private Object concats;
    /** 默认联系内容 */
    private String defaultConcat;

    private Integer defaultConcatType;

    private String personId;
    /**
     * 类型
     */
    private Integer type;
    private String typeName;
    /**
     * 是否常用联系人
     *
     */
    private Boolean isGeneralconcat;
    /** 终端类型 */
    private String clientType;
    /** 终端类型名称 */
    private String clientTypeName;

}
