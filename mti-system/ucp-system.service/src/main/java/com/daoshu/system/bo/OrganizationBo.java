package com.daoshu.system.bo;

import java.io.Serializable;
import java.util.List;

import com.daoshu.system.entity.OrganizationEntity;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ApiModel(description = "组织机构实体类")
@ToString
public class OrganizationBo extends BaseBo implements Serializable {

	/**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -5533205318805112527L;

    private String id;

    private String number;

    private String code;

    private Integer type;

    private String name;

    private String shortName;

    private Integer sort;

    private String description;

    private String regionId;

    private String regionPid;

    private String businessPid;

    private String address;

    private String zipCode;

    private String concatPhone;

    private String firstManager;

    private String secondManager;

    private String version;

    /**
     * 所属区域
     */
    private String qu;
    
    /**
     * 1 内部组织
     * 0 外部组织
     */
    private Integer internal = 0;

    private Integer available;
    
    private List<OrganizationEntity> children;
}
