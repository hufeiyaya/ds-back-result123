package com.daoshu.system.service;

import com.alibaba.fastjson.JSON;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.resource.entity.AgentEntity;
import com.daoshu.system.service.client.AgentClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName AgentFeignService
 * @Description AgentFeignService
 * @Author zhaoyj
 * @Date 2019/4/10 11:57
 */
@Service
public class AgentFeignService {

    @Autowired
    private AgentClient agentService;

    public AgentEntity getByIP(String ip) {
		ResponseInfo info = agentService.getByIP(ip);
		Object result = info.getData();
		if(result!=null) {
			return JSON.parseObject(JSON.toJSONString(result), AgentEntity.class);
		}
        return null;
    }

    public AgentEntity getByExt(String ext){
        ResponseInfo info = agentService.getByExt(ext);
        return JSON.parseObject(JSON.toJSONString(info.getData()), AgentEntity.class);
    }
}
