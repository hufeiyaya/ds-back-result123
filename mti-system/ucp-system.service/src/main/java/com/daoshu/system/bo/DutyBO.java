package com.daoshu.system.bo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DutyBO extends OrganizationBo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String dutyPersonName;
	
	private String dutyTel;

}
