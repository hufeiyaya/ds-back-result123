package com.daoshu.system.bo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author hf
 * @since 2020-11-16
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "授权日志", description = "授权日志")
public class AuthLogBo implements Serializable {

    private static final long serialVersionUID = 1690891663910437828L;

    @ApiModelProperty(value = "页大小", required = false)
    private Integer size;

    @ApiParam(value = "页数", name = "page", defaultValue = "1")
    @ApiModelProperty(value = "页数")
    private Integer page;

    @ApiParam(value = "授权角色", name = "authRole")
    private List<String> authRole;

    @ApiParam(value = "搜索条件", name = "searchTerm")
    private String searchTerm;

}
