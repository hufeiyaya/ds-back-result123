package com.daoshu.system.qo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
public class RecentConcatsQO extends PageQO{

	private String personId;
	/** 是否合并联系方式查询 */
	private Boolean group = false;
	
}
