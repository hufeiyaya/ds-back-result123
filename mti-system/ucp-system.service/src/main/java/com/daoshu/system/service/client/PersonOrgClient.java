package com.daoshu.system.service.client;

import com.daoshu.component.response.ResponseInfo;
import com.daoshu.system.entity.OrganizationPersonEntity;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @ClassName PersonOrgClient
 * @Description PersonOrgClient
 * @Author zhaoyj
 * @Date 2019/4/8 13:58
 */
@FeignClient(name = "UCP-SYSTEM")
public interface PersonOrgClient {

    @RequestMapping(value = "/organization/person/params",method = RequestMethod.POST)
    ResponseInfo getOne(@RequestBody OrganizationPersonEntity entity);

    @RequestMapping(value = "/organization/person/listByOrgIds",method = RequestMethod.POST)
    ResponseInfo listByOrgIds(@RequestBody List<String> ids);

    @RequestMapping(value = "/organization/person/listByPerson",method = RequestMethod.GET)
    ResponseInfo listByPerson(@RequestParam("personId") String personId);

    @RequestMapping(value = "/organization/person",method = RequestMethod.PUT)
    ResponseInfo updateById(@RequestBody OrganizationPersonEntity entity);
    @RequestMapping(value = "/organization/person/save",method = RequestMethod.POST)
    ResponseInfo save(@RequestBody OrganizationPersonEntity entity);
}
