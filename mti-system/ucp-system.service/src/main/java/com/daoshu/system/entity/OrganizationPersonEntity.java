package com.daoshu.system.entity;

import java.io.Serializable;

import javax.persistence.Entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@ToString
@Entity
@ApiModel(description = "组织人员实体类")
@TableName(value = "t_pf_organization_person")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrganizationPersonEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id_")
    private String id;

    @TableField(value = "org_id_")
    private String orgId;

    @TableField(value = "person_id_")
    private String personId;
    
    @TableField(value = "person_name_")
    private String personName;

    @TableField(value = "position_name_")
    private String positionName;

    @TableField(value = "main_position_")
    private Integer mainPosition;

    @TableField(value = "leader_")
    private Integer leader;

    @TableField(value = "available_")
    private Integer available;

    @TableField(value = "sort_")
    private Integer sort;

    @TableField(value = "position_sort_")
    private Integer positionSort;

    @TableLogic
    @TableField(value = "delete_")
    private Integer delete;
}
