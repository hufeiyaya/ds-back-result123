package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@ToString
@Entity
@ApiModel(description = "账户实体类")
@TableName(value = "t_pf_account")
public class AccountEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id_")
    private String id;

    @TableField(value = "login_name_")
    private String loginName;

    @TableField(value = "password_")
    private String password;

    @TableField(value = "create_time_")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @TableField(value = "creator_id_")
    private String creatorId;

    @TableField(value = "person_id_")
    private String personId;
    
    @TableField(exist = false)
    private PersonEntity person;

    @TableField(value = "description_")
    private String description;

    @TableField(value = "org_id_")
    private String orgId;

    @TableField(value = "available_")
    private Integer available;

    @TableField(value = "statu_")
    private Integer statu;

    @TableLogic
    @TableField(value = "delete_")
    private Integer delete;

    /**
     * 角色信息
     */
    @TableField(exist = false)
    private RoleEntity roleEntity;
}
