package com.daoshu.system.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@Entity
@ApiModel(description = "组织机构实体类")
@ToString
@TableName(value = "t_pf_organization")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrganizationEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id_")
    private String id;

    @TableField(value = "number_")
    private String number;

    @TableField(value = "code_")
    private String code;

    @TableField(value = "type_")
    private Integer type;

    @TableField(value = "name_")
    private String name;

    @TableField(value = "short_name_")
    private String shortName;

    @TableField(value = "sort_")
    private Integer sort;

    @TableField(value = "description_")
    private String description;

    @TableField(value = "region_id_")
    private String regionId;

    @TableField(value = "region_pid_")
    private String regionPid;

    @TableField(value = "business_pid_")
    private String businessPid;

    @TableField(value = "address_")
    private String address;

    @TableField(value = "zip_code_")
    private String zipCode;

    @TableField(value = "concat_phone_")
    private String concatPhone;

    @TableField(value = "first_manager_")
    private String firstManager;

    @TableField(value = "second_manager_")
    private String secondManager;

    @TableField(value = "version_")
    private String version;

    @TableField(value = "available_")
    private Integer available;

    @TableField(value = "creator_id_")
    private String creatorId;
    
    @TableField(value = "internal_")
    private Integer internal;

    @TableLogic
    @TableField(value = "delete_")
    private Integer delete;

    
    @TableField(value = "org_type_")
    private Integer orgType;

    @TableField(value = "org_sort_")
    private String orgSort;
    /**
     * 所属区域
     */
    @TableField(value = "qu_")
    private String qu;
    
    @TableField(exist = false)
    private List<OrganizationEntity> children;
    
    /**
     * 组织下人员数量
     */
    @TableField(exist = false)
    private int personCount = 0;
    
    /**
     * 是否包含外设机构
     */
    @TableField(exist = false)
    private boolean existExternal = false;
    
    /**
     * 是否含有内设机构
     */
    @TableField(exist = false)
    private boolean existInternal = false;
}
