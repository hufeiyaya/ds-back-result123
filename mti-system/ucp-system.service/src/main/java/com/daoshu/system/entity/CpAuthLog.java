package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author zhangmingxin
 */
@Data
@ApiModel(value = "授权信息日志")
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "cp_auth_log")
public class CpAuthLog implements Serializable {
    private static final long serialVersionUID = 2424149135140927843L;
    @ApiModelProperty(value = "id")
    @TableId
    private String id;
    @ApiModelProperty(value = "警号")
    @TableField("number")
    private String number;
    @ApiModelProperty(value = "姓名")
    @TableField("name")
    private String name;
    @ApiModelProperty(value = "ip")
    @TableField("ip")
    private String ip;
    @ApiModelProperty(value = "组织单位")
    @TableField("org")
    private String org;
    @ApiModelProperty(value = "授权类型(1：设置角色，0：删除角色)")
    @TableField("auth_type")
    private String authType;
    @ApiModelProperty(value = "授权角色")
    @TableField("auth_role")
    private String authRole;
    @ApiModelProperty(value = "授权角色名称")
    @TableField(exist = false)
    private String authRoleName;
    @ApiModelProperty(value = "授权人")
    @TableField("auth_person")
    private String authPerson;
    @ApiModelProperty(value = "授权人姓名")
    @TableField(exist = false)
    private String authPersonName;
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-mm-dd HH:MM:SS")
    private LocalDateTime createTime;
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-mm-dd HH:MM:SS")
    private String authDateTime;
    @ApiModelProperty(value = "组织名称")
    @TableField("org_id")
    private String orgId;
}
