package com.daoshu.system.bo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@ToString
public class ConfigBo extends BaseBo implements Serializable {

    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 5552222813114866313L;

    private String id;

    private String code;

    private String name;

    private String description;

    private String relationDict;

    private String relationDictDetail;

    private Integer valueType;

    private String value;

    private String defaultValue;

    private String currentValue;

    private Integer required;

    private Integer level;

    private Integer sort;
}
