package com.daoshu.system.bo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class BaseBo {

	private int start = 1;
	
	private int size = 10;
	
	private int total;
}
