package com.daoshu.system.service.client;

import com.daoshu.component.response.ResponseInfo;
import com.daoshu.system.entity.OrganizationEntity;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName OrgClient
 * @Description OrgClient
 * @Author zhaoyj
 * @Date 2019/4/8 13:56
 */
@FeignClient(name = "UCP-SYSTEM")
public interface OrgClient {

    @RequestMapping(value = "/organization/{id}",method = RequestMethod.GET)
    ResponseInfo getById(@PathVariable("id") String id);
    @RequestMapping(value = "/organization/save",method = RequestMethod.POST)
    ResponseInfo save(@RequestBody OrganizationEntity entity);
    @RequestMapping(value = "/organization/update",method = RequestMethod.POST)
    ResponseInfo updateById(@RequestBody OrganizationEntity entity);
    @RequestMapping(value = "/organization/listByIds",method = RequestMethod.POST)
    ResponseInfo listByIds(@RequestBody List<String> ids);
    @RequestMapping(value = "/organization/allChild",method = RequestMethod.GET)
    ResponseInfo getAllChild(@RequestParam("pIds") String pIds, @RequestParam("containsAll") boolean containsAll, @RequestParam("isBusiness") boolean isBusiness);
    @RequestMapping(value = "/organization/listAll",method = RequestMethod.GET)
    ResponseInfo listAll();
}
