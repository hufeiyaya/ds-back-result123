package com.daoshu.system.qo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 常用联系人查询实体
 * @author zhaoyj
 *
 */
@EqualsAndHashCode(callSuper=false)
@Data
public class GeneralConcatsQO extends PageQO{

	private String personId;
	
	private String organizationId;
}
