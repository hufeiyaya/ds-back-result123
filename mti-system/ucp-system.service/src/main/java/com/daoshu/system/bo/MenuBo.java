package com.daoshu.system.bo;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@ApiModel(description = "权限实体BO类")
@ToString
public class MenuBo extends BaseBo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String name;

    private String code;

    private String description;

    private Integer sort;

    private String path;

    private String clazz;

    private Integer type;
    
    private List<MenuBo> children; 
}
