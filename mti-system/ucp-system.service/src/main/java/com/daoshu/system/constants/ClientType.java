package com.daoshu.system.constants;

/**
 * 终端类型
 * @author zhaoyj
 *
 */
public enum ClientType {

	/** 未知 */
	UNKNOW_CLIENT("0","UNKNOW"),
	/** 手机 */
	PHONE_CLIENT("1","PHONE"),
	/** 坐席  */
	AGENT_CLIENT("2","AGENT"),
	/** 集群 */
	CLUSTER_CLIENT("3","CLUSTER");
	/**
	 * 获取终端类型
	 * 
	 * @param status
	 * 
	 * @return
	 */
	public static ClientType getClientType(String status){
		ClientType[] array = ClientType.values();
		for(ClientType item:array){
			if(item.status.equals(status)){
				return item;
			}
		}
		return ClientType.UNKNOW_CLIENT;
	}
	
	ClientType(String status, String title){
		this.status = status;
		this.title = title;
	}
	/** 类型 */
	private String status;
	/** 类型标题 */
	private String title;
	/**
	 * 获取类成员type
	 * @return {@link #type}
	 */
	public String getStatus() {
		return this.status;
	}
	/**
	 * 获取类成员title
	 * @return {@link #title}
	 */
	public String getTitle() {
		return this.title;
	}
}
