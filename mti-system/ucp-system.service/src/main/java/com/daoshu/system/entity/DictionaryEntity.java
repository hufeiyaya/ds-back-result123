package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: mds.pom
 * @description: 字典信息表
 * @author: Williams Guo
 * @create: 2018-12-12 11:38
 */
@Data
@TableName("t_pf_dicts")
public class DictionaryEntity implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4736974443073619381L;
	@TableId(value = "id_")
    int id;
    @TableField(value = "key_")
    String key;
    @TableField(value = "value_")
    String value;
    @TableField(value = "type_")
    String type;
}