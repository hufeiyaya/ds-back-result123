package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 人员通讯录
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@EqualsAndHashCode(callSuper=false)
@Data@TableName("t_mds_p_concats")
public class PConcatsEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId(value = "id_")
	private String id;

	/** 人员ID */
    @TableField("person_id_")
	private String personId;
	/** 通信方式类型 */
    @TableField("concat_type_")
	private Integer concatType;
	/** 联系信息 */
    @TableField("concat_number_")
	private String concatNumber;
	/** 序号 */
    @TableField("sort_")
	private Integer sort;
	/** 有效标记 */
    @TableField("available_")
	private Integer available;
	/** 默认通讯方式 */
    @TableField("default_")
	private Integer defaultValue;
	/** 是否删除 */
	@TableLogic
	 @TableField("delete_")
	private Integer delete;


	@Override
	public String toString() {
		return "TMdsPConcats{" +
			"id=" + id +
			", personId=" + personId +
			", concatType=" + concatType +
			", concatNumber=" + concatNumber +
			", sort=" + sort +
			", available=" + available +
			", defaultValue=" + defaultValue +
			", delete=" + delete +
			"}";
	}
}
