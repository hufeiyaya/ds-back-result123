package com.daoshu.system.service.client;

import com.daoshu.component.response.ResponseInfo;
import com.daoshu.system.entity.PersonEntity;
import com.daoshu.system.qo.PersonQO;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName PersonClient
 * @Description PersonClient
 * @Author zhaoyj
 * @Date 2019/4/8 13:57
 */
@FeignClient(name = "UCP-SYSTEM")
public interface PersonClient {

    /**
     * 获取人员列表
     * @param ids
     * @return
     */
    @RequestMapping(value="/person/list",method= RequestMethod.POST)
    ResponseInfo getByIds(@RequestBody List<String> ids);
    /**
     * 获取人员列表
     * @param qo
     * @return
     */
    @RequestMapping(value="/person/detaillPage",method= RequestMethod.POST)
    ResponseInfo queryPersonPage(@RequestBody PersonQO qo);

    /**
     * 获取人员列表
     * @param id
     * @return
     */
    @RequestMapping(value="/person/{id}",method= RequestMethod.GET)
    ResponseInfo getById(@PathVariable("id") String id);

    @RequestMapping(value="/person/save",method= RequestMethod.POST)
    ResponseInfo save(@RequestBody PersonEntity entity);
}
