package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@Entity
@TableName(value = "t_pf_role")
@ApiModel(description = "角色信息实体类")
@ToString
public class RoleEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id_")
    private String id;

    @TableField(value = "name_")
    private String name;
    
    /**
     * 角色类型
     * 0 所有部门数据权限
     * 1 本部门以及本部门下属机构数据权限
     * 2 本部门数据权限
     * 3 自定义数据权限
     */
    @TableField(value = "role_type_")
    private Integer roleType;

    @TableField(value = "client_type_")
    private String clientType;
    
    @TableField(exist = false)
    private List<String> orgIds;

    @TableField(value = "menu_ids_")
    private String menuIds;

    @TableField(value = "creator_id_")
    private String creatorId;

    @TableField(value = "create_time_")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
