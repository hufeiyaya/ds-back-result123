package com.daoshu.system.service.client;


import com.daoshu.component.response.ResponseInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "UCP-SYSTEM")
public interface PersonStatusClient {

    /**
     * 获取人员列表
     * @param ids
     * @return
     */
    @RequestMapping(value="/person/status",method= RequestMethod.POST)
    ResponseInfo getPersonStatus(@RequestBody(required = false) List<String> ids);
}
