package com.daoshu.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Setter
@Getter
@ApiModel(description = "权限实体类")
@Entity
@ToString
@TableName(value = "t_pf_menu")
public class MenuEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id_")
    private String id;

    @TableField(value = "name_")
    private String name;

    @TableField(value = "code_")
    private String code;

    @TableField(value = "description_")
    private String description;

    @TableField(value = "sort_")
    private Integer sort;

    @TableField(value = "path_")
    private String path;

    @TableField(value = "class_")
    private String clazz;

    @TableField(value = "type_")
    private Integer type;
}
