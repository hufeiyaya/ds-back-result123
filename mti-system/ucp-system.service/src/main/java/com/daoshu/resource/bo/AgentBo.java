package com.daoshu.resource.bo;

import com.daoshu.system.bo.BaseBo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 坐席信息
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Setter
@Getter
@ToString
public class AgentBo extends BaseBo {

    /** 编号 */
    private String number;
    /** IP地址 */
    private String ipAddress;
    /** 类型 */
    private Integer type;
    /** 分机号 */
    private String ext;
    /** 状态 */
    private Integer status;
    /** 可用标记 */
    private Integer available;
    /** 登陆人ID */
    private String personId;
}
