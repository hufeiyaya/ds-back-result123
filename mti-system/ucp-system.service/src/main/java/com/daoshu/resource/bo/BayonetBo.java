package com.daoshu.resource.bo;

import com.daoshu.system.bo.BaseBo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 卡口信息
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Getter
@Setter
@ToString
public class BayonetBo extends BaseBo {

    /** 卡口编号 */
    private String number;
    /** 卡口名称 */
    private String name;
    /** 卡口类型 */
    private Integer type;
    /** 卡口所属区域 */
    private String region;
    /** 卡口所属机构 */
    private String organizationId;
    /** X坐标（卡口位置） */
    private String latitude;
    /** Y坐标（卡口位置） */
    private String longitude;
    /** 状态 */
    private Integer status;
    /** 有效标志 */
    private Integer available;
}
