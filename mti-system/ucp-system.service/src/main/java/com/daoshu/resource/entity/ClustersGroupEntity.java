package com.daoshu.resource.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;

/**
 * 集群组
 * @ClassName: ClustersGroupEntity
 * @Description: ClustersGroupEntity
 * @author: Meifeng Tian
 * @date: 2018年10月19日 上午10:49:08
 */
@TableName(value = "t_pf_clusters_group")
@Setter
@Getter
@ToString
@Entity
public class ClustersGroupEntity implements Serializable {

	/**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -4660162997969375708L;

	@TableId("id_")
	protected String id;

	/**
	 * 集群组号
	 */
	@TableField(value = "number_")
	private String number;
	
	@TableField(value = "name_")
	private String name;
	
	@TableField(value = "org_id_")
	private String orgId;
	
	/**
	 * 机型
	 */
	@TableField(value = "type_")
	private String type;
	
	/**
	 * 频率
	 */
	@TableField(value = "frequency_")
	private String frequency;
	
	/**
	 * 总数
	 */
	@TableField(exist = false)
	private Integer count;
	
	@TableField(value = "delete_")
	private Integer delete;
	
	@TableField(value = "available_")
	private Integer available;
	
	@TableField(exist = false)
	private List<ClustersClientEntity> children;
}
