package com.daoshu.resource.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * <p>
 * 集群终端
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Getter
@Setter
@ToString
@Entity
@TableName("t_pf_clusters_client")
public class ClustersClientEntity implements Serializable {

	/**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 8495047383196883497L;

	@TableId("id_")
	protected String id;
	
	/** 终端编号 */
    @TableField("number_")
	private String number;
	/** 终端所属机构 */
	 @TableField("organization_id_")
	private String organizationId;
	/** 终端类型 */
	 @TableField("type_")
	private Integer type;
	/** X坐标（当前位置） */
	 @TableField("latitude_")
	private String latitude;
	/** Y坐标（当前位置） */
	 @TableField("longitude_")
	private String longitude;
	/** 终端状态 */
	 @TableField("available_")
	private Integer available;
	/** 删除标记 */
    @TableLogic
    @TableField("delete_")
	private Integer delete;
}
