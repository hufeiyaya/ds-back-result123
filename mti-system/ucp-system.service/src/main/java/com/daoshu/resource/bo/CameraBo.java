package com.daoshu.resource.bo;

import com.daoshu.system.bo.BaseBo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 摄像机信息
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Setter
@Getter
@ToString
public class CameraBo extends BaseBo {

    /** 设备编号 */
    private String number;
    /**设备名称  */
    private String name;
    /** 设备类型 */
    private Integer type;
    /** 设备所属区域 */
    private String region;
    /** 设备所属机构 */
    private String organizationId;
    /** X坐标 */
    private String latitude;
    /** Y坐标 */
    private String longitude;
    /** 状态 */
    private Integer status;
    /** 有效标志 */
    private Integer available;
}
