package com.daoshu.resource.bo;

import com.daoshu.system.bo.BaseBo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ClustersGroupBo extends BaseBo {

    /**
     * 集群组号
     */
    private String number;

    private String name;

    private String orgId;

    /**
     * 机型
     */
    private String type;

    /**
     * 频率
     */
    private String frequency;
}
