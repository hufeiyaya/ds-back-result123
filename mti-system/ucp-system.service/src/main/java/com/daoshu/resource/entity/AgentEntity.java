package com.daoshu.resource.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * <p>
 * 坐席信息
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Getter
@Setter
@Entity
@ToString
@TableName("t_pf_agent")
public class AgentEntity implements Serializable {

	/**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -2639472720875671L;

	@TableId("id_")
	protected String id;

	/** 编号 */
    @TableField("number_")
	private String number;
	/** IP地址 */
    @TableField("ip_address_")
	private String ipAddress;
	/** 类型 */
    @TableField("type_")
	private Integer type;
	/** 分机号 */
    @TableField("ext_")
	private String ext;
	/** 状态 */
    @TableField("status_")
	private Integer status;
	/** 可用标记 */
    @TableField("available_")
	private Integer available;
	/** 删除标记 */
    @TableLogic
    @TableField("delete_")
	private Integer delete;
    /** 登陆人ID */
    @TableField("person_id_")
	private String personId;
}
