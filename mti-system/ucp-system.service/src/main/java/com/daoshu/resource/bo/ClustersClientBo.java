package com.daoshu.resource.bo;

import com.daoshu.system.bo.BaseBo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 集群终端
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Setter
@Getter
@ToString
public class ClustersClientBo extends BaseBo {

    /** 终端编号 */
    private String number;
    /** 终端所属机构 */
    private String organizationId;
    /** 终端类型 */
    private Integer type;
    /** X坐标（当前位置） */
    private String latitude;
    /** Y坐标（当前位置） */
    private String longitude;
    /** 终端状态 */
    private Integer available;
}
