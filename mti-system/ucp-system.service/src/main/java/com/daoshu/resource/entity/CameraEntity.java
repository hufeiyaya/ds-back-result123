package com.daoshu.resource.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;

/**
 * <p>
 * 摄像机信息
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Getter
@Setter
@Entity
@ToString
@TableName("t_pf_camera")
public class CameraEntity {

	@TableId("id_")
	protected String id;

	/** 设备编号 */
	@TableField("number_")
	private String number;
	/**设备名称  */
	@TableField("name_")
	private String name;
	/** 路径 **/
	@TableField("path_")
	private String path;
	
	/** 设备类型 */
	@TableField("type_")
	private Integer type;
	/** 设备所属区域 */
	@TableField("region_")
	private String region;
	/** 设备所属机构 */
	@TableField("organization_id_")
	private String organizationId;
	/** X坐标 */
	@TableField("latitude_")
	private String latitude;
	/** Y坐标 */
	@TableField("longitude_")
	private String longitude;
	/** 状态 */
	@TableField("status_")
	private Integer status;
	/** 有效标志 */
	@TableField("available_")
	private Integer available;
	/** 删除标志 */
    @TableLogic
    @TableField("delete_")
	private Integer delete;
    @TableField("address_")
    private String address;
    /** 描述 */
    @TableField("describe_")
    private String describe;
}
