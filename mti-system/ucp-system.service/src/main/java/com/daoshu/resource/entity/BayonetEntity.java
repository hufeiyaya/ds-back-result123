package com.daoshu.resource.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * <p>
 * 卡口信息
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Getter
@Setter
@Entity
@ToString
@TableName("t_pf_bayonet")
public class BayonetEntity implements Serializable {

	/**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 6191425810991171327L;

	@TableId("id_")
	protected String id;

	/** 卡口编号 */
	 @TableField("number_")
	private String number;
	/** 卡口名称 */
	 @TableField("name_")
	private String name;
	/** 卡口类型 */
	 @TableField("type_")
	private Integer type;
	/** 卡口所属区域 */
	 @TableField("region_")
	private String region;
	/** 卡口所属机构 */
	 @TableField("organization_id_")
	private String organizationId;
	/** X坐标（卡口位置） */
	 @TableField("latitude_")
	private String latitude;
	/** Y坐标（卡口位置） */
	 @TableField("longitude_")
	private String longitude;
	/** 状态 */
	 @TableField("status_")
	private Integer status;
	/** 有效标志 */
	 @TableField("available_")
	private Integer available;
	/** 删除标志 */
    @TableLogic
    @TableField("delete_")
	private Integer delete;
}
