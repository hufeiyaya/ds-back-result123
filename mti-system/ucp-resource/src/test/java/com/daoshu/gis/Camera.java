/*package com.daoshu.gis;

import org.apache.commons.lang3.StringUtils;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.DefaultFeatureCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.daoshu.gis.model.TopoRelTypeEnum;
import com.daoshu.gis.service.BufferService;
import com.daoshu.gis.service.GeoJsonService;
import com.daoshu.gis.service.TopoService;
import com.vividsolutions.jts.geom.Geometry;

public class Camera {
	@Autowired private GeoJsonService geojsonService;
	@Autowired private BufferService bufferService;
	@Autowired private TopoService topoService;
	
    public JSONObject queryCamera(Double longgitude, Double lat, Double distance){
		try {

			String[] tnames = {"T_PF_CAMERA"};
			//String geo ="{\"type\":\"Polygon\",\"coordinates\":[[121.38313293457031,31.29967486999304],[121.57951354980467,31.285005920036724],[121.45042419433592,31.12584816072004],[121.38313293457031,31.29967486999304]]}";
			// String geo ="{\"type\":\"Point\",\"coordinates\":[106.6, 29.6]}";
			JSONObject obj = new JSONObject();
			obj.put("type", "Point");
			JSONArray array = new JSONArray();
			array.add(longgitude);
			array.add(lat);
			obj.put("coordinates", array);
			String geo = obj.toJSONString();
			Double buffDic =distance;
			String unit = "meter";
			Geometry geom = geojsonService.geoJson2Geom(geo);
			Geometry ggg = bufferService.buffer(geom, buffDic, unit);
			DefaultFeatureCollection ftList = new DefaultFeatureCollection();
			SimpleFeatureCollection fts = null;
			for (String tname : tnames) {
				fts = topoService.topoQuery(ggg, tname, TopoRelTypeEnum.intersect);
				ftList.addAll(fts);
			}
			return covertToJSONObject(geojsonService.featureCol2GeoJson(ftList));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
   }
    private JSONObject covertToJSONObject(String jsonString) {
    	if(StringUtils.isBlank(jsonString))
    		return null;
    	return JSONObject.parseObject(jsonString);
    }
}
*/