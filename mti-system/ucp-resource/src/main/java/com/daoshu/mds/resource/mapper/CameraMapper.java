package com.daoshu.mds.resource.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.resource.entity.CameraEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface CameraMapper extends BaseMapper<CameraEntity> {

	List<CameraEntity> queryCamera(@Param("longgitude") Double longgitude,@Param("lat") Double lat,@Param("distance") Double distance);
}