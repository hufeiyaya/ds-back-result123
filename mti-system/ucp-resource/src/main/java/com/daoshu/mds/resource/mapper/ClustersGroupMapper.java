package com.daoshu.mds.resource.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.resource.entity.ClustersGroupEntity;

public interface ClustersGroupMapper extends BaseMapper<ClustersGroupEntity> {

}