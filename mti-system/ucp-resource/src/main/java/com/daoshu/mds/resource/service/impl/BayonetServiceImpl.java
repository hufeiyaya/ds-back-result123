package com.daoshu.mds.resource.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.resource.mapper.BayonetMapper;
import com.daoshu.mds.resource.service.IBayonetService;
import com.daoshu.resource.entity.BayonetEntity;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Service
public class BayonetServiceImpl extends ServiceImpl<BayonetMapper, BayonetEntity> implements IBayonetService {
	
}
