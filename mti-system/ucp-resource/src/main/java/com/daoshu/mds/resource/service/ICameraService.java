package com.daoshu.mds.resource.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.resource.entity.CameraEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface ICameraService extends IService<CameraEntity> {
	
	/**
	 * 查询距离范围内的点的集合
	 * @param longs
	 * @param lat
	 * @param distance
	 * @return
	 */
	List<CameraEntity> queryCamera(Double longs,Double lat,Double distance);
}
