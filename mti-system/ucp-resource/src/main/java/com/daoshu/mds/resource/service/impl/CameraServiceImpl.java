package com.daoshu.mds.resource.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.resource.mapper.CameraMapper;
import com.daoshu.mds.resource.service.ICameraService;
import com.daoshu.resource.entity.CameraEntity;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Service
public class CameraServiceImpl extends ServiceImpl<CameraMapper, CameraEntity> implements ICameraService {
	
	@Cacheable(value = "camera", key = "#longgitude + '_' + #lat + '_' + #distance")
	@Override
	public List<CameraEntity> queryCamera(Double longgitude, Double lat, Double distance) {
		 //先计算查询点的经纬度范围
        double r = 6371;//地球半径千米
        double dis = distance/1000;//0.5千米距离
        double dlng =  2*Math.asin(Math.sin(dis/(2*r))/Math.cos(lat*Math.PI/180));
        dlng = dlng*180/Math.PI;//角度转为弧度
        double dlat = dis/r;
        dlat = dlat*180/Math.PI;        
        double minlat =lat-dlat;
        double maxlat = lat+dlat;
        double minlng = longgitude -dlng;
        double maxlng = longgitude + dlng;
        return  this.list(new QueryWrapper<CameraEntity>().between("latitude_", minlat, maxlat).between("longitude_", minlng, maxlng));

	}
}
