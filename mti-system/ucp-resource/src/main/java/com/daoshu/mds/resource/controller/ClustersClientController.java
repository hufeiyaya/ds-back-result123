package com.daoshu.mds.resource.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.resource.service.IClustersClientService;
import com.daoshu.resource.bo.ClustersClientBo;
import com.daoshu.resource.entity.ClustersClientEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Api(value = "集群管理接口", description = "集群管理接口")
@RestController
@RequestMapping("/clustersClient")
public class ClustersClientController {
	
	@Autowired
	private IClustersClientService clustersClientService;
	
	@GetMapping(value = "/get")
	@ApiOperation(value = "通过ID获取集群信息", notes = "通过ID获取集群相信信息")
	public ResponseInfo getById(@RequestParam(value = "id") final String id) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(clustersClientService.getById(id));
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/saveOrUpdate")
	@ApiOperation(value = "新增或修改集群信息", notes = "新增或者修改集群信息")
	public ResponseInfo saveOrUpdate(@RequestBody final ClustersClientBo clustersClientBo) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				ClustersClientEntity entity = new ClustersClientEntity();
				BeanUtils.copyProperties(clustersClientBo, entity);
				criteria.addSingleResult(clustersClientService.saveOrUpdate(entity));
			} 
		}.sendRequest();
	}
	
	@DeleteMapping(value = "/delete")
	@ApiOperation(value = "删除集群信息", notes = "通过IDS删除集群信息（逻辑删除）")
	public ResponseInfo delete(@RequestParam(value = "ids") final String ids) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(clustersClientService.removeByIds(Arrays.asList(ids.split(","))));
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/list")
	@ApiOperation(value = "获取集群集合", notes = "获取集群集合信息")
	public ResponseInfo list(@RequestBody final ClustersClientBo clustersClientBo) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(clustersClientService.page(new Page<ClustersClientEntity>(clustersClientBo.getStart(), clustersClientBo.getSize()), new QueryWrapper<>()));
			}
		}.sendRequest();
	}
}
