package com.daoshu.mds.resource.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.resource.service.IAgentService;
import com.daoshu.resource.bo.AgentBo;
import com.daoshu.resource.entity.AgentEntity;
import com.daoshu.system.utils.RequestUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Api(value = "坐席管理接口", description = "坐席管理接口")
@RestController
@RequestMapping(value = "/agent")
public class AgentController {
	@Autowired
	private IAgentService agentService;
	
	@Autowired
	private RequestUtils requestUtils;
	
	@GetMapping(value = "/get")
	@ApiOperation(value = "通过ID获取坐席信息", notes = "通过ID获取坐席相信信息")
	public ResponseInfo getById(@RequestParam(value = "id") final String id) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(agentService.getById(id));
			}
		}.sendRequest();
	}
	@GetMapping(value = "/ip")
	@ApiOperation(value = "通过IP获取坐席信息", notes = "通过IP获取坐席相信信息")
	public ResponseInfo getByIP(@RequestParam(value = "ip") final String ip) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				// agentService.getByIP(ip);
				criteria.addSingleResult("不给你，不用此功能");
			}
		}.sendRequest();
	}
	@GetMapping(value = "/ext")
	@ApiOperation(value = "通过分机号获取坐席信息", notes = "通过分机号获取坐席信息")
	public ResponseInfo getByExt(@RequestParam(value = "ext") final String ext) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(agentService.getOne(new QueryWrapper<AgentEntity>().eq("EXT_", ext)));
			}
		}.sendRequest();
	}
	@PostMapping(value = "/saveOrUpdate")
	@ApiOperation(value = "新增或修改坐席信息", notes = "新增或者修改坐席信息")
	public ResponseInfo saveOrUpdate(@RequestBody final AgentBo agentBo) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				AgentEntity entity = new AgentEntity();
				BeanUtils.copyProperties(agentBo, entity);
				criteria.addSingleResult(agentService.saveOrUpdate(entity));
			} 
		}.sendRequest();
	}
	
	@DeleteMapping(value = "/delete")
	@ApiOperation(value = "删除坐席信息", notes = "通过IDS删除坐席信息（逻辑删除）")
	public ResponseInfo delete(@RequestParam(value = "ids") final String ids) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(agentService.removeByIds(Arrays.asList(ids.split(","))));
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/list")
	@ApiOperation(value = "获取坐席集合", notes = "获取坐席集合信息")
	public ResponseInfo list(@RequestBody final AgentBo agentBo) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				AgentEntity entity = new AgentEntity();
				BeanUtils.copyProperties(agentBo, entity);
				criteria.addSingleResult(agentService.page(new Page<AgentEntity>(agentBo.getStart(), agentBo.getSize()), new QueryWrapper<AgentEntity>(entity)));
			}
		}.sendRequest();
	}
	
	@GetMapping(value = "/getCurrentAgent")
	@ApiOperation(value = "获取当前坐席", notes = "获取当前坐席")
	public ResponseInfo getCurrentAgent() {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(requestUtils.getCurrentIp());
				/*AgentEntity entity = new AgentEntity();
				entity.setIpAddress(requestUtils.getCurrentIp());
				criteria.addSingleResult(agentService.getOne(new QueryWrapper<AgentEntity>(entity)));*/
			}
		}.sendRequest();
	}
}
