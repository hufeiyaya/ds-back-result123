package com.daoshu.mds.resource.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.resource.mapper.AgentMapper;
import com.daoshu.mds.resource.service.IAgentService;
import com.daoshu.resource.entity.AgentEntity;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Service
public class AgentServiceImpl extends ServiceImpl<AgentMapper, AgentEntity> implements IAgentService {

	@Override
	public AgentEntity getByIP(String ip) {
		return this.getOne(new QueryWrapper<AgentEntity>().eq("ip_address_", ip));
	}
}
