package com.daoshu.mds.resource.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daoshu.resource.entity.AgentEntity;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface AgentMapper extends BaseMapper<AgentEntity> {

}