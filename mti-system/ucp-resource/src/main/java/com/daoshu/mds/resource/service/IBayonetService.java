package com.daoshu.mds.resource.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.resource.entity.BayonetEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
public interface IBayonetService extends IService<BayonetEntity> {
	
}
