package com.daoshu.mds.resource.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.resource.service.ICameraService;
import com.daoshu.resource.bo.CameraBo;
import com.daoshu.resource.entity.CameraEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Api(value = "摄像头管理接口", description = "摄像头管理接口")
@RestController
@RequestMapping("/camera")
public class CameraController {
	@Autowired
	private ICameraService cameraService;

	@GetMapping(value = "/get")
	@ApiOperation(value = "通过ID获取摄像机信息", notes = "通过ID获取摄像机信息")
	public ResponseInfo getById(@RequestParam(value = "id") final String id) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(cameraService.getById(id));
			}
		}.sendRequest();
	}

	@PostMapping(value = "/saveOrUpdate")
	@ApiOperation(value = "新增或修改摄像机信息", notes = "新增或者修改摄像机信息")
	public ResponseInfo saveOrUpdate(@RequestBody final CameraBo cameraBo) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				CameraEntity entity = new CameraEntity();
				BeanUtils.copyProperties(cameraBo, entity);
				criteria.addSingleResult(cameraService.saveOrUpdate(entity));
			} 
		}.sendRequest();
	}

	@DeleteMapping(value = "/delete")
	@ApiOperation(value = "删除摄像机信息", notes = "通过IDS删除摄像机信息（逻辑删除）")
	public ResponseInfo delete(@RequestParam(value = "ids") final String ids) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(cameraService.removeByIds(Arrays.asList(ids.split(","))));
			}
		}.sendRequest();
	}

	@PostMapping(value = "/list")
	@ApiOperation(value = "获取摄像机集合", notes = "获取摄像机集合信息")
	public ResponseInfo list(@RequestBody final CameraBo cameraBo) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				CameraEntity entity = new CameraEntity();
				BeanUtils.copyProperties(cameraBo, entity);
				criteria.addSingleResult(cameraService.page(new Page<CameraEntity>(cameraBo.getStart(), cameraBo.getSize()), new QueryWrapper<>(entity)));
			}
		}.sendRequest();
	}
	
	/*@GetMapping(value = "/list")
	@ApiOperation(value = "获取摄像机集合", notes = "获取摄像机集合信息")
	public ResponseInfo listBy(@RequestParam final Double longs,@RequestParam final Double lat,@RequestParam final Double distance) {
		return new ResponseCallBack() {
			public void execute(ResponseCriteria criteria, Object... obj) {
				List<CameraEntity> result = cameraService.queryCamera(longs, lat, distance);
				criteria.addSingleResult(result);
			}
		}.sendRequest();
	}*/
}
