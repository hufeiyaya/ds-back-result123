package com.daoshu.mds.resource.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daoshu.mds.resource.mapper.ClustersClientMapper;
import com.daoshu.mds.resource.service.IClustersClientService;
import com.daoshu.resource.entity.ClustersClientEntity;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Service
public class ClustersClientServiceImpl extends ServiceImpl<ClustersClientMapper, ClustersClientEntity> implements IClustersClientService {
	
}
