package com.daoshu.mds.resource.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.daoshu.component.page.PageConstants;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.resource.service.IClustersGroupService;
import com.daoshu.resource.bo.ClustersGroupBo;
import com.daoshu.resource.entity.ClustersGroupEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 集群组管理接口
 * @ClassName: ClustersGroupController
 * @Description: TODO
 * @author: Meifeng Tian
 * @date: 2018年10月19日 上午10:57:08
 */
@Api(value = "集群组管理接口", description = "集群管理接口")
@RestController
@RequestMapping("/clustersGroup")
public class ClustersGroupController {
	
	@Autowired
	private IClustersGroupService clustersGroupService;
	
	@GetMapping(value = "/get")
	@ApiOperation(value = "通过ID获取集群组信息", notes = "通过ID获取集群组信息")
	public ResponseInfo getById(@RequestParam(value = "id") final String id) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(clustersGroupService.getDetailById(id));
			}
		}.sendRequest();
	}
	
	@DeleteMapping(value = "/delete")
	@ApiOperation(value = "删除集群组信息", notes = "通过IDS删除集群组信息（逻辑删除）")
	public ResponseInfo delete(@RequestParam(value = "ids") final String ids) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(clustersGroupService.removeByIds(Arrays.asList(ids.split(","))));
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/list")
	@ApiOperation(value = "获取集群组集合", notes = "获取集群集合组信息")
	public ResponseInfo list(@RequestBody final ClustersGroupBo clustersClientBo) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				IPage<ClustersGroupEntity> page = clustersGroupService.getPage(clustersClientBo);
				criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
				criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
			}
		}.sendRequest();
	}
}
