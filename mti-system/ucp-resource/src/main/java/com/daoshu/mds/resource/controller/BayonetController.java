package com.daoshu.mds.resource.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daoshu.component.response.ResponseCallBack;
import com.daoshu.component.response.ResponseCriteria;
import com.daoshu.component.response.ResponseInfo;
import com.daoshu.mds.resource.service.IBayonetService;
import com.daoshu.resource.bo.BayonetBo;
import com.daoshu.resource.entity.BayonetEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-10
 */
@Api(value = "卡口管理接口", description = "卡口管理接口")
@RestController
@RequestMapping("/bayonet")
public class BayonetController {
	@Autowired
	private IBayonetService bayonetService;
	
	@GetMapping(value = "/get")
	@ApiOperation(value = "通过ID获取卡口信息", notes = "通过ID获取卡口信息")
	public ResponseInfo getById(@RequestParam(value = "id") final String id) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(bayonetService.getById(id));
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/saveOrUpdate")
	@ApiOperation(value = "新增或修改卡口信息", notes = "新增或者修改卡口信息")
	public ResponseInfo saveOrUpdate(@RequestBody final BayonetBo bayonetBo) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				BayonetEntity entity = new BayonetEntity();
				BeanUtils.copyProperties(bayonetBo, entity);
				criteria.addSingleResult(bayonetService.saveOrUpdate(entity));
			} 
		}.sendRequest();
	}
	
	@DeleteMapping(value = "/delete")
	@ApiOperation(value = "删除卡口信息", notes = "通过IDS删除卡口信息（逻辑删除）")
	public ResponseInfo delete(@RequestParam(value = "ids") final String ids) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(bayonetService.removeByIds(Arrays.asList(ids.split(","))));
			}
		}.sendRequest();
	}
	
	@PostMapping(value = "/list")
	@ApiOperation(value = "获取卡口集合", notes = "获取卡口集合信息")
	public ResponseInfo list(@RequestBody final BayonetBo bayonetBo) {
		return new ResponseCallBack() {
			@Override
			public void execute(ResponseCriteria criteria, Object... obj) {
				criteria.addSingleResult(bayonetService.page(new Page<BayonetEntity>(bayonetBo.getStart(), bayonetBo.getSize()), new QueryWrapper<>()));
			}
		}.sendRequest();
	}
}
