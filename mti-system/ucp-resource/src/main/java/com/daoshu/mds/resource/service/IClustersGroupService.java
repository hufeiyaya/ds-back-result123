package com.daoshu.mds.resource.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daoshu.resource.bo.ClustersGroupBo;
import com.daoshu.resource.entity.ClustersGroupEntity;

/**
 * @ClassName: IClustersGroupService
 * @Description: 集群组
 * @author: Meifeng Tian
 * @date: 2018年10月19日 上午10:58:15
 */
public interface IClustersGroupService extends IService<ClustersGroupEntity> {
	
	public ClustersGroupEntity getDetailById(String id);
	
	public IPage<ClustersGroupEntity> getPage(ClustersGroupBo clustersGroupBo);
}
