/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.zhpt.scheduled;

import com.alibaba.fastjson.JSON;
import com.mti.message.component.producer.ProducerOperator;
import com.mti.zhpt.model.websocket.Message;
import com.mti.zhpt.model.websocket.MessageType;
import com.mti.zhpt.model.websocket.WSEventFeedbackOverTimeEntity;
import com.mti.zhpt.service.impl.DataServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@Configurable
@EnableScheduling
@Slf4j
public class ScheduledTasks {

    @Autowired
    private DataServiceImpl dataService;
    @Autowired
    private ProducerOperator producerOperator;

    //每5 分钟执行一次（事件单表）
    @Scheduled(cron = "0 */1 *  * * * ")
    public void reportCurrentByCron(){
//        Date overTime = ;
        List<WSEventFeedbackOverTimeEntity > result = dataService.getCountEventFeedbackOverTime(null);
        Message msg = new Message();
        msg.setCode(MessageType.ALARM_DELAY.name());
        msg.setContent(result);

        log.info("sink rabbitmq msg - " + msg.toString());
        producerOperator.sendMsg(JSON.toJSONString(msg));

    }
}

