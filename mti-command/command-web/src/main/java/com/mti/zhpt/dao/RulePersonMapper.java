package com.mti.zhpt.dao;

import com.mti.zhpt.model.RulePerson;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/8
 * @change
 * @describe describe
 **/
public interface RulePersonMapper {
    List<RulePerson> listAll(
            @Param(value = "page")int page,
            @Param(value = "size")int size,
            @Param(value = "title")String title,
            @Param(value = "keyWord")String keyWord,
            @Param(value = "type")String type,
            @Param(value = "caseType")List<String> caseType
    );
    long countRows(
            @Param(value = "title")String title,
            @Param(value = "keyWord")String keyWord,
            @Param(value = "type")String type,
            @Param(value = "caseType")List<String> caseType
    );

    int save(RulePerson entity);
    int update(RulePerson entity);
    int deleteById(@Param(value = "id")String id);
    RulePerson getOneById(@Param(value = "id")String id);

    /**
     * 匹配法言法语
     * @param content 内容
     * @param type 类型
     * @return 匹配到的法言法语列表
     */
    List<RulePerson> matchRulePerson(
            @Param(value = "content")String content,
            @Param(value = "type")String type
    );

    /**
     * 根据ID获取法言法语
     * @param ids ids
     * @return list
     */
    List<RulePerson> findRulePersonWithIds(@Param(value = "ids") List<String> ids);
}
