package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.CpReserveProcessMapper;
import com.mti.zhpt.model.CpReserveProcess;
import com.mti.zhpt.utils.SnowFlake;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 预案流程 实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-21
 */
@Service
@Slf4j
@Transactional
public class CpReserveProcessServiceImpl {

	@Autowired
    CpReserveProcessMapper cpReserveProcessMapper;

	@Autowired
	SnowFlake snowFlake;

    /**
     * 根据预案id查询流程信息列表
     *
     * @param reserveId 预案id
     * @return
     */
    public List<CpReserveProcess> listByReserveId(String reserveId) {
        List<CpReserveProcess> result = cpReserveProcessMapper.listByReserveId(reserveId);
        return result;
    }

    /**
     * 根据预案id查询流程信息
     *
     * @param reserveId 预案id
     * @return
     */
    public CpReserveProcess getByReserveId(String reserveId) {
        CpReserveProcess result = cpReserveProcessMapper.getByReserveId(reserveId);
        return result;
    }

    /**
     *
     * 批量保存预案流程
     *
     * @param cpReserveProcessList 预案流程对象集合
     * @return
     */
    @Transactional
    public int saveCpReserveProcessBatch(List<CpReserveProcess> cpReserveProcessList) {
        Date date = new Date();
        cpReserveProcessList.forEach(cpReserveProcess -> {
            cpReserveProcess.setId(snowFlake.nextId());
            cpReserveProcess.setCreateTime(date);
        });
        Integer result = cpReserveProcessMapper.saveCpReserveProcessBatch(cpReserveProcessList);
        return result;
    }

    /**
     *
     * 保存或编辑预案流程
     *
     * @param cpReserveProcess 预案流程对象
     * @return
     */
    @Transactional
    public int saveOrUpdateCpReserveProcess(CpReserveProcess cpReserveProcess) {
        Integer result;
        if (StringUtils.isBlank(cpReserveProcess.getId())) {
            Date date = new Date();
            cpReserveProcess.setId(snowFlake.nextId());
            cpReserveProcess.setCreateTime(date);
            result = cpReserveProcessMapper.saveCpReserveProcess(cpReserveProcess);
        } else {
            cpReserveProcess.setUpdateTime(new Date());
            result = cpReserveProcessMapper.updateCpReserveProcess(cpReserveProcess);
        }

        return result;
    }

}
