package com.mti.zhpt.dao;

import com.mti.zhpt.model.CpLfGpsHistory;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 历史轨迹对象 mapper
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-08
 */
public interface CpLfGpsHistoryMapper {

	/**
	 * 根据警员id查询历史轨迹列表
	 *
	 * @param policeId 警员id
	 * @return
	 */
	@Select("select id, police_id as policeId, gps_id as gpsId, longitude, latitude, create_time as createTime, type, police_name as policeName, police_tel as policeTel, police_org as policeOrg from cp_lf_gps_history " +
			"where police_id = #{policeId} and create_time >= #{startTime} and create_time <= #{endTime} order by create_time asc")
	List<CpLfGpsHistory> listByPoliceId(String policeId, Date startTime, Date endTime);

	/**
	 * 根据eventId查询历史轨迹列表
	 *
	 * @return
	 */
	@Select("select id, police_id as policeId, gps_id as gpsId, longitude, latitude, create_time as createTime, type, police_name as policeName, police_tel as policeTel, police_org as policeOrg from cp_lf_gps_history where 1=1 order by create_time asc")
	List<CpLfGpsHistory> listByEventId(String eventId);

}