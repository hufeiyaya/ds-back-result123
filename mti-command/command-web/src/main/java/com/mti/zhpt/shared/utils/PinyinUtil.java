package com.mti.zhpt.shared.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.*;

public class PinyinUtil {
	private static final HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();

	static {
		// UPPERCASE：大写 (ZHONG)
		// LOWERCASE：小写 (zhong)
		format.setCaseType(HanyuPinyinCaseType.LOWERCASE);

		// WITHOUT_TONE：无音标 (zhong)
		// WITH_TONE_NUMBER：1-4数字表示英标 (zhong4)
		// WITH_TONE_MARK：直接用音标符（必须WITH_U_UNICODE否则异常） (zhòng)
		format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);

		// WITH_V：用v表示ü (nv)
		// WITH_U_AND_COLON：用"u:"表示ü (nu:)
		// WITH_U_UNICODE：直接用ü (nü)
		format.setVCharType(HanyuPinyinVCharType.WITH_V);
	}



	public static List<String> getPinyinSZM(String str) {
		if (StringUtil.isEmpty(str)) {
			return null;
		}
		List<List<String>> pinyinList = new ArrayList<List<String>>();
		char[] cs = str.toCharArray();
		for (char c : cs) {
			try {
				// 考虑多音字问题
				String[] strs = PinyinHelper.toHanyuPinyinStringArray(c, format);
				if (strs != null) {
					Set<String> set = new HashSet<String>();
					for (String s : strs) {
						set.add(s.substring(0, 1));
					}
					List<String> tempList = new ArrayList<String>();
					tempList.addAll(set);
					pinyinList.add(tempList);
				} else {
					pinyinList.add(Arrays.asList("" + c));
				}
			} catch (BadHanyuPinyinOutputFormatCombination e) {
				e.printStackTrace();
			}
		}

		return descartes(pinyinList);
	}

	@SuppressWarnings("rawtypes")
	private static List<String> descartes(List<List<String>> strs) {
		int total = 1;
		for (int i = 0; i < strs.size(); i++) {
			total *= strs.get(i).size();
		}
		String[] mysesult = new String[total];
		int now = 1;
		// 每个元素每次循环打印个数
		int itemLoopNum = 1;
		// 每个元素循环的总次数
		int loopPerItem = 1;
		for (int i = 0; i < strs.size(); i++) {
			List temp = strs.get(i);
			now = now * temp.size();
			// 目标数组的索引值
			int index = 0;
			int currentSize = temp.size();
			itemLoopNum = total / now;
			loopPerItem = total / (itemLoopNum * currentSize);
			int myindex = 0;
			for (int j = 0; j < temp.size(); j++) {

				// 每个元素循环的总次数
				for (int k = 0; k < loopPerItem; k++) {
					if (myindex == temp.size())
						myindex = 0;
					// 每个元素每次循环打印个数
					for (int m = 0; m < itemLoopNum; m++) {
						mysesult[index] = (mysesult[index] == null ? "" : mysesult[index] + "")
								+ ((String) temp.get(myindex));
						index++;
					}
					myindex++;

				}
			}
		}
		return Arrays.asList(mysesult);
	}

	public static void main(String[] args) {
/*		List<List<String>> resultList = getPinyin("090022066和平南路中国石油化工山西太原石油分公司和平南路加油站南侧墙壁4卡口");
		for (String s : resultList.get(0)) {
			System.out.println(s);
		}

		for (String s : resultList.get(1)) {
			System.out.println(s);
		}*/

		String chineseStr = "重庆";
		List<String> szmList = getPinyinSZM(chineseStr);
		StringBuilder sb = new StringBuilder("");
		for (String soundHead : szmList) {
			sb.append(soundHead).append(",");
		}
		String result = sb.toString().replaceAll(" ","");
		if(result.contains(",")){
			result = chineseStr + "<span 'display:none'>"+result.substring(0,result.lastIndexOf(","))+"</span>";
		}
		//System.out.println(StringEscapeUtils.escapeHtml4(result));
		System.out.println(result);
	}
}
