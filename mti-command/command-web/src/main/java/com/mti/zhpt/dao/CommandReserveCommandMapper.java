package com.mti.zhpt.dao;

import com.mti.zhpt.model.EventReserveCommandEntity;
import com.mti.zhpt.model.EventReserveEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommandReserveCommandMapper {

    List<EventReserveEntity> queryAll(@Param("param") String param);

    List<EventReserveCommandEntity> queryCommand(EventReserveCommandEntity entity);

    List<EventReserveCommandEntity> queryStageType(EventReserveCommandEntity entity);

    Integer insertCommand(@Param("entityList") List<EventReserveCommandEntity> entity);
}
