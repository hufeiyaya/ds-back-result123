package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactCityBureauUnitMemberMapper;
import com.mti.zhpt.model.relationnet.ContactCityBureauUnitMemberEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe describe
 **/
@Service(value = "ContactCityBureauUnitMemberEntity")
@AllArgsConstructor
public class ContactCityBureauUnitMemberServiceImpl implements BaseService<ContactCityBureauUnitMemberEntity> {
    private final ContactCityBureauUnitMemberMapper mapper;

    @Override
    public void save(ContactCityBureauUnitMemberEntity contactCityBureauUnitMemberEntity) {
        mapper.save(contactCityBureauUnitMemberEntity);
    }

    @Override
    public ContactCityBureauUnitMemberEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactCityBureauUnitMemberEntity contactCityBureauUnitMemberEntity) {
        mapper.update(contactCityBureauUnitMemberEntity);
    }

    @Override
    public PageModel<List<ContactCityBureauUnitMemberEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactCityBureauUnitMemberEntity>> pageModel = new PageModel<>();
        pageModel.setSize(size);
        pageModel.setPage(page);
        pageModel.setRecord(mapper.listAll(page, size,searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        PageModel<List<String>> pageModel = new PageModel<>();
        List<String> list = mapper.getGroupNames(page, size);
        pageModel.setRecord(list);
        pageModel.setTotal((long) list.size());
        return pageModel;
    }
}
