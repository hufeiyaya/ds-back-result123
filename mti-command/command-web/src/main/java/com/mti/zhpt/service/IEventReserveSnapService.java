package com.mti.zhpt.service;

import com.mti.zhpt.model.EventSnapShotReserveDetailEntity;

import java.util.List;
import java.util.Map;

public interface IEventReserveSnapService {
    Map<String, Object> recordSnapShotDataToDetail(String eventId, String reserverId);
    void addSnapshotData(EventSnapShotReserveDetailEntity detailEntity);
    void updateSnapshotData(EventSnapShotReserveDetailEntity detailEntity);
    EventSnapShotReserveDetailEntity getEntityById(String id);
    void updateSnapshotDataStatus(String eventReserveId);
    Map<String,Map<String, List<Map<Object, Object>>>> getReserveMonitorList(String eventId,String reserveId);
}
