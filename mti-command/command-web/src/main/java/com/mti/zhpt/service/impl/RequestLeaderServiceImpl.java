package com.mti.zhpt.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mti.zhpt.shared.Exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mti.zhpt.dao.EventDetailsMapper;
import com.mti.zhpt.dao.RequestLeaderMapper;
import com.mti.zhpt.model.TextRequest;
import com.mti.zhpt.shared.websocket.WSCustomServer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

/**
 * @Description: 请示汇报
 * @Author: yingjj@mti-sh.cn
 * @Date: 2019/4/23 11:14
 */
@Service
@Slf4j
public class RequestLeaderServiceImpl {
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	@Autowired
	private RequestLeaderMapper requestLeaderMapper;
	@Autowired
	private EventDetailsMapper eventDetailsMapper;

	public List<Map<String, Object>> queryRequestLeaderList() {
		List<Map<String, Object>> result = null;
		try {
			result = requestLeaderMapper.queryRequestLeaderList();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return result;
	}

	public Map<String, Object> generateRecord(int leaderid, int eventid) {
		Map<String, Object> prams = new HashMap<String, Object>();
		Date now=new Date();
		try {
			long timestamp =now.getTime();
			prams.put("eventid", eventid);
			prams.put("recordid", eventid + "_" + timestamp + "_" + leaderid);
			prams.put("create_time", now);
			prams.put("leaderid", leaderid);
			requestLeaderMapper.addRequestRecordPre(prams);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return prams;
	}

	public List<Map<String, Object>> queryRecord(String eventid) {
		List<Map<String, Object>> result = null;
		try {
			result = requestLeaderMapper.queryRecord(eventid);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return result;
	}

	public int doRequestLeader(Map<String, Object> request) {
		int result = 0;
		try {
			request.put("createtime", new Date());
			result = requestLeaderMapper.doRequestLeader(request);
			if ((int) request.get("status") == 1) {
				@SuppressWarnings("unchecked")
				List<String> playorglist = (List<String>) request.get("play_org_id");
				for (int i = 0; i < playorglist.size(); i++) {
					String playorgid = playorglist.get(i);
					requestLeaderMapper.playRequestOrg((Integer) request.get("id"), playorgid);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return result;
	}

	public List<Map<String, Object>> queryRequestList(String eventid) {
		List<Map<String, Object>> result = null;
		try {
			result = requestLeaderMapper.queryRequestList(eventid);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return result;
	}

	public int playRequest(Map<String, Object> request) {
		int result = 0;
		try {
			result = requestLeaderMapper.playRequest((Integer) request.get("id"));
			@SuppressWarnings("unchecked")
			List<String> playorglist = (List<String>) request.get("play_org_id");
			for (int i = 0; i < playorglist.size(); i++) {
				String playorgid = playorglist.get(i);
				requestLeaderMapper.playRequestOrg((int) request.get("id"), playorgid);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return result;
	}

	public int doRequest(TextRequest request) {
		int result = 0;
		try {
			result = requestLeaderMapper.doRequest(request);
			if (!CollectionUtils.isEmpty(request.getLeaders())){
				request.setLeaderId(request.getLeaders().get(0));
			}else {
				throw new BusinessException("领导id不能为空");
			}
			/**
			 * 插入 领导批复表
			 * cp_response
			 */
			request.setStatus(0);
			 requestLeaderMapper.instructContent(request);
			List<String> leaders = (List<String>) request.getLeaders();
			for (int i = 0; i < leaders.size(); i++) {
				String leaderid = leaders.get(i);
				requestLeaderMapper.addRequestLeader( request.getId(), leaderid);
				// 只给局长发socket消息
//				if (leaderid.equals(" 1")) {
//					log.debug("请示汇报局长:");
//					List<Map<String, Object>> infolist = new ArrayList<Map<String, Object>>();
//					Map<String, Object> reqobj = new HashMap<String, Object>();
//					reqobj.put("msgType", 4);
//					reqobj.put("id", request.getId());
//					reqobj.put("title", eventDetailsMapper.queryEventTitle(request.getEventid())+"请示汇报");
//					reqobj.put("content", request.getContent());
//					reqobj.put("createTime", formatter.format(request.getCreate_time()));
//					reqobj.put("isRead", 1);//1未读、2已读
//					infolist.add(reqobj);
//					WSCustomServer.sendInfo(infolist, "admin");
//				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return result;
	}

	public List<Map<String, Object>> queryPlayOrg(int responseid) {
		List<Map<String, Object>> result = null;
		try {
			result = requestLeaderMapper.queryPlayOrg(responseid);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return result;
	}

}
