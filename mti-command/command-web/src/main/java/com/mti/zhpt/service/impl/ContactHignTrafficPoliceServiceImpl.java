package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactHignTrafficPoliceMapper;
import com.mti.zhpt.model.relationnet.ContactHignTrafficPoliceEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
@Service(value = "ContactHignTrafficPoliceEntity")
@AllArgsConstructor
public class ContactHignTrafficPoliceServiceImpl implements BaseService<ContactHignTrafficPoliceEntity> {
    private final ContactHignTrafficPoliceMapper mapper;

    @Override
    public void save(ContactHignTrafficPoliceEntity contactHignTrafficPoliceEntity) {
        mapper.save(contactHignTrafficPoliceEntity);
    }

    @Override
    public ContactHignTrafficPoliceEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactHignTrafficPoliceEntity contactHignTrafficPoliceEntity) {
        mapper.update(contactHignTrafficPoliceEntity);
    }

    @Override
    public PageModel<List<ContactHignTrafficPoliceEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactHignTrafficPoliceEntity>> pageModel = new PageModel<>();
        pageModel.setSize(size);
        pageModel.setPage(page);
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setRecord(mapper.listAll(page,size,searchTerm[0],searchTerm[1],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }
}
