package com.mti.zhpt.controller;

import com.mti.zhpt.model.CpCarBank;
import com.mti.zhpt.model.KeyPersonneEntity;
import com.mti.zhpt.service.impl.CarBankServiceImpl;
import com.mti.zhpt.service.impl.KeyPersonnelServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 重点人员报警 每日统计
 *
 * @author zpf
 * @since 2019-07-29
 */
@RestController
@RequestMapping(path = "/keyPersonnel")
@Api(value = "/keyPersonnel", tags = "重点人员报警每日统计")
@Slf4j
public class KeyPersonnelController {

    @Autowired
    KeyPersonnelServiceImpl keyPersonnelService;

    /**
     * 重点人员报警每日统计
     *
     * @param
     * @return
     */
    @RequestMapping(path = "/statistics", method = RequestMethod.GET)
    @ApiOperation(value = "重点人员报警每日统计")
    public RetResult<List<KeyPersonneEntity>> getKP(
            @ApiParam(required = true, name = "date", value = "日期", example = "2019-09-22") @RequestParam(value = "date", required = true) String date,
            @ApiParam(required = true, name = "zrbm", value = "责任部门(警种)", example = "治安支队") @RequestParam(value = "zrbm", required = true) String zrbm,
            @ApiParam(name = "oid", value = "派出所ID") @RequestParam(value = "oid", required = false) String oid,
            @ApiParam(name = "ssxqId", value = "所属辖区ID") @RequestParam(value = "ssxqId", required = false) String ssxqId) {
        if (StringUtils.isEmpty(date)) {
            return RetResponse.makeErrCp("日期 不能为空");
        }
        List<KeyPersonneEntity> result = keyPersonnelService.getKP(date, zrbm,oid,ssxqId);
        return RetResponse.makeOKCp(result);
    }

}
