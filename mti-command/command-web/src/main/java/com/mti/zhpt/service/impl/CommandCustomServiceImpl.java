package com.mti.zhpt.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;

import com.mti.zhpt.dao.ResourceMapper;

/**
 * @Description: 精准指挥
 * @Author: zhaopf@mti-sh.cn
 * @Date: 2019/4/19 18:15
 */
@Service("commandCustomServiceImpl")
@EnableCaching
public class CommandCustomServiceImpl {

	@Autowired
	private ResourceMapper resourceMapper;

	/**
	 * @Description: 递归处理 数据库树结构数据->树形json
	 * @Author: zhaopf@mti-sh.cn
	 * @Date: 2019/4/18
	 */
	@Cacheable(value = { "orgTree" })
	public List<Map<Object, Object>> queryAll() {
		List<Map<Object, Object>> rootCompanys = resourceMapper.queryAllRootCompanys();
		for(Map<Object, Object> root:rootCompanys) {
			List<Map<String,Object>> companys = (List<Map<String, Object>>) root.get("companys");
			for(Map<String, Object> company:companys) {
				addChild(company);
			}
		}
		return rootCompanys;
	}

	private void addChild(Map<String, Object> company) {
		List<Map<String,Object>> childrens=null;
		childrens=resourceMapper.queryAllChildCompanys(company);
		for(Map<String, Object> children:childrens) {
			addChild(children);
		}
		company.put("childrens", childrens);
	}
}
