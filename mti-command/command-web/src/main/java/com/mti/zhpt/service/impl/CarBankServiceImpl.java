package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.CarBankMapper;
import com.mti.zhpt.model.CpCarBank;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 车辆库实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-29
 */
@Service
@Slf4j
@Transactional
public class CarBankServiceImpl {

	@Autowired
    CarBankMapper carBankMapper;

    /**
     * 根据车牌号查询车辆信息
     *
     * @param carNumber 车牌号
     * @return
     */
    public CpCarBank getByCarNumber(String carNumber) {
        CpCarBank result = carBankMapper.getByCarNumber(carNumber);
        return result;
    }

}
