package com.mti.zhpt.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mti.zhpt.dao.CommandReserveMapper;
import com.mti.zhpt.dao.EventDetailsTimerMapper;
import com.mti.zhpt.dao.EventMapper;
import com.mti.zhpt.dao.EventPowerMapper;
import com.mti.zhpt.dao.EventRefPersonMapper;
import com.mti.zhpt.dao.IpWarningMapper;
import com.mti.zhpt.dao.IpWarningUpMapper;
import com.mti.zhpt.dao.PersonMapper;
import com.mti.zhpt.dao.RequestLeaderMapper;

@Service("systemServiceImpl")
public class SystemServiceImpl {

	@Autowired
	private EventMapper eventMapper;
	@Autowired
	private PersonMapper personMapper;
	@Autowired
	private EventPowerMapper eventPowerMapper;
	@Autowired
	private EventDetailsTimerMapper eventDetailsTimerMapper;
	@Autowired
	private CommandReserveMapper commandReserveMapper;
	@Autowired
	private RequestLeaderMapper requestLeaderMapper;
	@Autowired
	private EventRefPersonMapper eventRefPersonMapper;
	@Autowired
	private IpWarningMapper ipWarningMapper;
	@Autowired
	private IpWarningUpMapper ipWarningUpMapper;

	public void recovery() {
		Date now = new Date();
		String id = "1";
		eventMapper.delEventNewCreated("6");
		eventMapper.updateDateToNow(id, now);
		eventMapper.updateEventStatus(id, 0, new Date());
		eventMapper.updateEventReserve(id, "0");
		eventPowerMapper.delEventPower(id);
		commandReserveMapper.cancelCommandModelCommandModel(id);
		eventDetailsTimerMapper.delTimerList(id);
		requestLeaderMapper.delRequest(id);
		requestLeaderMapper.delResponseRecord(id);
		requestLeaderMapper.delResponse(id);
		eventRefPersonMapper.delEventRefPersonsNewAdded(id);
		personMapper.resumePoliceStatus();
		ipWarningMapper.recoverAll();
		ipWarningUpMapper.recoverAll();
	}

}
