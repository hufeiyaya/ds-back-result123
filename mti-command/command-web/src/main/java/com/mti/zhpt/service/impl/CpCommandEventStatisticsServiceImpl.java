package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.CpCommandEventStatisticsMapper;
import com.mti.zhpt.model.CpCommandEventStatisticsEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe describe
 **/
@Service
@AllArgsConstructor
public class CpCommandEventStatisticsServiceImpl implements BaseService<CpCommandEventStatisticsEntity> {
    private final CpCommandEventStatisticsMapper mapper;
    @Override
    public void save(CpCommandEventStatisticsEntity cpCommandEventStatisticsEntity) {
        mapper.save(cpCommandEventStatisticsEntity);
    }

    @Override
    public CpCommandEventStatisticsEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(CpCommandEventStatisticsEntity cpCommandEventStatisticsEntity) {
        mapper.update(cpCommandEventStatisticsEntity);
    }

    @Override
    public PageModel<List<CpCommandEventStatisticsEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<CpCommandEventStatisticsEntity>> pageModel = new PageModel<>();
        pageModel.setRecord(mapper.findAllByPage(page,size));
        pageModel.setTotal(mapper.countAllRows());
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }

    public CpCommandEventStatisticsEntity getTotalData(String eventId){
        CpCommandEventStatisticsEntity entity = mapper.getEntityByRiskId(eventId);
        if(entity==null){
            entity = new CpCommandEventStatisticsEntity();
        }
        Map<String,Object> result = mapper.getStatisticsData(eventId);
        if(result!=null && !result.isEmpty()){
            entity.setOperateOrg(result.getOrDefault("operateorgcount","0").toString());
            entity.setOutboundPerson(result.getOrDefault("outboundpersoncount","0").toString());
            entity.setOutboundCar("0");
            entity.setCommandNum(result.getOrDefault("commandnum","0").toString());
            entity.setCommandBackNum(result.getOrDefault("feedbacknum","0").toString());
            entity.setWithPowerNum(result.getOrDefault("with_power_times","0").toString());
        }else{
            entity.setOperateOrg("0");
            entity.setOutboundPerson("0");
            entity.setOutboundCar("0");
            entity.setCommandNum("0");
            entity.setCommandBackNum("0");
            entity.setWithPowerNum("0");
        }
        return entity;
    }
}
