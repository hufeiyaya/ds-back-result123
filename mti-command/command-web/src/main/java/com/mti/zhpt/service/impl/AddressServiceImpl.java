package com.mti.zhpt.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mti.zhpt.dao.AddressMapper;

@Service("addressServiceImpl")
public class AddressServiceImpl {
	@Autowired
	private AddressMapper addressMapper;
	public List<Map<String, Object>> getLonLat(String address) {
		return addressMapper.getLonLat(address);
	}

}
