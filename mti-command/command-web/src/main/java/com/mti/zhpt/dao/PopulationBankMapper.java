package com.mti.zhpt.dao;

import com.mti.zhpt.model.CpPopulationBank;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 人口库 mapper
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-29
 */
public interface PopulationBankMapper {

	/**
	 * 根据身份证号查询人员信息
	 *
	 * @param idCard 身份证号
	 * @return
	 */
	@Select("select a.id_ as id, name_ as name, sex_ as sex, nation_ as nation, id_card as idCard, phone_ as phone, address_ as address, photo_ as photo, create_time as createTime from cp_population_bank a where id_card = #{idCard}")
	CpPopulationBank getByIdCard(@Param("idCard") String idCard);

}