package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactHebeiCommandCenterMapper;
import com.mti.zhpt.model.relationnet.ContactHebeiCommandCenterEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
@Service(value = "ContactHebeiCommandCenterEntity")
@AllArgsConstructor
public class ContactHebeiCommandCenterServiceImpl implements BaseService<ContactHebeiCommandCenterEntity> {
    private final ContactHebeiCommandCenterMapper mapper;

    @Override
    public void save(ContactHebeiCommandCenterEntity contactHebeiCommandCenterEntity) {
        mapper.save(contactHebeiCommandCenterEntity);
    }

    @Override
    public ContactHebeiCommandCenterEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactHebeiCommandCenterEntity contactHebeiCommandCenterEntity) {
        mapper.update(contactHebeiCommandCenterEntity);
    }

    @Override
    public PageModel<List<ContactHebeiCommandCenterEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactHebeiCommandCenterEntity>> pageModel = new PageModel<>();
        pageModel.setSize(size);
        pageModel.setPage(page);
        pageModel.setRecord(mapper.listAll(page,size));
        pageModel.setTotal(mapper.countRows());
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }
}
