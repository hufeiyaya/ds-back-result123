/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.DataMapper;
import com.mti.zhpt.model.websocket.WSEventFeedbackOverTimeEntity;
import com.mti.zhpt.shared.Exception.BusinessException;
import com.mti.zhpt.shared.utils.DateUtils;
import com.mti.zhpt.vo.AnyouTjVo;
import com.mti.zhpt.vo.JingqingTjVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;


@Service("dataServiceImpl")
@Slf4j
public class DataServiceImpl {

	@Resource
	private DataMapper dataMapper;

	public int getCountByJjdb(String startTime, String endTime,String ssxqId) {
		List<String> ids = null;
		if(!StringUtils.isEmpty(ssxqId)){
			String[] ssxqIds = ssxqId.split(",");
			int length = ssxqIds.length;
			ids = new ArrayList<>();
			for (int i = 0; i < length; i++) {
				ids.add(ssxqIds[i].substring(0,6));
			}
		}
		return dataMapper.getCountByJjdb(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime),ids);
	}

	public int getCountByCjdb(String startTime, String endTime) {
		return dataMapper.getCountByCjdb(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime));
	}

	public int getCountEventByLevel(String startTime, String endTime, String level) {
		return dataMapper.getCountEventByLevel(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), level);
	}

	public Map<String, String> getCountEventByAllLevel(String startTime, String endTime,String type) {
		Map<String, String> result = new HashMap<String, String>();

		int jq1 = StringUtils.isEmpty(type)? dataMapper.getCountEventByLevel(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), "01"):
				dataMapper.getCountEventByLevelAndType(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), "01",type);
		int jq2 = StringUtils.isEmpty(type)? dataMapper.getCountEventByLevel(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), "02"):
				dataMapper.getCountEventByLevelAndType(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), "02",type);
		int jq3 = StringUtils.isEmpty(type)? dataMapper.getCountEventByLevel(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), "03"):
				dataMapper.getCountEventByLevelAndType(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), "03",type);
		int jq4 =  StringUtils.isEmpty(type)? dataMapper.getCountEventByLevel(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), "04"):
				dataMapper.getCountEventByLevelAndType(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), "04",type);

		result.put("jq1", String.valueOf(jq1));
		result.put("jq2", String.valueOf(jq2));
		result.put("jq3", String.valueOf(jq3));
		result.put("jq4", String.valueOf(jq4));
		return result;
	}


	public List<Map<String, String>> getCountByAnyou(String startTime, String endTime,String ssxqId) {
		return dataMapper.getCountByAnyou(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime),StringUtils.isEmpty(ssxqId)?null:Arrays.asList(ssxqId.split(",")));
	}

	public List<Map<String, String>> getCountByXiaqu(String startTime, String endTime,String ssxqId) {
		return dataMapper.getCountByXiaqu(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime),StringUtils.isEmpty(ssxqId)?null:Arrays.asList(ssxqId.split(",")));
	}

	public List<Map<String, Object>> getCountByPCS(String startTime, String endTime,String ssxqId) {
		List<Map<String, Object>> orgList = dataMapper.queryPcsByFj(ssxqId);
		List<Map<String, Object>> statList = dataMapper.statByPcs(startTime, endTime, ssxqId);

		List<Map<String, Object>> resultList = new ArrayList<>();

		if(orgList.size() > 0) {
			orgList.forEach(orgMap->{
				Map<String, Object> org2StatMap = new HashMap<>();
				String orgName = orgMap.get("short_name_").toString();
				org2StatMap.put("xiaqu", orgName);
				org2StatMap.put("count", 0);

				statList.forEach(statMap->{
					String statOrgName = statMap.get("short_name_").toString();
					if(orgName.equals(statOrgName)) {
						org2StatMap.put("count", Integer.valueOf(statMap.get("cnt").toString()));
					}
				});

				resultList.add(org2StatMap);
			});

		}

		return resultList;
	}

	public List<Map<String, String>> getCountAvgByXiaqu(String date) {

		List<Map<String, String>> xqListMap = new ArrayList<Map<String, String>>();

//		String strLastMonth = DateUtils.getStrOfLastMonth(DateUtils.getDate(date));
//		String strTodayTime = DateUtils.getTodayTime();

		String fistDayOfLastMonth = DateUtils.getFirstDayOfLastMonth(DateUtils.getDate(date));
		String lastDayOfLastMonth = DateUtils.getLastDayOfLastMonth(DateUtils.getDate(date));
		int daysOfLastMonth = DateUtils.getDaysOfLastMonth(DateUtils.getDate(date));
		List<Map<String, String>> xqMap = dataMapper.getXiaqu(null, null);
		System.out.println(xqMap.size());
		if (xqMap.size() > 0) {
			for (int i = 0; i < xqMap.size(); i++) {
				String key = xqMap.get(i).get("key");
				String name = xqMap.get(i).get("value");
				int avg = 0;
				int total = 0;
//				if(DateUtils.isToday(date)){
//					int totalDay = 0;
//					for (int j = 1; j <= daysOfLastMonth; j++) {
//						String startTime = "";
//						String endTime = "";
//						if (j < 10) {
//							startTime = strLastMonth + "-0" + j + " 00:00:00";
//							endTime = strLastMonth  + "-" + j + " " + strTodayTime;
//						} else {
//							startTime = strLastMonth  + "-" + j + " 00:00:00";
//							endTime = strLastMonth  + "-" + j + " " + strTodayTime;
//						}
//						totalDay = dataMapper.getTotalByXiaqu(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), key);
//						total = total + totalDay;
//					}
//				}else {
					String startTime = fistDayOfLastMonth + " 00:00:00";
					String endTime = lastDayOfLastMonth + " 23:59:59";
					total = dataMapper.getTotalByXiaqu(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), key);
//				}
				avg = total%daysOfLastMonth;
				Map<String, String> avgMap = new HashMap<String, String>();
				avgMap.put("xiaqu", name);
				avgMap.put("count", String.valueOf(avg));
				xqListMap.add(avgMap);
			}
		}

		return xqListMap;
	}

	public List<JingqingTjVo> getCountByJingqing(String startTime, String endTime, String isImportant,
												 String deptCode, String deptLevel){
		List<JingqingTjVo> vos = null;
		try {
			if (StringUtils.isEmpty(deptCode)){
				throw new BusinessException("登录人id NOT NULL");
			}else if (StringUtils.isEmpty(deptLevel)){
				throw new BusinessException("登录人数据权限级别 NOT NULL");
			}
			vos = new ArrayList<JingqingTjVo>();
			List<Map<String, String>> xqMap = dataMapper.getXiaqu(deptCode.substring(0,6)+"000000",deptLevel);

			if (xqMap.size() > 0) {
				for (int i = 0; i < xqMap.size(); i++) {
					JingqingTjVo jingqingTjVo = new JingqingTjVo();
					String key = xqMap.get(i).get("key");
					String name = xqMap.get(i).get("value");
					jingqingTjVo.setId(key);
					jingqingTjVo.setName(name);
					if (isImportant.equals("0")) {
						int total = dataMapper.getTotalByXiaquNormal(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), key);
						jingqingTjVo.setCount(String.valueOf(total));
	//					List<AnyouTjVo> ayList = dataMapper.getCountAnyouByXiaquNormal(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), key);
	//					jingqingTjVo.setAnyou(ayList);
					} else if (isImportant.equals("1")){
						int total = dataMapper.getTotalByXiaquImportant(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), key);
						jingqingTjVo.setCount(String.valueOf(total));
	//					List<AnyouTjVo> ayList = dataMapper.getCountAnyouByXiaquImportant(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), key);
	//					jingqingTjVo.setAnyou(ayList);
					}
					vos.add(jingqingTjVo);
				}
			}
		} catch (Exception e) {
			log.error("警情统计",e);
			throw new BusinessException(e.getMessage());
		}
		return vos;
	}

	public List<JingqingTjVo> getCountByXqay(String startTime, String endTime, String branchUnitId, String isImportant){
		List<JingqingTjVo> vos = new ArrayList<JingqingTjVo>();

		JingqingTjVo jingqingTjVo = new JingqingTjVo();
		String name = dataMapper.getXiaquName(branchUnitId);
		if (StringUtils.isNotBlank(name)) {
			jingqingTjVo.setName(name);

			if (isImportant.equals("0")) {
				int total = dataMapper.getTotalByXiaquNormal(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), branchUnitId);
				jingqingTjVo.setCount(String.valueOf(total));
				List<AnyouTjVo> ayList = dataMapper.getCountAnyouByXiaquNormal(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), branchUnitId);
				jingqingTjVo.setAnyou(ayList);
			} else if (isImportant.equals("1")){
				int total = dataMapper.getTotalByXiaquImportant(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), branchUnitId);
				jingqingTjVo.setCount(String.valueOf(total));
				List<AnyouTjVo> ayList = dataMapper.getCountAnyouByXiaquImportant(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime), branchUnitId);
				jingqingTjVo.setAnyou(ayList);
			}

			vos.add(jingqingTjVo);
		}

		return vos;
	}

	/**
	 * 根据时间查询重点警情总数
	 *
	 * @param startTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 */
	public int countImportEvent(String startTime, String endTime) {
		return dataMapper.countImportEvent(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime));
	}

	public Map<String, String> getCountJq(String startTime, String endTime,String ssxqId) {
		Map<String, String> result = new HashMap<String, String>();
		List<String> ids = StringUtils.isEmpty(ssxqId)?null:Arrays.asList(ssxqId.split(","));
		int jq1 = dataMapper.getCountEventByYingji(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime),ids);
		int jq2 = dataMapper.getCountEventByZtry(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime),ids);
		int jq3 = dataMapper.getCountEventByZdry(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime),ids);
		int jq4 = dataMapper.getCountEventBy110(DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime),ids);

		result.put("jq1", String.valueOf(jq1));
		result.put("jq2", String.valueOf(jq2));
		result.put("jq3", String.valueOf(jq3));
		result.put("jq4", String.valueOf(jq4));
		return result;
	}


	public List<WSEventFeedbackOverTimeEntity> getCountEventFeedbackOverTime(Date overTime) {
		if (overTime == null){
			long currentTime = System.currentTimeMillis() - 150 * 60 * 1000;
			overTime = new Date(currentTime);
		}
		List<WSEventFeedbackOverTimeEntity> result = dataMapper.getCountEventFeedbackOverTimeList(overTime);
		return result;
	}

}
