package com.mti.zhpt.controller;

import com.mti.zhpt.model.Dictionary;
import com.mti.zhpt.service.impl.DictionaryServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@Api(value = "字典信息查询接口[需前后端保持一致]", description = "字典信息查询接口[需前后端保持一致]")
@RestController
@RequestMapping("system/dictionary")
@Slf4j
public class DictionaryController {

	private static final Logger logger = LoggerFactory.getLogger(DictionaryController.class);

	@Autowired
	private DictionaryServiceImpl dictionaryService;



	
	@ApiOperation(value = "查询字典信息Map结构", notes = "查询字典信息Map结构")
	@RequestMapping(value = "/getAllasMap", method = RequestMethod.GET)
	public RetResult getAllDictionaryAsMap() {
		Map<String, Map<String, String>> result = null;
		try {
			 result = dictionaryService.getAllDictionaryAsMap();
			return RetResponse.makeOKCp(result);

		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
	}

	@GetMapping(value = "/getTreeResultByType")
	@ApiOperation(value = "根据alarmCode和type查询对应的字典树案由", notes = "根据alarmCode和type查询对应的字典树案由")
	public RetResult getTreeResultByType(@RequestParam(value = "alarmCode", required = true) final String alarmCode,
											@RequestParam(value = "type", required = true) final String type) {
		Object result = null;
		try {
			result = dictionaryService.getTreeResultByType(alarmCode, type, false);
			return RetResponse.makeOKCp(result);

		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}

	}


	@ApiOperation(value = "根据type和key查询对应的字典树", notes = "据type和key查询对应的字典树——案由和地址分类")
	@ApiImplicitParams({ @ApiImplicitParam(name = "type", value = "字典类型信息", required = true, paramType = "query"),
			@ApiImplicitParam(name = "isNeedLastLevelChildren", value = "是否需要返回最后一级类型(0:不需要返回)", dataType = "Integer", required = false, paramType = "query") })
	@GetMapping(value = "/getTreeResult")
	public RetResult getTreeResult(@RequestParam(value = "type", required = true) final String type,
									  @RequestParam(value = "isNeedLastLevelChildren", required = false) final Integer isNeedLastLevelChildren) {

		List<Dictionary> result = null;
		try {
			result = dictionaryService.getTreeByType(type,isNeedLastLevelChildren);
			return RetResponse.makeOKCp(result);

		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
	}


	@ApiOperation(value = "根据type某一类型字典所有信息", notes = "根据type某一类型字典所有信息")
	@ApiImplicitParams({ @ApiImplicitParam(name = "type", value = "字典类型信息", required = true, paramType = "query") })
	@RequestMapping(value = "getDictionaryByType", method = RequestMethod.GET)
	public RetResult getDictionaryByType(@RequestParam(value = "type", required = true) final String type) {
		List<Dictionary> result = null;
		try {
			result = dictionaryService.getByType(type);
			return RetResponse.makeOKCp(result);

		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}

	}
	
	@ApiOperation(value = "查询事件类型字典信息", notes = "查询事件类型字典信息")
	@RequestMapping(value = "/getEventDictionary", method = RequestMethod.GET)
	public RetResult<List<Map<String, Object>>> getEventDictionary() {
		List<Map<String, Object>> result = null;
		try {
			result = dictionaryService.getEventDictionary();
			return RetResponse.makeOKCp(result);

		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}

	}



}
