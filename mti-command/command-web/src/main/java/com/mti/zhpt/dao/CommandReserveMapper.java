package com.mti.zhpt.dao;

import java.util.List;
import java.util.Map;

import com.mti.zhpt.dto.SubsidiaryFeedback;
import com.mti.zhpt.model.EventReserveDetailEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.mti.zhpt.model.EventReserveEntity;
import com.mti.zhpt.model.EventTask;

public interface CommandReserveMapper {


    List<Map<String,Object>> queryAll(@Param("event_id") String event_id, @Param("param") String param,@Param(value = "ssxqPrefix")String ssxqPrefix,@Param(value = "cType")String cType,@Param(value = "enable")Integer enable,@Param(value = "level")String level);

    List<Map<String, Object>> selectAll(@Param(value = "eventId") String eventId,@Param(value = "level")String level);

    @Select("select d.value_ from cp_system_dict d where d.type_='ALARM_TYPE_CODE' and key_=#{key}")
    String getCaseType(@Param(value = "key") String key);

    List<Map<Object, Object>> queryCommandModel(@Param("reserve_id") String reserve_id);

    /**
     * 查询案件对应单位指令
     *
     * @param event_id 案件id
     * @return
     */
    List<Map<Object, Object>> queryTaskCommand(@Param("event_id") String event_id);

    List<Map<Object, Object>> queryTaskCommandUnit(@Param("event_id") String event_id, @Param("send_org_id") String send_org_id, @Param("orgId") String orgId);

    /**
     * 查询案件对应汇报领导
     *
     * @param event_id 案件id
     * @return
     */
    List<Map<Object, Object>> queryTaskLeaderCommand(@Param("event_id") String event_id);

    /**
     * 查询案件指令警员指令
     *
     * @param event_id 案件id
     * @return
     */
    List<Map<Object, Object>> queryTaskPolice(@Param("event_id") String event_id);

    /**
     * 查询接收的指令-警员指令
     *
     * @param policeId 警员id
     * @return
     */
    List<Map<Object, Object>> queryTaskPoliceByPoliceId(@Param("police_id") String policeId);

    /**
     * 查询接收的指令-单位指令
     *
     * @param orgId 机构id
     * @return
     */
    List<Map<Object, Object>> queryTaskCommandByOrgId(@Param("org_id") String orgId, @Param("eventId") String eventId);

    /**
     * 插入案件对应预案信息
     *
     * @param event_id   案件id
     * @param reserve_id 预案id
     * @return
     */
    @Insert("insert into cp_event_tasks (event_id, id, stage_id, stage_type, remark, dispatch_number, reserve_number, org_id, status, longitude, latitude, instruction_type, time_presence, police_id, police_org_id, surrounding_police_officer, leader_id, leader_org_id, nearby_unit_location_closest, nearby_unit_area_recently, create_time) select #{event_id} as event_id, id, stage_id, stage_type, remark, dispatch_number, reserve_number, org_id, 0 as status, 0 as longitude, 0 as latitude, instruction_type, time_presence, police_id, police_org_id, surrounding_police_officer, leader_id, leader_org_id, nearby_unit_location_closest, nearby_unit_area_recently, now() from cp_reserve_detail where reserve_id=#{reserve_id}")
    int doCommandModel(@Param("event_id") String event_id, @Param("reserve_id") String reserve_id);

    @Insert("insert into cp_event_tasks (event_id, id, remark, dispatch_number, reserve_number, org_id,org_name, status, longitude, latitude, create_time, police_id, " +
            "police_org_id,send_org_id,send_org,send_police_id,send_police,feedback,instruction_type,police_name) " +
            "values(#{event_id}, #{id},#{remark}, #{dispatch_number}, #{reserve_number}, #{org_id},#{org_name}, #{status}, #{longitude}, #{latitude}, now(), #{policeId}," +
            " #{policeOrgId},#{sendOrgId},#{sendOrg},#{sendPoliceId},#{sendPolice},#{feedback},#{instructionType},#{policeName})")
    void taskaddCommand(EventTask eventTask);

    @Update("update cp_event_tasks set status = 1 where event_id = #{event_id} and id = #{id} and stage_id = #{stage_id}")
    void taskupdateCommand(EventTask eventTask);

    @Delete("delete from cp_event_tasks where event_id = #{event_id}")
    int cancelCommandModelCommandModel(String event_id);

    @Select("select max(id) from cp_event_tasks where stage_id = #{stage_id} and event_id = #{event_id}")
    int getMaxStageId(int stage_id, String event_id);

    /**
     * 插入预案信息
     *
     * @param reserve 预案实体类
     * @return
     */
    @Insert("insert into cp_reserve(reserve_id, reserve_name, case_type, create_time, update_time, keyword_, applicable_grade, plan_type,dept, file_, name_, enable_,creator_dept_id) values(#{reserveId}, #{reserveName}, #{caseType}, #{createTime}, #{updateTime}, #{keyword}, #{applicableGrade}, #{planType},#{dept}, #{file}, #{name},#{enable},#{creatorDeptId})")
    int insertReserve(EventReserveEntity reserve);

    int batUpdateById(@Param(value = "list") List<EventReserveEntity> list);

    /**
     * 根据预案id查询预案信息
     *
     * @param reserveId 预案id
     * @return
     */
    EventReserveEntity getById(String reserveId);

    /**
     * 删除预案
     *
     * @param reserveId 预案id
     * @return
     */
    @Delete("delete from cp_reserve where reserve_id = #{reserveId}")
    int removeByreserveId(String reserveId);

    /**
     * 单位指令监控列表查询
     *
     * @param eventId 案件id
     * @return
     */
    List<Map<Object, Object>> listByunitInstructionMonitoring(String eventId);

    /**
     * 删除对应警情警员信息(为下达指令添加指令数据做准备)
     *
     * @param eventId  警情id
     * @param policeId 警员id
     * @return
     */
    @Delete("delete from cp_event_tasks where event_id = #{eventId} and police_id = #{policeId}")
    int deleteByEventIdAndPoliceId(String eventId, String policeId);

    @Insert("insert into cp_event_tasks (event_id, id, remark, create_time,org_id,org_name,send_org_id,send_org,status,feedback,instruction_type,dispatch_number,reserve_number) " +
            "values(#{event_id}, #{id},  #{remark},  now(),  #{org_id} ,#{org_name},#{sendOrgId},#{sendOrg},#{status},#{feedback},#{instructionType},#{dispatch_number},#{reserve_number})")
    void taskaddCommandUnit(EventTask eventTask);

    @Select("select org_id,org_name,create_time,police_id,police_name,content,file_url,tel from cp_event_response where task_id = #{id}")
    List<Map<Object, Object>> queryResponse(@Param("id") String id);

    @Insert("insert into  cp_event_response (id,event_id,task_id,content,org_id,org_name,police_id,police_name,create_time,file_url,tel) " +
            "values(#{id},#{eventId},#{taskId},#{feedbackContent},#{feedbackOrgId},#{feedbackOrgName},#{feedbackPoliceId},#{feedbackPoliceName},#{createTime},#{feedbackFile},#{feedbackPhone}) ")
    void subsidiaryFeedback(SubsidiaryFeedback subsidiaryFeedback);

    @Update("update  cp_event_tasks  set status = #{status} where id = #{id}")
    void updateTaskStatus(@Param("id") String id, @Param("status") Integer status);

    @Select("SELECT org_id FROM cp_event_tasks a WHERE a.event_id = #{eventId} GROUP BY org_id")
    List<Map<Object, Object>> queryDeptCode(@Param("eventId") String eventId);

    @Select("SELECT org_name FROM cp_event_tasks a WHERE a.org_id = #{org_id} and org_name is not null limit 1")
    Map<Object, Object> queryDeptName(@Param("org_id") String org_id);

    List<Map<Object, Object>> queryUnitTask(@Param("eventId") String eventId);

    List<Map<Object, Object>> queryPoliceNumber(@Param("eventId") String eventId);

    List<Map<Object, Object>> newQueryTaskPolice(@Param("eventId") String eventId);

    @Insert("insert into cp_event_timer(event_id,create_time,feedback,remark,status,police_name,police_code,dept_code,dept_name)" +
            "values(#{eventId},#{createTime},#{feedback},#{feedbackContent},100,#{feedbackPoliceName},#{feedbackPoliceId},#{feedbackOrgId},#{feedbackOrgName})")
    void addEventTimer(SubsidiaryFeedback subsidiaryFeedback);
}
