package com.mti.zhpt.dao;

import com.mti.zhpt.model.UsersEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UsersMapper {
    List<UsersEntity> queryName(@Param("name") String name);
}
