package com.mti.zhpt.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mti.zhpt.service.impl.QueryHisServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * 查询历史 yingjj@mti-sh.cn 2019年4月29日10:51
 */
@RestController
@RequestMapping(path = "/queryhis")
@Api(value = "/queryhis", tags = "查询历史")
@Slf4j
public class QueryHisController {

	@Autowired
	private QueryHisServiceImpl queryHisService;

	@RequestMapping(path = "/list", method = RequestMethod.GET)
	@ApiOperation(value = "查询历史查询列表")
	public RetResult<List<Map<String, Object>>> getQueryHis() {

		List<Map<String, Object>> result = null;
		try {
			result = queryHisService.getQueryHis();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/del", method = RequestMethod.GET)
	@ApiOperation(value = "删除历史查询列表")
	public RetResult<Integer> delQueryHis(
			@ApiParam(required = true, name = "id", value = "要删除历史查询记录的ID", example = "1") @RequestParam(value = "id", required = false, defaultValue = "0") int id) {
		int result = 0;
		try {
			result = queryHisService.delQueryHis(id);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

}
