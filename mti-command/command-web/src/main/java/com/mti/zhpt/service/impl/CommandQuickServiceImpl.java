package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.CommandQuickMapper;
import com.mti.zhpt.model.*;
import com.mti.zhpt.constant.TargetMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 快速指挥 实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-05
 */

@Slf4j
@Service("commandQuickServiceImpl")
public class CommandQuickServiceImpl {

	@Autowired
	ContactLinkageUnitServiceImpl contactLinkageUnitService;

	@Autowired
	EventTimerServiceImpl eventTimerService;

	@Autowired
    ContactDutyRelationServiceImpl contactDutyRelationService;

	@Autowired
	ContactDutyCallServiceImpl contactDutyCallService;

	@Autowired
	ContactCardPoliceForceServiceImpl contactCardPoliceForceService;

	@Autowired
	CommandQuickMapper commandQuickMapper;

	/**
	 * 查询全部部门
	 *
	 * @return
	 */
	public Map<Object, Object> listAll() {
		Map<Object, Object> result = new HashMap<>();
		// 查询联动单位
		List<ContactLinkageUnit> contactLinkageUnits = contactLinkageUnitService.listAll();
		result.put("contactLinkageUnits", contactLinkageUnits);
        // 查询警令列表
        List<ContactDutyRelation> contactDutyRelations = contactDutyRelationService.listByAll();
        result.put("contactDutyRelations", contactDutyRelations);
		// 查询职能部门列表
		List<ContactDutyCall> contactDutyCalls = contactDutyCallService.listByAll();
		result.put("contactDutyCalls", contactDutyCalls);

		return result;
	}

	/**
	 * 执行指令
	 *
	 * @param eventTimerEntityList 时间轴列表
	 * @return
	 */
	public boolean doCommand(List<EventTimerEntity> eventTimerEntityList) {
		Date date = new Date();
		eventTimerEntityList.forEach(
			eventTimerEntity -> {
				eventTimerEntity.setFeedback("下达指令");
				eventTimerEntity.setCreateTime(date);
				eventTimerEntity.setStatus((Integer) TargetMap.TARGETMAP.get(TargetMap.ZLD_KEY_YXD));
			}
		);
		return eventTimerService.insertEventTimerBatch(eventTimerEntityList);
	}

	/**
	 * 查询防线数据列表
	 *
	 * @return
	 */
	public List<ContactCardPoliceForce> listByContactCardPoliceForce() {
		List<ContactCardPoliceForce> contactCardPoliceForces = contactCardPoliceForceService.listByAll();
		return contactCardPoliceForces;
	}

	public List<Map<String, Object>> quickCommand(final String orgId, final String level) {
		List<Map<String, Object>> result = null;

		if (level.equals("1")){
			result = commandQuickMapper.detachment(); // 查询所有支队
		} else if(level.equals("2")) {
			String orgIdLike = "'" + orgId + "%'";
			result = commandQuickMapper.brigade(orgIdLike); // 查询所有大队
		} else {
			log.error("level is unknown");
		}

		return result;
	}
}
