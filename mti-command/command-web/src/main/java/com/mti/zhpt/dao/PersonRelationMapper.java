package com.mti.zhpt.dao;

import java.util.List;
import java.util.Map;

import com.mti.zhpt.base.BaseMapper;
import com.mti.zhpt.model.CpPersonRelation;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

public interface PersonRelationMapper extends BaseMapper<CpPersonRelation> {
	@Select("select a.* from cp_person_relation_info a where a.event_id=#{eventId}")
//			"union all " +
//			"select id, name, null as kind, null as relation, null as star, null as possibility, '1' as type, id_number, phone, photourl, " +
//			"1 as person_type, create_time, null as label, null as car_number, null as car_model, null as remark, nation, sex, family_address, #{personid} as event_id  " +
//			"from cp_person p where p.id in (select b.person_id from cp_person_relation_info a, cp_person_relation b where a.event_id=#{personid} and a.id=b.relation_info_id)")
	List<Map<String,Object>> queryPersonRelations(String eventId);

//	/**
//	 * 相关人，车，其他关系保存
//	 *
//	 * @param personId 人员id
//	 * @param relationInfoId 相关信息id
//	 * @return
//	 */
//	@Insert("insert into cp_person_relation(person_id, relation_info_id) values (#{personId}, #{relationInfoId})")
//	int savePersonRelation(String personId, String relationInfoId);

}