package com.mti.zhpt.dao;

import com.mti.zhpt.model.TodolistEntity;

import java.util.List;
import java.util.Map;

public interface TodolistMapper {

	List<Map<String, Object>> queryList(TodolistEntity entity);

	int addByEntity(TodolistEntity entity);
}
