package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.CommandReserveDetailMapper;
import com.mti.zhpt.dao.CpCommandPreEventSummaryMapper;
import com.mti.zhpt.model.CpCommandPreEventSummaryEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe describe
 **/
@Service
@AllArgsConstructor
public class CpCommandPreEventSummaryServiceImpl implements BaseService<CpCommandPreEventSummaryEntity> {
    private final CpCommandPreEventSummaryMapper mapper;
    private final CommandReserveDetailMapper detailMapper;
    @Override
    public void save(CpCommandPreEventSummaryEntity cpCommandPreEventSummaryEntity) {
        mapper.save(cpCommandPreEventSummaryEntity);
    }

    @Override
    public CpCommandPreEventSummaryEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(CpCommandPreEventSummaryEntity cpCommandPreEventSummaryEntity) {
        mapper.update(cpCommandPreEventSummaryEntity);
    }

    @Override
    public PageModel<List<CpCommandPreEventSummaryEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<CpCommandPreEventSummaryEntity>> pageModel = new PageModel<>();
        pageModel.setRecord(mapper.findAllByPage(page,size));
        pageModel.setTotal(mapper.countAllRows());
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }


    public CpCommandPreEventSummaryEntity getData(String riskId){
        CpCommandPreEventSummaryEntity entity = mapper.getEntityByRiskId(riskId);
        if(entity==null){
            entity = new CpCommandPreEventSummaryEntity();
        }
        List<Map<String,Object>> result = mapper.getDataByRiskId(riskId);
        Map<String,Integer> step = new HashMap<>();
        Integer dealItemCount = 0;
        if(result!=null && !result.isEmpty()){
            for (Map<String, Object> map : result) {
                if(step.containsKey(map.get("reserve_id").toString())){
                    step.put(map.get("reserve_id").toString(),step.get(map.get("reserve_id").toString()) + 1);
                }else{
                    step.put(map.get("reserve_id").toString(),1);
                }
                dealItemCount = dealItemCount + Integer.parseInt(map.get("executedcount").toString());
            }
        }
        entity.setStartPreEvent(String.valueOf(step.size()));
        Integer stepCount = 0;
        for(Map.Entry<String,Integer> m:step.entrySet()){
            stepCount = stepCount+m.getValue();
        }
        entity.setOperationStep(stepCount.toString());
        entity.setDealItem(dealItemCount.toString());
        return entity;
    }

    public Object getStepInfoByRiskId(String riskId){
        return detailMapper.getEventDetailByEventId(riskId);
    }
}
