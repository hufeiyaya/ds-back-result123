package com.mti.zhpt.dao;

import com.mti.zhpt.model.EventReserveDetailEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface CommandReserveDetailMapper {

	/**
	 * 批量插入预案详情
	 *
	 * @param reserveId 预案id
	 * @param reserveDetailEntityList 预案详情集合
	 * @return
	 */
	int batchInsertReserveDetail(@Param("reserveId") String reserveId, List<EventReserveDetailEntity> reserveDetailEntityList);

	int insertReserveDetail(@Param(value = "list")List<EventReserveDetailEntity> list);

	/**
	 * 根据预案id查询预案详情列表
	 *
	 * @param reserveId
	 * @return
	 */
	List<EventReserveDetailEntity> listByReserveId(@Param("reserveId") String reserveId);

	List<Map<String,String>> getEventDetailByEventId(@Param(value = "eventId")String eventId);

	int removeByDetailIds(@Param(value = "ids")List<String> ids);

	int batchUpdate(@Param(value = "list")List<EventReserveDetailEntity> list);

	@Select("select id from cp_reserve_detail where reserve_id=#{reserveId}")
	List<String> queryOldDetailIds(@Param(value = "reserveId")String reserveId);

	@Select("select file_ as file,name_ as name from cp_reserve where reserve_id=#{reserveId}")
	List<Map<String,String>> getFilesByReserverId(@Param(value = "reserveId")String reserveId);

}
