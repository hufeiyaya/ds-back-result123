package com.mti.zhpt.dao;

import com.mti.zhpt.model.relationnet.ContactDistrictLeadersEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe describe
 **/
public interface ContactDistrictLeadersMapper {
    List<ContactDistrictLeadersEntity> listAll(
            @Param(value = "page")Integer page,
            @Param(value = "size")Integer size,
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );
    long countRows(
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );

    void save(ContactDistrictLeadersEntity entity);
    void update(ContactDistrictLeadersEntity entity);
    void deleteById(@Param(value = "id")String id);
    ContactDistrictLeadersEntity getOneById(@Param(value = "id")String id);
}
