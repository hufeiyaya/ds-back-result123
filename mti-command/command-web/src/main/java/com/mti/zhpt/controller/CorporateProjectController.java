package com.mti.zhpt.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mti.zhpt.service.impl.CorporateProjectServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 协同方案接口 yingjj@mti-sh.cn 2019年5月6日10:51
 */
@RestController
@RequestMapping(path = "/corporateproject")
@Api(value = "/corporateproject", tags = "协同方案查询")
@Slf4j
public class CorporateProjectController {

	@Autowired
	private CorporateProjectServiceImpl corporateProjectService;

	@RequestMapping(path = "/queryList", method = RequestMethod.GET)
	@ApiOperation(value = "查询协同方案列表")
	public RetResult<List<Map<String, Object>>> queryList() {
		List<Map<String, Object>> result = null;
		try {
			result = corporateProjectService.queryList();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}
}
