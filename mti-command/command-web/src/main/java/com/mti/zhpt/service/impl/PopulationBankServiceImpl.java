package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.PopulationBankMapper;
import com.mti.zhpt.model.CpPopulationBank;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 人口库实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-29
 */
@Service
@Slf4j
@Transactional
public class PopulationBankServiceImpl {

	@Autowired
    private PopulationBankMapper populationBankMapper;

    /**
     * 根据身份证号查询人员信息
     *
     * @param idCard 身份证号
     * @return
     */
    public CpPopulationBank getByIdCard(String idCard) {
        CpPopulationBank result = populationBankMapper.getByIdCard(idCard);
        return result;
    }

}
