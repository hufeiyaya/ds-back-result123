package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactAroundRelationMapper;
import com.mti.zhpt.model.relationnet.ContactAroundRelationEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe describe
 **/
@Slf4j
@Service(value = "ContactAroundRelationEntity")
@AllArgsConstructor
public class ContactAroundRelationServiceImpl implements BaseService<ContactAroundRelationEntity> {
    private final ContactAroundRelationMapper contactAroundRelationMapper;

    @Override
    public void save(ContactAroundRelationEntity contactAroundRelationEntity) {
        contactAroundRelationMapper.save(contactAroundRelationEntity);
    }

    @Override
    public ContactAroundRelationEntity getOneById(String id) {
        return contactAroundRelationMapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        if(!StringUtils.isEmpty(id)){
            contactAroundRelationMapper.deleteById(id);
        }
    }

    @Override
    public void update(ContactAroundRelationEntity entity) {
        contactAroundRelationMapper.update(entity);
    }

    @Override
    public PageModel<List<ContactAroundRelationEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactAroundRelationEntity>> pageModel = new PageModel<>();
        pageModel.setRecord(contactAroundRelationMapper.listAll(page,size,searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setTotal(contactAroundRelationMapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setPage(page);
        pageModel.setSize(size);
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }


}
