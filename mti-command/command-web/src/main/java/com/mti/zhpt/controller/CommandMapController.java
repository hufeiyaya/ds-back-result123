package com.mti.zhpt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.mti.zhpt.model.MapTask;
import com.mti.zhpt.service.impl.CommandMapServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/command/map")
@Api(value = "/command/map", tags = { "图上指挥" })
public class CommandMapController {

	@Autowired
	private CommandMapServiceImpl commandMapService;

	@RequestMapping(path = "/do-map-command", method = RequestMethod.POST)
	@ApiOperation(value = "执行地图指令")
	public RetResult<MapTask> doMapCommand(@RequestBody MapTask mapTask) {
		MapTask result=null;

		if (null == mapTask) {
			return RetResponse.makeErrCp("mapTask不能为空");
		}

		if (null == mapTask.getPersons() && mapTask.getPersons().size() <= 0) {
			return RetResponse.makeErrCp("请选择至少一个警员下达指令");
		}

		try {
			result=commandMapService.doMapCommand(mapTask);
		} catch (Exception e) {
			e.printStackTrace();
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}
	
	@RequestMapping(path = "/resumePoliceStatus", method = RequestMethod.GET)
	@ApiOperation(value = "一键恢复警员状态（内部接口，恢复警员状态至巡逻状态）")
	public RetResult<Object> resumePoliceStatus() {
		try {
			commandMapService.resumePoliceStatus();
		} catch (Exception e) {
			e.printStackTrace();
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp();
	}

	@RequestMapping(path = "/SomePoliceStatus", method = RequestMethod.GET)
	@ApiOperation(value = "恢复单个警员状态（警力状态： 1 出动 2 巡逻）")
	public RetResult<Object> resumePoliceStatus(@RequestParam String policeCode ,@RequestParam Integer status) {
		try {
			commandMapService.somePoliceStatus(policeCode,status);
		} catch (Exception e) {
			e.printStackTrace();
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp();
	}

}
