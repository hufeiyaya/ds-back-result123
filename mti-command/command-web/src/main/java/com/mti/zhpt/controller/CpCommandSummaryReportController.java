package com.mti.zhpt.controller;

import com.mti.zhpt.model.CpCommandSummaryReportEntity;
import com.mti.zhpt.service.BaseService;
import com.mti.zhpt.service.impl.CpCommandSummaryReportServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import com.mti.zhpt.utils.BusinessException;
import com.mti.zhpt.utils.POIUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Optional;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe describe
 **/
@Slf4j
@Api(value = "总结报告模块",tags = "总结报告模块")
@RestController
@AllArgsConstructor
@RequestMapping(value = "/commandSummaryReport")
public class CpCommandSummaryReportController {
    private final BaseService<CpCommandSummaryReportEntity> service;

    @GetMapping(value = "/list")
    @ApiOperation(value = "分页列表")
    public RetResult list(@RequestParam(value = "page")@ApiParam(value = "page",name = "page",required = true) Integer page, @RequestParam(value = "size")@ApiParam(value = "size",name = "size",required = true)Integer size){
        try {
            return RetResponse.makeOKCp(service.list(page,size));
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "保存总结报告")
    public RetResult save(@RequestBody CpCommandSummaryReportEntity entity){
        try {
            service.save(entity);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("error==>{}",e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @PutMapping
    @ApiOperation(value = "更新总结报告")
    public RetResult update(@RequestBody CpCommandSummaryReportEntity entity){
        try {
            service.update(entity);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("error==>{}",e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @GetMapping(value = "/export/word")
    @ApiOperation(value = "总结报告word导出接口")
    public RetResult<String> exportWord(@RequestParam(value = "id")String id){
        CpCommandSummaryReportEntity reportEntity = service.getOneById(id);
        Optional.ofNullable(reportEntity).orElseThrow(()->new BusinessException(500,"未查询到对应报告"));
        try {
            POIUtils.exportWord(reportEntity);
            return RetResponse.makeOKCp("");
        } catch (IOException e) {
            e.printStackTrace();
            log.error("error==>{}",e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @GetMapping
    @ApiOperation(value = "根据ID查询详情")
    public RetResult<CpCommandSummaryReportEntity> getById(
            @RequestParam(value = "riskId")String riskId
    ){
        return RetResponse.makeOKCp(((CpCommandSummaryReportServiceImpl)service).getEntityByRiskId(riskId));
    }
}
