package com.mti.zhpt.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface EventDetailsMapper {
    Map<String,Object> queryDetails(@Param("eventId") String eventId);
    
    @Select("select content from cp_event where event_id = #{eventId}")
    String queryEventTitle(@Param("eventId") String eventId);
}
