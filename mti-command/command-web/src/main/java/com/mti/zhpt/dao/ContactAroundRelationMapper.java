package com.mti.zhpt.dao;

import com.mti.zhpt.model.relationnet.ContactAroundRelationEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe describe
 **/

public interface ContactAroundRelationMapper {
    List<ContactAroundRelationEntity> listAll(
            @Param(value = "page") int page,
            @Param(value = "size") int size,
            @Param(value = "orgName") String orgName,
            @Param(value = "personName") String personName,
            @Param(value = "phone") String phone);

    long countRows(
            @Param(value = "orgName") String orgName,
            @Param(value = "personName") String personName,
            @Param(value = "phone") String phone
    );

    int save(ContactAroundRelationEntity entity);

    int deleteById(@Param(value = "id") String id);

    int update(ContactAroundRelationEntity entity);

    ContactAroundRelationEntity getOneById(@Param(value = "id")String id);
}
