package com.mti.zhpt.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mti.zhpt.service.impl.IpWarningServiceImpl;
import com.mti.zhpt.service.impl.IpWarningUpServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/ipwarning")
@Api(value = "/ipwarning", tags = { "要情" })
@Slf4j
public class IpWarningController {

	@Autowired
	private IpWarningServiceImpl ipWarningService;

	@Autowired
	private IpWarningUpServiceImpl ipWarningUpService;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class,
				new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), false));
	}

	@RequestMapping(path = "/list", method = RequestMethod.GET)
	@ApiOperation(value = "查询要情集合")
	public RetResult<List<Map<String, Object>>> query() {
		List<Map<String, Object>> result = null;
		try {
			result = ipWarningService.queryList();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}
	
	@RequestMapping(path = "/listUp", method = RequestMethod.GET)
	@ApiOperation(value = "查询要情集合")
	public RetResult<List<Map<String, Object>>> queryUp() {
		List<Map<String, Object>> result = null;
		try {
			result = ipWarningUpService.queryListUp();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}
	
	@RequestMapping(path = "/upLoadFile", method = RequestMethod.POST)
	@ApiOperation(value = "上传附件")
	public RetResult<Map<String, Object>> upLoadFile(
			@ApiParam(required = false, name = "file", value = "上传附件") @RequestParam(required = false, value = "file") MultipartFile file) {
		Map<String, Object> result = null;
		try {
			result = ipWarningUpService.upLoadFile(file);
		} catch (Exception e) {
			e.printStackTrace();
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/addIpWarningUp", method = RequestMethod.GET)
	@ApiOperation(value = "上报（添加要情）")
	public RetResult<Map<String, Object>> addIpWarningUp(
			@ApiParam(required = true, name = "title", value = "信息标题", example = "发生警情") @RequestParam(required = true, value = "title") String title,
			@ApiParam(required = true, name = "content", value = "信息内容", example = "有人杀人") @RequestParam(required = true, value = "content") String content,
			@ApiParam(required = true, name = "keywords", value = "关键字", example = "赵三") @RequestParam(required = true, value = "keywords") String keywords,
			@ApiParam(required = true, name = "org_id", value = "上报单位", example = "5") @RequestParam(required = true, value = "org_id") String org_id,
			@ApiParam(required = true, name = "sign_person_id", value = "核稿人", example = "9") @RequestParam(required = true, value = "sign_person_id") int sign_person_id,
			@ApiParam(required = true, name = "level", value = "紧急程度", example = "02") @RequestParam(required = true, value = "level") String level,
			@ApiParam(required = true, name = "casetype", value = "案件类型", example = "010304") @RequestParam(required = true, value = "casetype") String casetype,
			@ApiParam(required = false, name = "appendix_url", value = "上传文件路径", example = "http://192.168.0.200/up/sss.txt") @RequestParam(required = false, value = "appendix_url") String appendix_url,
			@ApiParam(required = false, name = "appendix_name", value = "上传文件名", example = "案件详情.docx") @RequestParam(required = false, value = "appendix_name") String appendix_name
			) {
		Map<String, Object> result = null;
		try {
			result = ipWarningUpService.addIpWarningUp(title, content, keywords, org_id, sign_person_id,
					level, casetype,appendix_url,appendix_name);
		} catch (Exception e) {
			e.printStackTrace();
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/checkIpWarningUp", method = RequestMethod.GET)
	@ApiOperation(value = "上报（复核要情）")
	public RetResult<Map<String, Object>> checkIpWarningUp(
			@ApiParam(required = true, name = "id", value = "要情ID", example = "1") @RequestParam(value = "id", required = true) String id,
			@ApiParam(required = true, name = "title", value = "信息标题", example = "发生警情") @RequestParam(required = true, value = "title") String title,
			@ApiParam(required = true, name = "content", value = "信息内容", example = "有人杀人") @RequestParam(required = true, value = "content") String content,
			@ApiParam(required = true, name = "keywords", value = "关键字", example = "赵三") @RequestParam(required = true, value = "keywords") String keywords,
			@ApiParam(required = true, name = "org_id", value = "上报单位", example = "5") @RequestParam(required = true, value = "org_id") String org_id,
			@ApiParam(required = true, name = "sign_person_id", value = "核稿人", example = "9") @RequestParam(required = true, value = "sign_person_id") int sign_person_id,
			@ApiParam(required = true, name = "level", value = "紧急程度", example = "02") @RequestParam(required = true, value = "level") String level,
			@ApiParam(required = true, name = "casetype", value = "案件类型", example = "010304") @RequestParam(required = true, value = "casetype") String casetype,
			@ApiParam(required = false, name = "appendix_url", value = "上传文件路径", example = "http://192.168.0.200/up/sss.txt") @RequestParam(required = false, value = "appendix_url") String appendix_url,
			@ApiParam(required = false, name = "appendix_name", value = "上传文件名", example = "案件详情.docx") @RequestParam(required = false, value = "appendix_name") String appendix_name) {
		Map<String, Object> result = null;
		try {
			result = ipWarningUpService.checkIpWarningUp(id, title, content, keywords, org_id, sign_person_id,level, casetype,appendix_url,appendix_name);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/approveIpWarningUp", method = RequestMethod.GET)
	@ApiOperation(value = "上报（审批要情）")
	public RetResult<Map<String, Object>> approveIpWarningUp(
			@ApiParam(required = true, name = "id", value = "要情ID", example = "1") @RequestParam(value = "id", required = true) String id,
			@ApiParam(required = true, name = "title", value = "信息标题", example = "发生警情") @RequestParam(required = true, value = "title") String title,
			@ApiParam(required = true, name = "content", value = "信息内容", example = "有人杀人") @RequestParam(required = true, value = "content") String content,
			@ApiParam(required = true, name = "keywords", value = "关键字", example = "赵三") @RequestParam(required = true, value = "keywords") String keywords,
			@ApiParam(required = true, name = "org_id", value = "上报单位", example = "5") @RequestParam(required = true, value = "org_id") String org_id,
			@ApiParam(required = true, name = "sign_person_id", value = "核稿人", example = "9") @RequestParam(required = true, value = "sign_person_id") int sign_person_id,
			@ApiParam(required = true, name = "level", value = "紧急程度", example = "02") @RequestParam(required = true, value = "level") String level,
			@ApiParam(required = true, name = "casetype", value = "案件类型", example = "010304") @RequestParam(required = true, value = "casetype") String casetype,
			@ApiParam(required = false, name = "appendix_url", value = "上传文件路径", example = "http://192.168.0.200/up/sss.txt") @RequestParam(required = false, value = "appendix_url") String appendix_url,
			@ApiParam(required = false, name = "appendix_name", value = "上传文件名", example = "案件详情.docx") @RequestParam(required = false, value = "appendix_name") String appendix_name) {
		Map<String, Object> result = null;
		try {
			result = ipWarningUpService.approveIpWarningUp(id, title, content, keywords, org_id, sign_person_id,level, casetype,appendix_url,appendix_name);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/signIpWarningUp", method = RequestMethod.GET)
	@ApiOperation(value = "上报（签发要情）")
	public RetResult<Map<String, Object>> signIpWarningUp(@ApiParam(required = true, name = "id", value = "要情ID", example = "1") @RequestParam(value = "id", required = true) String id,
			@ApiParam(required = true, name = "title", value = "信息标题", example = "发生警情") @RequestParam(required = true, value = "title") String title,
			@ApiParam(required = true, name = "content", value = "信息内容", example = "有人杀人") @RequestParam(required = true, value = "content") String content,
			@ApiParam(required = true, name = "keywords", value = "关键字", example = "赵三") @RequestParam(required = true, value = "keywords") String keywords,
			@ApiParam(required = true, name = "org_id", value = "上报单位", example = "5") @RequestParam(required = true, value = "org_id") String org_id,
			@ApiParam(required = true, name = "sign_person_id", value = "核稿人", example = "9") @RequestParam(required = true, value = "sign_person_id") int sign_person_id,
			@ApiParam(required = true, name = "level", value = "紧急程度", example = "02") @RequestParam(required = true, value = "level") String level,
			@ApiParam(required = true, name = "casetype", value = "案件类型", example = "010304") @RequestParam(required = true, value = "casetype") String casetype,
			@ApiParam(required = false, name = "appendix_url", value = "上传文件路径", example = "http://192.168.0.200/up/sss.txt") @RequestParam(required = false, value = "appendix_url") String appendix_url,
			@ApiParam(required = false, name = "appendix_name", value = "上传文件名", example = "案件详情.docx") @RequestParam(required = false, value = "appendix_name") String appendix_name) {
		Map<String, Object> result = null;
		try {
			result = ipWarningUpService.signIpWarningUp(id, title, content, keywords, org_id, sign_person_id,level, casetype,appendix_url,appendix_name);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/getRespondOrgList", method = RequestMethod.GET)
	@ApiOperation(value = "查询要情上报、批复单位集合")
	public RetResult<List<Map<String, Object>>> getRespondOrgList() {
		List<Map<String, Object>> result = null;
		try {
			result = ipWarningService.getRespondOrgList();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/getRespondOrgPersonList", method = RequestMethod.GET)
	@ApiOperation(value = "查询要情上报、批复单位对应的人员列表集合")
	public RetResult<List<Map<String, Object>>> getRespondOrgPersonList(
			@ApiParam(required = true, name = "org_id", value = "上报单位", example = "5") @RequestParam(required = true, value = "org_id") String org_id) {
		List<Map<String, Object>> result = null;
		try {
			result = ipWarningService.getRespondOrgPersonList(org_id);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/receive", method = RequestMethod.GET)
	@ApiOperation(value = "接收")
	public RetResult<Map<String, Object>> receive(
			@ApiParam(required = true, name = "id", value = "要情ID", example = "1") @RequestParam(value = "id", required = true) String id) {
		Map<String, Object> result = null;
		try {
			result = ipWarningService.receive(id);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/respond", method = RequestMethod.GET)
	@ApiOperation(value = "批复")
	public RetResult<Map<String, Object>> respond(
			@ApiParam(required = true, name = "id", value = "要情ID", example = "1") @RequestParam(value = "id", required = true) String id,
			@ApiParam(required = true, name = "org_id", value = "批复单位", example = "5") @RequestParam(value = "org_id", required = true) String org_id,
			@ApiParam(required = true, name = "respond_detail", value = "批复内容", example = "1") @RequestParam(value = "respond_detail", required = true) String respond_detail) {
		Map<String, Object> result = null;
		try {
			result = ipWarningService.respond(id, org_id, respond_detail);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/recover", method = RequestMethod.GET)
	@Deprecated
	@ApiOperation(value = "还原要情状态为未接收（内部接口）")
	public RetResult<Map<String, Object>> recover(
			@ApiParam(required = true, name = "id", value = "要情ID", example = "1") @RequestParam(value = "id", required = true) String id) {
		Map<String, Object> result = null;
		try {
			result = ipWarningService.recover(id);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/recoverUp", method = RequestMethod.GET)
	@Deprecated
	@ApiOperation(value = "还原上报要情状态为已拟稿（内部接口）")
	public RetResult<Map<String, Object>> recoverUp(
			@ApiParam(required = true, name = "id", value = "要情ID", example = "1") @RequestParam(value = "id", required = true) String id) {
		Map<String, Object> result = null;
		try {
			result = ipWarningUpService.recover(id);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

}
