package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactPoliceRoomMapper;
import com.mti.zhpt.model.relationnet.ContactPoliceRoomEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
@Service(value = "ContactPoliceRoomEntity")
@AllArgsConstructor
public class ContactPoliceRoomServiceImpl implements BaseService<ContactPoliceRoomEntity> {
    private final ContactPoliceRoomMapper mapper;

    @Override
    public void save(ContactPoliceRoomEntity contactPoliceRoomEntity) {
        mapper.save(contactPoliceRoomEntity);
    }

    @Override
    public ContactPoliceRoomEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactPoliceRoomEntity contactPoliceRoomEntity) {
        mapper.update(contactPoliceRoomEntity);
    }

    @Override
    public PageModel<List<ContactPoliceRoomEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactPoliceRoomEntity>> pageModel = new PageModel<>();
        pageModel.setPage(page);
        pageModel.setSize(size);
        pageModel.setTotal(mapper.countRows(searchTerm[0]));
        pageModel.setRecord(mapper.listAll(page,size,searchTerm[0]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }
}
