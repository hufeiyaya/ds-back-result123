package com.mti.zhpt.controller;

import com.mti.zhpt.base.BaseEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.ControllerDispatchExecution;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import com.mti.zhpt.vo.ModuleVO;
import com.mti.zhpt.vo.RelationNetDispatchVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe 联络网总接口
 **/
@Api(value = "联络网统一接口",description = "联络网统一接口",tags = "联络网统一接口")
@RestController
@AllArgsConstructor
@RequestMapping(value = "/relationNetDispatch")
public class RelationNetDispatchController<E extends BaseEntity> {
    private final ControllerDispatchExecution<Object, E> execution;

    @PostMapping
    @ApiOperation(value = "分页获取联络网数据统一接口",notes = "分页获取联络网数据统一接口")
    public RetResult<PageModel<Object>> list(@RequestBody RelationNetDispatchVO vo){
        try {
            return RetResponse.makeOKCp(execution.execution(vo));
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @DeleteMapping
    @ApiOperation(value = "联络网根据ID删除",notes = "联络网根据ID删除")
    public RetResult deleteById(@RequestParam(value = "id")@ApiParam(value = "id") String id, @RequestParam(value = "moduleType")@ApiParam(value = "tab页类型") String type){
        try {
            execution.executionDelete(id, type);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @PutMapping
    @ApiOperation(value = "更新联络网",notes = "更新联络网")
    public RetResult update(@RequestBody Map<String,Object> entity){
        try {
            execution.executionUpdate(entity);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @PostMapping(value = "/entity")
    @ApiOperation(value = "新增实体",notes = "新增实体")
    public RetResult save(@RequestBody Map<String,Object> entity){
        try {
            execution.executionSave(entity);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @GetMapping(value = "/entity")
    @ApiOperation(value = "根据ID获取联络网实体",notes = "根据ID获取联络网实体")
    public RetResult getOneById(@RequestParam(value = "id")@ApiParam(value = "id") String id,
                                @RequestParam(value = "moduleType")@ApiParam(value = "tab页类型") String type){
        try {
            return RetResponse.makeOKCp(execution.executionFindOneById(id, type));
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @GetMapping(value = "/moduleList")
    @ApiOperation(value = "获取联络网所有菜单",notes = "获取联络网所有菜单")
    public RetResult getModuleList(){
        try {
            return RetResponse.makeOKCp(execution.getModuleList());
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @GetMapping(value = "/firstModuleList")
    @ApiOperation(value = "获取联络网一级菜单",notes = "获取联络网一级菜单")
    public RetResult getFirstModuleList(){
        try {
            return RetResponse.makeOKCp(
                    execution.getModuleList().parallelStream().map(item->{
                      ModuleVO moduleVO = (ModuleVO) item;
                      switch (moduleVO.getType()){
                          case "ContactCityBureauUnitMemberEntity":{
                              moduleVO.setName("市局机关单位联络网");
                          }break;
                          case "ContactDutyRelationEntity":{
                              moduleVO.setName("县区指挥中心联络网");
                          }break;
                          case "ContactLinkageUnitEntity":{
                              moduleVO.setName("联动单位联络网");
                          }break;
                          case "ContactImportDistrictsEntity":{
                              moduleVO.setName("各县区重大紧急联络网");
                          }break;
                          default:return null;
                      }
                      return moduleVO;
                    }).filter(Objects::nonNull).collect(Collectors.toList())
            );
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @GetMapping(value = "/secondModuleList")
    @ApiOperation(value = "获取联络网二级菜单",notes = "获取联络网二级菜单")
    public RetResult getSecondModuleList(@RequestParam(value = "moduleType")String moduleType){
        try {
            return RetResponse.makeOKCp(execution.executeSecondModuleList(null,moduleType));
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }
}
