package com.mti.zhpt.controller;

import com.mti.zhpt.model.CpCommandEventStatisticsEntity;
import com.mti.zhpt.model.CpCommandSummaryEntity;
import com.mti.zhpt.model.EventTimerEntity;
import com.mti.zhpt.service.BaseService;
import com.mti.zhpt.service.impl.CpCommandEventStatisticsServiceImpl;
import com.mti.zhpt.service.impl.EventServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe describe
 **/
@Slf4j
@Api(value = "事件统计",tags = "事件统计")
@RestController
@AllArgsConstructor
@RequestMapping(value = "/commandEventStatistics")
public class CpCommandEventStatisticsController {
    private final BaseService<CpCommandEventStatisticsEntity> service;
    @Autowired
    private EventServiceImpl eventService;

    @GetMapping(value = "/list")
    @ApiOperation(value = "分页列表")
    public RetResult list(@RequestParam(value = "page")@ApiParam(value = "page",name = "page",required = true) Integer page, @RequestParam(value = "size")@ApiParam(value = "size",name = "size",required = true)Integer size){
        try {
            return RetResponse.makeOKCp(service.list(page,size));
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "保存事件统计")
    public RetResult save(@RequestBody CpCommandEventStatisticsEntity entity){
        try {
            service.save(entity);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("error==>{}",e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @PutMapping
    @ApiOperation(value = "更新事件统计")
    public RetResult update(@RequestBody CpCommandEventStatisticsEntity entity){
        try {
            service.update(entity);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("error==>{}",e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @GetMapping
    @ApiOperation(value = "根据ID查询详情")
    public RetResult<CpCommandEventStatisticsEntity> getById(
            @RequestParam(value = "riskId")@ApiParam(value = "警情ID",name = "riskId",required = true) String riskId
    ){
        return RetResponse.makeOKCp(((CpCommandEventStatisticsServiceImpl)service).getTotalData(riskId));
    }



    @RequestMapping(path = "/statistics", method = RequestMethod.GET)
    @ApiOperation(value = "指挥回溯——事件统计——统计")
    public RetResult statistics(@ApiParam(required = true, name = "eventId", value = "案件ID", example = "6") @RequestParam String eventId) {
        Map result = null;
        try {
            result = eventService.statistics(eventId);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }
}
