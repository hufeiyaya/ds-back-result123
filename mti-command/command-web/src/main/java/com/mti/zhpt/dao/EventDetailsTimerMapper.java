package com.mti.zhpt.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

import com.mti.zhpt.model.EventTask;
import com.mti.zhpt.model.EventTimerEntity;
import com.mti.zhpt.model.MapTask;
import com.mti.zhpt.model.websocket.WSCustomEntity;
import org.apache.ibatis.annotations.Select;

public interface EventDetailsTimerMapper {
	List<EventTimerEntity> queryDetailsTimer(@Param("eventId") String eventId);

	Integer insertTimerList(@Param("entityList") List<WSCustomEntity> entityList);

	Integer insertTimer(WSCustomEntity entityList);

	@Delete("delete from cp_event_timer where event_id=#{event_Id}")
	int delTimerList(@Param("event_Id") String event_Id);

	int insertTimerByEventTask(EventTask eventTask);
	
	int insertTimerByMapTask(MapTask mapTask);

	@Select("select status from cp_event_tasks where id= #{tasksId}")
	Integer queryTasksStatus(@Param("tasksId")String tasksId);
}
