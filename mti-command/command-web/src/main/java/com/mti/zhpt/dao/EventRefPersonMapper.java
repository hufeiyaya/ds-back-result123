package com.mti.zhpt.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface EventRefPersonMapper {
	@Select("select a.case_type,b.* from cp_event_personnel a,cp_person b where a.event_id=#{eventId} and a.person_id=b.id")
	List<Map<String,Object>> queryEventRefPersons(String eventId);
	
	@Delete("delete from cp_event_personnel where event_id=#{event_id} and (case_type!='报案人' or case_type is null)")
	int delEventRefPersonsNewAdded(@Param(value = "event_id") String event_id);

}