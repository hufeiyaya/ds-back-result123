package com.mti.zhpt;

import tk.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = "com.mti.zhpt")
@MapperScan(basePackages="com.mti.zhpt.dao")
@EnableScheduling
public class CommandWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommandWebApplication.class, args);
    }

}
