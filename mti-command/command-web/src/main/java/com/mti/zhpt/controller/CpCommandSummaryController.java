package com.mti.zhpt.controller;

import com.mti.zhpt.model.CpCommandSummaryEntity;
import com.mti.zhpt.model.CpCommandSummaryReportEntity;
import com.mti.zhpt.service.BaseService;
import com.mti.zhpt.service.impl.CpCommandSummaryServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe describe
 **/
@Slf4j
@Api(value = "指挥总结表",tags = "指挥总结表")
@RestController
@AllArgsConstructor
@RequestMapping(value = "/commandSummary")
public class CpCommandSummaryController {
    private final BaseService<CpCommandSummaryEntity> service;

    @GetMapping(value = "/list")
    @ApiOperation(value = "分页列表")
    public RetResult list(@RequestParam(value = "page")@ApiParam(value = "page",name = "page",required = true) Integer page, @RequestParam(value = "size")@ApiParam(value = "size",name = "size",required = true)Integer size){
        try {
            return RetResponse.makeOKCp(service.list(page,size));
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "保存指挥总结表")
    public RetResult save(@RequestBody CpCommandSummaryEntity entity){
        try {
            service.save(entity);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("error==>{}",e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @PutMapping
    @ApiOperation(value = "更新指挥总结表")
    public RetResult update(@RequestBody CpCommandSummaryEntity entity){
        try {
            service.update(entity);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("error==>{}",e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @GetMapping
    @ApiOperation(value = "根据ID查询详情")
    public RetResult<CpCommandSummaryEntity> getById(
            @RequestParam(value = "riskId")@ApiParam(value = "警情ID") String riskId
    ){
        return RetResponse.makeOKCp(((CpCommandSummaryServiceImpl)service).getSummaryByRiskId(riskId));
    }
}
