package com.mti.zhpt.dao;

import com.mti.zhpt.model.relationnet.ContactImportDistrictsEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
public interface ContactImportDistrictsMapper {
    List<ContactImportDistrictsEntity> listAll(
            @Param(value = "page")int page,
            @Param(value = "size")int size,
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );
    long countRows(
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );

    void save(ContactImportDistrictsEntity entity);
    void update(ContactImportDistrictsEntity entity);
    void deleteById(@Param(value = "id")String id);
    ContactImportDistrictsEntity getOneById(@Param(value = "id")String id);
}
