package com.mti.zhpt.service.impl;


import com.mti.zhpt.dao.UsersMapper;
import com.mti.zhpt.model.UsersEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: MySpringBoot
 * @description: 省市区服务
 * @author: zpf
 * @create: 2019-01-13 13:06
 **/
@Service("usersServiceImpl")
public class UsersServiceImpl {

    @Resource
    private UsersMapper usersMapper;

    public List<UsersEntity> queryName(String name) {
        //开启分页查询，写在查询语句上方
        //只有紧跟在PageHelper.startPage方法后的第一个Mybatis的查询（Select）方法会被分页。
//        PageHelper.startPage(provinceEntity.getPageNum(), provinceEntity.getPageSize());
        List<UsersEntity> result = usersMapper.queryName(name);
//        PageInfo<ProvinceEntity> pageInfo = new PageInfo<>(result);
        return result;
    }


}
