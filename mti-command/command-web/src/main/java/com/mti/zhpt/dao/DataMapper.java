package com.mti.zhpt.dao;

import com.mti.zhpt.model.websocket.WSEventFeedbackOverTimeEntity;
import com.mti.zhpt.shared.utils.DateUtils;
import com.mti.zhpt.vo.AnyouTjVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface DataMapper {

	@Select("<script> select count(1) from t_lf_jjdb where (bjsj &gt;= #{startTime} and bjsj &lt; #{endTime}) " +
				"<if test='ssxqId!=null and ssxqId.size()&gt;0' >" +
						" and "+
						"<foreach  collection='ssxqId' open='(' close=')' index='index' item='item' separator='OR'>" +
							"gxdwdm like concat('%',#{item},'%') " +
						"</foreach>" +
				"</if>" +
			" </script> ")
	int getCountByJjdb(@Param("startTime") Date startTime, @Param("endTime") Date endTime,@Param(value = "ssxqId")List<String> ssxqId);

	@Select("select count(1) from t_lf_cjdb where (cjsj >= #{startTime} and cjsj < #{endTime})")
	int getCountByCjdb(@Param("startTime") Date startTime, @Param("endTime") Date endTime);

	@Select("select count(1) from cp_event where (create_time >= #{startTime} and create_time < #{endTime}) and level = #{level}")
	int getCountEventByLevel(@Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("level") String level);

	@Select("select count(1) from cp_event where (create_time >= #{startTime} and create_time < #{endTime}) and level = #{level} and event_type = #{type}")
	int getCountEventByLevelAndType(@Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("level") String level,@Param(value = "type")String type);

	@Select("<script> select count(1) from cp_event where (create_time &gt;= #{startTime} and create_time &lt; #{endTime}) and important_event = 1 " +
				"<if test='ssxqId!=null and ssxqId.size()&gt;0'>" +
					" and branch_unit_id in " +
						"<foreach  collection='ssxqId' open='(' close=')' index='index' item='item' separator=','>" +
							"#{item}" +
						"</foreach>" +
				"</if>" +
			"</script> ")
	int getCountEventByYingji(@Param("startTime") Date startTime, @Param("endTime") Date endTime,@Param(value = "ssxqId")List<String> ssxqId);

	@Select("<script> SELECT COUNT ( 1 ) FROM (select * from (select * from cp_event where 1=1" +
				"<if test='ssxqId!=null and ssxqId.size()&gt;0'>" +
					" and branch_unit_id in " +
						"<foreach  collection='ssxqId' open='(' close=')' index='index' item='item' separator=','>" +
							"#{item}" +
						"</foreach>" +
				"</if>" +
			") e inner join (select zd.sfzh from t_lf_zdry zd inner join (SELECT rr.* FROM t_lf_zdry_arming_task_relation rr LEFT JOIN t_lf_zdry_arming_task tt ON rr.at_id = tt.ID WHERE tt.status = 1 AND rr.approve_status &gt;= 5 and rr.approve_status&lt;11 ) a on zd.id = a.sp_id) b on b.sfzh = e.sfzh) c WHERE ( c.create_time &gt;= #{ startTime } AND c.create_time &lt; #{ endTime } ) AND c.event_type = '1' </script> ")
	int getCountEventByZdry(@Param("startTime") Date startTime, @Param("endTime") Date endTime,@Param(value = "ssxqId")List<String> ssxqId);

	@Select("<script> select count(1) from cp_event where (create_time &gt;= #{startTime} and create_time &lt; #{endTime}) and event_type = '2' " +
				"<if test='ssxqId!=null and ssxqId.size()&gt;0'>" +
					" and branch_unit_id in " +
						"<foreach  collection='ssxqId' open='(' close=')' index='index' item='item' separator=','>" +
							"#{item}" +
						"</foreach>" +
				"</if>" +
			"</script> ")
	int getCountEventByZtry(@Param("startTime") Date startTime, @Param("endTime") Date endTime,@Param(value = "ssxqId")List<String> ssxqId);

	@Select("<script> select count(1) from cp_event where (create_time &gt;= #{startTime} and create_time &lt; #{endTime}) and event_type = '0' and important_event = 0 and daily_event = 1 " +
				"<if test='ssxqId!=null and ssxqId.size()&gt;0'>" +
					" and branch_unit_id in " +
						"<foreach  collection='ssxqId' open='(' close=')' index='index' item='item' separator=','>" +
							"#{item}" +
						"</foreach>" +
				"</if>" +
			"</script> ")
	int getCountEventBy110(@Param("startTime") Date startTime, @Param("endTime") Date endTime,@Param(value = "ssxqId")List<String> ssxqId);

	@Select("<script> select " +
			" t.value_ as anyou," +
			" (select count(1) from cp_event z where (z.create_time &gt;= #{startTime} and z.create_time &lt; #{endTime}) and z.case_type like t.key_||'%' " +
				"<if test='ssxqId!=null and ssxqId.size()&gt;0'>" +
				" and branch_unit_id in " +
					"<foreach  collection='ssxqId' open='(' close=')' index='index' item='item' separator=','>" +
						"#{item}" +
					"</foreach>" +
				"</if>" +
			") as count " +
			" from t_sys_dict t where t.delete_ = '0' and t.type_ = 'cp_ay' order by t.sort_ asc </script> ")
	List<Map<String, String>> getCountByAnyou(@Param("startTime") Date startTime, @Param("endTime") Date endTime,@Param(value = "ssxqId")List<String> ssxqId);

	@Select("<script> select " +
			" t.value_ as xiaqu," +
			" (select count(1) from cp_event z where (z.create_time &gt;= #{startTime} and z.create_time &lt; #{endTime}) and z.branch_unit_id = t.key_" +
				"<if test='ssxqId!=null and ssxqId.size()&gt;0'>" +
					" and branch_unit_id in " +
					"<foreach  collection='ssxqId' open='(' close=')' index='index' item='item' separator=','>" +
						"#{item}" +
					"</foreach>" +
				"</if>" +
			") as count " +
			" from t_sys_dict t where t.delete_ = '0' and t.type_ = 'kp_ssxq_event' order by t.sort_ asc </script> ")
	List<Map<String, String>> getCountByXiaqu(@Param("startTime") Date startTime, @Param("endTime") Date endTime,@Param(value = "ssxqId")List<String> ssxqId);

	@Select("<script> " +
			"select key_ as key, value_ as value from t_sys_dict t where t.delete_ = '0' and t.type_ = 'kp_ssxq_event'" +
			" <if test='deptLevel == 2 or deptLevel == 3'>" +
			"and  key_=#{deptCode}" +
			"</if> " +
			"order by t.sort_ asc" +
			"</script>")
	List<Map<String, String>> getXiaqu(@Param("deptCode")String deptCode, @Param("deptLevel")String deptLevel);

	@Select("select value_ as value from t_sys_dict t where t.delete_ = '0' and t.type_ = 'kp_ssxq_event' and t.key_ = #{branchUnitId} order by t.sort_ asc limit 1")
	String getXiaquName(@Param("branchUnitId") String branchUnitId);

	@Select("select count(*) from cp_event z where (z.create_time >= #{startTime} and z.create_time < #{endTime}) and z.branch_unit_id = #{branch_unit_id} ")
	int getTotalByXiaqu(@Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("branch_unit_id") String branch_unit_id);

	@Select("select " +
			" t.value_ as name," +
			" (select count(1) from cp_event z where (z.create_time >= #{startTime} and z.create_time < #{endTime}) and z.case_type like t.key_||'%' and z.branch_unit_id =#{branch_unit_id} ) as count " +
			" from t_sys_dict t where t.delete_ = '0' and t.type_ = 'cp_ay' order by t.sort_ asc")
	List<AnyouTjVo> getCountAnyouByXiaqu(@Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("branch_unit_id") String branch_unit_id);

	@Select("select count(*) from cp_event z where (z.create_time >= #{startTime} and z.create_time < #{endTime}) and (left(z.branch_unit_id,6) = left(#{branch_unit_id},6) or left(z.unit_id,6) = left(#{branch_unit_id},6))  and daily_event = 1 ")
	int getTotalByXiaquNormal(@Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("branch_unit_id") String branch_unit_id);

	@Select("select " +
			" t.value_ as name," +
			" (select count(1) from cp_event z where (z.create_time >= #{startTime} and z.create_time < #{endTime}) and z.case_type like t.key_||'%' and z.branch_unit_id =#{branch_unit_id} and daily_event = 1 ) as count " +
			" from t_sys_dict t where t.delete_ = '0' and t.type_ = 'cp_ay' order by t.sort_ asc")
	List<AnyouTjVo> getCountAnyouByXiaquNormal(@Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("branch_unit_id") String branch_unit_id);

	@Select("select count(*) from cp_event z where (z.create_time >= #{startTime} and z.create_time < #{endTime}) and (left(z.branch_unit_id,6) = left(#{branch_unit_id},6) or left(z.unit_id,6) = left(#{branch_unit_id},6))  and important_event = 1 ")
	int getTotalByXiaquImportant(@Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("branch_unit_id") String branch_unit_id);

	@Select("select " +
			" t.value_ as name," +
			" (select count(1) from cp_event z where (z.create_time >= #{startTime} and z.create_time < #{endTime}) and z.case_type like t.key_||'%' and z.branch_unit_id =#{branch_unit_id} and important_event = 1 ) as count " +
			" from t_sys_dict t where t.delete_ = '0' and t.type_ = 'cp_ay' order by t.sort_ asc")
	List<AnyouTjVo> getCountAnyouByXiaquImportant(@Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("branch_unit_id") String branch_unit_id);


	/**
	 * 根据时间查询重点警情总数
	 *
	 * @param startTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 */
	@Select("select count(1) from cp_event z where z.important_event = 1 and z.create_time >= #{startTime} and z.create_time < #{endTime}")
	int countImportEvent(@Param("startTime") Date startTime, @Param("endTime") Date endTime);

	@Select("select z.zrbm, count(z.zrbm) from cp_event e, t_lf_zdry z where e.status = 0 and e.event_type = '1' and create_time <= #{overTime} and e.sfzh = z.sfzh GROUP BY z.zrbm")
	List<Map<String, String>> getCountEventFeedbackOverTime(@Param("overTime") Date overTime);

	@Select("select e.address,e.create_time,e.event_id,e.event_type,z.mjId,z.oid,e.phone,e.send_time,e.sfzh,z.sspcs,z.ssxq,z.ssxq_id,e.warning_type,z.zrbm from " +
			"(select ev.* from cp_event ev inner join (select zd.sfzh from t_lf_zdry zd inner join (SELECT rr.* FROM t_lf_zdry_arming_task_relation rr LEFT JOIN t_lf_zdry_arming_task tt ON rr.at_id = tt.ID WHERE tt.status = 1 AND rr.approve_status >= 5 and rr.approve_status<11) a on zd.id = a.sp_id) b on b.sfzh = ev.sfzh) e, " +
			" t_lf_zdry z where e.status = 0 and e.event_type = '1' and create_time <= #{overTime} and e.sfzh = z.sfzh")
	List<WSEventFeedbackOverTimeEntity> getCountEventFeedbackOverTimeList(@Param("overTime") Date overTime);

	List<Map<String, Object>> queryPcsByFj(@Param("ssxqId") String ssxqId);
	List<Map<String, Object>> statByPcs(@Param("startTime") String startTime, @Param("endTime") String endTime,@Param(value = "ssxqId")String ssxqId);
}
