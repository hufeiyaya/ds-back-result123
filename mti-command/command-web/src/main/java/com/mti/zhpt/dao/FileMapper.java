package com.mti.zhpt.dao;

import com.mti.zhpt.model.FileEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/12
 * @change
 * @describe describe
 **/
public interface FileMapper extends BaseMapper<FileEntity>{
    List<FileEntity> listFilesByBussId(@Param(value = "bussId")String bussId);

    List<String> listFileIdsByBussId(@Param(value = "bussId")String bussId);

    List<FileEntity> listFilesByBussIds(List<String> bussIds);

    void deleteByBussId(@Param(value = "bussId")String bussId);
}
