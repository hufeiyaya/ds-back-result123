package com.mti.zhpt.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface ForeCastMapper {
	@Select("select * from cp_forecast")
	List<Map<String,Object>> queryForeCast();
	@Update("update cp_forecast set newflag='f' where forecast_id=#{forecastid}")
	int updateForeCastNewFlag(Integer forecastid);
	@Update("update cp_forecast set content=#{content} where forecast_id=#{forecast_id}")
	int updateForeCastContent(Map<String, Object> record);
	@Update("update cp_forecast set status=#{status} where forecast_id=#{foreCastId}")
	int updateForeCastStatus(@Param(value = "foreCastId") Integer foreCastId, @Param(value = "status") Integer status);
	
	@Select("select a.*,ST_DistanceSphere(a.geom,b.geom) as dis from cp_org a,cp_forecast b where b.forecast_id=#{forecastId} and a.sort='pcs' and ST_DistanceSphere(a.geom,b.geom)<#{dis} order by ST_DistanceSphere(a.geom,b.geom)")
	List<Map<String, Object>> queryNearByJurisdictions(@Param(value = "forecastId") Integer forecastId, Integer dis);

	List<Map<String, Object>> queryNearByPolices(@Param(value = "forecastId") Integer forecastId, Integer dis, boolean orderbydis);

	@Select("select a.*,ST_DistanceSphere(a.geom,b.geom) as dis from cp_moniter a,cp_forecast b where b.forecast_id=#{forecastId} and ST_DistanceSphere(a.geom,b.geom)<#{dis} order by ST_DistanceSphere(a.geom,b.geom)")
	List<Map<String, Object>> queryNearByMoniters(@Param(value = "forecastId") Integer forecastId, Integer dis);

	List<Map<String, Object>> queryNearByCompanys(@Param(value = "forecastId") Integer forecastId, Integer dis);
}