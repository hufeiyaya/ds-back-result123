package com.mti.zhpt.shared.core.tasks;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mti.zhpt.dao.EventMapper;
import com.mti.zhpt.shared.websocket.EventWebSocket;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ScheduledService {
	@Autowired
	private EventMapper eventMapper;
	public ScheduledService() {
	}

	//@Scheduled(cron = "0 0/5 * * * *")
	public void scheduled() {
		try {
			EventWebSocket.sendInfo(eventMapper.queryNewestEvent());
		} catch (IOException e) {
			log.error(e.getMessage());
		}
	}
}
