package com.mti.zhpt.controller;

import com.mti.zhpt.model.EventSnapShotReserveDetailEntity;
import com.mti.zhpt.service.IEventReserveSnapService;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(value = "预案记录信息",tags = "预案记录信息")
@RestController
@AllArgsConstructor
@RequestMapping(value = "/command/eventReserve")
public class EventSnapshotDataController {
    private final IEventReserveSnapService reserveSnapshotService;

    @PostMapping(value = "/reserveSnapshotData")
    @ApiOperation(value = "记录预案快照信息")
    public RetResult recordEventReserveSnapShotData(@ApiParam(required = true, name = "event_id", value = "案件ID", example = "6") @RequestParam(value = "event_id") String event_id,
                                                    @ApiParam(required = true, name = "reserve_id", value = "预案ID", example = "1") @RequestParam(value = "reserve_id") String reserve_id){
        try {
            return RetResponse.makeOKCp(reserveSnapshotService.recordSnapShotDataToDetail(event_id, reserve_id));
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = "新增预案快照信息")
    public RetResult addEventSnapshotData(@RequestBody EventSnapShotReserveDetailEntity detailEntity){
        try {
            reserveSnapshotService.addSnapshotData(detailEntity);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("error===>{}",e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @ApiOperation(value = "更新预案记录数据")
    @PostMapping(value = "/detailEntity")
    public RetResult updateEventSnapshotData(@RequestBody EventSnapShotReserveDetailEntity detailEntity){
        try {
            reserveSnapshotService.updateSnapshotData(detailEntity);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @ApiOperation(value = "根据ID获取预案详情记录信息")
    @GetMapping(value = "/detailEntity")
    public RetResult getEventReserveDetailById(@RequestParam(value = "id")@ApiParam(value = "预案详情ID",name = "id") String id){
        try {
            return RetResponse.makeOKCp(reserveSnapshotService.getEntityById(id));
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @PostMapping(value = "/sendCommandBefore")
    @ApiOperation(value = "下发指令前修改状态")
    public RetResult sendCommandBefore(@RequestParam(value = "eventReserveId")@ApiParam(value = "快照信息ID",name = "eventReserveId") String eventReserveId){
        try {
            reserveSnapshotService.updateSnapshotDataStatus(eventReserveId);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @GetMapping(value = "/reserveMonitor")
    @ApiOperation(value = "执行监督列表接口")
    public RetResult getReserveMonitorList(
            @ApiParam(value = "警情",name = "eventId")@RequestParam(value = "eventId") String eventId,
            @ApiParam(value = "预案ID",name = "reserveId")@RequestParam(value = "reserveId")String reserveId){
        try {
            return RetResponse.makeOKCp(reserveSnapshotService.getReserveMonitorList(eventId, reserveId));
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }
}
