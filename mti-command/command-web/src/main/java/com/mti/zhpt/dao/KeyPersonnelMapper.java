package com.mti.zhpt.dao;

import com.mti.zhpt.model.CpCarBank;
import com.mti.zhpt.model.KeyPersonneEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车辆库 mapper
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-29
 */
public interface KeyPersonnelMapper {

    /**
     * 根据车牌号查询车辆信息
     *
     * @param date 日期
     * @param zrbm 责任部门
     * @return
     */
//    @Select("<script>select b.xm,b.sfzh,b.hjd,b.rylb,a.warning_type,a.address,a.send_time ,a.create_time,a.event_id from (select date_trunc('day', t1.create_time) as ls_time ,create_time,sfzh,address,send_time,warning_type,event_id,event_type from(select create_time ,sfzh,address,send_time,warning_type,event_id,event_type from cp_event  ) t1)a," +
//            "(select xm,sfzh,hjd,rylb ,zrbm from t_lf_zdry t2)b where a.ls_time = #{date} " +
//            " and a.sfzh=b.sfzh " +
//            "<if test=\"zrbm!=null and zrbm!=''\">" +
//            "           and zrbm = #{zrbm}" +
//            "        </if> and a.event_type='1'     ORDER BY a.create_time desc</script>")
    List<KeyPersonneEntity> getKP(@Param("date") String date, @Param("zrbm") String zrbm,@Param(value = "oid")String oid,@Param(value = "ssxqId")String ssxqId);

    @Select("select create_time,dept_name,remark from cp_event_timer where event_id = #{event_id} and status='100' ORDER BY create_time asc")
    List<Map<String,Object>> getET(@Param("event_id") String event_id);
}
