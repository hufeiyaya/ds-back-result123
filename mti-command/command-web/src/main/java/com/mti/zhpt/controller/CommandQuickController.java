package com.mti.zhpt.controller;

import com.mti.zhpt.model.ContactCardPoliceForce;
import com.mti.zhpt.model.EventTimerEntity;
import com.mti.zhpt.service.impl.CommandQuickServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 快速指挥控制器
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-05
 */
@RestController
@RequestMapping(path = "/command/quick")
@Api(value = "/command/quick", tags = {"快速指挥"})
@Slf4j
public class CommandQuickController {

    @Autowired
    CommandQuickServiceImpl commandQuickService;

    /**
     * 查询所有部门
     *
     * @return
     */
    @GetMapping(path = "/listAll")
    @ApiOperation(value = "查询所有部门")
    public RetResult<Map<Object, Object>> listAll() {
        Map<Object, Object> result = null;
        try {
            result = commandQuickService.listAll();
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());

        }
        return RetResponse.makeOKCp(result);
    }


    /**
     * 快速指挥
     *
     * @return
     */
    @GetMapping(path = "/quickCommand")
    @ApiOperation(value = "快速指挥")
    public RetResult<List<Map<String, Object>>> quickCommand(
            @ApiParam(required = false, name = "orgId", value = "组织ID", example = "131022") @RequestParam String orgId,
            @ApiParam(required = true, name = "level", value = "角色类型", example = "1") @RequestParam String level) {
        List<Map<String, Object>> result = null;
        try {
            result = commandQuickService.quickCommand(orgId, level);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());

        }
        return RetResponse.makeOKCp(result);
    }


    /**
     * 下达指令
     *  请示汇报--传达
     *  精准指挥--下达
     *  周边资源--下达
     * @param eventTimerEntityList
     * @return
     */
    @PostMapping("/doCommand")
    @ApiOperation(value = "下达指令", notes = "下达指令")
    public RetResult doCommand(@RequestBody List<EventTimerEntity> eventTimerEntityList) {
        Boolean result = commandQuickService.doCommand(eventTimerEntityList);
        return RetResponse.makeOKCp(result);
    }

    /**
     * 查询防线数据列表
     *
     * @return
     */
    @GetMapping("/listByContactCardPoliceForce")
    @ApiOperation(value = "查询防线数据列表", notes = "查询防线数据列表")
    public RetResult listByContactCardPoliceForce() {
        List<ContactCardPoliceForce> contactCardPoliceForces = commandQuickService.listByContactCardPoliceForce();
        return RetResponse.makeOKCp(contactCardPoliceForces);
    }

}
