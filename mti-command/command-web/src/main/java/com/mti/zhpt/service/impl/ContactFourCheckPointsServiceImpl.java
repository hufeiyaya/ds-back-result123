package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactFourCheckPointsMapper;
import com.mti.zhpt.model.relationnet.ContactFourCheckPointsEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
@Service(value = "ContactFourCheckPointsEntity")
@AllArgsConstructor
public class ContactFourCheckPointsServiceImpl implements BaseService<ContactFourCheckPointsEntity> {
    private final ContactFourCheckPointsMapper mapper;

    @Override
    public void save(ContactFourCheckPointsEntity contactFourCheckPointsEntity) {
        mapper.save(contactFourCheckPointsEntity);
    }

    @Override
    public ContactFourCheckPointsEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactFourCheckPointsEntity contactFourCheckPointsEntity) {
        mapper.update(contactFourCheckPointsEntity);
    }

    @Override
    public PageModel<List<ContactFourCheckPointsEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactFourCheckPointsEntity>> pageModel = new PageModel<>();
        pageModel.setSize(size);
        pageModel.setPage(page);
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setRecord(mapper.listAll(page,size,searchTerm[0],searchTerm[1],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }
}
