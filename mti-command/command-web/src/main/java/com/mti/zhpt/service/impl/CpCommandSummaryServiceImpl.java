package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.CpCommandSummaryMapper;
import com.mti.zhpt.model.CpCommandSummaryEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe describe
 **/
@Data
@Service
@AllArgsConstructor
public class CpCommandSummaryServiceImpl implements BaseService<CpCommandSummaryEntity> {
    private final CpCommandSummaryMapper mapper;


    @Override
    public void save(CpCommandSummaryEntity cpCommandSummaryEntity) {
        mapper.save(cpCommandSummaryEntity);
    }

    @Override
    public CpCommandSummaryEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(CpCommandSummaryEntity cpCommandSummaryEntity) {
        mapper.update(cpCommandSummaryEntity);
    }

    @Override
    public PageModel<List<CpCommandSummaryEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<CpCommandSummaryEntity>> pageModel = new PageModel<>();
        pageModel.setRecord(mapper.findAllByPage(page,size));
        pageModel.setTotal(mapper.countAllRows());
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }

    public CpCommandSummaryEntity getSummaryByRiskId(String riskId){
        return mapper.getEntityByRiskId(riskId);
    }
}
