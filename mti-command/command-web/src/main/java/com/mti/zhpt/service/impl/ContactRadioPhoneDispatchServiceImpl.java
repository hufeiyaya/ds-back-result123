package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactRadioPhoneDispatchMapper;
import com.mti.zhpt.model.relationnet.ContactRadioPhoneDispatchEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
@Service(value = "ContactRadioPhoneDispatchEntity")
@AllArgsConstructor
public class ContactRadioPhoneDispatchServiceImpl implements BaseService<ContactRadioPhoneDispatchEntity> {
    private final ContactRadioPhoneDispatchMapper mapper;

    @Override
    public void save(ContactRadioPhoneDispatchEntity contactRadioPhoneDispatchEntity) {
        mapper.save(contactRadioPhoneDispatchEntity);
    }

    @Override
    public ContactRadioPhoneDispatchEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactRadioPhoneDispatchEntity contactRadioPhoneDispatchEntity) {
        mapper.update(contactRadioPhoneDispatchEntity);
    }

    @Override
    public PageModel<List<ContactRadioPhoneDispatchEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactRadioPhoneDispatchEntity>> pageModel = new PageModel<>();
        pageModel.setSize(size);
        pageModel.setPage(page);
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[2]));
        pageModel.setRecord(mapper.listAll(page,size,searchTerm[0],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }
}
