package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.EventMapper;
import com.mti.zhpt.dao.EventRefPersonMapper;
import com.mti.zhpt.dao.RulePersonMapper;
import com.mti.zhpt.model.FileEntity;
import com.mti.zhpt.model.RulePerson;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import com.mti.zhpt.service.RulePersonService;
import com.mti.zhpt.utils.SnowFlake;
import com.mti.zhpt.utils.StringUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/8
 * @change
 * @describe describe
 **/
@Slf4j
@Service
@AllArgsConstructor
public class RulePersonServiceImpl implements RulePersonService {
    private final RulePersonMapper mapper;
    private final FileServiceImpl fileService;
    private final SnowFlake snowFlake;
    private final EventMapper eventMapper;
    @Override
    public void save(RulePerson rulePerson) {
        mapper.save(rulePerson);
        if(rulePerson.getFiles()!=null && !rulePerson.getFiles().isEmpty()){
            rulePerson.getFiles().forEach(item->{
                item.setBussId(rulePerson.getId());
                item.setId(snowFlake.nextId());
            });
            fileService.saveByBatch(rulePerson.getFiles());
        }
    }

    @Override
    public RulePerson getOneById(String id) {
        RulePerson rulePerson = mapper.getOneById(id);
        if(rulePerson!=null){
            rulePerson.setFiles(fileService.getFileListByBussId(rulePerson.getId()));
        }
        return rulePerson;
    }

    @Override
    public void deleteById(String id) {
        //删除发言法语
        mapper.deleteById(id);
        //删除对应的文件
        fileService.deleteByBussId(id);
    }

    @Override
    public void update(RulePerson rulePerson) {
        int result = mapper.update(rulePerson);
        log.info("result==>{}",result);
        if(result==1){
            //删除所有关联文件
            fileService.deleteByBussId(rulePerson.getId());
            //重建关联
            if(rulePerson.getFiles()!=null && !rulePerson.getFiles().isEmpty()){
                rulePerson.getFiles().forEach(item->{
                    item.setBussId(rulePerson.getId());
                    item.setId(snowFlake.nextId());
                });
                fileService.saveByBatch(rulePerson.getFiles());
            }
        }
    }

    @Override
    public PageModel<List<RulePerson>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<RulePerson>> pageModel = new PageModel<>();
        pageModel.setPage(page);
        pageModel.setSize(size);
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2],StringUtils.isEmpty(searchTerm[3])?null: Arrays.asList(searchTerm[3].split(","))));
        List<RulePerson> rulePersonList = mapper.listAll(page,size,searchTerm[0],searchTerm[1],searchTerm[2], StringUtils.isEmpty(searchTerm[3])?null: Arrays.asList(searchTerm[3].split(",")));
        if(rulePersonList!=null && !rulePersonList.isEmpty()){
            List<String> bussIds = rulePersonList.stream().map(RulePerson::getId).collect(Collectors.toList());
            Map<String,List<FileEntity>> files = fileService.getFilesByBussIds(bussIds).stream().collect(Collectors.groupingBy(FileEntity::getBussId));
            rulePersonList.forEach(item->item.setFiles(files.get(item.getId())));
        }
        pageModel.setRecord(rulePersonList);
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }

    @Override
    public List<RulePerson> matchRulePerson(String content, String type, String keyWord) {
        if(StringUtils.isEmpty(content))
            content = "";
        if(StringUtils.isEmpty(type))
            type = "";
        List<RulePerson> rulePersonList = mapper.matchRulePerson(content, type);
        if(!StringUtils.isEmpty(keyWord)){
            rulePersonList = rulePersonList.stream().filter(item->item.getKeyWord().toLowerCase().matches(".*"+keyWord+".*")).collect(Collectors.toList());
        }
        rulePersonList.forEach(r->r.setFiles(fileService.getFileListByBussId(r.getId())));
        return rulePersonList;
    }

    @Override
    public void addWithPowerTimes(String eventId) {
        eventMapper.addWithPowerTimes(eventId);
    }
}
