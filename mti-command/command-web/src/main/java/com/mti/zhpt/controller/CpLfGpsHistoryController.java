package com.mti.zhpt.controller;

import com.mti.zhpt.model.CpLfGpsHistory;
import com.mti.zhpt.service.impl.CpLfGpsHistoryServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 历史轨迹 控制器
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-08
 */
@RestController
@RequestMapping(path = "/gpsHistory")
@Api(value = "/gpsHistory", tags = "历史轨迹")
@Slf4j
public class CpLfGpsHistoryController {

	@Autowired
	CpLfGpsHistoryServiceImpl cpLfGpsHistoryService;

    /**
	 * 根据警员id查询历史轨迹列表
	 *
	 * @param policeId 警员id
	 * @return
	 */
	@GetMapping(path = "/listByPoliceId")
	@ApiOperation(value = "根据警员id查询历史轨迹列表")
	public RetResult<List<CpLfGpsHistory>> listByPoliceId(
			@ApiParam(required = true, name = "policeId", value = "警员id", example = "076176") @RequestParam(value = "policeId", required = true) String policeId,
			@ApiParam(required = true, name = "startTime", value = "开始时间", example = "2019-11-14 00:00:00") @RequestParam(value = "startTime", required = true) String startTime,
			@ApiParam(required = true, name = "endTime", value = "结束时间", example = "2019-11-15 00:00:00") @RequestParam(value = "endTime", required = true) String endTime)  {

		List<CpLfGpsHistory> cpLfGpsHistories = cpLfGpsHistoryService.listByPoliceId(policeId, startTime, endTime);
		return RetResponse.makeOKCp(cpLfGpsHistories);
	}

	/**
	 * 根据警情id查询历史轨迹列表
	 * @return
	 */
	@GetMapping(path = "/listByEventId")
	@ApiOperation(value = "根据警情id查询历史轨迹列表")
	public RetResult<List<CpLfGpsHistory>> listByEventId(
			@ApiParam(required = true, name = "eventId", value = "警情id", example = "379304895406145536") @RequestParam(value = "eventId", required = true) String eventId) {
		if (StringUtils.isBlank(eventId)) {
			return RetResponse.makeErrCp("警情id不能为空");
		}
		List<CpLfGpsHistory> cpLfGpsHistories = cpLfGpsHistoryService.listByEventId(eventId);
		return RetResponse.makeOKCp(cpLfGpsHistories);
	}

}
