package com.mti.zhpt.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface PersonMapper {
	@Select("select a.id,a.longitude,a.latitude,a.status,a.equip,a.name,a.org_id,a.code,a.org" +
			" from cp_police a where a.code=#{code} order by time desc limit 1")
	Map<String, Object> queryPoliceDetails(@Param("code") String code);

	@Update("update cp_police set status=#{status} where code=#{code}")
	int updatePoliceStatus(@Param("code") String code, int status);

	@Update("update cp_police set status=2")
	void resumePoliceStatus();

	@Update("update cp_police set status=#{status} where code = #{policeCode} ")
	void somePoliceStatus(@Param("policeCode")String policeCode,@Param("status") Integer status);

	@Select("SELECT\n" +
			"\tt1.name_,\n" +
			"\tt1.number_,\n" +
			"\tt2.name_ AS dept_name,\n" +
			"\tt2.region_id_ AS dept_code \n" +
			"FROM\n" +
			"\t( SELECT name_, number_ FROM t_pf_person WHERE number_ = #{person} AND delete_ = 0 ) t1,\n" +
			"\t(\n" +
			"SELECT\n" +
			"\ta.name_,\n" +
			"\ta.region_id_ \n" +
			"FROM\n" +
			"\tt_pf_organization a\n" +
			"\tINNER JOIN ( SELECT org_id_ FROM t_pf_organization_person WHERE person_id_ = ( SELECT number_ FROM t_pf_person WHERE number_ = #{person} AND delete_ = 0 ) ) b ON a.region_id_ = b.org_id_ \n" +
			"\t) t2 \n" +
			"\tLIMIT 1 OFFSET 0")
	Map<String, Object> queryPersonTable(@Param("person") String person);
}