package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactMunicipalGovermentMapper;
import com.mti.zhpt.model.relationnet.ContactMunicipalGovermentEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
@Service(value = "ContactMunicipalGovermentEntity")
@AllArgsConstructor
public class ContactMunicipalGovermentServiceImpl implements BaseService<ContactMunicipalGovermentEntity> {
    private final ContactMunicipalGovermentMapper mapper;

    @Override
    public void save(ContactMunicipalGovermentEntity contactMunicipalGovermentEntity) {
        mapper.save(contactMunicipalGovermentEntity);
    }

    @Override
    public ContactMunicipalGovermentEntity getOneById(String id) {
        return getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactMunicipalGovermentEntity contactMunicipalGovermentEntity) {
        mapper.update(contactMunicipalGovermentEntity);
    }

    @Override
    public PageModel<List<ContactMunicipalGovermentEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactMunicipalGovermentEntity>> pageModel = new PageModel<>();
        pageModel.setSize(size);
        pageModel.setPage(page);
        pageModel.setRecord(mapper.listAll(page,size,searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }
}
