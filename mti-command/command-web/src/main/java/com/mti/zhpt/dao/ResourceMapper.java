package com.mti.zhpt.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface ResourceMapper {

	List<Map<Object, Object>> queryAllRootCompanys();
	
	List<Map<String, Object>> queryObscure(String query);

	List<Map<String, Object>> queryPolygon(@Param(value = "geomText") String geomText);

	List<Map<String, Object>> queryCircle(@Param(value = "geomText") String geomText, @Param(value = "radius") Double radius);

	List<Map<String, Object>> queryAllChildCompanys(Map<String, Object> company);

	@Select("select event_id as id,title_ as title,address,content,longitude,latitude,null as reporter,level,to_char(create_time,'YYYY-MM-DD HH24:MI:SS') as create_time,null as duty_org,null as task_org from cp_event where title_ is not null and event_id=#{id}")
	Map<String, Object> queryWarningDetail(@Param(value = "id") String id);

	@Select("select id,name,org,tel,longitude,latitude,status,equip,time from cp_police where name is not null")
	List<Map<String, Object>> queryAllPolices();
	
	@Select("select id,name,longitude,latitude,videostdid,kind from cp_moniter where longitude != 0 and longitude is not null")
	List<Map<String, Object>> queryAllMoniters();
	
	@Select("select id,title as name,address,longitude,latitude from cp_trafficcheckpoint")
	List<Map<String, Object>> queryAllTrafficcheckpoints();
	
	@Select("select id,name,org,tel,longitude,latitude,status,equip,time from cp_police where name is not null and status = 1")
	List<Map<String, Object>> queryAllCars();

	@Select("select event_id as id,title_ as title,address,content,longitude,latitude,null as reporter,level,to_char(create_time,'YYYY-MM-DD HH24:MI:SS') as create_time,null as duty_org,null as task_org from cp_event where title_ is not null and create_time>=to_timestamp('2019-05-06 00:00:00','YYYY-MM-DD HH24:MI:SS') and create_time<to_timestamp(#{fakenow},'YYYY-MM-DD HH24:MI:SS')")
	List<Map<String, Object>> queryAllWarnings(@Param(value = "fakenow") String fakenow);
}
