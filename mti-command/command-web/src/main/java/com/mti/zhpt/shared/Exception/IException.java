package com.mti.zhpt.shared.Exception;

public interface IException {
    String getErrorCode();

    void setErrorArguments(Object... paramVarArgs);

    Object[] getErrorArguments();

}