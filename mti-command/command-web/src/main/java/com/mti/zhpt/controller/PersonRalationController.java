package com.mti.zhpt.controller;

import com.mti.zhpt.model.CpPersonRelation;
import com.mti.zhpt.service.impl.PersonRelationServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/personrealtion")
@Api(value = "/personrealtion", tags = "警情关系图（相关人，车，其他）")
@Slf4j
public class PersonRalationController {

	@Autowired
	private PersonRelationServiceImpl personRelationService;

	@RequestMapping(path = "/querypersonrelation", method = RequestMethod.GET)
	@ApiOperation(value = "查询人员关系图")
	public RetResult<List<Map<String, Object>>> queryPersonRelations(
			@ApiParam(required = true, name = "eventId", value = "警情ID", example = "1") @RequestParam(value = "eventId", required = true) String eventId) {
		List<Map<String, Object>> result = null;
		try {
			result = personRelationService.queryPersonRelations(eventId);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	/**
	 * 相关人，车，其他保存
	 *
	 * @param cpPersonRelation 相关人，车，其他信息
	 * @return
	 */
	@ApiOperation(value = "相关人，车，其他保存")
	@PostMapping(path = "/savePersonRelationInfo")
	public RetResult<Integer> savePersonRelationInfo(@RequestBody CpPersonRelation cpPersonRelation) {
		if (null == cpPersonRelation) {
			return RetResponse.makeErrCp("相关人，车，其他信息不能为空");
		}

		if (null == cpPersonRelation.getType()) {
			return RetResponse.makeErrCp("类型不能为空");
		}

		cpPersonRelation.setCreateTime(new Date());
		int result = personRelationService.save(cpPersonRelation);

//		String personId = cpPersonRelation.getPersonId();
//		if (StringUtils.isNotBlank(personId)) {
//			String relationInfoId = cpPersonRelation.getId();
//			personRelationService.savePersonRelation(personId, relationInfoId);
//		}
		return RetResponse.makeOKCp(result);
	}

}
