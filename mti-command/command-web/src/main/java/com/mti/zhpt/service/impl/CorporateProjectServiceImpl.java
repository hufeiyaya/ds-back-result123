package com.mti.zhpt.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mti.zhpt.dao.CorporateProjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 协同方案服务
 * @Author: yingjj@mti-sh.cn
 * @Date: 2019/5/6 11:14
 */
@Service("corporateProjectServiceImpl")
@Slf4j
public class CorporateProjectServiceImpl {

	@Autowired
	private CorporateProjectMapper corporateProjectMapper;

	/**
	 * @Description:查询协同方案列表
	 * @Author: yingjj@mti-sh.cn
	 * @Date: 2019/5/6
	 */
	public List<Map<String, Object>> queryList() {
		List<Map<String, Object>> results = null;
		try {
			results=corporateProjectMapper.queryList();
			for (Map<String, Object> res : results) {
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> poilist = (List<Map<String, Object>>) res.get("pois");
				for (Map<String, Object> poi : poilist) {
				Double[] coordinates = { (Double) poi.get("longitude"), (Double) poi.get("latitude") };
				poi.put("coordinates", coordinates);
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return results;
	}
}
