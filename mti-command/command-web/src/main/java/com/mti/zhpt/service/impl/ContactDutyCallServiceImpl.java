package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactDutyCallMapper;
import com.mti.zhpt.model.ContactDutyCall;
import com.mti.zhpt.model.relationnet.ContactDutyCallEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
@Service(value = "ContactDutyCallEntity")
@AllArgsConstructor
public class ContactDutyCallServiceImpl implements BaseService<ContactDutyCallEntity> {
    private final ContactDutyCallMapper mapper;

    @Override
    public void save(ContactDutyCallEntity contactDutyCallEntity) {
        mapper.save(contactDutyCallEntity);
    }

    @Override
    public ContactDutyCallEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactDutyCallEntity contactDutyCallEntity) {
        mapper.update(contactDutyCallEntity);
    }

    @Override
    public PageModel<List<ContactDutyCallEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactDutyCallEntity>> pageModel = new PageModel<>();
        pageModel.setSize(size);
        pageModel.setPage(page);
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setRecord(mapper.listAll(page,size,searchTerm[0],searchTerm[1],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }

    /**
     * 查询全部职能部门数据列表
     *
     * @return
     */
    public List<ContactDutyCall> listByAll() {
        List<ContactDutyCall> contactDutyCallList = mapper.listByAll();
        return contactDutyCallList;
    }

}
