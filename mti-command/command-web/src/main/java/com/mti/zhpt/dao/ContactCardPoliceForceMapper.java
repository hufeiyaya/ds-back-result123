package com.mti.zhpt.dao;

import com.mti.zhpt.model.ContactCardPoliceForce;
import com.mti.zhpt.model.relationnet.ContactCardPoliceForceEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 卡点数据
 **/
public interface ContactCardPoliceForceMapper {
    List<ContactCardPoliceForceEntity> listAll(
            @Param(value = "page")Integer page,
            @Param(value = "size")Integer size,
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );
    long countRows(
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );

    void save(ContactCardPoliceForceEntity entity);
    void update(ContactCardPoliceForceEntity entity);
    void deleteById(@Param(value = "id")String id);
    ContactCardPoliceForceEntity getOneById(@Param(value = "id")String id);

    /**
     * 查询防线数据列表
     *
     * @return
     */
    @Select("select region_id AS regionId, unit_name as unitName, card_name as cardName, location_ as location, responsible_unit as responsibleUnit, distance_ as distance, type_ as type, principal_ as principal, employees_number as employeesNumber, carrying_equipment as carryingEquipment, arrival_time as arrivalTime, phone_radio as phoneRadio, phone_ as phone, line_type as lineType from t_contact_card_police_force where line_type = 1 or line_type = 2")
    List<ContactCardPoliceForce> listByAll();

}
