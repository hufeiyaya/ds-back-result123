package com.mti.zhpt.dao;

import com.mti.zhpt.model.CpCarBank;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 车辆库 mapper
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-29
 */
public interface CarBankMapper {

	/**
	 * 根据车牌号查询车辆信息
	 *
	 * @param carNumber 车牌号
	 * @return
	 */
	@Select("select a.id_ as id, a.name_ as name, a.phone_ as phone, a.car_number as carNumber, a.car_model as carModel, a.car_photo as carPhoto, a.create_time as createTime from cp_car_bank a where a.car_number = #{carNumber}")
	CpCarBank getByCarNumber(String carNumber);

}