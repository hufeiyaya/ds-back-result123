package com.mti.zhpt.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface IpWarningMapper {
	
	@Select("select a.*,b.dept_name as org_name from cp_ipwarning a left join cp_org b on a.org_id=b.id")
	List<Map<String, Object>> queryList();
	
	@Select("select * from cp_ipwarning where id=#{id}")
	Map<String, Object> selectById(@Param(value = "id") String id);

	@Update("update cp_ipwarning set receive_time=#{now},status=1,receiver='张一凡' where id=#{id}")
	int receive(@Param(value = "id") String id, @Param(value = "now") Date now);

	@Update("update cp_ipwarning set receive_time=null,status=0,receiver=null,responder=null,respond_detail=null,respond_time=null,respond_org_id=null where id=#{id}")
	int recover(@Param(value = "id") String id);
	
	@Update("update cp_ipwarning set receive_time=null,status=0,receiver=null,responder=null,respond_detail=null,respond_time=null,respond_org_id=null")
	int recoverAll();

	@Update("update cp_ipwarning set status=2,responder='张一凡',respond_detail=#{respond_detail},respond_time=#{now},respond_org_id=#{org_id} where id=#{id}")
	int respond(@Param(value = "id") String id, @Param(value = "org_id") String org_id, @Param(value = "respond_detail") String respond_detail, @Param(value = "now") Date now);

	@Select("select id as org_id,dept_name as org_name from cp_org where parent_code='0000' and sort='pcs'")
	List<Map<String, Object>> getRespondOrgList();

	@Select("select stuff_id as sign_person_id,stuff_name as name from cp_org_stuff where org_id=#{org_id}")
	List<Map<String, Object>> getRespondOrgPersonList(@Param(value = "org_id") String org_id);

}
