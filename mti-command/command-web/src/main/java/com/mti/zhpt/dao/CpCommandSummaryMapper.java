package com.mti.zhpt.dao;

import com.mti.zhpt.model.CpCommandSummaryEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe 指挥总结
 **/
public interface CpCommandSummaryMapper extends BaseMapper<CpCommandSummaryEntity> {
    CpCommandSummaryEntity getEntityByRiskId(@Param(value = "riskId")String riskId);
}
