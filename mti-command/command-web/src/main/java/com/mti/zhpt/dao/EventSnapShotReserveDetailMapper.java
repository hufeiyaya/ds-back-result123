package com.mti.zhpt.dao;

import com.mti.zhpt.model.EventSnapShotReserveDetailEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;
import java.util.Map;

public interface EventSnapShotReserveDetailMapper {
    void insertByBatch(@Param(value = "list")List<EventSnapShotReserveDetailEntity> list);

    EventSnapShotReserveDetailEntity getById(@Param(value = "id")String id);

    void updateById(@Param(value = "entity")EventSnapShotReserveDetailEntity entity);

    @Update("update cp_event_reserve_detail set status='1' where id=#{eventReserveId}")
    void updateSnapshotDataStatus(@Param(value = "eventReserveId")String eventReserveId);

    void batchUpdateSnapshotDataStatus(@Param(value = "ids")List<String> ids);

    @Select("select * from cp_event_reserve_detail where event_id=#{eventId}")
    List<EventSnapShotReserveDetailEntity> getDetailList(@Param(value = "eventId")String eventId);

    List<Map<Object,Object>> getStepInfoByEventId(@Param(value = "eventId")String eventId,@Param(value = "reserveId")String reserveId,@Param(value = "status")Integer status);

    List<Map<Object,Object>> getPersonStepInfoByEventId(@Param(value = "eventId")String eventId,@Param(value = "reserveId")String reserveId,@Param(value = "status")Integer status);

    @Select("select A.time_presence,A.surrounding_police_officer,A.org_id,A.org_name,A.stage_id,A.police_id,A.police_name,A.police_org_id,A.police_org_name,A.stage_type,A.ID,A.remark,A.leader_id,A.leader_org_id,A.leader_org_name leader_org_name from cp_reserve_detail A where A.reserve_id=#{reserveId} and A.instruction_type=1 order by A.stage_id,ID")
    List<Map<Object,Object>> getTemplatePersonStepInfoByEventId(@Param(value = "reserveId")String reserveId);

    List<Map<Object,Object>> getTemplateStepInfoByEventId(@Param(value = "reserveId")String reserveId);

    List<Map<Object,Object>> getTemplateLeaderStepInfoByEventId(@Param(value = "reserveId")String reserveId);

    List<Map<Object,Object>> getLeaderStepInfoByEventId(@Param(value = "eventId")String eventId,@Param(value = "reserveId")String reserveId,@Param(value = "status")Integer status);

    @Select("select count(*) from cp_event_reserve_detail where event_id=#{eventId} and reserve_id=#{reserveId}")
    long existByReserveIdAndEventId(@Param(value = "eventId")String eventId,@Param(value = "reserveId")String reserveId);


}
