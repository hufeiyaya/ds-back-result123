package com.mti.zhpt.dao;

import com.mti.zhpt.model.CpPerson;
import com.mti.zhpt.model.EventEntity;
import com.mti.zhpt.model.EventQueryPram;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface EventMapper {
	List<Map<String, Object>> queryList(EventEntity entity);
	
	Map<String, Object> queryNewestEvent();

	@Select("select a.*,c.sortname,ST_DistanceSphere(a.geom,b.geom) as dis from cp_org a,cp_event b,cp_org_sort_detail c where b.event_id=#{event_id} and a.sort='pcs' and a.sort=c.sort and ST_DistanceSphere(a.geom,b.geom)<#{dis} order by ST_DistanceSphere(a.geom,b.geom)")
	List<Map<String, Object>> queryNearByJurisdictions(@Param(value = "event_id") String event_id, @Param(value = "dis") Integer dis);

	@Select("select a.*,ST_DistanceSphere(a.geom,b.geom) as dis from cp_org_pcs a,cp_event b where b.event_id=#{event_id} and ST_DistanceSphere(a.geom,b.geom)<#{dis} order by ST_DistanceSphere(a.geom,b.geom)")
	List<Map<String, Object>> queryNearByJurisdictionsFake(@Param(value = "event_id") String event_id, @Param(value = "dis") Integer dis);
	
	@Select("select a.*,ST_DistanceSphere(a.geom,b.geom) as dis from cp_risk a,cp_event b where b.event_id=#{event_id} and ST_DistanceSphere(a.geom,b.geom)<#{dis} and a.event_id=b.event_id order by ST_DistanceSphere(a.geom,b.geom)")
	List<Map<String, Object>> queryNearByRisks(@Param(value = "event_id") String event_id, @Param(value = "dis") Integer dis);

	List<Map<String, Object>> queryNearByPolices(@Param(value = "event_id") String event_id, @Param(value = "dis") Integer dis, @Param(value = "orderbydis") boolean orderbydis);

	@Select("select a.*,ST_DistanceSphere(a.geom,b.geom) as dis from cp_moniter a,cp_event b where a.longitude != 0 and a.longitude is not null and b.event_id=#{event_id} and ST_DistanceSphere(a.geom,b.geom)<#{dis} order by ST_DistanceSphere(a.geom,b.geom)  limit 100")
	List<Map<String, Object>> queryNearByMoniters(@Param(value = "event_id") String event_id, @Param(value = "dis") Integer dis);

	@Select("select a.*,ST_DistanceSphere(a.geom,b.geom) as dis from cp_trafficcheckpoint a,cp_event b where b.event_id=#{event_id} and ST_DistanceSphere(a.geom,b.geom)<#{dis} order by ST_DistanceSphere(a.geom,b.geom)")
	List<Map<String, Object>> queryNearByTrafficcheckpoint(@Param(value = "event_id") String event_id, @Param(value = "dis") Integer dis);

	@Select("select a.*,ST_DistanceSphere(a.geom,b.geom) as dis from cp_event a,cp_event b where b.event_id=#{event_id} and ST_DistanceSphere(a.geom,b.geom)<#{dis} order by ST_DistanceSphere(a.geom,b.geom) limit 100")
	List<Map<String, Object>> queryNearByEvent(@Param(value = "event_id") String event_id, @Param(value = "dis") Integer dis);

	List<Map<String, Object>> queryNearByCompanys(@Param(value = "event_id") String event_id, @Param(value = "dis") Integer dis);

	@Update("<script>update cp_event set status = #{status} " +
			"<if test = 'status != null and status == 1'>, disposal_time = #{now}</if> " +
			"<if test = 'status != null and status == 2'>, disposal_end_time = #{now}</if> " +
			"where event_id=#{eventId}</script>")
	int updateEventStatus(@Param(value = "eventId") String eventId, @Param(value = "status") Integer status, @Param(value = "now") Date now);
	
	@Update("update cp_event set reserve_id=#{reserve_id} where event_id=#{event_id}")
	void updateEventReserve(@Param(value = "event_id") String event_id, @Param(value = "reserve_id") String reserve_id);
	
	int addEvent(Map<String, Object> event);
	
	List<Map<String, Object>> listByPrams(EventQueryPram eventQueryPram);
	
	int addEventByEventEntity(EventEntity event);
	
	@Delete("delete from cp_event where event_id>#{mineventId}")
	int delEventNewCreated(@Param(value = "mineventId") String mineventId);
	
	@Update("update cp_event set create_time=#{now} where event_id=#{eventid}")
	void updateDateToNow(@Param(value = "eventid") String eventid, @Param(value = "now") Date now);

	@Update("update cp_event set send_time=#{sendTime} where event_id=#{eventid}")
	int updateSendTime(@Param(value = "eventid") String eventid, @Param(value = "sendTime") Date sendTime);

	List<Map<String, Object>> listByEvent(EventEntity entity);

	@Update("update cp_event set with_power_times=with_power_times+1 where event_id=#{eventId}")
	void addWithPowerTimes(@Param(value = "eventId")String eventId);

	/**
	 * 日常警情升级为重大警情
	 *
	 * @param eventEntity 警情对象
	 * @return
	 */
	@Update("update cp_event set important_event = 1 ,status = 0 where event_id = #{eventId}")
	int upgradeImportEvent(EventEntity eventEntity);

	/**
	 * 保存报警人信息
	 *
	 * @param entity 人员信息
	 * @return
	 */
	int insertPerson(CpPerson entity);

	/**
	 * 保存报警人和案件关系数据
	 *
	 * @param eventId 警情id
	 * @param personId 人员id
	 * @param caseType 人员类型
	 * @return
	 */
	int insertEventPersonnel(@Param("eventId") String eventId, @Param("personId") String personId, @Param("caseType") String caseType);

	List<EventEntity> listByPage(
			@Param(value = "callPoliceType")List<String> callPoliceTypes,
			@Param(value = "eventStatus")List<String> eventStatus,
			@Param(value = "personTypes")List<String> personTypes,
			@Param(value = "zrbmList")List<String> zrbmList,
			@Param(value = "isControls")List<String> isControls,
			@Param(value = "personLevels")List<String> personLevels,
			@Param(value = "ranges")List<String> ranges,
			@Param(value = "rangeType")String rangeType,
			@Param(value = "startDate")String startDate,
			@Param(value = "endDate")String endDate,
			@Param(value = "start")Integer start,
			@Param(value = "size")Integer size,
			@Param(value = "eventType")String eventType,
			@Param(value = "policeId")String policeId,
            @Param(value = "searchTerm")String searchTerm,
			@Param(value = "overTimeDate")String overTimeDate,
			@Param(value = "ryly")List<String> ryly
	);

	List<EventEntity> listByPageV2(
			@Param(value = "callPoliceType")List<String> callPoliceTypes,
			@Param(value = "eventStatus")List<String> eventStatus,
			@Param(value = "personTypes")List<String> personTypes,
			@Param(value = "zrbmList")List<String> zrbmList,
			@Param(value = "isControls")List<String> isControls,
			@Param(value = "personLevels")List<String> personLevels,
			@Param(value = "ranges")List<String> ranges,
			@Param(value = "rangeType")String rangeType,
			@Param(value = "startDate")String startDate,
			@Param(value = "endDate")String endDate,
			@Param(value = "start")Integer start,
			@Param(value = "size")Integer size,
			@Param(value = "eventType")String eventType,
			@Param(value = "policeId")String policeId,
			@Param(value = "searchTerm")String searchTerm,
			@Param(value = "overTimeDate")String overTimeDate,
			@Param(value = "ryly")List<String> ryly
	);

	List<EventEntity> listByPageV3(
			@Param(value = "callPoliceType")List<String> callPoliceTypes,
			@Param(value = "eventStatus")List<String> eventStatus,
			@Param(value = "personTypes")List<String> personTypes,
			@Param(value = "zrbmList")List<String> zrbmList,
			@Param(value = "isControls")List<String> isControls,
			@Param(value = "personLevels")List<String> personLevels,
			@Param(value = "ranges")List<String> ranges,
			@Param(value = "rangeType")String rangeType,
			@Param(value = "startDate")String startDate,
			@Param(value = "endDate")String endDate,
			@Param(value = "start")Integer start,
			@Param(value = "size")Integer size,
			@Param(value = "eventType")String eventType,
			@Param(value = "policeId")String policeId,
			@Param(value = "searchTerm")String searchTerm,
			@Param(value = "overTimeDate")String overTimeDate,
			@Param(value = "ryly")List<String> ryly,
			@Param(value = "userOrgId")String userOrgId,
			@Param(value = "userZrbm")String userZrbm
	);

	long countListByPage(
			@Param(value = "callPoliceType")List<String> callPoliceTypes,
			@Param(value = "eventStatus")List<String> eventStatus,
			@Param(value = "personTypes")List<String> personTypes,
			@Param(value = "zrbmList")List<String> zrbmList,
			@Param(value = "isControls")List<String> isControls,
			@Param(value = "personLevels")List<String> personLevels,
			@Param(value = "ranges")List<String> ranges,
			@Param(value = "rangeType")String rangeType,
			@Param(value = "startDate")String startDate,
			@Param(value = "endDate")String endDate,
			@Param(value = "eventType")String eventType,
			@Param(value = "policeId")String policeId,
            @Param(value = "searchTerm")String searchTerm,
			@Param(value = "overTimeDate")String overTimeDate,
			@Param(value = "ryly")List<String> ryly
	);

	@Select("select e.*,d.value_ as caseTypeVal from cp_event e inner join cp_system_dict d on e.case_type = d.key_ where d.type_='ALARM_TYPE_CODE' and event_id=#{eventId}")
	EventEntity getByEventId(@Param(value = "eventId")String eventId);

	@Select("select count(*) from  cp_command_event_statistics where event_id  = #{eventId}" +
			"UNION ALL \n" +
			"select count(*) from cp_event_response where event_id = #{eventId}" +
			"UNION ALL \n" +
			"select count(*) from cp_event_tasks where event_id = #{eventId} and status in (10,20,30,40,50,60)")
	List<Long> countData(@Param("eventId") String eventId);
}
