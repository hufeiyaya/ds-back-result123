package com.mti.zhpt.controller;

import com.mti.zhpt.model.CpCarBank;
import com.mti.zhpt.service.impl.CarBankServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 车辆库控制器
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-29
 */
@RestController
@RequestMapping(path = "/carBank")
@Api(value = "/carBank", tags = "车辆库")
@Slf4j
public class CarBankController {

	@Autowired
	CarBankServiceImpl carBankService;

    /**
	 * 根据车牌号查询车辆信息
	 *
	 * @param carNumber 车牌号
	 * @return
	 */
	@GetMapping(path = "/getByCarNumber")
	@ApiOperation(value = "根据车牌号查询车辆信息")
	public RetResult<CpCarBank> getByCarNumber(
			@ApiParam(required = true, name = "carNumber", value = "车牌号", example = "111") @RequestParam(value = "carNumber", required = true) String carNumber) {
		if (StringUtils.isBlank(carNumber)) {
			return RetResponse.makeErrCp("车牌号不能为空");
		}
		CpCarBank cpCarBank = carBankService.getByCarNumber(carNumber);
		return RetResponse.makeOKCp(cpCarBank);
	}

}
