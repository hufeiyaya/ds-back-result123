package com.mti.zhpt.dao;

import com.mti.zhpt.model.relationnet.ContactFourCheckPointsEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
public interface ContactFourCheckPointsMapper {
    List<ContactFourCheckPointsEntity> listAll(
            @Param(value = "page")int page,
            @Param(value = "size")int size,
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );
    long countRows(
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );

    void save(ContactFourCheckPointsEntity entity);
    void update(ContactFourCheckPointsEntity entity);
    void deleteById(@Param(value = "id")String id);
    ContactFourCheckPointsEntity getOneById(@Param(value = "id")String id);
}
