package com.mti.zhpt.service.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mti.zhpt.dao.ForeCastMapper;

import lombok.extern.slf4j.Slf4j;

/**
* @Description: 预测预警
 * @Author: yingjj@mti-sh.cn
* @Date: 2019/4/23 11:14
*/
@Service
@Slf4j
public class ForeCastServiceImpl {

	@Autowired
    private ForeCastMapper foreCastMapper;
    
    public List<Map<String,Object>> queryForeCast() {
        List<Map<String,Object>> result = null;
        try {
            result = foreCastMapper.queryForeCast();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return result;
    }
    
    /**
	 * @Description:查询周边资源
	 * @Author: yingjj@mti-sh.cn
	 * @Date: 2019/4/26
	 */
	public Map<String, Object> queryNearBy(Integer forecastId, Integer dis, boolean orderbydis) {
		List<Map<String, Object>> resultJurisdictions = null;
		List<Map<String, Object>> resultPolices = null;
		List<Map<String, Object>> resultMoniters = null;
		List<Map<String, Object>> resultCompanys = null;
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			resultJurisdictions = foreCastMapper.queryNearByJurisdictions(forecastId, dis);
			for (Map<String, Object> res : resultJurisdictions) {
				Double[] coordinates = { (Double) res.get("longitude"), (Double) res.get("latitude") };
				res.put("coordinates", coordinates);
			}
			resultPolices = foreCastMapper.queryNearByPolices(forecastId, dis,orderbydis);
			for (Map<String, Object> res : resultPolices) {
				Double[] coordinates = { (Double) res.get("longitude"), (Double) res.get("latitude") };
				res.put("coordinates", coordinates);
			}
			resultMoniters = foreCastMapper.queryNearByMoniters(forecastId, dis);
			for (Map<String, Object> res : resultMoniters) {
				Double[] coordinates = { (Double) res.get("longitude"), (Double) res.get("latitude") };
				res.put("coordinates", coordinates);
			}
			resultCompanys = foreCastMapper.queryNearByCompanys(forecastId, dis);
			for (Map<String, Object> res : resultCompanys) {
				@SuppressWarnings("unchecked")
				List<Map<String, Object>> companys = (List<Map<String, Object>>) res.get("companys");
				for (Map<String, Object> com : companys) {
					Double[] coordinates = { (Double) com.get("longitude"), (Double) com.get("latitude") };
					com.put("coordinates", coordinates);
				}
			}
			result.put("Jurisdictions", resultJurisdictions);
			result.put("Polices", resultPolices);
			result.put("Moniters", resultMoniters);
			result.put("Companys", resultCompanys);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return result;
	}
    
    public int updateForeCastNewFlag(Integer forecastid) {
        int result = 0;
        try {
            result = foreCastMapper.updateForeCastNewFlag(forecastid);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return result;
    }
    
    public int updateForeCastContent(Map<String, Object> record) {
        int result = 0;
        try {
            result = foreCastMapper.updateForeCastContent(record);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return result;
    }
    
	public int updateForeCastStatus(Integer foreCastId,Integer status) {
		int result = foreCastMapper.updateForeCastStatus(foreCastId,status);
		return result;
	}


}
