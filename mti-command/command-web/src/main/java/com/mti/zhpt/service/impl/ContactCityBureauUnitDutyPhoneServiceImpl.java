package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactCityBureauUnitDutyPhoneMapper;
import com.mti.zhpt.model.relationnet.ContactCityBureauUnitDutyPhoneEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe describe
 **/
@Service(value = "ContactCityBureauUnitDutyPhoneEntity")
@AllArgsConstructor
public class ContactCityBureauUnitDutyPhoneServiceImpl implements BaseService<ContactCityBureauUnitDutyPhoneEntity> {
    private final ContactCityBureauUnitDutyPhoneMapper mapper;

    @Override
    public void save(ContactCityBureauUnitDutyPhoneEntity contactCityBureauUnitDutyPhoneEntity) {
        mapper.save(contactCityBureauUnitDutyPhoneEntity);
    }

    @Override
    public ContactCityBureauUnitDutyPhoneEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactCityBureauUnitDutyPhoneEntity contactCityBureauUnitDutyPhoneEntity) {
        mapper.update(contactCityBureauUnitDutyPhoneEntity);
    }

    @Override
    public PageModel<List<ContactCityBureauUnitDutyPhoneEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactCityBureauUnitDutyPhoneEntity>> pageModel = new PageModel<>();
        pageModel.setPage(page);
        pageModel.setSize(size);
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setRecord(mapper.listAll(page, size,searchTerm[0],searchTerm[1],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }
}
