package com.mti.zhpt.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mti.zhpt.dao.QueryHisMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 历史日志记录
 * @Author: yingjj@mti-sh.cn
 * @Date: 2019/4/29 11:14
 */
@Service("queryHisServiceImpl")
@Slf4j
public class QueryHisServiceImpl {

	@Autowired
	private QueryHisMapper queryHisMapper;

	/**
	 * @param Sting 
	 * @Description:
	 * @Author: yingjj@mti-sh.cn
	 * @Date: 2019/4/29 11:14
	 */
	public void logQuery(String query) {
		try {
			queryHisMapper.logQuery(new Date(),query);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
	
	public List<Map<String, Object>> getQueryHis() {
		List<Map<String, Object>> queryhis =null;
		try {
			queryhis = queryHisMapper.getQueryHis();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return queryhis;
	}
	
	public int delQueryHis(int id) {
		int result =0;
		try {
			result = queryHisMapper.delQueryHis(id);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return result;
	}

}
