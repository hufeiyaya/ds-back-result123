package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactImportDistrictsMapper;
import com.mti.zhpt.model.relationnet.ContactImportDistrictsEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
@Service(value = "ContactImportDistrictsEntity")
@AllArgsConstructor
public class ContactImportDistrictsServiceImpl implements BaseService<ContactImportDistrictsEntity> {
    private  final ContactImportDistrictsMapper mapper;

    @Override
    public void save(ContactImportDistrictsEntity contactImportDistrictsEntity) {
        mapper.save(contactImportDistrictsEntity);
    }

    @Override
    public ContactImportDistrictsEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactImportDistrictsEntity contactImportDistrictsEntity) {
        mapper.update(contactImportDistrictsEntity);
    }

    @Override
    public PageModel<List<ContactImportDistrictsEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactImportDistrictsEntity>> pageModel = new PageModel<>();
        pageModel.setRecord(mapper.listAll(page,size,searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setPage(page);
        pageModel.setSize(size);
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }
}
