package com.mti.zhpt.dao;

import com.mti.zhpt.model.CpCommandSummaryReportEntity;
import org.apache.ibatis.annotations.Param;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe 指挥总结报告
 **/
public interface CpCommandSummaryReportMapper extends BaseMapper<CpCommandSummaryReportEntity> {
    CpCommandSummaryReportEntity getEntityByRiskId(@Param(value = "riskId")String riskId);
}
