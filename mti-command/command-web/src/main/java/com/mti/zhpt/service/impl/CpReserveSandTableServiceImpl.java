package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.CpReserveSendTableMapper;
import com.mti.zhpt.model.CpReserveSandTable;
import com.mti.zhpt.utils.SnowFlake;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 预案沙盘实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-02
 */
@Service
@Slf4j
@Transactional
public class CpReserveSandTableServiceImpl {

	@Autowired
    CpReserveSendTableMapper cpReserveSendTableMapper;

	@Autowired
	SnowFlake snowFlake;

    /**
     * 根据预案id查询沙盘信息列表
     *
     * @param reserveId 预案id
     * @return
     */
    public List<CpReserveSandTable> listByReserveId(String reserveId) {
        List<CpReserveSandTable> result = cpReserveSendTableMapper.listByReserveId(reserveId);
        return result;
    }

    /**
     *
     * 批量保存预案沙盘
     *
     * @param cpReserveSandTableList 预案沙盘对象集合
     * @return
     */
    @Transactional
    public int saveCpReserveSandTableBatch(List<CpReserveSandTable> cpReserveSandTableList) {

        cpReserveSendTableMapper.deleteByReserveId(cpReserveSandTableList.get(0).getReserveId());

        Date date = new Date();
        cpReserveSandTableList.forEach(cpReserveSandTable -> {
            cpReserveSandTable.setId(snowFlake.nextId());
            cpReserveSandTable.setCreateTime(date);
        });
        Integer result = cpReserveSendTableMapper.saveCpReserveSandTableBatch(cpReserveSandTableList);

        return result;
    }

}
