package com.mti.zhpt.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface IpWarningUpMapper {

	@Select("select a.*,b.dept_name as org_name from cp_ipwarning_up a left join cp_org b on a.org_id=b.id")
	List<Map<String, Object>> queryList();
	
	int addIpWarningUp(Map<String, Object> prams);
	
	@Select("select * from cp_ipwarning_up where id=#{id}")
	Map<String, Object> selectById(@Param(value = "id") String id);
	
	@Update("update cp_ipwarning_up set check_time=null,approve_time=null,sign_time=null,status=0,checker=null,approver=null,signer=null where id=#{id}")
	int recover(@Param(value = "id") String id);
	
	@Update("update cp_ipwarning_up set check_time=null,approve_time=null,sign_time=null,status=0,checker=null,approver=null,signer=null")
	int recoverAll();

	int checkIpWarningUp(Map<String, Object> prams);
	
	int approveIpWarningUp(Map<String, Object> prams);
	
	int signIpWarningUp(Map<String, Object> prams);
	
}
