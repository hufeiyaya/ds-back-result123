package com.mti.zhpt.utils;

import com.mti.zhpt.model.CpCommandSummaryReportEntity;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;

import java.io.*;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/19
 * @change
 * @describe poi导出功能
 **/
//@Slf4j
public class POIUtils {
    /**
     * 导出总结报告word到文件服务器
     *
     * @throws IOException
     */
    public static void exportWord(CpCommandSummaryReportEntity entity) throws IOException {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            String currentDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            String classPath = POIUtils.class.getProtectionDomain().getCodeSource().getLocation().getFile();
            String[] parentPath = classPath.split("!");
            StringBuilder path = new StringBuilder();
            for (int i = 0; i < parentPath.length; i++) {
                if(i==parentPath.length-1){
                    path.append(parentPath[i]);
                }else{
                    path.append(parentPath[i]).append("!");
                }
            }
            System.out.println("classPath===>"+classPath);
            URL url = new URL("jar:" + path + "test.doc");
            System.out.println("url.path==>"+url.getPath());
            File file = new File(".");
            String docPath = file.getPath() + File.separator + System.currentTimeMillis() +".doc";
            System.out.println("docPath===>"+docPath);
            //创建读取word模板输入流
            inputStream = url.openStream();
            //创建Document对象
            HWPFDocument document = new HWPFDocument(inputStream);
            //事件标题
            String titleEventHolder = "${event}";
            String contentHolder = "${content}";
            String orgNameHolder = "${orgName}";
            String dateHolder = "${date}";
            String orgName = entity.getOrgName();
            String titleEventVal = entity.getTitle();
            //插入内容
            Range range = document.getRange();
            range.replaceText(titleEventHolder,titleEventVal);
            String content = entity.getContent();
            content = content.replace("</br>","XDSP");
            content = content.replace("\n","XDSP");
            content = content.replace("<br>","XDSP");
            StringBuilder sb = new StringBuilder();
            String[] strings = content.split("XDSP");
            for (String s : strings) {
                sb.append(s).append((char)11);
            }
            content = sb.toString();
            //替换内容
            range.replaceText(contentHolder, content);
            range.replaceText(orgNameHolder,orgName);
            range.replaceText(dateHolder,currentDate);
            //写出输出流
            outputStream = new FileOutputStream(docPath);
            //写出到输出流
            document.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            //关闭输出流
            if(outputStream!=null)
                outputStream.close();
            //关闭输入流
            if(inputStream!=null)
                inputStream.close();
        }
    }
}
