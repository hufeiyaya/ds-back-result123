package com.mti.zhpt.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.mti.zhpt.model.CommandCustomEntity;
import com.mti.zhpt.model.websocket.WSCustomEntity;

public interface CommandCustomMapper {
    List<String> querySort();
    List<CommandCustomEntity> querySortList(@Param("param") String param, @Param("name") String name);
    Integer updateStatus(@Param("entityList") List<WSCustomEntity> entityList);
	
}
