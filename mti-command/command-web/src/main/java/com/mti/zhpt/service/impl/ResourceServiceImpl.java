package com.mti.zhpt.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mti.zhpt.dao.ResourceMapper;
import com.mti.zhpt.model.Circle;
import com.mti.zhpt.model.GeoJson;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 资源服务
 * @Author: yingjj@mti-sh.cn
 * @Date: 2019/4/30 11:14
 */
@Service("resourceServiceImpl")
@Slf4j
public class ResourceServiceImpl {

	@Autowired
	private ResourceMapper resourceMapper;

	private static SimpleDateFormat formattertime = new SimpleDateFormat("HH:mm:ss");
	/**
	 * @Description:模糊全局查询
	 * @Author: yingjj@mti-sh.cn
	 * @Date: 2019/4/28
	 */
	public Map<String, Object> queryLike(String query, int pageid, int pagesize) {
		List<Map<String, Object>> results = null;
		Map<String, Object> resultmap = new HashMap<>();
		try {
			Page<Object> page = PageHelper.startPage(pageid, pagesize);
			results=resourceMapper.queryObscure(query);
			for (Map<String, Object> res : results) {
				Double[] coordinates = { (Double) res.get("longitude"), (Double) res.get("latitude") };
				res.put("coordinates", coordinates);
			}
			resultmap.put("data", results);
			resultmap.put("total", page.getTotal());
			resultmap.put("pages", page.getPages());

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return resultmap;
	}

	public Map<String, Object> queryPolygon(GeoJson request) {
		List<Map<String, Object>> results = null;
		Map<String, Object> resultmap = new HashMap<>();
		try {
			Map<String, Object> properties = request.getProperties();
			Page<Object> page = PageHelper.startPage((int)properties.get("pageid"), (int)properties.get("pagesize"));
			String geomText=request.getGeometry().GeomFromText();
			results=resourceMapper.queryPolygon(geomText);
			for (Map<String, Object> res : results) {
				Double[] coordinates = { (Double) res.get("longitude"), (Double) res.get("latitude") };
				res.put("coordinates", coordinates);
			}
			resultmap.put("data", results);
			resultmap.put("total", page.getTotal());
			resultmap.put("pages", page.getPages());

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return resultmap;
	}
	
	public Map<String, Object> queryCircle(Circle request) {
		List<Map<String, Object>> results = null;
		Map<String, Object> resultmap = new HashMap<>();
		try {
			Map<String, Object> properties = request.getProperties();
			Page<Object> page = PageHelper.startPage((int)properties.get("pageid"), (int)properties.get("pagesize"));
			List<Double> center = request.getCenter();
			String geomText="POINT("+center.get(0)+" "+center.get(1)+")";
			results=resourceMapper.queryCircle(geomText,request.getRadius()*0.8);
			for (Map<String, Object> res : results) {
				Double[] coordinates = { (Double) res.get("longitude"), (Double) res.get("latitude") };
				res.put("coordinates", coordinates);
			}
			resultmap.put("data", results);
			resultmap.put("total", page.getTotal());
			resultmap.put("pages", page.getPages());

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return resultmap;
	}

	public Map<String, Object> queryWarningDetail(String id) {
		Map<String, Object> result = new HashMap<String, Object>();
		result=resourceMapper.queryWarningDetail(id);
		return result;
	}

	public List<Map<String, Object>> queryAllPolices() {
		List<Map<String, Object>> results = null;
		try {
			results=resourceMapper.queryAllPolices();
			for (Map<String, Object> res : results) {
				Double[] coordinates = { (Double) res.get("longitude"), (Double) res.get("latitude") };
				res.put("coordinates", coordinates);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return results;
	}
	
	public List<Map<String, Object>> queryAllMoniters() {
		List<Map<String, Object>> results = null;
		try {
			results=resourceMapper.queryAllMoniters();
			for (Map<String, Object> res : results) {
				Double[] coordinates = { (Double) res.get("longitude"), (Double) res.get("latitude") };
				res.put("coordinates", coordinates);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return results;
	}
	
	public List<Map<String, Object>> queryAllTrafficcheckpoints() {
		List<Map<String, Object>> results = null;
		try {
			results=resourceMapper.queryAllTrafficcheckpoints();
			for (Map<String, Object> res : results) {
				Double[] coordinates = { (Double) res.get("longitude"), (Double) res.get("latitude") };
				res.put("coordinates", coordinates);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return results;
	}
	
	public List<Map<String, Object>> queryAllCars() {
		List<Map<String, Object>> results = null;
		try {
			results=resourceMapper.queryAllCars();
			for (Map<String, Object> res : results) {
				Double[] coordinates = { (Double) res.get("longitude"), (Double) res.get("latitude") };
				res.put("coordinates", coordinates);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return results;
	}
	
	public List<Map<String, Object>> queryAllWarnings() {
		Date now=new Date();
		String fakenow="2019-05-06 "+formattertime.format(now);
		log.debug(fakenow);
		List<Map<String, Object>> results = null;
		try {
			results=resourceMapper.queryAllWarnings(fakenow);
			for (Map<String, Object> res : results) {
				Double[] coordinates = { (Double) res.get("longitude"), (Double) res.get("latitude") };
				res.put("coordinates", coordinates);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return results;
	}
}
