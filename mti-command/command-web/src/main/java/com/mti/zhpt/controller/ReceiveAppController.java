package com.mti.zhpt.controller;


import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mti.zhpt.model.android.CommandSubmitEntity;
import com.mti.zhpt.service.impl.ReceiveAppServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 警情反馈websocket
 * @Author: zhaopf@mti-sh.cn
 * @Date: 2019/4/19 17:42
 */
@RestController
@RequestMapping(path = "/app")
 @Api(value = "/app", tags = {"APP警情反馈"}) 
@Slf4j
public class ReceiveAppController {

    @Resource
    private ReceiveAppServiceImpl receiveAppService;

    @RequestMapping(path = "/feedback", method = RequestMethod.POST)
	 @ApiOperation(value = "app通过http和web建立websocket(实时反馈)--警情反馈",hidden=true) 
    public RetResult<Object> FeedbackUpdate(@RequestBody Map<String, Object> data) {

        try {
            receiveAppService.FeedbackUpdate(data);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }

        return RetResponse.makeOKCp();

    }

    @RequestMapping(path = "/feedback-report", method = RequestMethod.POST)
	 @ApiOperation(value = "app通过http和web建立websocket(实时反馈)--批示反馈",hidden=true)
    public RetResult<Object> FeedbackReport(@RequestBody Map<Object, Object> data) {

        try {
            receiveAppService.FeedbackReport(data);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }

        return RetResponse.makeOKCp();

    }

    @RequestMapping(path = "/command-status", method = RequestMethod.POST)
    @ApiOperation(value = "app通过http和web建立websocket(实时反馈)--命令状态更新",hidden=true)
    public RetResult<Object> commandStatus(@RequestBody Map<Object, Object> data) {

        try {
            receiveAppService.commandStatus(data);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }

        return RetResponse.makeOKCp();

    }

}
