package com.mti.zhpt.shared.classHelper;


import com.github.pagehelper.PageHelper;
import com.mti.zhpt.dao.DictionaryMapper;
import com.mti.zhpt.model.Dictionary;
import com.mti.zhpt.shared.Exception.BusinessException;
import com.mti.zhpt.shared.utils.PinyinUtil;
import com.mti.zhpt.shared.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
@Slf4j
public class DictionaryHelper {

    /**
     * 根据类型查询改类型下所有字典返回树 suffixZeroNum 后缀0的数量
     *
     * @param type
     * @param isNeedLastLevelChildren
     * @param rootSuffixZeroNum
     * @param secondSuffixZeroNum
     * @param isNeedSoundHeads        是否需要返回音头
     * @param dictionaryMapper
     * @return
     */
    public static List<Dictionary> getTreeByType(String type,
                                                 Integer isNeedLastLevelChildren,
                                                 Integer rootSuffixZeroNum,
                                                 Integer secondSuffixZeroNum,
                                                 boolean isNeedSoundHeads,
                                                 DictionaryMapper dictionaryMapper) {
        List<Dictionary> rootList = null;
        try {
            List<Dictionary> allTypeList = getByTypeWithOrder(type,dictionaryMapper);
            if (isNeedSoundHeads) {
                allTypeList = buildSoundHeads4List(allTypeList);
            }
            System.out.println(allTypeList);
            rootList = new ArrayList<Dictionary>();
            List<Dictionary> secondList = new ArrayList<Dictionary>();
            for (Dictionary dictionary : allTypeList) {
                if (dictionary.getKey().endsWith(StringUtil.frontCompWithZore(0, rootSuffixZeroNum))) {
                    rootList.add(dictionary);
                    if (dictionary.getChildList() == null)
                        dictionary.setChildList(new ArrayList<Dictionary>());
                } else if (dictionary.getKey().endsWith(StringUtil.frontCompWithZore(0, secondSuffixZeroNum))) {
                    rootList.get(rootList.size() - 1).getChildList().add(dictionary);
                    secondList.add(dictionary);
                    if (dictionary.getChildList() == null)
                        dictionary.setChildList(new ArrayList<Dictionary>());
                } else {
                    // add by hsy at 2018-02-02 15:34:30
                    if (secondList.size() > 0) {
                        isNeedLastLevelChildren = isNeedLastLevelChildren == null ? 1 : isNeedLastLevelChildren;
                        if (isNeedLastLevelChildren != 0) {
                            secondList.get(secondList.size() - 1).getChildList().add(dictionary);
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("数据字典错误",e);
        }
        return rootList;
    }
    /**
     * 根据类型查询改类型下所有字典,并根据字典KEY排序
     *
     * @return
     * @throws
     */
    public static List<Dictionary> getByTypeWithOrder(String type, DictionaryMapper dictionaryMapper) {
        PageHelper.orderBy("KEY_ asc");
        return dictionaryMapper.getByType(type.toUpperCase());
    }

    /**
     * 字典集合组装音头
     *
     * @param dictList
     * @return
     */
    private static List<Dictionary> buildSoundHeads4List(List<Dictionary> dictList) {
        for (Dictionary dict : dictList) {
            buildSoundHeads(dict);
        }
        return dictList;
    }

    /**
     * 单个字典项组装音头
     *
     * @param dict
     * @return
     */
    private static Dictionary buildSoundHeads(Dictionary dict) {
        if (dict == null) {
            log.error("字典不能为空");
            throw new BusinessException(BusinessException.CODE_ILLEGAL_PARAMS, "字典不能为空");
        }
        StringBuilder sb = new StringBuilder("");
        if (!StringUtils.isEmpty(dict.getValue()) && "ALARM_TYPE_CODE".equals(dict.getType())) {
            List<String> soundHeadList = PinyinUtil.getPinyinSZM(dict.getValue());
            for (String soundHead : soundHeadList) {
                sb.append(soundHead).append(",");
            }
            String soundHeads = sb.toString().replaceAll(" ", "");
            if (soundHeads.contains(",")) {
                soundHeads = soundHeads.substring(0, soundHeads.lastIndexOf(","));
            }
            if (!StringUtils.isEmpty(soundHeads)) {
                soundHeads = dict.getValue() + "<span id='d0000'>" + soundHeads + "</span>";
            }
            dict.setValue(soundHeads);
        }
        return dict;
    }
}
