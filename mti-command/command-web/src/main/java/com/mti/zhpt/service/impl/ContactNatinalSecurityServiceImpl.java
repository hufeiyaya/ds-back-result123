package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactNatinalSecurityMapper;
import com.mti.zhpt.model.relationnet.ContactNatinalSecurityEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
@Service(value = "ContactNatinalSecurityEntity")
@AllArgsConstructor
public class ContactNatinalSecurityServiceImpl implements BaseService<ContactNatinalSecurityEntity> {
    private final ContactNatinalSecurityMapper mapper;

    @Override
    public void save(ContactNatinalSecurityEntity contactNatinalSecurityEntity) {
        mapper.save(contactNatinalSecurityEntity);
    }

    @Override
    public ContactNatinalSecurityEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactNatinalSecurityEntity contactNatinalSecurityEntity) {
        mapper.update(contactNatinalSecurityEntity);
    }

    @Override
    public PageModel<List<ContactNatinalSecurityEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactNatinalSecurityEntity>> pageModel = new PageModel<>();
        pageModel.setSize(size);
        pageModel.setPage(page);
        pageModel.setRecord(mapper.listAll(page,size));
        pageModel.setTotal(mapper.countRows());
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }
}
