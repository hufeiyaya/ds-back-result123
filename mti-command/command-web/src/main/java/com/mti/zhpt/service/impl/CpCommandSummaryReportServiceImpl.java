package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.CpCommandSummaryReportMapper;
import com.mti.zhpt.model.CpCommandSummaryReportEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe describe
 **/
@Service
@AllArgsConstructor
public class CpCommandSummaryReportServiceImpl implements BaseService<CpCommandSummaryReportEntity> {
    private final CpCommandSummaryReportMapper mapper;

    @Override
    public void save(CpCommandSummaryReportEntity cpCommandSummaryReportEntity) {
        mapper.save(cpCommandSummaryReportEntity);
    }

    @Override
    public CpCommandSummaryReportEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(CpCommandSummaryReportEntity cpCommandSummaryReportEntity) {
        mapper.update(cpCommandSummaryReportEntity);
    }

    @Override
    public PageModel<List<CpCommandSummaryReportEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<CpCommandSummaryReportEntity>> pageModel = new PageModel<>();
        pageModel.setRecord(mapper.findAllByPage(page,size));
        pageModel.setTotal(mapper.countAllRows());
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }

    public CpCommandSummaryReportEntity getEntityByRiskId(String riskId){
        return mapper.getEntityByRiskId(riskId);
    }
}
