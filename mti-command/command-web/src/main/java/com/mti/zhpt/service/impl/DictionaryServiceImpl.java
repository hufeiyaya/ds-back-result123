package com.mti.zhpt.service.impl;


import com.mti.zhpt.dao.DictionaryMapper;
import com.mti.zhpt.model.Dictionary;
import com.mti.zhpt.shared.classHelper.DictionaryHelper;
import com.mti.zhpt.utils.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service("dictionaryServiceImpl")
public class DictionaryServiceImpl {

    @Resource
    private DictionaryMapper dictionaryMapper;

    /**
     * 查询所有的字典，返回Map<String,Map<String,String>>
     *
     * @return
     */
    public Map<String, Map<String, String>> getAllDictionaryAsMap() {
        Map<String, Map<String, String>> resultMap = new HashMap<>();
        List<Dictionary> list = dictionaryMapper.findByCondition(null);
        for (Dictionary dictionary : list) {
            String type = dictionary.getType();
            if (!resultMap.containsKey(type)) {
                resultMap.put(type, getByTypeAsMap(type));
            }
        }
        return resultMap;
    }
    /**
     * 根据类型查询该类型下MAP集合
     *
     * @return
     * @throws
     */
    public Map<String, String> getByTypeAsMap(String type) {
        List<Dictionary> dicList = dictionaryMapper.getByType(type);
        Map<String, String> mapDic = new HashMap<>();
        for (Dictionary dic : dicList) {
            mapDic.put(dic.getKey(), dic.getValue());
        }
        return mapDic;
    }



    /**
     * 根据报警类型查询案由
     *
     * @param alarmCode
     * @param type
     * @param isNeedSoundHeads 是否需要返回音头
     * @return
     */
    public Object getTreeResultByType(String alarmCode, String type, boolean isNeedSoundHeads) {
        String key;
        if (alarmCode.contains("110")) {
            key = Constant.POLICE;
        } else if (alarmCode.contains("119")) {
            key =Constant. FIRE;
        } else if (alarmCode.contains("122")) {
            key = Constant.TRAFFIC;
        } else {
            key = "";
        }
        List<Dictionary> treeResult = DictionaryHelper.getTreeByType(type,
                null,
                4,
                2,
                isNeedSoundHeads ,
                dictionaryMapper);
        List<Dictionary> result = new ArrayList<>();
        for (Dictionary resultDic : treeResult) {
            if (key.contains(resultDic.getKey())) {
                result.add(resultDic);
            }
        }
        if (StringUtils.isEmpty(key)) {
            return treeResult;
        } else {
            return result;
        }
    }


    public List<Dictionary> getTreeByType(String type, Integer isNeedLastLevelChildren) {

        List<Dictionary> result = DictionaryHelper.getTreeByType(type,
                isNeedLastLevelChildren,
                4,
                2,
                false,
                dictionaryMapper);
        return result;
    }


    /**
     * 根据类型查询改类型下所有字典
     *
     * @return
     * @throws
     */

    public List<Dictionary> getByType(String type) {
        return dictionaryMapper.getByType(type);
    }

	public List<Map<String, Object>> getEventDictionary() {
		return dictionaryMapper.getEventDictionary();
	}

    /**
     * 根据类型和值查询字典对象
     *
     * @param type 类型
     * @param key 值
     * @return
     */
	public Dictionary getByTypeAndKey(String type, String key) {
        return dictionaryMapper.getByTypeAndKey(type, key);
    }

    /**
     * 根据类型和显示值查询字典对象
     *
     * @param type
     * @param value
     * @return
     */
    public Dictionary getByTypeAndValue(String type, String value) {
	    return dictionaryMapper.getByTypeAndValue(type, value);
    }
}
