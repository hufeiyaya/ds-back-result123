package com.mti.zhpt.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.mti.zhpt.model.android.CommandSubmitEntity;
import com.mti.zhpt.shared.websocket.WSCustomServer;

import lombok.extern.slf4j.Slf4j;

@Service("receiveAppServiceImpl")
@Slf4j
public class ReceiveAppServiceImpl {

	public void FeedbackUpdate(Map<String, Object>  data) {
		try {
			/**
			 * app实时提交警情反馈
			 * web端
			 * 建立websocket连接
			 */
			WSCustomServer.sendInfo(data, "web");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void FeedbackReport(Map<Object, Object> data) {
		/**
		 * app实时提交警情反馈
		 * web端
		 * 建立websocket连接
		 */
		try {
			WSCustomServer.sendInfo(data, "web");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void commandStatus(Map<Object, Object> data) {
		/**
		 * app实时提交警情反馈
		 * web端
		 * 建立websocket连接
		 */
		try {
			WSCustomServer.sendInfo(data, "web");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
