package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.KeyPersonnelMapper;
import com.mti.zhpt.model.CpCarBank;
import com.mti.zhpt.model.KeyPersonneEntity;
import com.mti.zhpt.shared.utils.DateUtils;
import com.mti.zhpt.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.Now;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车辆库实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-29
 */
@Service
@Slf4j
public class KeyPersonnelServiceImpl {

    @Resource
    KeyPersonnelMapper keyPersonnelMapper;

    /**
     * 根据时间 、责任部门  查找重点人员信息
     *
     * @param date
     * @param zrbm
     * @return
     */
    public List<KeyPersonneEntity> getKP(String date, String zrbm,String oid,String ssxqId) {

        List<KeyPersonneEntity> result = keyPersonnelMapper.getKP(date, zrbm,oid,ssxqId);
        if (!CollectionUtils.isEmpty(result)) {
            for (KeyPersonneEntity keyPersonneEntity : result) {
                StringBuilder stringBuilder = new StringBuilder();
                List<Map<String, Object>> et = keyPersonnelMapper.getET(keyPersonneEntity.getEvent_id());
                if (CollectionUtils.isEmpty(et)) continue;

                for (int i = 0; i < et.size(); i++) {
                    if (i == 0) {
                        Map<String, Object> stringObjectMap = et.get(0);
                        keyPersonneEntity.setFktime((Date) stringObjectMap.get("create_time"));
                        keyPersonneEntity.setDeptName((String) stringObjectMap.get("dept_name"));
                        if (( stringObjectMap.get("create_time"))==null||( stringObjectMap.get("create_time")).equals("")){
                            keyPersonneEntity.setIsNOt( DateUtil.getDateFZ(new Date(), keyPersonneEntity.getCreate_time()) > 180 ? "1" : "0");
                        }else {
                            keyPersonneEntity.setIsNOt(DateUtil.getDateFZ(((Date) stringObjectMap.get("create_time")), keyPersonneEntity.getCreate_time()) > 180 ? "1" : "0");
                        }


                    }
                    Map<String, Object> stringObjectMap = et.get(i);
                    stringBuilder.append(DateUtils.formatDates((Date) stringObjectMap.get("create_time"),"yyyyMMddHHmm")+ " "+ (String) stringObjectMap.get("remark")+"</br>");
                }
                keyPersonneEntity.setContent(stringBuilder.toString());



            }


        }


        return result;
    }
}
