package com.mti.zhpt.dao;

import com.mti.zhpt.model.ContactDutyRelation;
import com.mti.zhpt.model.relationnet.ContactDutyRelationEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
public interface ContactDutyRelationMapper {
    List<ContactDutyRelationEntity> listAll(
            @Param(value = "page")int page,
            @Param(value = "size")int size,
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );
    long countRows(
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );

    void save(ContactDutyRelationEntity entity);
    void update(ContactDutyRelationEntity entity);
    void deleteById(@Param(value = "id")String id);
    ContactDutyRelationEntity getOneById(@Param(value = "id")String id);

    List<String> getGroupNames(
            @Param(value = "page")int page,
            @Param(value = "size")int size,
            @Param(value = "orgName")String orgName
    );

    /**
     * 查询全部警令数据列表
     *
     * @return
     */
    @Select("select org_name as orgName, person_name as personName, position_ as position, office_in_line_phone as officeInLinePhone, office_out_line_phone as officeOutLinePhone, telephone_ as telephone, charge_work as chargeWork, residence_phone as residencePhone, room_ as room from t_contact_duty_relation")
    List<ContactDutyRelation> listByAll();

}
