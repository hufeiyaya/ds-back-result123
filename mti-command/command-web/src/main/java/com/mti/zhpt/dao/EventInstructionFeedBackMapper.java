package com.mti.zhpt.dao;

import com.mti.zhpt.model.InstructionFeedbackEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface EventInstructionFeedBackMapper {

    /**
     * 指令反馈
	 *
	 * @param instructionFeedbackEntity 指令反馈对象
	 * @return
     */
	@Insert("insert into cp_reserve_instruction_feedback(id_, feedback_org_id, feedback_org_name, feedback_police_id, feedback_police_name, feedback_phone, feedback_content, feedback_file, create_time, task_id) values(#{id}, #{feedbackOrgId}, #{feedbackOrgName}, #{feedbackPoliceId}, #{feedbackPoliceName}, #{feedbackPhone}, #{feedbackContent}, #{feedbackFile}, #{createTime}, #{taskId})")
	boolean instructionFeedback(InstructionFeedbackEntity instructionFeedbackEntity);

	/**
	 * 查询反馈列表
	 *
	 * @param instructionFeedbackEntity
	 * @return
	 */
	List<InstructionFeedbackEntity> listByInstructionFeedbackEntity(@Param("instructionFeedbackEntity") InstructionFeedbackEntity instructionFeedbackEntity);

}
