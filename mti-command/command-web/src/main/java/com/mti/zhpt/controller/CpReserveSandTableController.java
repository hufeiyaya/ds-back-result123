package com.mti.zhpt.controller;

import com.mti.zhpt.model.CpReserveSandTable;
import com.mti.zhpt.service.impl.CpReserveSandTableServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 预案沙盘 控制器
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-02
 */
@RestController
@RequestMapping(path = "/reserveSandTable")
@Api(value = "/reserveSandTable", tags = "预案沙盘")
@Slf4j
public class CpReserveSandTableController {

	@Autowired
	CpReserveSandTableServiceImpl cpReserveSandTableService;

	/**
	 * 根据预案id查询沙盘信息列表
	 *
	 * @param reserveId 预案id
	 * @return
	 */
	@GetMapping(path = "/getByReserveId")
	@ApiOperation(value = "根据预案id查询沙盘信息列表")
	public RetResult<List<CpReserveSandTable>> getByReserveId(
			@ApiParam(required = true, name = "reserveId", value = "预案id", example = "") @RequestParam(value = "reserveId", required = true) String reserveId) {
		if (StringUtils.isBlank(reserveId)) {
			return RetResponse.makeErrCp("预案id不能为空");
		}
		List<CpReserveSandTable> cpReserveSandTable = cpReserveSandTableService.listByReserveId(reserveId);
		return RetResponse.makeOKCp(cpReserveSandTable);
	}

	/**
	 * 批量保存预案沙盘
	 *
	 * @param cpReserveSandTableList 预案沙盘对象对象
	 * @return
	 */
	@PostMapping(path = "saveCpReserveSandTable")
	@ApiOperation(value = "批量保存预案沙盘")
	public RetResult saveCpReserveSandTableBatch(@RequestBody List<CpReserveSandTable> cpReserveSandTableList) {
		if (cpReserveSandTableList == null || cpReserveSandTableList.isEmpty()) {
			return RetResponse.makeErrCp("预案沙盘对象列表不能为空");
		}
		int result = cpReserveSandTableService.saveCpReserveSandTableBatch(cpReserveSandTableList);
		return RetResponse.makeOKCp(result);
	}

}
