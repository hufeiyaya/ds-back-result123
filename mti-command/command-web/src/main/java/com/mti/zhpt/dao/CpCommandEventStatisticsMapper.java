package com.mti.zhpt.dao;

import com.mti.zhpt.model.CpCommandEventStatisticsEntity;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe 事件统计
 **/
public interface CpCommandEventStatisticsMapper extends BaseMapper<CpCommandEventStatisticsEntity> {

    Map<String,Object> getStatisticsData(@Param(value = "eventId")String eventId);

    CpCommandEventStatisticsEntity getEntityByRiskId(@Param(value = "riskId")String riskId);

}
