package com.mti.zhpt.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mti.zhpt.utils.SnowFlake;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.mti.zhpt.dao.IpWarningUpMapper;

import lombok.extern.slf4j.Slf4j;

@Service("ipWarningUpServiceImpl")
@Slf4j
public class IpWarningUpServiceImpl {

	@Autowired
	private SnowFlake snowFlake;

	@Autowired
	private IpWarningUpMapper ipWarningUpMapper;

	@Autowired
	private FastFileStorageClient storageClient;


	public Map<String, Object> addIpWarningUp(String title, String content, String keywords, String org_id,
			int sign_person_id, String level, String casetype, String appendix_url, String appendix_name) {
		Date now = new Date();
		Map<String, Object> prams = new HashMap<String, Object>();
		prams.put("title", title);
		prams.put("content", content);
		prams.put("keywords", keywords);
		prams.put("org_id", org_id);
		prams.put("sign_person_id", sign_person_id);
		prams.put("level", level);
		prams.put("casetype", casetype);
		prams.put("create_time", now);
		prams.put("appendix_url", appendix_url);
		prams.put("appendix_name", appendix_name);
		prams.put("creater", "张一凡");
		prams.put("status", 0);
		prams.put("id", snowFlake.nextId());
		ipWarningUpMapper.addIpWarningUp(prams);
		return ipWarningUpMapper.selectById((String) prams.get("id"));
	}

	public Map<String, Object> checkIpWarningUp(String id, String title, String content,
			String keywords, String org_id, int sign_person_id, String level, String casetype,String appendix_url, String appendix_name) {
		Date now = new Date();
		Map<String, Object> prams = new HashMap<String, Object>();
		prams.put("id", id);
		prams.put("title", title);
		prams.put("content", content);
		prams.put("keywords", keywords);
		prams.put("org_id", org_id);
		prams.put("sign_person_id", sign_person_id);
		prams.put("level", level);
		prams.put("casetype", casetype);
		prams.put("appendix_url", appendix_url);
		prams.put("appendix_name", appendix_name);
		// 非表单提交内容
		prams.put("status", 1);
		prams.put("check_time", now);
		prams.put("checker", "张一凡");
		ipWarningUpMapper.checkIpWarningUp(prams);
		return ipWarningUpMapper.selectById(id);
	}

	public Map<String, Object> approveIpWarningUp(String id, String title, String content,
			String keywords, String org_id, int sign_person_id, String level, String casetype,String appendix_url, String appendix_name) {
		Date now = new Date();
		Map<String, Object> prams = new HashMap<String, Object>();
		prams.put("id", id);
		prams.put("title", title);
		prams.put("content", content);
		prams.put("keywords", keywords);
		prams.put("org_id", org_id);
		prams.put("sign_person_id", sign_person_id);
		prams.put("level", level);
		prams.put("casetype", casetype);
		prams.put("appendix_url", appendix_url);
		prams.put("appendix_name", appendix_name);
		// 非表单提交内容
		prams.put("status", 2);
		prams.put("approve_time", now);
		prams.put("approver", "张一凡");
		ipWarningUpMapper.approveIpWarningUp(prams);
		return ipWarningUpMapper.selectById(id);
	}

	public Map<String, Object> signIpWarningUp(String id, String title, String content,
			String keywords, String org_id, int sign_person_id, String level, String casetype,String appendix_url, String appendix_name) {
		Date now = new Date();

		Map<String, Object> prams = new HashMap<String, Object>();
		prams.put("id", id);
		prams.put("title", title);
		prams.put("content", content);
		prams.put("keywords", keywords);
		prams.put("org_id", org_id);
		prams.put("sign_person_id", sign_person_id);
		prams.put("level", level);
		prams.put("casetype", casetype);
		prams.put("appendix_url", appendix_url);
		prams.put("appendix_name", appendix_name);
		// 非表单提交内容
		prams.put("status", 3);
		prams.put("sign_time", now);
		prams.put("signer", "张一凡");
		ipWarningUpMapper.signIpWarningUp(prams);
		return ipWarningUpMapper.selectById(id);
	}

	public Map<String, Object> recover(String id) {
		ipWarningUpMapper.recover(id);
		return ipWarningUpMapper.selectById(id);
	}

	public List<Map<String, Object>> queryListUp() {
		List<Map<String, Object>> result = null;
		try {
			result = ipWarningUpMapper.queryList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public Map<String, Object> upLoadFile(MultipartFile file) {
		StorePath storePath;
		String path = null;
		String filename = null;
		Map<String, Object> prams = new HashMap<String, Object>();
		filename = file.getOriginalFilename();
		try {
			storePath = storageClient.uploadFile(file.getInputStream(), file.getSize(),
					FilenameUtils.getExtension(file.getOriginalFilename()), null);
			path = storePath.getFullPath();
			log.debug("上传文件{}成功!", filename);
			log.debug(path);
		} catch (Exception e) {
			log.error("上传文件异常!", e);
			e.printStackTrace();
		}
		prams.put("appendix_url", path);
		prams.put("appendix_name", filename);
		return prams;
	}

}
