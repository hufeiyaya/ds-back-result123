package com.mti.zhpt.dao;

import com.mti.zhpt.model.CpReserveSandTable;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 预案沙盘 mapper
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-02
 */
public interface CpReserveSendTableMapper {

	/**
	 * 根据预案id查询沙盘信息列表
	 *
	 * @param reserveId 预案id
	 * @return
	 */
	@Select("select id_ as id, reserve_id as reserveId, content_ as content, create_time createTime from cp_reserve_sand_table a where a.reserve_id = #{reserveId} and enable_ = 1 order by create_time desc")
	List<CpReserveSandTable> listByReserveId(String reserveId);

	/**
	 * 批量保存预案沙盘
	 *
	 * @param cpReserveSandTableList 预案沙盘对象集合
	 * @return
	 */
	@Insert("<script>insert into cp_reserve_sand_table(id_, reserve_id, content_, create_time) values " +
			"<foreach collection='cpReserveSandTableList' item='item' separator=','>" +
			"(#{item.id}, #{item.reserveId}, #{item.content}, #{item.createTime})" +
			"</foreach></script>")
	int saveCpReserveSandTableBatch(@Param("cpReserveSandTableList") List<CpReserveSandTable> cpReserveSandTableList);

	/**
	 * 逻辑删除对应预案沙盘数据
	 *
	 * @param reserveId 预案id
	 * @return
	 */
	@Update("update cp_reserve_sand_table set enable_ = -1 where reserve_id = #{reserveId}")
	int deleteByReserveId(String reserveId);
}