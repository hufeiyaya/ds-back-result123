package com.mti.zhpt.shared.classHelper;

import com.mti.zhpt.dao.CommandCustomMapper;
import com.mti.zhpt.dao.EventDetailsTimerMapper;
import com.mti.zhpt.model.websocket.WSCustomEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
* @Description:
* @Author: zhaopf@mti-sh.cn
* @Date: 2019/4/24 16:45
*/
@Component
@Slf4j
public class TaskHandler {

    @Resource
    private CommandCustomMapper commandCustomMapper;
    @Resource
    private EventDetailsTimerMapper eventDetailsTimerMapper;


    /**
    * @Description: 异步处理数据存储
    * @Author: zhaopf@mti-sh.cn
    * @Date: 2019/4/24 16:45
    */
    @Async("taskExecutor")
    public void handler(List<WSCustomEntity> wsCustomEntity) {

        try {
            commandCustomMapper.updateStatus(wsCustomEntity);
            eventDetailsTimerMapper.insertTimerList(wsCustomEntity);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }


}
