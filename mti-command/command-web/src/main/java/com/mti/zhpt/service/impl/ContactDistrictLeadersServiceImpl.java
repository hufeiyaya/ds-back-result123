package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactDistrictLeadersMapper;
import com.mti.zhpt.model.relationnet.ContactDistrictLeadersEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe describe
 **/
@Service(value = "ContactDistrictLeadersEntity")
@AllArgsConstructor
public class ContactDistrictLeadersServiceImpl implements BaseService<ContactDistrictLeadersEntity> {
    private final ContactDistrictLeadersMapper mapper;

    @Override
    public void save(ContactDistrictLeadersEntity contactDistrictLeadersEntity) {
        mapper.save(contactDistrictLeadersEntity);
    }

    @Override
    public ContactDistrictLeadersEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactDistrictLeadersEntity contactDistrictLeadersEntity) {
        mapper.update(contactDistrictLeadersEntity);
    }

    @Override
    public PageModel<List<ContactDistrictLeadersEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactDistrictLeadersEntity>> pageModel = new PageModel<List<ContactDistrictLeadersEntity>>();
        pageModel.setSize(size);
        pageModel.setPage(page);
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setRecord(mapper.listAll(page, size,searchTerm[0],searchTerm[1],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }
}
