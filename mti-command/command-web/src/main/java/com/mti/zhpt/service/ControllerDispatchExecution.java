package com.mti.zhpt.service;

import com.mti.zhpt.base.BaseEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.vo.RelationNetDispatchVO;

import java.util.List;
import java.util.Map;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
public interface ControllerDispatchExecution<T,E extends BaseEntity> {
    //分页获取列表
    PageModel<T> execution(RelationNetDispatchVO vo);
    //执行save
    void executionSave(Map<String,Object> t);
    //更新
    void executionUpdate(Map<String,Object> t);
    //删除
    void executionDelete(String id,String moduleType);
    //获取单个
    E executionFindOneById(String id,String moduleType);
    //获取联络网菜单列表
    List<T> getModuleList();
    //获取二级联络网菜单列表
    T executeSecondModuleList(String type,String moduleType);
}
