package com.mti.zhpt.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mti.zhpt.service.impl.DutyServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 值班备勤接口 yingjj@mti-sh.cn 2019年5月16日10:51
 */
@RestController
@RequestMapping(path = "/duty")
@Api(value = "/duty", tags = "值班备勤")
@Slf4j
public class DutyController {
	@Autowired
	private DutyServiceImpl dutyService;
	@RequestMapping(path = "/queryDuty", method = RequestMethod.GET)
	@ApiOperation(value = "查询值班备勤列表")
	public RetResult<Map<String, Object>> queryDuty() {
		Map<String, Object> result = null;
		try {
			result=dutyService.queryDuty();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

}
