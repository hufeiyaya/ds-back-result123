package com.mti.zhpt.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.mti.zhpt.model.TextRequest;

public interface RequestLeaderMapper {
	@Select("select * from cp_request_leaderlist")
	List<Map<String, Object>> queryRequestLeaderList();

	@Insert("insert into cp_response_record values(#{eventid},#{recordid},#{create_time},#{leaderid})")
	int addRequestRecordPre(Map<String, Object> prams);

	@Select("select * from cp_response_record where eventid=#{eventid}")
	List<Map<String, Object>> queryRecord(String eventid);

	int doRequestLeader(Map<String, Object> request);
	
	@Select("select a.*,b.name,b.duty from cp_response a,cp_request_leaderlist b where a.eventid=#{eventid} and a.leaderid=b.id order by createtime desc")
	List<Map<String, Object>> queryRequestList(String eventid);

	@Update("update cp_response set status=1 where id=#{responseid}")
	int playRequest(int responseid);

	int doRequest(TextRequest request);
	
	@Insert("insert into cp_request_leader values(#{requestid},#{leaderid})")
	void addRequestLeader(int requestid, String leaderid);
	
	@Select("select a.id,a.dept_name,COALESCE(b.orgid != null,false) as isplay from cp_org a left join (select * from cp_response_org where responseid=#{responseid}) b on a.id=b.orgid")
	List<Map<String,Object>> queryPlayOrg(int responseid);

	@Insert("insert into cp_response_org values(#{responseid},#{orgid})")
	void playRequestOrg(int responseid, String orgid);

	@Select("select * from cp_response where eventid=#{eventid}")
	List<Map<String, Object>> queryresponseList(int eventid);

	@Delete("delete from cp_request where eventid=#{event_id}")
	int delRequest(String event_id);

	@Delete("delete from cp_response where eventid=#{event_id}")
	int delResponse(String event_id);

	@Delete("delete from cp_response_record where eventid=#{event_id}")
	int delResponseRecord(String event_id);

	@Insert("insert into cp_response (eventid,leaderid,content,status,createtime) values(#{eventid},#{leaderId},#{instrucTcontent},#{status},#{create_time})")
	int instructContent(TextRequest request);
}