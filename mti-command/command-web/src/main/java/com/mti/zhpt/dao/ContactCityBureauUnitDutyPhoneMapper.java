package com.mti.zhpt.dao;

import com.mti.zhpt.model.relationnet.ContactCityBureauUnitDutyPhoneEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 市局单位值班电话
 **/
public interface ContactCityBureauUnitDutyPhoneMapper {
    List<ContactCityBureauUnitDutyPhoneEntity> listAll(
            @Param(value = "page")Integer page,
            @Param(value = "size")Integer size,
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );
    long countRows(
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );

    void save(ContactCityBureauUnitDutyPhoneEntity entity);
    void update(ContactCityBureauUnitDutyPhoneEntity entity);
    void deleteById(@Param(value = "id")String id);
    ContactCityBureauUnitDutyPhoneEntity getOneById(@Param(value = "id")String id);


}
