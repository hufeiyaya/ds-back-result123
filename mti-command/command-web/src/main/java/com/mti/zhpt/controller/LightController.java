package com.mti.zhpt.controller;


import com.mti.zhpt.service.impl.LightServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 灯
 * @Author: zhaopf@mti-sh.cn
 * @Date: 2020年1月7日
 */
@RestController
@RequestMapping(path = "/socket")
@Slf4j
public class LightController {



    @Autowired
    private LightServiceImpl lightService;

    @RequestMapping(path = "/light", method = RequestMethod.GET)
    @ApiOperation(value = "蓝绿红橙灯")
    public RetResult addEvent(
            @ApiParam(required = true, value = "1开灯，0关灯，打开闪烁2，关闭闪烁3", example = "1") @RequestParam String command,
            @ApiParam(required = true, value = "灯号（台号）1/2/3/4/5/6/7/8/9/254(254代表全部灯)", example = "1") @RequestParam String number,
            @ApiParam(required = false, value = "颜色：blue/green/red/orange", example = "1") @RequestParam String colour
    ){

        try {
            lightService.socketMessage(command,number,colour);

            return RetResponse.makeOKCp();
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

}
