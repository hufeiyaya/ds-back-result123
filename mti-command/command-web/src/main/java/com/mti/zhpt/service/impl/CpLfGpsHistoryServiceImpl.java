package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.CpLfGpsHistoryMapper;
import com.mti.zhpt.model.CpLfGpsHistory;
import com.mti.zhpt.shared.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 历史轨迹对象 实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-08
 */
@Service
@Slf4j
@Transactional
public class CpLfGpsHistoryServiceImpl {

	@Autowired
    CpLfGpsHistoryMapper cpLfGpsHistoryMapper;

    /**
     * 根据警员id查询历史轨迹列表
     *
     * @param policeId 警员id
     * @return
     */
    public List<CpLfGpsHistory> listByPoliceId(String policeId, String startTime, String endTime) {
        List<CpLfGpsHistory> result = cpLfGpsHistoryMapper.listByPoliceId(policeId, DateUtils.strToDateLong(startTime), DateUtils.strToDateLong(endTime));
        return result;
    }

    /**
     * 根据警情id查询历史轨迹列表
     *
     * @return
     */
    public List<CpLfGpsHistory> listByEventId(String eventId) {
        List<CpLfGpsHistory> result = cpLfGpsHistoryMapper.listByEventId(eventId);
        return result;
    }

}
