package com.mti.zhpt.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface QueryHisMapper {
	@Insert("insert into cp_query_log(querytime,querycontent) values(#{date},#{query})")
	void logQuery(Date date, String query);
	@Select("select id,querytime,querycontent from cp_query_log order by querytime desc")
	List<Map<String,Object>> getQueryHis();
	
	int delQueryHis(@Param(value = "id") int id);
	
}