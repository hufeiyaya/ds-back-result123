package com.mti.zhpt.controller;

import com.mti.zhpt.model.CpPopulationBank;
import com.mti.zhpt.service.impl.PopulationBankServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 人口库控制器
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-29
 */
@RestController
@RequestMapping(path = "/populationBank")
@Api(value = "/populationBank", tags = "人口库")
@Slf4j
public class PopulationBankController {

	@Autowired
	PopulationBankServiceImpl populationBankService;

	/**
	 * 根据身份证号查询人员信息
	 *
	 * @param idCard 身份证号
	 * @return
	 */
	@GetMapping(path = "/getByIdCard")
	@ApiOperation(value = "根据身份证号查询人员信息")
	public RetResult<CpPopulationBank> getByIdCard(
			@ApiParam(required = true, name = "idCard", value = "身份证号", example = "111") @RequestParam(value = "idCard", required = true) String idCard) {
		if (StringUtils.isBlank(idCard)) {
			return RetResponse.makeErrCp("身份证号不能为空");
		}
		CpPopulationBank cpPopulationBank = populationBankService.getByIdCard(idCard);
		return RetResponse.makeOKCp(cpPopulationBank);
	}

}
