package com.mti.zhpt.dao;

import com.mti.zhpt.model.EventTimerEntity;
import com.mti.zhpt.model.EventTimerFileEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EventTimerPhotoMapper {
    List<EventTimerFileEntity> queryFile(@Param("timerId") Integer timerId);

    /**
     * 插入附件
     *
     * @param eventTimerEntity 时间轴对象
     * @return
     */
    int insertImagesUrl(EventTimerEntity eventTimerEntity);

}
