package com.mti.zhpt.service;

import com.mti.zhpt.base.BaseEntity;
import com.mti.zhpt.model.relationnet.PageModel;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 基类接口
 **/
public interface BaseService<T extends BaseEntity> {
    void save(T t);
    /**
     * 通过ID获取
     * @param id id
     * @return T
     */
    T getOneById(String id);

    /**
     * 通过ID删除
     * @param id id
     */
    void deleteById(String id);

    /**
     * 更新实体
     * @param t 实体
     */
    void update(T t);

    /**
     * 获取分页列表
     * @param page 当前页
     * @param size 每页大小
     * @param searchTerm 搜索条件
     * @return list
     */
    PageModel<List<T>> list(Integer page,Integer size,String... searchTerm);

    /**
     * 分组获取数据
     * @param page page
     * @param size size
     * @param groupTerm group
     * @return list
     */
    PageModel<List<String>> listByGroupBy(Integer page,Integer size,String... groupTerm);

}
