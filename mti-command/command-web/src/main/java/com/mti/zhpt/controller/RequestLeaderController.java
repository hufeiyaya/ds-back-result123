package com.mti.zhpt.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mti.zhpt.model.TextRequest;
import com.mti.zhpt.service.impl.RequestLeaderServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * yingjj@mti-sh.cn 2019年4月23日10:51
 */
@RestController
@RequestMapping(path = "/requestleader")
@Api(value = "/requestleader", tags = "请示汇报")
@Slf4j
public class RequestLeaderController {

	@Autowired
	private RequestLeaderServiceImpl requestLeaderService;

	@RequestMapping(path = "/queryrequestleaderlist", method = RequestMethod.GET)
	@ApiOperation(value = "查询请示汇报领导列表")
	public RetResult<List<Map<String, Object>>> queryRequestLeaderList() {

		List<Map<String, Object>> result = null;
		try {
			result = requestLeaderService.queryRequestLeaderList();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/generaterecord", method = RequestMethod.GET)
	@ApiOperation(value = "生成录音")
	public RetResult<Map<String, Object>> generateRecord(
			@ApiParam(required = true, name = "leaderid", value = "领导ID", example = "1") @RequestParam(value = "leaderid", required = true) int leaderid,
			@ApiParam(required = true, name = "eventid", value = "案件ID", example = "1") @RequestParam(value = "eventid", required = true) int eventid) {

		Map<String, Object> result = null;
		try {
			result = requestLeaderService.generateRecord(leaderid, eventid);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/queryrecord", method = RequestMethod.GET)
	@ApiOperation(value = "查询录音号")
	public RetResult<List<Map<String, Object>>> queryRecord(@RequestParam("eventid") String eventid) {

		List<Map<String, Object>> result = null;
		try {
			result = requestLeaderService.queryRecord(eventid);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/dorequestleader", method = RequestMethod.POST)
	@ApiOperation(value = "记录请示汇报的内容并保存（0）或下发（1）")
	public RetResult<Integer> doRequestLeader(
			@ApiParam(name = "request", value = "{\"eventid\":6,\"leaderid\":1,\"recordid\":\"6_213123123_1\",\"content\":\"你好,这里是内容\",\"status\":1,\"play_org_id\":[12,13,14]}", required = true) @RequestBody Map<String, Object> request) {

		int result = 0;
		try {
			result = requestLeaderService.doRequestLeader(request);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/queryrequestlist", method = RequestMethod.GET)
	@ApiOperation(value = "查询请示汇报领导批复历史")
	public RetResult<List<Map<String, Object>>> queryRequestList(@RequestParam("eventid") String eventid) {

		List<Map<String, Object>> result = null;
		try {
			result = requestLeaderService.queryRequestList(eventid);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/playrequest", method = RequestMethod.POST)
	@ApiOperation(value = "更新请示汇报状态(下发)")
	public RetResult<Integer> playRequest(
			@ApiParam(name = "request", value = "{{\"id\":15,\"play_org_id\":[12,13,14]}}", required = true) @RequestBody Map<String, Object> request) {
		int result = 0;
		try {
			result = requestLeaderService.playRequest(request);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/dorequest", method = RequestMethod.POST)
	@ApiOperation(value = "请示汇报(文字)")
	public RetResult<Integer> doRequest(@RequestBody TextRequest request) {
		int result = 0;
		try {
			result = requestLeaderService.doRequest(request);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/queryplayorg", method = RequestMethod.GET)
	@ApiOperation(value = "显示下发组织列表")
	public RetResult<List<Map<String, Object>>> queryPlayOrg(
			@RequestParam(value = "responseid", required = false, defaultValue = "0") int responseid) {

		List<Map<String, Object>> result = null;
		try {
			result = requestLeaderService.queryPlayOrg(responseid);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

}
