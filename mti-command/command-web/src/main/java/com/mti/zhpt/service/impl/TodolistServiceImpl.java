package com.mti.zhpt.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mti.zhpt.dao.TodolistMapper;
import com.mti.zhpt.model.EventEntity;
import com.mti.zhpt.model.TodolistEntity;
import com.mti.zhpt.utils.SnowFlake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Author: yangsp@mti-sh.cn
 * @Date: 2019/6/23
 */
@Service("todolistServiceImpl")
public class TodolistServiceImpl {

	@Autowired
	private SnowFlake snowFlake;

	@Resource
	private TodolistMapper todolistMapper;

	public List<Map<String, Object>> queryList(TodolistEntity entity) {
		List<Map<String, Object>> result = null;
		try {
			result = todolistMapper.queryList(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public PageInfo pageByTodolist(TodolistEntity entity) {
		PageInfo pageInfo = null;
		PageHelper.startPage(entity.getPageNum(), entity.getPageSize());
		List<Map<String, Object>> result = null;
		try {
			result = todolistMapper.queryList(entity);
			pageInfo = new PageInfo(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageInfo;
	}

	public TodolistEntity addByEntity(TodolistEntity entity) {
		entity.setId(snowFlake.nextId());
		todolistMapper.addByEntity(entity);
		return entity;
	}

}
