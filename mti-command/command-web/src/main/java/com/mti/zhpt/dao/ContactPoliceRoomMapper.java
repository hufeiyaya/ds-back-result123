package com.mti.zhpt.dao;

import com.mti.zhpt.model.relationnet.ContactPoliceRoomEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
public interface ContactPoliceRoomMapper {
    List<ContactPoliceRoomEntity> listAll(
            @Param(value = "page")int page,
            @Param(value = "size")int size,
            @Param(value = "unitName")String unitName);
    long countRows(@Param(value = "unitName")String unitName);

    void save(ContactPoliceRoomEntity entity);
    void update(ContactPoliceRoomEntity entity);
    void deleteById(@Param(value = "id")String id);
    ContactPoliceRoomEntity getOneById(@Param(value = "id")String id);
}
