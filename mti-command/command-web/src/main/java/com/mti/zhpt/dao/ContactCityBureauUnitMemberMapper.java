package com.mti.zhpt.dao;

import com.mti.zhpt.model.relationnet.ContactCityBureauUnitDutyPhoneEntity;
import com.mti.zhpt.model.relationnet.ContactCityBureauUnitMemberEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe describe
 **/
public interface ContactCityBureauUnitMemberMapper {
    List<ContactCityBureauUnitMemberEntity> listAll(
            @Param(value = "page")Integer page,
            @Param(value = "size")Integer size,
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );
    long countRows(
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );

    void save(ContactCityBureauUnitMemberEntity entity);
    void update(ContactCityBureauUnitMemberEntity entity);
    void deleteById(@Param(value = "id")String id);
    ContactCityBureauUnitMemberEntity getOneById(@Param(value = "id")String id);

    List<String> getGroupNames(
            @Param(value = "page")Integer page,
            @Param(value = "size")Integer size
    );
}
