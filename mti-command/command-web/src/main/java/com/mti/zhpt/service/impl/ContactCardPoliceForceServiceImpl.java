package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactCardPoliceForceMapper;
import com.mti.zhpt.model.ContactCardPoliceForce;
import com.mti.zhpt.model.relationnet.ContactCardPoliceForceEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe describe
 **/
@Service(value = "ContactCardPoliceForceEntity")
@AllArgsConstructor
public class ContactCardPoliceForceServiceImpl implements BaseService<ContactCardPoliceForceEntity> {
    private final ContactCardPoliceForceMapper mapper;

    @Override
    public void save(ContactCardPoliceForceEntity contactCardPoliceForceEntity) {
        mapper.save(contactCardPoliceForceEntity);
    }

    @Override
    public ContactCardPoliceForceEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactCardPoliceForceEntity contactCardPoliceForceEntity) {
        mapper.update(contactCardPoliceForceEntity);
    }

    @Override
    public PageModel<List<ContactCardPoliceForceEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactCardPoliceForceEntity>> pageModel = new PageModel<>();
        pageModel.setSize(size);
        pageModel.setPage(page);
        pageModel.setRecord(mapper.listAll(page, size,searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }

    /**
     * 查询防线数据列表
     *
     * @return
     */
    public List<ContactCardPoliceForce> listByAll() {
        List<ContactCardPoliceForce> contactCardPoliceForceList = mapper.listByAll();
        return contactCardPoliceForceList;
    }

}
