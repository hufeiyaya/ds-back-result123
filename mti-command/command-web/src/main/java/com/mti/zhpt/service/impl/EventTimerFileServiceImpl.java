package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.EventTimerPhotoMapper;
import com.mti.zhpt.model.EventTimerEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 附件 实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-28
 */
@Service
@Slf4j
@Transactional
public class EventTimerFileServiceImpl {

	@Autowired
    EventTimerPhotoMapper eventTimerPhotoMapper;

    /**
     *
     * 批量保存附件
     *
     * @param eventTimerEntity 时间轴对象
     * @return
     */
    @Transactional
    public boolean saveEventTimerFileBatch(EventTimerEntity eventTimerEntity) {

        Integer result = eventTimerPhotoMapper.insertImagesUrl(eventTimerEntity);

        return null != result && result >= 1;
    }

}
