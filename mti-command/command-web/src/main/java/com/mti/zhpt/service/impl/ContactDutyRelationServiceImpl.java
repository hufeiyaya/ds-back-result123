package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactDutyRelationMapper;
import com.mti.zhpt.model.ContactDutyRelation;
import com.mti.zhpt.model.relationnet.ContactDutyRelationEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
@Service(value = "ContactDutyRelationEntity")
@AllArgsConstructor
public class ContactDutyRelationServiceImpl implements BaseService<ContactDutyRelationEntity> {
    private final ContactDutyRelationMapper mapper;

    @Override
    public void save(ContactDutyRelationEntity contactDutyRelationEntity) {
        mapper.save(contactDutyRelationEntity);
    }

    @Override
    public ContactDutyRelationEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactDutyRelationEntity contactDutyRelationEntity) {
        mapper.update(contactDutyRelationEntity);
    }

    @Override
    public PageModel<List<ContactDutyRelationEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactDutyRelationEntity>> pageModel =  new PageModel<>();
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setPage(page);
        pageModel.setSize(size);
        pageModel.setRecord(mapper.listAll(page,size,searchTerm[0],searchTerm[1],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        PageModel<List<String>> pageModel = new PageModel<>();
        List<String> list = mapper.getGroupNames(page,size,"");
        pageModel.setRecord(list);
        pageModel.setTotal((long) list.size());
        return pageModel;
    }

    /**
     * 查询全部警令数据列表
     *
     * @return
     */
    public List<ContactDutyRelation> listByAll() {
        List<ContactDutyRelation> contactDutyRelationList = mapper.listByAll();
        return contactDutyRelationList;
    }

}
