package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactLinkageUnitMapper;
import com.mti.zhpt.model.ContactLinkageUnit;
import com.mti.zhpt.model.relationnet.ContactLinkageUnitEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 联动单位实现类
 * </p>
 *
 * @author zhaichen,zhangmingxin
 * @since 2019-08-05
 */
@Service(value = "ContactLinkageUnitEntity")
@AllArgsConstructor
public class ContactLinkageUnitServiceImpl implements BaseService<ContactLinkageUnitEntity> {
    private final ContactLinkageUnitMapper contactLinkageUnitMapper;

    @Override
    public void save(ContactLinkageUnitEntity contactLinkageUnitEntity) {
        contactLinkageUnitMapper.save(contactLinkageUnitEntity);
    }

    @Override
    public ContactLinkageUnitEntity getOneById(String id) {
        return contactLinkageUnitMapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        contactLinkageUnitMapper.deleteById(id);
    }

    @Override
    public void update(ContactLinkageUnitEntity contactLinkageUnitEntity) {
        contactLinkageUnitMapper.update(contactLinkageUnitEntity);
    }

    @Override
    public PageModel<List<ContactLinkageUnitEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactLinkageUnitEntity>> pageModel = new PageModel<>();
        pageModel.setSize(size);
        pageModel.setPage(page);
        pageModel.setRecord(contactLinkageUnitMapper.pageListAll(page,size,searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setTotal(contactLinkageUnitMapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }

    /**
     * 查询联动单位列表
     *
     * @return
     */
    public List<ContactLinkageUnit> listAll() {
        List<ContactLinkageUnit> result = contactLinkageUnitMapper.listAll();
        return result;
    }

    /**
     * 查询联动单位列表
     *
     * @param unitName 单位名称
     * @return
     */
    public ContactLinkageUnit getByUnitName(String unitName) {
        ContactLinkageUnit result = contactLinkageUnitMapper.getByUnitName(unitName);
        return result;
    }
}
