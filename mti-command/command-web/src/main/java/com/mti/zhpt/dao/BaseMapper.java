package com.mti.zhpt.dao;

import com.mti.zhpt.base.BaseEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/12
 * @change
 * @describe describe
 **/
public interface BaseMapper<E extends BaseEntity> {
    List<E> findAllByPage(@Param(value = "page") int page, @Param(value = "size") int size);

    long countAllRows();

    int save(E entity);

    int deleteById(@Param(value = "id") String id);

    int update(E entity);

    E getOneById(@Param(value = "id")String id);

    int saveByBatch(List<E> list);
}
