package com.mti.zhpt.dao;

import com.mti.zhpt.model.CpReserveProcess;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 预案流程 mapper
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-21
 */
public interface CpReserveProcessMapper {

	/**
	 * 根据预案id查询流程信息列表
	 *
	 * @param reserveId 预案id
	 * @return
	 */
	@Select("select id_ as id, reserve_id as reserveId, content_ as content, create_time createTime from cp_reserve_process a where a.reserve_id = #{reserveId} and enable_ = 1 order by create_time desc")
	List<CpReserveProcess> listByReserveId(String reserveId);

	/**
	 * 根据预案id查询流程信息
	 *
	 * @param reserveId 预案id
	 * @return
	 */
	@Select("select id_ as id, reserve_id as reserveId, content_ as content, create_time createTime from cp_reserve_process a where a.reserve_id = #{reserveId} and enable_ = 1 order by create_time desc limit 1")
	CpReserveProcess getByReserveId(String reserveId);

	/**
	 * 批量保存预案沙盘
	 *
	 * @param cpReserveProcessList 预案流程对象集合
	 * @return
	 */
	@Insert("<script>insert into cp_reserve_process(id_, reserve_id, content_, create_time) values " +
			"<foreach collection='cpReserveProcessList' item='item' separator=','>" +
			"(#{item.id}, #{item.reserveId}, #{item.content}, #{item.createTime})" +
			"</foreach></script>")
	int saveCpReserveProcessBatch(@Param("cpReserveProcessList") List<CpReserveProcess> cpReserveProcessList);

	/**
	 * 保存预案沙盘
	 *
	 * @param cpReserveProcess 预案流程对象
	 * @return
	 */
	@Insert("<script>insert into cp_reserve_process(id_, reserve_id, content_, create_time) values " +
			"(#{cpReserveProcess.id}, #{cpReserveProcess.reserveId}, #{cpReserveProcess.content}, #{cpReserveProcess.createTime})" +
			"</script>")
	int saveCpReserveProcess(@Param("cpReserveProcess") CpReserveProcess cpReserveProcess);

	/**
	 * 更新预案沙盘
	 *
	 * @param cpReserveProcess 预案流程对象
	 * @return
	 */
	@Update("<script>update cp_reserve_process set content_ = #{cpReserveProcess.content}, update_time = #{cpReserveProcess.updateTime} " +
			"where id_ = #{cpReserveProcess.id}" +
			"</script>")
	int updateCpReserveProcess(@Param("cpReserveProcess") CpReserveProcess cpReserveProcess);
}