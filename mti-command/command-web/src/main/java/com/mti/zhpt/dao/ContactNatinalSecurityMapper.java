package com.mti.zhpt.dao;

import com.mti.zhpt.model.relationnet.ContactNatinalSecurityEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
public interface ContactNatinalSecurityMapper {
    List<ContactNatinalSecurityEntity> listAll(@Param(value = "page")int page,@Param(value = "size")int size);
    long countRows();

    void save(ContactNatinalSecurityEntity entity);
    void update(ContactNatinalSecurityEntity entity);
    void deleteById(@Param(value = "id")String id);
    ContactNatinalSecurityEntity getOneById(@Param(value = "id")String id);
}
