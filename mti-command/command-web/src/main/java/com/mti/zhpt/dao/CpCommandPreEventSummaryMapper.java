package com.mti.zhpt.dao;

import com.mti.zhpt.model.CpCommandPreEventSummaryEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe 预案统计
 **/
public interface CpCommandPreEventSummaryMapper extends BaseMapper<CpCommandPreEventSummaryEntity> {
    List<Map<String,Object>> getDataByRiskId(@Param(value = "riskId")String riskId);

    CpCommandPreEventSummaryEntity getEntityByRiskId(@Param(value = "riskId")String riskId);
}
