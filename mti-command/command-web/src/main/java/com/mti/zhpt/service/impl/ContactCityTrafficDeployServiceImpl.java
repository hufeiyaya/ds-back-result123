package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactCityTrafficDeployMapper;
import com.mti.zhpt.model.relationnet.ContactCityTrafficDeployEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/7
 * @change
 * @describe describe
 **/
@Data
@Slf4j
@AllArgsConstructor
@Service(value = "ContactCityTrafficDeployEntity")
public class ContactCityTrafficDeployServiceImpl implements BaseService<ContactCityTrafficDeployEntity> {
    private final ContactCityTrafficDeployMapper mapper;

    @Override
    public void save(ContactCityTrafficDeployEntity contactCityTrafficDeployEntity) {
        mapper.save(contactCityTrafficDeployEntity);
    }

    @Override
    public ContactCityTrafficDeployEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        mapper.deleteById(id);
    }

    @Override
    public void update(ContactCityTrafficDeployEntity contactCityTrafficDeployEntity) {
        mapper.update(contactCityTrafficDeployEntity);
    }

    @Override
    public PageModel<List<ContactCityTrafficDeployEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactCityTrafficDeployEntity>> pageModel = new PageModel<>();
        pageModel.setPage(page);
        pageModel.setSize(size);
        pageModel.setRecord(mapper.listAll(page,size,searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }
}
