package com.mti.zhpt.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface AddressMapper {
	@Select("select longitude,latitude from cp_address_lonlat where address like '${address}' union all select longitude,latitude from cp_address_lonlat where address is null")
	List<Map<String, Object>> getLonLat(@Param(value = "address") String address);

}