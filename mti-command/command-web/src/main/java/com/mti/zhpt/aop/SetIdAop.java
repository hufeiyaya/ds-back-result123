package com.mti.zhpt.aop;

import com.mti.zhpt.base.BaseEntity;
import com.mti.zhpt.utils.SnowFlake;
import lombok.AllArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/8
 * @change
 * @describe describe
 **/
@Aspect
@AllArgsConstructor
@Component
public class SetIdAop {
    private final SnowFlake snowFlake;
    @Pointcut("execution(* com.mti.zhpt.service.BaseService.save(..))")
    public void cut(){}

    @Around(value = "cut()")
    public Object beforeSaveSetId(ProceedingJoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        if(args.length>0){
            Object obj = args[0];
            if(obj instanceof BaseEntity){
                BaseEntity entity = (BaseEntity) obj;
                entity.setId(snowFlake.nextId());
                args[0] = entity;
            }
        }
        try {
            return joinPoint.proceed(args);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        }
    }
}
