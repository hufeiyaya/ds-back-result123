package com.mti.zhpt.dao;

import com.mti.zhpt.model.relationnet.ContactHebeiCommandCenterEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
public interface ContactHebeiCommandCenterMapper {
    List<ContactHebeiCommandCenterEntity> listAll(@Param(value = "page")int page,@Param(value = "size")int size);
    long countRows();

    void save(ContactHebeiCommandCenterEntity entity);
    void update(ContactHebeiCommandCenterEntity entity);
    void deleteById(@Param(value = "id")String id);
    ContactHebeiCommandCenterEntity getOneById(@Param(value = "id")String id);
}
