package com.mti.zhpt.dao;

import com.mti.zhpt.model.EventTimerEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 时间轴 mapper
 *
 * @author zhaichen
 * @since 2019-08-05
 */
public interface EventTimerMapper {

	/**
	 * 批量保存案件对应时间轴信息
	 *
	 * @param eventTimerEntityList 时间轴列表
	 */
	int insertEventTimerBatch(@Param("eventTimerEntityList") List<EventTimerEntity> eventTimerEntityList);

	/**
	 * 保存案件对应时间轴信息
	 *
	 * @param eventTimerEntity 时间轴对象
	 */
	int insertEventTimer(EventTimerEntity eventTimerEntity);

	@Update("update cp_response set status = 1 where id = #{responseId}")
	void updateResponse(@Param("responseId") int responseId);
}
