package com.mti.zhpt.dao;

import com.mti.zhpt.model.ContactLinkageUnit;
import com.mti.zhpt.model.CpCarBank;
import org.apache.ibatis.annotations.Select;
import com.mti.zhpt.model.relationnet.ContactLinkageUnitEntity;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * <p>
 * 联动单位 mapper
 * </p>
 *
 * @author zhaichen,zhangmingxin
 * @since 2019-08-05
 */
public interface ContactLinkageUnitMapper {

	/**
	 * 查询联动单位列表
	 *
	 * @return
	 */
	@Select("select unit_name as unitName, concat_name as concatName, concat_phone as concatPhone, duty_call_one as dutyCallOne, duty_call_two as dutyCallTwo, fax_number as faxNumber, duty_call_night as dutyCallNight, duty_call_inline as dutyCallInline, hotline_ as hotline, remark_ as remark from t_contact_linkage_unit")
	List<ContactLinkageUnit> listAll();

	/**
	 * 根据单位名字查询联动单位对象信息
	 *
	 * @param unitName 单位名称
	 * @return
	 */
	@Select("select unit_name as unitName, concat_name as concatName, concat_phone as concatPhone, duty_call_one as dutyCallOne, duty_call_two as dutyCallTwo, fax_number as faxNumber, duty_call_night as dutyCallNight, duty_call_inline as dutyCallInline, hotline_ as hotline, remark_ as remark from t_contact_linkage_unit where unit_name = #{unitName}")
	ContactLinkageUnit getByUnitName(String unitName);

    /**
     * 分页
     * @param page 当前页
     * @param size 页大小
     * @return 分页列表
     */
    List<ContactLinkageUnitEntity> pageListAll(
    		@Param(value = "page")int page,
			@Param(value = "size")int size,
			@Param(value = "orgName")String orgName,
			@Param(value = "personName")String personName,
			@Param(value = "phone")String phone
	);

    /**
     * 获取总数
     * @return count
     */
    long countRows(
			@Param(value = "orgName")String orgName,
			@Param(value = "personName")String personName,
			@Param(value = "phone")String phone
	);

	void save(ContactLinkageUnitEntity entity);
	void update(ContactLinkageUnitEntity entity);
	void deleteById(@Param(value = "id")String id);
	ContactLinkageUnitEntity getOneById(@Param(value = "id")String id);



}
