package com.mti.zhpt.service;

import com.mti.zhpt.model.RulePerson;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/9
 * @change
 * @describe describe
 **/
public interface RulePersonService extends BaseService<RulePerson> {
    /**
     * 法言法语匹配
     * @param content 内容
     * @param type 法言法语类型
     * @return 法言法语列表
     */
    List<RulePerson> matchRulePerson(String content,String type,String keyWord);

    /**
     * 增加信息赋能次数
     * @param eventId 警情ID
     */
    void addWithPowerTimes(String eventId);
}
