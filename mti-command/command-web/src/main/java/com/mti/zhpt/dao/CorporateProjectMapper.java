package com.mti.zhpt.dao;

import java.util.List;
import java.util.Map;

public interface CorporateProjectMapper {

	List<Map<String, Object>> queryList();

}