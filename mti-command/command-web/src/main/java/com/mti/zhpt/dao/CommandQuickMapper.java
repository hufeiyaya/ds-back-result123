package com.mti.zhpt.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface CommandQuickMapper {

	@Select("SELECT * FROM t_pf_organization WHERE name_ LIKE '%支队'")
	List<Map<String, Object>> detachment();

//	@Select("SELECT * from t_pf_organization where code_ = #{code} and  level =#{level} ")
	@Select("SELECT * FROM t_pf_organization WHERE region_id_ LIKE ${orgId} AND name_ LIKE '%大队'")
	List<Map<String, Object>> brigade(@Param("orgId")final String orgId);
}
