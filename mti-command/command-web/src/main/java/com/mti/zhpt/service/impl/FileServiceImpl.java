package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.FileMapper;
import com.mti.zhpt.model.FileEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/12
 * @change
 * @describe describe
 **/
@Service
@AllArgsConstructor
public class FileServiceImpl implements BaseService<FileEntity> {
    private final FileMapper fileMapper;
    @Override
    public void save(FileEntity entity) {

    }

    @Override
    public FileEntity getOneById(String id) {
        return fileMapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        fileMapper.deleteById(id);
    }

    public void deleteByBussId(String bussId){
        fileMapper.deleteByBussId(bussId);
    }

    @Override
    public void update(FileEntity entity) {

    }

    @Override
    public PageModel<List<FileEntity>> list(Integer page, Integer size, String... searchTerm) {
        return null;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }

    public void saveByBatch(List<FileEntity> fileEntityList){
        fileMapper.saveByBatch(fileEntityList);
    }

    public List<FileEntity> getFileListByBussId(String bussId){
        return fileMapper.listFilesByBussId(bussId);
    }

    public List<String> getFileIdsByBussId(String bussId){
        return fileMapper.listFileIdsByBussId(bussId);
    }


    public List<FileEntity> getFilesByBussIds(List<String> bussIds){
        return fileMapper.listFilesByBussIds(bussIds);
    }
}
