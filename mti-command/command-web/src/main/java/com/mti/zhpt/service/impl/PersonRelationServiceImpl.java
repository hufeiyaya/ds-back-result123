package com.mti.zhpt.service.impl;

import java.util.List;
import java.util.Map;

import com.mti.zhpt.base.BaseServiceImpl;
import com.mti.zhpt.model.CpPersonRelation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mti.zhpt.dao.PersonRelationMapper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

/**
* @Description: 预测预警
 * @Author: yingjj@mti-sh.cn
* @Date: 2019/4/23 11:14
*/
@Service
@Slf4j
@Transactional
public class PersonRelationServiceImpl extends BaseServiceImpl<CpPersonRelation> {

	@Autowired
    private PersonRelationMapper personRelationMapper;
    
    
    public List<Map<String,Object>> queryPersonRelations(String eventId) {
    	List<Map<String,Object>> result = null;
        try {
            result = personRelationMapper.queryPersonRelations(eventId);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return result;
    }

    /**
     * 相关人，车，其他关系保存
     *
     * @param personId 人员id
     * @param relationInfoId 相关信息id
     * @return
     */
//    public int savePersonRelation(String personId, String relationInfoId) {
//        int result = personRelationMapper.savePersonRelation(personId, relationInfoId);
//        return result;
//    }

}
