package com.mti.zhpt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mti.zhpt.dao.DutyMapper;

/**
 * @Description: 值班备勤服务
 * @Author: yingjj@mti-sh.cn
 * @Date: 2019/5/16 11:14
 */
@Service("dutyServiceImpl")
public class DutyServiceImpl {
	@Autowired
	private DutyMapper dutyMapper;
	public Map<String, Object> queryDuty() {
		HashMap<String, Object> result=new HashMap<String, Object>();
		List<Map<String, Object>> duty = dutyMapper.queryDuty();
		List<Map<String, Object>> dutypcs = dutyMapper.queryDutyPcs();
		List<Map<String, Object>> dutyzsjz = dutyMapper.queryDutyZsjz();
		result.put("sj", duty);
		result.put("fj", dutypcs);
		result.put("zsjz", dutyzsjz);
		return result;
	}

}
