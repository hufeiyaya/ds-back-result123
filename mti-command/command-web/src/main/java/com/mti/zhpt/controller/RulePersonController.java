package com.mti.zhpt.controller;

import com.mti.zhpt.model.RulePerson;
import com.mti.zhpt.service.RulePersonService;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;


/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/8
 * @change
 * @describe describe
 **/
@Slf4j
@Api(value = "法言法语",description = "法言法语",tags = "法言法语")
@RestController
@RequestMapping(value = "/rulePerson")
@AllArgsConstructor
public class RulePersonController {
    private final RulePersonService service;
    @PostMapping
    @ApiOperation(value = "新增法言法语信息",notes = "新增法言法语信息")
    public RetResult create(@RequestBody RulePerson rulePerson){
        try {
            service.save(rulePerson);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @DeleteMapping
    @ApiOperation(value = "根据ID删除法言法语",notes = "根据ID删除法言法语")
    public RetResult delete(@RequestParam(value = "id")@ApiParam(value = "法言法语ID",name = "id")String id){
        try {
            service.deleteById(id);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @PutMapping
    @ApiOperation(value = "更新法言法语",notes = "更新法言法语")
    public RetResult update(@RequestBody RulePerson rulePerson){
        try {
            service.update(rulePerson);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @GetMapping
    @ApiOperation(value = "根据ID查询法言法语",notes = "根据ID查询法言法语")
    public RetResult<RulePerson> findById(@RequestParam(value = "id")@ApiParam(value = "法言法语ID",name = "id")String id){
        try {
            return RetResponse.makeOKCp(service.getOneById(id));
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @GetMapping(value = "/list")
    @ApiOperation(value = "分页获取法言法语列表",notes = "分页获取法言法语列表")
    public RetResult findList(
            @RequestParam(value = "page")@ApiParam(value = "当前页",name = "page") Integer page,
            @RequestParam(value = "size")@ApiParam(value = "页大小",name = "size")Integer size,
            @RequestParam(value = "title",required = false)@ApiParam(value = "标题",name = "title")String title,
            @RequestParam(value = "keyWord",required = false)@ApiParam(value = "关键字",name = "keyWord")String keyWord,
            @RequestParam(value = "type",required = false)@ApiParam(value = "法言法语类型",name = "type")String type,
            @RequestParam(value = "caseType",required = false)@ApiParam(value = "案由",name = "caseType")String caseType
    ){
        try {
            return RetResponse.makeOKCp(service.list(page,size, title,keyWord,type,caseType));
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }


    @ApiOperation(value = "警情匹配法言法语",notes = "警情匹配法言法语")
    @GetMapping(value = "/matchRulePerson")
    public RetResult<Object> matchRulePerson(
            @RequestParam(value = "content",required = false)@ApiParam(value = "警情内容")String content,
            @RequestParam(value = "type",required = false)@ApiParam(value = "警情类型")String type,
            @RequestParam(value = "keyWord",required = false)@ApiParam(value = "关键字")String keyWord
    ){
        try {
            return RetResponse.makeOKCp(service.matchRulePerson(content, type,keyWord));
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @PutMapping(value = "/withPowerTimes")
    @ApiOperation(value = "累加信息赋能次数")
    public RetResult addWithPowerTimes(@RequestParam(value = "eventId")@ApiParam(value = "警情ID",name="eventId") String eventId){
        try {
            service.addWithPowerTimes(eventId);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
    }
}
