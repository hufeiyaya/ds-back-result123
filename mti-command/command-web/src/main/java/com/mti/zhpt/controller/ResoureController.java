package com.mti.zhpt.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mti.zhpt.model.Circle;
import com.mti.zhpt.model.GeoJson;
import com.mti.zhpt.service.impl.QueryHisServiceImpl;
import com.mti.zhpt.service.impl.ResourceServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * 资源接口 yingjj@mti-sh.cn 2019年4月30日10:51
 */
@RestController
@RequestMapping(path = "/resource")
@Api(value = "/resource", tags = "资源查询")
@Slf4j
public class ResoureController {

	@Autowired
	private ResourceServiceImpl resoureService;
	@Autowired
	private QueryHisServiceImpl queryHisService;

	@RequestMapping(path = "/queryAllPolices", method = RequestMethod.GET)
	@ApiOperation(value = "查询所有警员资源")
	public RetResult<List<Map<String, Object>>> queryAllPolices() {
		List<Map<String, Object>> result = null;
		try {
			result = resoureService.queryAllPolices();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}
	
	@RequestMapping(path = "/queryAllMoniters", method = RequestMethod.GET)
	@ApiOperation(value = "查询所有监控资源")
	public RetResult<List<Map<String, Object>>> queryAllMoniters() {
		List<Map<String, Object>> result = null;
		try {
			result = resoureService.queryAllMoniters();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}
	
	@RequestMapping(path = "/queryAllTrafficcheckpoints", method = RequestMethod.GET)
	@ApiOperation(value = "查询所有卡口资源")
	public RetResult<List<Map<String, Object>>> queryAllTrafficcheckpoints() {
		List<Map<String, Object>> result = null;
		try {
			result = resoureService.queryAllTrafficcheckpoints();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}
	
	@RequestMapping(path = "/queryAllCars", method = RequestMethod.GET)
	@ApiOperation(value = "查询所有警车资源")
	public RetResult<List<Map<String, Object>>> queryAllCars() {
		List<Map<String, Object>> result = null;
		try {
			result = resoureService.queryAllCars();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}
	
	@RequestMapping(path = "/queryAllWarnings", method = RequestMethod.GET)
	@ApiOperation(value = "查询所有警情资源")
	public RetResult<List<Map<String, Object>>> queryAllWarnings() {
		List<Map<String, Object>> result = null;
		try {
			result = resoureService.queryAllWarnings();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}
	
	@RequestMapping(path = "/queryLike", method = RequestMethod.GET)
	@ApiOperation(value = "模糊查询资源")
	public RetResult<Map<String, Object>> queryLike(
			@ApiParam(required = false, name = "query", value = "查询条件", example = "张三") @RequestParam(value="query",defaultValue = "") String query,
			@ApiParam(required = false, name = "pageid", value = "页码", example = "1") @RequestParam(value = "pageid", required = false, defaultValue = "1") int pageid,
			@ApiParam(required = false, name = "pagesize", value = "分页大小(每页显示多少个)", example = "10") @RequestParam(value = "pagesize", required = false, defaultValue = "10") int pagesize) {
		Map<String, Object> result = null;
		try {
			result = resoureService.queryLike("%" + query + "%", pageid, pagesize);
			queryHisService.logQuery(query);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/queryPolygon", method = RequestMethod.POST)
	@ApiOperation(value = "面选资源")
	public RetResult<Map<String, Object>> queryPolygon(@RequestBody GeoJson request) {
		Map<String, Object> result = null;
		try {
			result = resoureService.queryPolygon(request);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/queryCircle", method = RequestMethod.POST)
	@ApiOperation(value = "圈选资源")
	public RetResult<Map<String, Object>> queryCircle(@RequestBody Circle request) {
		Map<String, Object> result = null;
		try {
			result = resoureService.queryCircle(request);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}
	
	@RequestMapping(path = "/queryWarningDetail", method = RequestMethod.GET)
	@ApiOperation(value = "查询警情详情")
	public RetResult<Map<String, Object>> queryWarningDetail(@ApiParam(required = true, name = "id", value = "warningID", example = "1") @RequestParam("id") String id) {
		Map<String, Object> result = null;
		try {
			result = resoureService.queryWarningDetail(id);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

}
