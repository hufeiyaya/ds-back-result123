package com.mti.zhpt.dao;

import com.mti.zhpt.model.ContactDutyCall;
import com.mti.zhpt.model.relationnet.ContactDutyCallEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
public interface ContactDutyCallMapper {
    List<ContactDutyCallEntity> listAll(
            @Param(value = "page")int page,
            @Param(value = "size")int size,
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );
    long countRows(
            @Param(value = "orgName")String orgName,
            @Param(value = "personName")String personName,
            @Param(value = "phone")String phone
    );

    void save(ContactDutyCallEntity entity);
    void update(ContactDutyCallEntity entity);
    void deleteById(@Param(value = "id")String id);
    ContactDutyCallEntity getOneById(@Param(value = "id")String id);

    /**
     * 查询全部职能部门数据列表
     *
     * @return
     */
    @Select("select org_name as orgName, duty_call_sun as dutyCallSun, fax_number as faxNumber, duty_call_night as dutyCallNight from t_contact_duty_call")
    List<ContactDutyCall> listByAll();

}
