/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.zhpt.controller;

import com.alibaba.fastjson.JSON;
import com.mti.message.component.producer.ProducerOperator;
import com.mti.zhpt.model.websocket.Message;
import com.mti.zhpt.model.websocket.MessageType;
import com.mti.zhpt.model.websocket.WSEventFeedbackOverTimeEntity;
import com.mti.zhpt.service.impl.DataServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import com.mti.zhpt.utils.DateUtil;
import com.mti.zhpt.vo.JingqingTjVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 警情统计 yangsp@mti-sh.cn 2019年6月20日
 */
@RestController
@RequestMapping(path = "/dataCount")
@Api(value = "/dataCount", tags = "警情统计")
@Slf4j
public class DataController {
    @Autowired
    private DataServiceImpl dataService;
    @Autowired
    private ProducerOperator producerOperator;

    @RequestMapping(path = "/getCountByJjdb", method = RequestMethod.GET)
    @ApiOperation(value = "按接警统计")
    public RetResult<Integer> getCountByJjdb(
            @ApiParam(required = true, name = "startTime", value = "开始时间", example = "2019-05-21 00:00:00") @RequestParam(value = "startTime", required = true) String startTime,
            @ApiParam(required = true, name = "endTime", value = "结束时间", example = "2019-05-22 00:00:00") @RequestParam(value = "endTime", required = true) String endTime,
            @ApiParam(name = "ssxqId", value = "辖区ID") @RequestParam(value = "ssxqId", required = false) String ssxqId) {
        int result = 0;
        try {
            result = dataService.getCountByJjdb(startTime, endTime,ssxqId);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/getCountByCjdb", method = RequestMethod.GET)
    @ApiOperation(value = "按处警统计")
    public RetResult<Integer> getCountByCjdb(
            @ApiParam(required = true, name = "startTime", value = "开始时间", example = "2019-05-21 00:00:00") @RequestParam(value = "startTime", required = true) String startTime,
            @ApiParam(required = true, name = "endTime", value = "结束时间", example = "2019-05-22 00:00:00") @RequestParam(value = "endTime", required = true) String endTime) {
        int result = 0;
        try {
            result = dataService.getCountByCjdb(startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/getCountEventByLevel", method = RequestMethod.GET)
    @ApiOperation(value = "按警情级别统计" +
            "level： 01 一级警情 02 二级警情 03 三级警情 04 四级警情")
    public RetResult<Integer> getCountEventByLevel(
            @ApiParam(required = true, name = "startTime", value = "开始时间", example = "2019-05-21 00:00:00") @RequestParam(value = "startTime", required = true) String startTime,
            @ApiParam(required = true, name = "endTime", value = "结束时间", example = "2019-05-22 00:00:00") @RequestParam(value = "endTime", required = true) String endTime,
            @ApiParam(required = true, name = "level", value = "警情级别", example = "01") @RequestParam(value = "level", required = true) String level) {
        int result = 0;
        try {
            result = dataService.getCountEventByLevel(startTime, endTime, level);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/getCountEventByAllLevel", method = RequestMethod.GET)
    @ApiOperation(value = "按警情级别统计" +
            "jq1 一级警情 jq2 二级警情 jq3 三级警情 jq4 四级警情")
    public RetResult<Map<String, String>> getCountEventByAllLevel(
            @ApiParam(required = true, name = "startTime", value = "开始时间", example = "2019-05-21 00:00:00") @RequestParam(value = "startTime", required = true) String startTime,
            @ApiParam(required = true, name = "endTime", value = "结束时间", example = "2019-05-22 00:00:00") @RequestParam(value = "endTime", required = true) String endTime,
            @ApiParam(name = "type", value = "警情类型(0：110警情，1：重点人员，2：在逃人员)") @RequestParam(value = "type", required = false) String type) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            result = dataService.getCountEventByAllLevel(startTime, endTime, type);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/getCountByAnyou", method = RequestMethod.GET)
    @ApiOperation(value = "按警情案由统计")
    public RetResult<List<Map<String, String>>> getCountByAnyou(
            @ApiParam(required = true, name = "startTime", value = "开始时间", example = "2019-05-21 00:00:00") @RequestParam(value = "startTime", required = true) String startTime,
            @ApiParam(required = true, name = "endTime", value = "结束时间", example = "2019-05-22 00:00:00") @RequestParam(value = "endTime", required = true) String endTime,
            @ApiParam(name = "ssxqId", value = "辖区ID") @RequestParam(value = "ssxqId", required = false) String ssxqId) {
        List<Map<String, String>> result = null;
        try {
            result = dataService.getCountByAnyou(startTime, endTime,ssxqId);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/getCountByXiaqu", method = RequestMethod.GET)
    @ApiOperation(value = "按警情辖区统计")
    public RetResult<List<Map<String, String>>> getCountByXiaqu(
            @ApiParam(required = true, name = "startTime", value = "开始时间", example = "2019-05-21 00:00:00") @RequestParam(value = "startTime", required = true) String startTime,
            @ApiParam(required = true, name = "endTime", value = "结束时间", example = "2019-05-22 00:00:00") @RequestParam(value = "endTime", required = true) String endTime,
            @ApiParam(name = "ssxqId", value = "辖区ID") @RequestParam(value = "ssxqId", required = false) String ssxqId) {
        List<Map<String, String>> result = null;
        try {
            result = dataService.getCountByXiaqu(startTime, endTime,ssxqId);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/getCountByPCS", method = RequestMethod.GET)
    @ApiOperation(value = "按警情派出所统计")
    public RetResult<List<Map<String, Object>>> getCountByPCS(
            @ApiParam(required = true, name = "startTime", value = "开始时间", example = "2019-05-21 00:00:00") @RequestParam(value = "startTime", required = true) String startTime,
            @ApiParam(required = true, name = "endTime", value = "结束时间", example = "2019-05-22 00:00:00") @RequestParam(value = "endTime", required = true) String endTime,
            @ApiParam(name = "ssxqId", value = "辖区ID") @RequestParam(value = "ssxqId", required = true) String ssxqId) {
        List<Map<String, Object>> result = null;
        try {
            result = dataService.getCountByPCS(startTime, endTime, ssxqId);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/getCountAvgByXiaqu", method = RequestMethod.GET)
    @ApiOperation(value = "按警情辖区统计平均值（四色预警）")
    public RetResult<List<Map<String, String>>> getCountAvgByXiaqu(
            @ApiParam(required = true, name = "date", value = "日期", example = "2019-05-21") @RequestParam(value = "date", required = true) String date) {
        List<Map<String, String>> result = null;
        try {
            result = dataService.getCountAvgByXiaqu(date);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/getCountByJingqing", method = RequestMethod.GET)
    @ApiOperation(value = "按警情统计")
    public RetResult<List<JingqingTjVo>> getCountByJingqing(
            @ApiParam(required = true, name = "startTime", value = "开始时间", example = "2019-05-21 00:00:00") @RequestParam(value = "startTime", required = true) String startTime,
            @ApiParam(required = true, name = "endTime", value = "结束时间", example = "2019-05-22 00:00:00") @RequestParam(value = "endTime", required = true) String endTime,
            @ApiParam(required = true, name = "isImportant", value = "是否重大警情", example = "0 日常指挥警情 1 应急指挥警情") @RequestParam(value = "isImportant", required = true) String isImportant,
            @ApiParam(required = true, name = "deptCode", value = "当前登录人组织id", example = "131000250000") @RequestParam String deptCode,
            @ApiParam(required = true, name = "deptLevel", value = "当前登录人数据权限级别", example = "1") @RequestParam String deptLevel
    ) {
        List<JingqingTjVo> result = new ArrayList<JingqingTjVo>();
        ;
        try {
            result = dataService.getCountByJingqing(startTime, endTime, isImportant,deptCode,deptLevel);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/getCountByXqay", method = RequestMethod.GET)
    @ApiOperation(value = "按警情辖区编码统计")
    public RetResult<List<JingqingTjVo>> getCountByXqay(
            @ApiParam(required = true, name = "startTime", value = "开始时间", example = "2019-05-21 00:00:00") @RequestParam(value = "startTime", required = true) String startTime,
            @ApiParam(required = true, name = "endTime", value = "结束时间", example = "2019-05-22 00:00:00") @RequestParam(value = "endTime", required = true) String endTime,
            @ApiParam(required = true, name = "branchUnitId", value = "辖区编号", example = "131028000000") @RequestParam(value = "branchUnitId", required = true) String branchUnitId,
            @ApiParam(required = true, name = "isImportant", value = "是否重大警情", example = "0 日常指挥警情 1 应急指挥警情") @RequestParam(value = "isImportant", required = true) String isImportant) {
        List<JingqingTjVo> result = new ArrayList<JingqingTjVo>();
        ;
        try {
            result = dataService.getCountByXqay(startTime, endTime, branchUnitId, isImportant);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    /**
     * 根据时间查询重点警情总数
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return
     */
    @GetMapping(path = "/countImportEvent")
    @ApiOperation(value = "根据时间查询重点警情总数")
    public RetResult<Integer> countImportEvent(
            @ApiParam(required = true, name = "startTime", value = "开始时间", example = "2019-05-21 00:00:00") @RequestParam(value = "startTime", required = true) String startTime,
            @ApiParam(required = true, name = "endTime", value = "结束时间", example = "2019-05-22 00:00:00") @RequestParam(value = "endTime", required = true) String endTime) {
        Integer result;
        try {
            result = dataService.countImportEvent(startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/getCountJq", method = RequestMethod.GET)
    @ApiOperation(value = "今日警情统计" +
            "jq1 应急事件 jq2 在逃人员报警 jq3 重点人员报警 jq4 110警情")
    public RetResult<Map<String, String>> getCountByjingqing(
            @ApiParam(required = true, name = "startTime", value = "开始时间", example = "2019-05-21 00:00:00") @RequestParam(value = "startTime", required = true) String startTime,
            @ApiParam(required = true, name = "endTime", value = "结束时间", example = "2019-05-22 00:00:00") @RequestParam(value = "endTime", required = true) String endTime,
            @ApiParam(name = "ssxqId", value = "辖区ID") @RequestParam(value = "ssxqId", required = false) String ssxqId) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            result = dataService.getCountJq(startTime, endTime,ssxqId);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/getCountEventFeedbackOverTime", method = RequestMethod.GET)
    @ApiOperation(value = "内部测试接口---获取反馈超时数据")
    public RetResult<List<WSEventFeedbackOverTimeEntity>> getCountEventFeedbackOverTime(
            @ApiParam(name = "overTime", value = "超时时间", example = "2019-10-17 12:00:00") @RequestParam(value = "overTime") String overTime) {
        List<WSEventFeedbackOverTimeEntity> result = null;
        try {
            SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date overTime2 = DateUtil.strToDate(overTime, datetimeFormat);
            result = dataService.getCountEventFeedbackOverTime(overTime2);

            Message msg = new Message();
            msg.setCode(MessageType.ALARM_DELAY.name());
            msg.setContent(result);

            log.info("sink rabbitmq msg - " + msg.toString());
            producerOperator.sendMsg("", JSON.toJSONString(msg));

        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

}
