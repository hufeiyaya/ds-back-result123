package com.mti.zhpt.dao;

import java.util.List;
import java.util.Map;

public interface DutyMapper {

	List<Map<String, Object>> queryDuty();

	List<Map<String, Object>> queryDutyPcs();
	
	List<Map<String, Object>> queryDutyZsjz();

}
