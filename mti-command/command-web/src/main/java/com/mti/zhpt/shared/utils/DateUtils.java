package com.mti.zhpt.shared.utils;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	public static String createDate()
    {
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    	String date = sdf.format(new Date());
    	return date;
    }

	public static String getToday()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdf.format(new Date());
		return date;
	}

	public static String getYesterday()
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE,-1);
		Date time = cal.getTime();
		return new SimpleDateFormat("yyyy-MM-dd").format(time);
	}

	/*
	 * 将时间转换为时间戳
	 */
	public static String dateToStamp(String s) throws ParseException {
		String res;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = simpleDateFormat.parse(s);
		long ts = date.getTime();
		res = String.valueOf(ts);
		return res;
	}

	/*
	 * 将时间戳转换为时间
	 */
	public static String stampToDate(String s){
		String res;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long lt = new Long(s);
		Date date = new Date(lt);
		res = simpleDateFormat.format(date);
		return res;
	}

	/**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd HH:mm:ss
     *
     * @param strDate
     * @return
     */
	public static Date strToDateLong(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	/**
	 * 格式化日期
	 *
	 * @param date
	 * @param pattern "date":返回日期(yyyy-MM-dd)
	 *                "time":返回时间(HH:mm:ss)
	 *                "datetime":返回日期和时间(yyyy-MM-dd HH:mm:ss)
	 * @return
	 */
	public static String formatDates(Date date, String pattern) {
		if (date == null) {
			return "";
		}

		if ("date".equals(pattern)) {
			pattern = "yyyy-MM-dd";
		} else if ("yyyyMMddHHmm".equals(pattern)) {
			pattern = "yyyy-MM-dd HH:mm";
		} else if ("datetime".equals(pattern)) {
			pattern = "yyyy-MM-dd HH:mm:ss";
		} else if ("datetimeHMS".equals(pattern)) {
			pattern = "yyyyMMddHHmmssSSS";
		}

		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		return formatter.format(date);
	}

	public static Date getDate(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}


	public static Date getDayOfLastMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		return cal.getTime();
	}

	public static String getStrOfLastMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");

		return formatter.format(cal.getTime());
	}

	public static int getDaysOfLastMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}


	public static String getFirstDayOfLastMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
		return formatter.format(cal.getTime()) + "-01";
	}

	public static String getLastDayOfLastMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
		return formatter.format(cal.getTime()) + "-" + cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	public static int getDaysOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	public static Boolean isToday(String strDate) {

		Boolean flag = false;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		Date date = cal.getTime();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String todayStr = formatter.format(date);
		if (strDate.equals(todayStr)) {
			flag = true;
		}
		return flag;
	}

	public static String getTodayTime() {

		Boolean flag = false;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		Date date = cal.getTime();
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		String todayStr = formatter.format(date);
		return todayStr;
	}

}
