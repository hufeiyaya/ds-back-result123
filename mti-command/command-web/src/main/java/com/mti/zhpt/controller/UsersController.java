package com.mti.zhpt.controller;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mti.zhpt.model.UsersEntity;
import com.mti.zhpt.service.impl.UsersServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

/*@RestController
@RequestMapping(path = "/user")*/
public class UsersController {

    @Resource
    private UsersServiceImpl usersService;

    @RequestMapping(path = "/name", method = RequestMethod.GET)
    public RetResult<Object> query(@RequestParam String name){
        List<UsersEntity> result = usersService.queryName(name);
        return  RetResponse.makeOKCp(result);
    }

}
