package com.mti.zhpt.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface OrgMapper {
	@Select("select dept_name as org_name,longitude as org_longitude,latitude as org_latitude from cp_org where id=#{id}")
	Map<String,Object> getOrgNameById(@Param(value = "id") String id);

}