package com.mti.zhpt.aop;

import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.shared.Exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import javax.xml.crypto.dsig.SignatureMethod;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe
 **/
@Slf4j
@Aspect
@Component
public class BaseServiceArgumentsAop {
    /**
     * 拦截所有baseService的分页方法
     */
    @Pointcut("execution(* com.mti.zhpt.service.BaseService.list(..))")
    public void pointCut(){}

    @Around("pointCut()")
    public Object joinPoint(ProceedingJoinPoint joinPoint){
        //获取前两个参数
        Object[] args = joinPoint.getArgs();
        if(args.length>=2){
            Integer page = (Integer) args[0];
            Integer size = (Integer) args[1];
            if(page==null||size==null)
                throw new BusinessException("分页参数不可以为空");
            if(page<=0){
                log.warn("页码参数有误,使用默认页面数");
                page = 0;
            }else{
                page = page<=1?0:page-1;
            }
            if(size<=0){
                log.warn("分页大小参数有误,使用默认分页大小数");
                size=10;
            }
            args[0] = page;
            args[1] = size;
        }
        try {
            Object o = joinPoint.proceed(args);
            if(o instanceof PageModel){
                PageModel pageModel = (PageModel) o;
                pageModel.setPage(pageModel.getPage()+1);
                o = pageModel;
            }
            return o;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            log.error("error==>{}", throwable.getMessage());
        }
        return null;
    }
}
