package com.mti.zhpt.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mti.zhpt.dao.IpWarningMapper;

@Service("ipWarningServiceImpl")
public class IpWarningServiceImpl {

	@Autowired
	private IpWarningMapper ipWarningMapper;
	
	public List<Map<String, Object>> queryList() {
		List<Map<String, Object>> result = null;
		try {
			result = ipWarningMapper.queryList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public Map<String, Object> receive(String id) {
		Date now=new Date();
		ipWarningMapper.receive(id,now);
		return ipWarningMapper.selectById(id);
	}
	
	public Map<String, Object> recover(String id) {
		ipWarningMapper.recover(id);
		return ipWarningMapper.selectById(id);
	}
	
	public Map<String, Object> respond(String id, String org_id, String respond_detail) {
		Date now=new Date();
		ipWarningMapper.respond(id,org_id,respond_detail,now);
		return ipWarningMapper.selectById(id);
	}

	public List<Map<String, Object>> getRespondOrgList() {
		return ipWarningMapper.getRespondOrgList();
	}

	public List<Map<String, Object>> getRespondOrgPersonList(String org_id) {
		return ipWarningMapper.getRespondOrgPersonList(org_id);
	}

}
