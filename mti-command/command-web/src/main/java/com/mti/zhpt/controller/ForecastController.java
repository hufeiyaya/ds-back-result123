package com.mti.zhpt.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mti.zhpt.service.impl.ForeCastServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * 预测预警 yingjj@mti-sh.cn 2019年4月23日10:51
 */
@RestController
@RequestMapping(path = "/forecast")
@Api(value = "/forecast", tags = "预测预警")
@Slf4j
public class ForecastController {

	@Autowired
	private ForeCastServiceImpl foreCastService;

	@RequestMapping(path = "/list", method = RequestMethod.GET)
	@ApiOperation(value = "查询预测预警列表")
	public RetResult<List<Map<String, Object>>> queryForeCast() {

		List<Map<String, Object>> result = null;
		try {
			result = foreCastService.queryForeCast();
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/queryNearBy", method = RequestMethod.GET)
	@ApiOperation(value = "根据foreCastId、距离搜索周边资源")
	public RetResult<Map<String, Object>> queryNearBy(
			@ApiParam(required = true, name = "forecastId", value = "预测预警ID", example = "1") @RequestParam Integer forecastId,
			@ApiParam(required = true, name = "dis", value = "搜索范围(米)", example = "10000") @RequestParam Integer dis,
			@ApiParam(required = false, name = "orderbydis", value = "是否按照距离排序", example = "true") @RequestParam(value = "orderbydis", required = false, defaultValue = "true") boolean orderbydis) {
		Map<String, Object> result = null;
		try {
			result = foreCastService.queryNearBy(forecastId, dis, orderbydis);
		} catch (Exception e) {
			e.printStackTrace();
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/updateNewFlag", method = RequestMethod.GET)
	@ApiOperation(value = "更新预测预警信息已读状态")
	public RetResult<Integer> updateForeCastNewFlag(@RequestParam("forecastid") int forecastid) {

		int result = 0;
		try {
			result = foreCastService.updateForeCastNewFlag(forecastid);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/updateForeCastContent", method = RequestMethod.POST)
	@ApiOperation(value = "更新预测预警信息内容")
	public RetResult<Integer> updateForeCastContent(@RequestBody() Map<String, Object> record) {
		int result = 0;
		try {
			result = foreCastService.updateForeCastContent(record);
		} catch (Exception e) {
			e.printStackTrace();
			return RetResponse.makeErrCp(e.getMessage());

		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/updateForeCastStatus", method = RequestMethod.GET)
	@ApiOperation(value = "更新预测预警状态")
	public RetResult<Integer> updateForeCastStatus(
			@ApiParam(required = true, name = "foreCastId", value = "预测预警ID", example = "1") @RequestParam(value = "foreCastId", required = true) Integer foreCastId,
			@ApiParam(required = true, name = "status", value = "预测预警状态0-3含义待定", example = "1") @RequestParam(value = "status", required = true) Integer status) {
		int result = 0;
		try {
			result = foreCastService.updateForeCastStatus(foreCastId, status);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

}
