package com.mti.zhpt.service.impl;

import com.mti.zhpt.shared.Exception.BusinessException;
import com.mti.zhpt.shared.core.configurer.TCPSocket;
import com.mti.zhpt.utils.CRCCheckUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * zpf
 * 2020年1月7日
 */
@Service("lightServiceImpl")
@Slf4j
public class LightServiceImpl {

    @Autowired
    private TCPSocket tcpSocket;

    private boolean onOff = false;
    private String serial;
    private String colour;


    public void socketMessage(String command, String number, String col) {
        try {
            if (StringUtils.isEmpty(command)) {
                throw new BusinessException("command  1开灯，0关灯，闪烁2：command not null");
            }
            if (StringUtils.isEmpty(number)) {
                throw new BusinessException("number  灯号（台号）1/2/3/4/5/6/7/8/9/254(254代表全部灯)：number not null");
            }
            colour = col;
            switch (number) {
                case "1":
                    serial = "01 ";
                    onOff(command);
                    break;
                case "2":
                    serial = "02 ";
                    onOff(command);
                    break;
                case "3":
                    serial = "03 ";
                    onOff(command);
                    break;
                case "4":
                    serial = "04 ";
                    onOff(command);
                    break;
                case "5":
                    serial = "05 ";
                    onOff(command);
                    break;
                case "6":
                    serial = "06 ";
                    onOff(command);
                    break;
                case "7":
                    serial = "07 ";
                    onOff(command);
                    break;
                case "8":
                    serial = "08 ";
                    onOff(command);
                    break;
                case "9":
                    serial = "09 ";
                    onOff(command);
                    break;
                case "254":
                    onOffAll(command);
                    break;
                default:
                    throw new BusinessException("无 灯号:"+number+ "_Exception");
            }
        } catch (Exception e) {
            log.error("蓝绿红橙灯", e);
            throw new BusinessException(e.getMessage());
        }
    }

    private void onOff(String command) throws Exception {
        if (command.equals(constant.ONE)) {
            if (StringUtils.isEmpty(colour)) {
                throw new BusinessException("开灯：colour not null");
            }

            String strStop = serial + "0F 00 00 00 06 01 00";

            String blue = serial + "10 00 03 00 02 04 00 04 00 01";
            String green = serial + "10 00 08 00 02 04 00 04 00 01";
            String red = serial + "10 00 0D 00 02 04 00 04 00 01";
            String orange = serial + "10 00 12 00 02 04 00 04 00 01";
            commandColour(colour, strStop, blue, green, red, orange);

        } else if (command.equals(constant.ZERO)) {
            String strStop = serial + "0F 00 00 00 06 01 00";
            String strStopCommand = serial + "10 00 1C 00 02 04 00 04 00 01";
            commandMessage(strStop, strStopCommand);
        } else if (command.equals(constant.FLICKER)||command.equals(constant.NOT_FLICKER)) {

            if (command.equals(constant.FLICKER)){
                if (StringUtils.isEmpty(colour)) {
                    throw new BusinessException("闪烁：colour not null");
                }
                onOff = true;
            }
            if (command.equals(constant.NOT_FLICKER)){
                onOff = false;
            }
            if (onOff){
                OnOffThread thread =  new OnOffThread();
                thread.start();
            }
        }else {
            throw new BusinessException("command："+command +"not value");
        }
    }


    private void onOffAll(String command) throws Exception {
        if (command.equals(constant.ONE)) {
            if (StringUtils.isEmpty(colour)) {
                throw new BusinessException("开灯：colour not null");
            }
            String strStop = "FE 0F 00 00 00 06 01 00";

            String blue =   "FE 10 00 03 00 02 04 00 04 00 01";
            String green =  "FE 10 00 08 00 02 04 00 04 00 01";
            String red =    "FE 10 00 0D 00 02 04 00 04 00 01";
            String orange = "FE 10 00 12 00 02 04 00 04 00 01";
            commandColour(colour, strStop, blue, green, red, orange);
        } else if (command.equals(constant.ZERO)) {
            String strStop = "FE 0F 00 00 00 06 01 00";
            String strStopCommand =  "FE 10 00 1C 00 02 04 00 04 00 01";
            commandMessage(strStop, strStopCommand);
        } else if (command.equals(constant.FLICKER)||command.equals(constant.NOT_FLICKER)) {


            if (command.equals(constant.FLICKER)){
                if (StringUtils.isEmpty(colour)) {
                    throw new BusinessException("闪烁：colour not null");
                }
                onOff = true;
            }
            if (command.equals(constant.NOT_FLICKER)){
                onOff = false;
            }
            if (onOff){
                OnOffAllThread thread =  new OnOffAllThread();
                thread.start();
            }

        }else {
            throw new BusinessException("command："+command +"not value");
        }
    }
    private void commandColour(String colour, String strStop, String blue, String green, String red, String orange) throws Exception {
        if (colour.equals(constant.BLUE)) {
            commandMessage(strStop, blue);
        } else if (colour.equals(constant.GREEN)) {
            commandMessage(strStop, green);
        } else if (colour.equals(constant.RED)) {
            commandMessage(strStop, red);
        } else if (colour.equals(constant.ORANGE)) {
            commandMessage(strStop, orange);
        }
    }

    private void commandMessage(String strStop, String str) throws Exception {
        tcpSocket.sendMessage(strStop + " " + CRCCheckUtils.getCRC(strStop));
        Thread.currentThread().sleep(110);//毫秒
        tcpSocket.sendMessage(str + " " + CRCCheckUtils.getCRC(str));
    }

    public static class constant {
        public static final String ONE = "1";
        public static final String ZERO = "0";
        public static final String FLICKER = "2";
        public static final String NOT_FLICKER = "3";

        public static final String BLUE = "blue";
        public static final String GREEN = "green";
        public static final String RED = "red";
        public static final String ORANGE = "orange";

    }


    class OnOffThread extends Thread {

        @Override
        public void run() {
            try {
                while (onOff) {
                    /**
                     * 开
                     */
                    String strStop = serial + "0F 00 00 00 06 01 00";

                    String blue = serial + "10 00 03 00 02 04 00 04 00 01";
                    String green = serial + "10 00 08 00 02 04 00 04 00 01";
                    String red = serial + "10 00 0D 00 02 04 00 04 00 01";
                    String orange = serial + "10 00 12 00 02 04 00 04 00 01";
                    commandColour(colour, strStop, blue, green, red, orange);

                    Thread.currentThread().sleep(1000);//毫秒
                    /**
                     * 关
                     */
                    String strStopCommand =  "FE 10 00 1C 00 02 04 00 04 00 01";
                    commandMessage(strStop, strStopCommand);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class OnOffAllThread extends Thread {

        @Override
        public void run() {
            try {
                while (onOff) {
                    /**
                     * 开
                     */
                    String strStop = "FE 0F 00 00 00 06 01 00";

                    String blue =   "FE 10 00 03 00 02 04 00 04 00 01";
                    String green =  "FE 10 00 08 00 02 04 00 04 00 01";
                    String red =    "FE 10 00 0D 00 02 04 00 04 00 01";
                    String orange = "FE 10 00 12 00 02 04 00 04 00 01";
                    commandColour(colour, strStop, blue, green, red, orange);

                    Thread.currentThread().sleep(1000);//毫秒
                    /**
                     * 关
                     */
                    String strStopCommand = serial + "10 00 1C 00 02 04 00 04 00 01";
                    commandMessage(strStop, strStopCommand);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }





}
