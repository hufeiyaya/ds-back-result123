package com.mti.zhpt.dao;

import com.mti.zhpt.model.relationnet.ContactRadioPhoneDispatchEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
public interface ContactRadioPhoneDispatchMapper {
    List<ContactRadioPhoneDispatchEntity> listAll(
            @Param(value = "page")int page,
            @Param(value = "size")int size,
            @Param(value = "unitName")String unitName,
            @Param(value = "phone")String phone
    );
    long countRows(
            @Param(value = "unitName")String unitName,
            @Param(value = "phone")String phone
    );

    void save(ContactRadioPhoneDispatchEntity entity);
    void update(ContactRadioPhoneDispatchEntity entity);
    void deleteById(@Param(value = "id")String id);
    ContactRadioPhoneDispatchEntity getOneById(@Param(value = "id")String id);
}
