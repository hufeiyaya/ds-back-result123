package com.mti.zhpt.controller;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.mti.zhpt.dto.SubsidiaryFeedback;
import com.mti.zhpt.model.*;
import com.mti.zhpt.vo.EventReserveVo;
import io.swagger.annotations.ApiImplicitParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.mti.zhpt.service.impl.CommandReserveServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 预案指挥
 * @Author: zhaopf@mti-sh.cn
 * @Date: 2019/4/22 17:04
 */
@RestController
@RequestMapping(path = "/command/reserve")
@Api(value = "/command/reserve", tags = {"预案指挥"})
@Slf4j
public class CommandReserveController {

    @Autowired
    private CommandReserveServiceImpl commandReserveService;

    @RequestMapping(path = "/query-all", method = RequestMethod.GET)
    @ApiOperation(value = "查询预案列表")
    public RetResult<PageInfo> queryAll(
            @ApiParam(name = "event_id", value = "案件ID", example = "6") @RequestParam(value = "event_id", required = false) String event_id,
            @ApiParam(required = false, name = "param", value = "查询条件", example = "预案") @RequestParam(value = "param", required = false, defaultValue = "") String param,
            @ApiParam(required = false, name = "ssxqPrefix", value = "所属辖区前6位") @RequestParam(value = "ssxqPrefix", required = false) String ssxqPrefix,
            @ApiParam(required = true, name = "page", value = "当前页", example = "1") @RequestParam(value = "page", required = true, defaultValue = "") Integer page,
            @ApiParam(required = true, name = "pageSize", value = "每页条数", example = "15") @RequestParam(value = "pageSize", required = true, defaultValue = "") Integer pageSize) {
        PageInfo result = null;
        try {
            result = commandReserveService.queryAll(event_id, param,ssxqPrefix,page,pageSize);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }

        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/query-command-model", method = RequestMethod.GET)
    @ApiOperation(value = "查询预案详情")
    public RetResult<List<Map<Object, Object>>> queryCommandModel(
            @ApiParam(required = true, name = "reserve_id", value = "预案ID", example = "1")
            @RequestParam(value = "reserve_id", required = true) String reserve_id) {
        List<Map<Object, Object>> result = null;
        try {
            result = commandReserveService.queryCommandModel(reserve_id);
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/do-command-model", method = RequestMethod.GET)
    @ApiOperation(value = "启动预案")
    public RetResult<Map<Object, Object>> doCommandModel(
            @ApiParam(required = true, name = "event_id", value = "案件ID", example = "6") @RequestParam(value = "event_id", required = true) String event_id,
            @ApiParam(required = true, name = "reserve_id", value = "预案ID", example = "1") @RequestParam(value = "reserve_id", required = true) String reserve_id) {
        if (StringUtils.isBlank(event_id)) {
            return RetResponse.makeErrCp("event_id不能为空");
        }

        if (StringUtils.isBlank(reserve_id)) {
            return RetResponse.makeErrCp("reserve_id不能为空");
        }

        Map<Object, Object> result = null;
        try {
            result = commandReserveService.doCommandModel(event_id, reserve_id);

        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/cancel-command-model", method = RequestMethod.GET)
    @Deprecated
    @ApiOperation(value = "撤销预案(内部接口)")
    public RetResult<Integer> cancelCommandModel(
            @ApiParam(required = true, name = "event_id", value = "案件ID", example = "6") @RequestParam(value = "event_id", required = true) String event_id) {
        int result = 0;
        try {
            result = commandReserveService.cancelCommandModel(event_id);

        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/taskadd-command", method = RequestMethod.POST)
    @ApiOperation(value = "添加指令")
    public RetResult<List<Map<Object, Object>>> taskaddCommand(@RequestBody List<EventTask> eventTasks) {
        List<Map<Object, Object>> result = null;
        try {
            result = commandReserveService.taskaddCommand(eventTasks);
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/query-task-command", method = RequestMethod.GET)
    @ApiOperation(value = "查询指令")
    public RetResult<List<Map<Object, Object>>> queryTaskCommand(@ApiParam(required = true, name = "event_id", value = "案件ID", example = "6") @RequestParam(value = "event_id", required = true) String event_id) {
        List<Map<Object, Object>> result = null;
        try {
            result = commandReserveService.queryTaskCommand(event_id);
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }


    @RequestMapping(path = "/do-task-command", method = RequestMethod.POST)
    @ApiOperation(value = "执行指令")
    public RetResult<List<Map<Object, Object>>> doTaskCommand(@RequestBody List<EventTask> eventTasks) {
        List<Map<Object, Object>> result = null;
        try {
            result = commandReserveService.doTaskCommand(eventTasks);
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    /**
     * 添加或编辑预案
     *
     * @param eventReserveVo
     * @return
     */
//	@PostMapping(path = "/add-command")
    @RequestMapping(path = "/add-command", method = RequestMethod.POST)
    @ApiOperation(value = "添加或编辑预案", notes = "添加或编辑预案")
//	@ApiImplicitParam(name = "eventReserveVo", value = "预案指挥扩展信息对象", required = false, dataType = "EventReserveVo", paramType = "query")
    public RetResult<Integer> addCommand(@RequestBody EventReserveVo eventReserveVo) {
        try {
            int result = commandReserveService.addCommand(eventReserveVo);
            if (result == 1) {
                return RetResponse.makeOKCp(result);
            }
            return RetResponse.makeErrCp("添加预案失败");
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @RequestMapping(path = "/getByReserveId", method = RequestMethod.GET)
    @ApiOperation(value = "查询预案详情")
    public RetResult<EventReserveVo> getByReserveId(
            @ApiParam(required = true, name = "reserveId", value = "预案ID", example = "1") @RequestParam(value = "reserveId", required = true) String reserveId) {
        EventReserveVo result = null;
        try {
            result = commandReserveService.getByReserveId(reserveId);
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    @RequestMapping(path = "/pageByReserve", method = RequestMethod.POST)
    @ApiOperation(value = "分页查询预案")
    public RetResult pageByReserve(
            @ApiParam(required = true, name = "event_id", value = "案件ID", example = "6") @RequestParam(value = "event_id", required = true) String event_id,
            @ApiParam(required = false, name = "param", value = "查询条件", example = "预案") @RequestParam(value = "param", required = false, defaultValue = "") String param,
            @ApiParam(required = true, name = "page", value = "当前页", example = "1") @RequestParam(value = "page", required = true, defaultValue = "") Integer page,
            @ApiParam(required = true, name = "pageSize", value = "每页条数", example = "15") @RequestParam(value = "pageSize", required = true, defaultValue = "") Integer pageSize) {
        PageInfo result = null;
        try {
            result = commandReserveService.pageByReserve(event_id, param, page, pageSize);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }

        return RetResponse.makeOKCp(result);
    }

    /**
     * 查询下达的指令
     *
     * @param eventId 案件id
     * @return
     */
    @RequestMapping(path = "/queryTaskByeventId", method = RequestMethod.GET)
    @ApiOperation(value = "查询下达的指令")
    public RetResult<Map<Object, Object>> queryTaskByeventId(
            @ApiParam(required = true, name = "eventId", value = "案件id", example = "371405916345466880")
            @RequestParam(value = "eventId", required = true) String eventId,
            @ApiParam(required = true, name = "orgId", value = "登录人组织id", example = "131023470000")
            @RequestParam(value = "orgId", required = true) String orgId) {
        Map<Object, Object> map = null;
        try {
            map = commandReserveService.queryTaskByeventId(eventId,orgId);
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(map);
    }


    /**
     * 针对指令辅助反馈
     */
    @RequestMapping(path = "/subsidiaryFeedback", method = RequestMethod.POST)
    @ApiOperation(value = "指令辅助反馈")
    public RetResult<Map<Object, Object>> subsidiaryFeedback(@RequestBody SubsidiaryFeedback subsidiaryFeedback) {
        try {
          commandReserveService.subsidiaryFeedback(subsidiaryFeedback);
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp();
    }


    /**
     * 查询接收的指令
     *
     * @param policeId 警员id
     * @param orgId    机构id
     * @return
     */
    @RequestMapping(path = "/receiveTask", method = RequestMethod.GET)
    @ApiOperation(value = "查询接收的指令")
    public RetResult<Map<Object, Object>> receiveTask(
            @ApiParam(required = false, name = "policeId", value = "警员id", example = "1") @RequestParam(value = "policeId", required = false) String policeId,
            @ApiParam(required = true, name = "orgId", value = "机构id", example = "131000010100") @RequestParam(value = "orgId", required = true) String orgId,
            @ApiParam(required = true, name = "eventId", value = "警情id", example = "388682076905275392") @RequestParam(value = "eventId", required = true) String eventId
    ) {
        Map<Object, Object> map = null;
        try {
            map = commandReserveService.receiveTask(policeId, orgId,eventId);
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(map);
    }

    @RequestMapping(path = "/updateTasksStatus", method = RequestMethod.GET)
    @ApiOperation(value = "指令单状态更改：10已下达、20已到达、30已接收、40已出动、50已到场、60已反馈、70已作废")
    public RetResult updateTasksStatus(@ApiParam(example = "390218289982210048")@RequestParam String id,
                                  @ApiParam(example = "10")@RequestParam Integer status) {

        try {
            commandReserveService.updateTasksStatus(id, status);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }


    /**
     * 指令反馈
     *
     * @param instructionFeedbackEntity 指令反馈对象
     * @return
     */
    @RequestMapping(path = "/instructionFeedback", method = RequestMethod.POST)
    @ApiOperation(value = "指令反馈")
    public RetResult<Boolean> instructionFeedback(@RequestBody InstructionFeedbackEntity instructionFeedbackEntity) {
        boolean result = false;

        if (null == instructionFeedbackEntity) {
            return RetResponse.makeErrCp("参数对象不能为空");
        }

        if (StringUtils.isBlank(instructionFeedbackEntity.getTaskId())) {
            return RetResponse.makeErrCp("步骤id不能为空");
        }

        if (StringUtils.isBlank(instructionFeedbackEntity.getFeedbackOrgId())) {
            return RetResponse.makeErrCp("机构id不能为空");
        }

        if (StringUtils.isBlank(instructionFeedbackEntity.getFeedbackOrgName())) {
            return RetResponse.makeErrCp("机构名称不能为空");
        }

        if (StringUtils.isBlank(instructionFeedbackEntity.getFeedbackPoliceId())) {
            return RetResponse.makeErrCp("反馈人id不能为空");
        }

        if (StringUtils.isBlank(instructionFeedbackEntity.getFeedbackPoliceName())) {
            return RetResponse.makeErrCp("反馈人姓名不能为空");
        }

        if (StringUtils.isBlank(instructionFeedbackEntity.getFeedbackPhone())) {
            return RetResponse.makeErrCp("反馈电话不能为空");
        }

        if (StringUtils.isBlank(instructionFeedbackEntity.getFeedbackContent())) {
            return RetResponse.makeErrCp("反馈内容不能为空");
        }

        try {
            result = commandReserveService.instructionFeedback(instructionFeedbackEntity);
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    /**
     * 反馈列表查询
     *
     * @param instructionFeedbackEntity 反馈指令对象
     * @return
     */
    @RequestMapping(path = "/listByInstructionFeedbackEntity", method = RequestMethod.POST)
    @ApiOperation(value = "反馈列表查询")
    public RetResult<List<InstructionFeedbackEntity>> listByInstructionFeedbackEntity(@RequestBody InstructionFeedbackEntity instructionFeedbackEntity) {
        List<InstructionFeedbackEntity> list = null;
        try {
            list = commandReserveService.listByInstructionFeedbackEntity(instructionFeedbackEntity);
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(list);
    }

    /**
     * 单位指令监控列表查询
     *
     * @param eventId 案件id
     * @return
     */
    @GetMapping("/listByunitInstructionMonitoring")
    @ApiOperation(value = "单位指令监控列表查询")
    public RetResult< List<Map<Object, Object>>> listByunitInstructionMonitoring(@ApiParam(required = true, name = "eventId", value = "案件ID", example = "6")
                                                                              @RequestParam(value = "eventId", required = true) String eventId) {
        if (StringUtils.isBlank(eventId)) {
            return RetResponse.makeErrCp("eventId不能为空");
        }
        List<Map<Object, Object>> result = null;
        try {
            result = commandReserveService.listByunitInstructionMonitoring(eventId);
        } catch (Exception e) {
            e.printStackTrace();
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }

    /**
     * 模拟演练
     *
     * @param reserveId 预案id
     * @return
     */
    @PostMapping("simulationExercise")
    @ApiOperation(value = "模拟演练")
    public RetResult simulationExercise(@RequestParam("reserveId") String reserveId) {
        if (StringUtils.isBlank(reserveId)) {
            return RetResponse.makeErrCp("预案id不能为空");
        }

        Boolean result = commandReserveService.simulationExercise(reserveId);
        return RetResponse.makeOKCp(result);
    }

}
