package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.ContactBTHProvincialDepartmentMapper;
import com.mti.zhpt.model.relationnet.ContactBTHProvincialDepartmentEntity;
import com.mti.zhpt.model.relationnet.PageModel;
import com.mti.zhpt.service.BaseService;
import com.mti.zhpt.utils.SnowFlake;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe describe
 **/
@Service(value = "ContactBTHProvincialDepartmentEntity")
@AllArgsConstructor
public class ContactBTHProvincialDepartmentServiceImpl implements BaseService<ContactBTHProvincialDepartmentEntity> {
    private final ContactBTHProvincialDepartmentMapper mapper;
    @Override
    public void save(ContactBTHProvincialDepartmentEntity contactBTHProvincialDepartmentEntity) {
        mapper.save(contactBTHProvincialDepartmentEntity);
    }

    @Override
    public ContactBTHProvincialDepartmentEntity getOneById(String id) {
        return mapper.getOneById(id);
    }

    @Override
    public void deleteById(String id) {
        if(!StringUtils.isEmpty(id))
            mapper.deleteById(id);
    }

    @Override
    public void update(ContactBTHProvincialDepartmentEntity contactBTHProvincialDepartmentEntity) {
        mapper.update(contactBTHProvincialDepartmentEntity);
    }

    @Override
    public PageModel<List<ContactBTHProvincialDepartmentEntity>> list(Integer page, Integer size, String... searchTerm) {
        PageModel<List<ContactBTHProvincialDepartmentEntity>> pageModel = new PageModel<>();
        pageModel.setRecord(mapper.listAll(page, size,searchTerm[0],searchTerm[1],searchTerm[2]));
        pageModel.setPage(page);
        pageModel.setSize(size);
        pageModel.setTotal(mapper.countRows(searchTerm[0],searchTerm[1],searchTerm[2]));
        return pageModel;
    }

    @Override
    public PageModel<List<String>> listByGroupBy(Integer page, Integer size, String... groupTerm) {
        return null;
    }
}
