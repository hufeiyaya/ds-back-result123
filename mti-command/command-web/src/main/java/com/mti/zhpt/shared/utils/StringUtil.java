package com.mti.zhpt.shared.utils;

/**
 * @author can
 *String工具类
 */
public class StringUtil {
	
	  /** 将元数据前补零，补后的总长度为指定的长度，以字符串的形式返回 
	　　* @param sourceDate 
	　　* @param formatLength 
	　　* @return 重组后的数据 
	　　*/  
	public static String frontCompWithZore(int sourceDate,int formatLength) {
	  String formatString = String.format("%0"+formatLength+"d", sourceDate);  
	  return formatString;
	}


	/**
	 * 判断字符串是空
	 *
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		return "".equals(str) || str == null;
	}

	/**
	 * 判断字符串不是空
	 *
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(String str) {
		return !"".equals(str) && str != null;
	}


}
