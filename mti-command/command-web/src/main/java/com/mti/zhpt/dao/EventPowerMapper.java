package com.mti.zhpt.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.mti.zhpt.model.EventTask;

public interface EventPowerMapper {
	@Select("select a.*,b.dept_name,d.stuff_name as leader,d.stuff_tel as leader_phone from cp_event_power a left join cp_org b on a.org_id=b.id left join cp_org_duty c on a.org_id=c.org_id left join cp_org_stuff d on c.stuff_id=d.stuff_id where a.event_id=#{eventId}")
	List<Map<String, Object>> queryEventPowers(String eventId);

	@Insert("insert into cp_event_power (event_id,org_id,waiting,present,underway) values (#{event_id},#{org_id},#{reserve_number},0,#{dispatch_number})")
	void insertEventPower(EventTask eventTask);

	@Delete("delete from cp_event_power where event_id=#{eventId}")
	int delEventPower(String eventId);

	@Update("update cp_event_power set underway=underway+1 where event_id=#{event_id} and org_id=#{org_id}")
	int updateEventPowerOrgPlusPerson(String event_id, String org_id);

	@Select("select count(0) from cp_event_power where event_id=#{event_id} and org_id=#{org_id}")
	int queryOrg(String event_id, String org_id);

	@Insert("insert into cp_event_power (event_id,org_id,waiting,present,underway) values (#{event_id},#{org_id},0,0,1)")
	int newEventPowerOrgPlusPerson(String event_id, String org_id);

	@Update("update cp_event_power set underway=underway+#{dispatch_number},waiting=waiting+#{reserve_number} where event_id=#{event_id} and org_id=#{org_id}")
	void updateEventPower(EventTask eventTask);

}