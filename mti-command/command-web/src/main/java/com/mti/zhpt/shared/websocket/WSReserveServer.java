package com.mti.zhpt.shared.websocket;//package com.mti.zhpt.shared.websocket;
//
//import com.alibaba.fastjson.JSON;
//import com.mti.zhpt.model.websocket.WSCustomEntity;
//import com.mti.zhpt.shared.classHelper.SpringContext;
//import com.mti.zhpt.shared.classHelper.TaskHandler;
//import com.mti.zhpt.shared.core.ret.RetResponse;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.util.CollectionUtils;
//
//import javax.websocket.*;
//import javax.websocket.server.PathParam;
//import javax.websocket.server.ServerEndpoint;
//import java.io.IOException;
//import java.util.*;
//import java.util.concurrent.ConcurrentHashMap;
//
///**
//* @Description: 预案指挥-执行
//* @Author: zhaopf@mti-sh.cn
//* @Date: 2019/4/23 11:53
//*/
//@ServerEndpoint("/webSocket/reserve/{userId}")
//@Controller
//public class WSReserveServer {
//    private Logger logger = LoggerFactory.getLogger(WSReserveServer.class);
//    private static String userId;
//
//	public static TaskHandler taskHandler;
//
//	@Autowired
//	private void setTaskHandler(TaskHandler taskHandler) {
//		WSCustomServer.taskHandler = taskHandler;
//	}
//
////    TaskHandler taskHandler = SpringContext.getBean(TaskHandler.class);
//
//    // 使用map来收集session，key为name，value为用户
//    // concurrentMap的key不存在时报错，不是返回null
//    private static final Map<String, Session> nameMap = new ConcurrentHashMap();
//    List<WSCustomEntity> wsCustomEntity;
//
//
//    @OnOpen
//    public void connect(@PathParam("userId") String userId, Session session) throws Exception {
//        this.userId = userId;
//        // 将session按照用户来存储
//        if (!nameMap.containsKey(userId)) {
//            // 用户不存在时
//            // 添加用户
//            nameMap.put(userId, session);
//        }
////        else {
////            // 用户已存在
////            session.getBasicRemote().sendText(userId+"已经建立连接");
////            session.close();
////
////        }
//        logger.info("新连接：{}", userId);
//
//    }
//
//    @OnClose
//    public void disConnect(@PathParam("userId") String userId, Session session) {
//        nameMap.remove(userId);
//        logger.info("连接：{} 关闭", userId );
//
//    }
//
//    @OnMessage
//    public void receiveMsg(@PathParam("userId") String userId,
//                           String msg, Session session) throws Exception {
//
//        try {
//            logger.info("收到用户{}的消息:{}", userId, msg);
//
//            wsCustomEntity = JSON.parseArray(msg,  WSCustomEntity.class);
//            if (CollectionUtils.isEmpty(wsCustomEntity)){
//                return;
//            }
//
//            /**
//             * 给web端的结果
//             */
//            List<Map> list = new ArrayList<>();
//            if (userId.equals("web")){
//                wsCustomEntity .forEach((WSCustomEntity entity) ->{
//                    Map<Object,Object>  map = new HashMap<>();
//                    map.put("createTime",entity.getCreateTime());
//                    map.put("feedback","指令下达");
//                    map.put("remark",entity.getRemark());
//                    list.add(map);
//                    entity.setFeedback("指令下达");
//                });
//            }
//
//            /**
//             * 多线程处理
//             * （线程池）数据存储
//             */
//            taskHandler.handler(wsCustomEntity);
//            String webResult = JSON.toJSONString(list);
//            String AppResult = JSON.toJSONString(wsCustomEntity);
//
//
//            List<String> userIdList = Arrays.asList("web", "app");
//
//            for (String Id : userIdList) {
//                nameMap.forEach((k,v)->{
//                    if (Id.equals(k)&&k.equals("web")){
//                        try {
//                            v.getBasicRemote().sendText(JSON.toJSONString(RetResponse.makeOKCp(webResult)));
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                    if (Id.equals(k)&&k.equals("app")){
//                        try {
//                            v.getBasicRemote().sendText(JSON.toJSONString(RetResponse.makeOKCp(AppResult)));
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                });
//
//            }
//
//
//
//
//        } catch (Exception e) {
//            logger.error("用户id为：{}的连接发送错误", this.userId);
//        }
//    }
//
//
//    //连接错误时执行
//    @OnError
//    public void onError(Session session, Throwable error) {
//        logger.info("用户id为：{}的连接发送错误", this.userId);
//        error.printStackTrace();
//    }
//}