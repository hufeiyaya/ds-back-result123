package com.mti.zhpt.controller;

import com.mti.zhpt.model.CpReserveProcess;
import com.mti.zhpt.service.impl.CpReserveProcessServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 预案流程 控制器
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-21
 */
@RestController
@RequestMapping(path = "/reserveProcess")
@Api(value = "/reserveProcess", tags = "预案流程")
@Slf4j
public class CpReserveProcessController {

	@Autowired
	CpReserveProcessServiceImpl cpReserveProcessService;

	/**
	 * 根据预案id查询流程信息列表
	 *
	 * @param reserveId 预案id
	 * @return
	 */
	@GetMapping(path = "/listByReserveId")
	@ApiOperation(value = "根据预案id查询流程信息列表")
	public RetResult<List<CpReserveProcess>> listByReserveId(
			@ApiParam(required = true, name = "reserveId", value = "预案id", example = "") @RequestParam(value = "reserveId", required = true) String reserveId) {
		if (StringUtils.isBlank(reserveId)) {
			return RetResponse.makeErrCp("预案id不能为空");
		}
		List<CpReserveProcess> cpReserveProcesses = cpReserveProcessService.listByReserveId(reserveId);
		return RetResponse.makeOKCp(cpReserveProcesses);
	}

	/**
	 * 根据预案id查询流程信息
	 *
	 * @param reserveId 预案id
	 * @return
	 */
	@GetMapping(path = "/getByReserveId")
	@ApiOperation(value = "根据预案id查询流程信息")
	public RetResult<CpReserveProcess> getByReserveId(
			@ApiParam(required = true, name = "reserveId", value = "预案id", example = "") @RequestParam(value = "reserveId", required = true) String reserveId) {
		if (StringUtils.isBlank(reserveId)) {
			return RetResponse.makeErrCp("预案id不能为空");
		}
		CpReserveProcess cpReserveProcess = cpReserveProcessService.getByReserveId(reserveId);
		return RetResponse.makeOKCp(cpReserveProcess);
	}

	/**
	 * 批量保存预案流程
	 *
	 * @param cpReserveProcessList 预案流程对象
	 * @return
	 */
	@PostMapping(path = "saveCpReserveProcessBatch")
	@ApiOperation(value = "批量保存预案流程")
	public RetResult saveCpReserveProcessBatch(@RequestBody List<CpReserveProcess> cpReserveProcessList) {
		int result = cpReserveProcessService.saveCpReserveProcessBatch(cpReserveProcessList);
		return RetResponse.makeOKCp(result);
	}

	/**
	 * 保存或编辑预案流程
	 *
	 * @param cpReserveProcess 预案流程对象
	 * @return
	 */
	@PostMapping(path = "saveOrUpdateCpReserveProcess")
	@ApiOperation(value = "保存或编辑预案流程")
	public RetResult saveOrUpdateCpReserveProcess(@RequestBody CpReserveProcess cpReserveProcess) {
		int result = cpReserveProcessService.saveOrUpdateCpReserveProcess(cpReserveProcess);
		return RetResponse.makeOKCp(result);
	}

}
