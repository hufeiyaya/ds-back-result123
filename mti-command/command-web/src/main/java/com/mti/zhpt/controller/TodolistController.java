package com.mti.zhpt.controller;

import com.github.pagehelper.PageInfo;
import com.mti.zhpt.model.TodolistEntity;
import com.mti.zhpt.service.impl.TodolistServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: yangsp@mti-sh.cn
 * @Date: 2019/6/23
 */
@RestController
@RequestMapping(path = "/todolist")
@Api(value = "/todolist", tags = "代办事项")
@Slf4j
public class TodolistController {
	@Autowired
	private TodolistServiceImpl todolistService;

	@RequestMapping(path = "/pageByEntity", method = RequestMethod.POST)
	@ApiOperation(value = "根据代办事项对象分页查询案件集合")
	public RetResult pageByEntity(@RequestBody TodolistEntity entity) {
		PageInfo result = null;
		try {
			result = todolistService.pageByTodolist(entity);
		} catch (Exception e) {
			log.error(e.getMessage());
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(result);
	}

	@RequestMapping(path = "/addByEntity", method = RequestMethod.POST)
	@ApiOperation(value = "添加新待办事项")
	public RetResult<TodolistEntity> addByEntity(@RequestBody() TodolistEntity entity) {
		try {
			todolistService.addByEntity(entity);
		} catch (Exception e) {
			e.printStackTrace();
			return RetResponse.makeErrCp(e.getMessage());
		}
		return RetResponse.makeOKCp(entity);
	}

}
