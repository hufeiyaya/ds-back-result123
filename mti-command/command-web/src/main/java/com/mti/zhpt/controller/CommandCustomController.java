package com.mti.zhpt.controller;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mti.zhpt.service.impl.CommandCustomServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 精准指挥
 * @Author: zhaopf@mti-sh.cn
 * @Date: 2019/4/19 17:42
 */
@RestController
@RequestMapping(path = "/command/custom")
@Api(value = "/command/custom", tags = {"精准指挥"})
@Slf4j
public class CommandCustomController {
    @Resource
    private CommandCustomServiceImpl commandCustomService;
    
    @RequestMapping(path = "/query-all", method = RequestMethod.GET)
    @ApiOperation(value = "查询能够下达指令的组织结构树")
    public RetResult<List<Map<Object, Object>>> queryAll() {
        List<Map<Object, Object>> result = null;
        try {
            result = commandCustomService.queryAll();
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
        return RetResponse.makeOKCp(result);
    }
}
