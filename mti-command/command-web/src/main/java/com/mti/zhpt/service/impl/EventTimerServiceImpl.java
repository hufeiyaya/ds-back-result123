package com.mti.zhpt.service.impl;

import com.mti.zhpt.dao.CommandReserveMapper;
import com.mti.zhpt.dao.EventTimerMapper;
import com.mti.zhpt.model.EventTask;
import com.mti.zhpt.model.EventTimerEntity;
import com.mti.zhpt.utils.SnowFlake;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 警情时间轴 实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-05
 */
@Service("eventTimerServiceImpl")
@Slf4j
@Transactional
public class EventTimerServiceImpl {

	@Autowired
	EventTimerMapper eventTimerMapper;

	@Autowired
	CommandReserveMapper commandReserveMapper;

	@Autowired
	SnowFlake snowFlake;

	/**
	 * 批量插入时间轴信息
	 *
	 * @param eventTimerEntityList 时间轴列表
	 * @return
	 */
	@Transactional
	public boolean insertEventTimerBatch(List<EventTimerEntity> eventTimerEntityList) {
		Integer result = 0;
		try {
			if (null != eventTimerEntityList && eventTimerEntityList.size() > 0) {
                /**
                 * 批示内容是否已传达
                 */
				if (eventTimerEntityList.get(0).getType()!=null&&eventTimerEntityList.get(0).getType().equals("cd")){
					eventTimerMapper.updateResponse(eventTimerEntityList.get(0).getResponseId());
				}

				for (EventTimerEntity eventTimerEntity : eventTimerEntityList) {
					EventTask eventTask = new EventTask();
					eventTask.setId(snowFlake.nextId());
					eventTask.setEvent_id(eventTimerEntity.getEventId());
					eventTask.setRemark(eventTimerEntity.getRemark());
					eventTask.setStatus(eventTimerEntity.getStatus());
					eventTask.setFeedback(eventTimerEntity.getFeedback());
					/**
					 * 接收单位
					 */
					eventTask.setOrg_id(eventTimerEntity.getDeptCode());
					eventTask.setOrg_name(eventTimerEntity.getDeptName());
					/**
					 * 下达单位
					 */
					eventTask.setSendOrg(eventTimerEntity.getSendOrg());
					eventTask.setSendOrgId(eventTimerEntity.getSendOrgId());
					eventTask.setInstructionType(2);
					eventTask.setDispatch_number(eventTimerEntity.getDispatchNumber());
					eventTask.setReserve_number(eventTimerEntity.getReserveNumber());
					commandReserveMapper.taskaddCommandUnit(eventTask);

					eventTimerEntity.setTasksId(eventTask.getId());
				}
				result = eventTimerMapper.insertEventTimerBatch(eventTimerEntityList);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null != result && result >= 1;
	}

	/**
	 * 插入时间轴信息
	 *
	 * @param eventTimerEntity 时间轴对象
	 * @return
	 */
	@Transactional
	public boolean insertEventTimer(EventTimerEntity eventTimerEntity) {
		Integer result = 0;
		if (null != eventTimerEntity) {
			result = eventTimerMapper.insertEventTimer(eventTimerEntity);
		}
		return null != result && result >= 1;
	}

}
