package com.mti.zhpt.service.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mti.zhpt.dao.*;
import com.mti.zhpt.model.EventTask;
import com.mti.zhpt.utils.BusinessException;
import com.mti.zhpt.utils.SnowFlake;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mti.zhpt.model.MapTask;
import com.mti.zhpt.shared.websocket.EventWebSocket;
import com.mti.zhpt.shared.websocket.WSCustomServer;
import com.mti.zhpt.utils.Constant;

import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Service("commandMapServiceImpl")
@Slf4j
public class CommandMapServiceImpl {
	@Autowired
	private EventPowerMapper eventPowerMapper;
	@Autowired
	private PersonMapper personMapper;
	@Autowired
	private EventDetailsTimerMapper eventDetailsTimerMapper;
	@Autowired
	public EventDetailsMapper eventDetailsMapper;

	@Autowired
	CommandReserveMapper commandReserveMapper;

	@Autowired
	SnowFlake snowFlake;
	
	private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Transactional
	public MapTask doMapCommand(MapTask mapTask) {
			Date now = new Date();
			mapTask.setDoTime(now);
			for (String person : mapTask.getPersons()) {
				// 参战力量改变
				Map<String, Object> policeDetail = personMapper.queryPoliceDetails(person);
				if (null != policeDetail) {
					if (policeDetail.containsKey("org_id")) {
						mapTask.setOrg_id((String) policeDetail.get("org_id"));
						//只能对巡逻的警员发送消息
						if ((int) policeDetail.get("status") == 2) {
							personMapper.updatePoliceStatus(person,1);
//							//判断力量表里是否已存在该人的组织
//							int cov=eventPowerMapper.queryOrg(mapTask.getEvent_id(),mapTask.getOrg_id());
//							if(cov==0) {
//								log.debug("组织不存在，新建组织");
//								eventPowerMapper.newEventPowerOrgPlusPerson(mapTask.getEvent_id(),mapTask.getOrg_id());
//
//							}else {
//								log.debug("组织已存在，投靠组织");
//								eventPowerMapper.updateEventPowerOrgPlusPerson(mapTask.getEvent_id(),mapTask.getOrg_id());
//							}
							String nextId = snowFlake.nextId();
							mapTask.setName((String) policeDetail.get("name"));
							mapTask.setPerson_longitude((double) policeDetail.get("longitude"));
							mapTask.setPerson_latitude((double) policeDetail.get("latitude"));
							mapTask.setCode((String) policeDetail.get("code"));
							mapTask.setDeptName((String) policeDetail.get("org"));
							mapTask.setTasksId(nextId);
							eventDetailsTimerMapper.insertTimerByMapTask(mapTask);

//							//添加指令数据
//							String policeId = (String) policeDetail.get("id");
							String eventId = mapTask.getEvent_id();
//							commandReserveMapper.deleteByEventIdAndPoliceId(eventId, policeId);

							EventTask eventTask = new EventTask();
							eventTask.setId(nextId);
							eventTask.setEvent_id(eventId);
							eventTask.setStatus(10);
							eventTask.setRemark(mapTask.getRemark());
							eventTask.setLongitude((double) policeDetail.get("longitude"));
							eventTask.setLatitude((double) policeDetail.get("latitude"));
							eventTask.setPoliceId((String)policeDetail.get("code"));
							eventTask.setPoliceOrgId((String) policeDetail.get("org_id"));
							eventTask.setSendPolice(mapTask.getSendPolice());
							eventTask.setSendPoliceId(mapTask.getSendPoliceId());
							eventTask.setSendOrg(mapTask.getSendOrg());
							eventTask.setSendOrgId(mapTask.getSendOrgId());
							eventTask.setFeedback("下达指令");
							eventTask.setOrg_id((String) policeDetail.get("org_id"));
							eventTask.setOrg_name((String) policeDetail.get("org"));
							eventTask.setInstructionType(1);
							eventTask.setPoliceName((String) policeDetail.get("name"));
							eventTask.setDispatch_number(1);//出动默认是1
							commandReserveMapper.taskaddCommand(eventTask);

							// 只给警员发socket消息
//							Map<String, Object> detailsEntity = eventDetailsMapper.queryDetails(mapTask.getEvent_id());
//							List<Map<String, Object>> infolist = new ArrayList<Map<String, Object>>();
//							Map<String, Object> reqobj = new HashMap<String, Object>();
//							reqobj.put("msgType", Constant.MSG_ORDER);
//							reqobj.put("id", Integer.toString(mapTask.getTimer_id()));
//							reqobj.put("title", detailsEntity.get("address"));
//							reqobj.put("content", mapTask.getRemark());
//							reqobj.put("orderType",detailsEntity.get("case_type"));
//							reqobj.put("createTime", formatter.format(now));
//							reqobj.put("isRead", 1);
//							infolist.add(reqobj);
//							mapTask.setLongitude((double) detailsEntity.get("longitude"));
//							mapTask.setLatitude((double) detailsEntity.get("latitude"));
//							mapTask.setMsgType(Constant.MSG_ORDER_POLICE);
//							try {
//								WSCustomServer.sendInfo(infolist, "user");
//								EventWebSocket.sendInfo(mapTask);//给大屏发消息
//							} catch (Exception e) {
//								log.error("发送指令失败");
//								e.printStackTrace();
//								throw new BusinessException(500, "发送指令失败");
//							}
						} else {
							log.error("指令下发错误，只能对巡逻的警员发送消息");
							throw new BusinessException(500, "指令下发错误，只能对巡逻的警员发送消息");
						}
						log.debug(person);
					}
				}else {
					Map<String, Object> personTable = personMapper.queryPersonTable(person);
					if (ObjectUtils.isEmpty(personTable)){
						throw new com.mti.zhpt.shared.Exception.BusinessException("未找到该警员信息");
					}
					String nextId = snowFlake.nextId();
					mapTask.setName((String) personTable.get("name_"));
					mapTask.setCode((String) personTable.get("number_"));
					mapTask.setDeptName((String) personTable.get("dept_name"));
					mapTask.setTasksId(nextId);
					eventDetailsTimerMapper.insertTimerByMapTask(mapTask);

					EventTask eventTask = new EventTask();
					eventTask.setId(nextId);
					eventTask.setEvent_id(mapTask.getEvent_id());
					eventTask.setStatus(10);
					eventTask.setRemark(mapTask.getRemark());
					eventTask.setPoliceId((String) personTable.get("number_"));
					eventTask.setPoliceName((String) personTable.get("name_"));
					eventTask.setDispatch_number(1);//出动默认是1
					eventTask.setPoliceOrgId((String) personTable.get("dept_code"));  //待写
					eventTask.setSendPolice(mapTask.getSendPolice());
					eventTask.setSendPoliceId(mapTask.getSendPoliceId());
					eventTask.setSendOrg(mapTask.getSendOrg());
					eventTask.setSendOrgId(mapTask.getSendOrgId());
					eventTask.setFeedback("下达指令");
					eventTask.setOrg_id((String) personTable.get("dept_code"));	//待写
					eventTask.setOrg_name((String) personTable.get("dept_name"));	// 待写
					eventTask.setInstructionType(1);

					commandReserveMapper.taskaddCommand(eventTask);

				}


			}

		return mapTask;
	}
	public void resumePoliceStatus() {
		personMapper.resumePoliceStatus();
	}

	public void somePoliceStatus(String policeCode, Integer status) {
		personMapper.somePoliceStatus(policeCode,status);
	}
}
