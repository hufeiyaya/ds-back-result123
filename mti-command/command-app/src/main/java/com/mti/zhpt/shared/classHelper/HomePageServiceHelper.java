package com.mti.zhpt.shared.classHelper;

import com.mti.zhpt.dao.CommandMapper;
import com.mti.zhpt.dao.EventDetailsMapper;
import com.mti.zhpt.model.EventTimerEntity;
import com.mti.zhpt.utils.Constant;
import com.mti.zhpt.utils.DateUtil;

import java.util.*;

public class HomePageServiceHelper implements Comparator{

    public static void getRequest(String name, List<Map<String, Object>> listMap, EventDetailsMapper eventDetailsMapper) {
        List<Map<String, Object>> request = eventDetailsMapper.queryRequest(name);
        for (Map<String, Object> requestMap : request) {
            Map<String, Object> reqobj = new HashMap<String, Object>();
            reqobj.put("id", requestMap.get("id"));
            reqobj.put("msgType", 4);
            reqobj.put("title", eventDetailsMapper.queryEventTitle((String) requestMap.get("eventid"))+"请示汇报");
            reqobj.put("content", requestMap.get("content"));
            reqobj.put("createTime", DateUtil.formatDate((Date) requestMap.get("create_time"),"datetime"));
            reqobj.put("isRead",requestMap.get("is_read"));
            listMap.add(reqobj);

        }
    }

    public static void getOrder(String name, List<Map<String, Object>> listMap,
                                CommandMapper commandMapper, EventDetailsMapper eventDetailsMapper) {
        /**
         * 命令状态---未反馈30、已反馈40、已作废50  、已签收60、收到70
         */
        List<Integer> statusList = new ArrayList<>();
        statusList.add(30);
        statusList.add(40);
        statusList.add(50);
        statusList.add(60);
        statusList.add(70);
        List<EventTimerEntity> entities = commandMapper.commandAll(name, statusList, null, null,null,null,null);


        for (EventTimerEntity entity : entities) {
            Map<String,Object> map =  new HashMap<>();
            Map<String, Object> detailsEntity = eventDetailsMapper.queryDetails(entity.getEventId());
            map.put("id", entity.getTimerId());
            map.put("msgType", Constant.MSG_ORDER);
            map.put("title", detailsEntity.get("address"));
            map.put("content", entity.getRemark());
            map.put("orderType",detailsEntity.get("case_type"));
            map.put("createTime", DateUtil.formatDate( entity.getCreateTime(),"datetime"));
            map.put("isRead", entity.getIsRead());
            listMap.add(map);
        }

    }

    @Override
    public int compare(Object o1, Object o2) {



        Map<String, Object> user1 = (Map<String, Object>) o1;
        Map<String, Object> user2 = (Map<String, Object>) o2;
        Date begin =  DateUtil.strToDate((String) user1.get("createTime"), DateUtil.datetimeFormat);
        Date end = DateUtil.strToDate((String) user2.get("createTime"), DateUtil.datetimeFormat);
        if (begin == null && end == null) {
            return 0;
        }
        if (begin == null) {
            return -1;
        }
        if (end == null) {
            return 1;
        }
        if (begin.equals(end)){
            return 0;
        }
        if (begin.after(end)) {
            return -1;
        } else {
            return 1;
        }
    }

}
