package com.mti.zhpt.controller;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mti.zhpt.base.BaseController;
import com.mti.zhpt.model.CpPerson;
import com.mti.zhpt.service.impl.CpPersonServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import com.mti.zhpt.shared.utils.FdfsClient;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 人员 前端控制器
 * </p>
 *
 * @author 潘文俊
 */
@Slf4j
@Api(value = "人员接口", description = "人员接口")
@RestController
@CrossOrigin
@RequestMapping("/cpPerson")
public class CpPersonController extends BaseController {

	@Autowired
	private CpPersonServiceImpl cpPersonService;
	
	@Resource
	private FdfsClient fdfsClient;

	@ApiOperation(value = "插入人员信息", notes = "插入人员信息")
	@PostMapping("/insertPerson")
	public RetResult insertPerson(@ApiParam @RequestBody CpPerson cpPerson, @RequestParam("file") MultipartFile file) {
		if (!file.isEmpty()) {
			String url = fdfsClient.uploadFile(file);
			cpPerson.setPhotourl(url);
		}
		return RetResponse.makeOKCp(cpPersonService.save(cpPerson));

	}
	
	@ApiOperation(value = "查询人员列表信息", notes = "查询人员列表信息")
	@PostMapping("/findPersonList")
	public RetResult findPersonList(@ApiParam @RequestBody CpPerson cpPerson) {
		return RetResponse.makeOKCp(cpPersonService.select(cpPerson));

	}
	
	@ApiOperation(value = "根据身份证查询人员信息", notes = "根据身份证查询人员信息")
	@GetMapping("/findPersonByNumber/{idNumber}")
	public RetResult findPersonByNumber(@PathVariable final String idNumber) {
		CpPerson cpPerson = new CpPerson();
		cpPerson.setIdNumber(idNumber);
		return RetResponse.makeOKCp(cpPersonService.selectOne(cpPerson));

	}
}
