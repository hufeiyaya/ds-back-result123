package com.mti.zhpt.controller;


import com.mti.zhpt.model.android.ReceiveAlarmEntity;
import com.mti.zhpt.service.impl.AppEventServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
* @Description: 案事件
* @Author: zhaopf@mti-sh.cn
* @Date: 2019/4/28 13:24
*/
@RestController
@RequestMapping(path = "/event")
@Api(value = "/event", tags = {"/event"}, description = "案事件（警情）")
@Slf4j
public class AppEventController {

    @Resource
    private AppEventServiceImpl eventService;

    @RequestMapping(path = "/add-event", method = RequestMethod.POST)
    @ApiOperation(value = "新建警情")
    public RetResult addEvent(@RequestBody ReceiveAlarmEntity entity) {

        Integer result = null;
        try {
            result = eventService.addEvent(entity);
            return RetResponse.makeOKCp(result);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());

        }


    }


}
