package com.mti.zhpt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mti.zhpt.base.BaseController;
import com.mti.zhpt.model.EvidenceEntry;
import com.mti.zhpt.service.impl.EvidenceEntryServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 取证 前端控制器
 * </p>
 *
 * @author 潘文俊
 */
@Slf4j
@Api(value = "取证接口", description = "取证接口")
@RestController
@CrossOrigin
@RequestMapping("/evidenceEntry")
public class EvidenceEntryController extends BaseController {

	@Autowired
	private EvidenceEntryServiceImpl evidenceEntryService;

	@ApiOperation(value = "插入一条取证信息", notes = "插入一条取证信息")
	@PostMapping("/insertEvidence")
	public RetResult insertEvidence(@ApiParam @RequestBody EvidenceEntry evidenceEntry) {
		return RetResponse.makeOKCp(evidenceEntryService.save(evidenceEntry));

	}
	
	@ApiOperation(value = "查询取证信息", notes = "查询取证信息")
	@PostMapping("/findEvidenceList")
	public RetResult findEvidenceList(@ApiParam @RequestBody EvidenceEntry evidenceEntry) {
		return RetResponse.makeOKCp(evidenceEntryService.select(evidenceEntry));

	}
}
