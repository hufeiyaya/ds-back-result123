package com.mti.zhpt.form;

import lombok.Data;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by jingzhanwu
 * @date 2018/3/14 0014
 * @change
 * @describe 多文件上传 返回实体
 **/

@Data
public class MultiFileEntry {
	public String imagesUrl;
	public String audiosUrl;
	public String videosUrl;

}
