package com.mti.zhpt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mti.zhpt.base.BaseController;
import com.mti.zhpt.base.BasePageHelper;
import com.mti.zhpt.model.SystemUser;
import com.mti.zhpt.service.impl.SystemUserServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import com.mti.zhpt.shared.utils.MD5Util;
import com.mti.zhpt.utils.BusinessException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 用户 前端控制器
 * </p>
 *
 * @author 潘文俊
 */
@Slf4j
@Api(value = "用户接口", description = "用户接口")
@RestController
@CrossOrigin
@RequestMapping("/systemUser")
public class SystemUserController extends BaseController {

	@Autowired
	private SystemUserServiceImpl systemUserService;

	@ApiOperation(value = "新增用户", notes = "新增用户")
	@PostMapping("/insertUser")
	public RetResult insertUser(@ApiParam @RequestBody SystemUser user) {
		if (user.getPassword() == null || user.getUsername() == null) {
			throw new BusinessException(BusinessException.CODE_INSERT_ERROR, "用户名或密码不可为空");
		}
		user.setPassword(MD5Util.md5(user.getPassword()));
		return RetResponse.makeOKCp(systemUserService.saveUser(user));

	}

	@ApiOperation(value = "更新用户", notes = "更新用户")
	@PostMapping("/updateUser")
	public RetResult updateUser(@ApiParam @RequestBody SystemUser user) {
		if (user.getPassword() != null) {
			user.setPassword(MD5Util.md5(user.getPassword()));
		}
		return RetResponse.makeOKCp(systemUserService.updateUser(user));
	}

	@ApiOperation(value = "用户管理--删除", notes = "用户管理--删除接口")
	@ApiImplicitParam(name = "id", value = "id", required = true, dataType = "String", paramType = "path")
	@GetMapping(value = "/delete/{id}")
	public RetResult delete(@PathVariable final String id) {

		return RetResponse.makeOKCp(systemUserService.delete(id));
	}

	@ApiOperation(value = "登录", notes = "登录")
	@PostMapping("/loginUser")
	public RetResult loginUser(@ApiParam @RequestBody SystemUser user) {
				return RetResponse.makeOKCp(systemUserService.loginUser(user));
	}

	@ApiOperation(value = "查询分页列表", notes = "查询分页列表接口")
	@PostMapping("/queryPageList")
	public RetResult queryPageList(@ApiParam @RequestBody BasePageHelper<SystemUser> pageHelper) {
		return RetResponse.makeOKCp(systemUserService.queryUserList(pageHelper));
	}

	@ApiOperation(value = "查询列表", notes = "查询列表")
	@PostMapping("/findList")
	public RetResult findList(@ApiParam @RequestBody SystemUser systemUser) {
		return RetResponse.makeOKCp(systemUserService.findUserList(systemUser));
	}

}
