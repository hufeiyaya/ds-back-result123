package com.mti.zhpt.service.impl;


import com.mti.zhpt.constant.TargetMap;
import com.mti.zhpt.dao.AppEventMapper;
import com.mti.zhpt.dao.DictionaryMapper;
import com.mti.zhpt.enums.EventStatus;
import com.mti.zhpt.model.EventEntity;
import com.mti.zhpt.model.EventTimerEntity;
import com.mti.zhpt.model.android.ReceiveAlarmEntity;
import com.mti.zhpt.utils.SnowFlake;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
* @Description: 案事件
* @Author: zhaopf@mti-sh.cn
* @Date: 2019/4/28 13:34
*/
@Service("appEventServiceImpl")
@Slf4j
public class AppEventServiceImpl {

    @Resource
    private AppEventMapper eventMapper;
    @Resource
    private DictionaryMapper dictionaryMapper;

    @Autowired
    private SnowFlake snowFlake;

    public Integer addEvent(ReceiveAlarmEntity entity) {

        Integer result = null;
        try {
            EventEntity eventEntity = new EventEntity();
            eventEntity.setCaseType(entity.getAlarmDetail());
            eventEntity.setPoliceMan(entity.getReportPersonName());
            eventEntity.setPhone(entity.getReportPersonPhoneNum());
            eventEntity.setUnit(entity.getBelongedUnit());
            eventEntity.setLevel(entity.getAlarmLevel());
            eventEntity.setAddress(entity.getAlarmAddress());
            eventEntity.setContent(entity.getAlarmContent());
            eventEntity.setStatus(Integer.valueOf(EventStatus.Receiving.getCode()));
            if (StringUtils.isNotBlank(entity.getLongitude())) {
                eventEntity.setLongitude(Double.valueOf(entity.getLongitude()));
            } else {
                eventEntity.setLongitude(Double.valueOf("102.719775"));
            }
            if (StringUtils.isNotBlank(entity.getLatitude())) {
                eventEntity.setLatitude(Double.valueOf(entity.getLatitude()));
            } else {
//                eventEntity.setLatitude(Double.valueOf("39.543367"));
                eventEntity.setLatitude(Double.valueOf("24.839721"));
            }
            eventEntity.setRecordMp3Url(entity.getAlarmRecordId());
            eventEntity.setRecordMp3Length(30);
            eventEntity.setBranchUnit(entity.getBranchUnit());
            eventEntity.setBranchUnitId(entity.getBranchUnitId());
            eventEntity.setUnitPoliceId(entity.getUnitPoliceId());
            eventEntity.setUnitPoliceName(entity.getUnitPoliceName());
            eventEntity.setUnitId(entity.getUnitId());
            eventEntity.setTitle(entity.getTitle());
            String nextId = snowFlake.nextId();
            eventEntity.setEventId(nextId);
            eventEntity.setEventType("0");
            eventEntity.setDailyEvent(1);
            eventEntity.setImportantEvent(0);

            result = eventMapper.addEvent(eventEntity);



            EventTimerEntity eventTimerEntity = new EventTimerEntity();
            eventTimerEntity.setEventId(eventEntity.getEventId());
            eventTimerEntity.setCreateTime(new Date());
            eventTimerEntity.setFeedback("新建警情");
            eventTimerEntity.setRemark(eventEntity.getContent());
            eventTimerEntity.setStatus((Integer) TargetMap.TARGETMAP.get(TargetMap.ZLD_KEY_YXD));
            eventTimerEntity.setLongitude(eventEntity.getLongitude());
            eventTimerEntity.setLatitude(eventEntity.getLatitude());
            eventTimerEntity.setPoliceName(eventEntity.getUnitPoliceName());
            eventTimerEntity.setPoliceCode(eventEntity.getUnitPoliceId());
            eventTimerEntity.setDeptCode(entity.getUnitId());
            eventTimerEntity.setDeptName(entity.getBelongedUnit());
            eventMapper.addEventTimer(eventTimerEntity);
            eventTimerEntity.setId(snowFlake.nextId());
            eventTimerEntity.setSendOrg(entity.getBelongedUnit());
            eventTimerEntity.setSendOrgId(entity.getUnitId());
            eventTimerEntity.setSendPolice(entity.getUnitPoliceName());
            eventTimerEntity.setSendPoliceId(entity.getUnitPoliceId());
            eventMapper.addEventTasks(eventTimerEntity);




        } catch (Exception e) {
            log.error("新建警情报错：",e);
        }
        return result;
    }


}
