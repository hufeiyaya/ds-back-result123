package com.mti.zhpt.service.impl;


import com.mti.zhpt.dao.CommandMapper;
import com.mti.zhpt.dao.EventDetailsMapper;
import com.mti.zhpt.dao.HomePageMapper;
import com.mti.zhpt.shared.Exception.BusinessException;
import com.mti.zhpt.shared.classHelper.HomePageServiceHelper;
import com.mti.zhpt.utils.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;


@Service("homePageServiceImpl")
@Slf4j
public class HomePageServiceImpl {

    @Resource
    private CommandMapper commandMapper;

    @Resource
    private EventDetailsMapper eventDetailsMapper;

    @Resource
    private HomePageMapper homePageMapper;

    public List<Map<String, Object>> messageAll(String name, String type, String user) {
        List<Map<String, Object>> listMap = new ArrayList<>();

        switch (type) {
            case Constant.ML:
                if (user.equals("user")){
                    HomePageServiceHelper.getOrder(name, listMap, commandMapper, eventDetailsMapper);
                }
                break;
            case Constant.QSHB:
                if (user.equals("admin")){
                    HomePageServiceHelper.getRequest(name,listMap, eventDetailsMapper);
                }
                break;
            case Constant.ALL:
                if (user.equals("admin")){
                    HomePageServiceHelper.getRequest(name, listMap, eventDetailsMapper);
                }
                if (user.equals("user")) {
                    HomePageServiceHelper.getOrder(name, listMap, commandMapper, eventDetailsMapper);
                }

                break;
            default:
                throw new BusinessException("type参数错误");
        }


        /**
         * 日期排序
         */
        HomePageServiceHelper sortClass = new HomePageServiceHelper();
        Collections.sort(listMap, sortClass);

        return listMap;
    }


    public Integer messageIsRead(Map<Object, Object> data) {
        Integer msgType = (Integer) data.get("msgType");
        /**
         * 事件指令1
         * 微群2
         * 通知3
         * 请示汇报4
         * 勤务5
         * 布控6
         */
        Integer result = null;
        switch (msgType) {
            case 1:
                result = homePageMapper.msgOrder((Integer) data.get("id"));
                break;
            case 4:
                result = homePageMapper.msgReport((Integer) data.get("id"));
                break;
            default:
                throw new BusinessException("msgType参数错误");
        }
        return result;

    }
}
