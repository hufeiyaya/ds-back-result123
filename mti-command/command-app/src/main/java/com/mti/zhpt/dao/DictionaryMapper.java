package com.mti.zhpt.dao;

import com.mti.zhpt.model.Dictionary;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;


public interface DictionaryMapper {

    /**
     * 根据type 与key 查询字典信息
     *
     * @param type,key
     * @return
     */
    @Select("select * from DS_SYSTEM_DICT where type_=#{type} and key_=#{key}")
    @ResultMap("BaseResultMap")
     Dictionary getByTypeAndKey(@Param("type") String type, @Param("key") String key);


    /**
     * 根据type 与key 查询字典信息
     *
     * @param type,key
     * @return
     */
    @Select("select * from CP_SYSTEM_DICT where type_=#{type} and DELETE_FLAG_ = 0 ")
    @ResultMap("BaseResultMap")
     List<Dictionary> getByType(@Param("type") String type);


    /**
     * 根据Map条件查询报警单
     *
     * @param condition
     * @return
     */
    @SuppressWarnings("rawtypes")//抑制单类型的警告：rawtypes
     List<Dictionary> findByCondition(Map condition);
}