package com.mti.zhpt.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mti.zhpt.base.BaseServiceImpl;
import com.mti.zhpt.dao.CpPersonMapper;
import com.mti.zhpt.model.CpPerson;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author 潘文俊
 * @since 2018-09-03
 */
@Slf4j
@Service
@Transactional
public class CpPersonServiceImpl extends BaseServiceImpl<CpPerson> {
	
	@Autowired
	private CpPersonMapper cpPersonMapper;
	
}
