package com.mti.zhpt.dao;

import com.mti.zhpt.base.BaseMapper;
import com.mti.zhpt.model.CpPerson;

/**
 * <p>
  * 人员Mapper 接口
 * </p>
 *
 * @author 潘文俊
 */
public interface CpPersonMapper extends BaseMapper<CpPerson> {
	
}