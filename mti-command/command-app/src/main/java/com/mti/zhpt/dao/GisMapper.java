package com.mti.zhpt.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface GisMapper {

    @Select("select a.* from cp_location a")
    List<Map<String,Object>> queryAddress();
}
