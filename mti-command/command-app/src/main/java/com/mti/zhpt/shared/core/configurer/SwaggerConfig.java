package com.mti.zhpt.shared.core.configurer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * Swagger 配置
 *
 */
@Configuration
@ComponentScan(basePackages = {"com.mti.zhpt.controller"}) // 配置controller路径
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.mti.zhpt"))//扫描com路径下的api文档
                .paths(PathSelectors.any())//路径判断,这里是任何路径
                .build();
    }

    private ApiInfo apiInfo()  {
        return new ApiInfoBuilder()
                .title("一体化合成指挥平台"  + "模块RESTful APIs")
                .description("")
                .termsOfServiceUrl("http://daoshuit.com/")
                .contact(new Contact("道枢技术", "http://daoshuit.com/", ""))
                .version("1.0")
                .build();
    }
}

