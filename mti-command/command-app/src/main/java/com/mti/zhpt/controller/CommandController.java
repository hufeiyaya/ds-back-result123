package com.mti.zhpt.controller;


import com.github.pagehelper.PageInfo;
import com.mti.zhpt.dto.CommandEntity;
import com.mti.zhpt.model.android.CommandSubmitEntity;
import com.mti.zhpt.service.impl.CommandServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 * 待处理警情/历史警情
 * </p>
 *
 * @author zhaichen
 * @since 2019-09-02 13:24
 */
@RestController
@RequestMapping(path = "/command")
@Api(value = "/command", tags = {"/command"}, description = "待处理警情/历史警情/首页")
@Slf4j
public class CommandController {

    @Resource
    private CommandServiceImpl commandService;

    @RequestMapping(path = "/command-all", method = RequestMethod.POST)
    @ApiOperation(value = "待处理警情/历史警情/首页---指令列表")
    public RetResult commandAll(@RequestBody CommandEntity entity) {

        PageInfo result = null;
        try {
            result = commandService.commandAll(entity.getName(), entity.getAlert(), entity.getPageNum(), entity.getPageSize(), entity.isLocation(),
                    entity.getStartTime(), entity.getEndTime(),entity.getUnitPoliceId(),entity);
            return RetResponse.makeOKCp(result);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());

        }
    }


    @RequestMapping(path = "/command-timerId", method = RequestMethod.GET)
    @ApiOperation(value = "根据timerId(命令id) 查询命令详情、事件详情、基于事件位置的相关经纬度")
    public RetResult commandTimerId(@RequestParam String id, @RequestParam Integer distance) {

        Map<Object, Object> result = null;
        try {
            result = commandService.commandTimerId(id, distance);
            return RetResponse.makeOKCp(result);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @RequestMapping(path = "/command-updateStatus", method = RequestMethod.GET)
    @ApiOperation(value = "指令详情--根据timerId(命令id) ，更改命令状态：未反馈30、已反馈40、已退单50 、已签收60，收到70")
    public RetResult updateStatus(@ApiParam(example = "401")@RequestParam Integer timerId,
                                  @ApiParam(example = "70")@RequestParam Integer status) {

        Integer result = null;
        try {
            result = commandService.updateStatus(timerId, status);
            return RetResponse.makeOKCp(result);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @RequestMapping(path = "/command-disposeSubmit", method = RequestMethod.POST)
    @ApiOperation(value = "警情详情处置提交")
    public RetResult disposeSubmit(@RequestBody CommandSubmitEntity entity) {

        try {
            commandService.disposeSubmit(entity);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

    @RequestMapping(path = "/doCommand", method = RequestMethod.POST)
    @ApiOperation(value = "警情反馈")
    public RetResult doCommand(@RequestBody CommandSubmitEntity entity) {
        try {
            commandService.doCommand(entity);
            return RetResponse.makeOKCp();
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());
        }
    }

}
