package com.mti.zhpt.shared.classHelper;

import com.mti.zhpt.dao.EventDetailsMapper;
import com.mti.zhpt.model.EventTimerEntity;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

public class CommandServiceHelper {

    public static void isLocation(boolean isLocation, List<EventTimerEntity> entities, EventDetailsMapper eventDetailsMapper) {
        /**
         * 是否显示经纬度
         */
        if (isLocation && !CollectionUtils.isEmpty(entities)) {
            /**
             * 根据命令list
             * 查询出
             * 对应的事件list
             * 匹配event_id
             * 把事件经纬度
             * 放进对应命令实体
             */
            List<Map<String, Object>> eventDetails = eventDetailsMapper.queryDetailList(entities);

            if (!CollectionUtils.isEmpty(eventDetails)) {
                for (int x = 0; x < entities.size(); x++) {
                    for (int i = 0; i < eventDetails.size(); i++) {
                        Map<String, Object> map = eventDetails.get(i);
                        EventTimerEntity eventTimerEntity = entities.get(x);
                        if (map.get("event_id").equals(eventTimerEntity.getEventId())) {
                            eventTimerEntity.setLongitude((Double) map.get("longitude"));
                            eventTimerEntity.setLatitude((Double) map.get("latitude"));
                        }
                    }
                }
            }
        }
    }
}
