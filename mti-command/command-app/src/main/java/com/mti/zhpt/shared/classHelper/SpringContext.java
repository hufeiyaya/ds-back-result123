package com.mti.zhpt.shared.classHelper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;


/**
* @Description:
* @Author: zhaopf@mti-sh.cn
* @Date: 2019/4/24 16:12
 *
 * 由于
 * @ServerEndpoint中不能用
 * @Autowired等注入方式注入其它Bean，
 * 所以需要通过ApplicationContex显示获取其它Bean，SpringContext的util类
 *
*/
@Component
@Slf4j
public class SpringContext implements ApplicationContextAware {
    private static ApplicationContext applicationContext;


    /**
    * @Description: 获取bean的管理器
    * @Author: zhaopf@mti-sh.cn
    * @Date: 2019/4/24 16:03
     *
     * @see ApplicationContextAware#setApplicationContext(ApplicationContext)
    */
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        log.info("setApplicationContext，applicationContext=" + applicationContext);
        SpringContext.applicationContext = applicationContext;
    }

    //获取applicationContext
    private static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    //通过name获取 Bean.
    public static Object getBean(String name){
        return getApplicationContext().getBean(name);
    }

    //通过class获取Bean.
    public static <T> T getBean(Class<T> clazz){
        return getApplicationContext().getBean(clazz);
    }

    //通过name,以及Clazz返回指定的Bean
    public static <T> T getBean(String name,Class<T> clazz){
        return getApplicationContext().getBean(name, clazz);
    }

}
