package com.mti.zhpt.shared.Exception;

import java.io.Serializable;

/**
 * @Description: 业务异常类
 * @Author: zpf
 * @CreateDate: 2018/4/11 15:59
 * @Version: 1.0
 */
public class BusinessException extends RuntimeException implements IException,
        Serializable {

    public static final int CODE_INCORRECT_USER = 10002;// 用户名或密码错误
	public static final int CODE_DELETE_USER = 10017;// 用户已被删除
	public static final int CODE_DISTABLE_USER = 10016;// 用户已被禁用
	public static final int CODE_VALIDATE_USER = 10018;// 用户已过期
	public static final int CODE_ALREADY_LOGIN = 10019; // 用户已经登录
	public static final int CODE_ILLEGAL_SEAT = 10021; // 坐席类型不匹配
	public static final String CODE_ILLEGAL_PARAMS = "10003";// 不合法的参数
	public static final int CODE_ILLEGAL_IMAGE_SIZE = 10004;// 不合法的图片大小
	public static final int CODE_ILLEGAL_NOTMAPPING=10013;//违法上传未找到映射关系
	public static final int CODE_INSERT_ERROR = 10020;// 添加异常
	public static final int CODE_QUERY_ERROR = 10005;// 查询异常
	public static final int CODE_UPDATE_ERROR = 10006;// 更新记录异常
	public static final int CODE_DELETE_INEXISTENCE_ERROR = 10007;// 删除数据异常，记录不存在
	public static final int CODE_DELETE_INUSE_ERROR = 10008;// 删除数据异常，记录正在使用中
	public static final int CODE_REQUEST_DATA_INEXISTENCE = 10009;// 请求数据不存在或已被删除
	public static final int CODE_XML_PARSE_ERROR = 100010;// 解析XML或JSON数据异常
	public static final int CODE_REQUEST_IMAGE_INEXISTENCE = 100011;// 请求图片不存在或已被删除
	public static final int CODE_CRITERIA_DUPLICATE = 100012;// MAP与object不能同时存在
	public static final int CODE_REDIS_ERROR = 100013;// REDIS连接异常
	public static final int CODE_CACHE_ERROR = 100014;// 缓存对象不存在
	public static final int CODE_COLLETION_INDEX_ERROR = 100015;// list 下标越界
	public static final int CODE_USER_SESSION_ERROR = 100016;// 用户session不存在
	public static final int CODE_MISSING_SYSTENCONFIG_ERROR = 100017;// 缺少系统配置
	public static final int CODE_THREADS_FINISH_ERROR = 100018;// 线程未全部执行成功
	public static final int CODE_QUERY_OVERSIZE = 100019;// 查询数量超过100w
	public static final int CODE_RECORD_EXISTS = 100020;// 修改的记录已经存在
	public static final int CODE_SYNCHRONIZATION_FAILURE = 100021;// 大数据同步失败
	public static final int CODE_IMAGE_EXCEPTION = 100022;// 图片上传异常
	public static final int CODE_QUERY_EMPTY = 100023;// 查询结果为空
	public static final int CODE_ILLEGAL_OPERATION = 100024;// 导出操作不合法
	public static final int CODE_IMAGE_COMPRESSION_FAILURE = 100025;// 图片压缩失败
	public static final int CODE_DELETE_FILE_FAILURE = 100026;// 删除目录失败
	public static final int CODE_UPLOAD_FILE_FAILURE = 100027;// 上传ftp文件失败
	public static final int CODE_FTP_NO_LOGIN = 100028;// ftp登录失败
	public static final int CODE_MQ_SEND_MESSAGE = 100029;// MQ发送信息异常
	public static final int CODE_SAME_PLATENUMBER = 100044;// 不能添加相同白名单
	public static final int CODE_USER_LOGIN = 100045;//用户已经登录
	public static final int CODE_MAIL_SEND_MESSAGE = 100046;//发送邮件异常




    protected String errCode;
    private String errMsg;
    private transient Object[] arguments;

    public BusinessException() {

    }

    public BusinessException(String msg) {
        super(msg);
        this.errMsg = msg;
    }

    public BusinessException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public BusinessException(String code, String msg) {
        super(msg);
        this.errMsg = msg;
        this.errCode = code;
    }


    public BusinessException(String code, Object... args) {
        this.errCode = code;
        this.arguments = args;
    }

    public BusinessException(String code, String msg, Object... args) {
        super(msg);
        this.errMsg = msg;
        this.errCode = code;
        this.arguments = args;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public String getErrorCode() {
        // TODO Auto-generated method stub
        return errCode;
    }

    public void setErrorArguments(Object... paramVarArgs) {
        // TODO Auto-generated method stub
        this.arguments = paramVarArgs;
    }

    public Object[] getErrorArguments() {
        // TODO Auto-generated method stub
        return this.arguments;
    }
}