package com.mti.zhpt.service.impl;


import com.alibaba.fastjson.JSON;
import com.mti.zhpt.dao.EventDetailsMapper;
import com.mti.zhpt.shared.Exception.BusinessException;
import com.mti.zhpt.utils.Constant;
import com.mti.zhpt.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("requestServiceImpl")
@Slf4j
public class RequestServiceImpl {

    @Resource
    private EventDetailsMapper eventDetailsMapper;
    @Value("${web.url}")
    private String webUrl;

    //注入 RestTemplateBuilder
    @Autowired
    private void initRestTemplate(RestTemplateBuilder builder) {
        this.restTemplate = builder.build();
    }

    private RestTemplate restTemplate;

    public List<Map<String, Object>> requestDetails(Integer id) {

        List<Map<String, Object>> request = eventDetailsMapper.requestDetails(id);
        for (Map<String, Object> map : request) {

//            String title = eventDetailsMapper.queryEventTitle((Integer) map.get("eventid")) + "请示汇报";
            map.put("title", "请示汇报");
        }

        return request;
    }

    public Integer requestSubmit(Map<Object, Object> data) {

        Integer title = null;
        try {
            Date date = new Date();
            data.put("createTime",date );
            data.put("messageType", Constant.PISHI);
            title = eventDetailsMapper.requestSubmit(data);

            Map<String, Object> map = new HashMap<>();
            map.put("name", "王忠雷局长");
            map.put("content", data.get("content"));
            map.put("messageType", Constant.PISHI);
            map.put("createTime",  DateUtil.formatDate(new Date(),"datetime"));
            map.put("eventId", data.get("eventid"));

            /**
             * 提交的内容转发给web
             */
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            String body = JSON.toJSONString(map);
            HttpEntity<String> requestEntity = new HttpEntity<String>(body, headers);
            ResponseEntity<String> response = restTemplate.
                    postForEntity(webUrl + "/app/feedback-report", requestEntity, String.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BusinessException(e.getMessage());
        }

        return title;
    }

    public void insertData() {


        try {
            List<Map<String, Object>>  data=  eventDetailsMapper.selectData();
            for (Map<String, Object> datum : data) {

                eventDetailsMapper.insertData((String) datum.get("dm"),(String) datum.get("mc"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
