package com.mti.zhpt.controller;


import com.mti.zhpt.service.impl.GisServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/gis")
public class GisController {

    @Resource
    private GisServiceImpl gisService;

    @RequestMapping(path = "/address", method = RequestMethod.GET)
    public RetResult query(@RequestParam String id){
        List<Map<Object,Object>> result = gisService.queryAddress(id);

        return  RetResponse.makeOKCp(result);
    }

}
