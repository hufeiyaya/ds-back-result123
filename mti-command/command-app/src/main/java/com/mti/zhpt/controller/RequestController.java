package com.mti.zhpt.controller;


import com.mti.zhpt.service.impl.RequestServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Description: app首页
 * @Author: zhaopf@mti-sh.cn
 * @Date: 2019/5/7 15:17
 */
@RestController
@RequestMapping(path = "/request")
@Api(value = "/request", tags = {"/request"}, description = "请示汇报")
@Slf4j
public class RequestController {

    @Resource
    private RequestServiceImpl requestDetails;

    @RequestMapping(path = "/request-details", method = RequestMethod.GET)
    @ApiOperation(value = "请示汇报-详情")
    public RetResult requestDetails(@RequestParam Integer id ) {

        List<Map<String,Object>> result = null;
        try {
            result = requestDetails.requestDetails(id);
            return RetResponse.makeOKCp(result);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());

        }
    }


    @RequestMapping(path = "/request-submit", method = RequestMethod.POST)
    @ApiOperation(value = "批示-提交",notes = "json格式：{\"eventid\":1,\"content\":\"测试\"}")
    public RetResult requestSubmit(@RequestBody Map<Object,Object> data ) {

      Integer result = null;
        try {
            result = requestDetails.requestSubmit(data);
            return RetResponse.makeOKCp(result);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());

        }
    }


//    @RequestMapping(path = "/insertData", method = RequestMethod.GET)
//    @ApiOperation(value = "修改数据字典数据",notes = "json格式：{\"eventid\":1,\"content\":\"测试\"}")
//    public RetResult insertData( ) {
//
//        try {
//             requestDetails.insertData();
//            return RetResponse.makeOKCp();
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            return RetResponse.makeErrCp(e.getMessage());
//
//        }
//    }


}
