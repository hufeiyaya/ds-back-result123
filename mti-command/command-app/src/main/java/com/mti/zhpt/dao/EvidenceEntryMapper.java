package com.mti.zhpt.dao;

import com.mti.zhpt.base.BaseMapper;
import com.mti.zhpt.model.EvidenceEntry;

/**
 * <p>
  * 取证Mapper 接口
 * </p>
 *
 * @author 潘文俊
 */
public interface EvidenceEntryMapper extends BaseMapper<EvidenceEntry> {
	
}