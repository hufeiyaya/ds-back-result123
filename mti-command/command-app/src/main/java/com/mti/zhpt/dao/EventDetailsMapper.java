package com.mti.zhpt.dao;

import com.mti.zhpt.model.EventTimerEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface EventDetailsMapper {
    Map<String, Object> queryDetails(@Param("eventId") String eventId);

    @Select("select a.*,ST_DistanceSphere(a.geom,b.geom) as dis " +
            "from cp_org a,cp_event b " +
            "where b.event_id=#{eventId} " +
            "and a.sort='pcs' " +
            "and ST_DistanceSphere(a.geom,b.geom)<#{distance} " +
            "order by ST_DistanceSphere(a.geom,b.geom)")
    List<Map<String, Object>> queryNearByJurisdictions(String eventId, Integer distance);

    @Select("select a.*,ST_DistanceSphere(a.geom,b.geom) as dis " +
            "from cp_moniter a,cp_event b" +
            " where b.event_id=#{eventId} " +
            "and ST_DistanceSphere(a.geom,b.geom)<#{distance} " +
            "order by ST_DistanceSphere(a.geom,b.geom)")
    List<Map<String, Object>> queryNearByMoniters(String eventId, Integer distance);

    List<Map<String, Object>> queryDetailList(@Param("entities") List<EventTimerEntity> entities);

    List<Map<String, Object>> queryDetailsLists(@Param("entities") List<EventTimerEntity> entities);

    @Select("select content from cp_event where event_id=#{eventId}")
    String queryEventTitle(@Param("eventId") String eventId);

    @Select("select * from cp_request where  concat(content) like concat(concat('%',#{name}),'%') ")
    List<Map<String, Object>> queryRequest(@Param("name") String name);

    @Select("select * from cp_request where id=#{id}")
    List<Map<String, Object>> requestDetails(Integer id);

    @Insert({ "insert into cp_response( eventid, leaderid, content, status,createtime) values( #{eventid}, 1, #{content},0,#{createTime})" })
    Integer requestSubmit(Map<Object, Object> data);

    @Insert("INSERT INTO cp_system_dict( \"key_\", \"value_\", \"type_\", \"rate_\", \"delete_flag_\", \"create_time_\") VALUES ( #{key}, #{value}, 'ALARM_TYPE_CODE', 0, 0, now());")
    void insertData(@Param("key") String key, @Param("value") String value);

    @Select("SELECT dm,mc FROM t_dm_ay  order by dm asc  ")
    List<Map<String, Object>> selectData();
}
