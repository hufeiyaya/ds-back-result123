package com.mti.zhpt.dao;

import java.util.List;

import com.mti.zhpt.base.BaseMapper;
import com.mti.zhpt.base.BasePageHelper;
import com.mti.zhpt.model.SystemUser;

/**
 * <p>
  * 用户 Mapper 接口
 * </p>
 *
 * @author 潘文俊
 * @since 2018-11-13
 */
public interface SystemUserMapper extends BaseMapper<SystemUser> {
	
	/**
     * @methodDesc: 功能描述: 用户分页
     * @author 潘文俊
     * @createTime 2018年11月14日
     * @version v1.0.0
     * @param pageHelper
     */
    public List<SystemUser> pageUserList(BasePageHelper<SystemUser> pageHelper);

}