package com.mti.zhpt.dao;

import org.apache.ibatis.annotations.Update;

public interface HomePageMapper {

    @Update("UPDATE cp_event_timer SET is_read=2 WHERE timer_id=#{id}")
    Integer msgOrder(Integer id);

    @Update("UPDATE cp_request SET is_read=2 WHERE id=#{id}")
    Integer msgReport(Integer id);
}
