package com.mti.zhpt;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import tk.mybatis.spring.annotation.MapperScan;

@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = "com.mti.zhpt")
@MapperScan(basePackages="com.mti.zhpt.dao")
public class CommandAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommandAppApplication.class, args);
    }
    


}
