package com.mti.zhpt.service.impl;


import com.mti.zhpt.dao.GisMapper;
import org.postgresql.geometric.PGpoint;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: MySpringBoot
 * @description: 省市区服务
 * @author: zpf
 * @create: 2019-01-13 13:06
 **/
@Service("gisServiceImpl")
public class GisServiceImpl {

    @Resource
    private GisMapper gisMapper;

    public List<Map<Object,Object>> queryAddress(String id) {
        //开启分页查询，写在查询语句上方
        //只有紧跟在PageHelper.startPage方法后的第一个Mybatis的查询（Select）方法会被分页。
//        PageHelper.startPage(provinceEntity.getPageNum(), provinceEntity.getPageSize());
        List<Map<String,Object>> list = gisMapper.queryAddress();
        List<Map<Object,Object>> result = new ArrayList<>();
        for (Map<String, Object> m : list) {
            Map<Object, Object> map = new HashMap<>();
            for(String key: m.keySet()){
                if (key.equals("coordinates")){
                    List<Object> lis = new ArrayList<>();
                    PGpoint coordinates = (PGpoint) m.get("coordinates");
                    lis.add( coordinates.x);
                    lis.add( coordinates.y);
                    map.put("coordinates", lis);
                }
                if (key.equals("properties")) {
                    map.put("properties", m.get("properties"));
                }
            }
            result.add(map);
        }



        return result;
    }


}
