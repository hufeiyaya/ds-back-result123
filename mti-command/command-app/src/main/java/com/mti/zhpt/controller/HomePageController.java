package com.mti.zhpt.controller;


import com.mti.zhpt.service.impl.HomePageServiceImpl;
import com.mti.zhpt.shared.core.ret.RetResponse;
import com.mti.zhpt.shared.core.ret.RetResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Description: app首页
 * @Author: zhaopf@mti-sh.cn
 * @Date: 2019/5/7 15:17
 */
@RestController
@RequestMapping(path = "/homePage")
@Api(value = "/homePage", tags = {"/homePage"}, description = "首页")
@Slf4j
public class HomePageController {

    @Resource
    private HomePageServiceImpl homePageService;

    @RequestMapping(path = "/message-all", method = RequestMethod.GET)
    @ApiOperation(value = "首页---命令、请示、勤务、布控")
    public RetResult messageAll(@ApiParam(required = false,example = "分局") @RequestParam String name,
                                @ApiParam(example = "qshb") @RequestParam String type,
                                @ApiParam(example = "admin")@RequestParam String user) {

        List<Map<String,Object>> result = null;
        try {
            result = homePageService.messageAll(name, type,user);
            return RetResponse.makeOKCp(result);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());

        }
    }

    @RequestMapping(path = "/message-isRead", method = RequestMethod.POST)
    @ApiOperation(value = "首页---消息是否已读",
            notes = "msgType:事件指令1、微群2、通知3、请示汇报4、勤务5、布控6" +
                    "{\"msgType\": 4,\"id\": 101}")
    public RetResult messageIsRead(@RequestBody Map<Object,Object> data) {

      Integer result = null;
        try {
            result = homePageService.messageIsRead(data);
            return RetResponse.makeOKCp(result);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RetResponse.makeErrCp(e.getMessage());

        }
    }





}
