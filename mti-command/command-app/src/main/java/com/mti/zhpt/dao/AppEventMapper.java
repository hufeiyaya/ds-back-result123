package com.mti.zhpt.dao;

import com.mti.zhpt.model.EventEntity;
import com.mti.zhpt.model.EventTask;
import com.mti.zhpt.model.EventTimerEntity;
import org.apache.ibatis.annotations.Insert;

public interface AppEventMapper {

    /**
     * 添加案事件
     *
     * @param entity
     * @return
     */
    @Insert("insert into cp_event (event_id,police_man,phone,case_type,unit,level,address,content,status,create_time,longitude,latitude,record_mp3_url,record_mp3_length," +
            " branch_unit_id, branch_unit, unit_police_id, unit_police_name, unit_id, title_,event_type,daily_event,important_event) " +
            "values(#{eventId},#{policeMan},#{phone},#{caseType},#{unit}," +
            "#{level},#{address},#{content},#{status},now(),#{longitude},#{latitude},#{recordMp3Url},#{recordMp3Length}, #{branchUnitId}, #{branchUnit}, #{unitPoliceId}, " +
            "#{unitPoliceName}, #{unitId}, #{title}, #{eventType}, #{dailyEvent}, #{importantEvent})")
    Integer addEvent(EventEntity entity);


    @Insert("insert into cp_event_timer(event_id,create_time,feedback,remark,status,longitude,latitude,police_name,police_code,dept_code,dept_name)" +
            "values(#{eventId},#{createTime},#{feedback},#{remark},#{status},#{longitude},#{latitude},#{policeName},#{policeCode},#{deptCode},#{deptName})")
    void addEventTimer(EventTimerEntity eventTimerEntity);

    @Insert("insert into cp_event_tasks(event_id,id,remark,org_id,status,longitude,latitude,police_id,create_time,org_name,police_name," +
            "send_org_id,send_org,send_police_id,send_police,feedback)" +
            "values(#{eventId},#{id},#{remark},#{deptCode},#{status},#{longitude},#{latitude},#{policeCode},#{createTime},#{deptName},#{policeName}," +
            "#{sendOrgId},#{sendOrg},#{sendPoliceId},#{sendPolice},#{feedback})")
    void addEventTasks(EventTimerEntity eventTimerEntity);
}
