package com.mti.zhpt.dao;

import com.mti.zhpt.model.CpPerson;
import com.mti.zhpt.model.EventTimerEntity;
import com.mti.zhpt.model.android.CommandSubmitEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.sql.Timestamp;
import java.util.List;

public interface CommandMapper {
    List<EventTimerEntity> commandAll(@Param("name") String name,
                                      @Param("statusList") List<Integer> statusList,
                                      @Param("startTime") Timestamp startTime,
                                      @Param("endTime") Timestamp endTime,
                                      @Param("unitPoliceId") String unitPoliceId,
                                      @Param("unitId") String unitId,
                                      @Param("caseTypeList") List<String> caseTypeList);


    //  @Select("select * from cp_event_timer where timer_id= #{timerId}")
    EventTimerEntity command(@Param("id") String id);


    Integer updateStatus(@Param("timerId") Integer timerId, @Param("status") Integer status);

    Integer InsertImagesUrl(CommandSubmitEntity entity);

    Integer InsertVideosUrl(CommandSubmitEntity entity);

    Integer InsertAudiosUrl(CommandSubmitEntity entity);

    Integer InsertRelevantUeople(CpPerson entity);

    Integer InsertPersonnel(@Param("eventId") String eventId, @Param("numId") String numId, @Param("caseType") String caseType);

    void insertEventTimer(CommandSubmitEntity entity);

    @Select("select event_id from cp_event_timer where timer_id=#{timerId}")
    String queryEventid(Integer timerId);

    @Select("select id_number from cp_person where id_number=#{idNumber}")
    String selectCpPerson(String idNumber);

    @Select("select event_id from cp_event_personnel where event_id=#{eventId} and person_id=#{id}")
    String selectPersonnel(@Param("eventId") String eventId, @Param("id") String id);

    @Update("update cp_police set status = 2 where  code = #{policeCode}")
    void changePoliceStatus(@Param("policeCode") String policeCode);

    @Update("update cp_event set status = 1 where  event_id = #{eventId}")
    void changeEventStatus(@Param("eventId") String eventId);

    @Update("update cp_event_tasks set status = #{status} where  id = #{id}")
    void changeTasksStatus(CommandSubmitEntity entity);

    @Insert("insert into  cp_event_response (id,event_id,task_id,content,org_id,org_name,police_id,police_name,create_time,timer_id) " +
            "values(#{responseId},#{eventId},#{id},#{remark},#{deptCode},#{deptName},#{policeCode},#{policeName},#{createTime},#{timerId}) ")
    void insertEventResponse(CommandSubmitEntity entity);


}
