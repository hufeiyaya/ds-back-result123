package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@ApiModel(value="EventQueryPram对象",description="案件高级查询对象")
public class EventQueryPram {

	@ApiModelProperty(value = "查询的案件类型列表", name = "caseTypeList", example = "[\"010500\", \"010304\"]", required = false, hidden = false)
	private List<String> caseTypeList;

	@ApiModelProperty(value = "查询开始时间", name = "startTime", example = "2019-05-01 12:33:34", required = false, hidden = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date startTime;

	@ApiModelProperty(value = "查询结束时间", name = "endTime", example = "2019-05-21 12:33:34", required = false, hidden = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date endTime;

}
