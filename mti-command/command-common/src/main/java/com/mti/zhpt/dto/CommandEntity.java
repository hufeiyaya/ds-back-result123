package com.mti.zhpt.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;


@Getter
@Setter
@ApiModel(value = "我的警单or警情列表入参")
public class CommandEntity implements Serializable {

    @ApiModelProperty(value = "关键字", example = "分局",required=false)
    private String name;
    @ApiModelProperty(value = "我的警单or警情列表", example = "dcljq",required=true)
    private String alert;

    @ApiModelProperty(value = "所少页", example = "1",required=true)
    private Integer pageNum;

    @ApiModelProperty(value = "多少行", example = "20",required=true)
    private Integer pageSize;

    @ApiModelProperty(value = "是否显示经纬度", example = "false",required=false)
    private boolean isLocation;

    @ApiModelProperty(value = "起始时间", example = "2019-03-05 00:00:00",required=false)
    private String startTime;

    @ApiModelProperty(value = "结束时间", example = "2019-10-12 24:00:00",required=false)
    private String endTime;

    @ApiModelProperty(value = "警察code", example = "075056",required=false)
    private String unitPoliceId;
    @ApiModelProperty(value = "警察所属单位", example = "075056",required=false)
    private String unitId;

    @ApiModelProperty(value = "指令单状态：10已下达、20已到达、30已接收、40已出动、50已到场、60已反馈、70已作废", example = "30",required=false)
    private List<Integer> statusList;

    @ApiModelProperty(value = "案件类型集合",example = "[\"010101\", \"010102\"]",required = false)
    private List<String> caseTypeList;




}
