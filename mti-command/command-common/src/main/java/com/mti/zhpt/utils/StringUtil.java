/**
 * @createTime: 2018年3月19日
 * @copyright: 上海道枢信息技术有限公司
 */
package com.mti.zhpt.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @classDesc: 功能描述: TODO
 * @author Binrui Dong
 * @createTime 2018年3月19日 下午6:55:16
 * @version v1.0.0
 * @copyright: 上海道枢信息技术有限公司
 */
public class StringUtil {

	public static String listToString(List<String> list) {
		if (list == null) {
			return null;
		}
		StringBuilder result = new StringBuilder();
		boolean first = true;
		// 第一个前面不拼接","
		for (String string : list) {
			if (first) {
				first = false;
			} else {
				result.append(",");
			}
			result.append(string);
		}
		return result.toString();
	}

	/**
	 * Arrays.asLisvt() 返回java.util.Arrays$ArrayList，
	 * 而不是ArrayList。Arrays$ArrayList和ArrayList都是继承AbstractList，
	 * remove，add等method在AbstractList中是默认throw
	 * UnsupportedOperationException而且不作任何操作。 ArrayList
	 * override这些method来对list进行操作，但是Arrays$ArrayList没有override
	 * remove(int)，add(int)等， 所以throw UnsupportedOperationException。
	 * 
	 * @methodDesc: 功能描述: TODO
	 * @author Binrui Dong
	 * @createTime 2018年3月19日 下午7:30:28
	 * @version v1.0.0
	 * @param strs
	 * @return
	 *
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<String> stringToList(String strs) {
		String s = strs;
		if (strs.contains("["))
			s = strs.replaceFirst("\\[", "");
		if (strs.contains("]"))
			s = s.substring(0, s.length() - 1);
		String str[] = s.split(",");
		return new ArrayList(Arrays.asList(str));
	}

	/**
	 * 驼峰转换为下划线，默认最后添加下划线
	 * 
	 * @param camelCaseName
	 * @return
	 */
	public static String underscoreName(String camelCaseName) {
		StringBuilder result = new StringBuilder();
		if (camelCaseName != null && camelCaseName.length() > 0) {
			result.append(camelCaseName.substring(0, 1).toLowerCase());
			for (int i = 1; i < camelCaseName.length(); i++) {
				char ch = camelCaseName.charAt(i);
				if (Character.isUpperCase(ch)) {
					result.append("_");
					result.append(Character.toLowerCase(ch));
				} else {
					result.append(ch);
				}
			}
		}
		return String.valueOf(result).toUpperCase();
	}
	

	/**
	 * 转换为驼峰
	 * 
	 * @param underscoreName
	 * @return
	 */
	public static String camelCaseName(String underscoreName) {
		StringBuilder result = new StringBuilder();
		underscoreName = underscoreName.toLowerCase();
		if (underscoreName != null && underscoreName.length() > 0) {
			boolean flag = false;
			for (int i = 0; i < underscoreName.length(); i++) {
				char ch = underscoreName.charAt(i);
				if ("_".charAt(0) == ch) {
					flag = true;
				} else {
					if (flag) {
						result.append(Character.toUpperCase(ch));
						flag = false;
					} else {
						result.append(ch);
					}
				}
			}
		}
		return result.toString();
	}
	
	public static void main(String[] args) {
		String[] strs = {
				"ID_",
				"IDENTIFICATION_",
				"VALUE_TYPE_",
				"IS_REQUIRED_",
				"COLUMN_NUMBER_",
				"HEIGHT_",
				"FIELD_NAME_",
				"FIELD_TYPE_",
				"FIELD_LENGTH_",
				"IS_EDITABLE_",
				"UPDATE_TIME_",
				"CREATE_TIME_",
				"IS_AVAILABLE_"
		};
		
		for (String string : strs) {
			System.out.println(camelCaseName(string));
		}
		
	}

}
