package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommandCustomEntity extends BaseEntity {
    private Integer id;
    private String deptName;
    private String deptCode;
    private String parentCode;
    private String level;
    private String sort;
    private Date createTime;
    private Date updateTime;
    private String createBy;
    private String updateBy;
    private String isActive;
    private String node;
    private String mobilePhone;
    private String peopleAlways;





}
