package com.mti.zhpt.model;

import com.mti.zhpt.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Table;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangmingxin
 * @since 2019-08-12
 */
@Data
@Table(name = "t_file")
public class FileEntity extends BaseEntity {


    private static final long serialVersionUID = -8983156775792376590L;
    /**
     * 附件名称
     */
    @ApiModelProperty(value = "附件名称", dataType = "String", example = "关于xxx的通知")
    private String fileName;

    /**
     * 附件地址
     */
    @ApiModelProperty(value = "附件地址", dataType = "String", example = "group1/M00/00/0F/Cqgf4FvaikqAYjtcAAcwW4CHmyc378.jpg")
    private String fileUrl;

    /**
     * 附件类型 1 通知 2 任务  3 反馈 4 处置依据
     */
    @ApiModelProperty(value = "附件类型", dataType = "String", notes = "1 通知 2 任务 3 反馈 4 处置依据", example = "1", hidden = true)
    private String bussType;

    /**
     * 附件业务id
     */
    @ApiModelProperty(value = "附件业务id ", dataType = "String", example = "1155759906258595842", hidden = true)
    private String bussId;

}
