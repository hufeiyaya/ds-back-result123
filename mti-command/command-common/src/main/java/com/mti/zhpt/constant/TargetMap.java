package com.mti.zhpt.constant;

import java.util.HashMap;
import java.util.Map;

public class TargetMap {


    public static Map<String, Object> TARGETMAP = new HashMap<String, Object>();

    /**
     * 指令单状态
     */
    public static final String ZLD_KEY_YXD = "已下达";
    public static final String ZLD_KEY_YDD = "已到达";
    public static final String ZLD_KEY_YJS = "已接收";
    public static final String ZLD_KEY_YCD = "已出动";
    public static final String ZLD_KEY_YDC = "已到场";
    public static final String ZLD_KEY_YFK = "已反馈";
    public static final String ZLD_KEY_YZF = "已作废";

    public static final Integer ZLD_YXD = 10;
    public static final Integer ZLD_YDD = 20;
    public static final Integer ZLD_YJS = 30;
    public static final Integer ZLD_YCD = 40;
    public static final Integer ZLD_YDC = 50;
    public static final Integer ZLD_YFK = 60;
    public static final Integer ZLD_YZF = 70;


    public static final String PCS = "pcs";
    public static final String JXJ = "jxj";
    public static final String ZSJZ = "zsjz";
    public static final String XF = "xf";
    public static final String LDDW = "lddw";

    public static final String NAME_PCS = "派出所";
    public static final String NAME_JXJ = "交巡警";
    public static final String NAME_ZSJZ = "直属警种";
    public static final String NAME_XF = "消防";
    public static final String NAME_LDDW = "联动单位";


    static {

        TargetMap.TARGETMAP.put(PCS, NAME_PCS);
        TargetMap.TARGETMAP.put(JXJ, NAME_JXJ);
        TargetMap.TARGETMAP.put(ZSJZ, NAME_ZSJZ);
        TargetMap.TARGETMAP.put(XF, NAME_XF);
        TargetMap.TARGETMAP.put(LDDW, NAME_LDDW);
        TargetMap.TARGETMAP.put(ZLD_KEY_YXD, ZLD_YXD);
        TargetMap.TARGETMAP.put(ZLD_KEY_YDD, ZLD_YDD);
        TargetMap.TARGETMAP.put(ZLD_KEY_YJS, ZLD_YJS);
        TargetMap.TARGETMAP.put(ZLD_KEY_YCD, ZLD_YCD);
        TargetMap.TARGETMAP.put(ZLD_KEY_YDC, ZLD_YDC);
        TargetMap.TARGETMAP.put(ZLD_KEY_YFK, ZLD_YFK);
        TargetMap.TARGETMAP.put(ZLD_KEY_YZF, ZLD_YZF);


    }

}
