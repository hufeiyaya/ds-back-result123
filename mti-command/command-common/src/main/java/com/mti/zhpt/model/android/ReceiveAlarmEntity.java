package com.mti.zhpt.model.android;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReceiveAlarmEntity extends AlarmEntity{
    /*接警单id*/
    private String id;
    /*事件单id*/
    private String incidentId;
    /*重复关联id*/
    private String repeatId;
    /*统一代码*/
    private String unitCode;
    /*报警id*/
    private String alarmDeskId;
    /*报警ip*/
    private String alarmDeskIP;

    /*本地接警人id*/
    private String locateOfficerId;
    /*本地接警人姓名*/
    private String locateOfficerName;
    /*本地报警时间*/
    private String reportTime;
    /*本地报警录音id*/
    private String alarmLocateRecordId;
    /*报警人电话*/
    private String reportPhoneNum;
    /*报警人*/
    private String reportPhoneName;
    /*报警人地址*/
    private String reportPhoneAddress;
    /*响应时间*/
    private String responseTime;
    /*等待时长*/
    private int waitingTime;
    /*报警时长*/
    private int talkTime;
    private String foreignAlarmFlag;
    /*所属单位*/
    private String belongedUnit;
    /*水源*/
    private String waterSource;
    private String abuttedCondition;
    /*作案工具*/
    private String crimeTool;
    /*灯柱*/
    private String lampPosts;
    /*建筑类别*/
    private String buildingCategory;
    /*火场*/
    private String firedSite;
    /*建筑状态描述*/
    private String buildingConditionDsc;
    /*建筑结构*/
    private String buildingStructure;
    /*起火楼层*/
    private String floor;
    /*起火物质*/
    private String firedObject;
    /*起火位置*/
    private String firePosition;
    /*燃烧区域*/
    private String burningArea;
    /*危险物质*/
    private String hazardousSubstance;
    /*危险车辆*/
    private String hazardousCar;
    /*轰炸泄漏*/
    private String bombOrLeakage;
    /*特殊服务人群*/
    private String specialServiceNum;
    /*警情补充id*/
    private String alarmRelationId;
    /*手动定位经纬度*/
    private String manualLongitude;
    private String manualLatitude;
    /*附加内容*/
    private String additionContent;

    /**
     * 分局单位id
     */
    private String branchUnitId;

    /**
     * 分局单位
     */
    private String branchUnit;

    /**
     * 管辖单位警员id
     */
    private String unitPoliceId;

    /**
     * 管辖单位警员姓名
     */
    private String unitPoliceName;

    /**
     * 所属组织id
     */
    private String unitId;

    /**
     * 案件标题
     */
    private String title;



}
