package com.mti.zhpt.utils;

/**
 * @Description: 常量
 * @Author: zpf
 * @CreateDate: 2018/4/17 22:21
 * @Version: 1.0
 */
public class Constant {

    public static final String POLICE = "100000,200000,300000,400000,450000,500000,600000,700000,800000,900000,950000,990000";
//    public static final String POLICE = "010000,020000,050000,060000,070000,080000,090000,100000,110000,120000";
    public static final String FIRE = "040000,120000";
    public static final String TRAFFIC = "030000,120000";


    /**
     * 警情类型
     */
    public static final String DCL_JQ = "dcljq";
    public static final String LS_JQ = "lsjq";
    public static final String ALL = "all";

    /**
     * 首页类型
     */
    public static final String ML = "ml";
    public static final String QSHB = "qshb";
    /**
     * 大屏消息指令
     * 类型
     */
    public static final int MSG_EVENT=9;
    public static final int MSG_ORDER_POLICE = 11;
    public static final int MSG_ORDER_ORG = 12;

    /**
     * app消息指令
     * 类型
     */

    /**
     * 事件指令
     */
    public static final int MSG_ORDER = 1;
    /**
     * 微群
     */
    public static final int MSG_MICRO_GROUP = 2;
    /**
     * 通知
     */
    public static final int MSG_NOTICE = 3;
    /**
     * 请示汇报
     */
    public static final int MSG_REPORT = 4;
    /**
     * 勤务
     */
    public static final int MSG_DUTY = 5;
    /**
     * 布控
     */
    public static final int MSG_CONTROL = 6;


    /**
     * 消息类型
     */
    public static final String FANKUI = "fankui";
    public static final String PISHI = "pishi";
}
