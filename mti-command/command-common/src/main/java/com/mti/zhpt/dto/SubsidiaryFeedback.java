package com.mti.zhpt.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * <p>
 * 辅助反馈对象
 * </p>
 *
 * @author zpf
 * @since 2019年11月6日16:15:1
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "辅助反馈", description = "辅助反馈")
public class SubsidiaryFeedback {

    /**
     * id
     */
    @ApiModelProperty(value = "id", dataType = "String",required = false)
    private String id;

    /**
     * 警情id
     */
    @ApiModelProperty(value = "警情id", dataType = "String",example = "388682076905275392")
    private String eventId;

    /**
     * 指令id
     */
    @ApiModelProperty(value = "指令id", dataType = "String",example = "388769815432527872")
    private String taskId;


    /**
     * 机构id
     */
    @ApiModelProperty(value = "机构id", dataType = "String",example = "131023470000")
    private String feedbackOrgId;

    /**
     * 机构名称
     */
    @ApiModelProperty(value = "机构名称", dataType = "String",example = "河北省永清县公安局曹家务派出所")
    private String feedbackOrgName;

    /**
     * 反馈人id
     */
    @ApiModelProperty(value = "反馈人id", dataType = "String",example = "362897")
    private String feedbackPoliceId;

    /**
     * 反馈人姓名
     */
    @ApiModelProperty(value = "反馈人姓名", dataType = "String",example = "型士雨")
    private String feedbackPoliceName;

    /**
     * 反馈电话
     */
    @ApiModelProperty(value = "反馈电话", dataType = "String",example = "15734569876")
    private String feedbackPhone;

    /**
     * 反馈内容
     */
    @ApiModelProperty(value = "反馈内容", dataType = "String",example = "ff网吧打架事件已经处理好了5")
    private String feedbackContent;

    /**
     * 反馈附件
     */
    @ApiModelProperty(value = "反馈附件", dataType = "String",example = "121212121212")
    private String feedbackFile;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", dataType = "Date", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;


    /**
     * 反馈标题
     */
    @ApiModelProperty(value = "反馈标题", dataType = "String",example = "辅助反馈")
    private String feedback;
}
