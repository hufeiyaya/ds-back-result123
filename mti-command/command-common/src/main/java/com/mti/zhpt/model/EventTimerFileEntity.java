package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class EventTimerFileEntity extends BaseEntity {
    private Integer id;
    private Integer timerId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    private String photoAddress;
    private String recordMp3Length;
    private String record;

    /**
     * 文件名称
     */
    private String name;



}
