package com.mti.zhpt.base;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.mti.zhpt.utils.Validator;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @classDesc: 功能描述: 分页查询
 * @author 徐宝振
 * @createTime: 2018年8月3日 下午2:15:40
 * @version v1.0.0
 * @copyright: 上海道枢信息技术有限公司
 */
@Data
public class BasePageHelper<T> implements Serializable{
	

	private static final long serialVersionUID = -7068361700500939655L;

	/** 排序字段 */
	@ApiModelProperty(value = "排序字段")
	private String sort;

	/** 排序方向 */
	@ApiModelProperty(value = "排序方向 ")
	private String order;  
	 
	/** 页码 */
	@ApiModelProperty(value = "页码")
	private int pageNum;
	
	/** 每页条数 */
	@ApiModelProperty(value = "每页条数")
	private int pageSize;
	
	@ApiModelProperty(value = "参数实体")
	private T obj;
	
	
	@ApiModelProperty(value = "开始时间")
	private Date startTime;
	
	
	@ApiModelProperty(value = "开始时间")
	private Date endTime;
	
	
	/** 构建模式 */
	public  BasePageHelper<T> setObj(T obj){
		this.obj=obj;
		return this;	
	}
	
	/** 构建模式 */
	public  BasePageHelper<T> setPageSize(int pageSize){
		this.pageSize=pageSize;
		return this;	
	}
	

	/** 构建模式 */
	public  BasePageHelper<T> setPageNum(int pageNum){
		this.pageNum=pageNum;
		return this;	
	}
	
	/** 构建模式 */
	public  BasePageHelper<T> setSort(String sort){
		this.sort=sort;
		return this;	
	}
	

	/** 构建模式 */
	public  BasePageHelper<T> setStartTime(Date startTime){
		this.startTime=startTime;
		return this;	
	}
	

	/** 构建模式 */
	public  BasePageHelper<T> setEndTime(Date endTime){
		this.endTime=endTime;
		return this;	
	}
	

	/** 构建模式 */
	public  BasePageHelper<T> setOrder(String order){
		this.order=order;
		return this;	
	}
 
 
	
	@Deprecated
	public String getOrderRule(){
	    StringBuffer orderRule = new StringBuffer();
	    if(StringUtils.isNotBlank(sort)){
	        orderRule.append(sort).append(" ");
	    }else{
	        orderRule.append("CREATE_TIME ");
	    }
	    orderRule.append(Validator.isNotNullOrEmpty(order) ? order : "desc");
        return orderRule.toString();
	}
}
