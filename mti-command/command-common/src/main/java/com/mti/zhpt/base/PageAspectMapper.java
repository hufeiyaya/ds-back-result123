package com.mti.zhpt.base;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.github.pagehelper.PageHelper;
import com.mti.zhpt.utils.StringUtil;
import com.mti.zhpt.utils.Validator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Aspect
public class PageAspectMapper {

    @Around("execution(* com.mti.zhpt.dao.*.page*(..))")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {

        Signature signature = joinPoint.getSignature();
        if (null != signature) {
            // 分装mybatis的查询bean对应id
            Method method = ((MethodSignature) signature).getMethod();
            // 根据分页参数和分页返回对象自动封装成pageHelper对象
            if (method.getReturnType() == java.util.List.class) {
                for (Object object : joinPoint.getArgs()) {
                    if (object instanceof BasePageHelper) {
                        BasePageHelper<?> basePageVo = (BasePageHelper<?>) object;

                        // 页码和页面大小验证
                        if (basePageVo.getPageNum() > 0 && basePageVo.getPageSize() > 0) {
                            PageHelper.startPage(basePageVo.getPageNum(), basePageVo.getPageSize());
                        } else {
                            PageHelper.startPage(1, 10);
                        }

                        // 排序方式
                        if (Validator.isNotNullOrEmpty(basePageVo.getSort())) {

                            // 排序字段处理 驼峰转为数据库对应字段
                            PageHelper.orderBy(StringUtil.underscoreName(basePageVo.getSort()) + " " + (Validator.isNotNullOrEmpty(basePageVo.getOrder()) ? basePageVo.getOrder() : "desc"));
                        }else{
                            PageHelper.orderBy(StringUtil.underscoreName("create_time") + " " + (Validator.isNotNullOrEmpty(basePageVo.getOrder()) ? basePageVo.getOrder() : "desc"));
                        }
                    }
                }
            }
        }
        try {
            Object result = joinPoint.proceed();
            return result;
        } catch (Exception e) {
            log.error("PageAspect异常" + e, e);
            throw e;
        }
    }
}
