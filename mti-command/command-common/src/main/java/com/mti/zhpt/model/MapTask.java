package com.mti.zhpt.model;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author yingjj@mti-sh.cn 2019年5月8日10:51
 *
 */
@Getter
@Setter
@ApiModel(value = "MapTask对象", description = "地图下发指令实体")
public class MapTask {

	@ApiModelProperty(value = "案件ID", name = "event_id", example = "6", required = true)
	private String event_id;

	@ApiModelProperty(value = "命令详情", name = "remark", example = "请立即赶赴现场，顺便打一瓶酱油", required = true)
	private String remark;

	@ApiModelProperty(value = "单位ID", name = "org_id", example = "16", required = false,hidden = true)
	private String org_id;
	private String deptName;

	@ApiModelProperty(value = "下发指令的人员", name = "persons", example = "[13,14]", required = true)
	private List<String> persons;
	
	@ApiModelProperty(value = "下发指令的人员姓名", name = "name", example = "张三",  required = false,hidden = true)
	private String name;
	
	@ApiModelProperty(value = "人员纬度", name = "person_longitude", example = "0", required = false, hidden = true)
	private double person_longitude;
	
	@ApiModelProperty(value = "人员经度", name = "person_latitude", example = "0", required = false, hidden = true)
	private double person_latitude;
	
	@ApiModelProperty(value = "纬度(设置集结点的话需要提供)", name = "longitude", example = "0", required = false, hidden = false)
	private double longitude;

	@ApiModelProperty(value = "经度(设置集结点的话需要提供)", name = "latitude", example = "0", required = false, hidden = false)
	private double latitude;
	
	@ApiModelProperty(value = "变为指令的时间", name = "doTime", example = "2018-05-09 22:10:23", required = false, hidden = true)
	private Date doTime;

	@ApiModelProperty(value = "对应处置时间轴ID", name = "timer_id", example = "88", required = false, hidden = true)
	private int timer_id;

	@ApiModelProperty(value = "消息类型", name = "msgType", example = "1", required = false, hidden = true)
	private int msgType;

	private String code;



	/**
	 * 下达警察名称
	 */
	@ApiModelProperty(value = "警察名称", dataType = "Stirng")
	private String sendPolice;

	/**
	 * 下达警察code
	 */
	@ApiModelProperty(value = "警察code", dataType = "Stirng")
	private String sendPoliceId;


	/**
	 * 下达单位
	 */
	@ApiModelProperty(value = "下达单位", dataType = "Stirng")
	private String sendOrg;

	/**
	 * 下达单位ID
	 */
	@ApiModelProperty(value = "下达单位ID", dataType = "Stirng")
	private String sendOrgId;

	private String tasksId;
}