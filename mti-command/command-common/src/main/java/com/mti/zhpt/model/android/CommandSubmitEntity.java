package com.mti.zhpt.model.android;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.zhpt.model.CpPerson;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Getter
@Setter
public class CommandSubmitEntity implements Serializable {


    /**
     * 命令id
     */
    private String id;

    /**
     * 事件命令id
     */
    private Integer timerId;

    /**
     * 反馈id
     */
    private String responseId;

    /**
     * 事件id
     */
    private String eventId;

    /**
     * 图片list
     */
    private List<String> imagesUrl;
    /**
     * 视频list
     */
    private List<String> videosUrl;
    /**
     * 声音list
     */
    private List<String> audiosUrl;
    /**
     * 相关人list
     */
    private List<CpPerson> relevantPerson;
    /**
     * 现场描述文字
     * （指令内容）
     */
    private String remark;
    /**
     * 反馈标题
     */
    private String feedback;
    /**
     * 指令单状态：10已下达、20已到达、30已接收、40已出动、50已到场、60已反馈、70已作废
     */
    private Integer status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    /**
     * 组织名称
     */
    private String deptName;
    /**
     * 组织code
     */
    private String deptCode;
    /**
     * 警察名称
     */
    private String policeName;
    /**
     * 警察code
     */
    private String policeCode;

    /**
     * 消息类型
     */
    private String messageType;

    /**
     * 当前位置
     */
    private String address;

    /**
     * 地址分类
     */
    private String addressType;

    /**
     * 案由
     */
    private String case_;

    /**
     * 警情级别
     */
    private String policeLevel;

}
