package com.mti.zhpt.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseEntity implements Serializable {

    //当前页
    private Integer pageNum;
    //每页的数量
    private Integer pageSize;

}
