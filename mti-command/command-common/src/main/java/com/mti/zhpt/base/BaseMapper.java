package com.mti.zhpt.base;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.UpdateProvider;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;
import tk.mybatis.mapper.provider.SpecialProvider;
import tk.mybatis.mapper.provider.base.BaseUpdateProvider;

/**
 * @classDesc: 功能描述:(mybatis基础类)
 * @author 徐宝振
 * @createTime: 2018年8月3日 下午2:15:40
 * @version: v1.0
 * @copyright:上海道枢信息技术有限公司
 */
@Repository
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {

    /**
     * @methodDesc: 功能描述: 逻辑删除-批量删除
     * @author Wu,Hua-Zheng
     * @createTime 2018年3月16日 下午3:40:22
     * @version v1.0.0
     * @param idList
     */
    public int delGroup(@Param("idList") List<String> idList);

    /**
     * @methodDesc: 功能描述: 逻辑删除-刪除单个对象
     * @author Wu,Hua-Zheng
     * @createTime 2018年3月16日 下午3:39:44
     * @version v1.0.0
     * @param id
     */
    public int delSingle(@Param("id") String id);

    /**
     * @methodDesc: 功能描述: 条件查询-参数Map类型
     * @author Wu,Hua-Zheng
     * @createTime 2018年3月16日 下午4:43:39
     * @version v1.0.0
     * @param paraMap
     */
    public List<T> findAllByPara(Map<String, Object> paraMap);

    /**
     * @methodDesc: 功能描述: 条件查询-参数Object类型
     * @author Wu,Hua-Zheng
     * @createTime 2018年3月16日 下午4:43:39
     * @version v1.0.0
     * @param para
     */
    public List<T> findAllByPara(Object para);

    /**
     * @methodDesc: 功能描述: 通过主键更新不为null的字段
     * @author Wu,Hua-Zheng
     * @createTime 2018年3月15日 下午2:52:28
     * @version v1.0.0
     * @copyright: 上海道枢信息技术有限公司
     */
    @UpdateProvider(type = BaseUpdateProvider.class, method = "dynamicSQL")
    @Options(useCache = false, useGeneratedKeys = false)
    int updateByPrimaryKeySelective(T record);

    /**
     * @methodDesc: 功能描述: 批量插入，支持批量插入的数据库可以使用，例如MySQL,H2等，另外该接口限制实体包含`id`属性并且必须为自增列
     * @author Wu,Hua-Zheng
     * @createTime 2018年3月15日 下午2:52:28
     * @version v1.0.0
     * @copyright: 上海道枢信息技术有限公司
     */
    @Options(useGeneratedKeys = true, keyProperty = "ID_")
    @InsertProvider(type = SpecialProvider.class, method = "dynamicSQL")
    int insertList(List<T> recordList);

}
