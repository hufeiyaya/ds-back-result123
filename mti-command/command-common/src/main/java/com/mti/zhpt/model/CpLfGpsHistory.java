package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * <p>
 * 历史轨迹对象
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-08
 */
@Data
@Table(name = "cp_lf_gps_history")
@ApiModel(value = "历史轨迹对象", description = "历史轨迹对象")
public class CpLfGpsHistory {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	@Id
	@Column(name = "id")
	private String id;

	/**
	 * 警员id
	 */
	@ApiModelProperty(value = "警员id")
	@Column(name = "police_id")
	private String policeId;

	/**
	 * 设备id
	 */
	@ApiModelProperty(value = "设备id")
	@Column(name = "gps_id")
	private String gpsId;

	/**
	 * 经度
	 */
	@ApiModelProperty(value = "经度")
	@Column(name = "longitude")
	private String longitude;

	/**
	 * 纬度
	 */
	@ApiModelProperty(value = "纬度")
	@Column(name = "latitude")
	private String latitude;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间" , hidden = true)
	@Column(name = "create_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

    /**
     * 定位类型：0.未知 1.人 2.车辆
     */
	@ApiModelProperty(value = "定位类型：0.未知 1.人 2.车辆", notes = "0.未知 1.人 2.车辆")
	@Column(name = "type")
	private Integer type;

	/**
	 * 警员名称
	 */
	@ApiModelProperty(value = "警员名称")
	@Column(name = "police_name")
	private String policeName;

	/**
	 * 警员电话
	 */
	@ApiModelProperty(value = "警员电话")
	@Column(name = "police_tel")
	private String policeTel;

	/**
	 * 警员单位
	 */
	@ApiModelProperty(value = "警员单位")
	@Column(name = "police_org")
	private String policeOrg;

}
