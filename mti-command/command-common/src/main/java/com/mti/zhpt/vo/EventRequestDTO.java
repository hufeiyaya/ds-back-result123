package com.mti.zhpt.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "事件列表接口")
public class EventRequestDTO implements Serializable {
    private static final long serialVersionUID = 1320421148014966394L;
    @ApiModelProperty(value = "开始页",required = true)
    private Integer start;
    @ApiModelProperty(value = "页大小",required = true)
    private Integer size;
    @ApiModelProperty(value = "报警类型")
    private String callPoliceType;
    @ApiModelProperty(value = "开始时间")
    private String startDate;
    @ApiModelProperty(value = "结束时间")
    private String endDate;
    @ApiModelProperty(value = "事件状态(0:未处置 1:处置中 2:已处置)")
    private String eventStatus;
    @ApiModelProperty(value = "人员类型")
    private String personType;
    @ApiModelProperty(value = "事件类型(0：110警情，1：重点人员，2：在逃人员)")
    private String eventType;
    @ApiModelProperty(value = "警员ID")
    private String policeId;
    @ApiModelProperty(value = "所属辖区")
    private String range;
    @ApiModelProperty(value = "所属辖区类型(0:辖区ID,1:派出所ID,2:民警ID)")
    private String rangeType;
    @ApiModelProperty(value = "在控状态")
    private String isControl;
    @ApiModelProperty(value = "人员级别")
    private String personLevel;
    @ApiModelProperty(value = "负责部门")
    private String zrbm;
    @ApiModelProperty(value = "人员来源(多个使用英文逗号隔开)")
    private String ryly;
    @ApiModelProperty(value = "报警反馈超时(0:未超时 1:已超时)")
    private String isOverTime;
    @ApiModelProperty(value = "搜索条件")
    private String searchTerm;
    @ApiModelProperty(value = "当前用户组织ID")
    private String userOrgId;
    @ApiModelProperty(value = "当前用户责任部门")
    private String userZrbm;
    @ApiModelProperty(value = "当前角色ID(1:市局,2:分局,3:派出所)")
    private String userType;

    public String getRyly() {
        return StringUtils.isEmpty(ryly)?"":ryly;
    }

    public void setRyly(String ryly) {
        this.ryly = ryly;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start<=0?0:start;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size<=0?10:size;
    }

    public String getCallPoliceType() {
        return StringUtils.isEmpty(callPoliceType)?"":callPoliceType;
    }

    public void setCallPoliceType(String callPoliceType) {
        this.callPoliceType = callPoliceType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEventStatus() {
        return StringUtils.isEmpty(eventStatus)?"":eventStatus;
    }

    public void setEventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
    }

    public String getPersonType() {
        return StringUtils.isEmpty(personType)?"":personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getPoliceId() {
        return policeId;
    }

    public void setPoliceId(String policeId) {
        this.policeId = policeId;
    }

    public String getRange() {
        return StringUtils.isEmpty(range)?"":range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getIsControl() {
        return StringUtils.isEmpty(isControl)?"":isControl;
    }

    public void setIsControl(String isControl) {
        this.isControl = isControl;
    }

    public String getPersonLevel() {
        return StringUtils.isEmpty(personLevel)?"":personLevel;
    }

    public void setPersonLevel(String personLevel) {
        this.personLevel = personLevel;
    }

    public String getZrbm() {
        return StringUtils.isEmpty(zrbm)?"":zrbm;
    }

    public void setZrbm(String zrbm) {
        this.zrbm = zrbm;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public String getRangeType() {
        return StringUtils.isEmpty(rangeType)?"":rangeType;
    }

    public void setRangeType(String rangeType) {
        this.rangeType = rangeType;
    }

    public String getIsOverTime() {
        return StringUtils.isEmpty(isOverTime) ? "" : isOverTime;
    }

    public void setIsOverTime(String isOverTime) {
        this.isOverTime = isOverTime;
    }
}
