package com.mti.zhpt.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
@Data
@ApiModel(value = "联络网请求体")
@AllArgsConstructor
@NoArgsConstructor
public class RelationNetDispatchVO {
    @ApiModelProperty(value = "当前页")
    private Integer page;
    @ApiModelProperty(value = "页大小")
    private Integer size;
    @ApiModelProperty(value = "部门名称")
    private String orgName;
    @ApiModelProperty(value = "人员姓名")
    private String personName;
    @ApiModelProperty(value = "联系方式")
    private String phone;
    @ApiModelProperty(value = "tab页名称")
    private String moduleType;
}
