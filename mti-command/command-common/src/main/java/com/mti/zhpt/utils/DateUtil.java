package com.mti.zhpt.utils;

import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
* @Description: 时间工具类
* @Author: zhaopf@mti-sh.cn
* @Date: 2019/5/7 14:57
*/
public class DateUtil {

    public static final SimpleDateFormat datetimeFormat = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat yyyyMMdd = new SimpleDateFormat(
            "yyyy-MM-dd");
    /**
     * 格式化日期
     * @param date
     * @param pattern
     * 		"date":返回日期(yyyy-MM-dd)
     * 		"time":返回时间(HH:mm:ss)
     * 		"datetime":返回日期和时间(yyyy-MM-dd HH:mm:ss)
     * @return
     */
    public static String formatDate(Date date, String pattern) {
        if (date == null) {
            return "";
        }

        if ("date".equals(pattern)) {
            pattern = "yyyy-MM-dd";
        } else if ("time".equals(pattern)) {
            pattern = "HH:mm:ss";
        } else if ("datetime".equals(pattern)) {
            pattern = "yyyy-MM-dd HH:mm:ss";
        }

        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        return formatter.format(date);
    }

    /**
     * 字符串转日期
     *
     * @param dateString
     * @param format 日期格式
     * @return
     */
    public static Date strToDate(String dateString, SimpleDateFormat format){
        if(dateString == null) {
            throw new InvalidParameterException("dateString cannot be null!");
        }
        try {
            return format.parse(dateString);
        } catch (Exception e) {
            throw new RuntimeException("parse error! [dateString:"+ dateString +";format:"+ format +"]");
        }
    }
    public static Long getDateFZ(Date endDate, Date nowDate) {
        Long dateFZ =( endDate.getTime() - nowDate.getTime()) /(1000 * 60);
        return dateFZ;
    }

    public static String getDatePoor(Date endDate, Date nowDate) {

        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return day + "天" + hour + "小时" + min + "分钟";
    }

}
