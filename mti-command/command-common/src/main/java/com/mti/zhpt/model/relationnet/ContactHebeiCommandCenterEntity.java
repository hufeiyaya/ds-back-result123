package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 河北各地市指挥中心
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "t_contact_hebei_command_center")
@AllArgsConstructor
@NoArgsConstructor
public class ContactHebeiCommandCenterEntity extends BaseEntity {
    private static final long serialVersionUID = 3516525762315794670L;
    @Column(name = "city_")
    private String city_;
    @Column(name = "area_code")
    private String areaCode;
    @Column(name = "switchboard_")
    private String switchBoard_;
    @Column(name = "outside_line")
    private String outsideLine;
    @Column(name = "fax_number")
    private String faxNumber;
    @Column(name = "private_network")
    private String privateNetwork;
}
