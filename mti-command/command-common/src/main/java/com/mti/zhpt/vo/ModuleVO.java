package com.mti.zhpt.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/8
 * @change
 * @describe describe
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class ModuleVO {
    @ApiModelProperty(value = "菜单名")
    private String name;
    @ApiModelProperty(value = "菜单名对应的type")
    private String type;
}
