package com.mti.zhpt.model;

import lombok.Data;

@Data
public class TestEntity {
    private String name;
    private String age;
}
