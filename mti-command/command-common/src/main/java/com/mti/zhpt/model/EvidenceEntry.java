package com.mti.zhpt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mti.zhpt.base.BaseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


/**
 * 取证实体
 *
 * @author machao
 */
@Getter
@Setter
@Entity
@Table(name = "evidence_entry")
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "取证参数", description = "取证参数")
public class EvidenceEntry extends BaseEntity {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
     * 取证人id
     */
	@ApiModelProperty(value = "取证人id")
	@Column(name = "user_id")
    private String userId;
    /**
     * 取证人name
     */
	@ApiModelProperty(value = "取证人name")
	@Column(name = "user_name")
    private String userName;
    /**
     * 指令单号
     */
	@ApiModelProperty(value = "指令单号")
	@Column(name = "order_id")
    private String orderId;
    /**
     * 反馈单号
     */
	@ApiModelProperty(value = "反馈单号")
	@Column(name = "feedback_id")
    private String feedbackId;
    /**
     * 音频，多个地址用“，”隔开
     */
	@ApiModelProperty(value = "音频，多个地址用“，”隔开")
	@Column(name = "audios")
    private String audios;
    /**
     * 视频，多个地址用“，”隔开
     */
	@ApiModelProperty(value = "视频，多个地址用“，”隔开")
	@Column(name = "videos")
    private String videos;
    /**
     * 图片，多个地址用“，”隔开
     */
	@ApiModelProperty(value = "图片，多个地址用“，”隔开")
	@Column(name = "images")
    private String images;
    /**
     * 取证说明
     */
	@ApiModelProperty(value = "取证说明")
	@Column(name = "content")
    private String content;
    /**
     * 取证地址经度
     */
	@ApiModelProperty(value = "取证地址经度")
	@Column(name = "longitude")
    private Double longitude;
    /**
     * 取证地址经度
     */
	@ApiModelProperty(value = "取证地址经度")
	@Column(name = "latitude")
    private Double latitude;
    /**
     * 取证地址
     */
	@ApiModelProperty(value = "取证地址")
	@Column(name = "address")
    private String address;
    /**
     * 创建时间
     */
	@ApiModelProperty(value = "创建时间")
	@Column(name = "create_time")
    private String createTime;
    /**
     * 更新时间
     */
	@ApiModelProperty(value = "更新时间")
	@Column(name = "update_time")
    private String updateTime;

}
