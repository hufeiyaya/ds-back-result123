package com.mti.zhpt.model.android;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
* @Description: 警情的基本信息
* @Author: zhaopf@mti-sh.cn
* @Date: 2019/4/28 14:25
*/
@Setter
@Getter
public class AlarmEntity implements Serializable {

    /**
     * 行政区划id
     */
    private String adminRegionId;
    /**
     * 来话类别  报警求助 还是。。。
     */
    private String incomingType;
    /**
     * 接警类型编号
     */
    private String alarmMode;

    /**
     * 报警方式  电话报警 短信报警
     */
    private String reportAlarmMode;

    /**
     * 电话呼入时间
     */
    private String callTime;
    /**
     * 电话结束时间
     */
    private String endingTime;
    /**
     * 报警人名称
     */
    private String reportPersonName;
    /**
     * 报警人性别代码
     */
    private String reportPersonSex;
    /**
     * 报警联系人电话
     */
    private String reportPersonPhoneNum;
    /**
     * 报警联系人地址
     */
    private String reportPersonAddress;

    /**
     * 地址类别
     */
    private String locationCategory;
    /**
     * 地址类型
     */
    private String locationType;
    /**
     * 接警人id
     */
    private String alarmOfficerId;
    /**
     * 接警人姓名
     */
    private String alarmOfficerName;
    /**
     * 报警内容
     */
    private String alarmContent;

    /**
     * 警情地址
     */
    private String alarmAddress;
    /**
     * 事件详情
     */
    private String incidentDetail;
    /**
     * 警情状态
     */
    private String alarmStatus;

    /**
     * 警情 录音号
     */
    private String alarmRecordId;


    private Integer clue;

    /**
     * 案由类别代码 一级
     */
    private String alarmCategory;
    /**
     * 案由二级类型代码
     */
    private String alarmType;
    /**
     * 案由三级明细代码
     */
    private String alarmDetail;

    /**
     * 警情级别代码
     */
    private String alarmLevel;
    /**
     * 经纬度信息
     */
    private String longitude;
    private String latitude;

    private String autoLongitude;
    private String autoLatitude;

    private String transferAlarmId;

    /**
     * 管辖单位
     */
    private String jurisdictionUnit;
    /**
     * 管辖单位名称
     */
    private String jurisdictionUnitName;

    /**
     * 时间
     */
    private String createTime;
    private String updateTime;
    /*车牌类型*/
    private String carNumType;
    /*车辆数量*/
    private String carNum;
    /*受伤人员数量*/
    private String injuredNum;
    /*死亡人数*/
    private String deathNum;
    /*逃跑人员*/
    private String escape;
    /*被困人员数量*/
    private String trappedNum;

    /*分页*/
    private int size;
    private int start;


}
