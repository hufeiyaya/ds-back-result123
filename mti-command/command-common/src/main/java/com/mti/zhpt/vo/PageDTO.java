package com.mti.zhpt.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageDTO<T> implements Serializable {
    private static final long serialVersionUID = 4983722461045408551L;
    private List<T> record;
    private long total;
}
