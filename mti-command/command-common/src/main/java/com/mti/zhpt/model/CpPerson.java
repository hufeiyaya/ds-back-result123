package com.mti.zhpt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import com.mti.zhpt.base.BaseEntity;

/**
 * <p>
 * 人员
 * </p>
 *
 * @author 潘文俊
 */
@Getter
@Setter
@Entity
@Table(name = "cp_person")
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "人员参数", description = "人员参数")
public class CpPerson extends BaseEntity {

	private static final long serialVersionUID = 1L;


	/**
	 * 关联cp_event_personnel
	 * person_id
	 */
	private Integer numId;

	/**
	 * 性别
	 */
	@ApiModelProperty(value = "性别")
	@Column(name = "sex")
	private String sex;

	/**
	 * 身份证
	 */
	@ApiModelProperty(value = "身份证")
	@Column(name = "id_number")
	private String idNumber;

	/**
	 * 联系方式
	 */
	@ApiModelProperty(value = "联系方式")
	@Column(name = "phone")
	private String phone;

	// 创建时间
	@ApiModelProperty(hidden = true)
	@Column(name = "create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date createTime;
	
	/**
	 * 民族
	 */
	@ApiModelProperty(value = "民族")
	@Column(name = "nation")
	private String nation;
	
	/**
	 * 姓名
	 */
	@ApiModelProperty(value = "姓名")
	@Column(name = "name")
	private String name;
	
	/**
	 * 住址
	 */
	@ApiModelProperty(value = "住址")
	@Column(name = "family_address")
	private String familyAddress;
	
	/**
	 * 头像
	 */
	@ApiModelProperty(value = "头像")
	@Column(name = "photourl")
	private String photourl;
	
	/**
	 * 年龄
	 */
	@ApiModelProperty(value = "年龄")
	@Column(name = "age")
	private String age;
	
	/**
	 * 生日
	 */
	@ApiModelProperty(value = "生日")
	@Column(name = "birth")
	private String birth;
	
	/**
	 * 籍贯
	 */
	@ApiModelProperty(value = "籍贯")
	@Column(name = "native_place")
	private String nativePlace;

	/**
	 * 是否重点人员
	 */
	@ApiModelProperty(value = "是否重点人员")
	@Column(name = "is_key")
	private String isKey;

	/**
	 * 人员类型
	 */
	@ApiModelProperty(value = "人员类型", notes = "报警人；嫌疑人；受害人；证人")
	@Transient
	private String personType;
}
