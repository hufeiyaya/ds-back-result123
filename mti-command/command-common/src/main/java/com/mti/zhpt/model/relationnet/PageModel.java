package com.mti.zhpt.model.relationnet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by jingzhanwu
 * @date 2019/8/5
 * @change
 * @describe describe
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageModel<T> {
    private T record;
    private Long total;
    private Integer page;
    private Integer size;
}
