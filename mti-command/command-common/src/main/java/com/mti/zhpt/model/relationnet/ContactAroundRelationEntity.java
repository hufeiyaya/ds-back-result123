package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 市区周边通讯录
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_contact_around_relation")
public class ContactAroundRelationEntity extends BaseEntity {
    private static final long serialVersionUID = -5852350008990248076L;
    @Column(name = "org_name")
    @ApiModelProperty(value = "机构名称")
    private String orgName;
    @Column(name = "unit_name")
    @ApiModelProperty(value = "单位名称")
    private String unitName;
    @Column(name = "person_name")
    @ApiModelProperty(value = "姓名")
    private String personName;
    @Column(name = "telephone_")
    @ApiModelProperty(value = "手机号码")
    private String telephone_;
    @Column(name = "full_day_phone")
    @ApiModelProperty(value = "24小时值班电话")
    private String fullDayPhone;
    @Column(name = "inside_line_phone")
    @ApiModelProperty(value = "内线")
    private String insideLinePhone;

}
