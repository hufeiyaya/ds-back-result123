package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 350M电台，电话调度
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_contact_radio_phone_dispatch")
public class ContactRadioPhoneDispatchEntity extends BaseEntity {
    private static final long serialVersionUID = 1513931458856030435L;
    @Column(name = "unit_name")
    private String unitName;
    @Column(name = "radio_")
    private String radio_;
    @Column(name = "phone_")
    private String phone_;
}
