package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mti.zhpt.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * <p>
 * 人员
 * </p>
 *
 * @author 潘文俊
 */
@Getter
@Setter
@ApiModel(value = "", description = "重点人员报警每日统计")
public class KeyPersonneEntity {
    private String event_id;
    @ApiModelProperty(value = "姓名", dataType = "String")
    private String xm;
    @ApiModelProperty(value = "身份证号", dataType = "String")
    private String sfzh;
    @ApiModelProperty(value = "户籍地址", dataType = "String")
    private String hjd;
    @ApiModelProperty(value = "人员类型", dataType = "String")
    private String rylb;
    @ApiModelProperty(value = "报警方式", dataType = "String")
    private String warning_type;
    @ApiModelProperty(value = "报警地址", dataType = "String")
    private String address;
    @ApiModelProperty(value = "下发时间", dataType = "String")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date send_time;
    @ApiModelProperty(value = "反馈时间", dataType = "String")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date fktime;
    @ApiModelProperty(value = "反馈部门", dataType = "String")
    private String deptName;
    /**
     * 0  未超时
     * 1  超时
     */
    @ApiModelProperty(value = "反馈是否超时：0未超时、1超时", dataType = "String")
    private String isNOt;
    @ApiModelProperty(value = "反馈内容", dataType = "String")
    private String content;
    @ApiModelProperty(value = "编号", dataType = "String")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date create_time;


}
