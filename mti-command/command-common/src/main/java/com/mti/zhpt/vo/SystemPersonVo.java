package com.mti.zhpt.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;

/**
 * <p>
 * 系统用户对象
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-12
 */
@Data
public class SystemPersonVo {

    private String id;

    private String number;

    private String name;

    private String policeCode;

    private Integer credentialsType;

    private String credentialsNumber;

    private Integer sex;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date birthDate;

    private String address;

    private Integer security;

    private String version;

    private Integer available;

    private String phone;

    private String email;

    private String creatorId;

    private String mainDuty;

    private String avator;

    private Integer delete;

    private String orgName;

}
