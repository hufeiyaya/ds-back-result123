package com.mti.zhpt.model;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author yingjj@mti-sh.cn 2019年5月6日10:51
 *
 */
@Getter
@Setter
@ApiModel(value="Circle对象",description="地理空间对象Circle")
public class Circle{
	
	@ApiModelProperty(value="类型",name="type",example="Circle",required=true)
	private String type;
	@ApiModelProperty(value="半径",name="radius",example="3123.234",required=true)
	private Double radius;
	@ApiModelProperty(value="中心点坐标",name="center",example="[115.80690155406343,40.027625145035465]",required=true)
	private List<Double> center;
	@ApiModelProperty(value="附加信息",name="properties",example="{'pageid':1,'pagesize':10}",required=true)
	private Map<String,Object> properties;
	
	
	

}