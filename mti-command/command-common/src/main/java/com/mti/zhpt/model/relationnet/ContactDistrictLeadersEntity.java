package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 领导班子
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "t_contact_district_leaders")
@AllArgsConstructor
@NoArgsConstructor
public class ContactDistrictLeadersEntity extends BaseEntity {
    private static final long serialVersionUID = -7872192762518661207L;
    @Column(name = "org_name")
    private String orgName;
    @Column(name = "unit_name")
    private String unitName;
    @Column(name = "person_name")
    private String personName;
    @Column(name = "position_")
    private String position_;
    @Column(name = "office_phone")
    private String officePhone;
    @Column(name = "telephone_")
    private String telephone;
    @Column(name = "charge_work")
    private String chargeWork;
}
