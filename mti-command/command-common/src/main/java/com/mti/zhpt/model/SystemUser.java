package com.mti.zhpt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import com.mti.zhpt.base.BaseEntity;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author 潘文俊
 * @since 2018-11-12
 */
@Getter
@Setter
@Entity
@Table(name = "system_user")
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "用户参数", description = "用户参数")
public class SystemUser extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户名
	 */
	@ApiModelProperty(value = "用户名")
	@Column(name = "username")
	private String username;

	/**
	 * 密码
	 */
	@ApiModelProperty(value = "密码")
	@Column(name = "password")
	private String password;

	/**
	 * 数据权限
	 */
	@ApiModelProperty(value = "数据权限")
	@Column(name = "user_role")
	private String userRole;

	// 创建时间
	@ApiModelProperty(hidden = true)
	@Column(name = "create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date createTime;
	
	/**
	 * 辅警
	 */
	@ApiModelProperty(value = "辅警")
	@Column(name = "police")
	private String police;
	
	/**
	 * 姓名
	 */
	@ApiModelProperty(value = "姓名")
	@Column(name = "name")
	private String name;
	
	/**
	 * 组织结构
	 */
	@ApiModelProperty(value = "组织结构")
	@Column(name = "org_name")
	private String orgName;
	
	/**
	 * 状态
	 */
	@ApiModelProperty(value = "状态")
	@Column(name = "status")
	private String status;
	
	/**
	 * 装备
	 */
	@ApiModelProperty(value = "装备")
	@Column(name = "equip")
	private String equip;
}
