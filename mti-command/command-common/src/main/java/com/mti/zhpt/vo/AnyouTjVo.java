package com.mti.zhpt.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "案由统计对象", description = "案由统计对象")
public class AnyouTjVo {

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "总数")
    private String count;

}
