package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 市局机关单位成员
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_contact_city_bureau_unit_member")
public class ContactCityBureauUnitMemberEntity extends BaseEntity {
    private static final long serialVersionUID = 5560596506906350437L;
    @Column(name = "unit_name")
    private String unitName;
    @Column(name = "person_name")
    private String personName;
    @Column(name = "position_")
    private String position_;
    @Column(name = "sort_")
    private String sort_;
    @Column(name = "office_phone")
    private String officePhone;
    @Column(name = "residence_phone")
    private String residencePhone;
    @Column(name = "phone_")
    private String phone_;
    @Column(name = "room_number")
    private String roomNumber;
    @Column(name = "work_")
    private String work_;


}
