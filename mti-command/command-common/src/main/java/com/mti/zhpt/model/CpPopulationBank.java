package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * <p>
 * 人口库
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-29
 */
@Data
@Table(name = "cp_population_bank")
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "人口库", description = "人口库")
public class CpPopulationBank {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Id
	@Column(name = "id_")
	@ApiModelProperty(value = "主键")
	public String id;

	/**
	 * 姓名
	 */
	@ApiModelProperty(value = "姓名")
	@Column(name = "name_")
	private String name;

	/**
	 * 性别
	 */
	@ApiModelProperty(value = "性别")
	@Column(name = "sex_")
	private String sex;

	/**
	 * 民族
	 */
	@ApiModelProperty(value = "民族")
	@Column(name = "nation_")
	private String nation;

	/**
	 * 身份证号
	 */
	@ApiModelProperty(value = "身份证号")
	@Column(name = "id_card")
	private String idCard;

	/**
	 * 手机号
	 */
	@ApiModelProperty(value = "手机号")
	@Column(name = "phone_")
	private String phone;

	/**
	 * 地址
	 */
	@ApiModelProperty(value = "地址")
	@Column(name = "address_")
	private String address;

	/**
	 * 照片
	 */
	@ApiModelProperty(value = "照片")
	@Column(name = "photo_")
	private String photo;

	/**
     * 创建时间
	 */
	@ApiModelProperty(hidden = true)
	@Column(name = "create_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

}
