package com.mti.zhpt.vo;

import com.mti.zhpt.model.EventReserveDetailEntity;
import com.mti.zhpt.model.EventReserveEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/** 
 * <p>
 * 预案指挥 vo
 * </p>
 *
 * cp_reserve
 *
* @author: zhaichen
* @since 2019-06-19 20:14
*/ 
@Data
@ApiModel(value = "预案指挥扩展信息对象", description = "预案指挥扩展对象")
public class EventReserveVo extends EventReserveEntity {

    /**
     * 预案指挥详情对象列表
     */
    @ApiModelProperty(value = "预案指挥详情对象集合", dataType = "List<EventReserveDetailEntity>")
    private List<EventReserveDetailEntity> eventReserveDetailEntityList;


}
