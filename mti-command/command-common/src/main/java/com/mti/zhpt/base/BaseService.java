package com.mti.zhpt.base;

import java.util.List;

/**  
 * @classDesc: 功能描述:(service基础类)
 * @author 徐宝振
 * @createTime: 2018年8月3日 下午2:15:40
 * @version: v1.0  
 * @copyright:上海道枢信息技术有限公司
 */
public interface BaseService<T> {
	
    public int save(T entity);// 保存
    
    public int batchSave(List<T> list);//批量保存
    
    public int delete(String id);// 删除用户
    
    public int delete(T entity);// 删除用户

    public int update(T entity);// 更新用户null不更新
    
    public int updateByPrimaryKey(T entity);// 更新用户所有信息

    public T get(String id);// 查找用户
    
    public T selectOne(T entity);// 查找用户

    public List<T> getAll();// 查找所有
    
    public List<T> select(T entity);// 查找所有
    
    public  List<T> selectByExample(Object example);//根据Example条件进行查询
    
    /**
     * baseService 逻辑删除
     * @author Binrui Dong
     * @createTime 2018年4月24日 下午2:36:39
     * @param ids
     * @param entity
     */
    public int deleteByExample(List<String> ids, T entity);// 根据Example删除

} 