package com.mti.zhpt.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocateInfo {
	 	private double longitude;
	    private double Latitude;
	    
	    public String generateString() {
	    	return longitude+" "+Latitude;
	    }


}
