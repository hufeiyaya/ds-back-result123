package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * <p>
 * 指令反馈信息对象
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "指令反馈信息对象", description = "指令反馈信息对象")
public class InstructionFeedbackEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号", dataType = "String")
    private String id;

    /**
     * 步骤id
     */
    @ApiModelProperty(value = "步骤id", dataType = "String")
    private String taskId;

    /**
     * 机构id
     */
    @ApiModelProperty(value = "机构id", dataType = "String")
    private String feedbackOrgId;

    /**
     * 机构名称
     */
    @ApiModelProperty(value = "机构名称", dataType = "String")
    private String feedbackOrgName;

    /**
     * 反馈人id
     */
    @ApiModelProperty(value = "反馈人id", dataType = "String")
    private String feedbackPoliceId;

    /**
     * 反馈人姓名
     */
    @ApiModelProperty(value = "反馈人姓名", dataType = "String")
    private String feedbackPoliceName;

    /**
     * 反馈电话
     */
    @ApiModelProperty(value = "反馈电话", dataType = "String")
    private String feedbackPhone;

    /**
     * 反馈内容
     */
    @ApiModelProperty(value = "反馈内容", dataType = "String")
    private String feedbackContent;

    /**
     * 反馈附件
     */
    @ApiModelProperty(value = "反馈附件", dataType = "String")
    private String feedbackFile;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", dataType = "Date", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

}
