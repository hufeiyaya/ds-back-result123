package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 时间轴对象
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-05
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "时间轴对象", description = "时间轴对象")
public class EventTimerEntity {

    private String id;

    /**
     * 事件命令id
     */
    @ApiModelProperty(value = "事件命令id", dataType = "Integer")
    private Integer timerId;

    /**
     * 事件id
     */
    @ApiModelProperty(value = "事件id", dataType = "String")
    private String eventId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", dataType = "Date" ,hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 反馈标题
     */
    @ApiModelProperty(value = "反馈标题", dataType = "String")
    private String feedback;

    /**
     * 指令内容
     */
    @ApiModelProperty(value = "指令内容", dataType = "String")
    private String remark;

    /**
     * 接收单位名称
     */
    @ApiModelProperty(value = "接收单位名称", dataType = "Stirng")
    private String deptName;

    /**
     * 接收单位ID
     */
    @ApiModelProperty(value = "接收单位ID", dataType = "Stirng")
    private String deptCode;

    /**
     * 下达单位
     */
    @ApiModelProperty(value = "下达单位", dataType = "Stirng")
    private String sendOrg;

    /**
     * 下达单位ID
     */
    @ApiModelProperty(value = "下达单位ID", dataType = "Stirng")
    private String sendOrgId;



    /**
     * 接收警察名称
     */
    @ApiModelProperty(value = "警察名称", dataType = "Stirng")
    private String policeName;

    /**
     * 接收警察code
     */
    @ApiModelProperty(value = "警察code", dataType = "Stirng")
    private String policeCode;


    /**
     * 下达警察名称
     */
    @ApiModelProperty(value = "警察名称", dataType = "Stirng")
    private String sendPolice;

    /**
     * 下达警察code
     */
    @ApiModelProperty(value = "警察code", dataType = "Stirng")
    private String sendPoliceId;




    /**
     * 出动人数
     */
    @ApiModelProperty(value = "出动人数", dataType = "Integer")
    private Integer dispatchNumber;

    /**
     * 储备人数
     */
    @ApiModelProperty(value = "储备人数", dataType = "Integer")
    private Integer reserveNumber;

    /**
     */
    @ApiModelProperty(value = "指令单状态：10已下达、20已到达、30已接收、40已出动、50已到场、60已反馈、70已作废", dataType = "Integer")
    private Integer status;

    private List<Integer> statusList;

    private List<EventTimerFileEntity> photoEntityList;

    /**
     * 图片list
     */
    private List<String> imagesUrl;

    /**
     * 视频list
     */
    private List<String> videosUrl;

    /**
     * 声音list
     */
    private List<Map<Object,Object>> audios;

    /**
     * 基于命令的
     * 经度
     */
    @ApiModelProperty(value = "经度", dataType = "Double")
    private Double longitude;

    /**
     * 基于命令的
     * 纬度
     */
    @ApiModelProperty(value = "纬度", dataType = "Double")
    private Double latitude;

    /**
     * 是否已读
     * 1未读，2已读
     */
    @ApiModelProperty(value = "是否已读:1未读，2已读", dataType = "Double", notes = "1未读，2已读")
    private Integer isRead;

    /**
     * 当前位置
     */
    @ApiModelProperty(value = "当前位置", dataType = "String")
    private String address;

    /**
     * 地址分类
     */
    @ApiModelProperty(value = "地址分类", dataType = "String")
    private String addressType;

    /**
     * 案由
     */
    @ApiModelProperty(value = "案由", dataType = "String")
    private String case_;

    /**
     * 警情级别
     */
    @ApiModelProperty(value = "警情级别", dataType = "String")
    private String policeLevel;

    /**
     * 事件类型
     */
    private String orderType;

    /**
     * 事件地址
     * 历史警情标题
     */
    private String title;
    /**
     * 管辖单位
     */
    private String unit;

    private Map<String,String> statusContent;


    /**
     * 是否是传达
     * (命令类型)
     */
    private String type;
    private int responseId;

    /**
     * 接警台编号
     */
    private String jjtbh;

    /**
     * 接警员工号
     */
    private String jjygh;


    private String tasksId;

}
