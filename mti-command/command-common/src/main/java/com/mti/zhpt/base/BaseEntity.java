package com.mti.zhpt.base;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @classDesc: 功能描述:(实体基础类)
 * @author 徐宝振
 * @createTime: 2018年8月3日 下午2:15:40
 * @version: v1.04
 * @copyright:上海道枢信息技术有限公司
 */
@Data
@MappedSuperclass
public class BaseEntity implements Serializable {

	
	private static final long serialVersionUID = -6153048148601751741L;
	
	@Id
	@Column(name = "id")
	@ApiModelProperty(value = "id")
	//@GeneratedValue(strategy = GenerationType.AUTO)
	public String id;

	@ApiModelProperty(value = "tab页类型")
	@Transient
	private String moduleType;

}
