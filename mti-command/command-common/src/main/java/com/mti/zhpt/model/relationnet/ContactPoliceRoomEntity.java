package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 警务室巡区
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_contact_police_room")
public class ContactPoliceRoomEntity extends BaseEntity {
    private static final long serialVersionUID = -9030027253322550620L;
    @Column(name = "unit_name")
    private String unitName;
    @Column(name = "regional_division")
    private String regionalDivision;
    @Column(name = "police_place")
    private String policePlace;
    @Column(name = "police_place_location")
    private String policePlaceLocation;
    @Column(name = "police_car_main_line")
    private String policeCarMainLine;
    @Column(name = "key_target_task")
    private String keyTargetTask;
}
