package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 全国公安机关
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_contact_natinal_security")
public class ContactNatinalSecurityEntity extends BaseEntity {
    private static final long serialVersionUID = -1345471390470943721L;
    @Column(name = "province_name")
    private String provinceName;
    @Column(name = "switchboard_")
    private String switchboard_;
    @Column(name = "command_center")
    private String commandCenter;
}
