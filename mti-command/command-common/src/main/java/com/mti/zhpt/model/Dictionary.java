package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * @author can
 * 系统常用字典类（事发地址等）
 * 字典表数据由系统初始导入，运维人员维护
 *
 */
@Getter
@Setter
public class Dictionary extends BaseEntity{
	
	private static final long serialVersionUID = -5148836777890447142L;

    private String id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /** 字典Key */
    private String key;
	
    /** 字典value*/
    private String value;
    
    /** 字典分类，如报警类别，事发地址分类等*/
    private String type;
	
    /** 字典rate，控制显示排序*/
    private Double rate;
	
    /** 删除标记（1：已删除，0：未删除） */
    private Integer deleteFlag;
	
    /** 子属性*/
    private List<Dictionary> childList;

}