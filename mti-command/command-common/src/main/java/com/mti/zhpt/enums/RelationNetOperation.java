package com.mti.zhpt.enums;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/7
 * @change
 * @describe describe
 **/
public class RelationNetOperation {
    public static final String CREATE = "CREATE";
    public static final String LIST = "LIST";
    public static final String DELETE = "DELETE";
    public static final String UPDATE = "UPDATE";
    public static final String GETONEBYID = "GETONEBYID";
}
