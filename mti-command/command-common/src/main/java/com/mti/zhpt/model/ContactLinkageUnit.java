package com.mti.zhpt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * <p>
 * 联动单位
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-05
 */
@Data
@Table(name = "t_contact_linkage_unit")
@ApiModel(value = "联动单位", description = "联动单位")
public class ContactLinkageUnit {

	private static final long serialVersionUID = 1L;

	/**
	 * 单位名称
	 */
	@ApiModelProperty(value = "单位名称")
	@Column(name = "unit_name")
	private String unitName;

	/**
	 * 联系人
	 */
	@ApiModelProperty(value = "联系人")
	@Column(name = "concat_name")
	private String concatName;

	/**
	 * 联系人电话
	 */
	@ApiModelProperty(value = "联系人电话")
	@Column(name = "concat_phone")
	private String concatPhone;

	/**
	 * 值班电话1
	 */
	@ApiModelProperty(value = "值班电话1")
	@Column(name = "duty_call_one")
	private String dutyCallOne;

	/**
	 * 值班电话2
	 */
	@ApiModelProperty(value = "值班电话2")
	@Column(name = "duty_call_two")
	private String dutyCallTwo;

	/**
	 * 传真
	 */
	@ApiModelProperty(value = "传真")
	@Column(name = "fax_number")
	private String faxNumber;

	/**
	 * 值班电话（夜）
	 */
	@ApiModelProperty(value = "值班电话（夜）")
	@Column(name = "duty_call_night")
	private String dutyCallNight;

	/**
	 * 值班电话（内线）
	 */
	@ApiModelProperty(value = "值班电话（内线）")
	@Column(name = "duty_call_inline")
	private String dutyCallInline;

	/**
	 * 热线电话
	 */
	@ApiModelProperty(value = "热线电话")
	@Column(name = "hotline_")
	private String hotline;

	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注")
	@Column(name = "remark_")
	private String remark;

}
