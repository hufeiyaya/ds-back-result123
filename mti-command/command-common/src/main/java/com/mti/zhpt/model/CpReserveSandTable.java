package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * <p>
 * 预案沙盘
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-02
 */
@Data
@Table(name = "cp_reserve_sand_table")
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "预案沙盘", description = "预案沙盘")
public class CpReserveSandTable {

	private static final long serialVersionUID = 1L;

    /**
     * 主键
	 */
	@Id
	@Column(name = "id_")
	@ApiModelProperty(value = "主键")
	public String id;

	/**
	 * 预案id
	 */
	@ApiModelProperty(value = "预案id")
	@Column(name = "reserve_id")
	private String reserveId;

	/**
	 * 沙盘内容
	 */
	@ApiModelProperty(value = "沙盘内容")
	@Column(name = "content_")
	private String content;

	/**
     * 创建时间
	 */
	@ApiModelProperty(hidden = true)
	@Column(name = "create_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	/**
	 * 记录状态:0为禁用，1为启用，-1为已删除
	 */
	@ApiModelProperty(value = "记录状态:0为禁用，1为启用，-1为已删除", notes = "0为禁用，1为启用，-1为已删除", example = "1")
	@Column(name = "enable_")
	private Integer enable;

}
