package com.mti.zhpt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * <p>
 * 防线对象(卡点警力)
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-06
 */
@Data
@Table(name = "t_contact_card_police_force")
@ApiModel(value = "防线对象", description = "防线对象")
public class ContactCardPoliceForce {

	private static final long serialVersionUID = 1L;

	/**
	 * 单位ID
	 */
	@ApiModelProperty(value = "单位ID")
	@Column(name = "region_id")
	private String regionId;

	/**
	 * 单位名称
	 */
	@ApiModelProperty(value = "单位名称")
	@Column(name = "unit_name")
	private String unitName;

	/**
	 * 卡口名称
	 */
	@ApiModelProperty(value = "卡口名称")
	@Column(name = "card_name")
	private String cardName;

	/**
	 * 位置
	 */
	@ApiModelProperty(value = "位置")
	@Column(name = "location_")
	private String location;

	/**
	 * 责任单位
	 */
	@ApiModelProperty(value = "责任单位")
	@Column(name = "responsible_unit")
	private String responsibleUnit;

	/**
	 * 距离
	 */
	@ApiModelProperty(value = "距离")
	@Column(name = "distance_")
	private String distance;

	/**
	 * 类型
	 */
	@ApiModelProperty(value = "类型")
	@Column(name = "type_")
	private String type;

	/**
	 * 负责人
	 */
	@ApiModelProperty(value = "负责人")
	@Column(name = "principal_")
	private String principal;

	/**
	 * 人员数量
	 */
	@ApiModelProperty(value = "人员数量")
	@Column(name = "employees_number")
	private String employeesNumber;

	/**
	 * 携带设备
	 */
	@ApiModelProperty(value = "携带设备")
	@Column(name = "carrying_equipment")
	private String carryingEquipment;

	/**
	 * 到达时间
	 */
	@ApiModelProperty(value = "到达时间")
	@Column(name = "arrival_time")
	private String arrivalTime;

	/**
	 * 电话，电台数量
	 */
	@ApiModelProperty(value = "电话，电台数量")
	@Column(name = "phone_radio")
	private String phoneRadio;

	/**
	 * 电话
	 */
	@ApiModelProperty(value = "电话")
	@Column(name = "phone_")
	private String phone;

	/**
	 * 防道类型：1.一道防线 2.二道防线
	 */
	@ApiModelProperty(value = "防道类型：1.一道防线 2.二道防线", notes = "1.一道防线 2.二道防线")
	@Column(name = "line_type")
	private String lineType;

}
