package com.mti.zhpt.model.websocket;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class WSEventFeedbackOverTimeEntity implements Serializable {

    private String address;
    private String create_time;
    private String event_id;
    private String event_type;
    private String latitude;
    private String longitude;
    private String mjId;
    private String oid;
    private String phone;
    private String send_time;
    private String sfzh;
    private String sspcs;
    private String ssxq;
    private String ssxq_id;
    private String warning_type;
    private String zrbm;
}
