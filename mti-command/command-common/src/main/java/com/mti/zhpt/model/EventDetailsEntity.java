package com.mti.zhpt.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class EventDetailsEntity{
    /**
     * 事件id
     */
    private Integer eventId;
    /**
     * 报警人
     */
    private String policeMan;
    /**
     * 电话
     */
    private String phone;
    /**
     *  案件类型
     *  数据字典key
     */
    private String caseType;
    /**
     *  录音（对话内容）
     */
    private String record;
    /**
     *  管辖单位
     */
    private String unit;
    /**
     *  警情级别
     */
    private String level;
    /**
     *  地址
     */
    private String address;
    /**
     *  报警内容
     */
    private String content;
    /**
     *  执行的预案编号
     */
    private Integer reserve_id;
    /**
     *  状态
     */
    private String status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    /**
     *  经度
     */
    private Float longitude;
    /**
     *  纬度
     */
    private Float latitude;
    /**
     *  录音存储位置
     */
    private String recordMp3Url;

    /**
     * 录音长度（秒）
     */
    private String recordMp3Length;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date endTime;


}
