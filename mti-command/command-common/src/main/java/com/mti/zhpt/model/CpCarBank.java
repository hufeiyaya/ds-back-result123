package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * <p>
 * 车辆库
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-29
 */
@Data
@Table(name = "cp_car_bank")
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "车辆库", description = "车辆库")
public class CpCarBank {

	private static final long serialVersionUID = 1L;

    /**
     * 主键
	 */
	@Id
	@Column(name = "id_")
	@ApiModelProperty(value = "主键")
	public String id;

	/**
	 * 车主姓名
	 */
	@ApiModelProperty(value = "车主姓名")
	@Column(name = "name_")
	private String name;

	/**
	 * 车主手机号
	 */
	@ApiModelProperty(value = "车主手机号")
	@Column(name = "phone_")
	private String phone;

	/**
	 * 车牌号
	 */
	@ApiModelProperty(value = "车牌号")
	@Column(name = "car_number")
	private String carNumber;

	/**
	 * 车型
	 */
	@ApiModelProperty(value = "车型")
	@Column(name = "car_model")
	private String carModel;

	/**
	 * 照片
	 */
	@ApiModelProperty(value = "照片")
	@Column(name = "car_photo")
	private String carPhoto;

	/**
     * 创建时间
	 */
	@ApiModelProperty(hidden = true)
	@Column(name = "create_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

}
