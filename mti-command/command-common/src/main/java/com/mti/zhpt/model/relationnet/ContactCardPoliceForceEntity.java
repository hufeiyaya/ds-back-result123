package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 卡点数据
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "t_contact_card_police_force")
@AllArgsConstructor
@NoArgsConstructor
public class ContactCardPoliceForceEntity extends BaseEntity {
    private static final long serialVersionUID = 7860463014938343389L;
    @Column(name = "unit_name")
    private String unitName;
    @Column(name = "card_name")
    private String cardName;
    @Column(name = "location_")
    private String location_;
    @Column(name = "responsible_unit")
    private String responsibleUnit;
    @Column(name = "distance_")
    private String distance_;
    @Column(name = "type_")
    private String type_;
    @Column(name = "principal_")
    private String principal_;
    @Column(name = "employees_number")
    private String employeesNumber;
    @Column(name = "carrying_equipment")
    private String carryingEquipment;
    @Column(name = "arrival_time")
    private String arrivalTime;
    @Column(name = "phone_radio")
    private String phoneRadio;
    @Column(name = "phone_")
    private String phone_;
    @Column(name = "line_type")
    private String lineType;

}
