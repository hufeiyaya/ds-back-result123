package com.mti.zhpt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe 指挥总结表
 **/
@Data
@ApiModel(description = "指挥总结表")
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "cp_command_summary")
public class CpCommandSummaryEntity extends CpCommandBaseEntity {
    private static final long serialVersionUID = -896140229729807539L;

    @ApiModelProperty(value = "警情描述")
    @Column(name = "description")
    private String description;

    @ApiModelProperty(value = "处置要点")
    @Column(name = "deal_point")
    private String dealPoint;

    @ApiModelProperty(value = "经验总结")
    @Column(name = "experence_all")
    private String experenceAll;

    @ApiModelProperty(value = "其他")
    @Column(name = "other")
    private String other;
}
