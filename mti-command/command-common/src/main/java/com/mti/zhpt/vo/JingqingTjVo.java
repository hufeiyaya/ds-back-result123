package com.mti.zhpt.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "警情统计对象", description = "警情统计对象")
public class JingqingTjVo {

    @ApiModelProperty(value = "编号")
    private String id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "总数")
    private String count;

    @ApiModelProperty(value = "案由统计")
    private List<AnyouTjVo> anyou;

}
