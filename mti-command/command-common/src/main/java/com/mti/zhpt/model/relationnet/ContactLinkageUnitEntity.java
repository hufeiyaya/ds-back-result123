package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
     * @describe 联动单位
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_contact_linkage_unit")
public class ContactLinkageUnitEntity extends BaseEntity {
    private static final long serialVersionUID = -7325088065599199677L;
    @Column(name = "unit_name")
    private String unitName;
    @Column(name = "concat_name")
    private String concatName;
    @Column(name = "concat_phone")
    private String concatPhone;
    @Column(name = "duty_call_one")
    private String dutyCallOne;
    @Column(name = "duty_call_two")
    private String dutyCallTwo;
    @Column(name = "fax_number")
    private String faxNumber;
    @Column(name = "duty_call_night")
    private String dutyCallNight;
    @Column(name = "duty_call_inline")
    private String dutyCallInline;
    @Column(name = "hotline_")
    private String hotline_;
    @Column(name = "remark_")
    private String remark_;

}
