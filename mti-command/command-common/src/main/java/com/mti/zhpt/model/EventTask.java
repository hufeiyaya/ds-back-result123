package com.mti.zhpt.model;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author yingjj@mti-sh.cn 2019年5月8日10:51
 *
 */
@Getter
@Setter
@ApiModel(value = "EventTask对象", description = "案件指令")
public class EventTask {

	@ApiModelProperty(value = "案件ID", name = "event_id", example = "6", required = true)
	private String event_id;

	@ApiModelProperty(value = "阶段ID(预案添加自定义内容时需提供)", name = "stage_id", example = "2", required = false)
	private int stage_id;

	@ApiModelProperty(value = "阶段名称(预案添加自定义内容时需提供)", name = "stage_type", example = "处突", required = false)
	private String stage_type;

	@ApiModelProperty(value = "命令详情", name = "remark", example = "安排狙击手在厕所蹲点", required = true)
	private String remark;

	@ApiModelProperty(value = "出动人数", name = "dispatch_number", example = "50", required = true)
	private Integer dispatch_number;

	@ApiModelProperty(value = "备勤人数", name = "reserve_number", example = "20", required = true)
	private Integer reserve_number;

	@ApiModelProperty(value = "接收单位ID", name = "org_id", example = "16", required = true)
	private String org_id;
	
	@ApiModelProperty(value = "接收单位名称", name = "org_name", example = "XXX派出所", required = false, hidden = true)
	private String org_name;

	/**
	 * 周边单位-是否地点最近(0.否 1.是)
	 */
	@ApiModelProperty(value = "周边单位-是否地点最近(0.否 1.是)", dataType = "Integer")
	private Integer nearbyUnitLocationClosest;

	/**
	 * 周边单位-是否辖区最近(0.否 1.是)
	 */
	@ApiModelProperty(value = "周边单位-是否辖区最近(0.否 1.是)", dataType = "Integer")
	private Integer nearbyUnitAreaRecently;

	@ApiModelProperty(value = "单位纬度", name = "org_longitude", example = "121.12345", required = false, hidden = true)
	private double org_longitude;
	
	@ApiModelProperty(value = "单位经度", name = "org_latitude", example = "21.23456", required = false, hidden = true)
	private double org_latitude;

	@ApiModelProperty(value = "指令ID(预案执行时需提供)", name = "id", example = "1", required = false, hidden = false)
	private String id;

	@ApiModelProperty(value = "纬度(设置集结点的话需要提供)", name = "longitude", example = "121.12345", required = false, hidden = false)
	private double longitude;

	@ApiModelProperty(value = "经度(设置集结点的话需要提供)", name = "latitude", example = "21.23456", required = false, hidden = false)
	private double latitude;

	@ApiModelProperty(value = "变为指令的时间", name = "doTime", example = "2018-05-09 22:10:23", required = false, hidden = true)
	private Date doTime;

	@ApiModelProperty(value = "对应处置时间轴ID", name = "timer_id", example = "88", required = false, hidden = true)
	private int timer_id;
	
	@ApiModelProperty(value = "消息类型", name = "msgType", example = "1", required = false, hidden = true)
	private int msgType;

	/**
	 * 指令类型：1.人员指令 2.单位指令
	 */
	@ApiModelProperty(value = "指令类型", dataType = "Integer", notes = "1.人员指令 2.单位指令", example = "1")
	private Integer instructionType;

	/**
	 * 到场时限（单位：分钟）
	 */
	@ApiModelProperty(value = "到场时限（单位：分钟）", dataType = "String")
	private String timePresence;

	/**
	 * 警员id
	 */
	@ApiModelProperty(value = "警员id", dataType = "String")
	private String policeId;
	/**
	 * 接收警员
	 */
	@ApiModelProperty(value = "警员id", dataType = "String")
	private String policeName;

	/**
	 * 警员所在结构id
	 */
	@ApiModelProperty(value = "警员所在结构id", dataType = "String")
	private String policeOrgId;

	/**
	 * 周边警员（多少米范围内警员）
	 */
	@ApiModelProperty(value = "周边警员（多少米范围内警员）", dataType = "String")
	private String surroundingPoliceOfficer;

	/**
	 * 领导id
	 */
	@ApiModelProperty(value = "领导id", dataType = "String")
	private String leaderId;

	/**
	 * 领导所在机构id
	 */
	@ApiModelProperty(value = "领导所在机构id", dataType = "String")
	private String leaderOrgId;

	@ApiModelProperty(value = "指令单状态：10已下达、20已到达、30已接收、40已出动、50已到场、60已反馈、70已作废", dataType = "Integer")
	private Integer status;

	/**
	 * 反馈标题
	 */
	@ApiModelProperty(value = "反馈标题", dataType = "String")
	private String feedback;




	/**
	 * 下达警察名称
	 */
	@ApiModelProperty(value = "警察名称", dataType = "Stirng")
	private String sendPolice;

	/**
	 * 下达警察code
	 */
	@ApiModelProperty(value = "警察code", dataType = "Stirng")
	private String sendPoliceId;


	/**
	 * 下达单位
	 */
	@ApiModelProperty(value = "下达单位", dataType = "Stirng")
	private String sendOrg;

	/**
	 * 下达单位ID
	 */
	@ApiModelProperty(value = "下达单位ID", dataType = "Stirng")
	private String sendOrgId;





}