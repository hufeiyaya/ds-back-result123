package com.mti.zhpt.enums;

import lombok.AllArgsConstructor;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/6
 * @change
 * @describe describe
 **/
@AllArgsConstructor
public enum RelationNetTypeEnum {
    ContactAroundRelationEntity("市区周边通讯录","ContactAroundRelationEntity"),
    ContactBTHProvincialDepartmentEntity("京津冀","ContactBTHProvincialDepartmentEntity"),
    ContactCardPoliceForceEntity("卡点数据","ContactCardPoliceForceEntity"),
    ContactCityBureauUnitDutyPhoneEntity("市局单位值班电话","ContactCityBureauUnitDutyPhoneEntity"),
    ContactCityBureauUnitMemberEntity("市局机关单位成员","ContactCityBureauUnitMemberEntity"),
    ContactDistrictLeadersEntity("领导班子","ContactDistrictLeadersEntity"),
    ContactDutyCallEntity("各县区指挥中心值班电话","ContactDutyCallEntity"),
    ContactDutyRelationEntity("各县区指挥中心通讯录","ContactDutyRelationEntity"),
    ContactFourCheckPointsEntity("四个检查站","ContactFourCheckPointsEntity"),
    ContactHebeiCommandCenterEntity("河北各地市指挥中心","ContactHebeiCommandCenterEntity"),
    ContactHignTrafficPoliceEntity("高速交警","ContactHignTrafficPoliceEntity"),
    ContactImportDistrictsEntity("各区县重大紧急联系人","ContactImportDistrictsEntity"),
    ContactLinkageUnitEntity("联动单位","ContactLinkageUnitEntity"),
    ContactNatinalSecurityEntity("全国公安机关","ContactNatinalSecurityEntity"),
    ContactMunicipalGovermentEntity("市委市政府","ContactMunicipalGovermentEntity"),
    ContactPoliceRoomEntity("警务室巡区","ContactPoliceRoomEntity"),
    ContactRadioPhoneDispatchEntity("350M电台、电话调度","ContactRadioPhoneDispatchEntity"),
    ContactCityTrafficDeployEntity("市区交警部署","ContactCityTrafficDeployEntity");
    private String name;
    private String code;

    public static String getNameByCode(String code){
        switch (code){
            case "ContactAroundRelationEntity":return "市区周边通讯录";
            case "ContactBTHProvincialDepartmentEntity":return "京津冀";
            case "ContactCardPoliceForceEntity":return "卡点数据";
            case "ContactCityBureauUnitDutyPhoneEntity":return "市局单位值班电话";
            case "ContactCityBureauUnitMemberEntity":return "市局机关单位成员";
            case "ContactDistrictLeadersEntity":return "领导班子";
            case "ContactDutyCallEntity":return "各县区指挥中心值班电话";
            case "ContactDutyRelationEntity":return "各县区指挥中心通讯录";
            case "ContactFourCheckPointsEntity":return "四个检查站";
            case "ContactHebeiCommandCenterEntity":return "河北各地市指挥中心";
            case "ContactHignTrafficPoliceEntity":return "高速交警";
            case "ContactImportDistrictsEntity":return "各区县重大紧急联系人";
            case "ContactLinkageUnitEntity":return "联动单位";
            case "ContactNatinalSecurityEntity":return "全国公安机关";
            case "ContactMunicipalGovermentEntity":return "市委市政府";
            case "ContactPoliceRoomEntity":return "警务室巡区";
            case "ContactRadioPhoneDispatchEntity":return "350M电台、电话调度";
            case "ContactCityTrafficDeployEntity":return "市区交警部署";
        }
        return "";
    }

    public static String getCodeByName(String name){
        switch (name){
            case "市区周边通讯录":return "ContactAroundRelationEntity";
            case "京津冀":return "ContactBTHProvincialDepartmentEntity";
            case "卡点数据":return "ContactCardPoliceForceEntity";
            case "市局单位值班电话":return "ContactCityBureauUnitDutyPhoneEntity";
            case "市局机关单位成员":return "ContactCityBureauUnitMemberEntity";
            case "领导班子":return "ContactDistrictLeadersEntity";
            case "各县区指挥中心值班电话":return "ContactDutyCallEntity";
            case "各县区指挥中心通讯录":return "ContactDutyRelationEntity";
            case "四个检查站":return "ContactFourCheckPointsEntity";
            case "河北各地市指挥中心":return "ContactHebeiCommandCenterEntity";
            case "高速交警":return "ContactHignTrafficPoliceEntity";
            case "各区县重大紧急联系人":return "ContactImportDistrictsEntity";
            case "联动单位":return "ContactLinkageUnitEntity";
            case "市委市政府":return "ContactMunicipalGovermentEntity";
            default: return "";
        }
    }
}
