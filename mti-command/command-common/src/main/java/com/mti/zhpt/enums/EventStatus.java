package com.mti.zhpt.enums;

/**
 * @author hsy
 *  事件单状态枚举
 */
public enum EventStatus {
    Receiving("0","未处置"),
    Received("1","处置中"),
    Dispatching("2", "已处置"),
    sendBack("3", "已退回");


    private String code;
    private String name;

    EventStatus(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getName(String code) {
        for (EventStatus a : EventStatus.values()) {
            if (code.equals(a.getCode())) {
                return a.name;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
