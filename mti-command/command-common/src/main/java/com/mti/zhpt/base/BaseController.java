package com.mti.zhpt.base;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

/**
 * 
 * @classDesc: 功能描述:(控制器类父类)
 * @author 徐宝振
 * @createTime: 2018年8月3日 下午2:15:40
 * @version: v1.0
 * @copyright:上海道枢信息技术有限公司
 */
public class BaseController {

	/**
	 * 
	 * @methodDesc: 功能描述:(使用valid注解进行字段校验)
	 * @author: Wu,Hua-Zheng
	 * @param: bindingResult
	 * @createTime:2018年3月13日 下午2:37:32
	 * @returnType:String
	 * @copyright:上海道枢信息技术有限公司
	 */
	protected String valid(BindingResult bindingResult) {
		StringBuilder errors = new StringBuilder();
		if (bindingResult.hasErrors()) {
			if (bindingResult.getErrorCount() > 0) {
				for (FieldError error : bindingResult.getFieldErrors()) {
					if (StringUtils.isNoneBlank(errors)) {
						errors.append(",");
					}
					errors.append(error.getField()).append(":").append(error.getDefaultMessage());
				}
			}
		}
		return errors.toString();
	}

	/**
	 * 
	 * @methodDesc: 功能描述:(获取项目根目录地址)
	 * @author: Wu,Hua-Zheng
	 * @createTime:2017年7月20日 下午8:05:28
	 * @returnType:String
	 * @copyright:上海道枢信息技术有限公司
	 */
	protected String getRootUrl(HttpServletRequest req) {
		StringBuffer url = req.getRequestURL();
		String contextPath = req.getContextPath();
		return url.substring(0, url.indexOf(contextPath) + contextPath.length());
	}
}
