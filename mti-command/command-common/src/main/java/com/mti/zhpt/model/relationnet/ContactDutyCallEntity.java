package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 各县区指挥中心值班电话
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "t_contact_duty_call")
@AllArgsConstructor
@NoArgsConstructor
public class ContactDutyCallEntity extends BaseEntity {
    private static final long serialVersionUID = -9177230277797281532L;
    @Column(name = "org_name")
    private String orgName;
    @Column(name = "duty_call_sun")
    private String dutyCallSun;
    @Column(name = "fax_number")
    private String faxNumber;
    @Column(name = "duty_call_night")
    private String dutyCallNight;
}
