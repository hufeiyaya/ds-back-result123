package com.mti.zhpt.model;

import lombok.Data;

@Data
public class UsersEntity extends BaseEntity {
    private Integer id;
    private String name;
    private String password;

}
