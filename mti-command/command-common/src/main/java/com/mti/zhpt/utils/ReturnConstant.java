package com.mti.zhpt.utils;


public class ReturnConstant {

    public static class SUCCESS {
        public static final int value = 200;
        public static final String desc = "成功";
    }

    public static class NOT_FOUND {
        public static final int value = 404;
        public static final String desc = "请求的服务不存在";
    }

    public static class SERVICE_NOT_SUPPORT {
        public static final int value = 408;
        public static final String desc = "由于特殊原因，此服务已不支持";
    }

    public static class INTERNAL_SERVER_ERROR {
        public static final int value = 500;
        public static final String desc = "请求的服务产生异常";
    }

    public static class FAULT_ACCESS_TOKEN {
        public static final int value = 900;
        public static final String desc = "您没有权限访问该接口";
    }

    public static class NO_ACCESS_TOKEN {
        public static final int value = 901;
        public static final String desc = "缺少访问授权参数";
    }

    public static class EXPIRE_ACCESS_TOKEN {
        public static final int value = 902;
        public static final String desc = "您授权已到期";
    }

    public static class KEY_IN_USED {
        public static final int value = 903;
        public static final String desc = "该字典项已经被使用，不允许删除";
    }

    public static class JPUSH_COLLEGE_FAIL {
        public static final int value = 904;
        public static final String desc = "专版优圈推送异常";
    }

    public static class SMS_COUNT_LIMIT {
        public static final int value = 905;
        public static final String desc = "今天已到接受验证码次数上限（8次），请您明天再操作";
    }

    public static class BUSINESS_ERROR {
        public static final int value = 10001;
        public static final String desc = "业务逻辑异常";
    }

    public static class RONGYUN_ERROR {
        public static final int value = 10002;
    }

    // common : 10000
    public static class PARAMETER_EMPTY {
        public static final int value = 10003;
        public static final String desc = "参数不能为空";
    }

    public static class PARAMETER_INCORRECT {
        public static final int value = 10004;
        public static final String desc = "参数错误";
    }

    public static class CONFIG_KEY_REPEAT {
        public static final int value = 10005;
        public static final String desc = "字典项键重复";
    }

    public static class CONFIG_DETAIL_REPEAT {
        public static final int value = 10006;
        public static final String desc = "字典项值重复";
    }

    public static class NOT_ALLOW_PUBLISH {
        public static final int value = 10007;
        public static final String desc = "系统维护中，暂时不能使用！";
    }

    public static class BUSINESS_DISTRICTID_NOT_EXIST {
        public static final int value = 1000401;
        public static final String desc = "districtId不能为空";
    }


    public static class GLOBAL_CONFIG_USER_BEHAVIOR_VALID_ERROR {
        public static final int value = 1000403;
        public static final String desc = "用户有效操作记录时间不能大于60分钟";
    }

    public static class GLOBAL_CONFIG_USER_BEHAVIOR_INVALID_ERROR {
        public static final int value = 1000404;
        public static final String desc = "用户无效操作记录时间不能小于24小时";
    }

    public static class PAY_PREPAY_ID_NOT_FOUND {
        public static final int value = 1000405;
        public static final String desc = "预支付单获取失败";
    }

    public static class NUM_TOO_LANG {
        public static final int value = 1000406;
        public static final String desc = "编号太长";
    }
    // Other
    public static class UPLOAD_ERROR {
        public static final int value = 1002;
        public static final String desc = "文件上传异常";
    }

    public static class NULL_POINT_ERROR {
        public static final int value = 2001;
        public static final String desc = "空指针异常";
    }

    public static class SQL_ERROR {
        public static final int value = 2002;
        public static final String desc = "SQL执行异常";
    }

    public static class FILE_NOT_FOUND_ERROR {
        public static final int value = 2003;
        public static final String desc = "文件不存在";
    }

    public static class OTHER_ERROR {
        public static final int value = 9001;
        public static final String desc = "其他异常";
    }
    // Token : 10100
    public static class NO_TOKEN {
        public static final int value = 10101;
        public static final String desc = "缺少参数[tokenId]";
    }

    public static class FAULT_TOKEN {
        public static final int value = 10102;
        public static final String desc = "错误的Token";
    }

    public static class EXPIRE_TOKEN {
        public static final int value = 10103;
        public static final String desc = "Token已过期，请重新登录";
    }

    // Alarm 10200
    public static class ALARM_NOT_EXIST {
        public static final int value = 10201;
        public static final String desc = "警情不存在";
    }


}
