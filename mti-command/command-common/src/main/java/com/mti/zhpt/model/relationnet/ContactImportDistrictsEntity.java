package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 各区县重大紧急联系人
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_contact_import_districts")
public class ContactImportDistrictsEntity extends BaseEntity {
    private static final long serialVersionUID = 6427794148059093395L;
    /**
     * todo 这个不知道是序号还是id
     */
    @Column(name = "id_")
    private String id_;
    @Column(name = "unit_name")
    private String unitName;
    @Column(name = "person_type")
    private String personType;
    @Column(name = "person_name")
    private String personName;
    @Column(name = "position")
    private String position;
    @Column(name = "phone_")
    private String phone_;
    @Column(name = "telephone_")
    private String telephone_;
}
