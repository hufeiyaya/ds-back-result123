package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

/** 
* @Description: 预案指挥 实体
 *
 * cp_event_timer
 *
* @Author: zhaopf@mti-sh.cn
* @Date: 2019/4/22 17:07 
*/ 
@Data
public class EventReserveCommandEntity extends BaseEntity {
    private Integer commandId;
    /**
     * 预案事件id
     */
    private Integer reserveId;
    /**
     * 事件id
     */
    private Integer eventId;


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    /**
     * 指令标题
     */
    private String feedback;
    /**
     * 指令内容
     */
    private String remark;

    /**
     * 组织名称
     */
    private String deptName;
    /**
     * 组织code
     */
    private String deptCode;
    /**
     * 阶段类型
     */
    private String stageType;
    /**
     * 警察名字
     */
    private String policeName;
    /**
     * 警察code
     */
    private String policeCode;
    /**
     * 出动人数
     */
    private Integer dispatchNumber;
    /**
     * 储备人数
     */
    private Integer reserveNumber;







}
