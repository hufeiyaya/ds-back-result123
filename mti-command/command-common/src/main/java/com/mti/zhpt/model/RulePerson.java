package com.mti.zhpt.model;

import com.mti.zhpt.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Transient;
import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/8
 * @change
 * @describe describe
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "法言法语实体")
@AllArgsConstructor
@NoArgsConstructor
public class RulePerson extends BaseEntity {
    private static final long serialVersionUID = 2739499235361990923L;
    @ApiModelProperty(value = "标题")
    @Column(name = "title")
    private String title;
    @ApiModelProperty(value = "关键字")
    @Column(name = "key_word")
    private String keyWord;
    @ApiModelProperty(value = "类型")
    @Column(name = "type")
    private String type;
    @ApiModelProperty(value = "适用案由")
    @Column(name = "case_type")
    private String caseType;
    @ApiModelProperty(value = "内容")
    @Column(name = "content")
    private String content;
    @ApiModelProperty(value = "附件列表")
    @Transient
    List<FileEntity> files;
}
