package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mti.zhpt.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * <p>
 * 相关人，车，其他
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-29 14:05
 */
@Data
@Table(name = "cp_person_relation_info")
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "相关人，车，其他信息", description = "相关人，车，其他信息")
public class CpPersonRelation extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 姓名
	 */
	@ApiModelProperty(value = "姓名")
	@Column(name = "name")
	private String name;

	/**
	 * 种类
	 */
	@ApiModelProperty(value = "种类")
	@Column(name = "kind")
	private String kind;

	/**
	 * 关系
	 */
	@ApiModelProperty(value = "关系")
	@Column(name = "relation")
	private String relation;

	/**
	 * 等级
	 */
	@ApiModelProperty(value = "等级")
	@Column(name = "start")
	private String start;

	/**
	 * 类型：1.人 2.车 3.其他
	 */
	@ApiModelProperty(value = "类型：1.人 2.车 3.其他")
	@Column(name = "type")
	private Integer type;

	/**
	 * 身份证号
	 */
	@ApiModelProperty(value = "身份证号")
	@Column(name = "id_card")
	private String idCard;

	/**
	 * 手机号
	 */
	@ApiModelProperty(value = "手机号")
	@Column(name = "phone_")
	private String phone;

	/**
	 * 照片
	 */
	@ApiModelProperty(value = "照片")
	@Column(name = "photo_")
	private String photo;

	/**
	 * 关系人类型：1.报案人 2.嫌疑人 3.受害人 4.证人
	 */
	@ApiModelProperty(value = "关系人类型：1.报案人 2.嫌疑人 3.受害人 4.证人")
	@Column(name = "person_type")
	private Integer personType;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(hidden = true)
	@Column(name = "create_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	/**
	 * 标签
	 */
	@ApiModelProperty(value = "标签")
	@Column(name = "label_")
	private String label;

	/**
	 * 车牌号
	 */
	@ApiModelProperty(value = "车牌号")
	@Column(name = "car_number")
	private String carNumber;

	/**
	 * 车型
	 */
	@ApiModelProperty(value = "车型")
	@Column(name = "car_model")
	private String carModel;

	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注")
	@Column(name = "remark_")
	private String remark;

	/**
	 * 民族
	 */
	@ApiModelProperty(value = "民族")
	@Column(name = "nation_")
	private String nation;

	/**
	 * 性别
	 */
	@ApiModelProperty(value = "性别")
	@Column(name = "sex_")
	private String sex;

	/**
	 * 家庭住址
	 */
	@ApiModelProperty(value = "家庭住址")
	@Column(name = "address_")
	private String address;

	/**
	 * 人员id
	 */
//	@ApiModelProperty(value = "人员id")
//	@Transient
//	private String personId;

	/**
	 * 警情id
	 */
	@ApiModelProperty(value = "警情id")
	@Column(name = "event_id")
	private String eventId;

}
