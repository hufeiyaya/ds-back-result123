package com.mti.zhpt.model;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe 预案总结
 **/
@Data
@ApiModel(description = "预案总结")
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "cp_command_pre_event_summary")
public class CpCommandPreEventSummaryEntity extends CpCommandBaseEntity {
    private static final long serialVersionUID = 5426948461336191073L;
    @ApiModelProperty(value = "预案总结")
    @Column(name = "pre_event_summary")
    private String preEventSummary;
    @ApiModelProperty(value = "预案建议")
    @Column(name = "pre_event_suggestion")
    private String preEventSuggestion;
    @ApiModelProperty(value = "启动预案数")
    @Column(name = "start_pre_event")
    private String startPreEvent;
    @ApiModelProperty(value = "操作步骤数")
    @Column(name = "operation_step")
    private String operationStep;
    @ApiModelProperty(value = "处置项")
    @Column(name = "deal_item")
    private String dealItem;
}
