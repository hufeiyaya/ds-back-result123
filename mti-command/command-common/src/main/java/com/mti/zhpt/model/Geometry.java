package com.mti.zhpt.model;
import java.util.List;

import com.mti.zhpt.utils.GCJ02_WGS84;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
/**
 * @author yingjj@mti-sh.cn 2019年5月6日10:51
 *
 */
@Getter
@Setter
@ApiModel(value="Geometry对象",description="地理空间对象Geometry")
public class Geometry {
	@ApiModelProperty(value="类型",name="type",example="Polygon",required=true)
	private String type;
	@ApiModelProperty(value="坐标集合",name="coordinates",example="[[" +
			"[115.81304971112984, 40.0287699316198]," +
			"[115.81292686006391, 39.03283195163798]," +
			"[117.45584127183231, 39.03305864377007]," +
			"[117.45597263232362, 40.02899805754957]," +
			"[115.81304971112984, 40.0287699316198]" +
			"]]",required=true)
	private List<List<Double[]>> coordinates;
	
	public String GeomFromText() {
		StringBuffer temp=new StringBuffer(type.toUpperCase());
		temp.append("((");
		for(List<Double[]> i:coordinates) {
			for(Double[] j:i) {
//				LocateInfo locateInfo=GCJ02_WGS84.gcj02_To_Wgs84(j[1],j[0]);
//				temp.append(locateInfo.generateString()+",");
				String locateInfoStr = j[0]+" "+j[1];
				temp.append(locateInfoStr + ",");
			}
		}
		temp.deleteCharAt(temp.length()-1);
		temp.append("))");
		return temp.toString();
	}
}
