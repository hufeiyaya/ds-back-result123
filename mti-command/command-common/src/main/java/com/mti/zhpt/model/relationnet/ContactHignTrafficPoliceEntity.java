package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 高速交警
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "t_contact_hign_traffic_police")
@AllArgsConstructor
@NoArgsConstructor
public class ContactHignTrafficPoliceEntity extends BaseEntity {
    private static final long serialVersionUID = 6679776403591128208L;
    @Column(name = "traffic_name")
    private String trafficName;
    @Column(name = "phone_")
    private String phone_;
    @Column(name = "emergency_call")
    private String emergencyCall;
    @Column(name = "fax_number")
    private String faxNumber;
    @Column(name = "address_")
    private String address_;
    @Column(name = "postal_code")
    private String postalCode;
}
