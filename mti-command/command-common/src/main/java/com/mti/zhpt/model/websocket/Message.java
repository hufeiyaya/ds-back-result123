package com.mti.zhpt.model.websocket;

import lombok.Data;

@Data
public class Message {
    /** 消息编号*/
    private String code;
    /** 消息体 */
    private Object content;
}
