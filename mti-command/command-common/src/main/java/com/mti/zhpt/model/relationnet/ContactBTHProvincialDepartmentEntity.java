package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 京津冀
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "t_contact_bth_provincial_department")
@AllArgsConstructor
@NoArgsConstructor
public class ContactBTHProvincialDepartmentEntity extends BaseEntity {
    private static final long serialVersionUID = 2051976273911424730L;
    @Column(name = "provincial_office")
    private String provincialOffice;
    @Column(name = "dept_name")
    private String deptName;
    @Column(name = "class_")
    private String class_;
    @Column(name = "person_name")
    private String personName;
    @Column(name = "position_")
    private String position_;
    @Column(name = "private_line_telephone")
    private String privateLineTelephone;
    @Column(name = "outside_line_telephone")
    private String outsideLineTelephone;
    @Column(name = "telephone_")
    private String telephone_;
    @Column(name = "phone_one")
    private String phoneOne;
    @Column(name = "phone_two")
    private String phoneTwo;

}
