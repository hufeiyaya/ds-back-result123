package com.mti.zhpt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe 指挥总结报告
 **/
@Data
@ApiModel(value = "总结报告实体")
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "cp_command_summary_report")
public class CpCommandSummaryReportEntity extends CpCommandBaseEntity {
    private static final long serialVersionUID = 7147800902664750642L;
    @ApiModelProperty(value = "总结报告内容")
    @Column(name = "content")
    private String content;
    @ApiModelProperty(value = "文件ID")
    @Column(name = "file_id")
    private String fileId;
    @ApiModelProperty(value = "文件标题")
    @Column(name = "title")
    private String title;
    @ApiModelProperty(value = "创建时间")
    @Column(name = "date")
    private String date;
    @ApiModelProperty(value = "所属单位")
    @Column(name = "org_name")
    private String orgName;
}
