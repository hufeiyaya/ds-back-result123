package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.zhpt.utils.Constant;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 敬请-预案详情类
 * cp_event_reserve_detail
 * @createBy zhangmingxin
 */
@Data
public class EventSnapShotReserveDetailEntity extends EventReserveDetailEntity {
    private static final long serialVersionUID = 6797577691294107096L;
    /**
     * 警情ID
     */
    private String eventId;
    /**
     * 当前状态
     */
    private String status;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
    /**
     * 预案详情ID
     */
    private String reserveDetailId;
}
