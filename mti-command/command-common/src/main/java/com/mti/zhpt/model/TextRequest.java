package com.mti.zhpt.model;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author yingjj@mti-sh.cn 2019年5月6日10:51
 *
 */
@Getter
@Setter
@ApiModel(value="文字请示汇报实体",description="文字请示汇报实体对象")
public class TextRequest{
	
	@ApiModelProperty(value="请示汇报记录id(数据库自动生成)",required=false,hidden=true)
	private int id;

	@ApiModelProperty(value = "案件ID",  example = "1", required = true)
	private String eventid;

	@ApiModelProperty(value="汇报内容",example="领导您好，青浦区徐泾镇高泾路599号，有人聚众打架。",required=true)
	private String content;

	@ApiModelProperty(value = "汇报方式(--0 电话 1--短信 2--微信  3--其他)", name = "req_way", example = "2", required = true)
	private int req_way;

	@ApiModelProperty(value="批示内容",example="领导您好，青浦区徐泾镇高泾路599号，有人聚众打架。",required=true)
	private String instrucTcontent;

	@ApiModelProperty(value="请示汇报的领导列表",example="[1,2,3]",required=true)
	private List<String> leaders;
	private String leaderId;

	private Integer status;

	@ApiModelProperty(value="请示汇报的时间",required=false,hidden=true)
	private Date create_time=new Date();

}