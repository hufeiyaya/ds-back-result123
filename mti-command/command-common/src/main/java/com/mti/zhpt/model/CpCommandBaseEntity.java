package com.mti.zhpt.model;

import com.mti.zhpt.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe 指挥回溯基类
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpCommandBaseEntity extends BaseEntity {
    private static final long serialVersionUID = 9040336909663355311L;
    /**
     * 警情ID
     */
    @ApiModelProperty(value = "警情ID")
    @Column(name = "event_id")
    private String riskId;
}
