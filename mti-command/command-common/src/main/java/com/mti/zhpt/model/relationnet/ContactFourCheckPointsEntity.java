package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 四个检查站
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_contact_four_checkpoints")
public class ContactFourCheckPointsEntity extends BaseEntity {
    private static final long serialVersionUID = -4763203998298038402L;
    @Column(name = "qu_")
    private String qu_;
    @Column(name = "checkpoint_name")
    private String checkPointName;
    @Column(name = "outside_phone")
    private String outsidePhone;
    @Column(name = "call_sign")
    private String callSign;
    @Column(name = "fax_number")
    private String faxNumber;
    @Column(name = "call_number")
    private String callNumber;
    @Column(name = "leader_name")
    private String leaderName;
    @Column(name = "leader_phone")
    private String leaderPhone;
    @Column(name = "web_master")
    private String webMaster;
    @Column(name = "web_master_phone")
    private String webMasterPhone;
    @Column(name = "instructor_name")
    private String instructorName;
    @Column(name = "instructor_phone")
    private String instructorPhone;
    @Column(name = "deputy_chief")
    private String deputyChief;
    @Column(name = "deputy_chief_phone")
    private String deputyChiefPhone;
    @Column(name = "conference_contact_name")
    private String conferenceContactName;
    @Column(name = "conference_contact_phone")
    private String conferenceContactPhone;

}
