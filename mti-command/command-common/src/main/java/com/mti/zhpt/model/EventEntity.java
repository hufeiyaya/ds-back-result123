package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class EventEntity {
	@ApiModelProperty(hidden = true)
	private String eventId;

	@ApiModelProperty(value = "报警人姓名", dataType = "String")
	private String policeMan;

	@ApiModelProperty(value = "联系人电话", dataType = "String")
	private String phone;

	@ApiModelProperty(value = "案件类型", name = "caseType", example = "010500", required = true)
	private String caseType;

	@ApiModelProperty(value = "案件类型中文", name = "caseTypeVal", example = "010500", hidden = true)
	private String caseTypeVal;

	@ApiModelProperty(hidden = true)
	private String record;

	@ApiModelProperty(value = "管辖单位", dataType = "String")
	private String unit;

	@ApiModelProperty(value = "等级", dataType = "String", notes = "01.一级 02.二级 03.三级 04.四级", example = "1")
	private String level;

	@ApiModelProperty(value = "事件地址", name = "address", example = "XXXX路XXXX号", required = true, hidden = false)
	private String address;

	@ApiModelProperty(value = "事件描述", name = "content", example = "XXXX有人XXXX", required = true, hidden = false)
	private String content;

	@ApiModelProperty(hidden = true)
	private String risk;

	@ApiModelProperty(hidden = true)
	private Integer status;

	@ApiModelProperty(hidden = true)
	private Double longitude=0d;

	@ApiModelProperty(hidden = true)
	private Double latitude=0d;

	@ApiModelProperty(hidden = true)
	private String recordMp3Url;

	@ApiModelProperty(hidden = true)
	private Integer recordMp3Length;

	@ApiModelProperty(value = "案件类型集合", dataType = "List<String>")
	private List<String> caseTypeList;

	@ApiModelProperty(value = "案件发生时间", name = "createTime", example = "2019-05-01 12:33:34", required = true, hidden = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	@ApiModelProperty(hidden = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date startTime;
	@ApiModelProperty(hidden = true)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date endTime;

	/**
	 * 当前页码
	 */
	@ApiModelProperty(value = "当前页码", dataType = "Integer", example = "1")
	private Integer pageNum;

	/**
	 * 每页条数
	 */
	@ApiModelProperty(value = "每页条数", dataType = "Integer", example = "20")
	private Integer pageSize;

	/**
	 * 警情等级集合
	 */
	@ApiModelProperty(value = "警情等级集合", dataType = "List<String>")
	private List<String> levelList;

	/**
	 * 状态集合
	 */
	@ApiModelProperty(value = "状态集合", dataType = "List<Integer>")
	private List<Integer> statusList;

	/**
	 * 分局单位id
	 */
	@ApiModelProperty(value = "分局单位id", dataType = "String")
	private String branchUnitId;

	/**
	 * 分局单位
	 */
	@ApiModelProperty(value = "分局单位", dataType = "String")
	private String branchUnit;

	/**
	 * 管辖单位警员id
	 */
	@ApiModelProperty(value = "管辖单位警员id", dataType = "String")
	private String unitPoliceId;

	/**
	 * 管辖单位警员姓名
	 */
	@ApiModelProperty(value = "管辖单位警员姓名", dataType = "String")
	private String unitPoliceName;

	/**
	 * 所属组织id
	 */
	@ApiModelProperty(value = "所属组织id", dataType = "String")
	private String unitId;

	/**
	 * 案件标题
	 */
	@ApiModelProperty(value = "案件标题", dataType = "String")
	private String title;

	/**
	 * 是否测试类型预案: 0.否 1.是
	 */
	@ApiModelProperty(value = "是否测试类型预案: 0.否 1.是", dataType = "Integer")
	private Integer tested = 0;

	/**
	 * 是否日常事件: 0.否 1.是
	 */
	@ApiModelProperty(value = "是否日常事件: 0.否 1.是", dataType = "Integer")
	private Integer dailyEvent = 1;

	/**
	 * 是否重大事件: 0.否 1.是
	 */
	@ApiModelProperty(value = "是否重大事件: 0.否 1.是", dataType = "Integer")
	private Integer importantEvent = 0;

	/**
	 * 是否主动警情：0.否 1.是
	 */
	@ApiModelProperty(value = "是否主动警情：0.否 1.是", dataType = "Integer")
	private Integer initiatived = 0;

	/**
	 * 排序列
	 */
	@ApiModelProperty(hidden = true)
	private String orderField;

	/**
	 * 排序规则，升降序
	 */
	@ApiModelProperty(hidden = true)
	private String orderType;
	@ApiModelProperty(value = "事件类型(0：110警情，1：重点人员，2：在逃人员)")
	private String eventType;

	private String sfzh;

	private String warningType;

	private String personType;

	private String tx;

	@ApiModelProperty(value = "分局单位id List", dataType = "List<String>")
	private List<String> unitIdList;
	@ApiModelProperty(value = "警员id List", dataType = "List<String>")
	private List<String> unitPoliceIdList;
	@ApiModelProperty(value = "派出所id List", dataType = "List<String>")
	private List<String> branchUnitIdList;

	@ApiModelProperty(value = "下发时间", name = "sendTime", example = "2019-05-01 12:33:34", required = true, hidden = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date sendTime;


	/**
	 * 入参：
	 * 	当前登录人组织id
	 */
	@ApiModelProperty(value = "当前登录人组织id", example = "131099000000", required = true)
	private String deptCode;
	/**
	 * 登录人数据权限级别
	 * 市局：1
	 * 区县局：2
	 * 派出所：3
	 */
	@ApiModelProperty(value = "登录人数据权限级别:市局：1/区县局：2/派出所：3", example = "2", required = true)
	private String deptLevel;

	@ApiModelProperty(value = "责任部门")
	private String zrbm;
}
