package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/** 
 * <p>
 * 预案指挥 实体
 * </p>
 *
 * cp_reserve
 *
* @author zhaichen
* @since 2019-06-19 20:14
*/ 
@Data
@ApiModel(value = "预案信息对象", description = "预案信息")
public class EventReserveEntity {

    @ApiModelProperty(value = "案件id", dataType = "String")
    private String reserveId;

    /**
     * 预案标题
     */
    @ApiModelProperty(value = "预案标题", dataType = "String")
    private String reserveName;

    /**
     * 事件类型
     */
    @ApiModelProperty(value = "事件类型", dataType = "String")
    private String caseType;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", dataType = "Date", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    /**
     * 更改时间
     */
    @ApiModelProperty(value = "更改时间", dataType = "Date", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;

    /**
     * 关键字
     */
    @ApiModelProperty(value = "关键字", dataType = "String")
    private String keyword;

    /**
     * 适用等级:01.一级 02.二级 03.三级 04.四级
     */
    @ApiModelProperty(value = "适用等级:01.一级 02.二级 03.三级 04.四级", dataType = "String", notes = "01.一级 02.二级 03.三级 04.四级", example = "01")
    private String applicableGrade;

    /**
     * 预案类型：1.通用类型 2.专项类型
     */
    @ApiModelProperty(value = "预案类型:1.通用类型 2.专项类型 3.重点单位预案", dataType = "Integer", notes = "1.通用类型 2.专项类型 3.重点单位预案", example = "1")
    private Integer planType;

    @ApiModelProperty(value = "编制单位")
    private String dept;

    /**
     * 状态：0.禁用 1.启用
     */
    @ApiModelProperty(value = "状态:0.禁用 1.启用", dataType = "Integer", notes = "0.禁用 1.启用", example = "1")
    private Integer enable;

    /**
     * 附件
     */
    @ApiModelProperty(value = "附件", dataType = "String")
    private String file;

    /**
     * 附件
     */
    @ApiModelProperty(value = "附件文件名", dataType = "String")
    private String name;

    @ApiModelProperty(value = "是否启动")
    private long started;

    @ApiModelProperty(value = "创建人所属单位ID")
    private String creatorDeptId;

}
