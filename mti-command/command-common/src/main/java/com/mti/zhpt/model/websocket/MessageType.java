package com.mti.zhpt.model.websocket;

public enum MessageType {
    ALARM,
    ALARM_DELAY,
    OTHERS;
}
