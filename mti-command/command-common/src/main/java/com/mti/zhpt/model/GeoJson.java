package com.mti.zhpt.model;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author yingjj@mti-sh.cn 2019年5月6日10:51
 *
 */
@Getter
@Setter
@ApiModel(value="GeoJson对象",description="地理空间对象GeoJson")
public class GeoJson{
	
	@ApiModelProperty(value="类型",name="type",example="Feature",required=true)
	private String type;
	private Geometry geometry;
	@ApiModelProperty(value="附加信息",name="properties",example="{'pageid':1,'pagesize':10}",required=true)
	private Map<String,Object> properties;
	
	
	

}