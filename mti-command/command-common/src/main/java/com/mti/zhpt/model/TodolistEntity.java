package com.mti.zhpt.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * <p>
 * 代办事项信息对象
 * </p>
 *
 * @author yangsp@mti-sh.cn
 * @since 2019-06-23 9:35
 */
@Data
@ApiModel(value = "代办事项信息对象", description = "代办事项信息对象", parent = BaseEntity.class)
public class TodolistEntity extends BaseEntity {

    @ApiModelProperty(value = "主键ID", dataType = "String")
    private String id;

    /**
     * 标题
     */
    @ApiModelProperty(value = "标题", dataType = "String")
    private String title;

    /**
     * 类型
     */
    @ApiModelProperty(value = "类型", dataType = "String", notes = "任务,通知", example = "任务")
    private String type;

    /**
     * 内容
     */
    @ApiModelProperty(value = "内容", dataType = "String")
    private String content;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", name = "createTime", example = "2019-06-23 12:33:34", required = true, hidden = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    /**
     * 更改时间
     */
    @ApiModelProperty(value = "更改时间", name = "createTime", example = "2019-06-23 12:33:34", hidden = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;

    /**
     * 当前页码
     */
    @ApiModelProperty(value = "当前页码", dataType = "Integer", example = "1")
    private Integer pageNum;

    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页条数", dataType = "Integer", example = "20")
    private Integer pageSize;

}
