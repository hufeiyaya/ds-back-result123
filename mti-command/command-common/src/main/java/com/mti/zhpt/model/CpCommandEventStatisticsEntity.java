package com.mti.zhpt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/20
 * @change
 * @describe 事件统计
 **/
@Data
@ApiModel(description = "事件统计")
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "cp_command_event_statistics")
public class CpCommandEventStatisticsEntity extends CpCommandBaseEntity {
    private static final long serialVersionUID = -3459971303770858227L;
    @ApiModelProperty(value = "死亡人数")
    @Column(name = "dead_count")
    private String deadCount;
    @ApiModelProperty(value = "受伤人数")
    @Column(name = "hurt_count")
    private String hurtCount;
    @ApiModelProperty(value = "失踪人数")
    @Column(name = "lose_count")
    private String loseCount;
    @ApiModelProperty(value = "求助人数")
    @Column(name = "help_count")
    private String helpCount;
    @ApiModelProperty(value = "转移人数")
    @Column(name = "remove_count")
    private String removeCount;
    @ApiModelProperty(value = "抓捕人数")
    @Column(name = "catch_count")
    private String catchCount;
    @ApiModelProperty(value = "警员死亡人数")
    @Column(name = "police_dead_count")
    private String policeDeadCount;
    @ApiModelProperty(value = "警员受伤人数")
    @Column(name = "police_hurt_count")
    private String policeHurtCount;
    @ApiModelProperty(value = "涉案金额")
    @Column(name = "about_num")
    private String aboutNum;
    @ApiModelProperty(value = "直接经济损失")
    @Column(name = "direct_lose_money")
    private String directLoseMoney;
    @ApiModelProperty(value = "经济损失")
    @Column(name = "total_lose_money")
    private String totalLoseMoney;
    @ApiModelProperty(value = "挽回损失")
    @Column(name = "hold_money")
    private String holdMoney;
    @ApiModelProperty(value = "影响范围")
    @Column(name = "affect_range")
    private String affectRange;
    @ApiModelProperty(value = "其他")
    @Column(name = "other")
    private String other;
    @ApiModelProperty(value = "参战单位")
    @Column(name = "operate_org")
    private String operateOrg;
    @ApiModelProperty(value = "出动车次")
    @Column(name = "outbound_car")
    private String outboundCar;
    @ApiModelProperty(value = "出动人次")
    @Column(name = "outbound_person")
    private String outboundPerson;
    @ApiModelProperty(value = "下达指令")
    @Column(name = "command_num")
    private String commandNum;
    @ApiModelProperty(value = "指令反馈")
    @Column(name = "command_back_num")
    private String commandBackNum;
    @ApiModelProperty(value = "赋能信息")
    @Column(name = "with_power_num")
    private String withPowerNum;

}
