package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 市委市政府
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "t_contact_municipal_government")
@AllArgsConstructor
@NoArgsConstructor
public class ContactMunicipalGovermentEntity extends BaseEntity {
    private static final long serialVersionUID = 820251789690497131L;
    @Column(name = "id_")
    private String id_;
    @Column(name = "org_name")
    private String orgName;
    @Column(name = "dept_name")
    private String deptName;
    @Column(name = "person_name")
    private String personName;
    @Column(name = "phone_one")
    private String phoneOne;
    @Column(name = "phone_two")
    private String phoneTwo;
    @Column(name = "fax_number")
    private String faxNumber;
    @Column(name = "telphone_")
    private String telphone_;
}
