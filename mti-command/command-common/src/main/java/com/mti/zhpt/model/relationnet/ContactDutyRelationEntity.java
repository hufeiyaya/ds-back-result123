package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 各县区指挥中心通讯录
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_contact_duty_relation")
public class ContactDutyRelationEntity extends BaseEntity {
    private static final long serialVersionUID = 8983641831424275722L;
    @Column(name = "org_name")
    private String orgName;
    @Column(name = "person_name")
    private String personName;
    @Column(name = "position_")
    private String position_;
    @Column(name = "office_in_line_phone")
    private String officeInLinePhone;
    @Column(name = "office_out_line_phone")
    private String officeOutLinePhone;
    @Column(name = "telephone_")
    private String telephone;
    @Column(name = "charge_work")
    private String chargeWork;
    @Column(name = "residence_phone")
    private String residencePhone;
    @Column(name = "room_")
    private String room_;
}
