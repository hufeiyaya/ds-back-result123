package com.mti.zhpt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * <p>
 * 职能部门
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-06
 */
@Data
@Table(name = "t_contact_duty_call")
@ApiModel(value = "警令对象", description = "警令对象")
public class ContactDutyCall {

	private static final long serialVersionUID = 1L;

	/**
	 * 县区局
	 */
	@ApiModelProperty(value = "县区局")
	@Column(name = "org_name")
	private String orgName;

	/**
	 * 值班电话（日）
	 */
	@ApiModelProperty(value = "值班电话（日）")
	@Column(name = "duty_call_sun")
	private String dutyCallSun;

	/**
	 * 传真
	 */
	@ApiModelProperty(value = "传真")
	@Column(name = "fax_number")
	private String faxNumber;

	/**
	 * 值班电话（夜）
	 */
	@ApiModelProperty(value = "值班电话（夜）")
	@Column(name = "duty_call_night")
	private String dutyCallNight;

}
