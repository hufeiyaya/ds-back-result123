package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/5
 * @change
 * @describe 市局单位值班电话
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_contact_city_bureau_unit_duty_phone")
public class ContactCityBureauUnitDutyPhoneEntity extends BaseEntity {
    private static final long serialVersionUID = 3981798090849802092L;
    @Column(name = "unit_name")
    private String unitName;
    @Column(name = "office_phone")
    private String officePhone;
    @Column(name = "duty_phone")
    private String dutyPhone;
}
