package com.mti.zhpt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * <p>
 * 警令对象
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-06
 */
@Data
@Table(name = "t_contact_duty_relation")
@ApiModel(value = "警令对象", description = "警令对象")
public class ContactDutyRelation {

	private static final long serialVersionUID = 1L;

	/**
	 * 县区局
	 */
	@ApiModelProperty(value = "县区局")
	@Column(name = "org_name")
	private String orgName;

	/**
	 * 姓名
	 */
	@ApiModelProperty(value = "姓名")
	@Column(name = "person_name")
	private String personName;

	/**
	 * 职务
	 */
	@ApiModelProperty(value = "职务")
	@Column(name = "position_")
	private String position;

	/**
	 * 办公内线
	 */
	@ApiModelProperty(value = "办公内线")
	@Column(name = "office_in_line_phone")
	private String officeInLinePhone;

	/**
	 * 办公外线
	 */
	@ApiModelProperty(value = "办公外线")
	@Column(name = "office_out_line_phone")
	private String officeOutLinePhone;

	/**
	 * 手机号码
	 */
	@ApiModelProperty(value = "手机号码")
	@Column(name = "telephone_")
	private String telephone;

	/**
	 * 分管工作
	 */
	@ApiModelProperty(value = "分管工作")
	@Column(name = "charge_work")
	private String chargeWork;

	/**
	 * 住宅电话
	 */
	@ApiModelProperty(value = "住宅电话")
	@Column(name = "residence_phone")
	private String residencePhone;

	/**
	 * 房间
	 */
	@ApiModelProperty(value = "房间")
	@Column(name = "room_")
	private String room;

}
