package com.mti.zhpt.model.websocket;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
@Data
public class WSCustomEntity implements Serializable {
    /**
     * 命令id
     */
    private Integer timerId;

    /**
     * 事件id
     */
    private String eventId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    /**
     * 指令标题
     */
    private String feedback;
    /**
     * 指令内容
     */
    private String remark;
    /**
     * 组织名称
     */
    private String deptName;
    /**
     * 组织code
     */
    private String deptCode;
    /**
     * 警察名字
     */
    private String policeName;
    /**
     * 警察code
     */
    private String policeCode;
    /**
     * 出动人数
     */
    private Integer dispatchNumber;
    /**
     * 储备人数
     */
    private Integer reserveNumber;
    /**
     * 阶段类型
     */
    private String stageType;
    /**
     * 事件状态
     *
     */
    private Integer status;

}
