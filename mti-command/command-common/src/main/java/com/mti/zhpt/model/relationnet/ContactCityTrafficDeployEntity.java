package com.mti.zhpt.model.relationnet;

import com.mti.zhpt.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by jingzhanwu
 * @date 2019/8/7
 * @change
 * @describe describe
 **/
@Data
@Table(name = "t_contact_city_traffic_deploy")
@AllArgsConstructor
@NoArgsConstructor
public class ContactCityTrafficDeployEntity extends BaseEntity {
    private static final long serialVersionUID = 5139076245635183704L;
    @Column(name = "serial_no")
    private String serialNo;
    @Column(name = "police_area_name")
    private String policeAreaName;
    @Column(name = "middle_person_name")
    private String middlePersonName;
    @Column(name = "telephone_")
    private String telephone_;
    @Column(name = "police_count")
    private String policeCount;
    @Column(name = "police_car_count")
    private String policeCarCount;
    @Column(name = "car_ciper")
    private String carCiper;
    @Column(name = "control_range")
    private String controlRange;
    @Column(name = "leader_name")
    private String leaderName;
    @Column(name = "leader_phone")
    private String leaderPhone;
    @Column(name = "location_")
    private String location_;
}
