package com.mti.zhpt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;;

/** 
 * <p>
 * 预案指挥详情对象
 * </p>
 *
 * cp_reserve_detail
 *
* @author zhaichen
* @since 2019-06-20 9:35
*/ 
@Data
@ApiModel(value = "预案指挥详情信息对象", description = "预案指挥详情信息对象", parent = BaseEntity.class)
public class EventReserveDetailEntity extends BaseEntity {

    @ApiModelProperty(value = "主键ID", dataType = "String")
    private String id;

    /**
     * 预案事件id
     */
    @ApiModelProperty(value = "预案事件id", dataType = "String")
    private String reserveId;

    /**
     * 步骤名称(阶段类型)
     */
    @ApiModelProperty(value = "步骤名称(阶段类型)", dataType = "String")
    private String stageType;

    /**
     * 指令内容
     */
    @ApiModelProperty(value = "指令内容", dataType = "String")
    private String remark;

    /**
     * 出动人数
     */
    @ApiModelProperty(value = "出动人数", dataType = "Integer")
    private Integer dispatchNumber;

    /**
     * 备勤人数
     */
    @ApiModelProperty(value = "备勤人数", dataType = "Integer")
    private Integer reserveNumber;

    /**
     * 机构id
     */
    @ApiModelProperty(value = "机构id", dataType = "String")
    private String orgId;

    /**
     * 周边单位-是否地点最近(0.否 1.是)
     */
    @ApiModelProperty(value = "周边单位-是否地点最近(0.否 1.是)", dataType = "Integer")
    private String nearbyUnitLocationClosest;

    /**
     * 周边单位-是否辖区最近(0.否 1.是)
     */
    @ApiModelProperty(value = "周边单位-是否辖区最近(0.否 1.是)", dataType = "Integer")
    private Integer nearbyUnitAreaRecently;

    /**
     * 展示顺序
     */
    @ApiModelProperty(value = "展示顺序", dataType = "Integer")
    private Integer stageId;

    /**
     * 指令类型：1.人员指令 2.单位指令 3.汇报领导
     */
    @ApiModelProperty(value = "指令类型", dataType = "Integer", notes = "1.人员指令 2.单位指令 3.汇报领导", example = "1")
    private Integer instructionType;

    /**
     * 到场时限（单位：分钟）
     */
    @ApiModelProperty(value = "到场时限（单位：分钟）", dataType = "String")
    private String timePresence;

    /**
     * 警员id
     */
    @ApiModelProperty(value = "警员id", dataType = "String")
    private String policeId;

    /**
     * 警员所在结构id
     */
    @ApiModelProperty(value = "警员所在结构id", dataType = "String")
    private String policeOrgId;

    /**
     * 周边警员（多少米范围内警员）
     */
    @ApiModelProperty(value = "周边警员（多少米范围内警员）", dataType = "String")
    private String surroundingPoliceOfficer;

    /**
     * 领导id
     */
    @ApiModelProperty(value = "领导id", dataType = "String")
    private String leaderId;

    /**
     * 领导所在机构id
     */
    @ApiModelProperty(value = "领导所在机构id", dataType = "String")
    private String leaderOrgId;

    /**
     * 单位名称
     */
    @ApiModelProperty(value = "单位名称", dataType = "String")
    private String orgName;

    /**
     * 警员姓名
     */
    @ApiModelProperty(value = "警员姓名", dataType = "String")
    private String policeName;

    /**
     * 警员所在单位名称
     */
    @ApiModelProperty(value = "警员所在单位名称", dataType = "String")
    private String policeOrgName;

    /**
     * 领导姓名
     */
    @ApiModelProperty(value = "领导姓名", dataType = "String")
    private String leaderName;

    /**
     * 领导所在机构名称
     */
    @ApiModelProperty(value = "领导所在机构名称", dataType = "String")
    private String leaderOrgName;

}
