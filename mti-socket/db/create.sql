drop table t_ws_message;

/*==============================================================*/
/* Table: t_ws_message                                          */
/*==============================================================*/
create table t_ws_message (
   id                   SERIAL not null,
   message_id           INT8                 null,
   message_content      TEXT                 null,
   retry_times          INT2                 null,
   status               CHAR(2)              null default '00',
   created_by           varchar(32)          not null default '',
   created_on           timestamp(0)         not null default CURRENT_TIMESTAMP,
   updated_by           varchar(32)          not null default '',
   updated_on           timestamp(0)         not null default CURRENT_TIMESTAMP,
   is_active            boolean              not null default 'true',
   is_display           boolean              not null default 'true',
   is_deleted           boolean              not null default 'false',
   remark               varchar(1000)        not null default '',
   constraint PK_T_WS_MESSAGE primary key (id)
);

comment on table t_ws_message is
'WebSocket消息表';

comment on column t_ws_message.message_id is
'消息ID';

comment on column t_ws_message.message_content is
'消息内容';

comment on column t_ws_message.retry_times is
'重试次数';

comment on column t_ws_message.status is
'状态';

comment on column t_ws_message.created_by is
'创建者';

comment on column t_ws_message.created_on is
'创建时间';

comment on column t_ws_message.updated_by is
'更新者';

comment on column t_ws_message.updated_on is
'更新时间';

comment on column t_ws_message.is_active is
'是否有效';

comment on column t_ws_message.is_display is
'是否显示';

comment on column t_ws_message.is_deleted is
'是否删除';

comment on column t_ws_message.remark is
'备注';
