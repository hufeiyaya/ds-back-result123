package com.daoshu.socket.zookeeper.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @ClassName: ZookeeperProperties
 * @description: zk配置
 * @author: Allen
 * @create: 2019-06-20 13:52
 **/
@Data
@Component
@ConfigurationProperties(prefix = "spring.zookeeper")
public class ZookeeperProperties {

    private String host = "localhost:2181";
    private int sessionTimeout = 3000;
    private int connectionTimeout = 2147483647;

}
