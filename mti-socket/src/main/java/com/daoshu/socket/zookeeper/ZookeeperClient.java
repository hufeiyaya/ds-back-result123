package com.daoshu.socket.zookeeper;


import com.daoshu.socket.zookeeper.config.ZookeeperProperties;
import com.daoshu.socket.zookeeper.utils.ZkSerializerUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.I0Itec.zkclient.ZkClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName: ZookeeperClient
 * @description: zookeeper客户端
 * @author: Allen
 * @create: 2019-06-18 09:22
 **/
@Data
@Slf4j
@Component
public class ZookeeperClient {

    @Autowired
    ZookeeperProperties zookeeperProperties;

    /***
     * 创建 Zookeeper 客户端连接
     * @return
     */

    public static ZkClient createZkClient(ZookeeperProperties zookeeperProperties) {
        ZkClient client = new ZkClient(zookeeperProperties.getHost(),
                zookeeperProperties.getSessionTimeout(),
                zookeeperProperties.getConnectionTimeout(),
                new ZkSerializerUtils());
        client.setZkSerializer(new ZkSerializerUtils());
        return client;
    }

    public static ZkClient createZkClient() {
        return createZkClient(new ZookeeperProperties());
    }

}
