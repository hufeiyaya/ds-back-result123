package com.daoshu.socket.zookeeper.utils;

import org.I0Itec.zkclient.exception.ZkMarshallingError;
import org.I0Itec.zkclient.serialize.ZkSerializer;

import java.io.UnsupportedEncodingException;

public class ZkSerializerUtils implements ZkSerializer {

    String charset = "UTF-8";

    @Override
    public byte[] serialize(Object obj) {
        try {
            return String.valueOf(obj).getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new ZkMarshallingError(e);
        }

    }

    @Override
    public Object deserialize(byte[] bytes) {
        try {
            return new String(bytes, charset);
        } catch (UnsupportedEncodingException e) {
            throw new ZkMarshallingError(e);
        }
    }
}
