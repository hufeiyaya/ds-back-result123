package com.daoshu.socket.provider;

import com.daoshu.socket.bean.ResultOut;

import com.daoshu.socket.bean.MonitorBean;
import com.daoshu.socket.constants.Constants;
import com.daoshu.socket.redis.RedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: MonitorProvider
 * @description: 监控
 * @author: Allen
 * @create: 2019-06-20 10:41
 **/
@Slf4j
@RestController
@RequestMapping("/websocket/monitor")
@Api(tags = "监控接口", value = "监控接口")
public class MonitorProvider {

    @Autowired
    RedisUtil redisUtil;

    @ApiOperation(value = "监控消息", notes = "监控消息")
    @PostMapping("/getMonitorInfo")
    public ResultOut<MonitorBean> getMonitorInfo() {

        MonitorBean monitorBean = new MonitorBean();
        //monitorBean.setOnLinePersonNum(redisUtil.lGetListSize(Constants.USER_TOKEN));
        return ResultOut.resultSuccess(monitorBean);

    }

}
