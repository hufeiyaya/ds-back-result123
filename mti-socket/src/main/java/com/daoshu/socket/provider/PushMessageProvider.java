package com.daoshu.socket.provider;

import com.daoshu.socket.view.PushMessageIn;
import com.daoshu.socket.bean.ResultOut;
import com.daoshu.socket.notice.ISyncMessageNotice;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: PushMessageProvider
 * @description: 推送消息接口
 * @author: Allen
 * @create: 2019-06-19 14:27
 **/
@Slf4j
@RestController
@RequestMapping("/websocket/push")
@Api(tags = "推送接口", value = "推送接口")
public class PushMessageProvider implements PushMessageClientApi {

    @Autowired
    ISyncMessageNotice syncMessageNotice;

    @ApiOperation(value = "集群消息同步接口", notes = "集群消息同步接口")
    @PostMapping("/syncMessage")
    @Override
    public ResultOut syncMessage(@RequestBody PushMessageIn pushUserMessageIn) {
        syncMessageNotice.sendMessageNotice(pushUserMessageIn);
        return ResultOut.resultSuccess();
    }

    @ApiOperation(value = "单台服务推送消息接口", notes = "单台服务推送消息接口")
    @PostMapping("/message")
    @Override
    public ResultOut message(@RequestBody PushMessageIn pushUserMessageIn) {
        log.info("message ====>>>" + pushUserMessageIn);
        syncMessageNotice.monitorMessageNotice(pushUserMessageIn);
        return ResultOut.resultSuccess();
    }

}
