package com.daoshu.socket.provider;

import com.daoshu.socket.bean.ResultOut;
import com.daoshu.socket.view.PushMessageIn;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @ClassName: PushMessageClientApi
 * @description:
 * @author: Allen
 * @create: 2019-08-19 10:02
 **/
@RequestMapping("/websocket/push")
public interface PushMessageClientApi {


    @ApiOperation(value = "集群消息同步接口", notes = "集群消息同步接口")
    @PostMapping("/syncMessage")
    ResultOut syncMessage(@RequestBody PushMessageIn pushUserMessageIn);

    @ApiOperation(value = "单台服务推送消息接口", notes = "单台服务推送消息接口")
    @PostMapping("/message")
    ResultOut message(@RequestBody PushMessageIn pushUserMessageIn);

}
