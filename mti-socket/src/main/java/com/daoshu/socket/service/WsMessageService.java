package com.daoshu.socket.service;

import com.daoshu.socket.view.PushMessageIn;

/**
 * WebSocket消息表(WsMessage)表服务接口
 *
 * @author makejava
 * @since 2019-07-17 12:37:58
 */
public interface WsMessageService {

    /**
     * 保存消息
     */
    void saveMessage(PushMessageIn in);

    /***
     * 修改状态
     * @param vo
     */
    void updateByStatus(PushMessageIn vo);
}
