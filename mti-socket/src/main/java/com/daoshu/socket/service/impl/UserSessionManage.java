package com.daoshu.socket.service.impl;

import com.daoshu.socket.bean.UserInfo;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @ClassName: UserSessionManage
 * @description: 用户Session管理
 * @author: Allen
 * @create: 2019-07-01 10:59
 **/
@Slf4j
public class UserSessionManage {

    protected UserSessionManage() {
        throw new IllegalStateException("Utility class");
    }

    /***
     * 用户Session Map 集合 SessionId-UserInfo
     */
    protected static final Map<String, UserInfo> SESSIOIN_USERS = new ConcurrentHashMap<>();

    /****
     * 根据分组返回用户信息列表
     * @param groupId
     * @return
     */
    public static List<UserInfo> getUserInfoListByGroupId(List<String> groupId) {

        if (groupId == null || groupId.isEmpty()) {
            return Lists.newArrayList();
        }

        Collection<UserInfo> collection = SESSIOIN_USERS.values();
        if (collection != null && !collection.isEmpty()) {
            return collection.stream().filter(v -> contains(groupId, v.getGroupIds())).collect(Collectors.toList());
        }
        return Lists.newArrayList();
    }

    /***
     * 返回所有用户列表
     *
     * @return
     */
    public static List<UserInfo> getAllUserInfoList() {
        Collection<UserInfo> collection = SESSIOIN_USERS.values();
        if (collection != null && !collection.isEmpty()) {
            return collection.stream().collect(Collectors.toList());
        }
        return Lists.newArrayList();
    }

    /***
     * 根据用户ID返回用户列表集合
     * @param userIds
     * @return
     */
    public static List<UserInfo> getUserInfoListByUserIds(List<String> userIds) {

        if (userIds == null || userIds.isEmpty()) {
            return Lists.newArrayList();
        }

        Collection<UserInfo> collection = SESSIOIN_USERS.values();
        if (collection != null && !collection.isEmpty()) {
            return collection.stream().filter(v -> userIds.contains(v.getUserId())).collect(Collectors.toList());
        }
        return Lists.newArrayList();
    }

    /***
     * 根据用户ID列表返回指定用户组下面的用户
     * @param groupIds
     * @param userIds
     * @return
     */
    public static List<UserInfo> getUserInfoListByUserIdsAndGroupId(List<String> groupIds, List<String> userIds) {
        log.info("groupIds:{}, userIds:{}", groupIds, userIds);
        if (userIds == null || userIds.isEmpty()) {
            return Lists.newArrayList();
        }

        if (groupIds == null || groupIds.isEmpty()) {
            return Lists.newArrayList();
        }

        Collection<UserInfo> collection = SESSIOIN_USERS.values();
        log.info("collection:{}", collection);
        if (collection == null || collection.isEmpty()) {
            return Lists.newArrayList();
        }


        List<UserInfo> userInfos1 = new LinkedList<>();
        for (UserInfo userInfo : collection) {

            boolean c1 = userIds.contains(userInfo.getUserId());
            if (c1 && contains(groupIds, userInfo.getGroupIds())) {
                userInfos1.add(userInfo);
            }

        }

        return userInfos1;

    }

    /***
     * 组包含
     * @param groupIds
     * @param userGroupIds
     * @return
     */
    private static boolean contains(List<String> groupIds, List<String> userGroupIds) {

        if (userGroupIds == null || userGroupIds.isEmpty()) {
            return false;
        }

        if (groupIds == null || groupIds.isEmpty()) {
            return false;
        }

        for (String groupId : userGroupIds) {
            if (groupIds.contains(groupId)) {
                return true;
            }
        }

        return false;

    }

}
