package com.daoshu.socket.service.impl;

import com.alibaba.fastjson.JSON;

import com.daoshu.socket.mapper.WsMessageMapper;
import com.daoshu.socket.view.PushMessageIn;
import com.daoshu.socket.entity.WsMessage;
import com.daoshu.socket.entity.enums.WsMessageStatusEnum;
import com.daoshu.socket.service.WsMessageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * WebSocket消息表(WsMessage)表服务实现类
 *
 * @author makejava
 * @since 2019-07-17 12:37:58
 */
//@Service
@Slf4j
public class WsMessageServiceImpl implements WsMessageService {

    @Autowired
    WsMessageMapper wsMessageMapper;

    @Override
    public void saveMessage(PushMessageIn in) {

        WsMessage wsMessage = new WsMessage();
        BeanUtils.copyProperties(in, wsMessage);


//        if (!StringUtils.isBlank(in.getId())) {
//            Wrapper wrapper = new EntityWrapper();
//            wrapper.eq("message_id", in.getId());
//            int count = selectCount(wrapper);
//            if (count > 0) {
//                log.info("message_id already exists");
//                return;
//            }
//
//        }


        wsMessage.setMessageId(in.getId());
        if (in.getTo() != null && !in.getTo().isEmpty()) {
            wsMessage.setToUsers(StringUtils.join(in.getTo(), ","));
        }
        if (in.getBody() != null && !in.getBody().isEmpty()) {
            wsMessage.setBody(JSON.toJSONString(in.getBody()));
        }
        wsMessage.setRetryTimes(0);
//        wsMessageMapper.insert(wsMessage);
    }

    @Override
    public void updateByStatus(PushMessageIn vo) {
        if (vo == null || StringUtils.isBlank(vo.getId())) {
            log.info("updateByStatus vo is null");
            return;
        }

//        WsMessage wsMessage = new WsMessage();
//        wsMessage.setStatus(WsMessageStatusEnum.USER_ALL.getType());
//        Wrapper wrapper = new EntityWrapper();
//        wrapper.eq("message_id", vo.getId());
//        update(wsMessage, wrapper);
    }


}
