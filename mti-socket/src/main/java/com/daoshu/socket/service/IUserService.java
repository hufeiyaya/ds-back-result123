package com.daoshu.socket.service;

import com.corundumstudio.socketio.SocketIOClient;
import com.daoshu.socket.view.PushMessageIn;
import com.daoshu.socket.bean.UserInfo;

/**
 * @ClassName: IUserService
 * @description: 用户服务接口
 * @author: Allen
 * @create: 2019-06-20 14:52
 **/
public interface IUserService {

    /***
     * 保存内存信息
     * @param client
     * @param userInfo
     */
    void setUserMapInfo(SocketIOClient client, UserInfo userInfo);

    /****
     * 移除内存中MAP信息
     * @param client
     */
    void removeUserMapInfo(SocketIOClient client);

    /***
     * 发送信息到用户
     * @param in
     */
    void sendMessageForUser(PushMessageIn in);
}
