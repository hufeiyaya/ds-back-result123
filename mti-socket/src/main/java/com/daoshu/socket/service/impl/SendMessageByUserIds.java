package com.daoshu.socket.service.impl;

import com.daoshu.socket.redis.RedisUtil;
import com.daoshu.socket.view.PushMessageIn;
import com.daoshu.socket.bean.UserInfo;
import com.daoshu.socket.service.ISendMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @ClassName: SendMessageByUserIds
 * @description: 根据用户ID发送
 * @author: Allen
 * @create: 2019-06-25 22:33
 **/
@Component
@Slf4j
public class SendMessageByUserIds implements ISendMessage {

    /**
     * 引入redis
     */
    @Autowired
    RedisUtil redisUtil;
    /***
     * 根据用户ID发送根据 PushMessageIn 中 userList 参数
     * @see com.daoshu.socket.bean.PushMessageIn
     * @param in
     */
    @Override
    public void sendMessage(PushMessageIn in) {

        List<String> userIds = in.getTo();
        if (userIds == null || userIds.isEmpty()) {
            log.info("sendMessage userIds size == 0");
            return;
        }

        List<UserInfo> userInfos = UserSessionManage.getUserInfoListByUserIds(userIds);
        if (null == userInfos || userInfos.size() < 1) {
            //saveOfflineMessage(in);
        } else {
            for (UserInfo userInfo : userInfos) {
                if (userInfo.getClient() == null) {
                    log.info("client=null,PushMessageIn={}", in);
                    //saveOfflineMessage(in);
                    continue;
                }
                log.info("SendMessageByUserIds userInfo:{} PushMessageIn:{}", userInfo, in);
                userInfo.getClient().sendEvent(in.getType(), in);
                //delOfflineMessage(in);
            }
        }
//        for (UserInfo userInfo : UserSessionManage.getUserInfoListByUserIds(userIds)) {
//            if (userInfo.getClient() == null) {
//                log.info("client=null,PushMessageIn={}", in);
//                continue;
//            }
//            log.info("SendMessageByUserIds userInfo:{} PushMessageIn:{}", userInfo, in);
//            userInfo.getClient().sendEvent(in.getType(), in);
//        }
    }

    @Override
    public void sendMessageByGroup(PushMessageIn in) {
        List<String> userIds = in.getTo();
        if (userIds == null || userIds.isEmpty()) {
            log.info("sendMessageByGroup userIds size == 0");
            return;
        }

        if (in.getGroupId() == null || in.getGroupId().isEmpty()) {
            log.info("sendMessageByGroup groupId is null");
            return;
        }
        /************************/
        List<UserInfo> userInfos = UserSessionManage.getUserInfoListByUserIdsAndGroupId(in.getGroupId(), userIds);
        if (null == userInfos || userInfos.size() < 1) {
            //saveOfflineMessage(in);
        } else {
            for (UserInfo userInfo : userInfos) {
                if (userInfo.getClient() == null) {
                    log.info("client=null,PushMessageIn={}", in);
                    //saveOfflineMessage(in);
                    continue;
                }
                log.info("SendMessageByUserIds userInfo:{} PushMessageIn:{}", userInfo, in);
                userInfo.getClient().sendEvent(in.getType(), in);
                //delOfflineMessage(in);
            }
        }
//        for (UserInfo userInfo : UserSessionManage.getUserInfoListByUserIdsAndGroupId(in.getGroupId(), userIds)) {
//            if (userInfo.getClient() == null) {
//                log.info("client=null,PushMessageIn={}", in);
//                continue;
//            }
//            log.info("SendMessageByUserIds userInfo:{} PushMessageIn:{}", userInfo, in);
//            userInfo.getClient().sendEvent(in.getType(), in);
//        }

    }

    /**
     * 保存离线消息
     * @param in
     */
    private void saveOfflineMessage(PushMessageIn in) {
        //redisUtil.set(String.format(Constants.MESSAGE_BODY, in.getId()), in,90);
    }

    /**
     * 删除离线消息
     * @param in
     */
    private void delOfflineMessage(PushMessageIn in) {
        //redisUtil.del(String.format(Constants.MESSAGE_BODY, in.getId()));
    }
}
