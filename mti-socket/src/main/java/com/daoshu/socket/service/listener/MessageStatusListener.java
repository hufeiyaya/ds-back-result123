package com.daoshu.socket.service.listener;

import com.daoshu.socket.redis.RedisUtil;
import com.daoshu.socket.view.PushMessageIn;
import com.daoshu.socket.constants.Constants;
import com.daoshu.socket.envent.MessageStatusEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @ClassName: MessageStatusListener
 * @description: 消息状态更改
 * @author: Allen
 * @create: 2019-07-17 16:55
 **/
@Component
@Slf4j
public class MessageStatusListener implements SmartApplicationListener {


    @Autowired
    RedisUtil redisUtil;

    @Override
    public boolean supportsEventType(Class<? extends ApplicationEvent> eventType) {
        return eventType == MessageStatusEvent.class;
    }

    @Override
    public boolean supportsSourceType(Class<?> sourceType) {
        return sourceType == PushMessageIn.class;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {

        PushMessageIn vo = (PushMessageIn) applicationEvent.getSource();

        if (StringUtils.isBlank(vo.getId())) {
            log.info("MessageStatusListener vo id is null");
            return;
        }

        //删除Key
        //redisUtil.del(String.format(Constants.MESSAGE_BODY, vo.getId()));


    }

    @Override
    public int getOrder() {
        return 0;
    }
}
