package com.daoshu.socket.service;

import com.daoshu.socket.service.enums.SendTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @ClassName: SendMessageFactory
 * @description: 发送消息工厂类
 * @author: Allen
 * @create: 2019-07-10 11:31
 **/
@Component
@Slf4j
public class SendMessageFactory {

    @Autowired
    private ApplicationContext applicationContext;

    public static final String NAME = "sendMessage";

    /****
     *  根据发送类型创建对应发送类
     *  @see com.daoshu.socket.service.impl.SendMessageByUserIds 类型 00 - 根据用户id发送
     *  @see com.daoshu.socket.service.impl.SendMessageForAll    类型 01 - 发送给所有用户
     *  @see com.daoshu.socket.service.impl.SendMessageByGroupId 类型 02 - 根据分组发送消息
     *  @param sendType 发送类型 00、01、02
     * @return
     */
    public ISendMessage getInstance(String sendType) {

        SendTypeEnum sendTypeEnum = SendTypeEnum.getByKey(sendType);
        if (sendTypeEnum == null) {
            return null;
        }

        String beanName = NAME + sendTypeEnum.getDescription();
        log.debug("SendMessageFactory beanName:" + beanName);

        ISendMessage object = null;
        Map<String, ISendMessage> map = applicationContext.getBeansOfType(ISendMessage.class);
        if (map != null) {
            object = map.get(beanName);
        }
        return object == null ? (null) : object;
    }

}
