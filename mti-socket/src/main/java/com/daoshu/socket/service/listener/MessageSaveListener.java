package com.daoshu.socket.service.listener;

import com.daoshu.socket.config.WebsocketConfig;
import com.daoshu.socket.constants.Constants;
import com.daoshu.socket.envent.MessageSaveEvent;
import com.daoshu.socket.redis.RedisUtil;
import com.daoshu.socket.view.PushMessageIn;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @ClassName: SaveWsMessageListener
 * @description:
 * @author: Allen
 * @create: 2019-07-17 14:49
 **/
@Component
public class MessageSaveListener implements SmartApplicationListener {


    @Autowired
    RedisUtil redisUtil;

    @Autowired
    WebsocketConfig websocketConfig;


    @Override
    public boolean supportsEventType(Class<? extends ApplicationEvent> eventType) {
        return eventType == MessageSaveEvent.class;
    }

    @Override
    public boolean supportsSourceType(Class<?> sourceType) {
        return sourceType == PushMessageIn.class;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {

        /***
         * 保存数据库
         */
        PushMessageIn vo = (PushMessageIn) applicationEvent.getSource();

        /***
         * 当消息ID 不为空的时候，需要回执
         */
        if (!StringUtils.isBlank(vo.getId())) {
            /***
             * 保存Redis Key值
             */
            //redisUtil.set(String.format(Constants.MESSAGE_BODY, vo.getId()), vo, websocketConfig.getOfflineMessageInvalid());
        }

    }

    @Override
    public int getOrder() {
        return 0;
    }
}
