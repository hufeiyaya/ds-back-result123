package com.daoshu.socket.service.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @ClassName: SendTypeEnum
 * @description: 发送类型类
 * @author: Allen
 * @create: 2019-06-25 22:46
 **/

public enum SendTypeEnum {


    USER_IDS("00", "ByUserIds"),

    USER_ALL("01", "ForAll")
    ;

    private String type;

    private String description;

    SendTypeEnum(String type, String description) {
        this.type = type;
        this.description = description;
    }


    public static SendTypeEnum getByKey(String code) {
        if (StringUtils.isBlank(code)) {
            return null;
        }
        for (SendTypeEnum sendTypeEnum : values()) {
            if (sendTypeEnum.getType().equals(code)) {
                return sendTypeEnum;
            }
        }
        return null;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

}
