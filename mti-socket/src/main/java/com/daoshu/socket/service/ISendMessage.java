package com.daoshu.socket.service;

import com.daoshu.socket.view.PushMessageIn;

/**
 * @ClassName: ISendMessage
 * @description:
 * @author: Allen
 * @create: 2019-06-25 22:30
 **/
public interface ISendMessage {

    /***
     * 发送消息
     * @param in
     */
    void sendMessage(PushMessageIn in);

    /***
     * 组发送
     * @param in
     */
    void sendMessageByGroup(PushMessageIn in);

}
