package com.daoshu.socket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName: WebsocketServiceApplication
 * @description: 启动类
 * @author: Allen
 * @create: 2019-06-14 09:10
 **/


@SpringBootApplication(scanBasePackages = "com.daoshu")
public class SocketServiceApplication {

    public static void main(String[] args){
        SpringApplication.run(SocketServiceApplication.class, args);
    }

}
