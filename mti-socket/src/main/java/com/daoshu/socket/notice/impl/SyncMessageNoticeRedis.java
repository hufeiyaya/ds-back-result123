package com.daoshu.socket.notice.impl;

import com.daoshu.socket.redis.RedisUtil;
import com.daoshu.socket.view.PushMessageIn;
import com.daoshu.socket.config.NoticeTypeEnum;
import com.daoshu.socket.config.RedisSubConfig;
import com.daoshu.socket.config.WebsocketConfig;
import com.daoshu.socket.notice.ISyncMessageNotice;
import com.daoshu.socket.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName: SyncMessageNoticeRedis
 * @description: Redis 广播
 * @author: Allen
 * @create: 2019-06-26 09:09
 **/
@Slf4j
@Component("syncMessageNoticeRedis")
public class SyncMessageNoticeRedis implements ISyncMessageNotice {

    @Autowired
    WebsocketConfig websocketConfig;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    RedisSubConfig redisConfig;

    @Autowired
    IUserService userService;

   // private ChannelTopic topic;

    @Override
    public void initializer() {
        log.info("SyncMessageNoticeRedis");
        if (!NoticeTypeEnum.REDIS.getName().equalsIgnoreCase(websocketConfig.getNoticeType())) {
            return;
        }
        log.info("SyncMessageNoticeRedis initializer");
        //topic = new ChannelTopic(redisConfig.getSubscribe());

    }

    @Override
    public void sendMessageNotice(PushMessageIn pushMessageIn) {
        //redisUtil.getRedisTemplate().convertAndSend(topic.getTopic(), pushMessageIn);
    }

    @Override
    public void monitorMessageNotice(PushMessageIn pushMessageIn) {
        log.info("monitorMessageNotice , {}", pushMessageIn);
        userService.sendMessageForUser(pushMessageIn);
    }

    /**
     * 消息监听器，使用MessageAdapter可实现自动化解码及方法代理
     *
     * @return
     */
   /* @Bean
    @ConditionalOnProperty(prefix = "websocket.server", name = "notice-type", havingValue = "redis")
    public MessageListenerAdapter listener() {
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        MessageListenerAdapter adapter = new MessageListenerAdapter((MessageListener) (message, bytes) -> {
            if (message != null) {
                try {
                    PushMessageIn pushMessageIn = (PushMessageIn) redisUtil.getRedisTemplate().getValueSerializer().deserialize(message.getBody());
                    monitorMessageNotice(pushMessageIn);
                } catch (Exception e) {
                    log.error("MessageListenerAdapter err:" + e.getMessage());
                }
            }
        }, "onMessage");
        adapter.setSerializer(jackson2JsonRedisSerializer);
        adapter.afterPropertiesSet();
        return adapter;
    }*/

    /**
     * 将订阅器绑定到容器
     *
     * @param connectionFactory
     * @param listener
     * @return
     */
   /* @Bean
    @ConditionalOnProperty(prefix = "websocket.server", name = "notice-type", havingValue = "redis")
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory, MessageListenerAdapter listener) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(listener, new PatternTopic(redisConfig.getSubscribe()));
        return container;
    }*/

}
