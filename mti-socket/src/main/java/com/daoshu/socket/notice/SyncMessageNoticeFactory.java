package com.daoshu.socket.notice;

import com.daoshu.socket.config.NoticeTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @ClassName: SyncMessageNoticeFactory
 * @description:
 * @author: Allen
 * @create: 2019-06-26 10:57
 **/
@Component
@Slf4j
public class SyncMessageNoticeFactory {

    @Autowired
    private ApplicationContext applicationContext;

    public static final String NAME = "syncMessageNotice";

    public ISyncMessageNotice getInstance(String type) {

        NoticeTypeEnum noticeTypeEnum = NoticeTypeEnum.getByKey(type);
        if (noticeTypeEnum == null) {
            return null;
        }

        String beanName = NAME + noticeTypeEnum.getDesc();
        log.info("SyncMessageNoticeFactory beanName:" + beanName);
        Object object = applicationContext.getBean(beanName);
        return object == null ? (null) : (ISyncMessageNotice) object;
    }

}
