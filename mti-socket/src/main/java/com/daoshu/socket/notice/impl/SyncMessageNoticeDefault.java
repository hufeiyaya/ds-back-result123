package com.daoshu.socket.notice.impl;

import com.daoshu.socket.view.PushMessageIn;
import com.daoshu.socket.notice.ISyncMessageNotice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @ClassName: SyncMessageNoticeDefault
 * @description: 默认空处理
 * @author: Allen
 * @create: 2019-06-26 13:20
 **/
@Slf4j
@Component("syncMessageNoticeDefault")
public class SyncMessageNoticeDefault implements ISyncMessageNotice {
    @Override
    public void initializer() {
        log.info("SyncMessageNoticeDefault initializer");
    }

    @Override
    public void sendMessageNotice(PushMessageIn pushMessageIn) {
        log.info("SyncMessageNoticeDefault sendMessageNotice");
    }

    @Override
    public void monitorMessageNotice(PushMessageIn pushMessageIn) {
        log.info("SyncMessageNoticeDefault monitorMessageNotice");
    }
}
