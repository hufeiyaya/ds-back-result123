package com.daoshu.socket.notice;

import com.daoshu.socket.view.PushMessageIn;

/**
 * @ClassName: ISyncMessageNotice
 * @description: 同步集群消息接口
 * @author: Allen
 * @create: 2019-06-26 09:07
 **/
public interface ISyncMessageNotice {

    /****
     * 初始化设置 根据配置 websocket.server.notice-type 中配置的类型进行初始化对应的同步中间件
     */
    void initializer();

    /***
     * 通知所有服务器，同步集群中所有服务
     * @param pushMessageIn 发送的消息类
     */
    void sendMessageNotice(PushMessageIn pushMessageIn);

    /***
     * 监听订阅地址，接收sendMessageNotice 方法广播出来的消息
     */
    void monitorMessageNotice(PushMessageIn pushMessageIn);

}
