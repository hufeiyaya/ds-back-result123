package com.daoshu.socket.constants;

/**
 * @ClassName: Constants
 * @description:
 * @author: Allen
 * @create: 2019-06-19 16:16
 **/
public class Constants {

    private Constants() {
        throw new IllegalStateException("Utility class");
    }


    public static final String TRUE = "true";

    public static final String SYNC_MESSAGE = "syncMessage";

    /****
     * Redis 用户登录token
     */
    public static final String USER_TOKEN = "user_token";

    /***
     * 消息体key 用于离线消息发送  ws_message_body_消息ID
     */
    public static final String MESSAGE_BODY = "ws_message_body_%s";

}
