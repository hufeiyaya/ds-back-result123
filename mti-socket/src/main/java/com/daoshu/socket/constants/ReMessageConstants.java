package com.daoshu.socket.constants;

/**
 * @ClassName: MessageConstants
 * @description: 消息类型定义
 * @author: Allen
 * @create: 2019-06-25 15:56
 **/
public class ReMessageConstants {

    private ReMessageConstants() {
        throw new IllegalStateException("Utility class");
    }

    /***
     * 返回异常
     */
    public static final String RE_EXCEPTION = "exception";

}
