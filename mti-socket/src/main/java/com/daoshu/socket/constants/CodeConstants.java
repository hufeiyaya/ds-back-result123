package com.daoshu.socket.constants;

public class CodeConstants {

    private CodeConstants(){

    }

    /***
     * 正常返回
     */
    public static final String CODE_0 = "0";

    /***
     * 未登录
     */
    public static final String CODE_100 = "100";

    /**
     * 操作成功！
     */
    public static final String CODE_000000 = "000000";

    /**
     * 未知异常，请联系管理员！
     */
    public static final String CODE_999999 = "999999";

    /**
     * 登录失效，请重新登录！
     */
    public static final String CODE_999998 = "999998";

    /**
     * 没有权限，禁止访问
     */
    public static final String CODE_999997 = "999997";

    /**
     * 用户信息变更，请重新登录！
     */
    public static final String CODE_999996 = "999996";

    /****
     * 路径不存在，请检查路径是否正确！
     */
    public static final  String CODE_999995 = "999995";

    /***
     * 数据库中已存在该记录
     */
    public static final String CODE_999994 = "999994";

    /****
     * 参数校验错误
     */
    public static final String CODE_999993 = "999993";


    /**
     * 包含非法字符
     */
    public static final String CODE_900001 = "900001";

    /**
     * 用户状态异常
     */
    public static final String CODE_800001 = "800001";

}
