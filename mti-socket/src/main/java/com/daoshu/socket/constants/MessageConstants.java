package com.daoshu.socket.constants;

/**
 * @ClassName: MessageConstants
 * @description: 消息类型定义
 * @author: Allen
 * @create: 2019-06-25 15:56
 **/
public class MessageConstants {

    private MessageConstants() {
        throw new IllegalStateException("Utility class");
    }


    /***
     * 登录事件
     */
    public static final String LOGIN = "LOGIN";

    /****
     * 消息接收事件名称
     */
    public static final String MESSAGE = "message";

    /****
     *  消息回执接口 接收
     */
    public static final String RECEIPT_MESSAGE = "receipt_message";

}
