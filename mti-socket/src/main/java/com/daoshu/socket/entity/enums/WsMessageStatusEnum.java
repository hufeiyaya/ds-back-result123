package com.daoshu.socket.entity.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @ClassName: SendTypeEnum
 * @description: 消息状态
 * @author: Allen
 * @create: 2019-06-25 22:46
 **/

public enum WsMessageStatusEnum {


    USER_IDS("00", "默认"),

    USER_ALL("01", "已送达");

    private String type;

    private String description;

    WsMessageStatusEnum(String type, String description) {
        this.type = type;
        this.description = description;
    }


    public static WsMessageStatusEnum getByKey(String code) {
        if (StringUtils.isBlank(code)) {
            return null;
        }
        for (WsMessageStatusEnum sendTypeEnum : values()) {
            if (sendTypeEnum.getType().equals(code)) {
                return sendTypeEnum;
            }
        }
        return null;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

}
