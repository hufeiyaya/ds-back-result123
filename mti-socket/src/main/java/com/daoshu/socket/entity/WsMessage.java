package com.daoshu.socket.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * WebSocket消息表(WsMessage)实体类
 *
 * @author makejava
 * @since 2019-07-17 12:37:58
 */

@Data
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class WsMessage {
    private static final long serialVersionUID = -81849489807195133L;

    private Long id;
    /***
     * 消息ID
     */

    private String messageId;

    /***
     * 发送用户列表 逗号分隔
     */

    private String toUsers;

    /***
     * 消息内容
     */
    private String body;
    /***
     * 重试次数
     */
    private Integer retryTimes;

    private String type;

    private String sort;

    /***
     * 状态
     */
    private String status;
    /***
     * 创建者
     */
    private String createdBy;
    /****
     * 创建时间
     */
    private Date createdOn;
    /***
     * 更新者
     */
    private String updatedBy;
    /****
     * 更新时间
     */
    private Date updatedOn;
    /***
     * 是否有效
     */
    private Boolean active;
    /***
     * 是否显示
     */
    private Boolean display;
    /***
     * 是否删除
     */
    private Boolean deleted;
    /***
     * 备注
     */
    private String remark;

}
