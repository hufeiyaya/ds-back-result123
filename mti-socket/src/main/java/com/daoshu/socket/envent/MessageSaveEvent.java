package com.daoshu.socket.envent;

import org.springframework.context.ApplicationEvent;

/**
 * @ClassName: MessageSaveEvent
 * @description:
 * @author: Allen
 * @create: 2019-07-17 14:43
 **/
public class MessageSaveEvent extends ApplicationEvent {
    public MessageSaveEvent(Object source) {
        super(source);
    }
}
