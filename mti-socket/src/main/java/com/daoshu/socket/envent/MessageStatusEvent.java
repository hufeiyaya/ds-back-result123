package com.daoshu.socket.envent;

import org.springframework.context.ApplicationEvent;

/**
 * @ClassName: MessageStatusEvent
 * @description: 消息状态事件
 * @author: Allen
 * @create: 2019-07-17 16:56
 **/
public class MessageStatusEvent extends ApplicationEvent {

    public MessageStatusEvent(Object source) {
        super(source);
    }

}
