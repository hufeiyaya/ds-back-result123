package com.daoshu.socket.exception;

import com.daoshu.socket.bean.ErroCode;
import com.daoshu.socket.constants.CodeConstants;
import lombok.Data;

/**
 * 自定义异常
 *
 * @author Allen
 */
@Data
public class SysException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private String msg;

    private Integer code = Integer.valueOf(CodeConstants.CODE_999999);

    public SysException(String code) {
        super(code);
        ErroCode errorCode = ErrorCodeHandler.getErrorCode(code);
        if (errorCode != null) {
            this.code = Integer.valueOf(errorCode.getCode());
            this.msg = errorCode.getMsg();
        } else {
            this.msg = code;
        }
    }

    public SysException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

    public SysException(String msg, String code) {
        super(msg);
        this.msg = msg;
        this.code = Integer.valueOf(code);
    }

    public SysException(String msg, String code, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = Integer.valueOf(code);
    }


}
