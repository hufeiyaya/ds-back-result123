package com.daoshu.socket.job;

import com.alibaba.fastjson.JSONObject;
import com.daoshu.socket.redis.RedisUtil;
import com.daoshu.socket.view.PushMessageIn;
import com.daoshu.socket.constants.Constants;
import com.daoshu.socket.notice.ISyncMessageNotice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @ClassName: MessagePushJobHandler1
 * @description:
 * @author: Allen
 * @create: 2019-07-17 17:28
 **/
@Slf4j
@Component
@Configuration
@EnableScheduling
@ConditionalOnProperty(prefix = "websocket.server", name = "offline-message-enable", havingValue = "true")
public class MessagePushJobHandler {
/*
    @Autowired
    ISyncMessageNotice syncMessageNotice;

    @Autowired
    RedisUtil redisUtil;

   // @Scheduled(cron = "${websocket.server.offline-message-job-cron}")
    public void work() {
        // task execution logic
        log.info("MessagePushJobHandler working ...");

        Set<String> keys = redisUtil.keys(String.format(Constants.MESSAGE_BODY, "*"));
        if (keys != null && !keys.isEmpty()) {
            log.info("MessagePushJobHandler keys size" + keys.size());
            keys.forEach(key -> {
                Object value = redisUtil.get(key);
                if (value != null) {

                    PushMessageIn pushMessageIn = (PushMessageIn) value;
                    log.info("MessagePushJobHandler PushMessageIn:" + JSONObject.toJSONString(pushMessageIn));
                    syncMessageNotice.sendMessageNotice(pushMessageIn);
                }
            });
        }

    }*/
}
