package com.daoshu.socket.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @ClassName: RedisConfig
 * @description:
 * @author: Allen
 * @create: 2019-06-26 15:23
 **/
@Data
@Component
@ConfigurationProperties(prefix = "websocket.redis")
public class RedisSubConfig {

    /***
     * 订阅消息
     */
    private String subscribe = "mti_subscribe_message";

}
