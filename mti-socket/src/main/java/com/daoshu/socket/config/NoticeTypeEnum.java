package com.daoshu.socket.config;

import org.apache.commons.lang3.StringUtils;

/***
 * 通知类型
 */
public enum NoticeTypeEnum {

    /***
     * 默认空处理
     */
    NONE("none", "Default"),
    /***
     * ZOOKEEPER 通知
     */
    ZOOKEEPER("zookeeper", "Zookeeper"),
    /***
     * REDIS 通知
     */
    REDIS("redis", "Redis"),

    /***
     *
     */
    HTTP("http", "Http")
    ;

    NoticeTypeEnum(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    /***
     * 名称
     */
    private String name;

    /***
     * 描述
     */
    private String desc;

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public static NoticeTypeEnum getByKey(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        for (NoticeTypeEnum noticeTypeEnum : values()) {
            if (noticeTypeEnum.getName().equals(name)) {
                return noticeTypeEnum;
            }
        }
        return null;
    }

}
