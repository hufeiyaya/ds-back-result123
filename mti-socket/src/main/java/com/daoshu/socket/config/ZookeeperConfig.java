package com.daoshu.socket.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @ClassName: ZookeeperConfig
 * @description: Zookeeper 配置
 * @author: Allen
 * @create: 2019-06-18 16:54
 **/
@Data
@Component
@ConfigurationProperties(prefix = "websocket.zookeeper")
public class ZookeeperConfig {

    /***
     * 监听节点目录
     */
    private String dataChangeNode;




}
