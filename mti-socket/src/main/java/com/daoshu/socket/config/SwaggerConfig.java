package com.daoshu.socket.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/***
 *
 */
@Configuration
@EnableSwagger2
//@Profile({"dev", "local", "sit", "uat"})
public class SwaggerConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        registry.addResourceHandler("/swagger/**").addResourceLocations("classpath:/static/swagger/");
    }

    @Bean
    public Docket createRestApi() {

        List<Parameter> pars = new ArrayList<>();

        //添加head参数start
        ParameterBuilder tokenPar = new ParameterBuilder();
        tokenPar.name("accessToken").description("令牌").
                modelRef(new ModelRef("string")).parameterType("header").required(false).build();

        ParameterBuilder tokenPar1 = new ParameterBuilder();
        tokenPar1.name("refreshToken").description("刷新令牌").
                modelRef(new ModelRef("string")).parameterType("header").required(false).build();

        pars.add(tokenPar.build());
        pars.add(tokenPar1.build());
        return new Docket(DocumentationType.SWAGGER_2)
                .globalOperationParameters(pars)
                .apiInfo(apiInfo())
                .select()
                //加了ApiOperation注解的方法，生成接口文档
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                //包下的类，生成接口文档
                .apis(RequestHandlerSelectors.basePackage("com.daoshu"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact("新智道枢", "http://mti-sh.cn/", "");
        return new ApiInfoBuilder()
                .title("新智道枢一体化合成指挥核心系统接口")
                // 详细描述
                .description("通过RESTful API方式访问新智道枢合成指挥核心系统API.")
                // 版本
                .version("2.0")
                .termsOfServiceUrl("http://mti-sh.cn/terms")
                .contact(contact)
                .build();
    }

}
