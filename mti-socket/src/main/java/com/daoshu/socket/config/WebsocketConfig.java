package com.daoshu.socket.config;

import com.daoshu.socket.notice.ISyncMessageNotice;
import com.daoshu.socket.notice.SyncMessageNoticeFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: NettyConfig
 * @description: Netty配置
 * @author: Allen
 * @create: 2019-06-14 11:36
 **/

@Data
@Configuration
@ConfigurationProperties(prefix = "websocket.server")
public class WebsocketConfig {

    /***
     * netty 服务端口号
     */
    private int port;

    private String noticeType = "redis";

    /***
     * 丢失连接次数
     */
    private Integer lossConnectCount;


    /**
     * 协议升级超时时间（毫秒），默认10000。HTTP握手升级为ws协议超时时间
     */
    private Integer upgradeTimeout = 10000;


    /***
     * Ping消息间隔（毫秒），默认25000。客户端向服务器发送一条心跳消息间隔
     */
    private Integer pingInterval = 25000;


    /***
     * Ping消息超时时间（毫秒），默认60000，这个时间间隔内没有接收到心跳消息就会发送超时事件
     */
    private Integer pingTimeout = 60000;

    /***
     * 离线消息失效事件
     */
    private Integer offlineMessageInvalid = 90;


    /***
     * 初始化同步消息类
     * @return
     */
    @Bean("syncMessageNotice")
    ISyncMessageNotice syncMessageNotice(SyncMessageNoticeFactory syncMessageNoticeFactory) {
        ISyncMessageNotice syncMessageNotice = syncMessageNoticeFactory.getInstance(noticeType);
        if (syncMessageNotice != null) {
            syncMessageNotice.initializer();
        }
        return syncMessageNotice;
    }


}
