package com.daoshu.socket.bean;

import lombok.Data;

/**
 * @ClassName: MonitorBean
 * @description:
 * @author: Allen
 * @create: 2019-06-20 10:42
 **/
@Data
public class MonitorBean {


    /***
     * 在线人数
     */
    private Long onLinePersonNum;

}
