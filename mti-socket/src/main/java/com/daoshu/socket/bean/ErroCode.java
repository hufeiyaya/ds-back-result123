package com.daoshu.socket.bean;

import lombok.Data;

@Data
public class ErroCode {


    private String code;

    private String msg;

    public ErroCode() {

    }

    public ErroCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
