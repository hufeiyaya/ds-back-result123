package com.daoshu.socket.bean;

import com.daoshu.socket.constants.CodeConstants;
import lombok.Data;

import java.io.Serializable;


/***
 *
 * @author Allen
 */
@Data
public class ResultOut<T> implements Serializable {


    public static final String SUCCESS = "操作成功";

    private static final long serialVersionUID = 1L;

    /***
     * 返回结果
     */
    private transient T data;

    /****
     * 返回code
     */
    private Integer code;

    /****
     * 消息
     */
    private String message;
    /***
     * 状态
     */
    private Boolean status;

    public ResultOut(String code, T data, String message, Boolean status) {
        super();
        this.code = Integer.valueOf(code);
        this.message = message;
        this.data = data;
        this.status = status;
    }

    public static <T> ResultOut<T> resultSuccess() {
        return new ResultOut(CodeConstants.CODE_000000, null, SUCCESS, Boolean.TRUE);
    }

    public static <T> ResultOut<T> resultSuccess(T data) {
        return new ResultOut(CodeConstants.CODE_000000, data, SUCCESS, Boolean.TRUE);
    }

    public static <T> ResultOut<T> resultSuccess(String code, String message) {
        return new ResultOut(code, null, message, Boolean.TRUE);
    }

    public static <T> ResultOut<T> resultSuccess(T data, String code, String message) {
        return new ResultOut(code, data, message, Boolean.TRUE);
    }

    public static <T> ResultOut<T> resultError(String code, String message) {
        return new ResultOut(code, null, message, Boolean.FALSE);
    }

    public static <T> ResultOut<T> resultError(ErroCode erroCode) {
        return new ResultOut(erroCode.getCode(), null, erroCode.getMsg(), Boolean.FALSE);
    }


}
