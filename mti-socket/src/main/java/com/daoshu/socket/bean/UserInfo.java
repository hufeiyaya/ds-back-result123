package com.daoshu.socket.bean;

import com.corundumstudio.socketio.SocketIOClient;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: UserInfo
 * @description:
 * @author: Allen
 * @create: 2019-06-25 16:17
 **/
@Data
public class UserInfo {

    /***
     * 用户唯一标识
     */
    private String userId;

    /***
     * 分组ID
     */
    private List<String> groupIds;

    /***
     * SessionId
     */
    private String sessionId;

    /***
     * 客户端
     */
    private SocketIOClient client;

}
