package com.daoshu.socket.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname UserTypeInfo
 * @Description TODO
 * @Date 2020/3/13 9:53
 * @Created by duchaof
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserTypeInfo {
    /**
     * 表示链接的唯一值
     */
    private String token;

    /**
     * 连接角色分类：1 情报中心； 2 警种；3 分局； 4 派出所；
     */
    private String role;
    /**
     * 对应的id
     */
    private String id;

    /**
     * 名称
     */
    private String name;
}
