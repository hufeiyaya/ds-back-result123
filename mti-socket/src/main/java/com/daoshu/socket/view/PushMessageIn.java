package com.daoshu.socket.view;

import com.corundumstudio.socketio.SocketIOClient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @ClassName: PushMessageIn
 * @description:
 * @author: Allen
 * @create: 2019-06-19 14:30
 **/
@Data
@ToString
public class PushMessageIn {

    @ApiModelProperty(value = "消息ID")
    private String id;

    @ApiModelProperty(value = "要推送的用户列表")
    private List<String> to;

    @ApiModelProperty(value = "消息类型")
    private String type;

    /***
     * 推送内容
     */
    @ApiModelProperty(value = "推送内容")
    private List<String> body;

    /***
     * 分组ID 连接websocket时传入的groupId作为分组ID
     */
    @ApiModelProperty(value = "分组ID")
    private List<String> groupId;

    @ApiModelProperty(value = "排序")
    private String sort;

    /***
     * 当前发送用户 用作排除
     */
    @JsonIgnore
    private SocketIOClient socketIOClient;

    /***
     * 重试次数
     */
    @JsonIgnore
    private int retryTimes;


    /***
     * 拓展字段
     */
    @ApiModelProperty(value = "推送消息类型")
    private String contentType;


}
