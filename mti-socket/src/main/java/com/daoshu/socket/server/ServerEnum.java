package com.daoshu.socket.server;

/**
 * @ClassName: ServerEnum
 * @description:
 * @author: Allen
 * @create: 2019-06-25 14:20
 **/
public enum ServerEnum {

    NETTY("netty", ""),
    SOCKETIO("socketio", "");

    ServerEnum(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    /***
     * 名称
     */
    private String name;

    /***
     * 描述
     */
    private String desc;


}
