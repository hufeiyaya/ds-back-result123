//package com.daoshu.socket.service;
//
//import com.baomidou.mybatisplus.mapper.EntityWrapper;
//import com.baomidou.mybatisplus.mapper.Wrapper;
//import com.daoshu.socket.SocketServiceApplication;
//import com.daoshu.socket.entity.WsMessage;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.List;
//
///**
// * @ClassName: TestWsMessageService
// * @description:
// * @author: Allen
// * @create: 2019-07-17 13:05
// **/
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = SocketServiceApplication.class)
//@Slf4j
//public class TestWsMessageService {
//
//
//    @Autowired
//    WsMessageService wsMessageService;
//
//    @Test
//    public void testSelect(){
//
//        Wrapper wrapper = new EntityWrapper();
//        List<WsMessage> list = wsMessageService.selectList(wrapper);
//        log.info(String.valueOf(list));
//    }
//
//}
