package com.daoshu.socket;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

/**
 * @ClassName: TestSocketIo
 * @description: 测试WebSocket连接
 * @author: Allen
 * @create: 2019-06-28 16:09
 **/

@Slf4j
public class TestSocketIo {


    public static final String URL = "http://10.168.4.14";

    public static Long USER_ID = 1000L;

    public static List<Socket> clientList = new LinkedList<>();


    /***
     * 并发数量
     */
    private final int CONCURRENT_NUMBER = 100;

    /***
     * 测试连接数
     */
    private final Long CONNECTION_NUMBER = 5000L;


    CountDownLatch countDownLatch = new CountDownLatch(1);

    /***
     * 测试WebSocket连接数
     */
    @Test
    public void testConnectionNum() throws InterruptedException, URISyntaxException {


        CyclicBarrier barrier = new CyclicBarrier(CONCURRENT_NUMBER, () -> {

        });


        for (int i = 0; i < CONNECTION_NUMBER; i++) {
            new TaskThread(barrier).start();
        }

        countDownLatch.await();

    }

    /***
     * 创建连接线程
     */
    static class TaskThread extends Thread {

        CyclicBarrier barrier;

        public TaskThread(CyclicBarrier barrier) {
            this.barrier = barrier;
        }

        @Override
        public void run() {
            try {
                barrier.await();
                final Socket socket = IO.socket(URL + "?token=" + (USER_ID++) + "&t=" + System.currentTimeMillis(), null);
                socket.connect();
                clientList.add(socket);
                log.info("连接数：{}", clientList.size());
            } catch (Exception e) {
                log.error("testConnectionNum err:" + e.getMessage());
            }
        }
    }

    @Test
    public void testConn1() throws URISyntaxException, InterruptedException {
        String token = "1", groupId = null;
        CountDownLatch countDownLatch = new CountDownLatch(1);
        final Socket socket = IO.socket(URL + "?token=" + token + "&groupId=" + groupId, null);
        socket.connect();
        socket.on("message", new Message());
        countDownLatch.await();
    }



    public void testConn0(String token, String groupId) throws URISyntaxException, InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        final Socket socket = IO.socket(URL + "?token=" + token + "&groupId=" + groupId, null);
        socket.connect();
        socket.on("message", new Message());
        countDownLatch.await();
    }

    @Test
    public void testConn10() throws URISyntaxException, InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    testConn0(UUID.randomUUID().toString(), null);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }).start();
        }

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    testConn0(UUID.randomUUID().toString(), "1");
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    testConn0(UUID.randomUUID().toString(), "2");
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
        countDownLatch.await();
    }


    class Message implements Emitter.Listener {
        @Override
        public void call(Object... objects) {
            System.out.println(objects.toString());
        }
    }


    private Callable<Boolean> didTheThing(){
        return () -> true;
    }


    @Test
    public void testConn() throws URISyntaxException, InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        for (int i = 0; i < 5; i++) {
            final Socket socket = IO.socket(URL + "?token=" + (USER_ID++) + "&groupId=1", null);
            socket.connect();
        }

        for (int i = 0; i < 5; i++) {
            final Socket socket = IO.socket(URL + "?token=" + (USER_ID++) + "&groupId=2", null);
            socket.connect();
        }

        countDownLatch.await();
    }

}
