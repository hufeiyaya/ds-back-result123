package com.mti;

import java.util.List;

import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.headers.Header;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Document;
import org.w3c.dom.Element;





public class AddHeaderInterceptor extends AbstractPhaseInterceptor<SoapMessage>  {
	public String userName;
	public AddHeaderInterceptor(String username) {
		super(Phase.PREPARE_SEND);
		this.userName=userName;
	}
	
	@Override
	public void handleMessage (SoapMessage message)  throws Fault {
		List<Header> header = message.getHeaders();
		//创建Document 对象
		
		Document document = DOMUtils.createDocument();
		Element element =(Element) document.createElement("authHeader");

		Element userNameElement =(Element) document.createElement("userName");
		userNameElement.setTextContent(userName);
		
		element.appendChild(userNameElement);
		Header h = new Header(new QName(""),element);
		header.add(h);
	}
}
