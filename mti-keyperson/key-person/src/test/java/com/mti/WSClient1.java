//package com.mti;
//
//import com.google.protobuf.InvalidProtocolBufferException;
//import com.mti.proto.Linkproto;
//import com.mti.proto.Linksystem;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.Test;
//import org.springframework.web.reactive.socket.WebSocketMessage;
//import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
//import org.springframework.web.reactive.socket.client.WebSocketClient;
//import reactor.core.publisher.Flux;
//
//import java.net.URI;
//import java.time.Duration;
//
//@Slf4j
//public class WSClient1 {
//
//    public static void main(final String[] args) {
//        Linkproto.LinkCmd.Builder cmd = Linkproto.LinkCmd.newBuilder();
//
//        Linksystem.LinkLoginReq.Builder login =Linksystem.LinkLoginReq.newBuilder();
//        Linksystem.LinkLoginResp.Builder loginRes = Linksystem.LinkLoginResp.newBuilder();
//        login.setStrPasswd("123456");
//        login.setStrUserName("user");
//        login.setNClientType(1);
//        cmd.setTypeValue(Linkproto.LinkCmdType.LINK_CMD_CAM_LIST_REQ_VALUE);
//        byte[] bytes = cmd.build().toByteArray();
//        final WebSocketClient client = new ReactorNettyWebSocketClient();
//        client.execute(URI.create("ws://localhost:9999/echo"), session ->
//                session.send(Flux.just(session.binaryMessage((a) -> a.wrap(bytes))))
//                .thenMany(session.receive().take(1).map(WebSocketMessage::getPayload))
//                .doOnNext(msg -> {
//                    log.error(msg.toString());
//                    Linkproto.LinkCmd linkCmd = null;
//                    try {
//                        linkCmd = Linkproto.LinkCmd.parseFrom(msg.asByteBuffer());
//                    } catch (InvalidProtocolBufferException e) {
//                        e.printStackTrace();
//                    }
//                    log.info("收到消息:{}",linkCmd);
//                })
//                .then())
//                .block(Duration.ofMinutes(1L));
//
//    }
//
//    @Test
//    public void camListReqTest() {
//        Linkproto.LinkCmd.Builder cmd = Linkproto.LinkCmd.newBuilder();
//        cmd.setTypeValue(Linkproto.LinkCmdType.CMD_PUSH_STREAM_LIST_REQ_VALUE);
//        byte[] bytes = cmd.build().toByteArray();
//        final WebSocketClient client = new ReactorNettyWebSocketClient();
//        client.execute(URI.create("ws://localhost:9999/echo"), session ->
//                session.send(Flux.just(session.binaryMessage((a) -> a.wrap(bytes))))
//                        .thenMany(session.receive().take(1).map(WebSocketMessage::getPayload))
//                        .doOnNext(msg -> {
//                            Linkproto.LinkCmd linkCmd = null;
//                            try {
//                                linkCmd = Linkproto.LinkCmd.parseFrom(msg.asByteBuffer());
//                            } catch (InvalidProtocolBufferException e) {
//                                e.printStackTrace();
//                            }
//                            log.info("收到消息:{}",linkCmd);
//                        })
//                        .then())
//                .block(Duration.ofMinutes(1L));
//
//    }
//}
