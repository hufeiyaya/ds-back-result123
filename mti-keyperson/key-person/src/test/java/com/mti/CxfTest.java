package com.mti;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.mti.dao.model.LfZdryEntity;


public class CxfTest {
	/**
	 * 测试背景解析推送数据
	 * @throws Exception
	 */
	@Test
	public void test_bjjxpostsj(List<LfZdryEntity> zdryList) {
		List<Map<String, Object>> list = new ArrayList<>();
		for (LfZdryEntity lfZdry : zdryList) {
			Map<String, Object> m = new HashMap<>();
			m.put("id",lfZdry.getId());
			m.put("xm",lfZdry.getXm());
			m.put("sfzh",lfZdry.getSfzh());
			m.put("lxdh",lfZdry.getSjh());
			m.put("lgsj",lfZdry.getCreate_timex());
			m.put("gk_lb","100");
			m.put("gklb_xl","101");
			m.put("sslx","盗窃案");
			m.put("sjly","国保");
			m.put("cycd",1);
			m.put("ryfl","人员分类");
			m.put("gkjb",1);
			m.put("fxpg",1);
			m.put("sq","");
			m.put("wkqk",lfZdry.getWkzt());
			m.put("sdclqk","");
			m.put("tsdwdm","101");
			m.put("tsdwmc","云南省昆明市");
			m.put("yxx",1);
			list.add(m);
		}
	    String data = JSONObject.toJSONString(list);
	    System.out.println(data);
	    JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
	    Client client = dcf.createClient("http://10.166.113.208:8080/qbpt-webapi/skynet/bjjx?wsdl");
	    HTTPConduit http = (HTTPConduit) client.getConduit();
	    HTTPClientPolicy clientPolicy = new HTTPClientPolicy();
	    clientPolicy.setConnectionTimeout(6 * 100 * 1000);
	    clientPolicy.setAllowChunking(false);
	    clientPolicy.setReceiveTimeout(6 * 100 * 1000);
	    http.setClient(clientPolicy);
	    Endpoint endpoint = client.getEndpoint();
	    QName name = new QName(endpoint.getService().getName().getNamespaceURI(), "pushBjjxData");
	    client.getOutInterceptors().add(new AddHeaderInterceptor("8aca19b9a0eb2c4efe97bc7ff7ed054d"));
	    Object[] obj = new Object[0];
	    try {
	        obj = client.invoke(name, data);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    System.out.println(obj[0]);
	}
	
}

