package com.mti;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;

import lombok.extern.slf4j.Slf4j;

//@Slf4j
public class PoiTest {
	public static void main(String[] args) {
		try {
			formatExcel();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void excel() throws Exception {
		// 用流的方式先读取到你想要的excel的文件
		FileInputStream fis = new FileInputStream(new File("D:\\陈福资料\\昆明市局指调二期项目\\昆明派出所辖区_xqfw.xls"));
		// 解析excel
		POIFSFileSystem pSystem = new POIFSFileSystem(fis);
		// 获取整个excel
		HSSFWorkbook hb = new HSSFWorkbook(pSystem);
		System.out.println(hb.getNumCellStyles());
		// 获取第一个表单sheet
		HSSFSheet sheet = hb.getSheetAt(0);
		// 获取第一行
		int firstrow = sheet.getFirstRowNum();
		// 获取最后一行
		int lastrow = sheet.getLastRowNum();
		String result = "";
		// 循环行数依次获取列数
		for (int i = firstrow; i < lastrow + 1; i++) {
			// 获取哪一行i
			Row row = sheet.getRow(i);
			if (row != null) {
				// 获取这一行的第一列
				int firstcell = row.getFirstCellNum();
				// 获取这一行的最后一列
				int lastcell = row.getLastCellNum();
				// 创建一个集合，用处将每一行的每一列数据都存入集合中
				List<String> list = new ArrayList<>();

				if (null != row.getCell(1)) {
					String id = row.getCell(0).toString();
					String xqfw = row.getCell(1).toString();
					String frist = xqfw.split(",")[0];
					String sql = "UPDATE t_km_jurisdiction SET xqfw = concat(ST_GeomFromText('POLYGON((" + xqfw + ","+frist
							+ "))'), 4326)  where id='" + id + "';\t";
//            	result=result+sql;
//					log.error(sql);
					System.out.println("result:" + sql);
					method1("D://sql.sql", sql);

				}

//            for (int j = firstcell; j <lastcell; j++) {
//                //获取第j列
//                Cell cell=row.getCell(j);
//                
//                if (cell!=null) {
//                    System.out.print(cell+"\t");
//                    list.add(cell.toString());
//                }
//            }

//            User user=new User();
//            if (list.size()>0) {
//                user.setUsername(list.get(1));
//                user.setPassword(list.get(2));
//            }
//            BaseDAO dao=new BaseDAO();
//            dao.save(user);
			}
		}
//        System.out.println("result:"+result);
		fis.close();
	}

	public static void formatExcel() throws Exception {
		// 用流的方式先读取到你想要的excel的文件
		FileInputStream fis = new FileInputStream(new File("D:\\陈福资料\\昆明市局指调二期项目\\昆明派出所辖区数据\\昆明派出所辖区——新（替换过分号的-留经纬度和ID列）.xls"));
		// 解析excel
		POIFSFileSystem pSystem = new POIFSFileSystem(fis);
		// 获取整个excel
		HSSFWorkbook hb = new HSSFWorkbook(pSystem);
		System.out.println(hb.getNumCellStyles());
		// 获取第一个表单sheet
		HSSFSheet sheet = hb.getSheetAt(0);
		// 获取第一行
		int firstrow = sheet.getFirstRowNum();
		// 获取最后一行
		int lastrow = sheet.getLastRowNum();
		// 循环行数依次获取列数
		for (int i = firstrow; i < lastrow + 1; i++) {
			String  result = "";
			// 获取哪一行i
			Row row = sheet.getRow(i);
			if (row != null) {
				// 获取这一行的第一列
				int firstcell = row.getFirstCellNum();
				// 获取这一行的最后一列
				int lastcell = row.getLastCellNum();
				// 创建一个集合，用处将每一行的每一列数据都存入集合中
				List<String> list = new ArrayList<>();

				if (null != row.getCell(1)) {
					String id = row.getCell(0).toString();
					String xqfw = row.getCell(1).toString();
					xqfw=xqfw.replace(";", ",");
					String [] xqfws= xqfw.split(",");
					for( int j=0;j<xqfws.length;j++) {
						
						if(j % 2 == 0){
							if(StringUtils.isEmpty(result)) {
								
								result=result+ xqfws[j] ;
							}else {
								result=result+","+xqfws[j] ;
							}
						}else {
							result=result+" "+xqfws[j];
						}
					}
					if(id.equals("75B88B398050F179E05344005156F1F3")) {
						System.out.println("result:"+result);
					}
					String frist = xqfw.split(",")[0];
					String second = xqfw.split(",")[1];
					String sql = "UPDATE t_km_jurisdiction SET xqfw = ST_GeomFromText('POLYGON((" + result.toString() + ","+frist+" "+second
							+ "))', 4326)  where id='" + id + "';\t";
//					result.append(";\\t");
//System.out.println("result:"+result);
					method1("D:\\陈福资料\\昆明市局指调二期项目\\昆明派出所辖区数据\\xqfw.sql", sql);
				}

			}
		}
//        System.out.println("result:"+result);
		fis.close();
	}

	/**
	 * 使用FileOutputStream来写入txt文件
	 * 
	 * @param txtPath txt文件路径
	 * @param content 需要写入的文本
	 */
	public static void writeTxt(String txtPath, String content) {
		FileOutputStream fileOutputStream = null;
		File file = new File(txtPath);
		try {
			if (file.exists()) {
				// 判断文件是否存在，如果不存在就新建一个txt
				file.createNewFile();
			}
			fileOutputStream = new FileOutputStream(file);
			fileOutputStream.write(content.getBytes());

			fileOutputStream.flush();
			fileOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void method1(String txtPath, String content) {
		FileWriter fw = null;
		try {
			// 如果文件存在，则追加内容；如果文件不存在，则创建文件
			File f = new File(txtPath);
			fw = new FileWriter(f, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		PrintWriter pw = new PrintWriter(fw);
		pw.println(content);
		pw.flush();
		try {
			fw.flush();
			pw.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
