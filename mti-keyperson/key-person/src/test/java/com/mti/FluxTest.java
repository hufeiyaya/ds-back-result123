package com.mti;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

@Slf4j
public class FluxTest {

    public static void main(String[] args){
        AtomicInteger ai = new AtomicInteger();
        Function<Flux<String>, Flux<String>> filterAndMap = f -> {
            if (ai.incrementAndGet() == 1) {
                return f.filter(color -> !color.equals("orange"))
                        .map(String::toLowerCase);
            }
            return f.filter(color -> !color.equals("purple"))
                    .map(String::toUpperCase);
        };

        Flux<String> composedFlux =
                Flux.fromIterable(Arrays.asList("blue", "green", "orange", "purple"))
                        .compose(filterAndMap);//1

        composedFlux.subscribe(d -> log.error("Subscriber 1 to Composed MapAndFilter :" + d));//2
        composedFlux.subscribe(d -> log.error("Subscriber 2 to Composed MapAndFilter: " + d));//3
       /* Flux.range(1,10)
                .parallel(4)
                .runOn(Schedulers.parallel())
                .subscribe(i -> log.error(Thread.currentThread().getName() + "->" + i));*/
    }
}
