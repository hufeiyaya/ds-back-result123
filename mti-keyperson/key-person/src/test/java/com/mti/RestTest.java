package com.mti;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mti.configuration.SystemConfig;

public class RestTest {
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private SystemConfig systemConfig;
	@Test
	public   void test( ) {
		
		ResponseEntity result = restTemplate.getForEntity(
				systemConfig.getSystemServer() + "/api/system/organization/getOrgById?id=530122000000",
				JSONObject.class);
		Map<String, String> returnMap = new HashMap<>();
		if (null != result) {
			if (null != result.getBody()) {
				JSONObject obj = JSONObject.parseObject(result.getBody().toString());
				List<Map> res = JSONArray.parseArray(obj.get("data").toString(), Map.class);
				System.out.println(res);
			}
		}
	}
}
