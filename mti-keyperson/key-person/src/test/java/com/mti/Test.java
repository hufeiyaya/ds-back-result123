package com.mti;

import java.io.File;

public class Test {
	/**
     * 通过文件路径直接修改文件名
     * 
     * @param filePath    需要修改的文件的完整路径
     * @param newFileName 需要修改的文件的名称
     * @return
     */
    private static String FixFileName(String filePath, String newFileName) {
        File f = new File(filePath);
        if (!f.exists()) { // 判断原文件是否存在（防止文件名冲突）
            return null;
        }
        newFileName = newFileName.trim();
        if ("".equals(newFileName) || newFileName == null) // 文件名不能为空
            return null;
//        String newFilePath = null;
        File nf = new File(newFileName);
        try {
            f.renameTo(nf); // 修改文件名
        } catch (Exception err) {
            err.printStackTrace();
            return null;
        }
        return newFileName;
    }
    
    
    public static void main(String[] args) {
    	String path="F:\\temp\\jpg\\";
    	File file = new File(path);		//获取其file对象
		File[] fs = file.listFiles();	//遍历path下的文件和目录，放在File数组中
		for(File f:fs){					//遍历File[]数组
			if(!f.isDirectory()&&f.getName().contains("_")) {	//若非目录(即文件)，则打印
				  String  newFileName1 = f.getName().split("_")[0];
				  FixFileName(path+f.getName(),path+"new\\"+newFileName1+".jpg");
			}	
		}
    	
    	
    	
    	//FixFileName();
	}
}
