package com.mti.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.mti.dao.model.KmAlarmAreaEntity;
import com.mti.dao.model.TSwitchTypeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * <p>
 * 预警设置过滤表
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */
@Data
@ApiModel(value = "预警设置VO")
@AllArgsConstructor
@NoArgsConstructor
public class TSwitchTypeVo extends Model<TSwitchTypeVo> {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "开关数组")
    List<TSwitchTypeEntity> list;
    @ApiModelProperty(value = "开始时间")
    String startTime;
    @ApiModelProperty(value = "结束时间")
    String endTime;
}
