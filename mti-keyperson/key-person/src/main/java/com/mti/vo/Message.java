package com.mti.vo;

import lombok.Data;
import org.springframework.web.reactive.socket.WebSocketMessage;

import java.util.List;

@Data
public class Message {

    /** 消息ID，唯一的标识一条 UUID */
    private String msgId;
    /** 消息号，*/
    private String msgCode;
    /** 消息名称 */
    private String msgName;
    /** 消息类型 */
    private WebSocketMessage.Type type = WebSocketMessage.Type.BINARY;
    /** 发送方 */
    private String source;
    /** 消息 */
    private String msgSource;
    /** 接收方 */
    private List<String> target;
    /** 消息的数据体 */
    private byte[] content;


}
