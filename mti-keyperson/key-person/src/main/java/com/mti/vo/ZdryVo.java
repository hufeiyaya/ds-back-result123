/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.vo;

import lombok.Data;

@Data
public class ZdryVo {

    private String id;
    private String sfzh;
    private String x;
    private String y;
    private String rylbx;
    private String gkjb;
    private String xm;
    private String hjdX;
    private String hjdY;

}
