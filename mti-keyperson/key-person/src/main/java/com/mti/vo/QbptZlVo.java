package com.mti.vo;

import lombok.Data;

@Data
public class QbptZlVo {

    private String id;
    // 数据id
    private String dataid;
    // 指令id
    private String zlid;
    // 指令编号
    private String zlbh;
    // 下发日期
    private String xfrq;
    // 紧急程度
    private String jjcd;
    // 标题
    private String bt;
    // 内容
    private String zlnr;
    // 线索来源
    private String xsly;
    // 涉及群体
    private String sjqt;
    // 工作措施
    private String gzcs;
    // 签收时限
    private String qssx;
    // 反馈时限
    private String fksx;

}
