package com.mti.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PredictStatisticsDataItem {
    //所属辖区
    private String ssxq;
    //所属辖区ID
    private String ssxqId;
    //派出所ID
    private String oid;
    //派出所名称
    private String oname;
    //重点人员总数
    private long total;
    //在控总数
    private long isControlCount;
    //失控总人数
    private long loseControlCount;
    //一级在控人数
    private long oneLevelIsControlCount;
    //二级在控人数
    private long twoLevelIsControlCount;
    //三级在控人数
    private long threeLevelIsControlCount;
    //一级失控人数
    private long oneLevelLoseControlCount;
    //二级失控人数
    private long twoLevelLoseControlCount;
    //三级失控人数
    private long threeLevelLoseControlCount;
    //非法上访人数
    private long illegalReportCount;
    //涉恐人数
    private long terroristCount;
    //涉军人数
    private long armyCount;
    //其他人员
    private long otherCount;
    //其他触网人员
    private long otherEventCount;
    //涉政人数
    private long politicalCount;
    //精神病人数
    private long insanityCount;
    //刑侦人数
    private long criminalInvestCount;
    //涉警人数
    private long policeCount;
    //刑解人数
    private long criminalReleaseCount;
    //经侦人数
    private long dealInvestCount;
    //非法上访触网人数
    private long illegalEventReportCount;
    //涉恐触网人数
    private long terroristEventCount;
    //涉军触网人数
    private long armyEventCount;
    //涉政触网人数
    private long politicalEventCount;
    //精神病触网人数
    private long insanityEventCount;
    //刑侦触网人数
    private long criminalInvestEventCount;
    //涉警触网人数
    private long policeEventCount;
    //刑解触网人数
    private long criminalReleaseEventCount;
    //经侦触网人数
    private long dealInvestEventCount;
    //是否父级
    private Boolean isParent;
    //未触网
    private long unDropCount;
    //触网
    private long dropCount;
    //人脸报警触网
    private long faceDropCount;
    //人证核验触网
    private long cardDropCount;
    //大巴售票触网
    private long busDropCount;
    //失控报警触网
    private long uncontrolDropCount;
    //平安社区触网
    private long peaceDropCount;
    //技侦触网
    private long jzDropCount;
    //智慧社区触网
    private long smartDropCount;
    //检查站触网
    private long checkDropCount;
    //汽车购票触网
    private long ticketDropCount;
    //电子围栏触网
    private long barrierDropCount;
    //移动警务触网
    private long mobileDropCount;
    //酒店住宿触网
    private long hotelDropCount;
    //子级列表
    private List<PredictStatisticsDataItem> childList;
}
