package com.mti.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/21
 * @change
 * @describe describe
 **/
@Data
@ApiModel(value = "布防任务请求VO")
@AllArgsConstructor
@NoArgsConstructor
public class ArmingTaskDetailVO implements Serializable {
    private static final long serialVersionUID = 2421074799420160685L;
    @ApiModelProperty(value = "关键字")
    private String name;
    @ApiModelProperty(value = "姓名")
    private String personName;
    @ApiModelProperty(value = "身份证号")
    private String identityId;
    @ApiModelProperty(value = "手机号")
    private String phoneNumber;
    @ApiModelProperty(value = "是否在控")
    private String isControl;
    @ApiModelProperty(value = "所属分局(所属辖区)")
    private String ssxq;
    @ApiModelProperty(value = "所属派出所(派出所ID)")
    private String oid;
    @ApiModelProperty(value = "人员类型")
    private String rylx;
    @ApiModelProperty(value = "责任部门")
    private String zrbm;
    @ApiModelProperty(value = "上报单位")
    private String reportOrg;
    @ApiModelProperty(value = "任务ID")
    private String taskId;
    @ApiModelProperty(value = "审批状态(多状态筛选使用英文逗号隔开即可)")
    private String approveStatus;
    @ApiModelProperty(value = "当前页")
    private Integer page;
    @ApiModelProperty(value = "页大小")
    private Integer size;
}
