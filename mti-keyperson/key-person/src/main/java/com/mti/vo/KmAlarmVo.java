package com.mti.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.mti.dao.model.KmAlarmAreaEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * <p>
 * 预警设置过滤表
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */
@Data
@ApiModel(value = "预警设置VO")
@AllArgsConstructor
@NoArgsConstructor
public class KmAlarmVo extends Model<KmAlarmVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "人脸识别")
    private boolean rlsb;
    @ApiModelProperty(value = "手机预警")
    private boolean phyj;
    @ApiModelProperty(value = "区域数组")
    private List<KmAlarmAreaEntity> areaList;


}
