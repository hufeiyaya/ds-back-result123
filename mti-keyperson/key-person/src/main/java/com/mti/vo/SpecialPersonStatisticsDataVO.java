package com.mti.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/22
 * @change
 * @describe describe
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "重点人员统计数据实体")
public class SpecialPersonStatisticsDataVO implements Serializable {
    private static final long serialVersionUID = 7052946183080851112L;

    //市局名称
    @ApiModelProperty(value = "市局名称")
    String orgName;
    @ApiModelProperty(value = "市局ID")
    String orgId;
    //重点人员总数
    @ApiModelProperty(value = "重点人员总数")
    private long totalCount;
    //在控总数
    @ApiModelProperty(value = "在控总数")
    private long controlCount;
    //失控总数
    @ApiModelProperty(value = "失控总数")
    private long loseCount;
    //查找中总数
    @ApiModelProperty(value = "查找中总数")
    private long unReportCount;
    //是否分局
    @ApiModelProperty(value = "是否父级")
    private boolean isParent;
    //高级在控
    @ApiModelProperty(value = "高级在控")
    private long highLevelControlCount;
    //中级在控
    @ApiModelProperty(value = "中级在控")
    private long middleLevelControlCount;
    //低级在控
    @ApiModelProperty(value = "低级在控")
    private long lowLevelControlCount;
    //灰名单在控
    @ApiModelProperty(value = "灰名单在控")
    private long grayLevelControlCount;
    //已死亡在控
    @ApiModelProperty(value = "已死亡在控")
    private long deadLevelControlCount;
    //高级总数
    @ApiModelProperty(value = "高级总数")
    private long highLevelCount;
    //中级总数
    @ApiModelProperty(value = "中级总数")
    private long middleLevelCount;
    //、低级总数
    @ApiModelProperty(value = "低级总数")
    private long lowLevelCount;
    //灰名单总数
    @ApiModelProperty(value = "灰名单总数")
    private long grayLevelCount;
    //已死亡总数
    @ApiModelProperty(value = "已死亡总数")
    private long deadLevelCount;
    //派出所ID
    @ApiModelProperty(value = "派出所ID")
    private String oid;
    //派出所名称
    @ApiModelProperty(value = "派出所名称")
    private String oname;
    // 派处所ID
    @ApiModelProperty(value = "派出所Id")
    private String sspcsid;
    //涉政
    @ApiModelProperty(value = "涉政")
    private long politicalCount;
    //涉港
    @ApiModelProperty(value = "涉港")
    private long hongKongCount;
    //涉藏
    @ApiModelProperty(value = "涉藏")
    private long tibetCount;
    //涉邪
    @ApiModelProperty(value = "涉邪")
    private long cultsCount;
    //涉恐
    @ApiModelProperty(value = "涉恐")
    private long terroristCount;
    //涉稳
    @ApiModelProperty(value = "涉稳")
    private long stableCount;
    //涉众
    @ApiModelProperty(value = "涉众")
    private long personsCount;
    //涉访
    @ApiModelProperty(value = "涉访")
    private long reportCount;
    //涉网
    @ApiModelProperty(value = "涉网")
    private long netCount;
    //涉车
    @ApiModelProperty(value = "涉车")
    private long carCount;
    //涉毒
    @ApiModelProperty(value = "涉毒")
    private long drugCount;
    //精神病
    @ApiModelProperty(value = "精神病")
    private long innormalCount;
    //个人极端
    @ApiModelProperty(value = "个人极端")
    private long extremeCount;
    //犯罪前科
    @ApiModelProperty(value = "犯罪前科")
    private long criminalRecordCount;
    //其他
    @ApiModelProperty(value = "其他")
    private long otherCount;


    //涉政
    @ApiModelProperty(value = "涉政")
    private long politicalControlCount;
    //涉港
    @ApiModelProperty(value = "涉港")
    private long hongKongControlCount;
    //涉藏
    @ApiModelProperty(value = "涉藏")
    private long tibetControlCount;
    //涉邪
    @ApiModelProperty(value = "涉邪")
    private long cultsControlCount;
    //涉恐
    @ApiModelProperty(value = "涉恐")
    private long terroristControlCount;
    //涉稳
    @ApiModelProperty(value = "涉稳")
    private long stableControlCount;
    //涉众
    @ApiModelProperty(value = "涉众")
    private long personsControlCount;
    //涉访
    @ApiModelProperty(value = "涉访")
    private long reportControlCount;
    //涉网
    @ApiModelProperty(value = "涉网")
    private long netControlCount;
    //涉车
    @ApiModelProperty(value = "涉车")
    private long carControlCount;
    //涉毒
    @ApiModelProperty(value = "涉毒")
    private long drugControlCount;
    //精神病
    @ApiModelProperty(value = "精神病")
    private long innormalControlCount;
    //个人极端
    @ApiModelProperty(value = "个人极端")
    private long extremeControlCount;
    //犯罪前科
    @ApiModelProperty(value = "犯罪前科")
    private long criminalRecordControlCount;
    //其他
    @ApiModelProperty(value = "其他")
    private long otherControlCount;


    //下层分局集合
    @ApiModelProperty(value = "子级数据")
    private List<SpecialPersonStatisticsDataVO> childList;
}
