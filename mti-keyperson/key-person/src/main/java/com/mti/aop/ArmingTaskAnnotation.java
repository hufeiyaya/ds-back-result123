package com.mti.aop;

import java.lang.annotation.*;

/**
 * @company 上海道枢信息科技-->
 * @author created by zhangmingxin
 * @date 2019/9/12
 * @change
 * @describe 拦截注解
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ArmingTaskAnnotation {
    String operation() default "";
}
