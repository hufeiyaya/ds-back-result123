package com.mti.aop;

import com.mti.component.snowflake.KeyWorker;
import com.mti.dao.model.BaseEntity;
import com.mti.dao.model.Necessary;
import lombok.AllArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/8
 * @change
 * @describe describe
 **/
@Aspect
@AllArgsConstructor
@Component
public class SetIdAop {
    @Pointcut("execution(* com.baomidou.mybatisplus.extension.service.IService.save(..))")
    public void cut(){}

    @Around(value = "cut()")
    public Object beforeSaveSetId(ProceedingJoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        if(args.length>0){
            Object obj = args[0];
            if((obj instanceof BaseEntity) && (obj instanceof Necessary)){
                BaseEntity entity = (BaseEntity) obj;
                entity.setId(String.valueOf(KeyWorker.nextId()));
                args[0] = entity;
            }
        }
        try {
            return joinPoint.proceed(args);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        }
    }
}
