//package com.mti.aop;
//
//import lombok.extern.slf4j.Slf4j;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
//import org.springframework.expression.EvaluationContext;
//import org.springframework.expression.Expression;
//import org.springframework.expression.ExpressionParser;
//import org.springframework.expression.spel.standard.SpelExpressionParser;
//import org.springframework.expression.spel.support.StandardEvaluationContext;
//import org.springframework.stereotype.Component;
//
//import java.lang.reflect.Method;
//
///**
// * @company 上海道枢信息科技-->
// * @anthor created by zhangmingxin
// * @date 2019/9/12
// * @change
// * @describe
// **/
//@Component
//@Aspect
//@Slf4j
//public class ArmingTaskLogAop {
//
//    @Pointcut(value="@annotation(ArmingTaskAnnotation)")
//    private void getPointcut() {
//
//    }
//
//    @Around("getPointcut()")
//    public Object preProcessQueryPattern(ProceedingJoinPoint point) throws Throwable {
//        String targetName = point.getTarget().getClass().getName();
//        String methodName = point.getSignature().getName();
//        Object[] arguments = point.getArgs();
//        Class targetClass = Class.forName(targetName);
//        Method[] methods = targetClass.getMethods();
//        String key = "";
//        String[] paramNames = {};
//        for(Method method:methods){
//            if(method.getName().equals(methodName)){
//                key = method.getAnnotation(ArmingTaskAnnotation.class).operation();
//                System.out.println("key is "+key);
//                log.info("key is : {}",key);
//                paramNames = getParamterNames(method);
//            }
//        }
//
//        ExpressionParser parser = new SpelExpressionParser();
//        Expression expression = parser.parseExpression(key);
//        EvaluationContext context = new StandardEvaluationContext();
//        for(int i=0;i<arguments.length;i++){
//            context.setVariable(paramNames[i],arguments[i]);
//        }
//        Object object = expression.getValue(context,String.class);
//        System.out.println();
//        return point.proceed();
//    }
//
//    public String[] getParamterNames(Method method){
//        LocalVariableTableParameterNameDiscoverer u =
//                new LocalVariableTableParameterNameDiscoverer();
//        return  u.getParameterNames(method);
//    }
//
//}
