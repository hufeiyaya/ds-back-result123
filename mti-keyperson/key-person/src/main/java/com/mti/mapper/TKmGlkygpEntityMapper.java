package com.mti.mapper;


import com.mti.dao.model.TKmGlkygpEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.utils.ZdryQxzkUtilBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TKmGlkygpEntityMapper {
    int deleteByPrimaryKey(String pgPkid);

    int insert(TKmGlkygpEntity record);

    int insertSelective(TKmGlkygpEntity record);

    TKmGlkygpEntity selectByPrimaryKey(String pgPkid);

    int updateByPrimaryKeySelective(TKmGlkygpEntity record);

    int updateByPrimaryKey(TKmGlkygpEntity record);

    List<TKmGlkygpEntity> getGlkygpBySfzh(@Param("sfzf") String sfzf);

    /**
     * 获取客运出昆明的数量
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getKyOut(@Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 获取客运进昆明的数量
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getKyIn(@Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 获取公路客运订票数据
     * @param glkyEntities
     * @return
     */
    List<TKmGlkygpEntity> getGlkygpList(List<String> glkyEntities);
}