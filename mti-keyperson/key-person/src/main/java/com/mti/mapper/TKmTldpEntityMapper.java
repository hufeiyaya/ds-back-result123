package com.mti.mapper;


import com.mti.dao.model.TKmTldpEntity;
import com.mti.utils.ZdryQxzkUtilBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TKmTldpEntityMapper  {

    int insert(TKmTldpEntity record);

    int insertSelective(TKmTldpEntity record);

    TKmTldpEntity selectByPrimaryKey(@Param("xxzjbh") String xxzjbh);

    List<TKmTldpEntity> getTldpListBySfzh(@Param("sfzh") String sfzh);

    /**
     * 铁路出昆明
     * @param startTime
     * @param endTime
     * @return
     */
    List<ZdryQxzkUtilBean> getTlOut(@Param("startTime") String startTime, @Param("endTime") String endTime);

    /**
     * 铁路进昆明
     * @param startTime
     * @param endTime
     * @return
     */
    List<ZdryQxzkUtilBean> getTlIn(@Param("startTime") String startTime, @Param("endTime") String endTime);

    /**
     * 根据主键集合获取铁路订票数据
     * @param tldpEntities
     * @return
     */
    List<TKmTldpEntity> getTldpEntityList(List<String> tldpEntities);
}