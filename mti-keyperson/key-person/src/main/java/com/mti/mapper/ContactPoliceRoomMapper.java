package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.ContactPoliceRoomEntity;

/**
 * <p>
 *  警务室巡区信息 Mapper接口
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-26
 */
public interface ContactPoliceRoomMapper extends BaseMapper<ContactPoliceRoomEntity> {

}
