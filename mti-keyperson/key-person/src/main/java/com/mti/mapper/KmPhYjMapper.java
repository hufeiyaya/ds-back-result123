package com.mti.mapper;

import com.mti.dao.model.KmPhYjEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 手机预警 Mapper 接口
 * </p>
 *
 * @author duchao
 * @since 2020-04-29
 */
public interface KmPhYjMapper extends BaseMapper<KmPhYjEntity> {

}
