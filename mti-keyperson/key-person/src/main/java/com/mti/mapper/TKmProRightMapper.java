package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.TKmProEntity;

/**
 * @Classname TKmProRightMapper
 * @Description TODO
 * @Date 2020/2/20 16:50
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmProRightMapper extends BaseMapper<TKmProEntity> {
}
