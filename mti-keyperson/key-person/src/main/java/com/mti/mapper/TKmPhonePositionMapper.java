package com.mti.mapper;

import com.mti.dao.model.TkmPhonePositionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.qo.LfZdryQO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 手机定位 Mapper 接口
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
public interface TKmPhonePositionMapper extends BaseMapper<TkmPhonePositionEntity> {

    List<TkmPhonePositionEntity> queryZdryPhonePosition(@Param("qo") LfZdryQO qo);

    /**
     * 查询手机定位详情
     * @param sfzhs
     * @return
     */
    List<Map<String, Object>> queryPhoneLocationInfo(List<String> sfzhs);
}
