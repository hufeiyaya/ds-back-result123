package com.mti.mapper;


import com.mti.dao.model.TKmWbswEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TKmWbswEntityMapper {
    int deleteByPrimaryKey(String xxzjbh);

    int insert(TKmWbswEntity record);

    int insertSelective(TKmWbswEntity record);

    TKmWbswEntity selectByPrimaryKey(String xxzjbh);

    int updateByPrimaryKeySelective(TKmWbswEntity record);

    int updateByPrimaryKey(TKmWbswEntity record);

    List<TKmWbswEntity> getWbswListBySfzh(@Param("sfzf") String sfzf);

    /**
     * 获取网吧撒点信息
     * @param lfRyyjsjQO
     * @return
     */
    List<Map<String, Object>> queryWbLgKyNumber(@Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 获取网吧导出数据
     * @param wbswEntities
     * @return
     */
    List<TKmWbswEntity> getWbswList(List<String> wbswEntities);

    /**
     * 查询网吧定位详细信息
     * @param sfzhs
     * @return
     */
    List<Map<String, Object>> queryWbswLocationInfo(List<String> sfzhs);

    /**
     * 查询网吧 登录和退出时间
     * @param wybs
     * @return
     */
    Map<String, Object> findWbswStartAndEndTime(@Param("wybs") String wybs);
}