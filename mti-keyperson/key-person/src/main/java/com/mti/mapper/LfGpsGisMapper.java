package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.LfGpsGisEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface LfGpsGisMapper extends BaseMapper<LfGpsGisEntity> {

    LfGpsGisEntity getByGpsid(@Param("gpsid") String gpsid);

    int saveByEntity(LfGpsGisEntity entity);

    int updateByEntity(LfGpsGisEntity entity);

}
