package com.mti.mapper;

import com.mti.dao.model.KmZdryCustomEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 重点人员-自定义字段表 Mapper 接口
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
public interface KmZdryCustomMapper extends BaseMapper<KmZdryCustomEntity> {

}
