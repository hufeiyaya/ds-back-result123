package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.LfZdryModifyLogEntity;

public interface LfZdryModifyLogMapper extends BaseMapper<LfZdryModifyLogEntity> {
}
