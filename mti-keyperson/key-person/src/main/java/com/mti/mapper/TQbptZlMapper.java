/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.model.TQbptZlEntity;
import com.mti.dao.qo.TQbptQO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangsp
 * @since 2019-06-25
 */
public interface TQbptZlMapper extends BaseMapper<TQbptZlEntity> {

    IPage<TQbptZlEntity> queryPage(Page<?> page, @Param("qo") TQbptQO qo);
}
