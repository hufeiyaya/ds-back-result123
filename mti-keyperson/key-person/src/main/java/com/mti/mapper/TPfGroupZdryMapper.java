package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.model.LfZdryBaseEntity;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.TPfGroupEntity;
import com.mti.dao.model.TPfGroupZdryEntity;
import com.mti.dao.qo.TPfGroupZdryQo;
import com.mti.dao.vo.TPfGroupZdryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 重点人员分组表 Mapper 接口
 * </p>
 *
 * @author hf
 * @since 2020-03-25
 */

public interface TPfGroupZdryMapper extends BaseMapper<TPfGroupZdryEntity> {

    boolean addZdryToGroup(TPfGroupZdryEntity tPfGroupZdryEntity);

    boolean getByGroupId(@Param("groupId") String groupId,@Param("zdryId") String zdryId);

    IPage<LfZdryBaseEntity> getPageList(Page<?> page,@Param("qo") TPfGroupZdryQo qo);
}
