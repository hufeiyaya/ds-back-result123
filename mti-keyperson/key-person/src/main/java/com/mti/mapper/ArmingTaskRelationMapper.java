package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.ArmingTaskRelationEntity;
import com.mti.dao.model.LfZdryArmingTaskEntity;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/21
 * @change
 * @describe describe
 **/
public interface ArmingTaskRelationMapper extends BaseMapper<ArmingTaskRelationEntity> {
    List<ArmingTaskRelationEntity> getAllSpecialPersonIdsByTaskId(
            @Param(value = "taskId")String taskId,
            @Param(value = "reportOrg")String reportOrg,
            @Param(value = "type")String type,
            @Param(value = "approveStatus")List<String> approveStatus
    );

    int delSpecialPersonFromArmingTask(@Param(value = "taskId")String taskId,@Param(value = "specialPersonId")String specialPersonId,@Param(value = "reportOrg")String reportOrg,@Param(value = "type")String type);

    int addSpecialPersonToArmingTask(@Param(value = "id")String id,@Param(value = "taskId")String taskId,@Param(value = "specialPersonId")String specialPersonId,@Param(value = "reportOrg")String reportOrg,@Param(value = "type")String type);

    int addSpySpecialPersonToArmingTask(@Param(value = "id")String id,@Param(value = "taskId")String taskId,@Param(value = "specialPersonId")String specialPersonId,@Param(value = "reportOrg")String reportOrg,@Param(value = "type")String type);

    List<LfZdryArmingTaskEntity> listTask(
            @Param(value = "page") Integer page,
            @Param(value = "size")Integer size,
            @Param(value = "taskName")String taskName,
            @Param(value = "specialLevel")Integer specialLevel,
            @Param(value = "status")Integer status,
            @Param(value = "armingRange")String armingRange,
            @Param(value = "armingRangeId")String armingRangeId,
            @Param(value = "orgId")String orgId,
            @Param(value = "oid")String oid,
            @Param(value = "zrbm")String zrbm,
            @Param(value = "approveStatus")List<String> approveStatus
    );

    long listTaskCountRows(
            @Param(value = "taskName")String taskName,
            @Param(value = "specialLevel")Integer specialLevel,
            @Param(value = "status")Integer status,
            @Param(value = "armingRange")String armingRange,
            @Param(value = "armingRangeId")String armingRangeId,
            @Param(value = "orgId")String orgId,
            @Param(value = "oid")String oid,
            @Param(value = "zrbm")String zrbm
    );

    /**
     * 更新状态为目标状态
     * @param taskId 任务ID
     * @param reportOrg 上报单位
     * @param status 目标状态
     */
    void updateSpecialPersonToTargetStatus(@Param(value = "taskId")String taskId,@Param(value = "reportOrg")String reportOrg,@Param(value = "status")Integer status,@Param(value = "rids")List<String> rids);

    List<Map<String,Object>> getReportedOrg(@Param(value = "taskId")String taskId);

}
