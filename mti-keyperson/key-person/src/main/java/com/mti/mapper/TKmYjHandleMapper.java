package com.mti.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.model.TKmYjHandleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.qo.TkmYjHanQo;
import com.mti.dao.vo.TKmYjHandleVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 预警处置表 Mapper 接口
 * </p>
 *
 * @author duchaof
 * @since 2020-03-11
 */
public interface TKmYjHandleMapper extends BaseMapper<TKmYjHandleEntity> {

    /**
     * 查询预警处置类别信息
     * @param tkmYjHanQo
     * @return
     */
    List<TKmYjHandleVo> listTKmYjHandle(@Param("q")  TkmYjHanQo tkmYjHanQo);

    /**
     * 分页查询 预警信息
     * @param page
     * @param tkmYjHanQo
     * @return
     */
    IPage<TKmYjHandleVo> pageTKmYjHandle(Page<?> page, @Param("q") TkmYjHanQo tkmYjHanQo);
}
