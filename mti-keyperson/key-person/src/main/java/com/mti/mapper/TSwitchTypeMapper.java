package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.TSwitchTypeEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author hf
 * @Title: 开关设置表
 * @Description: TODO (开关设置)
 * @Param
 * @return
 * @Date 2020/11/23 15:32
 */
public interface TSwitchTypeMapper extends BaseMapper<TSwitchTypeEntity> {
    List<TSwitchTypeEntity>  getSwitchByName(@Param("typeList") List typeList,@Param("name")  String name);

    List<TSwitchTypeEntity> getByParentKey(@Param("parentKey") String parentKey);

    String getTimeKey(@Param("key") String key);
}
