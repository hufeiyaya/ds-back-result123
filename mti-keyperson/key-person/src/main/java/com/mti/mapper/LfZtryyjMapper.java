package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.model.LfZtryyjEntity;
import com.mti.dao.qo.KmAlarmFilterQo;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.dao.qo.YjInfoParamQo;
import com.mti.dao.vo.ZdryTxVo;
import com.mti.dao.vo.ZdryYjPushDataVo;
import com.mti.dao.vo.ZdryYjStatisticsVo;
import com.mti.utils.ZdryQxzkUtilBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LfZtryyjMapper extends BaseMapper<LfZtryyjEntity> {

    /**
     * 查询重点人员预警信息
     * @param page
     * @param qo
     * @return
     */
    IPage<LfZtryyjEntity> getZdryYjXx(Page<?> page,@Param("qo") LfRyyjsjQO qo);

    /**
     * 查询重点人员预警信息导出数据
     * @param lfRyyjsjQO
     * @return
     */
    List<LfZtryyjEntity> getZdryExportData(@Param("qo") LfRyyjsjQO lfRyyjsjQO);


    /**
     * 根据添加查询满足添加的身份证号
     * @param lfRyyjsjQO
     * @return
     */
    List<String> getSfzhByCondition(@Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 通过预警标识 查询重点人员的相关信息
     * @param refId
     * @return
     */
    Map<String, String> getZdryInfoByYjwybs(@Param("refId") String refId);

    /**
     * 查询超时的预警信息
     * @param page
     * @param qo
     * @return
     */
    IPage<LfZtryyjEntity> getYjTimeOutInfo(Page<?> page, @Param("qo") LfRyyjsjQO qo);

    /**
     * 根据迁徙图查询对应的信息
     * @param page
     * @param lfRyyjsjQO
     * @param param
     * @return
     */
    IPage<ZdryTxVo> queryTlKyMhQxInfo(Page<?> page, @Param("qo") LfRyyjsjQO lfRyyjsjQO, @Param("q") List<ZdryQxzkUtilBean> param);

    /**
     * 导出根据迁徙图上重点人员的预警信息
     * @param lfRyyjsjQO
     * @param param
     * @return
     */
    List<ZdryTxVo> exportYjTxInfo(@Param("qo")LfRyyjsjQO lfRyyjsjQO, @Param("q")List<ZdryQxzkUtilBean> param);

    /**
     * 查询网吧 旅馆撒点撒点的信息
     * @param page
     * @param paramMap
     * @return
     */
    IPage<ZdryTxVo> queryLocationInfo(Page<?> page, @Param("yjxx") List<YjInfoParamQo> paramMap);

    /**
     *
     * @param paramMap
     * @return
     */
    List<ZdryTxVo> queryExportLocationInfo(@Param("yjxx")List<YjInfoParamQo> paramMap);

    /**
     * 根据身份证查询预警信息
     * @param page
     * @param sfzh
     * @return
     */
    IPage<Map<String, Object>> getZdryAllYjBySfzh(Page<?> page, @Param("sfzh") String sfzh);

    /**
     * 根据身份证查询预警信息(不分页)
     * @param page
     * @param sfzh
     * @return
     */
    List<Map<String, Object>> getZdryAllYjNoPage( @Param("sfzh") String sfzh,@Param("phone") String phone,@Param("startTime") String startTime,@Param("endTime") String endTime);

    /**
     * 查询网吧和旅馆撒点定位
     * @param lfRyyjsjQO
     * @return
     */
    List<Map<String, Object>> queryWbLgKyNumber(@Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 查询迁徙状况数据
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getZdryTxzk(@Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 查询给ds的推送数据
     * @return
     */
    List<ZdryYjPushDataVo> queryYjDataPushToDs();

    /**
     * 编辑唯一标识
     * @param wybses
     */
    void updateYjSendFlag(List<String> wybses);

    /**
     * 统计重点人员预警数据
     * @return
     */
    List<ZdryYjStatisticsVo> zdryYjStatistics(@Param("qo") LfRyyjsjQO lfRyyjsjQOm);

    /**
     * 查询给ds推送的预警消息
     * @return
     */
    List<ZdryYjPushDataVo> queryTldpYjDataPushToDs();

    /**
     * 根据身份证号查询一个小时内离开区域推送的次数
     * @param idcard
     * @return
     */
    Integer queryPhyjBySfzh(@Param("idcard") String idcard);

    /**
     * 查询统计数据
     * @param page
     * @param lfRyyjsjQO
     * @return
     */
    IPage<ZdryTxVo> queryZdryYjStatisticsData(Page<?> page, @Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 导出重点人员预警统计信息
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryTxVo> exportZdryYjStatisticsData(@Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 查询符合预警处置规则的预警消息
     * @return
     */
    List<LfZtryyjEntity> queryAlarmFilterData(@Param("qo") KmAlarmFilterQo qo);

    /**
     * 根据预警唯一标识进行标记
     * @param wybsList
     */
    void updateAlarmFilterMark(List<String> wybsList);
}
