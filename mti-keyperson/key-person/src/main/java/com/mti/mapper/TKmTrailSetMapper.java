package com.mti.mapper;

import com.mti.dao.model.KmTrailEntity;
import com.mti.dao.model.TKmTrailSetEntity;
import com.mti.dao.model.TKmTrailSetRelEntity;
import com.mti.dao.qo.TKmTrailQO;
import com.mti.dao.qo.TKmTrailSetQO;
import com.mti.dao.vo.KmTrailVo;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
public interface TKmTrailSetMapper extends BaseMapper<TKmTrailSetEntity> {
	
	int insertRelPersonType(TKmTrailSetRelEntity relEntity);
	
	int deleteRelPersonType(@Param("id") String id);

	IPage<TKmTrailSetEntity> queryPage(Page<?> page, @Param("qo") TKmTrailSetQO qo);
	
	int updateTKmTrailSet(TKmTrailSetEntity entity);
	
	IPage<KmTrailEntity> getPageList(Page<?> page, @Param("qo") TKmTrailQO qo);
	
	List<KmTrailVo> getAllList(@Param("type") String type);
	
	int updateStatusById(@Param("id") String id);

}
