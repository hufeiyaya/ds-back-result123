package com.mti.mapper;


import com.mti.dao.model.TKmMhdzEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.utils.ZdryQxzkUtilBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TKmMhdzEntityMapper {
    int deleteByPrimaryKey(String xxzjbh);

    int insert(TKmMhdzEntity record);

    int insertSelective(TKmMhdzEntity record);

    TKmMhdzEntity selectByPrimaryKey(String xxzjbh);

    int updateByPrimaryKeySelective(TKmMhdzEntity record);

    int updateByPrimaryKey(TKmMhdzEntity record);

    List<TKmMhdzEntity> getMhdzListBySfzh(@Param("sfzf") String sfzf);

    /**
     * 获取民航出昆明的数据
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getMhOut(@Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 获取入昆明的数据
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getMhIn(@Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 获取民航订座导出数据
     * @param mhdzEntities
     * @return
     */
    List<TKmMhdzEntity> getMhdzList(List<String> mhdzEntities);
}