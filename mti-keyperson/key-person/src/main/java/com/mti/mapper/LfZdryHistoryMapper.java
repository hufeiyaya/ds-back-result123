package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.LfZdryHistoryEntity;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/15
 * @change
 * @describe describe
 **/
public interface LfZdryHistoryMapper extends BaseMapper<LfZdryHistoryEntity> {
    int insertHistoryByBatch(List<LfZdryHistoryEntity> list);

    List<HashMap<String,String>> listIdsHistoryByVersion(@Param(value = "version")String version, @Param(value = "ids")List<String> ids);
}
