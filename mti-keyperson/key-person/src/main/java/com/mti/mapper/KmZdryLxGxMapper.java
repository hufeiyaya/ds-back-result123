package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.kmmodel.KmZdryLxGxEntity;

/**
 * @Classname KmZdryLxGxMapper
 * @Description TODO
 * @Date 2020/1/10 22:52
 * @Created by duchaof
 * @Version 1.0
 */
public interface KmZdryLxGxMapper extends BaseMapper<KmZdryLxGxEntity> {
}
