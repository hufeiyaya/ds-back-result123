/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.model.LfRyyjsjEntity;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.LfZdryZrbmEntity;
import com.mti.dao.qo.LfZdryQO;
import com.mti.vo.PredictStatisticsDataItem;
import com.mti.vo.SpecialPersonStatisticsDataVO;
import com.mti.vo.ZdryVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface LfZdryMapper extends BaseMapper<LfZdryEntity> {

//    @Select("select " +
//            " t.value_ as rylbx, " +
//            " (select count(*) from t_lf_zdry z where z.rylbx = t.value_) as count, " +
//            " (select count(*) from t_lf_zdry z  where 1=1 and z.rylbx = t.value_  and z.sfzk = '在控') as zk, " +
//            " (select count(*) from t_lf_zdry z where 1=1 and z.rylbx = t.value_ and z.sfzk = '失控') as sk " +
//            " from t_sys_dict t where t.delete_ = '0' and t.type_ = 'kp_rylb' order by t.sort_ asc ")
    List<Map<String, String>> getCountByRylbx(@Param(value = "zrbms")List<String> zrbms,@Param(value = "rangeType") String rangeType,@Param(value = "rangeVal")List<String> rangeVal);

//    @Select("select " +
//            " t.value_ as sfzk, " +
//            " (select count(*) from t_lf_zdry z where z.sfzk = t.value_) as count, " +
//            " (select count(*) from t_lf_zdry z  where 1=1 and z.sfzk = t.value_  and z.sfzk = '在控') as zk, " +
//            " (select count(*) from t_lf_zdry z where 1=1 and z.sfzk = t.value_ and z.sfzk = '失控') as sk " +
//            " from t_sys_dict t where t.delete_ = '0' and t.type_ = 'kp_sfzk' order by t.sort_ asc ")
    List<Map<String, String>> getCountBySfzk(@Param(value = "zrbms")List<String> zrbms,@Param(value = "rangeType") String rangeType,@Param(value = "rangeVal")List<String> rangeVal);

//    @Select("select " +
//            " t.value_ as ssxq," +
//            " (select count(*) from t_lf_zdry z where z.ssxq = t.value_) as count," +
//            " (select count(*) from t_lf_zdry z  where 1=1 and z.ssxq = t.value_  and z.sfzk = '在控') as zk," +
//            " (select count(*) from t_lf_zdry z where 1=1 and z.ssxq = t.value_ and z.sfzk = '失控') as sk" +
//            " from t_sys_dict t where t.delete_ = '0' and t.type_ = 'kp_ssxq' order by count desc")
    List<Map<String, String>> getCountBySsxq(@Param(value = "zrbms")List<String> zrbms,@Param(value = "rangeType") String rangeType,@Param(value = "rangeVal")List<String> rangeVal);

//    @Select("select " +
//            " t.value_ as gkjbx, " +
//            " (select count(*) from t_lf_zdry z where z.gkjbx = t.value_) as count, " +
//            " (select count(*) from t_lf_zdry z  where 1=1 and z.gkjbx = t.value_  and z.sfzk = '在控') as zk, " +
//            " (select count(*) from t_lf_zdry z where 1=1 and z.gkjbx = t.value_ and z.sfzk = '失控') as sk " +
//            " from t_sys_dict t where t.delete_ = '0' and t.type_ = 'kp_gkjb' order by t.sort_ asc ")
    List<Map<String, String>> getCountByGkjbx(@Param(value = "zrbms")List<String> zrbms,@Param(value = "rangeType") String rangeType,@Param(value = "rangeVal")List<String> rangeVal);

//    @Select("select " +
//            " t.yjlx as yjlx, " +
//            " (select count(*) from t_lf_zdry z where  z.sfzh in (select t1.yjbs from t_lf_ryyjsj t1 where t1.yjlx = t.yjlx )) as count, " +
//            " (select count(*) from t_lf_zdry z where  z.sfzh in (select t1.yjbs from t_lf_ryyjsj t1 where t1.yjlx = t.yjlx )) as zk, " +
//            " 0 as sk " +
//            " from t_lf_ryyjsj t where 1=1 group by t.yjlx order by t.yjlx asc ")
    List<Map<String, String>> getCountByYjlx(@Param(value = "zrbms")List<String> zrbms,@Param(value = "rangeType") String rangeType,@Param(value = "rangeVal")List<String> rangeVal);

//    @Select("select " +
//            " t.value_ as ssxq," +
//            " (select count(*) from t_lf_zdry z where z.ssxq = t.value_) as count," +
//            " (select count(*) from t_lf_zdry z where z.gkjbx = '一级' and z.ssxq = t.value_ ) as gkjbx1, " +
//            " (select count(*) from t_lf_zdry z where z.gkjbx = '二级' and z.ssxq = t.value_ ) as gkjbx2, " +
//            " (select count(*) from t_lf_zdry z where z.gkjbx = '三级' and z.ssxq = t.value_ ) as gkjbx3 " +
//            " from t_sys_dict t where t.delete_ = '0' and t.type_ = 'kp_ssxq' order by count desc")
    List<Map<String, String>> getCountBySsxqGkjbx(@Param(value = "zrbms")List<String> zrbms,@Param(value = "rangeType") String rangeType,@Param(value = "rangeVal")List<String> rangeVal);

    IPage<LfZdryEntity> queryPage(Page<?> page, @Param("qo") LfZdryQO qo);

    IPage<LfZdryEntity> otherQueryPage(Page<?> page, @Param("qo") LfZdryQO qo);
    
    List<LfZdryEntity> queryExportData(@Param("qo") LfZdryQO qo);

    @Select("select id,sfzh,x,y,rylbx,gkjb,xm,hjd_x as hjdX,hjd_y as hjdY from t_lf_zdry where x is not null")
    List<ZdryVo> getSimpleAll();

    List<LfZdryEntity> listZdryByCreateTime(
            @Param(value = "page")Integer page,
            @Param(value = "size")Integer size,
            @Param(value = "date")String date,
            @Param(value = "rangeType")String rangeType,
            @Param(value = "rangeVal")List<String> rangeVal,
            @Param(value = "zrbms")List<String> zrbms,
            @Param(value = "isApproveFilter")String isApproveFilter,
            @Param(value = "start")String start,
            @Param(value = "end")String end
            );

   /* List<SpecialPersonStatisticsDataVO> getSpecialPersonStatistics(@Param(value = "rangeType")String rangeType, @Param(value = "rangeVal")List<String> rangeVal, @Param(value = "zrdws")List<String> zrdws);*/

    List<PredictStatisticsDataItem> getPredictSpecialPersonStatistics(@Param(value = "rangeType")String rangeType, @Param(value = "rangeVal")List<String> rangeVal, @Param(value = "start")String start, @Param(value = "end")String end);

    List<PredictStatisticsDataItem> getDropSpecialPersonStatistics(@Param(value = "rangeType")String rangeType, @Param(value = "rangeVal")List<String> rangeVal, @Param(value = "start")String start, @Param(value = "end")String end);

    List<LfZdryEntity> getZrbmByIds(@Param(value = "ids")List<String> ids);


    @Select("select rylbx from t_lf_zdry group by rylbx")
    List<String> getrylbx();

    @Select("select * from t_lf_zdry_zrbm where rylb = #{rylbx}")
    LfZdryZrbmEntity getByZrbm(String rylbx);

    LfRyyjsjEntity getByZdryBjxx(@Param("yjbs") String sfzh, @Param("yjsj") String yjsj);

    LfRyyjsjEntity getByZtryBjxx(@Param("yjbs") String sfzh, @Param("yjsj") String yjsj);

    @Select("select hphm from t_lf_zdry_car where sfzh=#{sfzh} limit 1")
    String getCiperBySfzh(@Param(value = "sfzh")String sfzh);

    List<Map<String,Integer>> getMjControlCountByPcs(@Param(value = "pcs")String pcs,@Param(value = "zrbm")List<String> zrbm,@Param(value = "rangeType")String rangeType,@Param(value = "rangeVal")List<String> rangeVal);

    List<Map<String,Integer>> getSpByHjs(@Param(value = "province") String province,@Param(value = "zrbm")List<String> zrbm,@Param(value = "rangeType")String rangeType,@Param(value = "rangeVal")List<String> rangeVal,@Param(value = "arenLevel")String areaLevel);


    /**
     * 根据关键字查询重点人员
     * @param page
     * @param paramMap
     * @return
     */
    IPage<LfZdryEntity> queryZdryByZrpcsPage(Page<?> page, @Param("q")Map<String, Object> paramMap);

    /**
     * 结合添加查询满足添加的重点人员的身份证
     * @param qo
     * @return
     */
    List<String> getSfzhsByCondition(@Param("qo") LfZdryQO qo);

    /**
     * 根据户籍地查询各省份的重点人员
     * @return
     */
    List<Map<String,Object>> getEveryProvinceZdryNum();

    /**
     * 根据省份的code获取各市的重点人员数量
     * @param code
     * @return
     */
    List<Map<String,Object>> getCityZdryNumByProvinceCode(@Param("code") String code);

    /**
     * 根据市的code获取各县/区的重点人员数量
     * @param code
     * @return
     */
    List<Map<String,Object>> getCountryZdryNumByCityCode(@Param("code")String code);

    /**
     * 根据添加查询重点人员信息，主要用户户籍地和现住地的撒点功能
     * @param qo
     * @return
     */
    List<Map<String,Object>> queryZdryList(@Param("qo")LfZdryQO qo);

    /**
     * 查询省级迁徙图
     * @param qo
     * @return
     */
    List<Map<String,Object>> queryProvinceTx(@Param("qo")LfZdryQO qo);

    /**
     * 查询市级迁徙图
     * @param qo
     * @return
     */
    List<Map<String,Object>> queryCityTx(@Param("qo")LfZdryQO qo);

    /**
     * 查询省级迁徙列表
     * @param page
     * @param qo
     * @return
     */
    IPage<LfZdryEntity> queryProvinceTxList(Page<?> page, @Param("qo")LfZdryQO qo);

    /**
     * 查询市级迁徙列表
     * @param page
     * @param qo
     * @return
     */
    IPage<LfZdryEntity> queryCityTxList(Page<?> page, @Param("qo")LfZdryQO qo);

    /**
     * 查询导出的execl数据
     * @param qo
     * @return
     */
    List<LfZdryEntity> queryExportProvinceTxList(@Param("qo")LfZdryQO qo);

    /**
     * 查询导出的execl数据
     * @param qo
     * @return
     */
    List<LfZdryEntity> queryExportCityTxList(@Param("qo")LfZdryQO qo);

    /**
     * 根据所属辖区和人员类别统计人数及小类名称
     * @return
     */
    List<Map<String,Object>> getRiskLevelCount(@Param("sspcs") String sspcs,@Param("ssxq")String ssxq,@Param("rylb")String rylb);

    /**
     * 根据身份证号查询重点人员
     */
    List<LfZdryEntity> listSFZH(@Param("sfzh") String sfzh);
}
