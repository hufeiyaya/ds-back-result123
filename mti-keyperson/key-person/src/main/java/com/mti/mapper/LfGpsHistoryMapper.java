package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.LfGpsHistoryEntity;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface LfGpsHistoryMapper extends BaseMapper<LfGpsHistoryEntity> {

    List<LfGpsHistoryEntity> getGpsHistory(@Param("tablename") String tablename, @Param("time") String time);

}
