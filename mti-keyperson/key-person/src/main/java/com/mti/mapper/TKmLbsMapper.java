package com.mti.mapper;

import com.mti.dao.model.TkmLbsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.qo.LfZdryQO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 微信/qq定位 Mapper 接口
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
public interface TKmLbsMapper extends BaseMapper<TkmLbsEntity> {
    /**
     * 根据身份证号查询重点人员的最新定位信息
     * @return
     */
    List<TkmLbsEntity> queryZdryLbsPosition( @Param("qo") LfZdryQO qo);

    /**
     * 查询LBS定位的详情
     * @param sfzhs
     * @return
     */
    List<Map<String, Object>> queryLbsLocationInfo(List<String> sfzhs);
}
