package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.SysDictEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface SysDictMapper extends BaseMapper<SysDictEntity> {
    List<String> getOrgByOid(@Param(value = "oids")List<String> oids);

    @Select("select * from t_sys_dict where type_=#{type} and value_ = #{key}")
    SysDictEntity getDictByTypeAndVal(@Param(value = "type")String type,@Param(value = "key")String key);

    @Select("select id_ from t_pf_organization where name_=#{oname} limit 1")
    String getOidByOname(@Param(value = "oname")String oname);

    @Select("select name_ as name,address_ as address from t_pf_person where number_=#{mjid}")
    Map<String,String> getPersonInfoByMjid(@Param(value = "mjid")String mjid);


    List<SysDictEntity> queryAllRyxles();
}
