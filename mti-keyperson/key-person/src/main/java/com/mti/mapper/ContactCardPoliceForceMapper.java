package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.ContactCardPoliceForceEntity;

/**
 * <p>
 *  卡点警力分布信息 Mapper接口
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-26
 */
public interface ContactCardPoliceForceMapper extends BaseMapper<ContactCardPoliceForceEntity> {

}
