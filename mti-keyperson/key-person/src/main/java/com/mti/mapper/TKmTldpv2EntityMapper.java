package com.mti.mapper;


import com.mti.dao.model.TKmTldpv2Entity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.utils.ZdryQxzkUtilBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TKmTldpv2EntityMapper {
    int insert(TKmTldpv2Entity record);

    int insertSelective(TKmTldpv2Entity record);

    /**
     * 判断铁路  进-出
     * @param tKmTldpv2Entity
     * @return
     */
    Integer judgeInOutType(TKmTldpv2Entity tKmTldpv2Entity);

    /**
     * 通过主键查询 铁路订票信息
     * @param glId
     * @return
     */
    TKmTldpv2Entity selectBySystemid(@Param("glId") String glId);

    /**
     * 根据身份证号查询铁路预警信息
     * @param sfzf
     * @return
     */
    List<TKmTldpv2Entity> getTldpListBySfzh(@Param("sfzf") String sfzf);

    /**
     * 查询铁路出昆明的数据
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getTlOut(@Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 查询铁路进昆明的数据
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getTlIn(@Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 根据systemid查询铁路订票的预警信息
     * @param tldpEntities
     * @return
     */
    List<TKmTldpv2Entity> getTldpEntityList(List<String> tldpEntities);
}