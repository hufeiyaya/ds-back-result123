package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.TPfGroupEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 重点人员分组表 Mapper 接口
 * </p>
 *
 * @author hf
 * @since 2020-03-25
 */

public interface TPfGroupMapper extends BaseMapper<TPfGroupEntity> {

    List<TPfGroupEntity> getGroupInfoByAccId(@Param("accId") String accId);
}
