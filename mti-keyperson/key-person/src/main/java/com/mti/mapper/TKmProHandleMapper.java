package com.mti.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.model.TKmProHandle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.qo.TkmProHanQo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 信访/聚访维权处理或化解信息 Mapper 接口
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
public interface TKmProHandleMapper extends BaseMapper<TKmProHandle> {
    /**
     * 根据条件查询记录，不分页
     * @param tKmProHandle
     * @return
     */
    List<TKmProHandle> listTkmProHandle(TKmProHandle tKmProHandle);

    /**
     * 根据添加查询记录，分页
     * @param page
     * @param tkmProHanQo
     * @return
     */
    IPage<TKmProHandle> pageTkmProHandle(Page<?> page, @Param("q") TkmProHanQo tkmProHanQo);
}
