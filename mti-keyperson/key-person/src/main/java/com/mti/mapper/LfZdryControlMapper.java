package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.LfZdryControlEntity;

/**
 * <p>
 *  布控信息 Mapper接口
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-15
 */
public interface LfZdryControlMapper extends BaseMapper<LfZdryControlEntity> {

}
