package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.LfZdryBaseEntity;
import org.apache.ibatis.annotations.Param;


/***
 * @Description: 关注/取消关注重点人员mapper接口
 * @Author: caoxx
 * @Date: 2020/9/2 9:40
   @Param:
   @Return:
 **/
public interface KmZdryFollowMapper extends BaseMapper<LfZdryBaseEntity>{

    void follow(@Param(value = "personId") String personId,@Param(value = "masterId") String masterId,@Param(value = "creator")String creator);

    void unFollow(@Param(value = "personId") String personId,@Param(value = "masterId") String masterId);

    int isFollow(@Param(value = "personId") String personId,@Param(value = "masterId") String masterId);

}
