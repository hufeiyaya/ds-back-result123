package com.mti.mapper;

import com.mti.dao.model.LfZdryExportDataEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author duchao
 * @since 2020-04-05
 */
public interface LfZdryExportDataMapper extends BaseMapper<LfZdryExportDataEntity> {
    /**
     * 根据版本号删除数据
     * @param version
     */
    void deleteByVersion(@Param("version") String version);

    /**
     * 批量添加重点人员
     * @param zdryList
     */
    void batchSaveZdry(List<LfZdryExportDataEntity> zdryList);

    /**
     * 跟新操作标志
     */
    void updateExportDataSign(@Param("sfzh") String sfzh);
}
