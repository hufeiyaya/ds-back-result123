package com.mti.mapper;

import com.mti.dao.model.KmJurisdictionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author duchao
 * @since 2020-05-03
 */
public interface KmJurisdictionMapper extends BaseMapper<KmJurisdictionEntity> {
    /**
     * 根据预警的经纬度查询预警地派出所信息
     * @param loc
     * @param lat
     * @return
     */
    KmJurisdictionEntity queryYjdPcsByLngAndLat(@Param("loc") String loc, @Param("lat") String lat);
}
