package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.LfZdryControlPcEntity;

/**
 * <p>
 *  重点人员布控pc信息 Mapper接口
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-26
 */
public interface LfZdryControlPcMapper extends BaseMapper<LfZdryControlPcEntity> {

}
