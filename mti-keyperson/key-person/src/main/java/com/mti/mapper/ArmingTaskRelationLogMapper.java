package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.LfZdryArmingTaskOperateLog;

public interface ArmingTaskRelationLogMapper extends BaseMapper<LfZdryArmingTaskOperateLog> {
}
