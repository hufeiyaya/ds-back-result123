package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.RylbRelationEntity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface RylbRelationMapper extends BaseMapper<RylbRelationEntity> {

}
