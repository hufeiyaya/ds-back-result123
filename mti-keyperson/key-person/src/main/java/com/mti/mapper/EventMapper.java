package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.EventEntity;

public interface EventMapper extends BaseMapper<EventEntity> {
}
