package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.LfGkjlEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface LfGkjlMapper extends BaseMapper<LfGkjlEntity> {

    List<LfGkjlEntity> queryListBySfzh(@Param("sfzh") String sfzh);

    /**
     * 通过身份证号获取是否在控
     * @param ids 身份证号集合
     * @return 是否在控列表
     */
    List<Map<String,String>> getIsControlByIdentityIds(@Param(value = "ids") List<String> ids);

}
