package com.mti.mapper;

import com.mti.dao.model.KmZdryAssistEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 重点人员-辅助信息表 Mapper 接口
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
public interface KmZdryAssistMapper extends BaseMapper<KmZdryAssistEntity> {

}
