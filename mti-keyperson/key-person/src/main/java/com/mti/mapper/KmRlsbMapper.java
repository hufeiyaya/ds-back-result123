package com.mti.mapper;

import com.mti.dao.model.KmRlsbEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 人脸识别 Mapper 接口
 * </p>
 *
 * @author duchao
 * @since 2020-06-19
 */
public interface KmRlsbMapper extends BaseMapper<KmRlsbEntity> {

    Integer findRlsbInfo(KmRlsbEntity kmRlsbEntity);
}
