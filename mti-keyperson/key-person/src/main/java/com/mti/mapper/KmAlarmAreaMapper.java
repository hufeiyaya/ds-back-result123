package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.KmAlarmAreaEntity;
import com.mti.dao.qo.KmAlarmAreaQO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 手机预警和人脸识别预警设置过滤 Mapper 接口
 * </p>
 *
 * @author hf
 * @since 2020-11-20
 */

public interface KmAlarmAreaMapper extends BaseMapper<KmAlarmAreaEntity> {
    boolean authIsInArea(@Param("qo") KmAlarmAreaQO qo);
}
