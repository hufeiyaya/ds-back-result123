package com.mti.mapper;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.model.LfZdryBaseTempEntity;
import com.mti.dao.qo.LfZdryQO;

/**
 * <p>
 * 重点人员表 Mapper 接口
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */

public interface LfZdryBaseTempMapper extends BaseMapper<LfZdryBaseTempEntity> {
	IPage<LfZdryBaseTempEntity> queryPage(Page<?> page, @Param("qo") LfZdryQO qo);
}
