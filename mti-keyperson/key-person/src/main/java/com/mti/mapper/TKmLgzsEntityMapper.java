package com.mti.mapper;


import com.mti.dao.model.TKmLgzsEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TKmLgzsEntityMapper {
    int deleteByPrimaryKey(String xxzjbh);

    int insert(TKmLgzsEntity record);

    int insertSelective(TKmLgzsEntity record);

    TKmLgzsEntity selectByPrimaryKey(String xxzjbh);

    int updateByPrimaryKeySelective(TKmLgzsEntity record);

    int updateByPrimaryKey(TKmLgzsEntity record);

    List<TKmLgzsEntity> getLgzsListBySfzh(@Param("sfzf") String sfzf);

    /**
     * 查询旅馆撒点信息
     * @param lfRyyjsjQO
     * @return
     */
    List<Map<String, Object>> queryWbLgKyNumber(@Param("qo") LfRyyjsjQO lfRyyjsjQO);

    /**
     * 获取旅馆住宿的数据集合
     * @param lgzsEntities
     * @return
     */
    List<TKmLgzsEntity> getLgzsList(List<String> lgzsEntities);

    /**
     * 查询旅馆 入住 和 退房时间
     * @param wybs
     * @return
     */
    Map<String, Object> findLgzsStartAndEndTime(@Param("wybs") String wybs);
}