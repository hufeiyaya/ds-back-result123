package com.mti.mapper;


import com.mti.dao.model.TKmMhlgEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TKmMhlgEntityMapper {
    int deleteByPrimaryKey(String xxzjbh);

    int insert(TKmMhlgEntity record);

    int insertSelective(TKmMhlgEntity record);

    TKmMhlgEntity selectByPrimaryKey(String xxzjbh);

    int updateByPrimaryKeySelective(TKmMhlgEntity record);

    int updateByPrimaryKey(TKmMhlgEntity record);

    List<TKmMhlgEntity> getMhlgListBySfzh(@Param("sfzf") String sfzf);

    List<TKmMhlgEntity> getMhlgList(List<String> mhlgEntities);
}