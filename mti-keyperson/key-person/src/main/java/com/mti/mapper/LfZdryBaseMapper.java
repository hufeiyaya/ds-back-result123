package com.mti.mapper;

import java.util.List;
import java.util.Map;

import com.mti.dao.dto.ZdryDto;
import com.mti.vo.SpecialPersonStatisticsDataVO;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.dto.LfZdryDto;
import com.mti.dao.model.LfZdryBaseEntity;
import com.mti.dao.qo.LfZdryQO;

/**
 * <p>
 * 重点人员表 Mapper 接口
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */

public interface LfZdryBaseMapper extends BaseMapper<LfZdryBaseEntity> {
    /**
     * 导出重点人员时 查询数据
     * @param qo
     * @return
     */
    List<LfZdryBaseEntity> findExportExeclData(@Param("qo") LfZdryQO qo);

    /**
     * 根据身份证号导出重点人员时 查询数据
     * @param qo
     * @return
     */
    List<LfZdryBaseEntity> findExportExeclDataNumber(@Param("listNumber") List<String> listNumber);

    /**
     * 分页查询重点人员
     * @param page
     * @param qo
     * @return
     */
	IPage<LfZdryBaseEntity> queryPage(Page<?> page, @Param("qo") LfZdryQO qo);

	/**
     * 根据添加查询重点人员
     * @param lfZdryDto
     * @return
     */
    List<ZdryDto> queryZdryByCondition(LfZdryDto lfZdryDto);

    /**
     * 根据派出所名称查询重点人员
     * @param page
     * @param paramMap
     * @return
     */
    IPage<ZdryDto> queryZdryByZrpcsPage(Page<?> page, @Param("q") Map<String, Object> paramMap);

    /**
     * 查询根据派出所导出重点人员的数据
     * @param sspcsId
     * @return
     */
    List<ZdryDto> queryExeclExportDataByZrpcs(@Param("ssxqId") String ssxqId,@Param("sspcsId") String sspcsId);

    /**
     * 查询全国各省重点人员数量
     * @return
     */
    List<Map<String, Object>> getEveryProvinceZdryNum();

    /**
     * 根据省份id获取城市重点人员数量
     * @param code
     * @return
     */
    List<Map<String, Object>> getCityZdryNumByProvinceCode(String code);

    /**
     * 根据市的code查询区县重点人员数量
     * @return
     */
    List<Map<String, Object>> getCountryZdryNumByCityCode();

    /**
     * 查询根据 户籍地 和 居住地 撒点的导出数据
     * @param sfzhs
     * @return
     */
    List<ZdryDto> queryZdryByHjdAndXzzData(List<String> sfzhs);

    /**
     * 查询从云南省外 迁入的数量
     * @param qo
     * @return
     */
    List<Map<String,Object>> queryProvinceTx(@Param("qo") LfZdryQO qo);

    /**
     * 查询省 市迁徙图列表
     * @param page
     * @param qo
     * @return
     */
    IPage<ZdryDto> queryProvinceAndCityTxList(Page<?> page, @Param("qo") LfZdryQO qo);

    /**
     * 导出省 市迁徙图数据
     * @param qo
     * @return
     */
    List<ZdryDto> queryProvinceAndCityTxExportData(@Param("qo") LfZdryQO qo);

    /**
     * 查询 户籍地 和 居住地 经纬度
     * @param qo
     * @return
     */
    List<Map<String, Object>> queryZdryList(@Param("qo") LfZdryQO qo);

    /**
     * 根据 户籍地 和 居住地 查询重点人员信息
     * @param page
     * @param sfzhs
     * @return
     */
    IPage<ZdryDto> queryZdry(Page<?> page, @Param("sfzhs") List<String> sfzhs);

    /**
     * 查询重点人员推送数据
     * @param page
     * @return
     */
    IPage<ZdryDto> queryPushZdryInfo(Page<ZdryDto> page);

    /**
     * 查询市级迁徙图
     * @param qo
     * @return
     */
    List<Map<String,Object>> queryCityTx(@Param("qo") LfZdryQO qo);

    /**
     * 查询情报中心 和 分局的稳控状态数量
     * @param code
     * @param name
     * @return
     */
    List<Map<String, String>> getCountWkztForQbzxAndSsxqAndSspcs(@Param("code") String code, @Param("name") String name);

    /**
     * 查询警种用户的稳控状态的数量
     * @param code
     * @param name
     * @return
     */
    List<Map<String, String>> getCountWkztForZrbm(@Param("code") String code, @Param("name") String name);

    /**
     * 统计区域数据 情报中心  分局 派出所
     * @param code
     * @param name
     * @return
     */
    List<Map<String, String>> getCountRegionForQbzxAndSsxqAndSspcs(@Param("code") String code, @Param("name") String name);

    /**
     * 统计区域数据 警种
     * @param code
     * @param name
     * @return
     */
    List<Map<String, String>> getCountRegionForZrbm(@Param("code") String code, @Param("name") String name);

    /**
     * 概览 管控级别 查询 情报中心 分局 警种
     * @param code
     * @param name
     * @return
     */
    List<Map<String, String>> getCountGkjbForQbzxAndSsxqAndSspcs(@Param("code") String code, @Param("name") String name);

    /**
     * 概览 管控级别 责任部门
     * @param code
     * @param name
     * @return
     */
    List<Map<String, String>> getCountGkjbForZrbm(@Param("code") String code, @Param("name") String name);

    /**
     * 概览 查询管控级别  情报中心 所属分局 派出所
     * @param code
     * @param name
     * @return
     */
    List<Map<String, String>> getCountRylb(@Param("code") String code, @Param("name") String name);

    /**
     * 重点人员统计接口
     * @param rangeType
     * @param list
     * @param zrdwList
     * @return
     */
//    List<SpecialPersonStatisticsDataVO> getSpecialPersonStatistics(String rangeType, List<String> list, List<String> zrdwList);
    List<SpecialPersonStatisticsDataVO> getSpecialPersonStatistics(@Param("qo") LfZdryQO qo );

    /**
     * 查询地址转坐标的对象
     * @param total
     * @return
     */
    List<LfZdryBaseEntity> findZdry(Integer total);

    /**
     * 通过身份证号 和 手机号查询重点人员信息
     * @param idCard
     * @param phone
     * @return
     */
    Map<String,String> findZdryBySfzhAndSjh(@Param("idCard") String idCard, @Param("phone") String phone);

    /**
     * 根据身份证号跟新重点人员
     * @param zdry
     * @return
     */
    Integer updateZdryBySfzh(LfZdryBaseEntity zdry);

    /**
     * 根据重点人员id获取重点人员信息
     *
     * @param personId 重点人员id
     * @return
     */
    Map<String, Object> getkeyPersonByPersonId(String personId);

    /**
     * 数据清理 查询重点人员列表
     * @param page
     * @param keyWords
     * @return
     */
    IPage<LfZdryBaseEntity> queryZdryListClear(Page<?> page, @Param("keyWords") String keyWords);

    /**
     * 根据当前登录账号查询下级单位
     * @param id
     * @param suffix
     * @return
     */
    List<Map<String, Object>> queryLowerlevelUnit(@Param("id") String id, @Param("suffix") String suffix);

    /**
     * 查询每日跟踪中稳控状态分组数量
     * @param qo
     * @return
     */
    List<Map<String, Object>> queryWkztCount(@Param("qo") LfZdryQO qo);

    /**
     * 查询警种统计数据
     * @param zrdw
     * @return
     */
     List<SpecialPersonStatisticsDataVO> getSpecialPersonStatisticsForPoliceCategory(@Param("qo") LfZdryQO qo);

    /**
     * 根据重点人员身份证号查询重点人员列表
     * @param sfzhs
     * @return
     */
    List<LfZdryBaseEntity> findZdryGatherList(List<String> sfzhs);
}
