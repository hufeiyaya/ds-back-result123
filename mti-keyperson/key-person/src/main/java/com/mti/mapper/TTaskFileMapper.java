package com.mti.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.TTaskFileEntity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author duchaof
 * @since 2020-03-11
 */
public interface TTaskFileMapper extends BaseMapper<TTaskFileEntity> {
	
	List<TTaskFileEntity> queryFileListById(String id);

}
