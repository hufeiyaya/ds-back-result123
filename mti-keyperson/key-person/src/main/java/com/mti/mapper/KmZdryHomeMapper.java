package com.mti.mapper;

import com.mti.dao.model.KmZdryHomeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 重点人员表 Mapper 接口
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
public interface KmZdryHomeMapper extends BaseMapper<KmZdryHomeEntity> {

}
