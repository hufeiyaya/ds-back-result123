/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.mapper;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.LfZdryTempEntity;
import com.mti.dao.qo.LfZdryQO;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface LfZdryTempMapper extends BaseMapper<LfZdryTempEntity> {
	
	IPage<LfZdryTempEntity> queryPage(Page<?> page, @Param("qo") LfZdryQO qo);

    IPage<LfZdryTempEntity> otherQueryPage(Page<?> page, @Param("qo") LfZdryQO qo);

}
