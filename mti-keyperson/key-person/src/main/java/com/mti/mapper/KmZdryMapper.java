package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.kmmodel.KmZdryDto;

/**
 * @Classname KmZdryMapper
 * @Description TODO
 * @Date 2020/1/10 22:27
 * @Created by duchaof
 * @Version 1.0
 */
public interface KmZdryMapper extends BaseMapper<KmZdryDto> {

}
