package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.LfZdryApproveLogEntity;

public interface LfZdryApproveLogMapper extends BaseMapper<LfZdryApproveLogEntity> {
}
