package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.LfZtryEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface LfZtryMapper extends BaseMapper<LfZtryEntity> {
    @Select("select zt.*,d.value_ as ztrylx from t_lf_ztry zt left join t_sys_dict d on zt.ztrylxdm = d.key_ where zt.sfzh=#{sfzh} and d.type_='event_ztrylx' limit 1")
    LfZtryEntity getOneSfzh(@Param(value = "sfzh")String sfzh);
}
