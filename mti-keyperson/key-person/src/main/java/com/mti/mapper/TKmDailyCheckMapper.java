package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.model.TKmDailyCheckEntity;
import com.mti.dao.qo.TkmDCheckQo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Classname TKmDailyCheckMapper
 * @Description TODO
 * @Date 2020/2/27 16:57
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmDailyCheckMapper extends BaseMapper<TKmDailyCheckEntity> {
    /**
     * 分页查询核查记录
     * @param page
     * @param tkmDCheckQo
     * @return
     */
    IPage<TKmDailyCheckEntity> queryTkmDailyCheckPage(Page<?> page, @Param("q") TkmDCheckQo tkmDCheckQo);

    /**
     * 查询每日核查的导出数据
     * @param tkmDCheckQo
     * @return
     */
    List<TKmDailyCheckEntity> queryexportDailyCheckData(@Param("q")TkmDCheckQo tkmDCheckQo);
}
