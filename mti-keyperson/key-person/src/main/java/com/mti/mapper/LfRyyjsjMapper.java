package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.model.LfRyyjsjEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface LfRyyjsjMapper extends BaseMapper<LfRyyjsjEntity> {

    @Select("select count(*), t.gkjbx, " +
            "(select count(*) from t_lf_zdry z  where 1=1 and z.gkjbx = t.gkjbx  and z.sfzk = '在控') as zk, " +
            "(select count(*) from t_lf_zdry s where 1=1 and s.gkjbx = t.gkjbx and s.sfzk = '失控' ) as sk " +
            " from t_lf_zdry t where 1=1 group by t.gkjbx")
    List<Map<String, String>> getCountByYjlx();

    List<LfRyyjsjEntity> queryListByYjbs(@Param("yjbs") String yjbs, @Param("orderBy") String orderBy);

    IPage<LfRyyjsjEntity> queryPage(Page<?> page, @Param("qo") LfRyyjsjQO qo);

    int updateByEntity(LfRyyjsjEntity entity);
}
