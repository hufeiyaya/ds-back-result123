package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.PhoneLog;

/**
 * Created by admin on 2020/8/24.
 */
public interface PhoneLogMapper  extends BaseMapper<PhoneLog> {
}
