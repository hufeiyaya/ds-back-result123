package com.mti.mapper;

import com.mti.dao.model.KmZdryZrbmEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 警种大类小类对应表 Mapper 接口
 * </p>
 *
 * @author chenf
 * @since 2020-03-26
 */
public interface KmZdryZrbmMapper extends BaseMapper<KmZdryZrbmEntity> {

}
