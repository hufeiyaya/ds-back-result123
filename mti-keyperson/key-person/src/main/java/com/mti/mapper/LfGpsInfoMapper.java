package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.model.LfGpsInfoEntity;
import com.mti.dao.qo.LfGpsInfoQO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface LfGpsInfoMapper extends BaseMapper<LfGpsInfoEntity> {

    LfGpsInfoEntity getByGpsid(@Param("gpsid") String gpsid);

    IPage<LfGpsInfoEntity> queryPage(Page<?> page, @Param("qo") LfGpsInfoQO qo);

}
