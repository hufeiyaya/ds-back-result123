package com.mti.mapper;

import com.mti.dao.model.KmZdryTypeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
public interface KmZdryTypeMapper extends BaseMapper<KmZdryTypeEntity> {

    /**
     * 通过身份号查询重点人员大类小类信息
     * @param yjbs
     * @return
     */
    List<KmZdryTypeEntity> getZdryTypeBySfzh(@Param("yjbs") String yjbs);
}
