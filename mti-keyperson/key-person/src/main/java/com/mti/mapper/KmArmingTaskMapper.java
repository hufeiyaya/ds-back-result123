package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.kmmodel.KmArmingTaskEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/***
 * @Description: 创建布控Mapper接口
 * @Author: caoxx
 * @Date: 2020/9/1 16:09
   @Param:
   @Return:
 **/
public interface KmArmingTaskMapper extends BaseMapper<KmArmingTaskEntity> {
    void saveRecordData(@Param(value = "taskId")String taskId,@Param(value = "status")Integer status);

    List<Map<String,Object>> getTimeFlows(@Param(value = "taskId")String taskId);
    List<String> selectIdByStartDateAndArmingStatus();
    List<String> selectIdByEndDateAndArmingStatus();
    void updateArmingStatusById(@Param(value = "taskId")String taskId,@Param(value = "status")Integer status);
    void updateArmingStatusBatchByIds(@Param(value = "list")List<String> list,@Param(value = "status")Integer status);
    void saveRecordDataBatch(@Param(value = "list")List<String> list,@Param(value = "status")Integer status);
    void updateDefault();
}
