package com.mti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.model.LfZdryArmingTaskEntity;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/21
 * @change
 * @describe 重点人员布防任务
 **/
public interface SpecialPersonArmingTaskMapper extends BaseMapper<LfZdryArmingTaskEntity> {
}
