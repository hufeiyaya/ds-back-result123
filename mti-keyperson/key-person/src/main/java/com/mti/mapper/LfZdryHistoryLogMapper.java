package com.mti.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.dao.model.LfZdryHistoryLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mti.dao.qo.HistoryLogQO;
import com.mti.dao.vo.ZdryHistoryLogVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenf
 * @since 2020-03-24
 */
public interface LfZdryHistoryLogMapper extends BaseMapper<LfZdryHistoryLogEntity> {

    /**
     * 分页
     * @param page
     * @param logqo
     * @return
     */
    IPage<ZdryHistoryLogVo> getZdryHistiryLog(Page<?> page, @Param("qo") HistoryLogQO logqo);

    /**
     * 查询重点人员日志导出记录
     * @param logqo
     * @return
     */
    List<ZdryHistoryLogVo> getExportZdryLogData(@Param("qo") HistoryLogQO logqo);
}
