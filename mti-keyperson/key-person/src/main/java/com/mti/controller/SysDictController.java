package com.mti.controller;


import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.SysDictEntity;
import com.mti.dao.model.SysTestEntity;
import com.mti.dao.qo.TypeQO;
import com.mti.service.ISysDictService;
import com.mti.service.ISysTestService;
import com.mti.utils.DateUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Api(value="数据字典接口",description="数据字典接口")
@RestController
@RequestMapping("/sys/dict")
@Slf4j
public class SysDictController {

    @Autowired
    private ISysDictService service;

    @ApiOperation(value="通过字典类型查询记录",notes=" * @param type\n" +
            "     *     重点人员列表\tkp_rylb\n" +
            "     *     重点人员负责部门\tkp_zrbm\n" +
            "     *     重点时期\tkp_zdsq\n" +
            "     *     重点人员管控状态\tkp_sfzk\n" +
            "     *     重点人员管控手段\tkp_gksd\n" +
            "     *     重点人员级别\tkp_gkjb\n" +
            "     *     重点人员报警方式\tkp_bjfs\n" +
            "     *     管控类型 访谈\tgklx_ft\n" +
            "     *     管控类型 协查\tgklx_xc\n" +
            "     *     所属辖区\tkp_ssxq")
    @RequestMapping(value = "/{type}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getByType(@PathVariable("type") String type) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<SysDictEntity> entitys = service.getByType(type);
                criteria.addSingleResult(entitys);
            }
        }.sendRequest());
    }
    
    
    @ApiOperation(value = "通过警种查询大类小类",notes = "通过警种查询大类小类")
    @RequestMapping(value = "/getType", method = RequestMethod.POST)
    public Mono<ResponseInfo> getType(ServerWebExchange exchange, @RequestBody TypeQO typeQo) {
    	   return Mono.just(new ResponseCallBack() {
               @Override
               public void execute(ResponseCriteria criteria, Object... obj) {
                   List<SysDictEntity> entitys = service.getType(exchange,typeQo);
                   criteria.addSingleResult(entitys);
               }
           }.sendRequest());
       }
    
    @ApiOperation(value = "通过关键字查询检索字段和类型",notes = "通过关键字查询检索字段和类型")
    @RequestMapping(value = "/queryKeyword", method = RequestMethod.GET)
    public Mono<ResponseInfo> queryKeyword(ServerWebExchange exchange, @RequestParam String keyword) {
    	   return Mono.just(new ResponseCallBack() {
               @Override
               public void execute(ResponseCriteria criteria, Object... obj) {
                   List<Object> entitys = service.queryKeyword(exchange,keyword);
                   criteria.addSingleResult(entitys);
               }
           }.sendRequest());
       }
    @ApiOperation(value = "通过关键字查询预警检索字段和类型",notes = "通过关键字查询预警检索字段和类型")
    @RequestMapping(value = "/queryWarnKeyword", method = RequestMethod.GET)
    public Mono<ResponseInfo> queryWarnKeyword(ServerWebExchange exchange, @RequestParam String keyword) {
    	   return Mono.just(new ResponseCallBack() {
               @Override
               public void execute(ResponseCriteria criteria, Object... obj) {
                   List<Object> entitys = service.queryWarnKeyword(exchange,keyword);
                   criteria.addSingleResult(entitys);
               }
           }.sendRequest());
       }
     
}
