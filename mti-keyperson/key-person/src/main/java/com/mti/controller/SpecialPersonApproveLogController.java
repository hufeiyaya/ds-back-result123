package com.mti.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.LfZdryApproveLogEntity;
import com.mti.service.ILfZdryApproveLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@Api
@AllArgsConstructor
@RestController
@RequestMapping(value = "/specialPerson/approveLog")
public class SpecialPersonApproveLogController {
    private final ILfZdryApproveLogService logService;
    @ApiOperation(value = "获取重点人员审批日志")
    @GetMapping
    public Mono<ResponseInfo> getApproveLogByZdId(@RequestParam(value = "zdId")@ApiParam(value = "重点人员ID",name = "zdId",required = true) String zdId){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(logService.list(new QueryWrapper<LfZdryApproveLogEntity>().lambda().eq(LfZdryApproveLogEntity::getSpId,zdId).orderByDesc(LfZdryApproveLogEntity::getCreateTime)));
            }
        }.sendRequest());
    }
}
