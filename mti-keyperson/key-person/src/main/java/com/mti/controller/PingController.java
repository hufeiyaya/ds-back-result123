package com.mti.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.service.IZtryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 第三方 前端控制器
 * </p>
 *
 * @author 潘文俊
 * @since 2018-09-19
 */
@Api(value = "健康检查接口", description = "健康检查接口")
@RestController
@CrossOrigin
@RequestMapping("/ping")
public class PingController  {
	
	@ApiOperation(value = "健康检查", notes = "健康检查")
	@GetMapping(value = "/healthCheck")
	public ResponseInfo healthCheck() {
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult("ok");
            }
        }.sendRequest();
    }
}

