package com.mti.controller;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.LfZtryyjEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.dao.qo.YjInfoParamQo;
import com.mti.dao.vo.ZdryTxVo;
import com.mti.service.ILfZtryyjsjService;
import com.mti.utils.ZdryQxzkUtilBean;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 重点人员预警业务 前端控制器
 * </p>
 */
@Slf4j
@RestController
@AllArgsConstructor
@Api(description = "重点人员预警",value = "重点人员预警")
@RequestMapping(value = "/ztryYjsj")
public class LfZtryYjsjController {
    private final ILfZtryyjsjService ztryyjsjService;
    @GetMapping(value = "/path")
    @ApiOperation(value = "获取在逃人员轨迹详情")
    public ResponseInfo getZtryDetail(@RequestParam(value = "sfzh")@ApiParam(value = "身份证号",name = "sfzh",required = true) String sfzh,
                                      @RequestParam(value = "page")@ApiParam(value = "当前页",name = "page",required = true) Integer page,
                                      @RequestParam(value = "size")@ApiParam(value = "页大小",name = "size",required = true) Integer size){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZtryyjEntity> ztryyjEntityIPage = ztryyjsjService.page(new Page<>(page,size),new QueryWrapper<LfZtryyjEntity>().lambda().eq(LfZtryyjEntity::getYjbs,sfzh));
                criteria.addMapResult("record",ztryyjEntityIPage.getRecords());
                criteria.addMapResult("total",ztryyjEntityIPage.getTotal());
            }
        }.sendRequest();
    }


    @PostMapping(value = "/getZdryYjInfo")
    @ApiOperation(value = "获取预警信息")
    public ResponseInfo getZdryYjInfo(@RequestBody LfRyyjsjQO lfRyyjsjQO, ServerWebExchange exchange){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZtryyjEntity> ztryyjEntityIPage= ztryyjsjService.getZdryYjInfo(lfRyyjsjQO,exchange);
                criteria.addMapResult("record",ztryyjEntityIPage.getRecords());
                criteria.addMapResult("total",ztryyjEntityIPage.getTotal());
            }
        }.sendRequest();
    }


    @PostMapping(value = "/exportZdryYjInfo")
    @ApiOperation(value = "根据条件导出预警信息")
    @ApiImplicitParam(value = "export名称",name = "exportName",dataType = "String",paramType = "query")
    public Mono<Void> exportYjXx(@RequestBody LfRyyjsjQO lfRyyjsjQO,
                                 @RequestParam(required = false) String exportName,
                                 ServerHttpResponse response,ServerWebExchange exchange) throws Exception {
        String fileName = StringUtils.isEmpty(exportName) ? "重点人员预警信息(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : exportName;
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}",fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = ztryyjsjService.exportYjXx(lfRyyjsjQO, exportName,exchange);
        return response.writeWith(flux);
    }

    @PostMapping("/getYjTimeOutInfo")
    @ApiOperation("查询预警超时信息")
    public ResponseInfo getYjTimeOutInfo(@RequestBody LfRyyjsjQO lfRyyjsjQO,ServerWebExchange exchange){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZtryyjEntity> ztryyjEntityIPage= ztryyjsjService.getYjTimeOutInfo(lfRyyjsjQO,exchange);
                criteria.addMapResult("record",ztryyjEntityIPage.getRecords());
                criteria.addMapResult("total",ztryyjEntityIPage.getTotal());
            }
        }.sendRequest();
    }


    @GetMapping(value = "/getZdryAllYj")
    @ApiOperation(value = "根据身份证号获取所有的预警信息")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "身份证号",name = "sfzh",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "分页参数",name = "offset",dataType = "int",paramType = "query"),
            @ApiImplicitParam(value = "分页参数",name = "pageSize",dataType = "int",paramType = "query")
    })
    public ResponseInfo getZdryAllYj(@RequestParam String sfzh,
                                     @RequestParam Integer offset,
                                     @RequestParam Integer pageSize){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<Map<String,Object>> page = ztryyjsjService.getZdryAllYj(sfzh,offset,pageSize);
                criteria.addMapResult("record",page.getRecords());
                criteria.addMapResult("total",page.getTotal());
            }
        }.sendRequest();
    }

    @GetMapping(value = "/getZdryAllYjNoPage")
    @ApiOperation(value = "根据身份证号获取所有的预警信息（不分页）")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "身份证号",name = "sfzh",dataType = "String",paramType = "query")
    })
    public ResponseInfo getZdryAllYjNoPage(@RequestParam String sfzh,@RequestParam String startTime,@RequestParam String endTime){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<Map<String,Object>> page = ztryyjsjService.getZdryAllYjNoPage(sfzh,startTime,endTime);
                criteria.addMapResult("record",page);
                criteria.addMapResult("total",page.size());
            }
        }.sendRequest();
    }

    @PostMapping(value = "/getZdryTxzk")
    @ApiOperation(value = "查询重点人员迁徙状况")
    public ResponseInfo getZdryTxzk(@RequestBody LfRyyjsjQO lfRyyjsjQO,ServerWebExchange exchange){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(ztryyjsjService.getZdryTxzk(lfRyyjsjQO,exchange));
            }
        }.sendRequest();
    }

    @PostMapping(value = "/queryWbLgKyNumber")
    @ApiOperation(value = "查询网吧旅馆客运站的撒点")
    public ResponseInfo queryWbLgKyNumber(@RequestBody LfRyyjsjQO lfRyyjsjQO,ServerWebExchange exchange){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(ztryyjsjService.queryWbLgKyNumber(lfRyyjsjQO,exchange));
            }
        }.sendRequest();
    }

    @PostMapping(value = "/queryLocationInfo")
    @ApiOperation(value = "查询定位信息")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "分页参数",name = "offset",dataType = "int",paramType = "query"),
            @ApiImplicitParam(value = "分页参数",name = "pageSize",dataType = "int",paramType = "query")
    })
    public ResponseInfo queryLocationInfo(@RequestBody List<YjInfoParamQo> paramMap,
                                          @RequestParam Integer offset,
                                          Integer pageSize){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<ZdryTxVo> zdryLocations = ztryyjsjService.queryLocationInfo(paramMap,offset,pageSize);
                criteria.addMapResult("record",zdryLocations.getRecords());
                criteria.addMapResult("total",zdryLocations.getTotal());
            }
        }.sendRequest();
    }

    @PostMapping(value = "/exportYjLocationInfo")
    @ApiOperation(value = "导出定位信息")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "查询定位的参数",name = "paramMap",dataType = "String",paramType = "query",allowMultiple = true),
            @ApiImplicitParam(value = "execl名称",name = "exportName",dataType = "String",paramType = "query")
    })
    public Mono<Void> exportYjLocationInfo(@RequestBody List<YjInfoParamQo> param,
                                           @RequestParam(required = false) String exportName,
                                           ServerHttpResponse response) throws Exception{
        //List<YjInfoParamQo> param = JSONArray.parseArray(paramMap,YjInfoParamQo.class);
        String fileName = StringUtils.isEmpty(exportName) ? "重点人员预警定位信息(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : exportName;
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}",fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = ztryyjsjService.exportYjLocationInfo(param, exportName);
        return response.writeWith(flux);
    }

    @PostMapping("/queryTlKyMhQxInfo")
    @ApiOperation("查询铁路客运民航迁徙的详细信息")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "预警类型集合",name = "yjlxs",dataType = "String",paramType ="query",allowMultiple = true),
            @ApiImplicitParam(value = "出发省",name = "sendPro",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "出发市",name = "sendCity",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "到达省",name = "endPro",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "到达市",name = "endCity",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "分页参数",name = "offset",dataType = "int",paramType = "query"),
            @ApiImplicitParam(value = "分页参数",name = "pagesize",dataType = "int",paramType = "query")
    })
    public ResponseInfo queryTlKyMhQxInfo(@RequestBody LfRyyjsjQO lfRyyjsjQO,
                                          @RequestParam List<String> yjlxs,
                                          @RequestParam String sendPro,
                                          @RequestParam(required = false) String sendCity,
                                          @RequestParam String endPro,
                                          @RequestParam(required = false) String endCity,
                                          @RequestParam Integer offset,
                                          @RequestParam Integer pagesize,ServerWebExchange exchange){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                ZdryQxzkUtilBean zdryQxzkUtilBean = new ZdryQxzkUtilBean(yjlxs,sendPro,sendCity,endPro,endCity);
                IPage<ZdryTxVo> ztryyjEntityIPage= ztryyjsjService.queryTlKyMhQxInfo(lfRyyjsjQO,zdryQxzkUtilBean,offset,pagesize,exchange);
                criteria.addMapResult("record",ztryyjEntityIPage.getRecords());
                criteria.addMapResult("total",ztryyjEntityIPage.getTotal());
            }
        }.sendRequest();
    }

    @GetMapping("/exportYjTxInfo")
    @ApiOperation("根据迁徙图上的数据导出预警信息")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "关键字",name = "name",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "开始时间,格式：yyyyMMdd HH:mm:ss",name="yjsj_start",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "结束时间,格式：yyyyMMdd HH:mm:ss",name="yjsj_end",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "人员类别集合",name = "rylbxList",dataType = "String",paramType = "query",allowMultiple=true),
            @ApiImplicitParam(value = "责任部门",name = "zrbmList",dataType = "String",paramType = "query",allowMultiple=true),
            @ApiImplicitParam(value = "管控级别",name = "gkjbxList",dataType = "String",paramType = "query",allowMultiple=true),
            @ApiImplicitParam(value = "人员报警类型",name = "yjlbxs",dataType = "String",paramType = "query",allowMultiple=true),
            @ApiImplicitParam(value = "责任分局",name = "zrfj",dataType = "String",paramType = "query",allowMultiple=true),
            @ApiImplicitParam(value = "责任民警",name = "zrmj",dataType = "String",paramType = "query",allowMultiple=true),
            @ApiImplicitParam(value = "预警类型集合",name = "yjlxs",dataType = "String",paramType ="query",allowMultiple = true),
            @ApiImplicitParam(value = "所属派出所",name = "sspcsList",dataType = "String",paramType ="query",allowMultiple = true),
            @ApiImplicitParam(value = "稳控状态",name = "wkztList",dataType = "String",paramType ="query",allowMultiple = true),
            @ApiImplicitParam(value = "出发省",name = "sendPro",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "出发市",name = "sendCity",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "到达省",name = "endPro",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "到达市",name = "endCity",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "export名称",name = "exportName",dataType = "String",paramType = "query")
    })
    public Mono<Void> exportYjTxInfo(@RequestParam(required = false) String name,
                                     @RequestParam(required = false) String yjsj_start,
                                     @RequestParam(required = false) String yjsj_end,
                                     @RequestParam(required = false) List<String[]> rylbxList,
                                     @RequestParam(required = false) List<String> zrbmList,
                                     @RequestParam(required = false) List<String> gkjbxList,
                                     @RequestParam(required = false) List<String> yjlbxs,
                                     @RequestParam(required = false) List<String> zrfj,
                                     @RequestParam(required = false) List<String> zrmj,
                                     @RequestParam(required = false) List<String[]> hjssxList,
                                     @RequestParam(required = false) List<String> sspcsList,
                                     @RequestParam(required = false)List<String> yjczlbList,
                                     @RequestParam(required = false) List<String> wkztList,
                                     @RequestParam List<String> yjlxs,
                                     @RequestParam String sendPro,
                                     @RequestParam(required = false) String sendCity,
                                     @RequestParam String endPro,
                                     @RequestParam(required = false) String endCity,
                                     @RequestParam(required = false) String exportName,
                                     ServerHttpResponse response,ServerWebExchange exchange) throws Exception{
        LfRyyjsjQO lfRyyjsjQO = new LfRyyjsjQO(name,yjsj_start,yjsj_end,rylbxList,zrbmList,gkjbxList,yjlbxs,zrfj,zrmj,hjssxList,sspcsList,yjczlbList,wkztList);
        ZdryQxzkUtilBean zdryQxzkUtilBean = new ZdryQxzkUtilBean(yjlxs,sendPro,sendCity,endPro,endCity);
        String fileName = StringUtils.isEmpty(exportName) ? "重点人员预警迁徙信息(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : exportName;
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}",fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = ztryyjsjService.exportYjTxInfo(lfRyyjsjQO,zdryQxzkUtilBean,exchange);
        return response.writeWith(flux);
    }

    @PostMapping("/zdryYjStatistics")
    @ApiOperation("重点人员预警数据")
    public ResponseInfo zdryYjStatistics(@RequestBody LfRyyjsjQO lfRyyjsjQO,
                                          ServerWebExchange exchange){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(ztryyjsjService.zdryYjStatistics(lfRyyjsjQO,exchange));
            }
        }.sendRequest();
    }

    @PostMapping("/queryZdryYjStatisticsData")
    @ApiOperation("查询重点人员统计数据")
    public ResponseInfo queryZdryYjStatisticsData(@RequestBody LfRyyjsjQO lfRyyjsjQO,ServerWebExchange exchange){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<ZdryTxVo> ztryyjEntity = ztryyjsjService.queryZdryYjStatisticsData(lfRyyjsjQO,exchange);
                criteria.addMapResult("record",ztryyjEntity.getRecords());
                criteria.addMapResult("total",ztryyjEntity.getTotal());
            }
        }.sendRequest();
    }

    @PostMapping("/exportZdryYjStatisticsData")
    @ApiOperation("导出重点人员预警统计数据")
    public Mono<Void> exportZdryYjStatisticsData(@RequestBody LfRyyjsjQO lfRyyjsjQO,
                                                 @RequestParam(required = false) String exportName,
                                                 ServerWebExchange exchange,
                                                 ServerHttpResponse response) throws Exception {
        String fileName = StringUtils.isEmpty(exportName) ? "重点人员预警统计数据(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : exportName;
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}",fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = ztryyjsjService.exportZdryYjStatisticsData(lfRyyjsjQO, exchange);
        return response.writeWith(flux);
    }

}
