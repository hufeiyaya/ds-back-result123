/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.component.redis.RedisService;
import com.mti.dao.model.LfZdryControlEntity;
import com.mti.exception.BusinessException;
import com.mti.jwt.RequestUtils;
import com.mti.service.ILfZdryControlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.*;

/**
 * <p>
 *  布控信息 前端控制器
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-15
 */
@Api(value="布控数据接口", description="布控数据接口")
@RestController
@RequestMapping("/lf/zdryControl")
@Slf4j
public class LfZdryControlController {

    @Autowired
    ILfZdryControlService zdryControlService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private RequestUtils requestUtils;

    /**
     * 单个保存或者更新布控信息
     *
     * @param zdryControl 布控信息
     * @return
     */
    @ApiOperation(value = "单个保存或者更新布控信息", notes = "单个保存或者更新布控信息")
    @PostMapping("saveOrUpdate")
    public Mono<ResponseInfo> saveOrUpdate(@RequestBody LfZdryControlEntity zdryControl) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                if (StringUtils.isBlank(zdryControl.getId())) {
                    zdryControl.setCreateTime(new Date());
                } else {
                    zdryControl.setUpdateTime(new Date());

                    LfZdryControlEntity lfZdryControlEntity = new LfZdryControlEntity();
                    lfZdryControlEntity.setId(zdryControl.getId());
                    lfZdryControlEntity = zdryControlService.getByBean(lfZdryControlEntity);
                    if (lfZdryControlEntity != null) {
                        if (lfZdryControlEntity.getStatus() == 1) {
                            throw new BusinessException(HttpStatus.SC_INTERNAL_SERVER_ERROR, "已审核状态不允许操作");
                        }
                    }

                }
                boolean result = zdryControlService.saveOrUpdate(zdryControl);
                criteria.addSingleResult(result);
            }
        }.sendRequest());
    }

    /**
     * 根据ZdryControl对象属性获取布控信息
     *
     * @param zdryControl 布控信息
     * @return
     */
    @ApiOperation(value = "根据ZdryControl对象属性获取布控信息", notes = "根据ZdryControl对象属性获取布控信息")
    @GetMapping("getByZdryControl")
    public Mono<ResponseInfo> getByZdryControl(LfZdryControlEntity zdryControl) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfZdryControlEntity zdryControlEntity = zdryControlService.getByBean(zdryControl);
                criteria.addSingleResult(zdryControlEntity);
            }
        }.sendRequest());
    }

    /**
     * 根据ZdryControl对象属性检索所有布控信息
     *
     * @param zdryControl 布控信息
     * @return
     */
    @ApiOperation(value = "根据ZdryControl对象属性检索所有布控信息", notes = "根据ZdryControl对象属性检索所有布控信息")
    @PostMapping("listByZdryControl")
    public Mono<ResponseInfo> listByZdryControl(@RequestBody LfZdryControlEntity zdryControl) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                Collection<LfZdryControlEntity> zdryControls = zdryControlService.listByBean(zdryControl);
                criteria.addSingleResult(zdryControls);
            }
        }.sendRequest());
    }

    /**
     * 根据ZdryControl对象属性分页检索布控信息
     *
     * @param zdryControl 布控信息
     * @return
     */
    @ApiOperation(value = "根据ZdryControl对象属性分页检索布控信息", notes = "根据ZdryControl对象属性分页检索布控信息")
    @PostMapping("pageByZdryControl")
    public Mono<ResponseInfo> pageByZdryControl(@RequestBody LfZdryControlEntity zdryControl) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZdryControlEntity> page = zdryControlService.pageByBean(zdryControl);
                criteria.addSingleResult(page);
            }
        }.sendRequest());
    }

}
