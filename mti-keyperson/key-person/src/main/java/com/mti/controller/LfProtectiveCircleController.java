package com.mti.controller;


import com.alibaba.fastjson.JSON;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.component.redis.CacheKeyEnum;
import com.mti.component.redis.RedisService;
import com.mti.dao.model.LfProtectiveCircleEntity;
import com.mti.service.ILfProtectiveCircleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Api(value = "检查站安检数据接口", description = "检查站安检数据接口")
@RestController
@RequestMapping("/lf/protectivecircle")
@Slf4j
public class LfProtectiveCircleController {

    @Autowired
    private ILfProtectiveCircleService service;

    @Autowired
    private RedisService redisService;

    @ApiOperation(value = "通过id查询记录", notes = "通过id查询记录")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getById(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfProtectiveCircleEntity entity = service.getById(id);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "查询列表记录", notes = "查询列表记录")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Mono<ResponseInfo> selectAll() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<LfProtectiveCircleEntity> entitys = new ArrayList<LfProtectiveCircleEntity>();
                boolean hasCache = redisService.has(CacheKeyEnum.RedisPrefixKeys.R_protective_circle.getKey());
                if (hasCache) {
                    Object entitysJson = redisService.getValue(CacheKeyEnum.RedisPrefixKeys.R_protective_circle.getKey());
                    entitys = JSON.parseArray(entitysJson.toString(), LfProtectiveCircleEntity.class);
                } else {
                    entitys = service.list();
                    redisService.setValue(CacheKeyEnum.RedisPrefixKeys.R_protective_circle.getKey(), JSON.toJSONString(entitys), 86400000);
                }
                criteria.addSingleResult(entitys);
            }
        }.sendRequest());
    }

}
