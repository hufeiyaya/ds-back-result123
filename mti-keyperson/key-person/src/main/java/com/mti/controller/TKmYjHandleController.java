package com.mti.controller;


import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.constant.AlarmFilterEnum;
import com.mti.constant.PageConstants;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.TKmYjHandleEntity;
import com.mti.dao.model.TSwitchTypeEntity;
import com.mti.dao.qo.TkmYjHanQo;
import com.mti.dao.vo.TKmYjHandleVo;
import com.mti.service.ILfZtryyjsjService;
import com.mti.service.TKmYjHandleService;
import com.mti.service.TSwitchTypeServie;
import com.mti.vo.TSwitchTypeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 预警处置表 前端控制器
 * </p>
 *
 * @author duchaof
 * @since 2020-03-11
 */
@Slf4j
@RestController
@RequestMapping("/tKmYjHandle")
@Api(value = "预警处置",description = "预警处置")
public class TKmYjHandleController {
    @Autowired
    private TKmYjHandleService tKmYjHandleService;

    @Autowired
    private TSwitchTypeServie tSwitchTypeServie;

    @PostMapping("/add")
    @ApiOperation("添加预警处置")
    public Mono<ResponseInfo> addTKmYjHandle(@RequestBody List<TKmYjHandleEntity> tKmYjHandleEntityList,
                                             ServerWebExchange exchange){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean flag = tKmYjHandleService.addTKmYjHandle(tKmYjHandleEntityList,exchange);
                criteria.addSingleResult(flag ? "添加成功": "添加失败");
            }
        }.sendRequest());
    }

    @GetMapping("/delete")
    @ApiOperation("删除预警处置")
    public Mono<ResponseInfo> deleteTKmYjHandle(@RequestParam String id){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean flag = tKmYjHandleService.deleteTKmYjHandle(id);
                criteria.addSingleResult(flag ? "删除成功": "删除失败");
            }
        }.sendRequest());
    }

    @PostMapping("/update")
    @ApiOperation("更新预警处置")
    public Mono<ResponseInfo> updateTKmYjHandle(@RequestBody TKmYjHandleEntity tKmYjHandleEntity){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean flag = tKmYjHandleService.updateTKmYjHandle(tKmYjHandleEntity);
                criteria.addSingleResult(flag ? "更新成功": "更新失败");
            }
        }.sendRequest());
    }

    @PostMapping("/list")
    @ApiOperation("查询预警处置信息,不分页")
    public Mono<ResponseInfo> listTKmYjHandle(@RequestBody TkmYjHanQo tkmYjHanQo){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(tKmYjHandleService.listTKmYjHandle(tkmYjHanQo));
            }
        }.sendRequest());
    }

    @PostMapping("/page")
    @ApiOperation("查询预警处置信息,分页")
    public Mono<ResponseInfo> pageTKmYjHandle(@RequestBody TkmYjHanQo tkmYjHanQo){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<TKmYjHandleVo> page = tKmYjHandleService.pageTKmYjHandle(tkmYjHanQo);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }

    @GetMapping(value = "/exportYjHandleInfo")
    @ApiOperation(value = "根据添加筛选导出execl")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "导出的execl名称",name = "exportName",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "预警主键id",name = "id",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "关键字",name = "keyWords",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "开始时间",name = "start_time",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "结束时间",name = "end_time",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "身份证号",name = "sfzh",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "处置标志",name = "handleFlag",dataType = "int",paramType = "query"),
            @ApiImplicitParam(value = "处置结果",name = "handleResult",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "处置内容",name = "handleContent",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "关联id,预警表的唯一标识:wybs",name = "refId",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "所属分局",name = "ssfj",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "所属派出所",name = "sspcs",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "警种",name = "jz",dataType = "String",paramType = "query")
    })
    public Mono<Void> exportYjHandleInfo(@RequestParam(required = false) String exportName, @RequestParam(required = false) String id,
                                         @RequestParam(required = false) String keyWords, @RequestParam(required = false) String start_time,
                                         @RequestParam(required = false) String end_time, @RequestParam(required = false) String sfzh,
                                         @RequestParam(required = false) Integer handleFlag, @RequestParam(required = false) String handleResult,
                                         @RequestParam(required = false) String handleContent, @RequestParam(required = false) String refId,
                                         @RequestParam(required = false) String ssfj, @RequestParam(required = false) String sspcs,
                                         @RequestParam(required = false) String jz,
                                         ServerHttpResponse response) throws Exception{
        TkmYjHanQo tkmYjHanQo = new TkmYjHanQo(id,keyWords,start_time,end_time,sfzh,handleFlag,handleResult,handleContent,refId,
                                               ssfj,sspcs,jz);
        String fileName = StringUtils.isEmpty(exportName) ? "重点人员预警处置(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : exportName;
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}",fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = tKmYjHandleService.exportYjHandleInfo(tkmYjHanQo, exportName);
        return response.writeWith(flux);
    }



    @PostMapping("/getYjManageInfo")
    @ApiOperation("获取预警处置信息")
    public Mono<ResponseInfo> getYjManageInfo(@RequestParam(required = false) List<Integer> typeList,
                                              @RequestParam(required = false) String name) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                Map result = tSwitchTypeServie.getSwitchByType(typeList, name);
                criteria.addSingleResult(result);
            }
        }.sendRequest());
    }

    @PostMapping("/setYjManageInfo")
    @ApiOperation("设置预警处置信息")
    public Mono<ResponseInfo> setYjManageInfo(@RequestBody(required = true) TSwitchTypeVo tSwitchTypeVo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                tSwitchTypeServie.remove(new QueryWrapper<>());
                tSwitchTypeServie.saveList(tSwitchTypeVo.getList());
                tSwitchTypeServie.setTime(tSwitchTypeVo.getStartTime(),tSwitchTypeVo.getEndTime());
                criteria.addSingleResult("设置成功");
            }
        }.sendRequest());
    }


    @GetMapping("/initYjManageInfo")
    @ApiOperation("初始化预警处置信息")
    public Mono<ResponseInfo> initYjManageInfo() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                tSwitchTypeServie.initYjManageInfo();
                criteria.addSingleResult("");
            }
        }.sendRequest());
    }

}

