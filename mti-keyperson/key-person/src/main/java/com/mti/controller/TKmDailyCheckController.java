package com.mti.controller;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.TKmDailyCheckEntity;
import com.mti.dao.qo.LfZdryQO;
import com.mti.dao.qo.TkmDCheckQo;
import com.mti.exception.BusinessException;
import com.mti.service.TKmDailyCheckService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @Classname TKmDailyCheckController
 * @Description TODO
 * @Date 2020/2/27 17:05
 * @Created by duchaof
 * @Version 1.0
 */
@Api(value="日常核查",description="日常核查")
@RestController
@RequestMapping("/daily/check")
@Slf4j
public class TKmDailyCheckController {
    @Autowired
    private TKmDailyCheckService tKmDailyCheckService;

    @PostMapping("/add")
    @ApiOperation("添加核查记录")
    public Mono<ResponseInfo> saveTkmDailyCheck(@RequestBody TKmDailyCheckEntity tKmDailyCheckEntity){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(tKmDailyCheckService.saveTKmDailyCheck(tKmDailyCheckEntity));
            }
        }.sendRequest());
    }

    @PostMapping("/page")
    @ApiOperation("查询核查记录，分页")
    public Mono<ResponseInfo> queryTkmDailyCheckPage(@RequestBody TkmDCheckQo tkmDCheckQo){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<TKmDailyCheckEntity> iPage = tKmDailyCheckService.queryTkmDailyCheckPage(tkmDCheckQo);
                criteria.addMapResult("record",iPage.getRecords());
                criteria.addMapResult("total",iPage.getTotal());
            }
        }.sendRequest());
    }

    @PostMapping("/list")
    @ApiOperation("查询核查记录，不分页")
    public Mono<ResponseInfo> queryTkmDailyCheckList(@RequestBody TKmDailyCheckEntity tKmDailyCheckEntity){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(tKmDailyCheckService.queryTkmDailyCheckList(tKmDailyCheckEntity));
            }
        }.sendRequest());
    }


    @ApiOperation(value = "导出重点人员信息为Excel")
    @GetMapping("/excel/export")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "关键字",name = "keyWords",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "开始时间",name = "startTime",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "结束时间",name = "endTime",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "核查结果",name = "checkResult",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "所属分局",name = "ssfj",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "管辖派出所",name = "sspcs",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "警种",name = "jz",dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "execl名称",name = "exportName",dataType = "String",paramType = "query")
    })
    public Mono<Void> exportDailyCheck(
                                          @RequestParam(required = false) String keyWords,
                                          @RequestParam(required = false) String startTime,
                                          @RequestParam(required = false) String endTime,
                                          @RequestParam(required = false) String checkResult,
                                          @RequestParam(required = false) String ssfj,
                                          @RequestParam(required = false) String sspcs,
                                          @RequestParam(required = false) String jz,
                                          @RequestParam(required = false) String exportName,
                                          ServerHttpResponse response
    ) throws IOException {
        String fileName = StringUtils.isEmpty(exportName) ? "重点人员日常核查表(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : exportName;
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}",fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = tKmDailyCheckService.exportDailyCheck(exportName,keyWords,startTime,endTime,checkResult,ssfj,sspcs,jz);
        return response.writeWith(flux);
    }
}
