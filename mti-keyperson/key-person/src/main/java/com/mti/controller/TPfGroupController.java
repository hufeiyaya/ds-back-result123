package com.mti.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.TPfGroupEntity;
import com.mti.dao.model.TPfGroupZdryEntity;
import com.mti.exception.BusinessException;
import com.mti.service.TPfGroupService;
import com.mti.service.TPfGroupZdryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * <p>
 * 重点人员分组表
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */
@Slf4j
@RestController
@RequestMapping("/tPfGroup")
@Api(value = "重点人员分组接口", description = "重点人员分组接口")
public class TPfGroupController {

    @Autowired
    private TPfGroupService tPfGroupService;
    @Autowired
    private TPfGroupZdryService tPfGroupZdryService;


    @ApiOperation(value = "新增分组数据", notes = "新增分组数据")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Mono<ResponseInfo> add(ServerWebExchange exchange, @RequestBody TPfGroupEntity tPfGroupEntity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean flag = tPfGroupService.add(exchange, tPfGroupEntity);
                if (!flag) {
                    throw new BusinessException(10020, "添加异常");
                }
                criteria.addSingleResult("操作成功");
            }
        }.sendRequest());
    }

    @ApiOperation(value = "修改分组数据", notes = "修改分组数据")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Mono<ResponseInfo> update(ServerWebExchange exchange, @RequestBody TPfGroupEntity tPfGroupEntity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean flag = tPfGroupService.update(exchange, tPfGroupEntity);
                if (!flag) {
                    throw new BusinessException(10020, "修改异常");
                }
                criteria.addSingleResult("操作成功");
            }
        }.sendRequest());
    }


    @ApiOperation(value = "根据当前登录用户获取分组信息", notes = "根据当前登录用户获取分组信息")
    @RequestMapping(value = "/getGroupInfoByCurrentUser", method = RequestMethod.GET)
    public Mono<ResponseInfo> getGroupInfoByAccId(ServerWebExchange exchange) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<TPfGroupEntity> list = tPfGroupService.getGroupInfoByAccId(exchange);
                criteria.addSingleResult(list);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "删除分组信息", notes = "删除分组信息")
    @RequestMapping(value = "delGroup/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> delGroupById(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                TPfGroupEntity tPfGroupEntity = tPfGroupService.getById(id);
                if (tPfGroupEntity.isDefault()) {
                    throw new BusinessException(10020, "默认分组不可删除！");
                }
                //删除分组信息
                tPfGroupService.removeById(id);
                //删除分组下的所有重点人员
                tPfGroupZdryService.remove(new QueryWrapper<TPfGroupZdryEntity>().eq("group_id", id));
                criteria.addSingleResult("删除成功");
            }
        }.sendRequest());
    }

}

