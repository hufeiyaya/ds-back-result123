package com.mti.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.constant.PageConstants;
import com.mti.service.ILfZtryyjsjService;
import com.mti.service.LfZdryBaseService;
import com.mti.service.TKmYjHandleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * @Classname KmDbPushController
 * @Description TODO
 * @Date 2020/4/3 14:59
 * @Created by duchaof
 * @Version 1.0
 */
@Api(value="给大保推送数据类",description = "给大保推送数据类")
@RestController
@RequestMapping("/kmdb/push")
@Slf4j
public class KmDbPushController {
    @Autowired
    private LfZdryBaseService lfZdryBaseService;
    @Autowired
    private ILfZtryyjsjService ztryyjsjService;
    @Autowired
    private TKmYjHandleService tKmYjHandleService;


    @GetMapping("/pushZdryInfo")
    @ApiOperation("推送重点人员数据")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "分页参数",name = "offset",dataType = "int",paramType = "query"),
            @ApiImplicitParam(value = "分页参数",name = "pageSize",dataType = "int",paramType = "query")
    })
    public Mono<ResponseInfo> pushZdryInfo(@RequestParam Integer offset, @RequestParam Integer pageSize){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                Map<String,Object> result = lfZdryBaseService.queryPushZdryInfo(offset,pageSize);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, result.get("records"));
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, result.get("total"));
            }
        }.sendRequest());
    }

    @GetMapping("/pushZdryYjInfo")
    @ApiOperation("推送重点人员预警数据")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "分页参数",name = "offset",dataType = "int",paramType = "query"),
            @ApiImplicitParam(value = "分页参数",name = "pageSize",dataType = "int",paramType = "query")
    })
    public Mono<ResponseInfo> pushZdryYjInfo(@RequestParam Integer offset, @RequestParam Integer pageSize){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                Map<String,Object> result = ztryyjsjService.pushZdryYjInfo(offset,pageSize);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, result.get("records"));
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, result.get("total"));
            }
        }.sendRequest());
    }

    @GetMapping("/pushYjHandleInfo")
    @ApiOperation("推送重点人员预警处置数据")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "分页参数",name = "offset",dataType = "int",paramType = "query"),
            @ApiImplicitParam(value = "分页参数",name = "pageSize",dataType = "int",paramType = "query")
    })
    public Mono<ResponseInfo> pushYjHandleInfo(@RequestParam Integer offset, @RequestParam Integer pageSize){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                Map<String,Object> result = tKmYjHandleService.pushYjHandleInfo(offset,pageSize);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, result.get("records"));
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, result.get("total"));
            }
        }.sendRequest());
    }
}
