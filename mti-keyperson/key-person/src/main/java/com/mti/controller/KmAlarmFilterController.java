package com.mti.controller;

import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.qo.KmAlarmAreaQO;
import com.mti.service.KmAlarmAreaService;
import com.mti.service.KmAlarmTypeService;
import com.mti.vo.KmAlarmVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

/**
 * @Classname AlarmFilterController
 * @Description TODO 对手机预警和人脸识别预警设置过滤
 * @Date 2020/11/20 09：03
 * @Created by hf
 * @Version 1.0
 */
@Api(value = "预警拦截设置", description = "预警拦截设置")
@RestController
@RequestMapping("/alarmFilterSet")
@Slf4j
public class KmAlarmFilterController {
    @Autowired
    private KmAlarmTypeService kmAlarmTypeService;

    @Autowired
    private KmAlarmAreaService kmAlarmAreaService;

    @ApiOperation("获取预警开关状态")
    @GetMapping("/getAlarmStatus")
    public Mono<ResponseInfo> getAlarmStatus() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(kmAlarmTypeService.getAlarmStatus());
            }
        }.sendRequest());
    }

    @ApiOperation("设置预警过滤信息")
    @PostMapping("/setAlarmFilterInfo")
    public Mono<ResponseInfo> setAlarmStatus(@RequestBody KmAlarmVo KmAlarmVo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                kmAlarmTypeService.setAlarmStatus(KmAlarmVo);
                criteria.addSingleResult("设置成功");
            }
        }.sendRequest());
    }

    @ApiOperation("删除预警区域信息")
    @GetMapping("/delAlarmAreaInfo")
    public Mono<ResponseInfo> delAlarmAreaInfo(String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                try {
                    kmAlarmAreaService.removeById(id);
                    criteria.addSingleResult("删除成功");
                } catch (Exception e) {
                    log.error("删除预警区域信息失败", e);
                    criteria.addSingleResult("删除失败");
                }
            }
        }.sendRequest());
    }

    @ApiOperation("检测坐标是否在预警区域内")
    @PostMapping("/authIsInArea")
    public Mono<ResponseInfo> authIsInArea(@RequestBody KmAlarmAreaQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean flag = kmAlarmAreaService.authIsInArea(qo);
                criteria.addSingleResult(flag);
            }
        }.sendRequest());
    }

}
