package com.mti.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 重点人员-自定义字段表 前端控制器
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@RestController
@RequestMapping("/kmZdryCustom")
public class KmZdryCustomController {

}

