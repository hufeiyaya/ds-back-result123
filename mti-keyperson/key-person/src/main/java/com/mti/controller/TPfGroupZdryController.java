package com.mti.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.constant.PageConstants;
import com.mti.dao.model.LfZdryBaseEntity;
import com.mti.dao.qo.TPfGroupZdryQo;
import com.mti.dao.vo.TPfGroupZdryVo;
import com.mti.service.TPfGroupZdryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * <p>
 * 重点人员分组表
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */
@Slf4j
@RestController
@RequestMapping("/tPfGroupZdry")
@Api(value = "重点人员分组列表接口", description = "重点人员分组列表接口")
public class TPfGroupZdryController {

    @Autowired
    private TPfGroupZdryService tPfGroupZdryService;

    @ApiOperation(value = "更新重点人员分组情况", notes = "更新重点人员分组情况")
    @RequestMapping(value = "/updateZdryGroupInfo", method = RequestMethod.POST)
    public Mono<ResponseInfo> updateZdryGroupInfo(
            @RequestParam(value = "groupIdList",required = false) @ApiParam(value = "分组ID", name = "groupIdList") List<String> groupIdList,
            @RequestParam(value = "zdryId") @ApiParam(value = "重点人员ID", name = "zdryId", required = true) String zdryId) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                try {
                    tPfGroupZdryService.updateZdryGroupInfo(groupIdList, zdryId);
                    criteria.addSingleResult("操作成功");
                } catch (Exception e) {
                    log.error("失败", e);
                    criteria.addSingleResult("操作失败");
                }
            }
        }.sendRequest());
    }

    @ApiOperation(value = "获取重点人员分组信息", notes = "获取重点人员分组信息")
    @RequestMapping(value = "/getZdryGroupInfo", method = RequestMethod.POST)
    public Mono<ResponseInfo> getZdryGroupInfo(ServerWebExchange exchange, @RequestParam(value = "zdryId", defaultValue = "", required = false) @ApiParam(value = "重点人员ID", name = "zdryId") String zdryId) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<TPfGroupZdryVo> list = tPfGroupZdryService.getZdryGroupInfo(exchange, zdryId);
                criteria.addSingleResult(list);
            }
        }.sendRequest());

    }

    @ApiOperation(value = "获取被关注重点人员列表信息", notes = "获取被关注重点人员列表信息")
    @RequestMapping(value = "/getPageList", method = RequestMethod.POST)
    public Mono<ResponseInfo> getPageList(ServerWebExchange exchange, @RequestBody final TPfGroupZdryQo qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZdryBaseEntity> page = null;
                page = tPfGroupZdryService.getPageList(exchange,qo);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());

    }


}

