/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.controller;


import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.LfGkjlEntity;
import com.mti.service.ILfGkjlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Api(value = "管控记录数据接口",description = "管控记录数据接口")
@RestController
@RequestMapping("/lf/gkjl")
@Slf4j
public class LfGkjlController {

    @Autowired
    private ILfGkjlService service;

    @ApiOperation(value = "通过id查询记录",notes = "通过id查询记录")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getById(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfGkjlEntity entity = service.getById(id);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "新增管控记录数据",notes = "新增管控记录数据")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Mono<ResponseInfo> saveOrUpdate(@RequestBody LfGkjlEntity entity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean flag = service.saveOrUpdateGkjl(entity);
                criteria.addSingleResult(flag ? entity.getId() : "更新或者添加失败");
            }
        }.sendRequest());
    }


    @ApiOperation(value = "通过身份证号查询记录",notes = "通过身份证号查询记录")
    @RequestMapping(value = "/sfzh/{sfzh}", method = RequestMethod.GET)
    public Mono<ResponseInfo> queryListBySfzh(@PathVariable("sfzh") String sfzh) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<LfGkjlEntity> entity = service.queryListBySfzh(sfzh);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

}
