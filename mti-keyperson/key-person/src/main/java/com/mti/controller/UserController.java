//package com.mti.controller;
//
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.mti.component.JwcComponent;
//import com.mti.component.ResponseCallBack;
//import com.mti.component.ResponseCriteria;
//import com.mti.component.ResponseInfo;
//import com.mti.dao.model.UserEntity;
//import com.mti.exception.BusinessException;
//import com.mti.service.IUserService;
//import com.mti.utils.DateUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import reactor.core.publisher.Mono;
//
//import java.util.List;
//import java.util.Optional;
//
///**
// * UserController class
// *
// * @author zhaoyj
// * @date 2019/3/12
// */
//@RestController
//@RequestMapping(value = "/users")
//@Slf4j
//public class UserController {
//
//    @Autowired
//    private IUserService userService;
//
//
//    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
//    public Mono<ResponseInfo> getById(@PathVariable("id") String id) {
//        return Mono.just(new ResponseCallBack() {
//            @Override
//            public void execute(ResponseCriteria criteria, Object... obj) {
//                UserEntity user = userService.getById(id);
//                criteria.addSingleResult(user);
//            }
//        }.sendRequest());
//    }
//    @RequestMapping(value = "", method = RequestMethod.GET)
//    public Mono<ResponseInfo> selectAll() {
//        return Mono.just(new ResponseCallBack() {
//            @Override
//            public void execute(ResponseCriteria criteria, Object... obj) {
//                List<UserEntity> users = userService.list();
//                criteria.addSingleResult(users);
//            }
//        }.sendRequest());
//    }
//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public Mono<ResponseInfo> login(@RequestParam String userName,@RequestParam String password, @RequestParam Integer type) {
//        return Mono.just(new ResponseCallBack() {
//            @Override
//            public void execute(ResponseCriteria criteria, Object... obj) {
//                UserEntity user = Optional.ofNullable(userService.getOne(new QueryWrapper<UserEntity>().lambda().eq(UserEntity::getType,type).eq(UserEntity::getName,userName).eq(UserEntity::getPassword,password))).orElseThrow(() -> new BusinessException(500,"用户名或者密码错误"));
//                String token = JwcComponent.createToken(user.getName());
//                criteria.addSingleResult(token);
//            }
//        }.sendRequest());
//    }
//    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
//    public Mono<ResponseInfo> saveUser(@RequestBody UserEntity user) {
//        return Mono.just(new ResponseCallBack() {
//            @Override
//            public void execute(ResponseCriteria criteria, Object... obj) {
//                boolean code = userService.save(user);
//                criteria.addSingleResult(code?"保存成功":"保存失败");
//            }
//        }.sendRequest());
//    }
//
//    @RequestMapping(value = "/findUserByIp", method = RequestMethod.GET)
//    public Mono<ResponseInfo> findUserByIp(
//            @RequestParam(required = false) String mac,
//            @RequestParam(required = false) String ip) {
//
//        return Mono.just(new ResponseCallBack() {
//            @Override
//            public void execute(ResponseCriteria criteria, Object... obj) {
//                log.info("获取用户信息：{}", DateUtils.createDate());
//                QueryWrapper<UserEntity> wrapper = new QueryWrapper<>();
//                wrapper.lambda().eq(null!=ip,UserEntity::getSipIp,ip)
//                        .eq(null!=mac,UserEntity::getSipMac,mac);
//                Optional<UserEntity> user = Optional.ofNullable(wrapper.nonEmptyOfWhere()?userService.getOne(wrapper):null);
//                criteria.addSingleResult(user);
//            }
//        }.sendRequest());
//    }
//}
