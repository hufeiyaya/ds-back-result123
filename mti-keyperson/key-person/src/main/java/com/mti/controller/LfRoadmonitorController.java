package com.mti.controller;


import com.alibaba.fastjson.JSON;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.component.redis.CacheKeyEnum;
import com.mti.component.redis.RedisService;
import com.mti.dao.model.LfRoadmonitorEntity;
import com.mti.service.ILfRoadmonitorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Api(value="视频监控数据接口",description="视频监控数据接口")
@RestController
@RequestMapping("/lf/roadmonitor")
@Slf4j
public class LfRoadmonitorController {

    @Autowired
    private ILfRoadmonitorService service;

    @Autowired
    private RedisService redisService;

    @ApiOperation(value="通过id查询记录",notes="通过id查询记录")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getById(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfRoadmonitorEntity entity = service.getById(id);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

    @ApiOperation(value="查询列表记录",notes="查询列表记录\n" +
            "cid=1：卡口设备数据 layerid:405,406,407,408\n" +
            "cid=2：视频点位数据 layerid:257,401,402,403\n" +
            "cid=3：卡口/视频点位 layerid:405,406,407,408,257,401,402,403\n" +
            "cid=407：卡口监控 人脸 407（在线）\n" +
            "cid=408：卡口监控 人脸 408（离线）\n" +
            "cid=405：卡口监控 车辆 405 (在线)\n" +
            "cid=406：卡口监控 车辆 406（离线）\n" +
            "cid=257：枪型监控：257 (在线)\n" +
            "cid=401：枪型监控：401（离线）\n" +
            "cid=402：球型监控：402 (在线)\n" +
            "cid=403：球型监控：403（离线）\n" +
            "\n" +
            "卡口监控：layerid 人脸：407（在线）408（离线） 车辆：405(在线) 406（离线）\n" +
            "枪型监控：layerid 257(在线) 401（离线）\n" +
            "球型监控：layerid 402（在线） 403（离线）")
    @RequestMapping(value = "/list/{cid}", method = RequestMethod.GET)
    public Mono<ResponseInfo> list(@PathVariable("cid") String cid) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<LfRoadmonitorEntity> entitys = new ArrayList<LfRoadmonitorEntity>();
                boolean hasCache = redisService.has(CacheKeyEnum.RedisPrefixKeys.R_roadmonitor.getKey() + cid);
                if (hasCache) {
                    Object entitysJson = redisService.getValue(CacheKeyEnum.RedisPrefixKeys.R_roadmonitor.getKey() + cid);
                    entitys = JSON.parseArray(entitysJson.toString(), LfRoadmonitorEntity.class);
                } else {
                    entitys = service.queryList(cid);
                    redisService.setValue(CacheKeyEnum.RedisPrefixKeys.R_roadmonitor.getKey() + cid, JSON.toJSONString(entitys), 86400000);
                }
                criteria.addSingleResult(entitys);
            }
        }.sendRequest());
    }

}
