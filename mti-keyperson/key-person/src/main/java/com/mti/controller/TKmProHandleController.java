package com.mti.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.component.snowflake.KeyWorker;
import com.mti.constant.PageConstants;
import com.mti.dao.model.TKmProHandle;
import com.mti.dao.qo.TkmProHanQo;
import com.mti.service.TKmProHandleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * <p>
 * 信访/聚访维权处理或化解信息 前端控制器
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
@Api(value="信访/聚访维权处理或化解信息",description="信访/聚访维权处理或化解信息")
@RestController
@RequestMapping("/tKmProHandle")
@Slf4j
public class TKmProHandleController {
    @Autowired
    private TKmProHandleService tKmProHandleService;

    @ApiOperation("添加信访/聚访的处理或化解的方式")
    @PostMapping("/add")
    public Mono<ResponseInfo> addTkmProHandle(ServerWebExchange exchange,@RequestBody TKmProHandle tKmProHandle) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                tKmProHandle.setId(String.valueOf(KeyWorker.nextId()));
                criteria.addSingleResult(tKmProHandleService.add(exchange,tKmProHandle));
            }
        }.sendRequest());
    }

    @ApiOperation("删除信访/聚访的处理或化解的方式")
    @GetMapping("/delete")
    @ApiImplicitParam(value = "主键",name = "id",dataType = "int",paramType = "query")
    public Mono<ResponseInfo> deleteTkmProHandle(@RequestParam String id){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(tKmProHandleService.removeById(id));
            }
        }.sendRequest());
    }

    @ApiOperation("更新信访/聚访的处理或化解的方式")
    @PostMapping("/update")
    public Mono<ResponseInfo> updateTkmProHandle(@RequestBody TKmProHandle tKmProHandle){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(tKmProHandleService.updateById(tKmProHandle));
            }
        }.sendRequest());
    }
    @ApiOperation("查询信访/聚访的处理或化解的方式，不分页")
    @PostMapping("/list")
    public Mono<ResponseInfo> listTkmProHandle(@RequestBody TKmProHandle tKmProHandle){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(tKmProHandleService.listTkmProHandle(tKmProHandle));
            }
        }.sendRequest());
    }

    @ApiOperation("查询信访/聚访的处理或化解的方式，分页")
    @PostMapping("/page")
    public Mono<ResponseInfo> pageTkmProHandle(@RequestBody TkmProHanQo tkmProHanQo){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<TKmProHandle> page = tKmProHandleService.pageTkmProHandle(tkmProHanQo);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }

}

