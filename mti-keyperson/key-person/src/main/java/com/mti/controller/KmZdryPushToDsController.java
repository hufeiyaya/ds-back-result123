package com.mti.controller;

import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.vo.ZdryYjPushToDsUtil;
import com.mti.service.ILfZtryyjsjService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Classname KmZdryPushToDsController
 * @Description TODO
 * @Date 2020/5/4 10:41
 * @Created by duchaof
 * @Version 1.0
 */
@Api(value = "给ds推送预警信息", description = "给ds推送预警信息")
@RestController
@RequestMapping("/whale/km/focalAlarm")
@Slf4j
public class KmZdryPushToDsController {
    @Autowired
    private ILfZtryyjsjService iLfZtryyjsjService;


    /**
     * 0 不推送，1 推送。
     */
    private static volatile AtomicInteger pushFlag = new AtomicInteger(0);

    @ApiOperation("给ds推送预警的接口")
    @PostMapping("/welcomeGetLastestAlarm")
    public Object pushYjToDs() {
        Map<String, Object> resultMap = new HashMap<>();
        try {
            resultMap.put("code", 0);
            if (pushFlag.get() == 0) {
                resultMap.put("data", null);
                resultMap.put("msg", "DS推送开关未打开!");
            } else {
                List<ZdryYjPushToDsUtil> data = iLfZtryyjsjService.queryYjDataPushToDs();
                resultMap.put("data", data);
                resultMap.put("msg", "DS数据推送成功!");
            }
            log.info("给ds推送预警的接口的出参打印：{}" + resultMap);
            return resultMap;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("DS远程调用时发生异常：==========》{}", e.getMessage());
            resultMap.put("code", -1);
            resultMap.put("msg", "DS数据推送失败!");
            return resultMap;
        }
    }

    @GetMapping("/onOffDsPushSwitch")
    @ApiOperation("打开和关闭")
    public Mono<ResponseInfo> onOffDsPushSwitch(@RequestParam(required = false) Integer pFlag) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                if (null != pFlag) {
                    pushFlag.getAndSet(pFlag);
                }
                criteria.addSingleResult(pushFlag.get());
            }
        }.sendRequest());
    }

}
