/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.component.redis.CacheKeyEnum.RedisPrefixKeys;
import com.mti.component.redis.RedisService;
import com.mti.constant.PageConstants;
import com.mti.dao.dto.ZdryDto;
import com.mti.dao.model.*;
import com.mti.dao.qo.AddressQO;
import com.mti.dao.qo.LfZdryQO;
import com.mti.dao.qo.PoliceQO;
import com.mti.jwt.AccountBo;
import com.mti.jwt.RequestUtils;
import com.mti.mapper.LfZdryTempMapper;
import com.mti.service.*;
import com.mti.service.impl.SpecialPersonComponent;
import com.mti.vo.ZdryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Api(value = "重点人员数据接口",description = "重点人员数据接口")
@RestController
@RequestMapping("/lf/zdry")
@Slf4j
public class LfZdryController {

    @Autowired
    private ILfZdryService service;
    @Autowired
    private KmZdryZrbmService  kmzdryZrbmService;
    @Autowired
    private RedisService redisService;

    @Autowired
    private RequestUtils requestUtils;
    @Autowired
    private SpecialPersonComponent specialPersonComponent;

    @Autowired
    private ISpecialPersonService iSpecialPersonService;

    @Autowired
    private ISysDictService iSysDictService;

    @Autowired
    private CacheableService cacheableService;

    @Autowired
    private IThirdService thirdService;
    
    @Resource
	private LfZdryTempMapper tempZdryMapper;
    
    @ApiOperation(value = "通过序号查询记录",notes = "通过序号查询记录")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getById(@PathVariable("id") String id) {

        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfZdryEntity entity = service.getById(id);
                if (org.apache.commons.lang3.StringUtils.isNotBlank(entity.getRylbx())) {
                    LfZdryZrbmEntity lfZdryZrbmEntity = service.getByZrbm(entity.getRylbx());
                    entity.setZrbmlxr(lfZdryZrbmEntity.getLxr());
                    entity.setZrbmlxdh(lfZdryZrbmEntity.getLxdh());
                }
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "新增重点人员数据",notes = "新增重点人员数据")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Mono<ResponseInfo> save(ServerWebExchange exchange, @RequestBody ZdryDto entity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
            	boolean flag = service.saveZdry(exchange, entity);
                criteria.addSingleResult(flag ? entity.getId() : "添加失败");
            }
        }.sendRequest());
    }

    @ApiOperation(value = "根据大类小类获取警种",notes = "根据大类小类获取警种")
    @RequestMapping(value = "/getPoliceType", method = RequestMethod.POST)
    public Mono<ResponseInfo> getPoliceType(ServerWebExchange exchange, @RequestBody PoliceQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
            	KmZdryZrbmEntity entity = kmzdryZrbmService.getPoliceType(exchange, qo);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

    /*private void setUpAddress(Map<String,String> regiones,LfZdryEntity entity){
        entity.setHjs(regiones.get(entity.getHjs()));
        entity.setHjsS(regiones.get(entity.getHjsS()));
        entity.setHjx(regiones.get(entity.getHjx()));
        entity.setXzzs(regiones.get(entity.getXzzs()));
        entity.setXzzsS(regiones.get(entity.getXzzsS()));
        entity.setXzzx(regiones.get(entity.getXzzx()));
    }*/

    @ApiOperation(value = "重点人员修改",notes = "重点人员修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Mono<ResponseInfo> update(ServerWebExchange exchange, @RequestBody LfZdryEntity entity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                   
            	LfZdryTempEntity temp= tempZdryMapper.selectById(entity.getId());
        		if(temp!=null) {
        			criteria.addSingleResult("该人员的其它流程中还未结束，请结束后再操作！");
        		} else {
        			boolean flag = service.updateZdry(exchange, entity);
        			criteria.addSingleResult(flag ? "操作成功" : "操作失败");
        		}    
            }
        }.sendRequest());
    }

    @ApiOperation(value = "重点人员移交",notes = "重点人员移交")
    @RequestMapping(value = "/transfer", method = RequestMethod.POST)
    public Mono<ResponseInfo> transfer(ServerWebExchange exchange, @RequestBody LfZdryEntity entity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                   
            	LfZdryTempEntity temp= tempZdryMapper.selectById(entity.getId());
        		if(temp!=null) {
        			criteria.addSingleResult("该人员的其它流程中还未结束，请结束后再操作！");
        		} else {
        			boolean flag = service.transfer(exchange, entity);
        			criteria.addSingleResult(flag ? "操作成功" : "操作失败");
        		}       
            }
        }.sendRequest());
    }
    
    @ApiOperation(value = "重点人员列表，通过sfzh查询记录",notes = "重点人员列表，通过id查询记录")
    @RequestMapping(value = "/getZdry/{sfzh}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getBySfzh(@PathVariable("sfzh") String sfzh) {
        return Mono.just(new ResponseCallBack() {
			@Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfZdryBaseEntity entity = service.getBySfzh(sfzh);
                ZdryDto dto = (ZdryDto)entity;
                
               /* if (org.apache.commons.lang3.StringUtils.isNotBlank(entity.getRylbx())) {
                    LfZdryZrbmEntity lfZdryZrbmEntity = service.getByZrbm(entity.getRylbx());
                    entity.setZrbmlxr(lfZdryZrbmEntity.getLxr());
                    entity.setZrbmlxdh(lfZdryZrbmEntity.getLxdh());
                }*/
                //人员属性
               /* if(entity!=null) {
                	SysDictEntity rysxdictEntity = iSysDictService.getEntityByKey("zdry_ryxx",String.valueOf(entity.getRysx()));
                	if(null != rysxdictEntity){
                		entity.setRysxName(rysxdictEntity.getValue());
                	}
                	//预警处置级别
                	SysDictEntity yjczlbdictEntity = iSysDictService.getEntityByKey("zdry_yjczlb",String.valueOf(entity.getYjczlb()));
                	if(null!= yjczlbdictEntity){
                		entity.setYjczlbName(yjczlbdictEntity.getValue());
                	}
                }*/
               /* LfZdryTempEntity tempEntity = new LfZdryTempEntity();
                if(entity==null) {
                	//在临时表中
                	tempEntity = tempZdryMapper.selectById(id);
                     if (org.apache.commons.lang3.StringUtils.isNotBlank(entity.getRylbx())) {
                         LfZdryZrbmEntity lfZdryZrbmEntity = service.getByZrbm(entity.getRylbx());
                         entity.setZrbmlxr(lfZdryZrbmEntity.getLxr());
                         entity.setZrbmlxdh(lfZdryZrbmEntity.getLxdh());
                     }
                     //人员属性
                     SysDictEntity rysxdictEntity2 = iSysDictService.getEntityByKey("zdry_ryxx",String.valueOf(tempEntity.getRysx()));
                     if(null != rysxdictEntity2){
                    	 tempEntity.setRysxName(rysxdictEntity2.getValue());
                     }
                     //预警处置级别
                     SysDictEntity yjczlbdictEntity2 = iSysDictService.getEntityByKey("zdry_yjczlb",String.valueOf(tempEntity.getYjczlb()));
                     if(null!= yjczlbdictEntity2){
                    	 tempEntity.setYjczlbName(yjczlbdictEntity2.getValue());
                     }
                     criteria.addSingleResult(tempEntity);
                }else {
                }*/
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }
    
    
    @ApiOperation(value = "重点人员列表，通过id查询记录",notes = "重点人员列表，通过id查询记录")
    @RequestMapping(value = "/getdetail/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getDetailById(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfZdryTempEntity entity = service.getDetailById(id);
                //人员属性
                SysDictEntity rysxdictEntity = iSysDictService.getEntityByKey("zdry_ryxx",String.valueOf(entity.getRysx()));
                if(null != rysxdictEntity){
                    entity.setRysxName(rysxdictEntity.getValue());
                }
                //预警处置级别
                SysDictEntity yjczlbdictEntity = iSysDictService.getEntityByKey("zdry_yjczlb",String.valueOf(entity.getYjczlb()));
                if(null!= yjczlbdictEntity){
                  entity.setYjczlbName(yjczlbdictEntity.getValue());
                }
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }
    
    @ApiOperation(value = "通过id删除记录",notes = "通过id删除记录")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> delete(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                Integer num = service.delete(id);
                criteria.addSingleResult(num>0 ? "删除成功": "删除失败");
            }
        }.sendRequest());
    }
    
    @ApiOperation(value = "重点人员审核",notes = "重点人员审核")
    @RequestMapping(value = "/review", method = RequestMethod.POST)
    public Mono<ResponseInfo> reviewZdry(ServerWebExchange exchange ,@RequestBody LfZdryTempEntity entity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean flag = service.reviewProcess(exchange,entity);
                criteria.addSingleResult(flag ? "审核成功": "审核失败");
            }
        }.sendRequest());
    }

    @ApiOperation(value = "地址转坐标",notes = "地址转坐标")
    @RequestMapping(value = "/getCoordinate", method = RequestMethod.POST)
    public Mono<ResponseInfo> getCoordinate(ServerWebExchange exchange ,@RequestBody AddressQO entity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
            	Map<String, String> map = service.getCoordinate(entity);
                criteria.addSingleResult(map);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "通过身份证号查询记录",notes = "通过身份证号查询记录")
    @RequestMapping(value = "/zdrysfzh/{sfzh}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getByZdrySfzh(@PathVariable("sfzh") String sfzh) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<LfZdryEntity> entities  =
                        service.list(new QueryWrapper<LfZdryEntity>().lambda().eq(LfZdryEntity::getSfzh,sfzh));
                for(LfZdryEntity entity : entities){
                    //人员属性
                    SysDictEntity rysxdictEntity = iSysDictService.getEntityByKey("zdry_ryxx",String.valueOf(entity.getRysx()));
                    if(null != rysxdictEntity){
                        entity.setRysxName(rysxdictEntity.getValue());
                    }
                    //预警处置级别
                    SysDictEntity yjczlbdictEntity = iSysDictService.getEntityByKey("zdry_yjczlb",String.valueOf(entity.getYjczlb()));
                    if(null!= yjczlbdictEntity){
                        entity.setYjczlbName(yjczlbdictEntity.getValue());
                    }
                }
                criteria.addSingleResult(entities);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "查询列表记录",notes = "查询列表记录")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Mono<ResponseInfo> selectAll() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<LfZdryEntity> entitys = new ArrayList<LfZdryEntity>();
                boolean hasCache = redisService.has(RedisPrefixKeys.R_zdry.getKey());
                if (hasCache){
                    Object entitysJson = redisService.getValue(RedisPrefixKeys.R_zdry.getKey());
                    entitys = JSON.parseArray(entitysJson.toString(), LfZdryEntity.class);
                } else {
                    entitys = service.list();
                    redisService.setValue(RedisPrefixKeys.R_zdry.getKey(), JSON.toJSONString(entitys), 86400000);
                }
                criteria.addSingleResult(entitys);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "查询列表记录（简单数据）",notes = "查询列表记录（简单数据）")
    @RequestMapping(value = "/easy", method = RequestMethod.GET)
    public Mono<ResponseInfo> selectSimpleAll() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<ZdryVo> entitys = new ArrayList<ZdryVo>();
//                boolean hasCache = redisService.has(RedisPrefixKeys.R_zdry_easy.getKey());
//                if (hasCache){
//                    Object entitysJson = redisService.getValue(RedisPrefixKeys.R_zdry_easy.getKey());
//                    entitys = JSON.parseArray(entitysJson.toString(), ZdryVo.class);
//                } else {
                    entitys = service.getSimpleAll();
//                    redisService.setValue(RedisPrefixKeys.R_zdry_easy.getKey(), JSON.toJSONString(entitys), 86400000);
//                }
                criteria.addSingleResult(entitys);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "查询列表记录（分页）",notes = "查询列表记录（分页）\n" +
            "\t * name 关键字\n" +
            "\t * rylbx 人员类别\n" +
            "\t * sfzk 是否在控\n" +
            "\t * zrbm 责任部门\n" +
            "\t * ssxq 所属辖区\n" +
            "\t * gkjbx 管控级别\n" +
            "\t * mjlxdh 民警联系电话(责任民警)\n" +
            "\t * sspcs 所属派出所\n" +
            "\t * ryly 所属派出所\n" +
            "")
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Mono<ResponseInfo> queryPage(ServerWebExchange exchange ,@RequestBody final LfZdryQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
            	AccountBo bo = requestUtils.getCurrentUser(exchange);
                IPage<LfZdryEntity> page = service.queryPage(qo);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }

    @ApiOperation(value = "重点人员列表查询",notes = "重点人员列表查询")
    @RequestMapping(value = "/pageList", method = RequestMethod.POST)
    public Mono<ResponseInfo> queryPageList(ServerWebExchange exchange ,@RequestBody final LfZdryQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZdryEntity> page = null ;
                page =service.queryPageList(qo, exchange);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }

    @ApiOperation(value = "待审核列表查询",notes = "待审核列表查询")
    @RequestMapping(value = "/pageStatyList", method = RequestMethod.POST)
    public Mono<ResponseInfo> pageStatyList(ServerWebExchange exchange ,@RequestBody final LfZdryQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZdryTempEntity> page = null ;
                page = service.queryPageStatyReviewedList(qo, exchange);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }
    
    @ApiOperation(value = "重点人员信息为Excel")
    @PostMapping("/page/export")
    public Mono<Void> downloadByWriteWith(
            @RequestBody final LfZdryQO qo,
            ServerHttpResponse response
    ) throws IOException {
        String fileName = "重点人员"+LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}",fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = service.queryForExport(qo);
        return response.writeWith(flux);
    }

    @ApiOperation(value = "查询统计记录",notes = "查询统计记录\n" +
            "\t * cid=1：重点人员类型分析 \n" +
            "\t * cid=2：是否在控 \n" +
            "\t * cid=3：区域占比\n" +
            "\t * cid=4：等级分析\n" +
            "\t * cid=5：报警方式统计\n" +
            "\t * cid=6：区域管控级别统计\n" +
            "")
    @RequestMapping(value = "/getcount", method = RequestMethod.GET)
    public Mono<ResponseInfo> getCountByRylbx(@RequestParam("cid") String cid,
                                              @RequestParam(value = "zrbm",required = false) String zrbm,
                                              @RequestParam(value = "rangeType",required = false) String rangeType,
                                              @RequestParam(value = "rangeVal",required = false) String rangeVal) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<Map<String, String>> entitys = service.getCount(cid,zrbm,rangeType,rangeVal);
                criteria.addSingleResult(entitys);
            }
        }.sendRequest());
    }


    @ApiOperation(value = "获取头像",notes = "获取头像")
    @RequestMapping(value = "/tx/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getByTx(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                service.getTx(id);
                criteria.addSingleResult("0");
            }
        }.sendRequest());
    }

    /**
     * 获取布控接收对象
     *
     * @return
     */
    @ApiOperation(value = "获取布控接收对象", notes = "获取布控接收对象")
    @RequestMapping(value = "/queryReceivingInfo", method = RequestMethod.POST)
    public Mono<ResponseInfo> queryReceivingInfo() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
               Map result =  service.queryReceivingInfo();
               criteria.addSingleResult(result);
            }
        }.sendRequest());
    }

    /**
     * 保存重点人员布控信息
     *
     * @return
     */
    @ApiOperation(value = "保存重点人员布控信息", notes = "保存重点人员布控信息")
    @RequestMapping(value = "/saveZdryControlPc", method = RequestMethod.POST)
    public Mono<ResponseInfo> saveZdryControlPc(@RequestBody LfZdryControlPcEntity lfZdryControlPcEntity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                Boolean result =  service.saveZdryControlPc(lfZdryControlPcEntity);
                criteria.addSingleResult(result);
            }
        }.sendRequest());
    }


    @ApiOperation(value = "通过身份证号和报警时间查询记录",notes = "通过身份证号和预警时间查询记录")
    @RequestMapping(value = "/getBjxx", method = RequestMethod.GET)
    public Mono<ResponseInfo> getBjxx(
            @ApiParam(required = true, name = "sfzh", value = "身份证号", example = "132801196302204411") @RequestParam(value = "sfzh",required = true) String sfzh,
            @ApiParam(required = true, name = "datetime", value = "报警时间", example = "2019-09-16 17:07:03") @RequestParam(value = "datetime",required = true) String datetime,
            @ApiParam(required = true, name = "eventType", value = "报警类型 1 重点人员报警 2 在逃人员报警", example = "1") @RequestParam(value = "eventType",required = true) String eventType
        ) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfRyyjsjEntity entity = service.getByBjxx(sfzh,datetime,eventType);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "获取所有人员类型以及对应的责任部门")
    @GetMapping(value = "/rylx/zrbm")
    public Mono<ResponseInfo> getRylxWithZrbm(){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getAllRylxWithZrbm());
            }
        }.sendRequest());
    }

    @PutMapping(value = "/locationInfo")
    @ApiOperation(value = "回填重点人员位置信息")
    public Mono<ResponseInfo> fullLocationInfo(@RequestParam(value = "sfzh")@ApiParam(value = "身份证号",name = "sfzh") String sfzh){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                service.fullLocationInfo(sfzh);
            }
        }.sendRequest());
    }


    @GetMapping(value = "/getMjControlCountByPcs")
    public ResponseInfo getMjControlCount(@RequestParam(value = "pcs")@ApiParam(value = "派出所名称",name = "pcs",required = true) String pcs,
                                          @RequestParam(value = "zrbm",required = false) @ApiParam(value = "责任部门",name = "zrbm",required = false)String zrbm,
                                          @RequestParam(value = "rangeType",required = false) String rangeType,
                                          @RequestParam(value = "rangeVal",required = false) String rangeVal){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getMjControlCountByPcs(pcs,zrbm,rangeType,rangeVal));
            }
        }.sendRequest();
    }

    @GetMapping(value = "/getSpByHjs")
    public ResponseInfo getSpByHjs(@RequestParam(value = "province",required = false)@ApiParam(value = "省份名称",name = "province") String province,
                                   @RequestParam(value = "zrbm",required = false) String zrbm,
                                   @RequestParam(value = "rangeType",required = false) String rangeType,
                                   @RequestParam(value = "rangeVal",required = false) String rangeVal,
                                   @RequestParam(value = "areaLevel",required = false)@ApiParam(value = "区域级别(hjs:统计各省,hjss:统计各市,默认hjs)") String areaLevel){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getSpByHjs(province,zrbm,rangeType,rangeVal,areaLevel));
            }
        }.sendRequest();
    }

    @ApiOperation(value = "上报数据省厅", notes = "上报数据省厅")
    @PostMapping(value = "/pushdata")
    public ResponseInfo pushDataToProvincialOffice(@RequestBody List<LfZdryEntity> lfZdryEntityList){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(thirdService.upData(lfZdryEntityList));
            }
        }.sendRequest();
    }

    /*@ApiOperation(value = "根据派出所名称查询重点人员", notes = "根据派出所名称查询重点人员")
    @GetMapping(value = "/queryZdryByZrpcs")
    public ResponseInfo queryZdryByZrpcs(@RequestParam(required = false) String keyWords,
                                         @RequestParam String zrpcs,
                                         @RequestParam Integer offset,
                                         @RequestParam Integer size){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZdryEntity> page = service.queryZdryByZrpcsPage(keyWords,zrpcs,offset,size);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest();
    }*/

    /*@ApiOperation(value = "根据派出所导出重点人员")
    @GetMapping("/excel/exportZdryBySspcs")
    public Mono<Void> exportZdryBySspcs(@RequestParam String zrpcs,
                                        @RequestParam(value = "exportName",required = false) String exportName,
                                        ServerHttpResponse response) throws Exception{
        String fileName = StringUtils.isEmpty(exportName) ? zrpcs+"重点人员统计报表(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : exportName;
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}",fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = service.export(zrpcs, exportName);
        return response.writeWith(flux);
    }*/

    /*@ApiOperation(value = "根据主键ID修改稳控状态")
    @GetMapping("/updateWkztById")
    public ResponseInfo updateWkztById(@RequestParam @ApiParam(value = "主键ID",name = "id") String id,
                                        @RequestParam @ApiParam(value = "稳控状态",name = "wkzt") String wkzt){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.updateWkztById(id,wkzt));
            }
        }.sendRequest();
    }*/

   /* @ApiOperation(value = "根据户籍地查询全国各省份的重点人员数量")
    @PostMapping("/getEveryProvinceZdryNum")
    public ResponseInfo getEveryProvinceZdryNum(){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getEveryProvinceZdryNum());
            }
        }.sendRequest();
    }*/

    /*@ApiOperation(value = "根据省份code查询各市的重点人员数量")
    @PostMapping("/getCityZdryNumByProvinceCode")
    public ResponseInfo getCityZdryNumByProvinceCode(@RequestParam String code){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getCityZdryNumByProvinceCode(code));
            }
        }.sendRequest();
    }*/

   /* @ApiOperation(value = "根据市code查询各县/区的重点人员数量")
    @GetMapping("/getCountryZdryNumByCityCode")
    public ResponseInfo getCountryZdryNumByCityCode(@RequestParam String code){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getCountryZdryNumByCityCode(code));
            }
        }.sendRequest();
    }
    */
    /*@ApiOperation(value = "导出重点人员信息，根据户籍地 和 现住地撒点信息")
    @PostMapping("/exportZdryByHjdAndXzz")
   *//* @ApiImplicitParams({
            @ApiImplicitParam(value = "身份证号集合",name = "sfzhs",dataType = "String",paramType = "query",allowMultiple = true),
            @ApiImplicitParam(value = "execl名称",name = "exportName",dataType ="String",paramType = "query")
    })*//*
    public Mono<Void> exportZdryByHjdAndXzz(//@RequestParam List<String> sfzhs,
                                            //@RequestParam(required = false) String exportName,
                                            @RequestBody Map<String,Object> paramMap,
                                            ServerHttpResponse response) throws Exception{
        String fileName = com.alibaba.druid.util.StringUtils.isEmpty(paramMap.get("exportName").toString()) ? "重点人员信息(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : paramMap.get("exportName").toString();
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}",fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = service.exportZdryByHjdAndXzz((List<String>) paramMap.get("sfzhs"), paramMap.get("exportName").toString());
        return response.writeWith(flux);
    }*/

    /*@ApiOperation("查询省级迁徙图，从云南省外迁入的")
    @PostMapping("/queryProvinceTx")
    public Mono<ResponseInfo> queryProvinceTx(ServerWebExchange exchange,@RequestBody final LfZdryQO qo){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.queryProvinceTx(exchange,qo));
            }
        }.sendRequest());
    }*/

   /* @ApiOperation("查询市级迁徙图，从云南省内，昆明市外迁入的")
    @PostMapping("/queryCityTx")
    public Mono<ResponseInfo> queryCityTx(ServerWebExchange exchange,@RequestBody final LfZdryQO qo){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.queryCityTx(exchange,qo));
            }
        }.sendRequest());
    }*/

    /*@ApiOperation("根据 省市 迁徙图的数据查询列表数据")
    @PostMapping("/queryProvinceAndCityTxList")
    public Mono<ResponseInfo> queryProvinceAndCityTxList(ServerWebExchange exchange,@RequestBody final LfZdryQO qo){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZdryEntity> page = service.queryProvinceAndCityTxList(exchange,qo);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }*/

   /* @ApiOperation("根据省市迁徙图导出重点人员信息")
    @GetMapping("/exportZdryProvinceAndCityQxData")
    public Mono<Void> exportZdryProvinceAndCityQxData(@ModelAttribute LfZdryQO qo,
                                                      @RequestParam(value = "exportName",required = false)String exportName,
                                                      ServerHttpResponse response, ServerWebExchange exchange) throws Exception{
        String fileName = com.alibaba.druid.util.StringUtils.isEmpty(exportName) ? "重点人员迁徙信息(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : exportName;
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}",fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = service.exportZdryProvinceAndCityQxData(qo, exportName,exchange);
        return response.writeWith(flux);
    }*/

   /* @ApiOperation("查询 户籍地 和 现住地经纬度")
    @PostMapping("/queryZdryList")
    public Mono<ResponseInfo> queryZdryList(ServerWebExchange exchange ,@RequestBody final LfZdryQO qo){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.queryZdryList(exchange,qo));
            }
        }.sendRequest());
    }*/

   /* @ApiOperation("根据户籍地 和 现住地的撒点信息查询重点人员信息")
    @PostMapping("/queryZdry")
    *//*@ApiImplicitParams({
            @ApiImplicitParam(value = "身份证号集合",name = "sfzhs",dataType = "String",paramType = "query",allowMultiple = true),
            @ApiImplicitParam(value = "分页参数",name = "offset",dataType = "int",paramType = "query"),
            @ApiImplicitParam(value = "分页参数",name = "pageSize",dataType = "int",paramType = "query")
    })*//*
    public Mono<ResponseInfo> queryZdry(//@RequestParam List<String> sfzhs,
                                        //@RequestParam Integer offset,
                                        //@RequestParam Integer pageSize
                                        @RequestBody Map<String,Object> paramMap){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZdryEntity> page = service.queryZdry(paramMap);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }*/


}
