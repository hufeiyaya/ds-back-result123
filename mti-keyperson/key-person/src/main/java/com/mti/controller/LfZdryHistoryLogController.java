package com.mti.controller;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.constant.PageConstants;
import com.mti.dao.vo.ZdryHistoryLogVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.*;

import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.LfZdryHistoryLogEntity;
import com.mti.dao.qo.HistoryLogQO;
import com.mti.service.LfZdryHistoryLogService;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chenf
 * @since 2020-03-24
 */
@Slf4j
@RestController
@RequestMapping("/lfZdryHistoryLog")
public class LfZdryHistoryLogController {
	@Autowired
	private LfZdryHistoryLogService lfZdryHistoryLogService;
	
	 @ApiOperation(value = "通过类型查询历史记录",notes = "通过类型查询历史记录")
	    @RequestMapping(value = "/getByType", method = RequestMethod.POST)
	    public Mono<ResponseInfo> getByType(@RequestBody HistoryLogQO logqo) {

	        return Mono.just(new ResponseCallBack() {
	            @Override
	            public void execute(ResponseCriteria criteria, Object... obj) {
	            	List<LfZdryHistoryLogEntity> list = lfZdryHistoryLogService.getByType(  logqo) ;
	                criteria.addSingleResult(list);
	            }
	        }.sendRequest());
	    }

	    @ApiOperation("获取部门下的日志记录")
		@PostMapping("/getZdryHistiryLog")
		public Mono<ResponseInfo> getZdryHistiryLog(@RequestBody HistoryLogQO logqo, ServerWebExchange exchange){
			return Mono.just(new ResponseCallBack() {
				@Override
				public void execute(ResponseCriteria criteria, Object... obj) {
					IPage<ZdryHistoryLogVo> page = lfZdryHistoryLogService.getZdryHistiryLog(logqo,exchange);
					criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
					criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
				}
			}.sendRequest());
		}

	    @ApiOperation("导出重点人员日志记录")
	    @PostMapping("/exportZdryHistoryLog")
		public Mono<Void> exportZdryHistoryLog(@RequestBody HistoryLogQO logqo, ServerWebExchange exchange,
											   ServerHttpResponse response) throws Exception{
			String fileName =  "重点人员日志记录(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")";
			String fn = new String(fileName.getBytes(), "iso8859-1");
			log.info("fileName==>{}",fileName);
			response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
			response.getHeaders().add("Accept-Ranges", "bytes");
			Mono<DataBuffer> flux = lfZdryHistoryLogService.exportZdryHistoryLog(logqo, exchange);
			return response.writeWith(flux);
		}

}

