package com.mti.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.constant.PageConstants;
import com.mti.dao.dto.ZdryDto;
import com.mti.dao.model.KmZdryZrbmEntity;
import com.mti.dao.model.LfZdryBaseEntity;
import com.mti.dao.model.LfZdryBaseTempEntity;
import com.mti.dao.qo.AddressQO;
import com.mti.dao.qo.LfZdryQO;
import com.mti.dao.qo.PoliceQO;
import com.mti.exception.BusinessException;
import com.mti.service.IKmZdryFollowService;
import com.mti.service.KmZdryZrbmService;
import com.mti.service.LfZdryBaseService;
import com.mti.service.LfZdryBaseTempService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 重点人员核心业务 前端控制器
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Slf4j
@RestController
@RequestMapping("/lfZdryBase")
@Api(value = "重点人员数据接口v2.0", description = "重点人员数据接口v2.0")
public class LfZdryBaseController {
    @Autowired
    private LfZdryBaseService service;

    @Autowired
    private LfZdryBaseTempService tempService;

    @Autowired
    private KmZdryZrbmService kmzdryZrbmService;

    @Autowired
    private IKmZdryFollowService followService;

    @ApiOperation(value = "新增重点人员数据", notes = "新增重点人员数据")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Mono<ResponseInfo> save(ServerWebExchange exchange, @RequestBody LfZdryBaseEntity entity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean flag = service.saveZdry(exchange, entity);
                if (!flag) {
                    throw new BusinessException(10020, "添加异常");
                }
                criteria.addSingleResult("操作成功");
            }
        }.sendRequest());
    }

    @ApiOperation(value = "重点人员修改", notes = "重点人员修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Mono<ResponseInfo> update(ServerWebExchange exchange, @RequestBody LfZdryBaseEntity entity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                if (0 == entity.getResubmitFlag() || entity.getResubmitFlag() == null) {
                    LfZdryBaseTempEntity temp = tempService.getById(entity.getId());
                    if (temp != null) {
                        throw new BusinessException(10020, "该人员的其它流程中还未结束，请结束后再操作！");
                    }
                }
                boolean flag = service.updateZdry(exchange, entity);
                if (!flag) {
                    throw new BusinessException(10020, "操作失败");
                }
                criteria.addSingleResult("操作成功");
            }
        }.sendRequest());
    }

    @ApiOperation(value = "重点人员在流程中的数量", notes = "重点人员在流程中的数量")
    @RequestMapping(value = "/getInProcessCount/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getInProcessCount(@PathVariable String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                int count = tempService.count(new QueryWrapper<LfZdryBaseTempEntity>().lambda().eq(LfZdryBaseTempEntity::getId, id));
                //+service.count(new QueryWrapper<LfZdryBaseEntity>().lambda().eq(LfZdryBaseEntity::getId,id));
                criteria.addSingleResult(count);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "重点人员移交", notes = "重点人员移交")
    @RequestMapping(value = "/transfer", method = RequestMethod.POST)
    public Mono<ResponseInfo> transfer(ServerWebExchange exchange, @RequestBody LfZdryBaseEntity entity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                if (0 == entity.getResubmitFlag() || entity.getResubmitFlag() == null) {//1==entity.getResubmitFlag()
                    //1重新提交标记
                    LfZdryBaseTempEntity temp = tempService.getById(entity.getId());
                    if (temp != null) {
                        throw new BusinessException(10020, "该人员的其它流程中还未结束，请结束后再操作！");
                    }
                }
                boolean flag = service.transfer(exchange, entity);
                if (!flag) {
                    throw new BusinessException(10020, "操作失败");
                }

                criteria.addSingleResult("操作成功");
            }
        }.sendRequest());
    }


    @ApiOperation(value = "重点人员列表查询", notes = "重点人员列表查询")
    @RequestMapping(value = "/pageList", method = RequestMethod.POST)
    public Mono<ResponseInfo> queryPageList(ServerWebExchange exchange, @RequestBody final LfZdryQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZdryBaseEntity> page = null;
                page = service.queryPageList(qo, exchange);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }

    @ApiOperation(value = "每日跟踪查询稳控状态的数量", notes = "每日跟踪查询稳控状态的数量")
    @RequestMapping(value = "/queryWkztCount", method = RequestMethod.POST)
    public Mono<ResponseInfo> queryWkztCount(ServerWebExchange exchange, @RequestBody LfZdryQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.queryWkztCount(exchange, qo));
            }
        }.sendRequest());
    }


    @ApiOperation(value = "重点人员列表，通过sfzh查询记录", notes = "重点人员列表，通过id查询记录")
    @RequestMapping(value = "/getZdry/{sfzh}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getBySfzh(@PathVariable("sfzh") String sfzh, @RequestParam(value = "currentUserId", required = false) String currentUserId) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfZdryBaseEntity entity = service.getBySfzh(sfzh, currentUserId);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "通过重点人员id获取信息", notes = "重点人员列表，通过id查询记录")
    @RequestMapping(value = "/getZdryById/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getZdryById(@PathVariable String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getOne(new QueryWrapper<LfZdryBaseEntity>().lambda().eq(LfZdryBaseEntity::getId, id)));
            }
        }.sendRequest());
    }

    @ApiOperation(value = "通过接口刷新tx字段", notes = "通过接口刷新tx字段")
    @RequestMapping(value = "/getTxUpdate", method = RequestMethod.GET)
    public Mono<ResponseInfo> getTxUpdate() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                String path = "/app/web/zdry/tx/";
                File file = new File(path);        //获取其file对象
                File[] fs = file.listFiles();    //遍历path下的文件和目录，放在File数组中
                int i = 0;
                for (File f : fs) {
                    if (!f.isDirectory() && f.getName().contains(".jpg")) {    //若非目录(即文件)，则打印
                        String sfzh = f.getName().split(".jpg")[0];
                        // System.out.println(newFileName1.length());
                        if (sfzh.length() == 18) {
                            System.out.println(sfzh);
                            service.updateTxBySfzh(sfzh);
                            i++;
                        }
                    }
                }
                criteria.addSingleResult(i);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "通过权限查询未接收重点人员总数信息", notes = "通过权限查询未接收重点人员总数信息")
    @RequestMapping(value = "/getNotReceviedCount", method = RequestMethod.POST)
    public Mono<ResponseInfo> getNotReceviedCount(ServerWebExchange exchange) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getNotReceviedCount(exchange));
            }
        }.sendRequest());
    }

    @ApiOperation(value = "通过权限查询待处理重点人员总数信息", notes = "通过权限查询待处理重点人员总数信息")
    @RequestMapping(value = "/getStayHandleCount", method = RequestMethod.POST)
    public Mono<ResponseInfo> getStayHandleCount(ServerWebExchange exchange) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getStayHandleCount(exchange));
            }
        }.sendRequest());
    }

    @ApiOperation(value = "根据名字获取所有匹配信息", notes = "根据名字获取所有匹配信息")
    @RequestMapping(value = "/getInfoByName", method = RequestMethod.GET)
    public Mono<ResponseInfo> getInfoByName(@RequestParam(required = true) String name) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.list(new QueryWrapper<LfZdryBaseEntity>().lambda().like(LfZdryBaseEntity::getXm, name)));
            }
        }.sendRequest());
    }
    @ApiOperation(value = "待处理重点人员详情", notes = "通过身份证号待处理重点人员详情")
    @RequestMapping(value = "/getdetail/{sfzh}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getDetailBySfzh(@PathVariable("sfzh") String sfzh) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfZdryBaseTempEntity entity = service.getDetailBySfzh(sfzh);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }


    @ApiOperation(value = "通过id删除记录", notes = "通过id删除记录")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> delete(@PathVariable("id") String id, ServerWebExchange exchange) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfZdryBaseTempEntity temp = tempService.getById(id);
                if (temp != null) {
                    throw new BusinessException(10020, "重点人员删除失败！该人员的其它流程中还未结束，请结束后再操作!");
                }
                Integer num = service.deleteZdry(id, exchange);
                if (num == 0) {
                    throw new BusinessException(10020, "删除异常");
                }

                criteria.addSingleResult("删除成功");
            }
        }.sendRequest());
    }


    @ApiOperation(value = "通过id删除被驳回重点人员记录", notes = "通过id删除被驳回重点人员记录")
    @RequestMapping(value = "/deleteRejectZdry/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> deleteRejectZdry(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                Boolean flag = tempService.removeById(id);
                if (!flag) {
                    throw new BusinessException(10020, "删除异常");
                }
                criteria.addSingleResult("删除成功");
            }
        }.sendRequest());
    }

    @ApiOperation(value = "根据派出所名称查询重点人员", notes = "根据派出所名称查询重点人员")
    @GetMapping(value = "/queryZdryByZrpcs")
    public ResponseInfo queryZdryByZrpcs(@RequestParam(required = false) String keyWords,
                                         @RequestParam String ssxqId,
                                         @RequestParam String sspcsId,
                                         @RequestParam Integer offset,
                                         @RequestParam Integer size) {
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<ZdryDto> page = service.queryZdryByZrpcsPage(keyWords, sspcsId, ssxqId, offset, size);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest();
    }

    @ApiOperation(value = "根据派出所导出重点人员")
    @GetMapping("/excel/exportZdryBySspcs")
    public Mono<Void> exportZdryBySspcs(@RequestParam String sspcsId,
                                        @RequestParam String ssxqId,
                                        @RequestParam(value = "exportName", required = false) String exportName,
                                        ServerHttpResponse response) throws Exception {
        String fileName = StringUtils.isEmpty(exportName) ? "重点人员统计报表(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : exportName;
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}", fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = service.export(ssxqId, sspcsId, exportName);
        return response.writeWith(flux);
    }


    @ApiOperation(value = "根据主键ID修改稳控状态")
    @GetMapping("/updateWkztById")
    public ResponseInfo updateWkztById(@RequestParam @ApiParam(value = "主键ID", name = "id") String id,
                                       @RequestParam @ApiParam(value = "稳控状态", name = "wkzt") String wkzt) {
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.updateWkztById(id, wkzt));
            }
        }.sendRequest();
    }

    @ApiOperation(value = "根据户籍地查询全国各省份的重点人员数量")
    @PostMapping("/getEveryProvinceZdryNum")
    public ResponseInfo getEveryProvinceZdryNum() {
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getEveryProvinceZdryNum());
            }
        }.sendRequest();
    }

    @ApiOperation(value = "根据省份code查询各市的重点人员数量")
    @PostMapping("/getCityZdryNumByProvinceCode")
    public ResponseInfo getCityZdryNumByProvinceCode(@RequestParam String code) {
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getCityZdryNumByProvinceCode(code));
            }
        }.sendRequest();
    }

    @ApiOperation(value = "根据市code查询各县/区的重点人员数量")
    @GetMapping("/getCountryZdryNumByCityCode")
    public ResponseInfo getCountryZdryNumByCityCode(@RequestParam String code) {
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getCountryZdryNumByCityCode(code));
            }
        }.sendRequest();
    }


    @ApiOperation(value = "导出重点人员信息，根据户籍地 和 现住地撒点信息")
    @PostMapping("/exportZdryByHjdAndXzz")
    public Mono<Void> exportZdryByHjdAndXzz(@RequestBody Map<String, Object> paramMap,
                                            ServerHttpResponse response) throws Exception {
        String fileName = com.alibaba.druid.util.StringUtils.isEmpty(paramMap.get("exportName").toString()) ? "重点人员信息(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : paramMap.get("exportName").toString();
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}", fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = service.exportZdryByHjdAndXzz((List<String>) paramMap.get("sfzhs"), paramMap.get("exportName").toString());
        return response.writeWith(flux);
    }

    @ApiOperation("查询省级迁徙图，从云南省外迁入的")
    @PostMapping("/queryProvinceTx")
    public Mono<ResponseInfo> queryProvinceTx(ServerWebExchange exchange, @RequestBody final LfZdryQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.queryProvinceTx(exchange, qo));
            }
        }.sendRequest());
    }

    @ApiOperation("查询市级迁徙图，从云南省内，昆明市外迁入的")
    @PostMapping("/queryCityTx")
    public Mono<ResponseInfo> queryCityTx(ServerWebExchange exchange, @RequestBody final LfZdryQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.queryCityTx(exchange, qo));
            }
        }.sendRequest());
    }

    @ApiOperation("根据 省市 迁徙图的数据查询列表数据")
    @PostMapping("/queryProvinceAndCityTxList")
    public Mono<ResponseInfo> queryProvinceAndCityTxList(ServerWebExchange exchange, @RequestBody final LfZdryQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<ZdryDto> page = service.queryProvinceAndCityTxList(exchange, qo);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }

    @ApiOperation("根据省市迁徙图导出重点人员信息")
    @GetMapping("/exportZdryProvinceAndCityQxData")
    public Mono<Void> exportZdryProvinceAndCityQxData(@ModelAttribute LfZdryQO qo,
                                                      @RequestParam(value = "exportName", required = false) String exportName,
                                                      ServerHttpResponse response, ServerWebExchange exchange) throws Exception {
        String fileName = com.alibaba.druid.util.StringUtils.isEmpty(exportName) ? "重点人员迁徙信息(" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : exportName;
        String fn = new String(fileName.getBytes(), "iso8859-1");
        log.info("fileName==>{}", fileName);
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fn + ".xlsx");
        response.getHeaders().add("Accept-Ranges", "bytes");
        Mono<DataBuffer> flux = service.exportZdryProvinceAndCityQxData(qo, exportName, exchange);
        return response.writeWith(flux);
    }

    @ApiOperation("查询 户籍地 和 现住地经纬度")
    @PostMapping("/queryZdryList")
    public Mono<ResponseInfo> queryZdryList(ServerWebExchange exchange, @RequestBody final LfZdryQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.queryZdryList(exchange, qo));
            }
        }.sendRequest());
    }

    @ApiOperation("根据户籍地 和 现住地的撒点信息查询重点人员信息")
    @PostMapping("/queryZdry")
    public Mono<ResponseInfo> queryZdry(@RequestBody Map<String, Object> paramMap) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<ZdryDto> page = service.queryZdry(paramMap);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }

    @ApiOperation(value = "地址转坐标", notes = "地址转坐标")
    @RequestMapping(value = "/getCoordinate", method = RequestMethod.POST)
    public Mono<ResponseInfo> getCoordinate(ServerWebExchange exchange, @RequestBody AddressQO entity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                Map<String, String> map = service.getCoordinate(entity);
                criteria.addSingleResult(map);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "根据大类小类获取警种", notes = "根据大类小类获取警种")
    @RequestMapping(value = "/getPoliceType", method = RequestMethod.POST)
    public Mono<ResponseInfo> getPoliceType(ServerWebExchange exchange, @RequestBody PoliceQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                KmZdryZrbmEntity entity = kmzdryZrbmService.getPoliceType(exchange, qo);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "待审核列表查询", notes = "待审核列表查询")
    @RequestMapping(value = "/pageStatyList", method = RequestMethod.POST)
    public Mono<ResponseInfo> pageStatyList(ServerWebExchange exchange, @RequestBody final LfZdryQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZdryBaseTempEntity> page = null;
                page = service.queryPageStatyReviewedList(qo, exchange);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }

    @ApiOperation(value = "重点人员审核", notes = "重点人员审核")
    @RequestMapping(value = "/review", method = RequestMethod.POST)
    public Mono<ResponseInfo> reviewZdry(ServerWebExchange exchange, @RequestBody LfZdryBaseTempEntity entity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean flag = service.reviewProcess(exchange, entity);
                if (!flag) {
                    throw new BusinessException(10020, "审核失败");
                }

                criteria.addSingleResult("审核成功");
            }
        }.sendRequest());
    }

    @ApiOperation(value = "概览统计查询", notes = "概览统计查询\n" +
            "\t * cid=1：重点人员在控比例 \n" +
            "\t * cid=2：重点人员区域占比 \n" +
            "\t * cid=3：重点人员等级分析\n" +
            "\t * cid=4：重点人员类型（大类）分析\n" +
            "")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "类型区别", name = "cid", dataType = "String", paramType = "query"),
            @ApiImplicitParam(value = "code: 1 情报中心；2 分局；3 派出所; 4 警种。", name = "code", dataType = "String", paramType = "query"),
            @ApiImplicitParam(value = "用户对应的组织id", name = "regionId", dataType = "String", paramType = "query")
    })
    @RequestMapping(value = "/getcount", method = RequestMethod.GET)
    public Mono<ResponseInfo> getCountByRylbx(@RequestParam("cid") String cid,
                                              @RequestParam String code,
                                              @RequestParam(required = false) String regionId) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<Map<String, String>> entitys = service.getCount(cid, code, regionId);
                criteria.addSingleResult(entitys);
            }
        }.sendRequest());
    }

    @GetMapping("/getZdryCountBySfzh")
    @ApiImplicitParam(value = "身份证号", name = "sfzh", dataType = "String", paramType = "query")
    public Mono<ResponseInfo> getZdryCountBySfzh(String sfzh) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.getZdryCountBySfzh(sfzh));
            }
        }.sendRequest());
    }

    @GetMapping("/sendKeyPersonMessage")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "重点人员id", name = "personId", dataType = "String", paramType = "query"),
            @ApiImplicitParam(value = "更新类型:0.新增 1.修改 2.删除", name = "gxlx", dataType = "String", paramType = "query")
    })
    public Mono<ResponseInfo> sendKeyPersonMessage(String personId, String gxlx) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.sendKeyPersonMessage(personId, gxlx));
            }
        }.sendRequest());
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "分页参数，页面", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "分页参数，每页大小", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "keyWords", value = "搜索关键字", dataType = "String", paramType = "query")
    })
    @GetMapping("/queryZdryListClear")
    public Mono<ResponseInfo> queryZdryListClear(@RequestParam Integer start,
                                                 @RequestParam Integer size,
                                                 @RequestParam(required = false) String keyWords) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfZdryBaseEntity> page = service.queryZdryListClear(start, size, keyWords);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }

    @ApiOperation("根据当前登录人获取所属分局")
    @GetMapping("/querySsxq")
    public Mono<ResponseInfo> querySsxq(ServerWebExchange exchange) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.querySsxq(exchange));
            }
        }.sendRequest());
    }

    @ApiOperation("每日追踪 根据当前登录用户查询单位")
    @GetMapping("/findUnitByCurrentUser")
    public Mono<ResponseInfo> findUnitByCurrentUser(ServerWebExchange exchange) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.findUnitByCurrentUser(exchange));
            }
        }.sendRequest());
    }

    @ApiOperation("查询重点人员聚集的列表")
    @PostMapping("/findZdryGatherList")
    public Mono<ResponseInfo> findZdryGatherList(@RequestBody List<String> sfzhs) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(service.findZdryGatherList(sfzhs));
            }
        }.sendRequest());
    }

    @PutMapping(value = "/follow")
    @ApiOperation(value = "关注/取消关注重点人员")
    public Mono<ResponseInfo> attention(@RequestParam(value = "userId") @ApiParam(value = "当前用户ID", name = "userId", required = true) String userId,
                                        @RequestParam(value = "personId") @ApiParam(value = "重点人员ID", name = "personId", required = true) String personId,
                                        @RequestParam(value = "operate") @ApiParam(value = "操作类型(1：关注，0：取消关注)", required = true, name = "operate") String operate,
                                        @RequestParam(value = "creator") @ApiParam(value = "操作人", required = true, name = "creator") String creator,
                                        ServerWebExchange exchange) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                if ("1".equalsIgnoreCase(operate)) {
                    followService.follow(personId, userId, creator, exchange);
                } else if ("0".equalsIgnoreCase(operate)) {
                    followService.unFollow(personId, userId);
                } else
                    log.warn("===>未知操作");
            }
        }.sendRequest());
    }


}

