package com.mti.controller;

import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.TSwitchTypeEntity;
import com.mti.service.TSwitchTypeServie;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * @Author hf
 * @Title:
 * @Description: TODO (开关设置类)
 * @Param
 * @return
 * @Date 2020/11/24 10:43
 */
@Api(value = "开关状态控制类")
@RestController
@RequestMapping("tSwitchType")
@Slf4j
public class TSwitchTypeController {
    @Autowired
    private TSwitchTypeServie tSwitchTypeServie;

    @PostMapping("/upSertSwitchStatus")
    @ApiOperation("设置预警处置开关")
    public Mono<ResponseInfo> upSertSwitchStatus(@RequestBody TSwitchTypeEntity tSwitchTypeEntity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean result = tSwitchTypeServie.upSertSwitchStatus(tSwitchTypeEntity);
                criteria.addSingleResult(result);
            }
        }.sendRequest());
    }

    @PostMapping("/getSwitchStatus")
    @ApiOperation("获取预警处置开关")
    public Mono<ResponseInfo> getSwitchStatus(@RequestBody TSwitchTypeEntity tSwitchTypeEntity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean flag = tSwitchTypeServie.getSwitchStatus(tSwitchTypeEntity.getType(), tSwitchTypeEntity.getName());
                criteria.addSingleResult(flag);
            }
        }.sendRequest());
    }

}
