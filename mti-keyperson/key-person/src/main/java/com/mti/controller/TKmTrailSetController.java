package com.mti.controller;



import io.swagger.annotations.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.constant.PageConstants;
import com.mti.dao.model.KmTrailEntity;
import com.mti.dao.model.TKmTrailSetEntity;
import com.mti.dao.qo.TKmTrailQO;
import com.mti.dao.qo.TKmTrailSetQO;
import com.mti.dao.vo.KmTrailVo;
import com.mti.exception.BusinessException;
import com.mti.service.TKmTrailSetService;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * <p>
 * 模型配置 前端控制器
 * </p>
 *
 * @author qiangxy
 * @since 2020-09-02
 */
@Slf4j
@RestController
@RequestMapping("/TKmTrailSet")
@Api(value = "模型配置接口v2.0",description = "模型配置接口v2.0")
public class TKmTrailSetController {
	 
	 @Autowired
	 private TKmTrailSetService  service;
	 

	 @ApiOperation(value = "模型配置查询",notes = "模型配置查询")
	    @RequestMapping(value = "/pageList", method = RequestMethod.POST)
	    public Mono<ResponseInfo> queryPageList( @RequestBody final TKmTrailSetQO qo) {
	        return Mono.just(new ResponseCallBack() {
	            @Override
	            public void execute(ResponseCriteria criteria, Object... obj) {
	                IPage<TKmTrailSetEntity> page = null ;
	                page =service.queryPageList(qo);
	                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
	                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
	            }
	        }.sendRequest());
	    }
	 @ApiOperation(value = "新增模型配置",notes = "新增模型配置")
	 @RequestMapping(value = "/save", method = RequestMethod.POST)
	 public Mono<ResponseInfo> save( @RequestBody TKmTrailSetEntity entity,ServerWebExchange exchange) {
	        return Mono.just(new ResponseCallBack() {
	            @Override
	            public void execute(ResponseCriteria criteria, Object... obj) {
	            	boolean flag = service.saveTKmTrailSet(entity,exchange);
	            	if(!flag) {
	                    throw new BusinessException(10020,"添加异常");
	                }
	                criteria.addSingleResult("操作成功");
	            }
	        }.sendRequest());
	    }
	    @ApiOperation(value = "通过id删除",notes = "通过id删除")
	    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	    public Mono<ResponseInfo> delete(@PathVariable("id") String id,ServerWebExchange exchange) {
	        return Mono.just(new ResponseCallBack() {
	            @Override
	            public void execute(ResponseCriteria criteria, Object... obj) {
	            	Integer num = service.deleteTKmTrailSet(id,exchange);
	                if(num==0) {
	                    throw new BusinessException(10020,"删除异常");
	                }
	                criteria.addSingleResult("删除成功");
	            }
	        }.sendRequest());
	    }
	    @ApiOperation(value = "通过id暂停",notes = "通过id暂停")
	    @RequestMapping(value = "/stop/{id}", method = RequestMethod.GET)
	    public Mono<ResponseInfo> stop(@PathVariable("id") String id) {
	        return Mono.just(new ResponseCallBack() {
	            @Override
	            public void execute(ResponseCriteria criteria, Object... obj) {
	            	Integer num = service.stop(id);
	                if(num==0) {
	                    throw new BusinessException(10020,"更新异常");
	                }
	                criteria.addSingleResult("暂停成功");
	            }
	        }.sendRequest());
	    }
	    @ApiOperation(value = "通过id启动",notes = "通过id启动")
	    @RequestMapping(value = "/start/{id}", method = RequestMethod.GET)
	    public Mono<ResponseInfo> start(@PathVariable("id") String id) {
	        return Mono.just(new ResponseCallBack() {
	            @Override
	            public void execute(ResponseCriteria criteria, Object... obj) {
	            	Integer num = service.start(id);
	                if(num==0) {
	                    throw new BusinessException(10020,"更新异常");
	                }
	                criteria.addSingleResult("启动成功");
	            }
	        }.sendRequest());
	    }
	    
	    @ApiOperation(value = "线索列表查询",notes = "线索列表查询")
	    @RequestMapping(value = "/getPageList", method = RequestMethod.POST)
	    public Mono<ResponseInfo> getPageList( @RequestBody final TKmTrailQO qo) {
	        return Mono.just(new ResponseCallBack() {
	            @Override
	            public void execute(ResponseCriteria criteria, Object... obj) {
	                IPage<KmTrailEntity> page = null ;
	                page =service.getPageList(qo);
	                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
	                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
	            }
	        }.sendRequest());
	    }
	    
	    @ApiOperation(value = "线索模型列表",notes = "线索模型列表")
	    @RequestMapping(value = "/getAllList", method = RequestMethod.GET)
	    public Mono<ResponseInfo> getAllList(@RequestParam(required = false) String type) {
	        return Mono.just(new ResponseCallBack() {
	            @Override
	            public void execute(ResponseCriteria criteria, Object... obj) {
	                List<KmTrailVo> list = service.getAllList(type);
	                criteria.addSingleResult(list);
	            }
	        }.sendRequest());
	    }
	    
	     @ApiOperation(value = "标记为已读状态",notes = "标记为已读状态")
	     @RequestMapping(value = "/flagIsReadState", method = RequestMethod.GET)
		 public Mono<ResponseInfo> flagState( @RequestParam(required = true) String id) {
		        return Mono.just(new ResponseCallBack() {
		            @Override
		            public void execute(ResponseCriteria criteria, Object... obj) {
		            	service.updateStatusById(id);
		                criteria.addSingleResult("操作成功");
		            }
		        }.sendRequest());
		    }
	    
	 
}

