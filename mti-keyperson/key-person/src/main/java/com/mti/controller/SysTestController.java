package com.mti.controller;


import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.dto.ResultDto;
import com.mti.dao.dto.ZdryYjHandlePushDto;
import com.mti.dao.model.SysTestEntity;
import com.mti.jwt.AccountBo;
import com.mti.jwt.RequestUtils;
import com.mti.service.ISysTestService;
import com.mti.utils.DateUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Api(value="测试接口",description="测试接口描述")
@RestController
@RequestMapping("/test")
@Slf4j
public class SysTestController {

    @Autowired
    private ISysTestService sysTestService;

    @Autowired
    private RequestUtils requestUtils;

    @ApiOperation(value="通过id查询记录",notes="通过id查询记录")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getById(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                SysTestEntity sysTestEntity = sysTestService.getById(id);
                criteria.addSingleResult(sysTestEntity);
            }
        }.sendRequest());
    }

    @ApiOperation(value="查询列表记录",notes="查询列表记录")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Mono<ResponseInfo> selectAll() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<SysTestEntity> sysTestEntity = sysTestService.list();
                criteria.addSingleResult(sysTestEntity);
            }
        }.sendRequest());
    }

    @ApiOperation(value="新增测试",notes="新增测试")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Mono<ResponseInfo> saveOrUpdate(@RequestBody SysTestEntity entity) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                boolean flag  = sysTestService.saveOrUpdate(entity);
                criteria.addSingleResult(flag?"添加或更新成功":"更新或者添加失败");
            }
        }.sendRequest());
    }

    @ApiOperation(value="创建会议",notes="创建会议")
    @RequestMapping(value = "/createMeet", method = RequestMethod.GET, produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Mono<ResponseInfo> meet(
            @RequestParam(required = false) String strKey,
            @RequestParam(required = false) String strCaller,
            @RequestParam(required = false) String strCalled,
            @RequestParam(required = false) Integer nCallType
    ) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                log.info("会议ID:{}",strKey);
                log.info("时间：{}", DateUtils.createDate());
//                iSipConferenceService.createMeeting(strKey, strCaller, strCalled, nCallType);
                criteria.addSingleResult("ok");
            }
        }.sendRequest());
    }

    @ApiOperation(value = "获取当前用户信息",notes = "获取当前用户信息")
    @RequestMapping(value = "/getUser", method = RequestMethod.GET)
    public Mono<ResponseInfo> getByuser(ServerWebExchange serverWebExchange) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                AccountBo accountBo = requestUtils.getCurrentUser(serverWebExchange);
                if (accountBo != null) {
                    System.out.println(accountBo.getLoginName());
                }
                criteria.addSingleResult(accountBo);
            }
        }.sendRequest());
    }

    @ApiOperation("测试返回接口返回")
    @PostMapping("/testYjHandleFack")
   public ResultDto testYjHandleFack(ZdryYjHandlePushDto zdryYjHandlePushDto){
       ResultDto resultDto = new ResultDto();
       resultDto.setId(zdryYjHandlePushDto.getNotificationID());
       return resultDto;
   }

}
