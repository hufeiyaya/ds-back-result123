package com.mti.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.ArmingTaskRelationEntity;
import com.mti.dao.model.KmProcessEntity;
import com.mti.dao.model.LfZdryApproveLogEntity;
import com.mti.mapper.KmProcessMapper;
import com.mti.service.ILfZdryApproveLogService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

@Api
@RestController
@AllArgsConstructor
@RequestMapping(value = "/specialPersonApproveLog")
public class LfZdryApproveLogController {
    private final ILfZdryApproveLogService logService;
    
    @Resource
    private KmProcessMapper mapper;
    
    @GetMapping(value = "/{zdryId}")
    @ApiOperation(value = "根据Id获取人员数据审批记录")
    public Mono<ResponseInfo> getInfoByZdryId(@PathVariable(value = "zdryId")@ApiParam(value = "重点人员ID",name = "zdryId")String zdryId){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(logService.list(new LambdaQueryWrapper<LfZdryApproveLogEntity>().eq(LfZdryApproveLogEntity::getSpId,zdryId).eq(LfZdryApproveLogEntity::getApprovingFlag,"0").orderByAsc(LfZdryApproveLogEntity::getCreateTime)));
            }
        }.sendRequest());
    }

    @GetMapping(value = "/one/{zdryId}")
    @ApiOperation(value = "根据Id获取最新的人员数据审批记录")
    public Mono<ResponseInfo> getOneInfoByZdryId(@PathVariable(value = "zdryId")@ApiParam(value = "重点人员ID",name = "zdryId")String zdryId){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(logService.list(new LambdaQueryWrapper<LfZdryApproveLogEntity>().eq(LfZdryApproveLogEntity::getSpId,zdryId).orderByDesc(LfZdryApproveLogEntity::getCreateTime).last("limit 1")));
            }
        }.sendRequest());
    }
    
    @GetMapping
    @ApiOperation(value = "获取日志列表")
    public Mono<ResponseInfo> getLogsList(@RequestParam(value = "startTime",required = false)@ApiParam(value = "开始时间",name = "startTime") String startTime,
                                          @RequestParam(value = "endTime",required = false)@ApiParam(value = "结束时间",name = "endTime") String endTime,
                                          @RequestParam(value = "operator",required = false)@ApiParam(value = "操作人",name = "operator") String operator,
                                          @RequestParam(value = "content",required = false)@ApiParam(value = "内容",name = "content") String content,
                                          @RequestParam(value = "page",required = false)@ApiParam(value = "页码",name = "page") Integer page,
                                          @RequestParam(value = "size",required = false)@ApiParam(value = "分页大小",name = "size") Integer size){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LambdaQueryWrapper<LfZdryApproveLogEntity> queryWrapper = new LambdaQueryWrapper<>();
                if(!StringUtils.isEmpty(operator))
                    queryWrapper.like(LfZdryApproveLogEntity::getCreator,operator);
                if(!StringUtils.isEmpty(content))
                    queryWrapper.like(LfZdryApproveLogEntity::getApproveResult,content);
                if(!StringUtils.isEmpty(startTime)&&!StringUtils.isEmpty(endTime))
                    queryWrapper.ge(LfZdryApproveLogEntity::getCreateTime, LocalDateTime.parse(startTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))).le(LfZdryApproveLogEntity::getCreateTime, LocalDateTime.parse(endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
                if(page!=null && size!=null){
                    IPage<LfZdryApproveLogEntity> ipage = logService.page(new Page<>(page,size),queryWrapper);
                    criteria.addSingleResult(ipage);
                }else{
                    criteria.addSingleResult(logService.list(queryWrapper));
                }
            }
        }.sendRequest());
    }
    
    @GetMapping(value = "queryProcessNode/{processCode}")
    @ApiOperation(value = "根据流程编码获流程节点")
    public Mono<ResponseInfo> getProcessInfoByCode(@PathVariable(value = "processCode")@ApiParam(value = "流程编码",name = "processCode")Integer processCode){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
            	criteria.addSingleResult(mapper.selectOne(new LambdaQueryWrapper<KmProcessEntity>().eq(KmProcessEntity::getProcessCode,processCode)));
            }  
        }.sendRequest());
    }
    
}
