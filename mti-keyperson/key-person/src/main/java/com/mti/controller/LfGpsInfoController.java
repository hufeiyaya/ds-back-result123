package com.mti.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.constant.PageConstants;
import com.mti.dao.model.LfGpsInfoEntity;
import com.mti.dao.qo.LfGpsInfoQO;
import com.mti.service.ILfGpsInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Api(value="警力数据接口",description="警力数据接口")
@RestController
@RequestMapping("/lf/gpsinfo")
@Slf4j
public class LfGpsInfoController {

    @Autowired
    private ILfGpsInfoService service;

    @ApiOperation(value="通过id查询记录",notes="通过id查询记录")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getById(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfGpsInfoEntity entity = service.getById(id);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

    @ApiOperation(value="查询列表记录",notes="查询列表记录")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Mono<ResponseInfo> list() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<LfGpsInfoEntity> entitys = service.list();
                criteria.addSingleResult(entitys);
            }
        }.sendRequest());
    }

    @ApiOperation(value="查询列表记录（分页）",notes="查询列表记录（分页）" )
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Mono<ResponseInfo> queryPage(@RequestBody final LfGpsInfoQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfGpsInfoEntity> page = service.queryPage(qo);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }

}
