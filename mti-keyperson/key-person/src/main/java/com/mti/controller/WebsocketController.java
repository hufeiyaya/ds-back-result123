//package com.mti.controller;
//
//
//import com.mti.component.ResponseCallBack;
//import com.mti.component.ResponseCriteria;
//import com.mti.component.ResponseInfo;
//import com.mti.service.IWebsocketService;
//import com.mti.vo.Message;
//import com.mti.vo.MessageText;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import reactor.core.publisher.Mono;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * WebsocketController class
// *
// * @author zhaoyj
// * @date 2019/3/12
// */
//@RestController
//@RequestMapping(value = "/websocket")
//public class WebsocketController {
//
//    @Autowired
//    private IWebsocketService websocketService;
//
//    @RequestMapping(value = "/sendMessage/users", method = RequestMethod.GET)
//    public Mono<ResponseInfo> sendMessage(@RequestParam String content, @RequestParam String target) {
//        return Mono.just(new ResponseCallBack() {
//            @Override
//            public void execute(ResponseCriteria criteria, Object... obj) {
//                MessageText message = new MessageText();
//                message.setContent(content);
//                List<String> targets = new ArrayList<>();
//                targets.add(target);
//                message.setTarget(targets);
//                websocketService.sendMessageText(message);
//            }
//        }.sendRequest());
//    }
//
//}
