package com.mti.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.constant.PageConstants;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.SysDictEntity;
import com.mti.dao.model.TKmProEntity;
import com.mti.dao.qo.TkmProRiQo;
import com.mti.service.TKmProRightService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @Classname TKmProRightController
 * @Description TODO
 * @Date 2020/2/20 16:57
 * @Created by duchaof
 * @Version 1.0
 */
@Api(value="信访/聚访维权记录",description = "信访/聚访维权记录")
@RestController
@RequestMapping("/pro/right")
@Slf4j
public class TKmProRightController {
    @Autowired
    private TKmProRightService tKmProRightService;

    @ApiOperation("新增信访/聚访记录")
    @PostMapping("/add")
    public Mono<ResponseInfo> addProRightRecord(ServerWebExchange exchange,@RequestBody TKmProEntity tKmProEntity){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(tKmProRightService.add(exchange,tKmProEntity));
            }
        }.sendRequest());
    }
    @ApiOperation("删除信访/聚访记录")
    @GetMapping("/delete")
    @ApiImplicitParam(value = "记录Id",name = "id",dataType = "int",paramType = "query")
    public Mono<ResponseInfo> deleteProRightRecord(@RequestParam Integer id){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(tKmProRightService.removeById(id));
            }
        }.sendRequest());
    }

    @ApiOperation("修改信访/聚访记录")
    @PostMapping("/update")
    public Mono<ResponseInfo> updateProRightRecord(@RequestBody TKmProEntity tKmProEntity){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(tKmProRightService.updateById(tKmProEntity));
            }
        }.sendRequest());
    }

    @ApiOperation("查询信访/聚访记录，不分页")
    @PostMapping("/list")
    public Mono<ResponseInfo> queryProRightRecordList(@RequestBody TkmProRiQo tkmProRiQo){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(tKmProRightService.queryProRightRecordList(tkmProRiQo));
            }
        }.sendRequest());
    }

    @ApiOperation("查询信访/聚访记录，分页")
    @PostMapping("/page")
    public Mono<ResponseInfo> queryProRightRecordPage(@RequestBody TkmProRiQo tkmProRiQo){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<TKmProEntity> page = tKmProRightService.queryProRightRecordPage(tkmProRiQo);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }


}
