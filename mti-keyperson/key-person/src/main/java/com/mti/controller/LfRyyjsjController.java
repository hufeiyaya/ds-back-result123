/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.constant.PageConstants;
import com.mti.dao.model.LfRyyjsjEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.jwt.RequestUtils;
import com.mti.service.ILfRyyjsjService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Api(value = "重点人员预警数据接口",description = "重点人员预警数据接口")
@RestController
@RequestMapping("/lf/ryyjsj")
@Slf4j
public class LfRyyjsjController {

    @Autowired
    private ILfRyyjsjService service;

    @Autowired
    private RequestUtils requestUtils;

    @ApiOperation(value = "查询列表记录",notes = "查询列表记录")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Mono<ResponseInfo> list() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<LfRyyjsjEntity> entitys = service.list();
                criteria.addSingleResult(entitys);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "通过预警标识查询列表记录",notes = "通过预警标识查询列表记录")
    @RequestMapping(value = "/yjbs", method = RequestMethod.GET)
    public Mono<ResponseInfo> yjbs(
            @RequestParam(value = "yjbs")@ApiParam(value = "预警标识",name = "yjbs",required = true) String yjbs,
            @RequestParam(value = "orderBy")@ApiParam(value = "排序方式",name = "orderBy", example = "排序方式: 降序 desc 升序 asc") String orderBy
    ) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<LfRyyjsjEntity> entitys = service.queryListByYjbs(yjbs, orderBy);
                criteria.addSingleResult(entitys);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "通过预警标识查询列表记录（分页）",notes = "通过预警标识查询列表记录（分页）")
    @RequestMapping(value = "/yjbsByPage", method = RequestMethod.POST)
    public Mono<ResponseInfo> yjbsByPage(@RequestBody final LfRyyjsjQO qo) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfRyyjsjEntity> page = service.queryPageByYjbs(qo);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }

    @ApiOperation(value = "查询列表记录（分页）",notes = "查询列表记录（分页）")
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Mono<ResponseInfo> queryPage(@RequestBody final LfRyyjsjQO qo,@RequestParam(required = false) String tokenValue) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<LfRyyjsjEntity> page = service.queryPage(qo);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }

    @ApiOperation(value = "更新位置",notes = "更新位置")
    @RequestMapping(value = "/tx/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getTx(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                service.getTx(id);
                criteria.addSingleResult("0");
            }
        }.sendRequest());
    }

}
