package com.mti.controller;

import com.alibaba.fastjson.JSONArray;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.LfZdryZrbmEntity;
import com.mti.utils.JSONHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

/**
 * @Classname SystemVersionController
 * @Description TODO
 * @Date 2020/3/24 16:38
 * @Created by duchaof
 * @Version 1.0
 */
@Api(value = "系统版本",description = "系统版本")
@RestController
@RequestMapping("/system/version")
@Slf4j
public class SystemVersionController {

    @ApiOperation("查询系统版本")
    @GetMapping("/querySystemVersion")
    public Mono<ResponseInfo> querySystemVersion(){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                String jsonArray =   JSONHelper.ResolveJsonFileToString("system_version.json");
                System.out.println(jsonArray);
                List<Map> versions = JSONArray.parseArray(jsonArray,Map.class);
                criteria.addSingleResult(versions);
            }
        }.sendRequest());
    }
}
