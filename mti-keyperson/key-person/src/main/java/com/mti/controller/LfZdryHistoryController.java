/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.controller;

import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.service.ILfZdryHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Api(value = "重点人员历史数据接口",description = "重点人员历史数据接口")
@RestController
@RequestMapping("/lf/zdryHistory")
@Slf4j
public class LfZdryHistoryController {

    @Autowired
    private ILfZdryHistoryService service;

    @ApiOperation(value = "同步数据",notes = "同步数据")
    @RequestMapping(value = "/sync", method = RequestMethod.GET)
    public Mono<ResponseInfo> sync(
            @RequestParam(value = "date")@ApiParam(value = "日期",name = "date", example = "2019-09-09", required = true) String date
    ) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                service.SyncHistoryByVersion(date);
                criteria.addSingleResult("0");
            }
        }.sendRequest());
    }

}
