package com.mti.controller;


import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.LfGpsHistoryEntity;
import com.mti.service.ILfGpsHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Api(value="警力定位数据接口",description="警力定位数据接口")
@RestController
@RequestMapping("/lf/gpshistory")
@Slf4j
public class LfGpsHistoryController {

    @Autowired
    private ILfGpsHistoryService service;

    @ApiOperation(value="通过id查询记录",notes="通过id查询记录")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getById(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfGpsHistoryEntity entity = service.getById(id);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

    @ApiOperation(value="查询列表记录",notes="查询列表记录")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Mono<ResponseInfo> list() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                List<LfGpsHistoryEntity> entitys = service.list();
                criteria.addSingleResult(entitys);
            }
        }.sendRequest());
    }

//    @ApiOperation(value = "根据ID获取指定实体,字段对应的返回值", notes = "根据ID获取指定实体,字段对应的返回值")
//    @RequestMapping(value = "/get", method = RequestMethod.GET)
//    public  Mono<ResponseInfo> get(@RequestParam(required = false) String ids,
//                             @RequestParam(required = false) String tableName,
//                             @RequestParam(required = false) String fields) {
//        List<Map<String, Object>> maps = commonService.get(ids, tableName, fields);
//        return renderSuccess(maps);
//    }

}
