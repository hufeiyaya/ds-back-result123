package com.mti.controller;

import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.LfZdryZrbmEntity;
import com.mti.service.ILfZdryService;
import com.mti.service.ISpecialPersonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * @Classname ConvertAddressController
 * @Description TODO
 * @Date 2020/3/9 10:01
 * @Created by duchaof
 * @Version 1.0
 */
@RestController
@RequestMapping("/convert")
@Api(value = "地址装换测试类",description = "地址装换测试类")
public class ConvertAddressController {
   /* @Autowired
    private ISpecialPersonService iSpecialPersonService;*/

   /* @GetMapping("/convertAddress")
    @ApiOperation("地址装换")
    @ApiImplicitParam(value = "转化的总数",name = "total",dataType = "int",paramType = "query")
    public Mono<ResponseInfo> convertAddress(@RequestParam Integer total) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
               // iSpecialPersonService.convertAddress(total);
            }
        }.sendRequest());
    }*/
}
