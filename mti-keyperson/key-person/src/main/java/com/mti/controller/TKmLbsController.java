package com.mti.controller;


import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.qo.LfZdryQO;
import com.mti.service.TKmLbsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;

/**
 * <p>
 * 微信/qq定位 前端控制器
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
@RestController
@RequestMapping("/lbs")
@Api(value = "LBS定位",description = "LBS定位")
public class TKmLbsController {
    @Autowired
    private TKmLbsService tKmLbsService;

    @ApiOperation(value = "查询重点人员微信或者qq的定位信息")
    @PostMapping("/queryZdryLbsPosition")
    public ResponseInfo queryZdryLbsPosition(ServerWebExchange exchange , @RequestBody LfZdryQO qo){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(tKmLbsService.queryZdryLbsPosition(exchange,qo));
            }
        }.sendRequest();
    }
}

