package com.mti.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.constant.PageConstants;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.SysDictEntity;
import com.mti.dao.model.TQbptZlEntity;
import com.mti.dao.qo.TQbptQO;
import com.mti.service.ILfRyyjsjService;
import com.mti.service.ILfZdryService;
import com.mti.service.ISysDictService;
import com.mti.service.ITQbptZlService;
import com.mti.utils.DateUtils;
import com.mti.vo.QbptZlVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Api(value = "预警指令数据接口",description = "预警指令数据接口")
@RestController
@RequestMapping("/lf/zl")
@Slf4j
public class TQbptZlController {

    @Autowired
    private ITQbptZlService service;

    @Autowired
    private ILfRyyjsjService iLfRyyjsjService;

    @Autowired
    private ILfZdryService iLfZdryService;

    @Autowired
    private ISysDictService iSysDictService;


    @ApiOperation(value = "通过序号查询记录",notes = "通过序号查询记录")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getById(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                TQbptZlEntity entity = service.getById(id);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "通过序号查询警情记录",notes = "通过序号查询警情记录")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getInfoById(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                TQbptZlEntity entity = service.getById(id);
                QbptZlVo qbptZlVo = new QbptZlVo();
                String data = entity.getData();
                if (StringUtils.isNotBlank(data)) {
                    JSONObject jSONObject = JSON.parseObject(data);
                    String info = jSONObject.getString("info");
                    if (StringUtils.isNotBlank(info)) {
                        JSONObject jSONObjectInfo = JSON.parseObject(info);
                        qbptZlVo.setId(id);
                        qbptZlVo.setDataid(jSONObject.getString("id"));
                        qbptZlVo.setZlid(jSONObjectInfo.getString("id"));
                        qbptZlVo.setZlbh(jSONObjectInfo.getString("zlbh"));
                        qbptZlVo.setXfrq(DateUtils.stampToDate(jSONObjectInfo.getString("xfrq")));
                        qbptZlVo.setBt(jSONObjectInfo.getString("bt"));
                        qbptZlVo.setZlnr(jSONObjectInfo.getString("zlnr"));
                        qbptZlVo.setGzcs(jSONObjectInfo.getString("gzcs"));
                        qbptZlVo.setQssx(DateUtils.stampToDate(jSONObjectInfo.getString("qssx")));
                        qbptZlVo.setFksx(DateUtils.stampToDate(jSONObjectInfo.getString("fksx")));
                        qbptZlVo.setJjcd("");
                        qbptZlVo.setXsly("");
                        qbptZlVo.setSjqt("");

                        String jjcd = jSONObjectInfo.getString("jjcd");
                        SysDictEntity sysDictEntityjjcd = iSysDictService.getEntityByKey("zl_jjcd", jjcd);
                        if (sysDictEntityjjcd != null){
                            qbptZlVo.setJjcd(sysDictEntityjjcd.getValue());
                        }

                        String xslybm = jSONObjectInfo.getString("xslybm");
                        SysDictEntity sysDictEntityxslybm = iSysDictService.getEntityByKey("zl_xslybm", xslybm);
                        if (sysDictEntityxslybm != null){
                            qbptZlVo.setXsly(sysDictEntityxslybm.getValue());
                        }

                        String sjqtdl = jSONObjectInfo.getString("sjqtdl");
                        String sjqtxl = jSONObjectInfo.getString("sjqtxl");
                        String sjqtdlStr = "";
                        String sjqtxlStr = "";
                        SysDictEntity sysDictEntitysjqtdl = iSysDictService.getEntityByKey("zl_sjqt", sjqtdl);
                        if (sysDictEntitysjqtdl != null){
                            sjqtdlStr = sysDictEntitysjqtdl.getValue();
                        }
                        SysDictEntity sysDictEntitysjqtxl = iSysDictService.getEntityByKey("zl_sjqt", sjqtxl);
                        if (sysDictEntitysjqtxl != null){
                            sjqtxlStr = sysDictEntitysjqtxl.getValue();
                        }
                        qbptZlVo.setSjqt(sjqtdlStr + "-" + sjqtxlStr);
                    }
                }
                criteria.addSingleResult(qbptZlVo);
            }
        }.sendRequest());
    }

    @ApiOperation(value = "通过序号查询人员记录",notes = "通过序号查询人员记录")
    @RequestMapping(value = "/ry/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getRyById(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                TQbptZlEntity entity = service.getById(id);
                List<LfZdryEntity> entitys = new ArrayList<LfZdryEntity>();
                String data = entity.getData();
                if (StringUtils.isNotBlank(data)){
                    JSONObject jSONObject = JSON.parseObject(data);
                    String personList = jSONObject.getString("personList");
                    if (StringUtils.isNotBlank(personList)){
                        JSONArray jSONArray = JSON.parseArray(personList);
                        if (jSONArray.size() > 0){
                            for (int i = 0; i < jSONArray.size();i++){
                                JSONObject job = jSONArray.getJSONObject(i);
                                String sfz = job.getString("sfz");
                                if (StringUtils.isNotBlank(sfz)) {
                                    LfZdryEntity lfZdryEntity = iLfZdryService.getBySfzhV2(sfz);
                                    if (lfZdryEntity != null){
                                        entitys.add(lfZdryEntity);
                                    }
                                }
                            }
                        }
                    }
                }
                criteria.addSingleResult(entitys);
            }
        }.sendRequest());
    }


    @ApiOperation(value = "查询列表记录（分页）",notes = "查询列表记录（分页）\n"
            + "\t * name 关键字\n" +
            "")
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Mono<ResponseInfo> queryPage(@RequestBody final TQbptQO qo, @RequestParam(required = false) String tokenValue) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                IPage<TQbptZlEntity> page = service.queryPage(qo);
                criteria.addMapResult(PageConstants.PAGE_RESULT_NAME, page.getRecords());
                criteria.addMapResult(PageConstants.PAGE_RESULT_COUNT, page.getTotal());
            }
        }.sendRequest());
    }


    @ApiOperation(value = "处理数据（内部接口）",notes = "处理数据（内部接口）")
    @RequestMapping(value = "/doData", method = RequestMethod.POST)
    public Mono<ResponseInfo> doData() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                service.doData();
            }
        }.sendRequest());
    }

}
