package com.mti.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.kmmodel.KmArmingTaskEntity;
import com.mti.service.IKmArmingTaskService;
import com.mti.service.impl.KmArmingTaskServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;
import java.time.LocalDateTime;

/**
 *
 * @author zhangmingxin
 * @date 2020/09/01
 */
@Api(value = "昆明布控任务模块",description = "昆明布控任务模块")
@RestController
@AllArgsConstructor
@RequestMapping(value = "/kmdb/armingTask")
public class KmArmingTaskController {
    private final IKmArmingTaskService armingTaskService;
    @PostMapping
    @ApiOperation(value = "保存布控任务",notes = "保存布控任务")
    public Mono<ResponseInfo> save(@RequestBody KmArmingTaskEntity taskEntity,ServerWebExchange exchange){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                armingTaskService.saveOrUpdateKmArmingTask(taskEntity,exchange);
            }
        }.sendRequest());
    }

    @GetMapping(value = "/list")
    @ApiOperation(value = "布控任务列表",notes = "布控任务列表")
    public Mono<ResponseInfo> list(@RequestParam(value = "page")@ApiParam(value = "页码",name = "page")Integer page,
                                   @RequestParam(value = "size")@ApiParam(value = "页大小",name = "size")Integer size,
                                   @RequestParam(value = "listType")@ApiParam(value = "列表类型",name = "listType")Integer listType,
                                   @RequestParam(value = "armingOrgId",required = false)@ApiParam(value = "布控单位ID",name = "armingOrgId")String armingOrgId,
                                   @RequestParam(value = "armingStatus",required = false)@ApiParam(value = "布控状态",name = "armingStatus")Integer armingStatus,
                                   @RequestParam(value = "searchTerm",required = false)@ApiParam(value = "搜索条件",name = "searchTerm")String searchTerm){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LambdaQueryWrapper<KmArmingTaskEntity> queryWrapper = new LambdaQueryWrapper<>();
                if(!StringUtils.isEmpty(armingOrgId)){
                    queryWrapper.eq(KmArmingTaskEntity::getArmingOrgId,armingOrgId);
                }
                if(armingStatus!=null){
                    queryWrapper.eq(KmArmingTaskEntity::getArmingStatus,armingStatus);
                }
                queryWrapper.ne(KmArmingTaskEntity::getArmingStatus,-1);
                if(!StringUtils.isEmpty(searchTerm)){
                    queryWrapper.and(wrapper->wrapper.like(KmArmingTaskEntity::getTaskName,searchTerm));
                }
                if(listType!=null && listType==1){
                    queryWrapper.orderByDesc(KmArmingTaskEntity::getLastUpdateTime);
                }else{
                    queryWrapper.orderByAsc(KmArmingTaskEntity::getStartDate).orderByAsc(KmArmingTaskEntity::getArmingStatus);
                }
                if(page!=null && size!=null){
                    IPage<KmArmingTaskEntity> ipage = armingTaskService.page(new Page<>(page,size),queryWrapper);
                    criteria.addSingleResult(ipage);
                }else{
                    criteria.addSingleResult(armingTaskService.list(queryWrapper));
                }
            }
        }.sendRequest());
    }

    @GetMapping
    @ApiOperation(value = "获取布控任务详情",notes = "获取布控任务详情")
    public Mono<ResponseInfo> getDetail(@RequestParam(value = "id")@ApiParam(value = "布控任务ID",name = "id") String id){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(armingTaskService.getKmArmingTaskByid(id));
            }
        }.sendRequest());
    }

    @PutMapping(value = "/status")
    @ApiOperation(value = "更新布控任务状态(布控状态(0:基本信息状态，1：启动，2：暂停，3：已结束，-1：已删除))")
    public Mono<ResponseInfo> updateStatus(@RequestParam(value = "taskId")@ApiParam(value = "布控任务ID",name = "taskId")String taskId,
                                           @RequestParam(value = "status")@ApiParam(value = "要更新的状态(布控状态(0:基本信息状态，1：启动，2：暂停，3：已结束，-1：已删除))")Integer status,
                                           ServerWebExchange exchange
    		){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
            	armingTaskService.updateStatus(taskId,status,exchange);
            }
        }.sendRequest());
    }
    
    @GetMapping(value = "/getDefault")
    @ApiOperation(value = "设置默认值",notes = "设置默认值")
    public Mono<ResponseInfo> getDefault(@RequestParam(value = "id")@ApiParam(value = "布控任务ID",name = "id") String id){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(armingTaskService.getDefault(id));
            }
        }.sendRequest());
    }
    
    @GetMapping(value = "/cancelDefault")
    @ApiOperation(value = "取消默认值",notes = "取消默认值")
    public Mono<ResponseInfo> cancelDefault(@RequestParam(value = "id")@ApiParam(value = "布控任务ID",name = "id") String id){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(armingTaskService.cancelDefault(id));
            }
        }.sendRequest());
    }
    
}
