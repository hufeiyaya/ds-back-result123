package com.mti.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.constant.PageConstants;
import com.mti.dao.dto.ZdryDto;
import com.mti.service.KmPhYjService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * <p>
 * 手机预警 前端控制器
 * </p>
 *
 * @author duchao
 * @since 2020-04-29
 */
@Slf4j
@RestController
@RequestMapping("/kmPhYj")
@Api(value = "手机预警",description = "手机预警")
public class KmPhYjController {
    @Autowired
    private KmPhYjService kmPhYjService;

    @ApiOperation("查询每个人的最新定位")
    @GetMapping("/queryPhYjaLastLocation")
    public Mono<ResponseInfo> queryPhYjaLastLocation(){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(kmPhYjService.queryPhYjaLastLocation());
            }
        }.sendRequest());
    }

}

