package com.mti.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.LfZdryModifyLogEntity;
import com.mti.service.ILfZdryModifyLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Api
@RestController
@AllArgsConstructor
@RequestMapping(value = "/specialPersonModifyLog")
public class LfZdryOperateLogController {
    private final ILfZdryModifyLogService logService;
    @GetMapping(value = "/{zdryId}")
    @ApiOperation(value = "根据Id获取人员数据变更记录")
    public Mono<ResponseInfo> getInfoByZdryId(@PathVariable(value = "zdryId")@ApiParam(value = "重点人员ID",name = "zdryId")String zdryId){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(logService.list(new LambdaQueryWrapper<LfZdryModifyLogEntity>().eq(LfZdryModifyLogEntity::getZdryId,zdryId).orderByDesc(LfZdryModifyLogEntity::getCreateTime)));
            }
        }.sendRequest());
    }

    @GetMapping(value = "/one/{zdryId}")
    @ApiOperation(value = "根据Id获取最新的人员数据变更记录")
    public Mono<ResponseInfo> getOneInfoByZdryId(@PathVariable(value = "zdryId")@ApiParam(value = "重点人员ID",name = "zdryId")String zdryId){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(logService.list(new LambdaQueryWrapper<LfZdryModifyLogEntity>().eq(LfZdryModifyLogEntity::getZdryId,zdryId).orderByDesc(LfZdryModifyLogEntity::getCreateTime).last("limit 1")));
            }
        }.sendRequest());
    }
    
    @GetMapping
    @ApiOperation(value = "获取日志列表")
    public Mono<ResponseInfo> getLogsList(@RequestParam(value = "startTime",required = false)@ApiParam(value = "开始时间",name = "startTime") String startTime,
                                          @RequestParam(value = "endTime",required = false)@ApiParam(value = "结束时间",name = "endTime") String endTime,
                                          @RequestParam(value = "operator",required = false)@ApiParam(value = "操作人",name = "operator") String operator,
                                          @RequestParam(value = "content",required = false)@ApiParam(value = "内容",name = "content") String content,
                                          @RequestParam(value = "page",required = false)@ApiParam(value = "页码",name = "page") Integer page,
                                          @RequestParam(value = "size",required = false)@ApiParam(value = "分页大小",name = "size") Integer size){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LambdaQueryWrapper<LfZdryModifyLogEntity> queryWrapper = new LambdaQueryWrapper<>();
                if(!StringUtils.isEmpty(operator))
                    queryWrapper.like(LfZdryModifyLogEntity::getCreator,operator);
                if(!StringUtils.isEmpty(content))
                    queryWrapper.like(LfZdryModifyLogEntity::getLog,content);
                if(!StringUtils.isEmpty(startTime)&&!StringUtils.isEmpty(endTime))
                    queryWrapper.ge(LfZdryModifyLogEntity::getCreateTime, LocalDateTime.parse(startTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))).le(LfZdryModifyLogEntity::getCreateTime, LocalDateTime.parse(endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
                if(page!=null && size!=null){
                    IPage<LfZdryModifyLogEntity> ipage = logService.page(new Page<>(page,size),queryWrapper);
                    criteria.addSingleResult(ipage);
                }else{
                    criteria.addSingleResult(logService.list(queryWrapper));
                }
            }
        }.sendRequest());
    }
}
