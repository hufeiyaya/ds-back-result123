package com.mti.controller;

import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.service.IZtryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Api(description = "在逃人员接口",value = "在逃人员接口")
@RequestMapping(value = "/ztry")
public class LfZtryController {
    private final IZtryService ztryService;
    @GetMapping(value = "/{sfzh}")
    @ApiOperation(value = "获取在逃人员详情")
    public ResponseInfo getZtryDetail(@PathVariable(value = "sfzh")@ApiParam(value = "身份证号",name = "sfzh",required = true) String sfzh){
        return new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(ztryService.getBySfzh(sfzh));
            }
        }.sendRequest();
    }
}
