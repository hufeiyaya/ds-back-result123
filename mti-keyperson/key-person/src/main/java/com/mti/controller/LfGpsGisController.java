package com.mti.controller;


import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.dao.model.LfGpsGisEntity;
import com.mti.service.ILfGpsGisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Api(value="警力定位数据接口",description="警力定位数据接口")
@RestController
@RequestMapping("/lf/gpsgis")
@Slf4j
public class LfGpsGisController {

    @Autowired
    private ILfGpsGisService service;

    @ApiOperation(value="通过id查询记录",notes="通过id查询记录")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Mono<ResponseInfo> getById(@PathVariable("id") String id) {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                LfGpsGisEntity entity = service.getById(id);
                criteria.addSingleResult(entity);
            }
        }.sendRequest());
    }

    @ApiOperation(value="查询列表记录",notes="查询列表记录")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Mono<ResponseInfo> list() {
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                int flag = service.saveByHistory("");
                criteria.addSingleResult(flag);
            }
        }.sendRequest());
    }


}
