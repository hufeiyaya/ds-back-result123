package com.mti.controller;

import com.mti.component.ResponseCallBack;
import com.mti.component.ResponseCriteria;
import com.mti.component.ResponseInfo;
import com.mti.service.CacheableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * @Classname CacheableController
 * @Description TODO
 * @Date 2020/2/8 16:45
 * @Created by duchaof
 * @Version 1.0
 */
@Api(value="缓存测试类",description = "缓存测试类")
@RestController
@RequestMapping("/CacheableController")
@Slf4j
public class CacheableController {
    @Autowired
    private CacheableService cacheableService;

    @ApiOperation("测试查询责任派出所")
    @GetMapping("/getZrpcs")
    public Mono<ResponseInfo> getZrpcs(String orgType){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(cacheableService.getZrpcs("3"));
            }
        }.sendRequest());
    }

   /* @ApiOperation("查询所有的地域信息")
    @GetMapping("/getAllRegionInfo")
    public Mono<ResponseInfo> getAllRegionInfo(){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                criteria.addSingleResult(cacheableService.getAllRegionInfo());
            }
        }.sendRequest());
    }*/

    @ApiOperation("删除所有的缓存信息")
    @GetMapping("/deleteAllCacheable")
    public Mono<ResponseInfo> deleteAllCacheable(){
        return Mono.just(new ResponseCallBack() {
            @Override
            public void execute(ResponseCriteria criteria, Object... obj) {
                cacheableService.deleteAllCacheale();
            }
        }.sendRequest());
    }

}
