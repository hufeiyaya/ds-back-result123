package com.mti.component;

/**
 * ApiResponse class
 *
 * @author yangsp
 * @date 2019/3/22
 */
public class ApiResponse {
	private String code;
	private int type;
	private Object data;

	public ApiResponse(String code, int type, Object data) {
		this.code = code;
		this.type = type;
		this.data = data;
	}

	public ApiResponse() {
		super();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
