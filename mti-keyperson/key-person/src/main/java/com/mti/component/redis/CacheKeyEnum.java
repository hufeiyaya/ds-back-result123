package com.mti.component.redis;

public class CacheKeyEnum {

    public enum RedisPrefixKeys{
        /**
         * 测试
         */
        R_test("test"),

        /**
         * 电子围栏
         */
        R_code_device("code_device"),

        /**
         * 检查站
         */
        R_protective_circle("protective_circle"),

        /**
         * 视频监控
         */
        R_roadmonitor("roadmonitor"),

        /**
         * 重点人员
         */
        R_zdry("zdry"),

        R_zdry_easy("zdry_easy")
        ;

        private String key;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        /**
         * 构造函数
         */
        private RedisPrefixKeys(String key) {
            this.key = "R_"+key+"_";
        }
    }
}