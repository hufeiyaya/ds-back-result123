package com.mti.component;

/**
 * ApiCode class
 *
 * @author yangsp
 * @date 2019/3/22
 */
import java.util.ArrayList;
import java.util.List;

public enum ApiCode {
	/**
	 * 0,调用成功
	 */
	OK("0","ok"),

	/**
	 * "-1","信息不正确"
	 */
	ERROR("-1","信息不正确"),

	/**
	 * "-2","参数错误，请对照错误提示和文档检查请求参数"
	 */
	ERROR_Params("-2","参数错误，请对照错误提示和文档检查请求参数"),

	/**
	 * "-3","请求的数据不存在，请验证数据是否正确"
	 */
	ERROR_Exsit("-3","请求的数据不存在，请验证数据是否正确"),

	/**
	 * "-4","未知错误"
	 */
	ERROR_NONE("-4","未知错误"),

	/**
	 * "-5","操作的数据已发生变化，不能继续操作"
	 */
	ERROR_Change("-5","操作的数据已发生变化，不能继续操作"),
	/**
	 * "-6","网络异常"
	 */
	ERROR_NET_EXCEPTION("-6","网络异常"),
	/**
	 * "-7","呼叫信息不正确"
	 */
	ERROR_CALL("-7","呼叫信息不正确"),
	/**
	 * "-8","会议被锁定"
	 */
	ERROR_CONF_LOCK("-8","会议被锁定")
	;

	private String code;
	private String message;

	private ApiCode(String code,String message){
		this.code = code;
		this.message = message;
	}

	public static ApiCode getApiByCode(String code){
		for(ApiCode apiCode:ApiCode.values()){
			if(apiCode.code == code){
				return apiCode;
			}
		}
		return null;
	}

	public static List<ApiCode> getAllList(){
		List<ApiCode> list = new ArrayList<ApiCode>();
		for(ApiCode apiCode:ApiCode.values()){
			list.add(apiCode);
		}
		return list;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}