package com.mti.component;

/**
 * ResponseInfo class
 *
 * @author zhaoyj
 * @date 2019/3/12
 */
public class ResponseInfo {
	private int code;

	private String message;

	private Object data;

	public ResponseInfo(int code, String message, Object data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public ResponseInfo() {
		super();
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
