package com.mti.scheduled;


import com.mti.service.impl.KmArmingTaskServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author ： caoxx
 * @date ：2020/09/07 11:44
 * @description :布控定时任务
 * @path : com.mti.scheduled.KmArmingTaskDsrw
 * @modifiedBy ：
 */
@Slf4j
@Component
@EnableScheduling
@AllArgsConstructor
public class KmArmingTaskSchedule {
    private final KmArmingTaskServiceImpl taskService;

    @Scheduled(cron = "0 */1 * * * ?")
    public void changeTaskStatus() {
        log.info("===> start to execute scheduled task...");
        List<String> unStartStatusIds = taskService.selectIdByStartDateAndArmingStatus();
        if (unStartStatusIds != null && !unStartStatusIds.isEmpty()){
            taskService.getBaseMapper().updateArmingStatusBatchByIds(unStartStatusIds, 1);
            taskService.getBaseMapper().saveRecordDataBatch(unStartStatusIds, 1);
        }
        List<String> unEndStatusIds = taskService.selectIdByEndDateAndArmingStatus();
        if (unEndStatusIds != null && !unEndStatusIds.isEmpty()){
            taskService.getBaseMapper().updateArmingStatusBatchByIds(unEndStatusIds, 3);
            taskService.getBaseMapper().saveRecordDataBatch(unEndStatusIds, 3);
        }
        log.info("===>execude schecuted task end!!!");
    }
}
