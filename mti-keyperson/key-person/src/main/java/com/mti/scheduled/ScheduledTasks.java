/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.scheduled;

import com.mti.service.ILfZdryHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
@Configurable
@EnableScheduling
@Slf4j
public class ScheduledTasks{

    @Autowired
    private ILfZdryHistoryService iLfZdryHistoryService;

    // 同步重点人员到历史记录时间点(每天23:55进行数据同步)
    //@Scheduled(cron = "0 0 1 ? * *")
    public void reportCurrentByCron(){
        String version = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        iLfZdryHistoryService.SyncHistoryByVersion(version);
    }



}

