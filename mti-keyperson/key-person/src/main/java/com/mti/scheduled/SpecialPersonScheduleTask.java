//package com.mti.scheduled;
//
//import com.mti.component.snowflake.KeyWorker;
//import com.mti.configuration.SystemConfig;
//import com.mti.dao.model.LfZdryEntity;
//import com.mti.dao.model.LfZdryHistoryEntity;
//import com.mti.mapper.LfZdryHistoryMapper;
//import com.mti.mapper.LfZdryMapper;
//import com.mti.service.ILfGkjlService;
//import lombok.AllArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.scheduling.annotation.SchedulingConfigurer;
//import org.springframework.scheduling.config.ScheduledTaskRegistrar;
//import org.springframework.stereotype.Component;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.util.StringUtils;
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * @company 上海道枢信息科技-->
// * @anthor created by zhangmingxin
// * @date 2019/8/15
// * @change
// * @describe describe
// **/
//@Slf4j
//@Component
//@AllArgsConstructor
//public class SpecialPersonScheduleTask implements SchedulingConfigurer {
//    private final SystemConfig systemConfig;
//    private final LfZdryHistoryMapper historyMapper;
//    private final LfZdryMapper zdryMapper;
//
//    @Transactional
//    public void executeSpecialPersonHistory(String targetDate){
//        log.info("start to async special person to record table===>");
//        //查询当前版本重点人员数据
//        String today = StringUtils.isEmpty(targetDate)?LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")):targetDate;
//        List<LfZdryEntity> specialPersons = zdryMapper.listZdryByCreateTime(null,null,today,null,null);
//        if(specialPersons!=null && !specialPersons.isEmpty()){
//            //插入历史记录表
//            log.info("重点人员数===>{}",specialPersons.size());
//            String recordVersion = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
//            List<LfZdryHistoryEntity> historyEntities = specialPersons
//                    .parallelStream()
//                    .map(sp -> {
//                        LfZdryHistoryEntity entity = new LfZdryHistoryEntity();
//                        entity.setId(String.valueOf(KeyWorker.nextId()));
//                        entity.setName(sp.getXm());
//                        entity.setIdentityId(sp.getSfzh());
//                        entity.setResourceLocation(sp.getHjd());
//                        entity.setOwnCarType("暂无");
//                        entity.setControlDep(sp.getZrdw());
//                        entity.setControlPolice(sp.getGkmj());
//                        entity.setIsControl(sp.getSfzk());
//                        entity.setPersonType(sp.getRylbx());
//                        entity.setRecordVersion(recordVersion);
//                        entity.setOwnRange(sp.getSsxq());
//                        entity.setOwnRangeId(sp.getSsxqId());
//                        entity.setPoliceHomeId(sp.getOid());
//                        return entity;
//                    }).collect(Collectors.toList());
//            //分批进行插入，每批1000条
//            int times = historyEntities.size()/1000;
//            int left = historyEntities.size()%1000;
//            for(int i=0;i<times;i++){
//                int from = i*1000;
//                int to = from+1000;
//                historyMapper.insertHistoryByBatch(historyEntities.subList(from,to));
//            }
//            int leftStart = times*1000;
//            left = leftStart+left;
//            historyMapper.insertHistoryByBatch(historyEntities.subList(leftStart,left));
//        }
//    }
//
//    @Override
//    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
//        scheduledTaskRegistrar.addCronTask(()->executeSpecialPersonHistory(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))),systemConfig.getSyncCon());
//    }
//}
