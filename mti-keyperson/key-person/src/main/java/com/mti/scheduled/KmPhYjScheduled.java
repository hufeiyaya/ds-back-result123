package com.mti.scheduled;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mti.dao.model.KmPhYjEntity;
import com.mti.dao.model.PhoneLog;
import com.mti.mapper.PhoneLogMapper;
import com.mti.service.*;
import com.mti.threadhandle.PhoneYjDataHandle;
import com.mti.utils.AddressOrLngLatConvertUtil;
import com.mti.utils.UUIDUtils;
import com.mti.websocket.WebSocketUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Classname KmPhYjScheduled
 * @Description TODO
 * @Date 2020/4/29 20:12
 * @Created by duchaof
 * @Version 1.0
 */
@Component
@EnableScheduling
@Slf4j
public class KmPhYjScheduled {
    @Autowired
    private KmPhYjService phYjService;
    @Resource
    private RestTemplate restTemplate;
    @Autowired
    private ExecutorService executorService;
    @Autowired
    private ILfZtryyjsjService iLfZtryyjsjService;
    @Autowired
    private AddressOrLngLatConvertUtil addressOrLngLatConvertUtil;
    @Autowired
    private LfZdryBaseService lfZdryBaseService;

    @Resource
    private WebSocketUtils webSocketUtils;
    @Autowired
    private KmJurisdictionService kmJurisdictionService;
    @Autowired
    private KmZdryAssistService kmZdryAssistService;
    @Autowired
    private PhoneLogMapper phoneLogMapper;

    private static Lock lock = new ReentrantLock();

    // 只能在180上运行
    private static final String URL = "http://86.84.254.100:9084/gaw_webservice/km/fa/welcomeGetPersonAlarm";

    /**
     * 每个30秒获取一次手机定位信息
     */
    @Scheduled(cron = "0/30 * * * * ? ")
    public void getPhoneYjData() {
        try {
            String str = restTemplate.getForObject(URL, String.class);
//          String str = "{\"code\":\"0\",\"data\":[{\"alarmId\":\"2b2318725c5f7171e56f252fd8c1c65a\",\"alarmLocation\":\"航班CA1938|订票|KMG->PVG\",\"alarmTime\":\"2020-10-27 19:50:00\",\"alarmType\":\"航班预警\",\"idcard\":\"530121195203221211\",\"lat\":\"\",\"loc\":\"\",\"name\":\"陈福-大豆\",\"phone\":\"13333333333\"},{\"alarmId\":\"da3cbefe3af62c197d6ac950905d77ee\",\"alarmLocation\":\"[进入区域]昆明新机场1_21\",\"alarmTime\":\"2020-10-27 20:37:21\",\"alarmType\":\"手机预警\",\"idcard\":\"530102199602121137\",\"lat\":\"25.099415\",\"loc\":\"102.927431\",\"name\":\"张栖豪\",\"phone\":\"18669203097\"}],\"msg\":\"操作成功\"}";
            log.info("本次手机定位接受到的内容为：" + str);
            JSONObject json = JSONObject.parseObject(str);

            try {
                if (json.get("code").toString().equals("0")) {
                    List<KmPhYjEntity> dataList = JSONArray.parseArray(json.get("data").toString(), KmPhYjEntity.class);
                    //将获取到的手机定位数据入库
                    if (dataList != null && dataList.size() > 0) {
                        PhoneLog phoneLog = new PhoneLog();
                        phoneLog.setId(UUIDUtils.getUUID());
                        phoneLog.setCodeStatus(json.get("code").toString());
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        phoneLog.setCreatTime(sdf.format(new Date()));
                        phoneLog.setRemark("");
                        phoneLog.setResponsehone(json.get("data").toString());
                        phoneLogMapper.insert(phoneLog);
                    }
                }
            } catch (Exception e) {
                log.error("手机定位返参入库失败：======》{}", e.getMessage());
            } finally {
                if (json.get("code").toString().equals("0")) {
                    List<KmPhYjEntity> kmPhYjEntityList = JSONArray.parseArray(json.get("data").toString(), KmPhYjEntity.class);
                    for (KmPhYjEntity ph : kmPhYjEntityList) {
                        executorService.execute(new PhoneYjDataHandle(phYjService, ph, iLfZtryyjsjService,
                                addressOrLngLatConvertUtil, webSocketUtils, lfZdryBaseService, kmJurisdictionService, kmZdryAssistService, lock));
                    }
                }
            }
        } catch (RestClientException e) {
            log.error("远程调用接口时发生异常：======》{}", e.getMessage());
        }
    }
}
