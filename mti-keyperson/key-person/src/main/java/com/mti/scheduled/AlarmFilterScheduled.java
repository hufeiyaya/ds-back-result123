package com.mti.scheduled;

import com.mti.dao.model.LfZtryyjEntity;
import com.mti.dao.qo.KmAlarmFilterQo;
import com.mti.service.ILfZtryyjsjService;
import com.mti.service.TSwitchTypeServie;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author hf
 * @Title:
 * @Description: TODO (预警处置过滤定时任务)
 * @Param
 * @return
 * @Date 2020/11/25 14:26
 */
@Component
@EnableScheduling
@EnableAsync
@Slf4j
public class AlarmFilterScheduled {

    @Autowired
    private ILfZtryyjsjService iLfZtryyjsjService;

    @Autowired
    private TSwitchTypeServie tSwitchTypeServie;

    /**
     * 开关类型
     */
    private static int TYPE = 2;

    /**
     * 每三分钟执行一次
     * 程序执行时间太短没有关系，只要cron表达式秒的匹配符不设置为*就可以了。
     */
    @Async
    @Scheduled(cron = "0 */3 * * * ? ")
    public void sighOfAlarmData() {
        log.info("AlarmFilterScheduled task start");
        try {
            KmAlarmFilterQo qo = tSwitchTypeServie.getQoInfo(Arrays.asList(TYPE));
            List<LfZtryyjEntity> list = iLfZtryyjsjService.queryAlarmFilterData(qo);
            if (null != list && list.size() > 0) {
                iLfZtryyjsjService.updateAlarmFilterMark(list.stream().map(LfZtryyjEntity::getWybs).collect(Collectors.toList()));
            }
            log.info("AlarmFilterScheduled task end");
        } catch (Exception e) {
            log.error("AlarmFilterScheduled task error======》{}", e);
        }
    }
}
