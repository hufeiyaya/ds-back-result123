package com.mti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZdryBaseEntity;
import com.mti.dao.model.TPfGroupEntity;
import com.mti.dao.model.TPfGroupZdryEntity;
import com.mti.dao.qo.TPfGroupZdryQo;
import com.mti.dao.vo.TPfGroupZdryVo;
import org.springframework.web.server.ServerWebExchange;

import java.util.List;

/**
 * <p>
 * 重点人员分组表 服务类
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */

public interface TPfGroupZdryService extends IService<TPfGroupZdryEntity> {
    void updateZdryGroupInfo(List<String> groupIdList, String zdryId);

    List<TPfGroupZdryVo> getZdryGroupInfo(ServerWebExchange exchange, String zdryId);

    IPage<LfZdryBaseEntity> getPageList(ServerWebExchange exchange, TPfGroupZdryQo qo);
}