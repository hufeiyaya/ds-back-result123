package com.mti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfRyyjsjEntity;
import com.mti.dao.qo.LfRyyjsjQO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface ILfRyyjsjService extends IService<LfRyyjsjEntity> {

    List<LfRyyjsjEntity> queryListByYjbs(String yjbs, String orderBy);

    IPage<LfRyyjsjEntity> queryPage(LfRyyjsjQO qo);

    int getTx(String id);

//    int updateByEntity(LfRyyjsjEntity entity);

    IPage<LfRyyjsjEntity> queryPageByYjbs(LfRyyjsjQO qo);
}
