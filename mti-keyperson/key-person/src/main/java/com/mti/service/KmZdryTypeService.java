package com.mti.service;

import com.mti.dao.model.KmZdryTypeEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
public interface KmZdryTypeService extends IService<KmZdryTypeEntity> {
    /**
     * 通过身份证号查询 重点人员大类小类信息
     * @param yjbs
     * @return
     */
    List<KmZdryTypeEntity> getZdryTypeBySfzh(String yjbs);
}
