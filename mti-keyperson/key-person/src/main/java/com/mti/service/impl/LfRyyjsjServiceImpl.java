package com.mti.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfRyyjsjEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.mapper.LfRyyjsjMapper;
import com.mti.service.ILfRyyjsjService;
import com.mti.utils.GpsUtils;
import com.mti.utils.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Service
public class LfRyyjsjServiceImpl extends ServiceImpl<LfRyyjsjMapper, LfRyyjsjEntity> implements ILfRyyjsjService {

    @Resource
    private LfRyyjsjMapper mapper;

    public List<LfRyyjsjEntity> queryListByYjbs(String yjbs, String orderBy) {
        if (StringUtils.isBlank(orderBy)) {
            orderBy = "desc";
        }
        return mapper.queryListByYjbs(yjbs, orderBy);
    }

    public IPage<LfRyyjsjEntity> queryPageByYjbs(LfRyyjsjQO qo) {
        Page<LfRyyjsjEntity> page = new Page<LfRyyjsjEntity>(qo.getStart(), qo.getSize());
        if (qo != null && StringUtils.isNotBlank(qo.getYjbs()) ){
            QueryWrapper<LfRyyjsjEntity> queryWrapper = new QueryWrapper<LfRyyjsjEntity>();
            queryWrapper.eq("yjbs",qo.getYjbs());
            if (StringUtils.isBlank(qo.getOrderBy()) || qo.getOrderBy().equals("desc")) {
                queryWrapper.orderByDesc("yjsj");
            } else {
                queryWrapper.orderByAsc("yjsj");
            }
            return this.page(page,queryWrapper);
        } else {
            return page;
        }
    }

    public IPage<LfRyyjsjEntity> queryPage(LfRyyjsjQO qo) {
        Page<LfRyyjsjEntity> page = new Page<LfRyyjsjEntity>(qo.getStart(), qo.getSize());
        if(qo == null
                || (qo.getRylbxList() != null && qo.getRylbxList().size() == 0)
                || (qo.getSfzkList() != null && qo.getSfzkList().size() == 0)
                || (qo.getZrbmList() != null && qo.getZrbmList().size() == 0)
                || (qo.getSsxqList() != null && qo.getSsxqList().size() == 0)
                || (qo.getGkjbxList() != null && qo.getGkjbxList().size() == 0)
                || (qo.getMjlxdhList() != null && qo.getMjlxdhList().size() == 0)
                || (qo.getSspcsList() != null && qo.getSspcsList().size() == 0) ){
            QueryWrapper<LfRyyjsjEntity> queryWrapper = new QueryWrapper<LfRyyjsjEntity>();
            queryWrapper.eq("ryxm","-##-");
            return this.page(page,queryWrapper);
        } else {
            qo.setRylbxList(ListUtils.removeNullElement(qo.getRylbxList()));
            qo.setSfzkList(ListUtils.removeNullElement(qo.getSfzkList()));
            qo.setZrbmList(ListUtils.removeNullElement(qo.getZrbmList()));
            qo.setSsxqList(ListUtils.removeNullElement(qo.getSsxqList()));
            qo.setGkjbxList(ListUtils.removeNullElement(qo.getGkjbxList()));
            qo.setMjlxdhList(ListUtils.removeNullElement(qo.getMjlxdhList()));
            qo.setSspcsList(ListUtils.removeNullElement(qo.getSspcsList()));
            return mapper.queryPage(page,qo);
        }

//        QueryWrapper<LfRyyjsjEntity> queryWrapper = new QueryWrapper<>();
//
//        // 关键字
//        String name = (String) qo.getName();
//        if(StringUtils.isNotBlank(name)) {
//            queryWrapper.and(wrapper -> wrapper.like("ryxm", name).or().like("yjdd", name).or().like("yjlx", name).or().like("sjbs", name));
//        }
//
//        // 预警标识，身份证号
//        if (StringUtils.isNotBlank(qo.getYjbs())) {
//            queryWrapper.eq(true, "yjbs", qo.getYjbs());
//        }
//
//        queryWrapper.orderByDesc("yjsj");
//        return this.page(page, queryWrapper);

    }

    @Override
    public int getTx(String id) {

        if (id.equals("all")) {
            List<LfRyyjsjEntity> entitys = this.list();
            entitys.forEach(item -> {
                String gpsString = new GpsUtils().getgps(item.getYjdd()+item.getDdmc());
                if(!StringUtils.isBlank(gpsString)) {
                    JSONObject resultJson = JSON.parseObject(gpsString);
                    if(resultJson != null && resultJson.size() > 0){
                        JSONObject jSONObject = (JSONObject) resultJson.get(0);
                        String x = jSONObject.getString("gcjlon");
                        String y = jSONObject.getString("gcjlat");
                        item.setX(x);
                        item.setY(y);
                    }
                }
                this.saveOrUpdate(item);
            });
        } else {
//            List<LfRyyjsjEntity> entitys = this.getById()
//            String gpsString = new GpsUtils().getgps(item.getYjdd()+item.getDdmc());
//            if(!StringUtils.isBlank(gpsString)) {
//                JSONObject resultJson = JSON.parseObject(gpsString);
//                if(resultJson != null && resultJson.size() > 0){
//                    JSONObject jSONObject = (JSONObject) resultJson.get(0);
//                    String x = jSONObject.getString("gcjlon");
//                    String y = jSONObject.getString("gcjlat");
//                    item.setX(x);
//                    item.setY(y);
//                }
//            }
//            this.saveOrUpdate(item);
        }
        return 0;
    }

}
