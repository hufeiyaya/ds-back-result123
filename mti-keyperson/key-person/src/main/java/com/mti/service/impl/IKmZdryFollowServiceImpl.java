package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfZdryApproveLogEntity;
import com.mti.dao.model.LfZdryBaseEntity;
import com.mti.dao.model.LfZdryHistoryLogEntity;
import com.mti.enums.ApproveStatus;
import com.mti.exception.BusinessException;
import com.mti.jwt.AccountBo;
import com.mti.jwt.RequestUtils;
import com.mti.mapper.KmZdryFollowMapper;
import com.mti.service.IKmZdryFollowService;
import com.mti.service.LfZdryHistoryLogService;
import com.mti.utils.GetRemoteIpUtil;

import lombok.AllArgsConstructor;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;

/**
 * @author ： caoxx
 * @date ：2020/09/02 09:45
 * @description : 关注/取消关注service实现类
 * @path : com.mti.service.impl.KmZdryAttentionServiceImpl
 * @modifiedBy ：
 */
@Service
@AllArgsConstructor
public class IKmZdryFollowServiceImpl extends ServiceImpl<KmZdryFollowMapper, LfZdryBaseEntity> implements IKmZdryFollowService {

    private final KmZdryFollowMapper followMapper;
    
    @Autowired
	private GetRemoteIpUtil getRemoteIpUtil;
    
    @Autowired
	private RequestUtils requestUtils;
    
    @Autowired
	private LfZdryHistoryLogService lfZdryHistoryLogService;
    @Override
    public void follow(String personId,String masterId,String creator,ServerWebExchange exchange) {
    	this.operatLog("关注", "关注重点人员",personId,exchange) ;
        this.followMapper.follow(personId,masterId,creator);
    }

    @Override
    public void unFollow(String personId,String masterId) {
        this.followMapper.unFollow(personId,masterId);
    }
    
    
	/**
	 * 操作日志方法
	 * @param type
	 * @param bo
	 * @param entity
	 * @param logRecord
	 * @return
	 */
	public void operatLog(String type,String logRecord,String personId,ServerWebExchange exchange) {
		AccountBo bo = requestUtils.getCurrentUser(exchange);
		if(bo==null) {
			throw new BusinessException(10019,"此账号已在别处登录，请查证");
		}
   		//历史记录
   		LfZdryHistoryLogEntity history = new LfZdryHistoryLogEntity();
        history.setCreator(bo.getPersonBo().getName());
        history.setCreatorCode(bo.getPersonBo().getPoliceCode());
        history.setCreateTime(new Date());
   		history.setSpId(personId);
   		history.setApproveResult(logRecord);
   		history.setOperation(type);
   		history.setOperateDept(bo.getOrganizationBo().getShortName());
   		history.setFunctionModule("重点人员模块");

		ServerHttpRequest request = exchange.getRequest();
		String ip = getRemoteIpUtil.getRemoteIP(request);
		history.setOperateIp(ip);
		history.setType(3);
   		boolean flag = lfZdryHistoryLogService.save(history);
   		
	}
}
