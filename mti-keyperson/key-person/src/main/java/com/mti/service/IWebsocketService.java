package com.mti.service;

import com.mti.vo.Message;
import com.mti.vo.MessageText;

import java.util.List;

public interface IWebsocketService {

    /**
     * 发送消息
     * @param message 消息实体
     */
    void sendMessage(Message message);

    /**
     * 发送消息
     * @param messageText 消息实体
     * @param mjidList 民警ID列表
     */
    void sendMessage2AppMj(final MessageText messageText, List<String> mjidList);

    /**
     * 发送消息（Text）
     * @param message 消息实体
     */
    void sendMessageText(MessageText message);

    void sendMessageTextToAll(final MessageText message);

    /**
     * @brief send message to some pc-terminal client
     * @param message
     */
    void sendMessageTextToSome(final MessageText message);
}
