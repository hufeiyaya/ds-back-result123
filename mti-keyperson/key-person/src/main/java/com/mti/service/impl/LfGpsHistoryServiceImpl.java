package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfGpsHistoryEntity;
import com.mti.mapper.LfGpsHistoryMapper;
import com.mti.service.ILfGpsHistoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Service
public class LfGpsHistoryServiceImpl extends ServiceImpl<LfGpsHistoryMapper, LfGpsHistoryEntity> implements ILfGpsHistoryService {

}
