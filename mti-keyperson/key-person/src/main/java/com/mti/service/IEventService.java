package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.EventEntity;

public interface IEventService extends IService<EventEntity> {
}
