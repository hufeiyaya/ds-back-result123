package com.mti.service.impl;

import com.mti.dao.model.TKmWbswEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.mapper.TKmWbswEntityMapper;
import com.mti.service.TKmWbswService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Classname TKmWbswServiceImpl
 * @Description TODO
 * @Date 2020/1/17 23:22
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmWbswServiceImpl implements TKmWbswService {
    @Resource
    private TKmWbswEntityMapper tKmWbswEntityMapper;

    @Override
    public Integer insert(TKmWbswEntity tKmWbswEntity) {
        return tKmWbswEntityMapper.insert(tKmWbswEntity);
    }

    @Override
    public TKmWbswEntity getByGlId(String glId) {
        return tKmWbswEntityMapper.selectByPrimaryKey(glId);
    }

    @Override
    public List<TKmWbswEntity> getWbswListBySfzh(String sfzf) {
        return tKmWbswEntityMapper.getWbswListBySfzh(sfzf);
    }

    @Override
    public List<Map<String, Object>> queryWbLgKyNumber(LfRyyjsjQO lfRyyjsjQO) {
        return tKmWbswEntityMapper.queryWbLgKyNumber(lfRyyjsjQO);
    }

    @Override
    public List<TKmWbswEntity> getWbswList(List<String> wbswEntities) {
        return tKmWbswEntityMapper.getWbswList(wbswEntities);
    }

    @Override
    public List<Map<String, Object>> queryWbswLocationInfo(List<String> sfzhs) {
        return tKmWbswEntityMapper.queryWbswLocationInfo(sfzhs);
    }

    @Override
    public Map<String, Object> findWbswStartAndEndTime(String wybs) {
        return tKmWbswEntityMapper.findWbswStartAndEndTime(wybs);
    }
}
