package com.mti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.TKmDailyCheckEntity;
import com.mti.dao.qo.TkmDCheckQo;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Mono;

/**
 * @Classname TKmDailyCheckService
 * @Description TODO
 * @Date 2020/2/27 17:00
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmDailyCheckService extends IService<TKmDailyCheckEntity> {
    /**
     * 添加每日核查记录
     * @param tKmDailyCheckEntity
     * @return
     */
    Object saveTKmDailyCheck(TKmDailyCheckEntity tKmDailyCheckEntity);

    /**
     * 分页查询核查记录
     * @param tkmDCheckQo
     * @return
     */
    IPage<TKmDailyCheckEntity> queryTkmDailyCheckPage(TkmDCheckQo tkmDCheckQo);

    /**
     * 导出重点人员的核查信息
     * @param exportName
     * @param keyWords
     * @param startTime
     * @param endTime
     * @param checkResult
     * @return
     */
    Mono<DataBuffer> exportDailyCheck(String exportName, String keyWords, String startTime, String endTime,
                                      String checkResult,String ssfj,String sspcs,String jz);

    /**
     * 根据条件查询日常核查信息
     * @param tKmDailyCheckEntity
     * @return
     */
    Object queryTkmDailyCheckList(TKmDailyCheckEntity tKmDailyCheckEntity);
}
