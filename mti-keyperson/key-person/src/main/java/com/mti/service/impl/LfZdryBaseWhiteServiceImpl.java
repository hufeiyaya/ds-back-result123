/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.service.impl;



import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfZdryBaseWhiteEntity;
import com.mti.mapper.LfZdryBaseWhiteMapper;
import com.mti.service.LfZdryBaseWhiteService;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 
 * @author chenf
 * @since 2019-04-29
 */
@Slf4j
@Service
public class LfZdryBaseWhiteServiceImpl extends ServiceImpl<LfZdryBaseWhiteMapper, LfZdryBaseWhiteEntity> implements LfZdryBaseWhiteService {
	
}