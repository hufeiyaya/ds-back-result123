package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.ArmingTaskRelationEntity;
import com.mti.dao.model.LfZdryArmingTaskOperateLog;
import org.springframework.web.server.ServerWebExchange;

import java.util.List;
import java.util.Map;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/9/12
 * @change
 * @describe describe
 **/
public interface IArmingTaskRelationService extends IService<ArmingTaskRelationEntity> {
    void approveArmingTask(String rid, Integer status, String description, ServerWebExchange exchange);
    List<LfZdryArmingTaskOperateLog> getLogs(String rid);
    List<Map<String,Object>> getReportedOrg(String taskId);
}
