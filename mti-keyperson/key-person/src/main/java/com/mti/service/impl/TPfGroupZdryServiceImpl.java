package com.mti.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.component.snowflake.KeyWorker;
import com.mti.dao.model.LfZdryBaseEntity;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.TPfGroupEntity;
import com.mti.dao.model.TPfGroupZdryEntity;
import com.mti.dao.qo.TPfGroupZdryQo;
import com.mti.dao.vo.TPfGroupZdryVo;
import com.mti.exception.BusinessException;
import com.mti.jwt.AccountBo;
import com.mti.jwt.RequestUtils;
import com.mti.mapper.TPfGroupZdryMapper;
import com.mti.service.TPfGroupService;
import com.mti.service.TPfGroupZdryService;
import com.mti.utils.ListUtils;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 重点人员分组表 服务实现类
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */
@Slf4j
@Service
public class TPfGroupZdryServiceImpl extends ServiceImpl<TPfGroupZdryMapper, TPfGroupZdryEntity> implements TPfGroupZdryService {

    @Autowired
    private RequestUtils requestUtils;

    @Autowired
    private TPfGroupService tPfGroupService;

    /**
     * 更新重点人员分组情况之前，先清空他的分组信息
     *
     * @param groupIdList
     * @param zdryId
     */
    @Override
    public void updateZdryGroupInfo(List<String> groupIdList, String zdryId) {
        QueryWrapper<TPfGroupZdryEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("zdry_id", zdryId);
        this.remove(wrapper);
        if (!Collections.isEmpty(groupIdList)) {
            groupIdList.forEach(groupId -> {
                TPfGroupZdryEntity tPfGroupZdryEntity = new TPfGroupZdryEntity();
                tPfGroupZdryEntity.setId(String.valueOf(KeyWorker.nextId()));
                tPfGroupZdryEntity.setZdryId(zdryId);
                tPfGroupZdryEntity.setGroupId(groupId);
                this.save(tPfGroupZdryEntity);
            });
        }

    }

    @Override
    public List<TPfGroupZdryVo> getZdryGroupInfo(ServerWebExchange exchange, String zdryId) {
        List<TPfGroupZdryVo> glist = new ArrayList<>();
        List<TPfGroupEntity> list = tPfGroupService.getGroupInfoByAccId(exchange);
        list.forEach(e -> {
            TPfGroupZdryVo tPfGroupZdryVo = new TPfGroupZdryVo();
            tPfGroupZdryVo.setId(e.getId());
            tPfGroupZdryVo.setName(e.getGroupName());
            tPfGroupZdryVo.setDefault(e.isDefault());

            QueryWrapper<TPfGroupZdryEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("group_id", e.getId());
            tPfGroupZdryVo.setCount(this.count(queryWrapper));
            if(!StringUtils.isEmpty(zdryId))
            {
                boolean flag = this.getBaseMapper().getByGroupId(e.getId(), zdryId);
                tPfGroupZdryVo.setIn(flag);
            }
            glist.add(tPfGroupZdryVo);
        });
        return glist;
    }

    @Override
    public IPage<LfZdryBaseEntity> getPageList(ServerWebExchange exchange, TPfGroupZdryQo qo) {
        // 权限相关代码
        AccountBo bo = requestUtils.getCurrentUser(exchange);
        if (bo == null) {
            throw new BusinessException(10019, "此账号已在别处登录，请查证");
        }
        Page<LfZdryEntity> page = new Page<>(qo.getStart(), qo.getSize());
        qo.setAccId(bo.getId());
        return this.getBaseMapper().getPageList(page,qo);
    }

}


