package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.dto.TimeFlow;
import com.mti.dao.kmmodel.GisModel;
import com.mti.dao.kmmodel.KmArmingTaskEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.server.ServerWebExchange;

import java.util.List;

/***
 * @Description: 创建布控服务类
 * @Author:
 * @Date: 2020/9/1 16:14
   @Param: caoxx
   @Return: 
 **/
public interface IKmArmingTaskService extends IService<KmArmingTaskEntity> {
    /***
     * 通过id获取布控任务
     * @param id 布控任务id
     * @return KmArmingTaskEntity对象
     */
    KmArmingTaskEntity getKmArmingTaskByid(String id);

    /***
     * 根据id删除布控任务
     * @param id 布控任务id
     */
    void deleteKmArmingTaskByid(String id);

    /***
     * 保存或修改布控任务
     * @param taskEntity 对象
     */
    void saveOrUpdateKmArmingTask(KmArmingTaskEntity taskEntity,ServerWebExchange exchange);

    /***
     * 保存布控任务信息
     * @param taskId 布控任务id
     * @param recordStatus 布控任务状态
     */
    void saveRecordData(String taskId,Integer recordStatus);

    /***
     * 根据任务开始时间和任务状态获取任务id
     * @return
     */
    List selectIdByStartDateAndArmingStatus();

    /***
     * 根据任务结束时间和任务状态获取任务id
     * @return
     */
    List selectIdByEndDateAndArmingStatus();

    /***
     * 根据任务id修改任务状态
     * @param taskId
     * @param status
     */
    void updateArmingStatusById(String taskId,Integer status);

    List<GisModel> getGisModels(String json);

    boolean setTimeFlows(String taskId, LfRyyjsjQO qo);
    
    
    boolean updateStatus(String taskId,Integer status,ServerWebExchange exchange);
    
    boolean getDefault(String id);
    
    boolean cancelDefault(String id);
}
