package com.mti.service.impl;

import com.mti.dao.model.KmRlsbEntity;
import com.mti.mapper.KmRlsbMapper;
import com.mti.service.KmRlsbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 人脸识别 服务实现类
 * </p>
 *
 * @author duchao
 * @since 2020-06-19
 */
@Service
public class KmRlsbServiceImpl extends ServiceImpl<KmRlsbMapper, KmRlsbEntity> implements KmRlsbService {

    @Override
    public boolean findRlsbInfo(KmRlsbEntity kmRlsbEntity) {
        Integer count = this.getBaseMapper().findRlsbInfo(kmRlsbEntity);
        if(count>0) return true;
        return false;
    }
}
