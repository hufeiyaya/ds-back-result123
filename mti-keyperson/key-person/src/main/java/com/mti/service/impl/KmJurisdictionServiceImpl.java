package com.mti.service.impl;

import com.mti.dao.model.KmJurisdictionEntity;
import com.mti.mapper.KmJurisdictionMapper;
import com.mti.service.KmJurisdictionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author duchao
 * @since 2020-05-03
 */
@Service
public class KmJurisdictionServiceImpl extends ServiceImpl<KmJurisdictionMapper, KmJurisdictionEntity> implements KmJurisdictionService {

    @Override
    public KmJurisdictionEntity queryYjdPcsByLngAndLat(String loc, String lat) {
        return this.getBaseMapper().queryYjdPcsByLngAndLat(loc,lat);
    }
}
