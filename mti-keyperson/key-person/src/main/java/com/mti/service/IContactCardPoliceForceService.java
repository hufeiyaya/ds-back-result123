package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.ContactCardPoliceForceEntity;

/**
 * <p>
 *  卡点警力分布信息 服务类
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-15
 */
public interface IContactCardPoliceForceService extends IService<ContactCardPoliceForceEntity> {

}
