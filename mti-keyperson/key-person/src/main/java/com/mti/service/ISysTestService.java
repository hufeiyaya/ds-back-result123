package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.SysTestEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yangsp
 * @since 2019-03-18
 */
public interface ISysTestService extends IService<SysTestEntity> {

}
