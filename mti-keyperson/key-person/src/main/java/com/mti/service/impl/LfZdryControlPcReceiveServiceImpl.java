package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfZdryControlPcEntity;
import com.mti.dao.model.LfZdryControlPcReceiveEntity;
import com.mti.mapper.LfZdryControlPcMapper;
import com.mti.mapper.LfZdryControlPcReceiveMapper;
import com.mti.service.ILfZdryControlPcReceiveService;
import com.mti.service.ILfZdryControlPcService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  重点人员布控pc接收单位信息服务实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-26
 */
@Service
public class LfZdryControlPcReceiveServiceImpl extends ServiceImpl<LfZdryControlPcReceiveMapper, LfZdryControlPcReceiveEntity> implements ILfZdryControlPcReceiveService {

}
