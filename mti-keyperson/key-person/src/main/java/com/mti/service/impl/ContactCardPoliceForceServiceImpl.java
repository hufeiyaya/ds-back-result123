package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.ContactCardPoliceForceEntity;
import com.mti.mapper.ContactCardPoliceForceMapper;
import com.mti.service.IContactCardPoliceForceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  卡点警力分布信息 服务实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-15
 */
@Service
public class ContactCardPoliceForceServiceImpl extends ServiceImpl<ContactCardPoliceForceMapper, ContactCardPoliceForceEntity> implements IContactCardPoliceForceService {

}
