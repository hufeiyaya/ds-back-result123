package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZdryControlPcEntity;

/**
 * <p>
 *  重点人员布控pc信息服务类
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-26
 */
public interface ILfZdryControlPcService extends IService<LfZdryControlPcEntity> {

}
