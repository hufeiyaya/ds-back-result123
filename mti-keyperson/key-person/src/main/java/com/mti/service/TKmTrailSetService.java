package com.mti.service;


import java.util.List;

import org.springframework.web.server.ServerWebExchange;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.KmTrailEntity;
import com.mti.dao.model.TKmTrailSetEntity;
import com.mti.dao.qo.TKmTrailQO;
import com.mti.dao.qo.TKmTrailSetQO;
import com.mti.dao.vo.KmTrailVo;

/**
 * @Classname TKmTrailSetService
 * @Description TODO
 * @Date 2020/1/10 22:30
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmTrailSetService extends IService<TKmTrailSetEntity> {
	
	boolean saveTKmTrailSet(TKmTrailSetEntity entity,ServerWebExchange exchange);
	Integer deleteTKmTrailSet(String id,ServerWebExchange exchange);
	Integer stop(String id);
	Integer start(String id);
	/**
	 *  列表查询
	 * @param qo
	 * @return
	 */
	IPage<TKmTrailSetEntity> queryPageList(TKmTrailSetQO qo);
	
	IPage<KmTrailEntity> getPageList(TKmTrailQO qo);
	
	List<KmTrailVo>  getAllList(String type);
	
	Integer updateStatusById(String id);

}
