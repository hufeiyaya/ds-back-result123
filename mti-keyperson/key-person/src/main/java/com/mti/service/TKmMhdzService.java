package com.mti.service;


import com.mti.dao.model.TKmMhdzEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.utils.ZdryQxzkUtilBean;

import java.util.List;
import java.util.Map;

/**
 * @Classname TKmMhdzService
 * @Description TODO
 * @Date 2020/1/17 23:20
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmMhdzService {
    Integer insert(TKmMhdzEntity tKmMhdzEntity);

    TKmMhdzEntity getByGlId(String glId);

    List<TKmMhdzEntity> getMhdzListBySfzh(String sfzf);

    /**
     * 获取民航出昆明的数据
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getMhOut(LfRyyjsjQO lfRyyjsjQO);

    /**
     * 获取民航入昆明的数据
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getMhIn(LfRyyjsjQO lfRyyjsjQO);

    /**
     * 获取民航订座导出数据
     * @param mhdzEntities
     * @return
     */
    List<TKmMhdzEntity> getMhdzList(List<String> mhdzEntities);
}
