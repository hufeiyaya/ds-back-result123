package com.mti.service.impl;

import com.mti.dao.model.TKmGlkygpEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.mapper.TKmGlkygpEntityMapper;
import com.mti.service.TKmGlkygpService;
import com.mti.utils.ZdryQxzkUtilBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Classname TKmGlkygpServiceImpl
 * @Description TODO
 * @Date 2020/1/17 23:15
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmGlkygpServiceImpl implements TKmGlkygpService {

    @Resource
    private TKmGlkygpEntityMapper tKmGlkygpEntityMapper;

    @Override
    public Integer insert(TKmGlkygpEntity tKmGlkygpEntity) {
        return tKmGlkygpEntityMapper.insert(tKmGlkygpEntity);
    }

    @Override
    public TKmGlkygpEntity getByGlId(String glId) {

        return tKmGlkygpEntityMapper.selectByPrimaryKey(glId);
    }

    @Override
    public List<TKmGlkygpEntity> getGlkygpBySfzh(String sfzf) {
        return tKmGlkygpEntityMapper.getGlkygpBySfzh(sfzf);
    }

    @Override
    public List<ZdryQxzkUtilBean> getKyOut(LfRyyjsjQO lfRyyjsjQO) {
        return tKmGlkygpEntityMapper.getKyOut(lfRyyjsjQO);
    }

    @Override
    public List<ZdryQxzkUtilBean> getKyIn(LfRyyjsjQO lfRyyjsjQO) {
        return tKmGlkygpEntityMapper.getKyIn(lfRyyjsjQO);
    }

    @Override
    public List<TKmGlkygpEntity> getGlkygpList(List<String> glkyEntities) {
        return tKmGlkygpEntityMapper.getGlkygpList(glkyEntities);
    }
}
