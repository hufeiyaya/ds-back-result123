package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.kmmodel.KmZdryDto;
import com.mti.mapper.KmZdryMapper;
import com.mti.service.KmZdryService;
import org.springframework.stereotype.Service;

/**
 * @Classname KmZdryServiceImpl
 * @Description TODO
 * @Date 2020/1/10 22:32
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class KmZdryServiceImpl extends ServiceImpl<KmZdryMapper, KmZdryDto> implements KmZdryService {
}
