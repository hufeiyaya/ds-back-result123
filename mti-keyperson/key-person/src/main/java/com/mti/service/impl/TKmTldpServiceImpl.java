package com.mti.service.impl;

import com.mti.dao.model.TKmTldpEntity;
import com.mti.mapper.TKmTldpEntityMapper;
import com.mti.service.TKmTldpService;
import com.mti.utils.ZdryQxzkUtilBean;
import com.mti.utils.ZdryYjUtilBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Classname TKmTldpServiceImpl
 * @Description TODO
 * @Date 2020/1/17 16:14
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmTldpServiceImpl implements TKmTldpService {
    @Resource
    private TKmTldpEntityMapper tKmTldpEntityMapper;


    @Override
    public Integer insert(TKmTldpEntity tKmTldpEntity) {
        return tKmTldpEntityMapper.insert(tKmTldpEntity);
    }


    @Override
    public TKmTldpEntity getByGlId(String glId) {
        return tKmTldpEntityMapper.selectByPrimaryKey(glId);
    }

    @Override
    public List<TKmTldpEntity> getTldpListBySfzh(String sfzh) {
        return tKmTldpEntityMapper.getTldpListBySfzh(sfzh);
    }

    @Override
    public List<ZdryQxzkUtilBean> getTlOut(String startTime, String endTime) {
        return tKmTldpEntityMapper.getTlOut(startTime,endTime);
    }

    @Override
    public List<ZdryQxzkUtilBean> getTlIn(String startTime, String endTime) {
        return tKmTldpEntityMapper.getTlIn(startTime,endTime);
    }

    @Override
    public List<TKmTldpEntity> getTldpEntityList(List<String> tldpEntities) {
        return tKmTldpEntityMapper.getTldpEntityList(tldpEntities);
    }
}
