package com.mti.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfZtryEntity;
import com.mti.mapper.LfZtryMapper;
import com.mti.service.IZtryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ZtryServiceImpl extends ServiceImpl<LfZtryMapper, LfZtryEntity> implements IZtryService {
    @Override
    public LfZtryEntity getBySfzh(String sfzh) {
        return this.getBaseMapper().getOneSfzh(sfzh);
    }
}
