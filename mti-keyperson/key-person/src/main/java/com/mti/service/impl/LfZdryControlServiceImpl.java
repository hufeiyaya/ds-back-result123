package com.mti.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfZdryControlEntity;
import com.mti.mapper.LfZdryControlMapper;
import com.mti.service.ILfZdryControlService;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * <p>
 *  布控信息 服务实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-15
 */
@Service
public class LfZdryControlServiceImpl extends ServiceImpl<LfZdryControlMapper, LfZdryControlEntity> implements ILfZdryControlService {


    @Override
    public Boolean removeByBean(LfZdryControlEntity entity) {
        return this.remove(new QueryWrapper(entity));
    }

    @Override
    public LfZdryControlEntity getByBean(LfZdryControlEntity entity) {
        return this.getOne(new QueryWrapper<>(entity));
    }

    @Override
    public Collection<LfZdryControlEntity> listByBean(LfZdryControlEntity entity) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.setEntity(entity);
        if (entity.getStatusList() !=null && entity.getStatusList().size() > 0) {
            queryWrapper.in("status_", entity.getStatusList());
        }

        return this.list(queryWrapper);
    }

    @Override
    public IPage pageByBean(LfZdryControlEntity entity) {
        IPage<LfZdryControlEntity> page = new Page(entity.getPage(), entity.getSize());
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.setEntity(entity);
        if (entity.getStatusList() !=null && entity.getStatusList().size() > 0) {
            queryWrapper.in("status_", entity.getStatusList());
        }
        queryWrapper.orderByDesc("create_time");
        return this.page(page, queryWrapper);
    }
}
