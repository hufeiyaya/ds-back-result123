package com.mti.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.KmZdryZrbmEntity;
import com.mti.dao.qo.PoliceQO;
import com.mti.mapper.KmZdryZrbmMapper;
import com.mti.service.KmZdryZrbmService;

import java.util.List;

/**
 * <p>
 * 警种大类小类对应表 服务实现类
 * </p>
 *
 * @author chenf
 * @since 2020-03-26
 */
@Service
public class KmZdryZrbmServiceImpl extends ServiceImpl<KmZdryZrbmMapper, KmZdryZrbmEntity> implements KmZdryZrbmService {

	@Override
	public KmZdryZrbmEntity getPoliceType(ServerWebExchange exchange, PoliceQO qo) {
		return  this.getOne(new QueryWrapper<KmZdryZrbmEntity>().eq("rydl_code", qo.getRydl()).eq("ryxl_code",
				qo.getRyxl()).lambda().last("LIMIT 1"));
	}

	@Override
	public List<KmZdryZrbmEntity> getAllZrbm(){
		return this.list();
	}

	@Override
	public KmZdryZrbmEntity queryZrbmByDlAndXl(String rylb, String xl) {
		return this.getOne(new QueryWrapper<KmZdryZrbmEntity>().eq("rydl",rylb).eq("ryxl",xl)
				.lambda().last("limit 1"));
	}

	@Override
	public KmZdryZrbmEntity queryDlInfo(String rylb) {
		return this.getOne(new QueryWrapper<KmZdryZrbmEntity>().eq("rydl",rylb).lambda().last("limit 1"));
	}

	@Override
	public KmZdryZrbmEntity queryXlInfo(String xl) {
		return this.getOne(new QueryWrapper<KmZdryZrbmEntity>().eq("ryxl",xl).lambda().last("limit 1"));
	}

}
