package com.mti.service;

import com.mti.dao.model.KmZdryZrbmEntity;
import com.mti.dao.qo.PoliceQO;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ServerWebExchange;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 警种大类小类对应表 服务类
 * </p>
 *
 * @author chenf
 * @since 2020-03-26
 */
public interface KmZdryZrbmService extends IService<KmZdryZrbmEntity> {

	KmZdryZrbmEntity getPoliceType(ServerWebExchange exchange, @RequestBody PoliceQO qo);

	/**
	 * 导入时获取所有的 大类 和 小类
	 * @return
	 */
	 List<KmZdryZrbmEntity> getAllZrbm();

	/**
	 * 根据大类 和 小类 查询警种
	 * @param rylb
	 * @param xl
	 * @return
	 */
	KmZdryZrbmEntity queryZrbmByDlAndXl(String rylb, String xl);

	/**
	 * 根据大类名称查询大类信息
	 * @param rylb
	 * @return
	 */
	KmZdryZrbmEntity queryDlInfo(String rylb);

	/**
	 * 根据小类名称查询小类信息
	 * @param xl
	 * @return
	 */
	KmZdryZrbmEntity queryXlInfo(String xl);
}
