package com.mti.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.TTaskFileEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author duchaof
 * @since 2020-03-11
 */
public interface TTaskFileService extends IService<TTaskFileEntity> {
    List<TTaskFileEntity> 	getByBussId(String bussId);

	boolean removeByZdryId(String zdryId);
}
