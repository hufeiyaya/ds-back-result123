package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZdryModifyLogEntity;

public interface ILfZdryModifyLogService extends IService<LfZdryModifyLogEntity> {
}
