package com.mti.service;


import com.mti.dao.model.TKmTldpEntity;
import com.mti.dao.model.TKmTldpv2Entity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.utils.ZdryQxzkUtilBean;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @Classname TKmTldpv2Service
 * @Description TODO
 * @Date 2020/3/5 14:05
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmTldpv2Service {
    /**
     * 插入预警信息
     * @param tKmTldpv2Entity
     * @return
     */
    Integer insert(TKmTldpv2Entity tKmTldpv2Entity);

    /**
     * 判断铁路订票 进-出
     * @param tKmTldpv2Entity
     * @return
     */
    Integer judgeInOutType(TKmTldpv2Entity tKmTldpv2Entity);

    /**
     * 通过关联glId查询预警信息
     * @param glId
     * @return
     */
    TKmTldpv2Entity getByGlId(String glId);

    List<TKmTldpv2Entity> getTldpListBySfzh(String sfzf);

    /**
     * 查询铁路出昆明的数据
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getTlOut(LfRyyjsjQO lfRyyjsjQO);

    /**
     * 查询铁路进昆明的数据
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getTlIn(LfRyyjsjQO lfRyyjsjQO);

    /**
     * 根据铁路订票的systemid查询铁路订票的预警信息
     * @param tldpEntities
     * @return
     */
    List<TKmTldpv2Entity> getTldpEntityList(List<String> tldpEntities);
}
