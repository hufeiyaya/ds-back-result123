package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZdryControlPcReceiveEntity;

/**
 * <p>
 *  重点人员布控pc接收单位信息服务类
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-26
 */
public interface ILfZdryControlPcReceiveService extends IService<LfZdryControlPcReceiveEntity> {

}
