package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.ContactPoliceRoomEntity;
import com.mti.mapper.ContactPoliceRoomMapper;
import com.mti.service.IContactPoliceRoomService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  警务室巡区信息 服务实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-15
 */
@Service
public class ContactPoliceRoomImpl extends ServiceImpl<ContactPoliceRoomMapper, ContactPoliceRoomEntity> implements IContactPoliceRoomService {

}
