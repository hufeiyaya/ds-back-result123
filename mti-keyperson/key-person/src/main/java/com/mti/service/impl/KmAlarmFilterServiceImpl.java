package com.mti.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.component.snowflake.KeyWorker;
import com.mti.constant.AlarmFilterEnum;
import com.mti.dao.model.KmAlarmAreaEntity;
import com.mti.dao.model.KmAlarmTypeEntity;
import com.mti.exception.BusinessException;
import com.mti.mapper.KmAlarmTypeMapper;
import com.mti.service.KmAlarmAreaService;
import com.mti.service.KmAlarmTypeService;
import com.mti.vo.KmAlarmVo;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 手机预警和人脸识别预警设置过滤 服务实现类
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */
@Slf4j
@Service
public class KmAlarmFilterServiceImpl extends ServiceImpl<KmAlarmTypeMapper, KmAlarmTypeEntity> implements KmAlarmTypeService {
    @Autowired
    private KmAlarmAreaService kmAlarmAreaService;

    /**
     *  默认的数据只有人脸识别和手机预警两个
     */
    private static final int ALARM_FILTER_SIZE =2;

    @Override
    public KmAlarmTypeEntity getAlarmStatusByName(String name) {
        KmAlarmTypeEntity kmAlarmTypeEntity = this.getOne(new QueryWrapper<KmAlarmTypeEntity>().eq("name", name));
        if (ObjectUtils.isEmpty(kmAlarmTypeEntity)) {
            KmAlarmTypeEntity entity = new KmAlarmTypeEntity();
            entity.setId(String.valueOf(KeyWorker.nextId()));
            entity.setName(name);
            entity.setStatus(false);
            this.save(entity);
        }
        return this.getOne(new QueryWrapper<KmAlarmTypeEntity>().eq("name", name));
    }

    @Override
    public Object getAlarmStatus() {
        List<KmAlarmTypeEntity> list = this.list();
        //如果结果为空则插入默认数据
        if (Collections.isEmpty(list) || list.size() != ALARM_FILTER_SIZE) {
            //清空表
            this.remove(new QueryWrapper<>());
            KmAlarmTypeEntity rlsb = new KmAlarmTypeEntity();
            rlsb.setId(String.valueOf(KeyWorker.nextId()));
            rlsb.setName(AlarmFilterEnum.RLSB_SWITCH.getName());
            rlsb.setStatus(false);
            KmAlarmTypeEntity phyj = new KmAlarmTypeEntity();
            phyj.setId(String.valueOf(KeyWorker.nextId()));
            phyj.setName(AlarmFilterEnum.PHONE_SWITCH.getName());
            phyj.setStatus(false);
            this.saveBatch(Arrays.asList(rlsb, phyj));
        }
        KmAlarmVo kmAlarmVo = new KmAlarmVo();
        list.forEach(e -> {
            if (AlarmFilterEnum.RLSB_SWITCH.getName().equals(e.getName())) {
                kmAlarmVo.setRlsb(e.isStatus());
            }
            if (AlarmFilterEnum.PHONE_SWITCH.getName().equals(e.getName())) {
                kmAlarmVo.setPhyj(e.isStatus());
            }
            List<KmAlarmAreaEntity> areaList = kmAlarmAreaService.list();
            kmAlarmVo.setAreaList(areaList);
        });
        return kmAlarmVo;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public void setAlarmStatus(KmAlarmVo kmAlarmVo) {
        try {
            UpdateWrapper<KmAlarmTypeEntity> rlsb = new UpdateWrapper<KmAlarmTypeEntity>();
            rlsb.eq("name", AlarmFilterEnum.RLSB_SWITCH.getName());
            rlsb.set("status", kmAlarmVo.isRlsb());
            this.update(rlsb);
            UpdateWrapper<KmAlarmTypeEntity> phyj = new UpdateWrapper<KmAlarmTypeEntity>();
            phyj.eq("name", AlarmFilterEnum.PHONE_SWITCH.getName());
            phyj.set("status", kmAlarmVo.isPhyj());
            this.update(phyj);
            if (!Collections.isEmpty(kmAlarmVo.getAreaList())) {
                kmAlarmAreaService.remove(new QueryWrapper<>());
                kmAlarmAreaService.saveBatch(kmAlarmVo.getAreaList());
            }
        } catch (Exception e) {
            throw new BusinessException(10020, "设置预警过滤信息失败");
        }
    }
}


