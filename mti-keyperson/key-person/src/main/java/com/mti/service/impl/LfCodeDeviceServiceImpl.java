package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfCodeDeviceEntity;
import com.mti.mapper.LfCodeDeviceMapper;
import com.mti.service.ILfCodeDeviceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Service
public class LfCodeDeviceServiceImpl extends ServiceImpl<LfCodeDeviceMapper, LfCodeDeviceEntity> implements ILfCodeDeviceService {

}
