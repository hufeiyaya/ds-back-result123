package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.EventEntity;
import com.mti.mapper.EventMapper;
import com.mti.service.IEventService;
import org.springframework.stereotype.Service;

@Service
public class EventServiceImpl extends ServiceImpl<EventMapper,EventEntity> implements IEventService {
}
