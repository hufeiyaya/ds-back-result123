package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZdryApproveLogEntity;

public interface ILfZdryApproveLogService extends IService<LfZdryApproveLogEntity> {
	/**
	 * 通过重点人员ID更新最后一次审核标记
	 * @param zdryId
	 * @return
	 */
	Boolean updateFlagByZdryId(String zdryId);
	/**
	 * 重写save方法
	 * @param LogEntity
	 * @return
	 */
	public Boolean saveLogEntity(LfZdryApproveLogEntity LogEntity) ;
}
