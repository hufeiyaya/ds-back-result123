package com.mti.service.impl;

import com.mti.dao.model.LfZdryExportDataEntity;
import com.mti.mapper.LfZdryExportDataMapper;
import com.mti.service.LfZdryExportDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.utils.ListUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author duchao
 * @since 2020-04-05
 */
@Slf4j
@Service
public class LfZdryExportDataServiceImpl extends ServiceImpl<LfZdryExportDataMapper, LfZdryExportDataEntity> implements LfZdryExportDataService {

    @Override
    public void deleteByVersion(String version) {
        this.getBaseMapper().deleteByVersion(version);
    }

    @Override
    public boolean batchSaveZdry(List<LfZdryExportDataEntity> tempData) {
        try {
            List<List<LfZdryExportDataEntity>> data = ListUtils.splitList(tempData,500);
            for(List<LfZdryExportDataEntity> zdryList : data){
                 this.getBaseMapper().batchSaveZdry(zdryList);
            }
            return true;
        } catch (Exception e) {
            log.error("批量添加重点人员时发生异常======》{}",e.getMessage());
            return false;
        }
    }

    @Override
    public void updateExportDataSign(String sfzh) {
        this.getBaseMapper().updateExportDataSign(sfzh);
    }
}
