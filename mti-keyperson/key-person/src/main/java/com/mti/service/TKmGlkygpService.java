package com.mti.service;


import com.mti.dao.model.TKmGlkygpEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.utils.ZdryQxzkUtilBean;

import java.util.List;
import java.util.Map;

/**
 * @Classname TKmGlkygpService
 * @Description TODO
 * @Date 2020/1/17 23:15
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmGlkygpService {
    Integer insert(TKmGlkygpEntity tKmGlkygpEntity);

    TKmGlkygpEntity getByGlId(String glId);

    List<TKmGlkygpEntity> getGlkygpBySfzh(String sfzf);

    /**
     * 客运出昆明
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getKyOut(LfRyyjsjQO lfRyyjsjQO);

    /**
     * 客运进昆明
     * @param lfRyyjsjQO
     * @return
     */
    List<ZdryQxzkUtilBean> getKyIn(LfRyyjsjQO lfRyyjsjQO);

    /**
     * 获取公路客运订票数据
     * @param glkyEntities
     * @return
     */
    List<TKmGlkygpEntity> getGlkygpList(List<String> glkyEntities);
}
