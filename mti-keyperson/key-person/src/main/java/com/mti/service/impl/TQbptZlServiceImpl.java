package com.mti.service.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfRyyjsjEntity;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.TQbptZlEntity;
import com.mti.dao.qo.TQbptQO;
import com.mti.mapper.TQbptZlMapper;
import com.mti.service.ILfZdryService;
import com.mti.service.ITQbptZlService;
import com.mti.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yangsp
 * @since 2019-06-25
 */
@Service
public class TQbptZlServiceImpl extends ServiceImpl<TQbptZlMapper, TQbptZlEntity> implements ITQbptZlService {

    @Resource
    private TQbptZlMapper mapper;

    @Autowired
    private ILfZdryService service;

    public IPage<TQbptZlEntity> queryPage(TQbptQO qo) {
        Page<TQbptZlEntity> page = new Page<TQbptZlEntity>(qo.getStart(), qo.getSize());
        return mapper.queryPage(page,qo);
    }

    public int doData() {

        LambdaQueryWrapper<TQbptZlEntity> wrapper = new QueryWrapper<TQbptZlEntity>().lambda();
        wrapper = wrapper.isNull(TQbptZlEntity::getZlbt).or().eq(TQbptZlEntity::getZlbt,"");
        List<TQbptZlEntity> entitys =  this.list(wrapper);
        entitys.forEach(item -> {
            String data = item.getData();
            if (StringUtils.isNotBlank(data)) {
                JSONObject jSONObject = JSON.parseObject(data);
                String info = jSONObject.getString("info");
                if (StringUtils.isNotBlank(info)) {
                    JSONObject jSONObjectInfo = JSON.parseObject(info);
                    String bt = jSONObjectInfo.getString("bt");
                    String xfrq = jSONObjectInfo.getString("xfrq");

                    item.setZlbt(bt);
                    item.setZlsj(DateUtils.stampToDate2(xfrq));
                    item.setZllx("1");
                }

                String personList = jSONObject.getString("personList");
                if (StringUtils.isNotBlank(personList)) {
                    JSONArray jSONArray = JSON.parseArray(personList);
                    if (jSONArray.size() > 0) {
                        for (int i = 0; i < jSONArray.size(); i++) {
                            JSONObject job = jSONArray.getJSONObject(i);
                            String sfz = job.getString("sfz");
                            if (StringUtils.isNotBlank(sfz)) {
                                LfZdryEntity zdryEntity = service.getBySfzhV2(sfz);
                                if (zdryEntity == null) {
                                    item.setIs_zdry("0");
                                } else {
                                    item.setIs_zdry("1");
                                }
                            }
                        }
                    }
                }
                if (item.getId() != null){
                    this.saveOrUpdate(item);
                }
            }
        });

        return 0;
    }

}
