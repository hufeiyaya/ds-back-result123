package com.mti.service;

import com.mti.dao.dto.LfZdryDto;
import com.mti.dao.dto.ZdryDto;
import com.mti.dao.kmmodel.KmZdryDto;
import com.mti.dao.model.LfZdryBaseEntity;
import com.mti.dao.model.LfZdryBaseTempEntity;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.qo.LfZdryQO;

import java.util.List;
import java.util.Map;

import org.springframework.web.server.ServerWebExchange;

/**
 * <p>
 * 重点人员表 服务类
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */

public interface LfZdryBaseTempService extends IService<LfZdryBaseTempEntity> {

}