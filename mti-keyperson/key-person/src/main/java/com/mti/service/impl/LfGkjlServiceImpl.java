/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfGkjlEntity;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.LfZdryHistoryEntity;
import com.mti.mapper.LfGkjlMapper;
import com.mti.mapper.LfRyyjsjMapper;
import com.mti.service.IContactCardPoliceForceService;
import com.mti.service.ILfGkjlService;
import com.mti.service.ILfZdryHistoryService;
import com.mti.service.ILfZdryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Service
public class LfGkjlServiceImpl extends ServiceImpl<LfGkjlMapper, LfGkjlEntity> implements ILfGkjlService {

    @Resource
    private LfGkjlMapper mapper;

    @Autowired
    ILfZdryService iLfZdryService;

    @Autowired
    ILfZdryHistoryService iLfZdryHistoryService;

    @Autowired
    SpecialPersonComponent specialPersonComponent;


    public boolean saveOrUpdateGkjl(LfGkjlEntity entity) {
        boolean flag = false;
        if (entity.getId() == null){
            entity.setCreate_time(new Date());
            entity.setUpdate_time(new Date());
            flag = this.saveOrUpdate(entity);
            if (StringUtils.isNotBlank(entity.getLxdl()) && entity.getLxdl().equals("1")) {
                this.upZksk(entity.getSfzh(), entity.getSfzk());
            }
        } else {
            entity.setUpdate_time(new Date());
            flag = this.saveOrUpdate(entity);
            if (StringUtils.isNotBlank(entity.getLxdl()) && entity.getLxdl().equals("1")) {
                this.upZksk(entity.getSfzh(), entity.getSfzk());
            }
        }
        return flag;
    }

    @Override
    public void upZksk(String sfzh, String sfzk) {
        LfZdryEntity lfZdryEntity = iLfZdryService.getBySfzhV2(sfzh);
        LfZdryHistoryEntity lfZdryHistoryEntity = iLfZdryHistoryService.getEntityBySfzh(sfzh, LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        if (lfZdryEntity != null && StringUtils.isNotBlank(lfZdryEntity.getId())) {
            if (StringUtils.isNotBlank(sfzk) && sfzk.equals("0")) {
                lfZdryEntity.setSfzk("失控");
            } else if (StringUtils.isNotBlank(sfzk) && sfzk.equals("1")) {
                lfZdryEntity.setSfzk("在控");
            } else {
            }
            iLfZdryService.updateById(lfZdryEntity);
        }
        if (lfZdryHistoryEntity != null && StringUtils.isNotBlank(lfZdryHistoryEntity.getId())) {
            if (StringUtils.isNotBlank(sfzk) && sfzk.equals("0")) {
                lfZdryHistoryEntity.setIsControl("失控");
                //添加一条event记录，并且发送websocket预警
            } else if (StringUtils.isNotBlank(sfzk) && sfzk.equals("1")) {
                lfZdryHistoryEntity.setIsControl("在控");
            } else {
            }
            iLfZdryHistoryService.updateById(lfZdryHistoryEntity);
        }
        if(lfZdryEntity!=null && "0".equalsIgnoreCase(sfzk))
            specialPersonComponent.insertEvent(lfZdryEntity);
    }

    public List<LfGkjlEntity> queryListBySfzh(String sfzh) {
        return mapper.queryListBySfzh(sfzh);
    }

    @Override
    public Map<String, String> getIsControlByIdentityIds(List<String> ids) {
        Map<String,String> map = new HashMap<>();
        if(ids!=null && !ids.isEmpty()){
            List<Map<String,String>> mapList = mapper.getIsControlByIdentityIds(ids);
            mapList.forEach(item->map.put(item.get("sfzh"),item.get("sfzk")));
        }
        return map;
    }
}
