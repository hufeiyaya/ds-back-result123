package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.SysTestEntity;
import com.mti.mapper.SysTestMapper;
import com.mti.service.ISysTestService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Service
public class SysTestServiceImpl extends ServiceImpl<SysTestMapper, SysTestEntity> implements ISysTestService {

}
