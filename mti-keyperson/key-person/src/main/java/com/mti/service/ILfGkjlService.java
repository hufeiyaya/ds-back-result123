/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfGkjlEntity;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface ILfGkjlService extends IService<LfGkjlEntity> {

    List<LfGkjlEntity> queryListBySfzh(String sfzh);

    boolean saveOrUpdateGkjl(LfGkjlEntity entity);

    /**
     * 通过身份证号获取是否在控
     * @param ids 身份证号集合
     * @return 是否在控列表
     */
    Map<String, String> getIsControlByIdentityIds(List<String> ids);

    void upZksk(String sfzh,String sfzk);
}
