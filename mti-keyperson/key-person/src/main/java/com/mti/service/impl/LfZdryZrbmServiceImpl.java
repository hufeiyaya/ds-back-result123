package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfZdryZrbmEntity;
import com.mti.mapper.LfZdryZrbmMapper;
import com.mti.service.ILfZdryZrbmService;
import org.springframework.stereotype.Service;

@Service
public class LfZdryZrbmServiceImpl extends ServiceImpl<LfZdryZrbmMapper, LfZdryZrbmEntity> implements ILfZdryZrbmService {
}
