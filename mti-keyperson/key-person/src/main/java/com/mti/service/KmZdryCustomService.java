package com.mti.service;

import com.mti.dao.model.KmZdryCustomEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 重点人员-自定义字段表 服务类
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
public interface KmZdryCustomService extends IService<KmZdryCustomEntity> {

}
