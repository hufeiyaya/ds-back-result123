package com.mti.service;

import org.springframework.web.server.ServerWebExchange;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZdryBaseEntity;

public interface IKmZdryFollowService extends IService<LfZdryBaseEntity> {

    void  follow(String personId,String masterId,String creator,ServerWebExchange exchange);

    void unFollow(String personId,String masterId);
}
