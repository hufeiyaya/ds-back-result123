package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZtryEntity;

public interface IZtryService extends IService<LfZtryEntity> {
    LfZtryEntity getBySfzh(String sfzh);
}
