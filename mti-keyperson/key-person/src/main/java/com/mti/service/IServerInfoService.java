package com.mti.service;

import com.mti.dao.sipcofig.ServerInfoDto;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lil
 * @since 2019-03-19
 */
public interface IServerInfoService  {

    ServerInfoDto getServerInfo(String deviceId);
}
