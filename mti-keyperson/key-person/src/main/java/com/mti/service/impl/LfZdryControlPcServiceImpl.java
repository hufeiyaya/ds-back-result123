package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfZdryControlPcEntity;
import com.mti.mapper.*;
import com.mti.service.ILfZdryControlPcService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  重点人员布控pc信息服务实现类
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-26
 */
@Service
public class LfZdryControlPcServiceImpl extends ServiceImpl<LfZdryControlPcMapper, LfZdryControlPcEntity> implements ILfZdryControlPcService {

}
