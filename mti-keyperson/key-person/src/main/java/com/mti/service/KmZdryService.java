package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.kmmodel.KmZdryDto;

/**
 * @Classname KmZdryService
 * @Description TODO
 * @Date 2020/1/10 22:30
 * @Created by duchaof
 * @Version 1.0
 */
public interface KmZdryService extends IService<KmZdryDto> {
}
