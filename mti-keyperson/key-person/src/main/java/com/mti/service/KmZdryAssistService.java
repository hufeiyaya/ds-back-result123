package com.mti.service;

import com.mti.dao.model.KmZdryAssistEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 重点人员-辅助信息表 服务类
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
public interface KmZdryAssistService extends IService<KmZdryAssistEntity> {

}
