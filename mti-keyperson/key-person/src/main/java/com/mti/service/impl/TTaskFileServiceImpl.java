package com.mti.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.SysDictEntity;
import com.mti.dao.model.TTaskFileEntity;
import com.mti.mapper.TTaskFileMapper;
import com.mti.service.TTaskFileService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author duchaof
 * @since 2020-03-11
 */
@Service
public class TTaskFileServiceImpl extends ServiceImpl<TTaskFileMapper, TTaskFileEntity> implements TTaskFileService {
	@Override
	public List<TTaskFileEntity> 	getByBussId(String bussId){
		QueryWrapper<TTaskFileEntity> wrapper = new QueryWrapper<>();
		wrapper.eq("buss_id", bussId);
		return this.list(wrapper);
		
	}
	@Override
	public  boolean	removeByZdryId(String zdryId){
		QueryWrapper<TTaskFileEntity> wrapper = new QueryWrapper<>();
		wrapper.eq("buss_id", zdryId);
		return this.remove(wrapper);
		
	}
	
	
}
