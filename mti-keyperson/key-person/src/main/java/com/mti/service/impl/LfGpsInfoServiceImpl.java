package com.mti.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfGpsInfoEntity;
import com.mti.dao.model.LfRoadmonitorEntity;
import com.mti.dao.qo.LfGpsInfoQO;
import com.mti.mapper.LfGpsInfoMapper;
import com.mti.service.ILfGpsInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Service
public class LfGpsInfoServiceImpl extends ServiceImpl<LfGpsInfoMapper, LfGpsInfoEntity> implements ILfGpsInfoService {

    @Resource
    private LfGpsInfoMapper mapper;

    public List<LfGpsInfoEntity> queryList() {
        List<LfRoadmonitorEntity> queryList = new ArrayList<>();
        QueryWrapper<LfGpsInfoEntity> queryWrapper = new QueryWrapper<>();
        return this.list(queryWrapper);
    }

    public IPage<LfGpsInfoEntity> queryPage(LfGpsInfoQO qo) {
        Page<LfGpsInfoEntity> page = new Page<LfGpsInfoEntity>(qo.getStart(), qo.getSize());
        return mapper.queryPage(page,qo);
    }

}
