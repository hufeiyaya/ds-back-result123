package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfZdryModifyLogEntity;
import com.mti.mapper.LfZdryModifyLogMapper;
import com.mti.service.ILfZdryModifyLogService;
import org.springframework.stereotype.Service;

@Service
public class LfZdryModifyLogServiceImpl extends ServiceImpl<LfZdryModifyLogMapper, LfZdryModifyLogEntity> implements ILfZdryModifyLogService {

}
