package com.mti.service;

import com.mti.dao.model.KmPhYjEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 手机预警 服务类
 * </p>
 *
 * @author duchao
 * @since 2020-04-29
 */
public interface KmPhYjService extends IService<KmPhYjEntity> {

    /**
     * 查询最新定位接口
     * @return
     */
    List<Map<String,Object>> queryPhYjaLastLocation();
}
