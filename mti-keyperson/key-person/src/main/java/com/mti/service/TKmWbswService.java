package com.mti.service;


import com.mti.dao.model.TKmWbswEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Classname TKmWbswService
 * @Description TODO
 * @Date 2020/1/17 23:22
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmWbswService {
    Integer insert(TKmWbswEntity tKmWbswEntity);

    TKmWbswEntity getByGlId(String glId);

    List<TKmWbswEntity> getWbswListBySfzh(String sfzf);

    /**
     * 获取网吧撒点信息
     * @param lfRyyjsjQO
     * @return
     */
    List<Map<String, Object>> queryWbLgKyNumber(LfRyyjsjQO lfRyyjsjQO);

    /**
     * 获取网吧信息
     * @param wbswEntities
     * @return
     */
    List<TKmWbswEntity> getWbswList(List<String> wbswEntities);

    /**
     * 查询定位信息的详情
     * @param sfzhs
     * @return
     */
    List<Map<String, Object>> queryWbswLocationInfo(List<String> sfzhs);

    /**
     * 查询网吧的登录和退出时间
     * @param wybs
     * @return
     */
    Map<String, Object> findWbswStartAndEndTime(String wybs);
}
