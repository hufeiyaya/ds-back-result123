package com.mti.service;

import com.mti.dao.dto.OrganizationDto;

import java.util.List;
import java.util.Map;

/**
 * @Classname CacheableService
 * @Description TODO
 * @Date 2020/2/8 16:15
 * @Created by duchaof
 * @Version 1.0
 */
public interface CacheableService {

     Map<String,String> getZrpcs(String orgType);

     Map<String,String> getAllRegionInfo(String arg);

      void deleteAllCacheale();

     Map<String,Object> getOrganization(String shortName,String ssxqId);

     Map<String,Object> getZrmjInfo(String mjxm);

     List<String> getAllGkzrfj(String orgType);

     List<String> getRegionByLevel(Integer level);

     List<OrganizationDto> getUpOrgById(String regionId);

}
