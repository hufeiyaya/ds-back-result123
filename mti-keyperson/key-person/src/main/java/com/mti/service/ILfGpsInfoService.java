package com.mti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfGpsInfoEntity;
import com.mti.dao.qo.LfGpsInfoQO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface ILfGpsInfoService extends IService<LfGpsInfoEntity> {

    List<LfGpsInfoEntity> queryList();

    IPage<LfGpsInfoEntity> queryPage(LfGpsInfoQO qo);

}
