package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZdryHistoryEntity;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface ILfZdryHistoryService extends IService<LfZdryHistoryEntity> {

    int insertHistoryByBatch(List<LfZdryHistoryEntity> list);

    List<HashMap<String,String>> listIdsHistoryByVersion(@Param(value = "version")String version, @Param(value = "ids")List<String> ids);

    LfZdryHistoryEntity getEntityBySfzh(String sfzh, String version);

    void SyncHistoryByVersion(String version);
}
