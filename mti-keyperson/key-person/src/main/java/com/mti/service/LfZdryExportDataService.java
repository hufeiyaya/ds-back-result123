package com.mti.service;

import com.mti.dao.model.LfZdryExportDataEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author duchao
 * @since 2020-04-05
 */
public interface LfZdryExportDataService extends IService<LfZdryExportDataEntity> {
    /**
     * 导入原始数据时发现异常，根据版本号全本删除
     * @param version
     */
    void deleteByVersion(String version);

    /**
     * 批量保存原始数据
     * @param tempData
     * @return
     */
    boolean batchSaveZdry(List<LfZdryExportDataEntity> tempData);

    /**
     * 跟新操作标志
     */
    void updateExportDataSign(String sfzh);
}
