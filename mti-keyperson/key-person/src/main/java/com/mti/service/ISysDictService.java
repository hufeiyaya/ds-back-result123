package com.mti.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.web.server.ServerWebExchange;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.SysDictEntity;
import com.mti.dao.qo.TypeQO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface ISysDictService extends IService<SysDictEntity> {

    /**
     *  通过类型获取数据字典
     * @param type
     *     重点人员分类	kp_category
     *     重点人员负责部门	kp_department
     *     重点时期	kp_period
     *     重点人员管控状态	kp_control_state
     *     重点人员管控手段	kp_control_type
     *     重点人员级别	kp_level
     *     重点人员报警方式	kp_alarm_mode
     *     辖区	kp_area
     * @return
     */
     List<SysDictEntity> getByType(String type);

     String getByTypeRedis(String type);
    /**
     * 通过警种查询大类小类
     * @param typeQo
     * @return
     */
    public List<SysDictEntity> getType(ServerWebExchange exchange,TypeQO typeQo);

    public SysDictEntity getEntityByKey(String type, String key);

    List<String> getOrgByOid(@Param(value = "oids")List<String> oids);

    SysDictEntity getDictByTypeAndVal(String type,String key);

    String getOidByOname(String oname);

    Map<String,String> getPersonInfoByMjid(String mjid);

    public SysDictEntity queryEntityByKey(String key);

    public SysDictEntity queryEntityById(String id);

    List<SysDictEntity> queryAllRyxles();
    /**
     * 通过关键字查询检索字段
     * @param exchange
     * @param keyword
     * @return
     */
	public List<Object> queryKeyword(ServerWebExchange exchange, String keyword);
	/**
	   * 通过关键字查询预警模块检索字段
     * @param exchange
     * @param keyword
     * @return
     */
	List<Object> queryWarnKeyword(ServerWebExchange exchange, String keyword);

}
