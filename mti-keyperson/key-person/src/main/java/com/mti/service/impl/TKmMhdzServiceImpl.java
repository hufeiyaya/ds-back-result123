package com.mti.service.impl;

import com.mti.dao.model.TKmMhdzEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.mapper.TKmMhdzEntityMapper;
import com.mti.service.TKmMhdzService;
import com.mti.utils.ZdryQxzkUtilBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Classname TKmMhdzServiceImpl
 * @Description TODO
 * @Date 2020/1/17 23:20
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmMhdzServiceImpl implements TKmMhdzService {
    @Resource
    private TKmMhdzEntityMapper tKmMhdzEntityMapper;

    @Override
    public Integer insert(TKmMhdzEntity tKmMhdzEntity) {
        return tKmMhdzEntityMapper.insert(tKmMhdzEntity);
    }

    @Override
    public TKmMhdzEntity getByGlId(String glId) {
        return tKmMhdzEntityMapper.selectByPrimaryKey(glId);
    }

    @Override
    public List<TKmMhdzEntity> getMhdzListBySfzh(String sfzf) {
        return tKmMhdzEntityMapper.getMhdzListBySfzh(sfzf);
    }



    @Override
    public List<ZdryQxzkUtilBean> getMhOut(LfRyyjsjQO lfRyyjsjQO) {
        return tKmMhdzEntityMapper.getMhOut(lfRyyjsjQO);
    }

    @Override
    public List<ZdryQxzkUtilBean> getMhIn(LfRyyjsjQO lfRyyjsjQO) {
        return tKmMhdzEntityMapper.getMhIn(lfRyyjsjQO);
    }

    @Override
    public List<TKmMhdzEntity> getMhdzList(List<String> mhdzEntities) {
        return tKmMhdzEntityMapper.getMhdzList(mhdzEntities);
    }
}
