package com.mti.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfRoadmonitorEntity;
import com.mti.mapper.LfRoadmonitorMapper;
import com.mti.service.ILfRoadmonitorService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Service
public class LfRoadmonitorServiceImpl extends ServiceImpl<LfRoadmonitorMapper, LfRoadmonitorEntity> implements ILfRoadmonitorService {

    public List<LfRoadmonitorEntity> queryList(String cid) {

        System.out.println("LfRoadmonitorServiceImpl=cid=>" + cid);

//        AccountBo user = userUtil.getCurrentUser();
        List<LfRoadmonitorEntity> queryList = new ArrayList<>();
        QueryWrapper<LfRoadmonitorEntity> queryWrapper = new QueryWrapper<>();
        // 关键字
        if(StringUtils.isNotBlank(cid) ){
            if(cid.equals("405") || cid.equals("406") || cid.equals("407") || cid.equals("408")
                    || cid.equals("257") || cid.equals("401") || cid.equals("402") || cid.equals("403") ){
                System.out.println("LfRoadmonitorServiceImpl=cid2=>" + cid);
                queryWrapper.eq("layerid", Integer.parseInt(cid));
                queryWrapper.orderByDesc("id");
                queryList = this.list(queryWrapper);
            } else {
                if(cid.equals("1")) {
                    // 卡口设备数据 layerid:405\406\407\408
                    queryWrapper.in("layerid", new Integer[]{405,406,407,408});
                    queryWrapper.orderByDesc("id");
                    queryList = this.list(queryWrapper);
                }else if(cid.equals("2")) {
                    // 视频点位数据 layerid:257\401\402\403
                    queryWrapper.in("layerid", new Integer[]{257,401,402,403});
                    queryWrapper.orderByDesc("id");
                    queryList = this.list(queryWrapper);
                }else if(cid.equals("3")) {
                    // 卡口设备数据 layerid:405\406\407\408 视频点位数据 layerid:257\401\402\403
                    queryWrapper.in("layerid", new Integer[]{405,406,407,408,257,401,402,403});
                    queryWrapper.orderByDesc("id");
                    queryList = this.list(queryWrapper);
                }
            }
        }
        return queryList;
    }
}
