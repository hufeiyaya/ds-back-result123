package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfGpsGisEntity;
import com.mti.dao.model.LfGpsHistoryEntity;
import com.mti.dao.model.LfGpsInfoEntity;
import com.mti.mapper.LfGpsGisMapper;
import com.mti.mapper.LfGpsHistoryMapper;
import com.mti.mapper.LfGpsInfoMapper;
import com.mti.service.ILfGpsGisService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Service
@Slf4j
public class LfGpsGisServiceImpl extends ServiceImpl<LfGpsGisMapper, LfGpsGisEntity> implements ILfGpsGisService {

    @Resource
    private LfGpsGisMapper mapper;

    @Resource
    private LfGpsHistoryMapper lfGpsHistoryMapper;

    @Resource
    private LfGpsInfoMapper lfGpsInfoMapper;

    public int saveByHistory(String date) {

//        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.MINUTE, -5);// 5分钟之前的时间
//        Date beforeD = calendar.getTime();
//        String before5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(beforeD);  // 前五分钟时间
//        String day = new SimpleDateFormat("yyyy_MM_dd").format(calendar.getTime());
//        String tablename = "ga_gps_history_"+day;
//        System.out.println("tablename==>" + tablename);
//        List<LfGpsHistoryEntity> entitys = lfGpsHistoryMapper.getGpsHistory(tablename, before5);
//        System.out.println("entitys==>" + entitys.size());

        String tablename = "ga_gps_history_2019_05_06";
        String before5 = "2019-05-05 00:00:00";
        List<LfGpsHistoryEntity> entitys = lfGpsHistoryMapper.getGpsHistory(tablename, before5);

        System.out.println("entitys==>" + entitys.size());

        entitys.forEach(item->{
            LfGpsGisEntity lfGpsGisEntity = mapper.getByGpsid(item.getGpsid());
            if(lfGpsGisEntity == null ){
                lfGpsGisEntity = new LfGpsGisEntity();
                lfGpsGisEntity.setMobileid(item.getMobileid());
                lfGpsGisEntity.setGpsid(item.getGpsid());
                lfGpsGisEntity.setTime(item.getTime());
                lfGpsGisEntity.setDir(item.getDir());
                lfGpsGisEntity.setSpeed(item.getSpeed());
                lfGpsGisEntity.setX(item.getX());
                lfGpsGisEntity.setY(item.getY());
                System.out.println("save==>" + item.getGpsid());

                if(item.getGpsid() != null && item.getGpsid() != ""){

                    LfGpsInfoEntity lfGpsInfoEntity = lfGpsInfoMapper.getByGpsid(item.getGpsid());
                    if(lfGpsInfoEntity != null) {
                        lfGpsGisEntity.setCallno(lfGpsInfoEntity.getCallno());
                        lfGpsGisEntity.setUim(lfGpsInfoEntity.getUim());
                    }

                    if(item.getGpsid().startsWith("34")) {
                        // 警务通
                        lfGpsGisEntity.setType("2");
                    } else {
                        if(lfGpsInfoEntity != null) {
                            String callno = lfGpsInfoEntity.getCallno();
                            if(callno.startsWith("冀R") && !item.getGpsid().startsWith("34")) {
                                // 警车
                                lfGpsGisEntity.setType("1");
                            } else if(StringUtils.isNumeric(callno.substring(0,5)) ){
                                // PDT 对讲
                                lfGpsGisEntity.setType("3");
                            }
                        }
                    }
                }
                mapper.saveByEntity(lfGpsGisEntity);
            }else {
                System.out.println("update==>" + item.getGpsid());
                lfGpsGisEntity.setGpsid(item.getGpsid());
                lfGpsGisEntity.setTime(item.getTime());
                lfGpsGisEntity.setX(item.getX());
                lfGpsGisEntity.setY(item.getY());
                mapper.updateByEntity(lfGpsGisEntity);
            }
        });

        return 0;
    }
}
