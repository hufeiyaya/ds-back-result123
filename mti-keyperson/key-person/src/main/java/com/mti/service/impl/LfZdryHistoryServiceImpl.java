package com.mti.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.component.snowflake.KeyWorker;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.LfZdryHistoryEntity;
import com.mti.mapper.LfZdryHistoryMapper;
import com.mti.service.ILfZdryHistoryService;
import com.mti.service.ILfZdryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Service
public class LfZdryHistoryServiceImpl extends ServiceImpl<LfZdryHistoryMapper, LfZdryHistoryEntity> implements ILfZdryHistoryService {

    @Resource
    private LfZdryHistoryMapper mapper;

    @Autowired
    private ILfZdryService iLfZdryService;

    public int insertHistoryByBatch(List<LfZdryHistoryEntity> list) {
        return mapper.insertHistoryByBatch(list);
    }

    public List<HashMap<String,String>> listIdsHistoryByVersion(String version, List<String> ids) {
        return listIdsHistoryByVersion(version, ids);
    }

    public LfZdryHistoryEntity getEntityBySfzh(String sfzh, String version) {
        QueryWrapper<LfZdryHistoryEntity> queryWrapper = new QueryWrapper<LfZdryHistoryEntity>();
        queryWrapper.eq("identity_id", sfzh);
        queryWrapper.eq("record_version", version);
        List<LfZdryHistoryEntity> lfZdryHistoryEntityList = this.list(queryWrapper);
        if (lfZdryHistoryEntityList != null && lfZdryHistoryEntityList.size() > 0) {
            return lfZdryHistoryEntityList.get(0);
        } else {
            return null;
        }
    }

    public void SyncHistoryByVersion(String version) {
        List<LfZdryEntity> lfZdryEntityList = iLfZdryService.list();
        if (lfZdryEntityList != null && lfZdryEntityList.size() > 0) {
            for (LfZdryEntity lfZdryEntity : lfZdryEntityList) {
                LfZdryHistoryEntity entity = new LfZdryHistoryEntity();
                entity.setId(String.valueOf(KeyWorker.nextId()));
                entity.setName(lfZdryEntity.getXm());
                entity.setIdentityId(lfZdryEntity.getSfzh());
                entity.setResourceLocation(lfZdryEntity.getHjd());
                entity.setOwnCarType("暂无");
                entity.setControlDep(lfZdryEntity.getZrdw());
                entity.setControlPolice(lfZdryEntity.getGkmj());
                entity.setIsControl("未上报");
                entity.setPersonType(lfZdryEntity.getRylbx());
                entity.setRecordVersion(version);
                entity.setOwnRange(lfZdryEntity.getSsxq());
                entity.setOwnRangeId(lfZdryEntity.getSsxqId());
                entity.setPoliceHomeId(lfZdryEntity.getOid());
                entity.setZrbm(lfZdryEntity.getZrbm());
                LfZdryHistoryEntity lfZdryHistoryEntity = getEntityBySfzh(lfZdryEntity.getSfzh(), version);
                if (lfZdryHistoryEntity == null) {
                    this.save(entity);
                }
            }
        }
    }

}
