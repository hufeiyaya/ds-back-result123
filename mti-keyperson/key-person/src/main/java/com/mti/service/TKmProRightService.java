package com.mti.service;

import org.springframework.web.server.ServerWebExchange;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.TKmProEntity;
import com.mti.dao.qo.TkmProRiQo;

/**
 * @Classname TKmProRightService
 * @Description TODO
 * @Date 2020/2/20 16:53
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmProRightService extends IService<TKmProEntity> {
    /**
     * 查询信访/聚访历史信息，不分页
     * @param tkmProRiQo
     * @return
     */
    Object queryProRightRecordList(TkmProRiQo tkmProRiQo);

    /**
     * 查询信访/聚访历史信息，分页
     * @param tkmProRiQo
     * @return
     */
    IPage<TKmProEntity> queryProRightRecordPage(TkmProRiQo tkmProRiQo);
	/**
	 * 保存信访记录
	 * @return
	 */
	boolean add(ServerWebExchange exchange,TKmProEntity tKmProEntity);
}
