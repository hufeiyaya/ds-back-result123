package com.mti.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.RylbRelationEntity;
import com.mti.mapper.RylbRelationMapper;
import com.mti.service.IRylbRelationService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chenf
 * @since 2019-04-29
 */
@Service
public class RylbRelationServiceImpl extends ServiceImpl<RylbRelationMapper, RylbRelationEntity> implements IRylbRelationService {

}
