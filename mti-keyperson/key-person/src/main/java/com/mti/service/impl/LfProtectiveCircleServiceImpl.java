package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfProtectiveCircleEntity;
import com.mti.mapper.LfProtectiveCircleMapper;
import com.mti.service.ILfProtectiveCircleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Service
public class LfProtectiveCircleServiceImpl extends ServiceImpl<LfProtectiveCircleMapper, LfProtectiveCircleEntity> implements ILfProtectiveCircleService {

}
