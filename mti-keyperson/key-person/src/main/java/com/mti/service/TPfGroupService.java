package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.TPfGroupEntity;
import org.springframework.web.server.ServerWebExchange;

import java.util.List;

/**
 * <p>
 * 重点人员分组表 服务类
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */

public interface TPfGroupService extends IService<TPfGroupEntity> {

    List<TPfGroupEntity> getGroupInfoByAccId(ServerWebExchange exchange);

    boolean add(ServerWebExchange exchange, TPfGroupEntity tPfGroupEntity);

    boolean update(ServerWebExchange exchange, TPfGroupEntity tPfGroupEntity);
}