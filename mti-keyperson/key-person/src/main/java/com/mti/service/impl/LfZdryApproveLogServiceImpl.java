package com.mti.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfZdryApproveLogEntity;
import com.mti.mapper.LfZdryApproveLogMapper;
import com.mti.service.ILfZdryApproveLogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
@Slf4j
@Service
@AllArgsConstructor
public class LfZdryApproveLogServiceImpl extends ServiceImpl<LfZdryApproveLogMapper, LfZdryApproveLogEntity> implements ILfZdryApproveLogService {@Override
	public Boolean updateFlagByZdryId(String zdryId) {
		UpdateWrapper<LfZdryApproveLogEntity> wrapper = new UpdateWrapper<LfZdryApproveLogEntity>();
		wrapper.eq("sp_id", zdryId);
		wrapper.set("approving_flag","1"); //诉求字段
		return this.update(wrapper);
	}

	public Boolean saveLogEntity(LfZdryApproveLogEntity LogEntity) {
		Integer result = this.baseMapper.insert(LogEntity);
//		log.error("=======result:"+result);
	return this.retBool(result);
}
}
