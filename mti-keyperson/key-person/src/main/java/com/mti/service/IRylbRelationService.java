package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.RylbRelationEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenf
 * @since 2019-04-29
 */
public interface IRylbRelationService extends IService<RylbRelationEntity> {
    
}
