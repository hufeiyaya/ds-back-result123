package com.mti.service.impl;


import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfZdryBaseTempEntity;
import com.mti.mapper.LfZdryBaseTempMapper;
import com.mti.service.LfZdryBaseTempService;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 重点人员表 服务实现类
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Slf4j
@Service
public class LfZdryBaseTempServiceImpl extends ServiceImpl<LfZdryBaseTempMapper, LfZdryBaseTempEntity> implements LfZdryBaseTempService {
	
	
}
