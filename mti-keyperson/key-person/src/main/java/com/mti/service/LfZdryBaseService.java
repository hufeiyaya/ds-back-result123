package com.mti.service;

import com.mti.dao.dto.LfZdryDto;
import com.mti.dao.dto.OrganizationDto;
import com.mti.dao.dto.ZdryDto;
import com.mti.dao.kmmodel.KmZdryDto;
import com.mti.dao.model.LfZdryBaseEntity;
import com.mti.dao.model.LfZdryBaseTempEntity;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.LfZdryTempEntity;
import com.mti.dao.qo.AddressQO;
import com.mti.dao.qo.LfZdryQO;
import com.mti.jwt.AccountBo;

import java.util.List;
import java.util.Map;

import com.mti.vo.SpecialPersonStatisticsDataVO;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * <p>
 * 重点人员表 服务类
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */

public interface LfZdryBaseService extends IService<LfZdryBaseEntity> {

    /**
     * 导出重点人员时查询数据
     * @param qo
     * @return
     */
    List<LfZdryBaseEntity> findExportExeclData(LfZdryQO qo);

	/**
	 * 根据身份证号导出重点人员数据
	 * @param qo
	 * @return
	 */
	List<LfZdryBaseEntity> findExportExeclDataNumber(List<String> listNumber);
	/**
     *  根据条件查询重点人员
     * @param lfZdryDto
     * @return
     */
    List<ZdryDto> queryZdryByCondition(LfZdryDto lfZdryDto);

	/**
	 * 重点人员列表查询
	 * @param qo
	 * @param exchange
	 * @return
	 */
	IPage<LfZdryBaseEntity> queryPageList(LfZdryQO qo, ServerWebExchange exchange);

	/**
	 * 重点人员新增
	 * @param exchange
	 * @param entity
	 * @return
	 */
	boolean saveZdry(ServerWebExchange exchange, LfZdryBaseEntity entity);
	/**
	 * 根据身份证号查询重点 人员
	 * @param sfzh
	 * @return
	 */
	LfZdryBaseEntity getBySfzh(String sfzh,String currentUserId);
	/**
	 * 删除重点人员
	 * @param id
	 * @return
	 */
	Integer deleteZdry(String id,ServerWebExchange exchange);
	
	boolean updateZdry(ServerWebExchange exchange, LfZdryBaseEntity entity);

	/**
	 * 根据派出所查询重点人员
	 * @param keyWords
	 * @param sspcsId
	 * @param ssxqId
	 * @param offset
	 * @param size
	 * @return
	 */
    IPage<ZdryDto> queryZdryByZrpcsPage(String keyWords, String sspcsId, String ssxqId,Integer offset, Integer size);

	/**
	 * 根据派出所名称导出重点人员
	 * param  ssxqId
	 * @param sspcsId
	 * @param exportName
	 * @return
	 */
	Mono<DataBuffer> export(String ssxqId,String sspcsId, String exportName);

	/**
	 * 根据重点人员id修改稳控状态
	 * @param id
	 * @param wkzt
	 * @return
	 */
	
	boolean updateWkztById(String id, String wkzt);
	/**
	 * 移交
	 * @param exchange
	 * @param entity
	 * @return
	 */
	boolean transfer(ServerWebExchange exchange, LfZdryBaseEntity entity);
	/**
	 * 流程审核
	 * @param exchange
	 * @param entity
	 * @return
	 */
	boolean reviewProcess(ServerWebExchange exchange, LfZdryBaseTempEntity entity);

	/**
	 * 查询每个省份的重点人员数量
	 * @return
	 */
	Object getEveryProvinceZdryNum();

	/**
	 * 根据省份id获取城市的重点人员数量
	 * @param code
	 * @return
	 */
	Object getCityZdryNumByProvinceCode(String code);

	/**
	 * 根据市code查询区县重点人员数量
	 * @param code
	 * @return
	 */
	Object getCountryZdryNumByCityCode(String code);

	/**
	 * 导出 户籍地 和 先住地 的数据
	 * @param sfzhs
	 * @param exportName
	 * @return
	 */
	Mono<DataBuffer> exportZdryByHjdAndXzz(List<String> sfzhs, String exportName);

	/**
	 * 查询从云南省外 迁入的数据
	 * @param exchange
	 * @param qo
	 * @return
	 */
	Object queryProvinceTx(ServerWebExchange exchange, LfZdryQO qo);

	/**
	 * 查询省/市迁徙图数据
	 * @param exchange
	 * @param qo
	 * @return
	 */
	IPage<ZdryDto> queryProvinceAndCityTxList(ServerWebExchange exchange, LfZdryQO qo);

	/**
	 * 导出省/市迁徙图出数据
	 * @param qo
	 * @param exportName
	 * @param exchange
	 * @return
	 */
	Mono<DataBuffer> exportZdryProvinceAndCityQxData(LfZdryQO qo, String exportName, ServerWebExchange exchange);

	/**
	 * 查询 户籍地 和 现住地经纬度
	 * @param exchange
	 * @param qo
	 * @return
	 */
	List<Map<String,Object>> queryZdryList(ServerWebExchange exchange, LfZdryQO qo);

	/**
	 * 根据户籍地 和 现住地的撒点信息查询重点人员信息
	 * @param paramMap
	 * @return
	 */
	IPage<ZdryDto> queryZdry(Map<String, Object> paramMap);

	/**
	 * 查询 给昆明大保推送重点人员数据
	 * @param offset
	 * @param pageSize
	 * @return
	 */
    Map<String, Object> queryPushZdryInfo(Integer offset, Integer pageSize);
/**
     * 地址转经纬度
     * @param address
     * @return
     */
	Map<String, String> getCoordinate(AddressQO address);
	/**
	 * 待审核列表查询
	 * @param qo
	 * @param exchange
	 * @return
	 */
	IPage<LfZdryBaseTempEntity> queryPageStatyReviewedList(LfZdryQO qo, ServerWebExchange exchange);/**
	 * 查询实际迁徙数据
	 * @param exchange
	 * @param qo
	 * @return
	 */
	Object queryCityTx(ServerWebExchange exchange, LfZdryQO qo);
	/**
	 * 通过身份证号查询待处理详情
	 * @param sfzh
	 * @return
	 */
	LfZdryBaseTempEntity getDetailBySfzh(String sfzh);
	/**
	 * 通过ID修改诉求
	 * @param id
	 * @param appeal
	 * @return
	 */
	boolean updateAppealById(String id, String appeal);
	/**
	 * 操作日志记录
	 * @param type
	 * @param bo
	 * @param entity
	 * @param logRecord
	 * @param opinion
	 * @return
	 */
	boolean operatLog(String type, AccountBo bo, LfZdryBaseEntity entity, String logRecord, String opinion,ServerWebExchange exchange);

	/**
	 * 概览接口
	 * @param cid
	 * @param code
	 * @param regionId
	 * @return
	 */
    List<Map<String, String>> getCount(String cid, String code, String regionId);

	/**
	 * 重点人员统计接口
	 * @param rangeType
	 * @param rangeVal
	 * @param zrdw
	 * @return
	 */
    List<SpecialPersonStatisticsDataVO> getSpecialPersonStatistics(LfZdryQO qo);
    /**
     * 通过权限查询未接收重点人员总数信息
     * @param exchange
     * @return
     */
	Integer getNotReceviedCount(ServerWebExchange exchange);
	/**
	 * 通过权限查询待处理数量
	 * @param exchange
	 * @return
	 */
	Integer getStayHandleCount(ServerWebExchange exchange);

	/**
	 * 通过省份证号 和 电话号码查询重点人员信息
	 * @param idCard
	 * @param phone
	 * @return
	 */
	Map<String,String> findZdryBySfzhAndSjh(String idCard, String phone);
	/**
	 * 通过sfzh修改tx标记
	 * @param sfzh
	 * @return
	 */
	boolean updateTxBySfzh(String sfzh);

	/**
	 * 根据身份证号更新重点人员
	 * @param zdry
	 * @return
	 */
	Integer updateZdryBySfzh(LfZdryBaseEntity zdry);

	/**
	 * 根据数据库查询重点人员数量
	 * @param sfzh
	 * @return
	 */
    Object getZdryCountBySfzh(String sfzh);

	/**
	 * 推送重点人员信息
	 *
	 * @param personId 重点人员id
	 * @param gxlx 更新类型: 0.新增 1.修改 2.删除
	 * @return
	 */
	boolean sendKeyPersonMessage(String personId, String gxlx);

	/**
	 * 数据清理 查询重点人员列表
	 * @param start
	 * @param size
	 * @param keyWords
	 * @return
	 */
	IPage<LfZdryBaseEntity> queryZdryListClear(Integer start, Integer size, String keyWords);

	/**
	 * 根据当前登录人员获取所属分局
	 * @param exchange
	 * @return
	 */
    Object querySsxq(ServerWebExchange exchange);

	/**
	 * 每日跟踪 查询下拉单位
	 * @param exchange
	 * @return
	 */
	List<Map<String,Object>> findUnitByCurrentUser(ServerWebExchange exchange);

	/**
	 * 用户权限设置
	 * @param exchange
	 * @param qo
	 */
	void setUpAuthority(ServerWebExchange exchange, LfZdryQO qo);

	/**
	 * 每日跟踪查询稳控状态数量
	 * @param exchange
	 * @param qo
	 * @return
	 */
    Object queryWkztCount(ServerWebExchange exchange, LfZdryQO qo);

	/**
	 * 查询警种统计数据
	 * @param zrdw
	 * @return
	 */
	List<SpecialPersonStatisticsDataVO> getSpecialPersonStatisticsForPoliceCategory(LfZdryQO qo);

	/**
	 * 根据重点人员省份证号查询重点人员列表
	 * @param sfzhs
	 * @return
	 */
    List<LfZdryBaseEntity> findZdryGatherList(List<String> sfzhs);
}