package com.mti.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mti.utils.GetRemoteIpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.component.snowflake.KeyWorker;
import com.mti.dao.model.LfZdryHistoryLogEntity;
import com.mti.dao.model.TKmProHandle;
import com.mti.dao.model.TTaskFileEntity;
import com.mti.dao.qo.TkmProHanQo;
import com.mti.exception.BusinessException;
import com.mti.jwt.AccountBo;
import com.mti.jwt.RequestUtils;
import com.mti.mapper.TKmProHandleMapper;
import com.mti.service.LfZdryHistoryLogService;
import com.mti.service.TKmProHandleService;
import com.mti.service.TTaskFileService;

/**
 * <p>
 * 信访/聚访维权处理或化解信息 服务实现类
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
@Service
public class TKmProHandleServiceImpl extends ServiceImpl<TKmProHandleMapper, TKmProHandle> implements TKmProHandleService {

	@Autowired
	private TTaskFileService taskFileService;
	
	@Autowired
	private RequestUtils requestUtils;
	
	@Autowired
	private LfZdryHistoryLogService lfZdryHistoryLogService;
	@Autowired
	private GetRemoteIpUtil getRemoteIpUtil;

    @Override
    public List<TKmProHandle> listTkmProHandle(TKmProHandle tKmProHandle) {
    	List<TKmProHandle> kmHandles = this.getBaseMapper().listTkmProHandle(tKmProHandle);
    	for (TKmProHandle kmHandle : kmHandles) {
    		kmHandle.setFileList(taskFileService.getByBussId(kmHandle.getId()));
		}
        return kmHandles;
    }

    @Override
    public IPage<TKmProHandle> pageTkmProHandle(TkmProHanQo tkmProHanQo) {
        Page<TKmProHandle> page = new Page<>(tkmProHanQo.getStart(),tkmProHanQo.getSize());
        IPage<TKmProHandle> result = this.getBaseMapper().pageTkmProHandle(page,tkmProHanQo);
        return result;
    }
    
    @Override
    public boolean add(ServerWebExchange exchange,TKmProHandle kmProHandle) {
    	AccountBo bo = requestUtils.getCurrentUser(exchange);
		if (bo == null) {
			throw new BusinessException(10019,"此账号已在别处登录，请查证");
		}
    	boolean result = this.save(kmProHandle);
    	List<TTaskFileEntity> fileList = new ArrayList<TTaskFileEntity>();
    	for (TTaskFileEntity fileEntity : kmProHandle.getFileList()) {
    		TTaskFileEntity file =new TTaskFileEntity();
    		file.setId(String.valueOf(KeyWorker.nextId()));
    		file.setBussType("6");//6 处理备注
    		file.setBussId(kmProHandle.getId().toString());
    		file.setFileUrl(fileEntity.getFileUrl());
    		file.setFileName(fileEntity.getFileName());
    		fileList.add(file);
		}
    	if(result) {
    		result =taskFileService.saveBatch(fileList);
    	}
    	//历史信息表
        LfZdryHistoryLogEntity history = new LfZdryHistoryLogEntity();
        history.setId(kmProHandle.getId());
        history.setCreator(bo.getPersonBo().getName());
        history.setOperateDept(bo.getOrganizationBo().getShortName());
        history.setCreateTime(kmProHandle.getOpTime());
   		history.setSpId(kmProHandle.getRefId());
   		history.setInsertTime(new Date());
   		history.setAddition(kmProHandle.getOpType());
   		history.setType(kmProHandle.getType());
   		history.setApproveResult(kmProHandle.getOpType());
   		String remoteIp = getRemoteIpUtil.getRemoteIP(exchange.getRequest());
		history.setOperateIp(remoteIp);
   		result = lfZdryHistoryLogService.save(history);
    	return result;
    	
    	
    }
}
