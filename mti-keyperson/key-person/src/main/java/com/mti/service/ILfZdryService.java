package com.mti.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.FutureTask;

import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.server.ServerWebExchange;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.dto.ZdryDto;
import com.mti.dao.model.LfRyyjsjEntity;
import com.mti.dao.model.LfZdryControlPcEntity;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.LfZdryTempEntity;
import com.mti.dao.model.LfZdryZrbmEntity;
import com.mti.dao.qo.AddressQO;
import com.mti.dao.qo.LfZdryQO;
import com.mti.vo.PredictStatisticsDataItem;
import com.mti.vo.SpecialPersonStatisticsDataVO;
import com.mti.vo.ZdryVo;

import reactor.core.publisher.Mono;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface ILfZdryService extends IService<LfZdryEntity> {

    LfZdryEntity getByXh(String id);

    ZdryDto getBySfzh(String sfzh);
    LfZdryEntity getBySfzhV2(String sfzh);

    LfZdryZrbmEntity getByZrbm(String rylbx);

    List<LfZdryZrbmEntity> getAllRylxWithZrbm();

    LfRyyjsjEntity getByBjxx(String sfzh, String datetime, String eventType);

    IPage<LfZdryEntity> queryPage(LfZdryQO qo);

    Mono<DataBuffer> queryForExport(LfZdryQO qo);

    List<Map<String, String>> getCount(String cid,String zrbm,String rangeType,String rangeVal);

    List<ZdryVo> getSimpleAll();

    int getTx(String id);

    /**
     * 获取布控接收对象
     *
     * @return
     */
    Map<Object, Object> queryReceivingInfo();

    /**
     * 保存重点人员布控信息
     *
     * @param lfZdryControlPcEntity 重点人员布控pc信息
     * @return
     */
    boolean saveZdryControlPc(LfZdryControlPcEntity lfZdryControlPcEntity);

    IPage<LfZdryEntity> pageData(
            Integer page,
            Integer size,
            String date,
            String rangeVal,
            String rangeType
    );

    IPage<LfZdryEntity> listStatisticsPage(
            Integer page,
            Integer size,
            String rangeVal,
            String rangeType,
            String zrbm,
            String isApproveFilter,
            String start,
            String end
    );

    List<LfZdryEntity> listData(
            String date,
            String rangeVal,
            String rangeType
    );

   /* List<SpecialPersonStatisticsDataVO> getSpecialPersonStatistics(String rangeType, String rangeVal, String zrdws);*/

    List<PredictStatisticsDataItem> getPredictSpecialPersonStatistics(String rangeType,String rangeVal,String start,String end);

    List<PredictStatisticsDataItem> getPredictEventSpecialPersonStatistics(String rangeType,String rangeVal,String start,String end);

    String getCiperBySfzh(String sfzh);

    void fullLocationInfo(String sfzh);
    //通过组织ID获取组织名称
	String getOrgName(String orgId);
	//通过权限查重点人员列表
	IPage<LfZdryEntity> queryPageList(LfZdryQO qo, ServerWebExchange exchange);
	//通过权限和审核状态 查询待审核列表
	IPage<LfZdryTempEntity> queryPageStatyReviewedList(LfZdryQO qo, ServerWebExchange exchange);

    /**
     * 远程获取低于信息
     * @return
     */
    Map<String,String> getAllRegionInfo();

    /**
     * 远程获取责任派出所
     * @param orgType
     * @return
     */
    Map<String,String> getZrpcs(String orgType);

	List<Map<String,Integer>> getMjControlCountByPcs(String pcs,String zrbm,String rangeType,String rangeVal);


    List<Map<String,Integer>> getSpByHjs(String province,String zrbm,String rangeType,String rangeVal,String areaLevel);
	/**
	 * 重点人员修改方法
	 * @param exchange
	 * @param entity
	 * @return
	 */
	boolean updateZdry(ServerWebExchange exchange, LfZdryEntity entity);


    /**
     * 根据派出所名称查询重点人员
     * @param zrpcs
     * @return
     */
    //List<LfZdryEntity> queryZdryByZrpcs(String zrpcs);

    /**
     * 导出对应的派出所的所有重点人员
     * @param zrpcs
     * @param exportName
     * @return
     */
    //Mono<DataBuffer> export(String zrpcs, String exportName);

    /**
     * 根据派出所名称查询重点人员
     * @param zrpcs
     * @return
     */
    //IPage<LfZdryEntity> queryZdryByZrpcsPage(String keyWords,String zrpcs, Integer offset, Integer size);

    /**
     * 根据id修改稳控状态
     * @param id
     * @param wkzt
     * @return
     */
    //Object updateWkztById(String id, String wkzt);


/**
	 * 流程审核方法
	 * @param exchange
	 * @param entity
	 * @return
	 */
	boolean reviewProcess(ServerWebExchange exchange, LfZdryTempEntity entity);
	/**
	 * 待审核列表通过Id查询详情
	 * @param id
	 * @return
	 */
	LfZdryTempEntity getDetailById(String id);
	/**
	 * 重点人员移交	
	 * @param exchange  用户信息
	 * @param entity
	 * @return
	 */
	boolean transfer(ServerWebExchange exchange, LfZdryEntity entity);
	/**
	 * 通过ID删除记录
	 * @param id
	 * @return
	 */
	Integer delete(String id);



    /**
     * 插入或者编辑时设置经纬度
     * @param entity
     */
	FutureTask<Map<String, String>> getLngLat(LfZdryEntity entity);

    /**
     * 根据条件查询满足条件的重点人员的身份证
     * @param exchange
     * @param qo
     * @return
     */
     List<String> getSfzhsByCondition(ServerWebExchange exchange, LfZdryQO qo);

    /**
     * 根据户籍地址 或者详细地址 转化省市县
     * @return
     */
    void convertAddress(Integer total) ;

    /**
     * 根据户籍地查询全国各省重点人员数量
     * @return
     */
    //Object getEveryProvinceZdryNum();

    /**
     * 根据省份的code获取各市的重点人员数量
     * @param code
     * @return
     */
    //Object getCityZdryNumByProvinceCode(String code);

    /**
     * 根据市的code获取各县/区的重点人员数量
     * @param code
     * @return
     */
    //Object getCountryZdryNumByCityCode(String code);

    /**
     * 导出重点人员  户籍地  和 现住地 撒点出的功能
     * @param sfzhs
     * @param exportName
     * @return
     */
    //Mono<DataBuffer> exportZdryByHjdAndXzz(List<String> sfzhs, String exportName);

    /**
     * 查询重点人员信息，不分页，户籍地和现住地撒点功能
     * @param exchange
     * @param qo
     * @return
     */

    //List<Map<String,Object>> queryZdryList(ServerWebExchange exchange, LfZdryQO qo);

    /**
     * 查询省级迁徙数据
     * @param exchange
     * @param qo
     * @return
     */
    //Object queryProvinceTx(ServerWebExchange exchange, LfZdryQO qo);

    /**
     * 查询实际迁徙数据
     * @param exchange
     * @param qo
     * @return
     */
    Object queryCityTx(ServerWebExchange exchange, LfZdryQO qo);

    /**
     * 查询省市迁徙图的列表数据
     * @param exchange
     * @param qo
     * @return
     */
    //IPage<LfZdryEntity> queryProvinceAndCityTxList(ServerWebExchange exchange, LfZdryQO qo);

    /**
     * 导出省市迁徙图的列表数据
     * @param qo
     * @param exportName
     * @param exchange
     * @return
     */
    //Mono<DataBuffer> exportZdryProvinceAndCityQxData(LfZdryQO qo, String exportName, ServerWebExchange exchange);

    /**
     * 根据户籍地 和 现住地的撒点信息查询重点人员信息
     * @param paramMap
     * @return
     */
    //IPage<LfZdryEntity> queryZdry(Map<String,Object> paramMap);
    /**
	 * 新增重点人员
	 * @param exchange
	 * @param entity
	 * @return
	 */
	boolean saveZdry(ServerWebExchange exchange, ZdryDto entity);
/**
 * 根据地址转坐标
 * @param address
 * @return
 */
	Map<String, String> getCoordinate(AddressQO address);

	/**
	 * 根据所属辖区和人员类别统计人数及小类名称
	 * @return
	 */
	List<Map<String,Object>> getRiskLevelCount(String sspcs,String ssxq,String rylb);


}
