package com.mti.service.impl;

import com.mti.dao.model.KmPhYjEntity;
import com.mti.mapper.KmPhYjMapper;
import com.mti.service.KmPhYjService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 手机预警 服务实现类
 * </p>
 *
 * @author duchao
 * @since 2020-04-29
 */
@Service
public class KmPhYjServiceImpl extends ServiceImpl<KmPhYjMapper, KmPhYjEntity> implements KmPhYjService {

    @Override
    public List<Map<String,Object>> queryPhYjaLastLocation() {

        return null;
    }
}
