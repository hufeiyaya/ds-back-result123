/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZdryArmingTaskEntity;
import com.mti.dao.model.LfZdryBaseEntity;
import com.mti.dao.model.LfZdryEntity;
import com.mti.vo.ArmingTaskDetailVO;
import org.springframework.web.server.ServerWebExchange;

import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/21
 * @change
 * @describe describe
 **/
public interface ISpecialPersonArmingTaskService extends IService<LfZdryArmingTaskEntity> {
    /**
     * 添加重点人员到布防信息
     * @param taskId 布防任务ID
     * @param reportOrg 上报单位
     * @param specialPersonId 重点人员ID
     */
    void addSpecialPersonToArmingTask(String taskId,String reportOrg,String specialPersonId,String isSpyAdd, ServerWebExchange exchange);

    /**
     * 添加重点人员到布防信息(多人)
     * @param taskId 布防任务ID
     * @param reportOrg 上报单位
     * @param specialPersonIds 重点人员ID(多人)
     */
    void addSpecialPersonListToArmingTask(String taskId,String reportOrg, String specialPersonIds,String isSpyAdd, ServerWebExchange exchange);

    /**
     * 删除布控任务下的重点人员
     * @param taskId 任务ID
     * @param specialPersonId 人员id
     * @param reportOrg 上报单位
     */
    void delSpecialPersonFromArmingTask(String taskId,String specialPersonId,String reportOrg, ServerWebExchange exchange);

    /**
     * 获取布防任务所有重点人员
     * @param vo 任务id
     * @return 重点人员
     */
    IPage<LfZdryBaseEntity> getTaskSpecialPersons(ArmingTaskDetailVO vo);

    Page<LfZdryArmingTaskEntity> getTaskList(
            Integer page,
            Integer size,
            String taskName,
            Integer specialLevel,
            Integer status,
            String armingRange,
            String armingRangeId,
            String orgId,
            String oid,
            String zrbm,
            String approveStatus
    );

    /**
     * 启动或停止布防任务
     * @param operate 操作类型
     * @param taskId 任务ID
     */
    void armingTaskOperation(String operate,String taskId);

    /**
     * 上报重点人员
     * @param taskId taskId
     */
    void armingTaskReport(String taskId,String reportOrgId,String reportUserId,String reportUserName,String rid,String type,String isSpyAdd, ServerWebExchange exchange);
}
