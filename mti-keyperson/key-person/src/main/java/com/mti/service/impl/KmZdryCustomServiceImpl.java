package com.mti.service.impl;

import com.mti.dao.model.KmZdryCustomEntity;
import com.mti.mapper.KmZdryCustomMapper;
import com.mti.service.KmZdryCustomService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 重点人员-自定义字段表 服务实现类
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Service
public class KmZdryCustomServiceImpl extends ServiceImpl<KmZdryCustomMapper, KmZdryCustomEntity> implements KmZdryCustomService {

}
