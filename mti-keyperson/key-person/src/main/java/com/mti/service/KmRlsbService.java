package com.mti.service;

import com.mti.dao.model.KmRlsbEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 人脸识别 服务类
 * </p>
 *
 * @author duchao
 * @since 2020-06-19
 */
public interface KmRlsbService extends IService<KmRlsbEntity> {
    /**
     * 判断 一个小时内 预警重复的问题
     * @param kmRlsbEntity
     * @return
     */
    boolean findRlsbInfo(KmRlsbEntity kmRlsbEntity);
}
