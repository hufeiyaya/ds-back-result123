package com.mti.service.impl;

import com.mti.dao.model.TKmMhlgEntity;
import com.mti.mapper.TKmMhlgEntityMapper;
import com.mti.service.TKmMhlgService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Classname TKmMhlgServiceImpl
 * @Description TODO
 * @Date 2020/1/18 11:48
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmMhlgServiceImpl implements TKmMhlgService {
    @Resource
    private TKmMhlgEntityMapper tKmMhlgEntityMapper;

    @Override
    public Integer insert(TKmMhlgEntity tKmMhlgEntity) {
        return tKmMhlgEntityMapper.insert(tKmMhlgEntity);
    }

    @Override
    public TKmMhlgEntity getByGlId(String glId) {
        return tKmMhlgEntityMapper.selectByPrimaryKey(glId);
    }

    @Override
    public List<TKmMhlgEntity> getMhlgListBySfzh(String sfzf) {
        return tKmMhlgEntityMapper.getMhlgListBySfzh(sfzf);
    }

    @Override
    public List<TKmMhlgEntity> getMhlgList(List<String> mhlgEntities) {
        return tKmMhlgEntityMapper.getMhlgList(mhlgEntities);
    }
}
