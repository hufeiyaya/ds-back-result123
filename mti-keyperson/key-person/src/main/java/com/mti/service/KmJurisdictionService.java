package com.mti.service;

import com.mti.dao.model.KmJurisdictionEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author duchao
 * @since 2020-05-03
 */
public interface KmJurisdictionService extends IService<KmJurisdictionEntity> {
    /**
     * 根据预警的经纬度查询预警地派出所信息
     * @param loc
     * @param lat
     * @return
     */
    KmJurisdictionEntity queryYjdPcsByLngAndLat(String loc, String lat);
}
