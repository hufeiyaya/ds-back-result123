package com.mti.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.TKmTldpEntity;
import com.mti.utils.ZdryQxzkUtilBean;
import com.mti.utils.ZdryYjUtilBean;

import java.util.List;

/**
 * @Classname TKmTldpService
 * @Description TODO
 * @Date 2020/1/17 16:14
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmTldpService {
    Integer insert(TKmTldpEntity tKmTldpEntity);

    /**
     * 通过注解获取数据
     * @param glId
     * @return
     */
    TKmTldpEntity getByGlId(String glId);

    List<TKmTldpEntity> getTldpListBySfzh(String sfzf);

    /**
     * 铁路出昆明 人数统计
     * @param startTime
     * @param endTime
     * @return
     */
    List<ZdryQxzkUtilBean> getTlOut(String startTime, String endTime);

    /**
     * 铁路进昆明 人数统计
     * @param startTime
     * @param endTime
     * @return
     */
    List<ZdryQxzkUtilBean> getTlIn(String startTime, String endTime);

    /**
     * 根据铁路订票主键获取铁路订票信息
     * @param tldpEntities
     * @return
     */
    List<TKmTldpEntity> getTldpEntityList(List<String> tldpEntities);
}
