package com.mti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZtryyjEntity;
import com.mti.dao.qo.KmAlarmFilterQo;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.dao.qo.YjInfoParamQo;
import com.mti.dao.vo.ZdryTxVo;
import com.mti.dao.vo.ZdryYjPushToDsUtil;
import com.mti.utils.ZdryQxzkUtilBean;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

public interface ILfZtryyjsjService extends IService<LfZtryyjEntity> {

    IPage<LfZtryyjEntity> getZdryYjInfo(LfRyyjsjQO lfRyyjsjQO, ServerWebExchange exchange);

    /**
     * 根据省份证查询所有预警信息
     *
     * @param sfzf
     * @return
     */
    IPage<Map<String, Object>> getZdryAllYj(String sfzf, Integer offset, Integer pageSize);

    /**
     * 根据省份证查询所有预警信息(不分页)
     *
     * @param sfzf
     * @return
     */
    List<Map<String, Object>> getZdryAllYjNoPage(String sfzf,String startTime,String endTime);

    /**
     * 获取重点人员迁徙状况
     *
     * @param lfRyyjsjQO
     * @return
     */
    Object getZdryTxzk(LfRyyjsjQO lfRyyjsjQO, ServerWebExchange exchange);

    /**
     * 查询
     *
     * @param lfRyyjsjQO
     * @return
     */
    Object queryWbLgKyNumber(LfRyyjsjQO lfRyyjsjQO, ServerWebExchange exchange);

    /**
     * 导出重点人员的预警信息
     *
     * @param lfRyyjsjQO
     * @param exportName
     * @return
     */
    Mono<DataBuffer> exportYjXx(LfRyyjsjQO lfRyyjsjQO, String exportName, ServerWebExchange exchange) throws Exception;

    /**
     * 通过预警标识 查询出需要推送的token
     *
     * @param refId
     * @return
     */
    List<String> findWebsocketPsuhUserListByYjwybs(String refId);

    /**
     * 查询预警超时信息
     *
     * @param lfRyyjsjQO
     * @return
     */
    IPage<LfZtryyjEntity> getYjTimeOutInfo(LfRyyjsjQO lfRyyjsjQO, ServerWebExchange exchange);

    /**
     * 查询定位信息
     *
     * @param paramMap
     * @return
     */
    IPage<ZdryTxVo> queryLocationInfo(List<YjInfoParamQo> paramMap, Integer offset, Integer pageSize);

    /**
     * 导出预警信息定位信息
     *
     * @param paramMap
     * @param exportName
     * @return
     */
    Mono<DataBuffer> exportYjLocationInfo(List<YjInfoParamQo> paramMap, String exportName);

    /**
     * 查询铁路客运民航迁徙的详细信息
     *
     * @param lfRyyjsjQO
     * @param zdryQxzkUtilBean
     * @return
     */
    IPage<ZdryTxVo> queryTlKyMhQxInfo(LfRyyjsjQO lfRyyjsjQO, ZdryQxzkUtilBean zdryQxzkUtilBean, Integer offset, Integer pagesize, ServerWebExchange exchange);

    /**
     * 导出重点人员的预警信息
     *
     * @param lfRyyjsjQO
     * @param zdryQxzkUtilBean
     * @return
     */
    Mono<DataBuffer> exportYjTxInfo(LfRyyjsjQO lfRyyjsjQO, ZdryQxzkUtilBean zdryQxzkUtilBean, ServerWebExchange exchange);

    /**
     * 推送预警信息
     *
     * @param offset
     * @param pageSize
     * @return
     */
    Map<String, Object> pushZdryYjInfo(Integer offset, Integer pageSize);

    /**
     * 查询预警推送数据
     *
     * @return
     */
    List<ZdryYjPushToDsUtil> queryYjDataPushToDs();

    /**
     * 重点人员的预警统计
     *
     * @return
     */
    Object zdryYjStatistics(LfRyyjsjQO lfRyyjsjQO, ServerWebExchange exchange);

    /**
     * 查询重点人员预警统计数据
     *
     * @param lfRyyjsjQO
     * @return
     */
    IPage<ZdryTxVo> queryZdryYjStatisticsData(LfRyyjsjQO lfRyyjsjQO, ServerWebExchange exchange);

    /**
     * 导出重点人员预警统计数据
     *
     * @param lfRyyjsjQO
     * @param exchange
     * @return
     */
    Mono<DataBuffer> exportZdryYjStatisticsData(LfRyyjsjQO lfRyyjsjQO, ServerWebExchange exchange);

    /**
     * @return java.util.List<com.mti.dao.model.LfZtryyjEntity>
     * @Author hf
     * @Title: queryAlarmFilterData
     * @Description: TODO (获取符合预警处置规则的数据)
     * @Param [qo]
     * @Date 2020/11/25 14:36
     */
    List<LfZtryyjEntity> queryAlarmFilterData(KmAlarmFilterQo qo);

    /**
     * @return void
     * @Author hf
     * @Title: updateAlarmFilterMark
     * @Description: TODO (更新预警过滤标识)
     * @Param [collect]
     * @Date 2020/11/25 15:11
     */
    void updateAlarmFilterMark(List<String> collect);

    /**
     * @return boolean
     * @Author hf
     * @Title: authEarlyWarning
     * @Description: TODO (预警筛选附加条件)
     * @Param [lfZtryyjEntity]
     * @Date 2020/11/30 11:28
     */
    boolean authEarlyWarning(LfZtryyjEntity lfZtryyjEntity, String name);
}
