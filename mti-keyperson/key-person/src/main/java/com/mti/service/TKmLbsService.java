package com.mti.service;

import com.mti.dao.model.TkmLbsEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.qo.LfZdryQO;
import org.springframework.web.server.ServerWebExchange;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 微信/qq定位 服务类
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
public interface TKmLbsService extends IService<TkmLbsEntity> {

    /**
     * 查询
     * @param exchange
     * @param qo
     * @return
     */
    Object queryZdryLbsPosition(ServerWebExchange exchange, LfZdryQO qo);

    /**
     * 查询LBS定位详情
     * @param sfzhs
     * @return
     */
    List<Map<String, Object>> queryLbsLocationInfo(List<String> sfzhs);
}
