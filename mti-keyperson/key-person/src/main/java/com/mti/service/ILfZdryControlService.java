package com.mti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZdryControlEntity;

import java.util.Collection;

/**
 * <p>
 *  布控信息 服务类
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-15
 */
public interface ILfZdryControlService extends IService<LfZdryControlEntity> {

    /**
     * 根据对象属性作为条件删除对应的数据列表
     *
     * @param entity
     * @return
     */
    Boolean removeByBean(LfZdryControlEntity entity);

    /**
     * 根据对象属性作为条件获取对象数据
     *
     * @param entity
     * @return
     */
    LfZdryControlEntity getByBean(LfZdryControlEntity entity);

    /**
     * 根据对象属性作为条件检索所有的数据列表
     *
     * @param entity
     * @return
     */
    Collection<LfZdryControlEntity> listByBean(LfZdryControlEntity entity);

    /**
     * 根据对象属性作为条件分页检索数据列表
     *
     * @param entity
     * @return
     */
    IPage pageByBean(LfZdryControlEntity entity);

}
