package com.mti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.dao.model.TKmYjHandleEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.qo.TkmYjHanQo;
import com.mti.dao.vo.TKmYjHandleVo;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 预警处置表 服务类
 * </p>
 *
 * @author duchaof
 * @since 2020-03-11
 */
public interface TKmYjHandleService extends IService<TKmYjHandleEntity> {

    /**
     * 添加预警处置
     * @param tKmYjHandleEntityList
     * @return
     */
    boolean addTKmYjHandle(List<TKmYjHandleEntity> tKmYjHandleEntityList, ServerWebExchange exchange);

    /**
     * 删除预警处置
     * @param id
     * @return
     */
    boolean deleteTKmYjHandle(String id);

    /**
     * 更新预警处置
     * @param tKmYjHandleEntityList
     * @return
     */
    boolean updateTKmYjHandle(TKmYjHandleEntity tKmYjHandleEntity);

    /**
     * 查询预警处置类别信息
     * @param tkmYjHanQo
     * @return
     */
    Object listTKmYjHandle(TkmYjHanQo tkmYjHanQo);

    /**
     * 分页查询预警处置信息
     * @param tkmYjHanQo
     * @return
     */
    IPage<TKmYjHandleVo> pageTKmYjHandle(TkmYjHanQo tkmYjHanQo);

    /**
     * 导出预警处置信息
     * @param tkmYjHanQo
     * @param exportName
     * @return
     */
    Mono<DataBuffer> exportYjHandleInfo(TkmYjHanQo tkmYjHanQo, String exportName);

    /**
     * 推送重点人员预警处置数据
     * @param offset
     * @param pageSize
     * @return
     */
    Map<String, Object> pushYjHandleInfo(Integer offset, Integer pageSize);
}
