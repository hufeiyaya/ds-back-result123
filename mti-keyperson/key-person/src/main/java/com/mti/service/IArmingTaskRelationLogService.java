package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfZdryArmingTaskOperateLog;

public interface IArmingTaskRelationLogService extends IService<LfZdryArmingTaskOperateLog> {
}
