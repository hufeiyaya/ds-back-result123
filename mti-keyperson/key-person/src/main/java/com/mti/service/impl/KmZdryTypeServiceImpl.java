package com.mti.service.impl;

import com.mti.dao.model.KmZdryTypeEntity;
import com.mti.mapper.KmZdryTypeMapper;
import com.mti.service.KmZdryTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Service
public class KmZdryTypeServiceImpl extends ServiceImpl<KmZdryTypeMapper, KmZdryTypeEntity> implements KmZdryTypeService {

    @Override
    public List<KmZdryTypeEntity> getZdryTypeBySfzh(String yjbs) {
        return this.getBaseMapper().getZdryTypeBySfzh(yjbs);
    }
}
