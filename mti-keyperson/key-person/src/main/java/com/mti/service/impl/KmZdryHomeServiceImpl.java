package com.mti.service.impl;

import com.mti.dao.model.KmZdryHomeEntity;
import com.mti.mapper.KmZdryHomeMapper;
import com.mti.service.KmZdryHomeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 重点人员表 服务实现类
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Service
public class KmZdryHomeServiceImpl extends ServiceImpl<KmZdryHomeMapper, KmZdryHomeEntity> implements KmZdryHomeService {

}
