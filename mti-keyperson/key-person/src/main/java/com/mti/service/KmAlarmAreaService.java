package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.kmmodel.GisModel;
import com.mti.dao.model.KmAlarmAreaEntity;
import com.mti.dao.qo.KmAlarmAreaQO;

import java.util.List;

/**
 * <p>
 * 预警区域设置 接口类
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */

public interface KmAlarmAreaService extends IService<KmAlarmAreaEntity> {

    boolean authIsInArea(KmAlarmAreaQO qo);


    List<GisModel> getAreaList();
}