package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.TSwitchTypeEntity;
import com.mti.dao.qo.KmAlarmFilterQo;

import java.util.List;
import java.util.Map;

public interface TSwitchTypeServie extends IService<TSwitchTypeEntity> {

    /**
     * @return boolean
     * @Author hf
     * @Title: upSertSwitchStatus
     * @Description: TODO (根据状态和类型更新/设置开关状态)
     * @Param [tSwitchTypeEntity]
     * @Date 2020/11/24 10:04
     */
    boolean upSertSwitchStatus(TSwitchTypeEntity tSwitchTypeEntity);

    /**
     * @return boolean
     * @Author hf
     * @Title: getSwitchStatus
     * @Description: TODO (根据类型和名称获取开关状态)
     * @Param [type, name]
     * @Date 2020/11/24 10:07
     */
    boolean getSwitchStatus(int type, String name);

    /**
     * @return java.util.Map
     * @Author hf
     * @Title: getSwitchByType
     * @Description: TODO 根据类型list查询开关列表
     * @Param [typeList]
     * @Date 2020/11/24 11:47
     */
    Map getSwitchByType(List typeList,String name);

    /**
     * @return void
     * @Author hf
     * @Title: initYjManageInfo
     * @Description: TODO (初始化信息)
     * @Param [list]
     * @Date 2020/11/24 16:57
     */
    void initYjManageInfo();

    /**
     * @Author hf
     * @Title: saveList
     * @Description: TODO (保存设置信息数据集)
     * @Param [list]
     * @return void
     * @Date 2020/11/24 19:44
     */
    void saveList(List<TSwitchTypeEntity> list);

    /**
     * @Author hf
     * @Title: getQoInfo
     * @Description: TODO (查询过滤条件)
     * @Param [asList]
     * @return com.mti.dao.qo.KmZdryPushToDsQo
     * @Date 2020/11/24 19:45
     */
    KmAlarmFilterQo getQoInfo(List<Integer> asList);

    void setTime(String startTime, String endTime);

}
