package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.KmProcessEntity;

public interface IKmProcessService extends IService<KmProcessEntity> {
}
