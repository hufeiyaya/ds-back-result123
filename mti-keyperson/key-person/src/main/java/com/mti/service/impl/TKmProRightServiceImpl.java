package com.mti.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mti.utils.GetRemoteIpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.component.snowflake.KeyWorker;
import com.mti.dao.model.LfZdryHistoryLogEntity;
import com.mti.dao.model.TKmProEntity;
import com.mti.dao.model.TTaskFileEntity;
import com.mti.dao.qo.TkmProRiQo;
import com.mti.exception.BusinessException;
import com.mti.jwt.AccountBo;
import com.mti.jwt.RequestUtils;
import com.mti.mapper.TKmProRightMapper;
import com.mti.service.LfZdryBaseService;
import com.mti.service.LfZdryHistoryLogService;
import com.mti.service.TKmProRightService;
import com.mti.service.TTaskFileService;

/**
 * @Classname TKmProRightServiceImpl
 * @Description TODO
 * @Date 2020/2/20 16:54
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmProRightServiceImpl extends ServiceImpl<TKmProRightMapper, TKmProEntity> implements TKmProRightService {
	@Autowired
	private TTaskFileService taskFileService;
	@Autowired
	private LfZdryHistoryLogService lfZdryHistoryLogService;
	@Autowired
	private LfZdryBaseService baseService;
	@Autowired
	private RequestUtils requestUtils;
	@Autowired
	private GetRemoteIpUtil getRemoteIpUtil;
	
    @Override
    public Object queryProRightRecordList(TkmProRiQo tkmProRiQo) {
        QueryWrapper<TKmProEntity> queryWrapper = new QueryWrapper<>();
        if(null != tkmProRiQo.getId())
            queryWrapper.lambda().eq(TKmProEntity::getId,tkmProRiQo.getId());
        if(null!=tkmProRiQo.getStart_time()&&tkmProRiQo.getStart_time().length()>0)
            queryWrapper.lambda().ge(TKmProEntity::getProRightTime,tkmProRiQo.getStart_time());
        if(null!=tkmProRiQo.getEnd_time())
             queryWrapper.lambda().le(TKmProEntity::getProRightTime,tkmProRiQo.getEnd_time());
        if(null!=tkmProRiQo.getProRightContent())
            queryWrapper.lambda().like(TKmProEntity::getProRightContent,tkmProRiQo.getProRightContent());
         if(null!=tkmProRiQo.getRefId())
             queryWrapper.lambda().eq(TKmProEntity::getRefId,tkmProRiQo.getRefId());
        queryWrapper.lambda().orderByDesc(TKmProEntity::getProRightTime);
        List<TKmProEntity> kmProEntitys = this.list(queryWrapper);
        //List<TKmProEntity> kmHandles = this.getBaseMapper().listTkmProHandle(tKmProHandle);
    	for (TKmProEntity kmProEntity : kmProEntitys) {
    		kmProEntity.setFileList(taskFileService.getByBussId(kmProEntity.getId().toString()));
		}
        return kmProEntitys;
    }

    @Override
    public IPage<TKmProEntity> queryProRightRecordPage(TkmProRiQo tkmProRiQo) {
        Page<TKmProEntity> page = new Page<>(tkmProRiQo.getStart(), tkmProRiQo.getSize());
        QueryWrapper<TKmProEntity> queryWrapper = new QueryWrapper<>();
        if(null != tkmProRiQo.getId())
            queryWrapper.lambda().eq(TKmProEntity::getId,tkmProRiQo.getId());
        if(null!=tkmProRiQo.getStart_time()&&tkmProRiQo.getStart_time().length()>0)
            queryWrapper.lambda().ge(TKmProEntity::getProRightTime,tkmProRiQo.getStart_time());
        if(null!=tkmProRiQo.getEnd_time())
            queryWrapper.lambda().le(TKmProEntity::getProRightTime,tkmProRiQo.getEnd_time());
        if(null!=tkmProRiQo.getProRightContent())
            queryWrapper.lambda().like(TKmProEntity::getProRightContent,tkmProRiQo.getProRightContent());
        if(null!=tkmProRiQo.getRefId())
            queryWrapper.lambda().eq(TKmProEntity::getRefId,tkmProRiQo.getRefId());
        queryWrapper.lambda().orderByDesc(TKmProEntity::getProRightTime);
        return this.page(page,queryWrapper);
    }
    
    @Override
    public boolean add(ServerWebExchange exchange,TKmProEntity tKmProEntity) {
    	AccountBo bo = requestUtils.getCurrentUser(exchange);
		if (bo == null) {
			throw new BusinessException(10019,"此账号已在别处登录，请查证");
		}
    	tKmProEntity.setId(String.valueOf(KeyWorker.nextId()));
    	boolean result = this.save(tKmProEntity);
    	List<TTaskFileEntity> fileList = new ArrayList<TTaskFileEntity>();
    	for (TTaskFileEntity fileEntity : tKmProEntity.getFileList()) {
    		TTaskFileEntity file =new TTaskFileEntity();
    		file.setId(String.valueOf(KeyWorker.nextId()));
    		file.setBussType("5");//5 信访类型
    		file.setBussId(tKmProEntity.getId().toString());
    		file.setFileUrl(fileEntity.getFileUrl());
    		file.setFileName(fileEntity.getFileName());
    		fileList.add(file);
		}
    	if(result) {
    		result =taskFileService.saveBatch(fileList);
    	}
    	//新增最后一次诉求
    	result = baseService.updateAppealById(tKmProEntity.getRefId(),tKmProEntity.getAppeal());
    	//历史信息表
        LfZdryHistoryLogEntity history = new LfZdryHistoryLogEntity();
        history.setId(tKmProEntity.getId().toString());
        history.setCreator(bo.getPersonBo().getName());
        history.setCreatorCode(bo.getPersonBo().getPoliceCode());
        history.setOperateDept(bo.getOrganizationBo().getShortName());
        history.setCreateTime(tKmProEntity.getProRightTime());
   		history.setSpId(tKmProEntity.getRefId());
   		history.setProvince(tKmProEntity.getProvince()) ;
   		history.setCity(tKmProEntity.getCity()) ;
   		history.setRegion(tKmProEntity.getRegion()) ;
   		history.setUnit(tKmProEntity.getUnit()) ;
   		history.setInsertTime(new Date());
   		history.setAppeal(tKmProEntity.getAppeal());
   		history.setAddition(tKmProEntity.getProRightContent());
   		history.setApproveResult("维权情况变更:"+tKmProEntity.getAppeal());
   		history.setFunctionModule("重点人员模块");

   		String remoteIp = getRemoteIpUtil.getRemoteIP(exchange.getRequest());
        history.setOperateIp(remoteIp);

   		history.setType(5);
   		result = lfZdryHistoryLogService.save(history);
    	return result;
    	
    	
    }
    
}
