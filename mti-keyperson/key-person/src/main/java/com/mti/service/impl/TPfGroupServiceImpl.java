package com.mti.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.component.snowflake.KeyWorker;
import com.mti.dao.model.TPfGroupEntity;
import com.mti.exception.BusinessException;
import com.mti.jwt.AccountBo;
import com.mti.jwt.RequestUtils;
import com.mti.mapper.TPfGroupMapper;
import com.mti.service.TPfGroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.server.ServerWebExchange;

import java.util.List;

/**
 * <p>
 * 重点人员分组表 服务实现类
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */
@Slf4j
@Service
public class TPfGroupServiceImpl extends ServiceImpl<TPfGroupMapper, TPfGroupEntity> implements TPfGroupService {

    @Autowired
    private RequestUtils requestUtils;

    @Override
    public boolean add(ServerWebExchange exchange, TPfGroupEntity tPfGroupEntity) {
        // 权限相关代码
        AccountBo bo = requestUtils.getCurrentUser(exchange);
        if (bo == null) {
            throw new BusinessException(10019, "此账号已在别处登录，请查证");
        }
        TPfGroupEntity entity = this.getOne(new QueryWrapper<TPfGroupEntity>().eq("group_name", tPfGroupEntity.getGroupName()).lambda());
        if (!ObjectUtils.isEmpty(entity)) {
            throw new BusinessException(10019, "分组已存在");
        }

        tPfGroupEntity.setAccountId(bo.getId());
        tPfGroupEntity.setId(String.valueOf(KeyWorker.nextId()));
        tPfGroupEntity.setDefault(false);
        return this.save(tPfGroupEntity);
    }

    @Override
    public boolean update(ServerWebExchange exchange, TPfGroupEntity tPfGroupEntity) {
        // 权限相关代码
        AccountBo bo = requestUtils.getCurrentUser(exchange);
        if (bo == null) {
            throw new BusinessException(10019, "此账号已在别处登录，请查证");
        }
        TPfGroupEntity entity = this.getOne(new QueryWrapper<TPfGroupEntity>().eq("id", tPfGroupEntity.getId()).lambda());
        if (ObjectUtils.isEmpty(entity)) {
            throw new BusinessException(10019, "分组不存在");
        }
        entity.setGroupName(tPfGroupEntity.getGroupName());
        return this.updateById(entity);
    }

    /**
     * 根据token获取当前用户的分组信息
     * 如果没有则建立一个默认的分组
     *
     * @param exchange
     * @return
     */
    @Override
    public List<TPfGroupEntity> getGroupInfoByAccId(ServerWebExchange exchange) {
        // 权限相关代码
        AccountBo bo = requestUtils.getCurrentUser(exchange);
        if (bo == null) {
            throw new BusinessException(10019, "此账号已在别处登录，请查证");
        }
        TPfGroupEntity tPfGroupEntity = this.getOne(new QueryWrapper<TPfGroupEntity>().eq("group_name", "默认分组").eq("account_id", bo.getId()).lambda());
        if (ObjectUtils.isEmpty(tPfGroupEntity)) {
            TPfGroupEntity entity = new TPfGroupEntity();
            entity.setId(String.valueOf(KeyWorker.nextId()));
            entity.setAccountId(bo.getId());
            entity.setDefault(true);
            entity.setGroupName("默认分组");
            this.save(entity);
        }
        return this.getBaseMapper().getGroupInfoByAccId(bo.getId());
    }


}


