package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfCodeDeviceEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface ILfCodeDeviceService extends IService<LfCodeDeviceEntity> {

}
