package com.mti.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.mti.configuration.SystemConfig;
import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.RylbRelationEntity;
import com.mti.jwt.BusinessException;
import com.mti.service.ILfZdryService;
import com.mti.service.IRylbRelationService;
import com.mti.service.IThirdService;
import com.mti.utils.AddHeaderInterceptor;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class ThirdServiceImpl implements  IThirdService  {
	
	 @Autowired
	 private IRylbRelationService service;
	
	 @Autowired
	 private SystemConfig config;
	 
	 @Autowired
	 private ILfZdryService lfZdryService;
	/**
	 * 解析推送数据
	 * @throws Exception
	 */
	@Override
	public Integer upData(List<LfZdryEntity> zdryList) {
		List<Map<String, Object>> list = new ArrayList<>();
		for (LfZdryEntity lfZdry : zdryList) {
			Map<String, Object> m = new HashMap<>();
			m.put("zdrybh",lfZdry.getId());
			m.put("xm",lfZdry.getXm());
			m.put("sfzh",lfZdry.getSfzh());
			m.put("lxdh",lfZdry.getSjh());
			m.put("lgsj",lfZdry.getCreate_timex());
			//通过昆明细类 查询省厅大类 小类
			List<Map<String, Object>> typeList = lfZdry.getTypeList();
			if(null != typeList && typeList.size() > 0){
				QueryWrapper<RylbRelationEntity> queryWrapper = new QueryWrapper<RylbRelationEntity>();
				queryWrapper.eq("km_ry_xl", typeList.get(0).get("xl"));
				RylbRelationEntity rylbRelationEntity = service.getOne(queryWrapper);
				m.put("gk_lb",rylbRelationEntity.getStdlcode());//大类====代码=====
				m.put("gklb_xl",rylbRelationEntity.getStxlcode());//小类====代码====
			}
//			QueryWrapper<RylbRelationEntity> queryWrapper = new QueryWrapper<RylbRelationEntity>();
//			queryWrapper.eq("km_ry_xl", lfZdry.getXl());
//			RylbRelationEntity rylbRelationEntity = service.getOne(queryWrapper);
//			m.put("gk_lb",rylbRelationEntity.getStdlcode());//大类====代码=====
//			m.put("gklb_xl",rylbRelationEntity.getStxlcode());//小类====代码====
			m.put("sslx","");  //涉事类型为空
			m.put("sjfl","101");//数据来源 101
			//挑头骨干	1	挑头
			//活跃人员	2	活跃
			//一般参与	3	关注    //两边代码一样，所以不用转换
			m.put("cycd",lfZdry.getRysx()); //参与程度====代码======
			//m.put("ryfl","");//人员分类  为空
			//管控级别======代码=======
			//11506	控制 ,     1   一级
			//11507	核查  ,    2  二级
			//11508	关注 ,     3  三级
			//11505	抓捕,      1
			if("11507".equals(lfZdry.getYjczlb())) {
				m.put("gkjb","2"); 
			}else if("11508".equals(lfZdry.getYjczlb())) {
				m.put("gkjb","3");
			}else if("11505".equals(lfZdry.getYjczlb()) || "11506".equals(lfZdry.getYjczlb())) {
				m.put("gkjb","1");
			}
			//风险评估 根据稳控状态来判断====代码===
			//失控	    1
			//查找中	2
			//已稳控	3
			if("失控".equals(lfZdry.getWkzt())) {
				m.put("fxpg",1);
			}else if("查找中".equals(lfZdry.getWkzt())) {
				m.put("fxpg",2);
			}else if("已稳控".equals(lfZdry.getWkzt())) {
				m.put("fxpg",3);
			}
			m.put("sq",""); //诉求为空
			m.put("wkqk",lfZdry.getWkzt());
			m.put("sdclqk","");//煽动串联情况 为空
			m.put("tsdwdm","101");
			m.put("tsdwmc","云南省昆明市");
			m.put("yxx",1);
			list.add(m);
		}
	    String data = JSONObject.toJSONString(list);
	    log.info("insert into list ==:"+list+"||"+config.getStUserName().trim()+"||"+config.getStAddressIp());
	    JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
	    Client client = dcf.createClient(config.getStAddressIp()+"/qbpt-webapi/skynet/bjjx?wsdl");
	    HTTPConduit http = (HTTPConduit) client.getConduit();
	    HTTPClientPolicy clientPolicy = new HTTPClientPolicy();
	    clientPolicy.setConnectionTimeout(6 * 100 * 1000);
	    clientPolicy.setAllowChunking(false);
	    clientPolicy.setReceiveTimeout(6 * 100 * 1000);
	    http.setClient(clientPolicy);
	    Endpoint endpoint = client.getEndpoint();
	    QName name = new QName(endpoint.getService().getName().getNamespaceURI(), "pushBjjxData");
	    client.getOutInterceptors().add(new AddHeaderInterceptor(config.getStUserName().trim()));
	    log.info("push data userName ==:"+config.getStUserName().trim()+"|IP|"+config.getStAddressIp()+"/qbpt-webapi/skynet/bjjx?wsdl");
	    Object[] obj = new Object[0];
	    try {
	        obj = client.invoke(name, data);
	        log.info("push data return obj==:"+obj);
	        //如果已上报成功，则对已上报的重点人员状态进行调整
	        if(obj!=null) {
	        	log.info("push data return obj[0]==:"+obj[0]);
	        	JSONObject json = JSONObject.parseObject(obj[0].toString());
	    		Integer status = (Integer)json.get("status");
	        	if(1==status) {
	        		for (LfZdryEntity lfZdry : zdryList) {
		        		UpdateWrapper<LfZdryEntity> wrapper = new UpdateWrapper<LfZdryEntity>();
	        			wrapper.eq("id", lfZdry.getId());
	                    wrapper.set("up_status",1); //1是已上报
	        			lfZdryService.update(wrapper);
	        		}
	        		return 1;
	        	}
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	        new BusinessException(10001,"上报异常："+e.getMessage());
	    }
	    return 0;
	}
	
}

