package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfRoadmonitorEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface ILfRoadmonitorService extends IService<LfRoadmonitorEntity> {

    List<LfRoadmonitorEntity> queryList(String cid);
}
