package com.mti.service.impl;

import com.mti.dao.model.TKmLgzsEntity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.mapper.TKmLgzsEntityMapper;
import com.mti.service.TKmLgzsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Classname TKmLgzsServiceImpl
 * @Description TODO
 * @Date 2020/1/17 23:18
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmLgzsServiceImpl implements TKmLgzsService {
    @Resource
    private TKmLgzsEntityMapper tKmLgzsEntityMapper;

    @Override
    public Integer insert(TKmLgzsEntity kmLgzsEntity) {
        return tKmLgzsEntityMapper.insert(kmLgzsEntity);
    }

    @Override
    public TKmLgzsEntity getByGlId(String glId) {
        return tKmLgzsEntityMapper.selectByPrimaryKey(glId);
    }

    @Override
    public List<TKmLgzsEntity> getLgzsListBySfzh(String sfzf) {
        return tKmLgzsEntityMapper.getLgzsListBySfzh(sfzf);
    }

    @Override
    public List<Map<String, Object>> queryWbLgKyNumber(LfRyyjsjQO lfRyyjsjQO) {
        return tKmLgzsEntityMapper.queryWbLgKyNumber(lfRyyjsjQO);
    }

    @Override
    public List<TKmLgzsEntity> getLgzsList(List<String> lgzsEntities) {
        return tKmLgzsEntityMapper.getLgzsList(lgzsEntities);
    }

    @Override
    public Map<String, Object> findLgzsStartAndEndTime(String wybs) {
        return tKmLgzsEntityMapper.findLgzsStartAndEndTime(wybs);
    }


}
