package com.mti.service.impl;

import com.mti.dao.model.KmZdryAssistEntity;
import com.mti.mapper.KmZdryAssistMapper;
import com.mti.service.KmZdryAssistService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 重点人员-辅助信息表 服务实现类
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Service
public class KmZdryAssistServiceImpl extends ServiceImpl<KmZdryAssistMapper, KmZdryAssistEntity> implements KmZdryAssistService {

}
