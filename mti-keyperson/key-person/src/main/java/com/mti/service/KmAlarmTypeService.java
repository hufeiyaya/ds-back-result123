package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.KmAlarmTypeEntity;
import com.mti.vo.KmAlarmVo;

/**
 * <p>
 * 手机预警和人脸识别预警设置过滤 接口类
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */

public interface KmAlarmTypeService extends IService<KmAlarmTypeEntity> {

    KmAlarmTypeEntity getAlarmStatusByName(String name);

    Object getAlarmStatus();

    void setAlarmStatus(KmAlarmVo kmAlarmVo);
}