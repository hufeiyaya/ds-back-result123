package com.mti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.dao.model.TKmProEntity;
import com.mti.dao.model.TKmProHandle;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.qo.TkmProHanQo;

import java.util.List;

import org.springframework.web.server.ServerWebExchange;

/**
 * <p>
 * 信访/聚访维权处理或化解信息 服务类
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
public interface TKmProHandleService extends IService<TKmProHandle> {
    /**
     * 查询处理和化解信息，不分页
     * @param tKmProHandle
     * @return
     */
    List<TKmProHandle> listTkmProHandle(TKmProHandle tKmProHandle);

    /**
     * 查询处理和化解信息，分页
     * @param tkmProHanQo
     * @return
     */
    IPage<TKmProHandle> pageTkmProHandle(TkmProHanQo tkmProHanQo);
    /**
             * 保存重点人员处理和备注
     * @param tKmProHandle
     * @return
     */
     boolean add(ServerWebExchange exchange,TKmProHandle tKmProHandle);
}
