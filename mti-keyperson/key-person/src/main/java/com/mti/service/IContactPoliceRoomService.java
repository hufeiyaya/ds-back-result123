package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.ContactPoliceRoomEntity;

/**
 * <p>
 *  警务室巡区信息 服务类
 * </p>
 *
 * @author zhaichen
 * @since 2019-08-15
 */
public interface IContactPoliceRoomService extends IService<ContactPoliceRoomEntity> {

}
