package com.mti.service;


import com.mti.dao.model.TKmLgzsEntity;
import com.mti.dao.qo.LfRyyjsjQO;

import java.util.List;
import java.util.Map;

/**
 * @Classname TKmLgzsService
 * @Description TODO
 * @Date 2020/1/17 23:18
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmLgzsService {
    Integer insert(TKmLgzsEntity kmLgzsEntity);

    TKmLgzsEntity getByGlId(String glId);

    List<TKmLgzsEntity> getLgzsListBySfzh(String sfzf);

    /**
     * 查询旅店撒点信息
     * @param lfRyyjsjQO
     * @return
     */
    List<Map<String, Object>> queryWbLgKyNumber(LfRyyjsjQO lfRyyjsjQO);

    /**
     * 获取旅馆住宿的集合
     * @param lgzsEntities
     * @return
     */
    List<TKmLgzsEntity> getLgzsList(List<String> lgzsEntities);

    /**
     * 查询旅馆的入住时间 和 退房时间
     * @param wybs
     * @return
     */
    Map<String, Object> findLgzsStartAndEndTime(String wybs);
}
