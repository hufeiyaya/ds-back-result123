/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.TQbptZlEntity;
import com.mti.dao.qo.TQbptQO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yangsp
 * @since 2019-06-25
 */
public interface ITQbptZlService extends IService<TQbptZlEntity> {

    IPage<TQbptZlEntity> queryPage(TQbptQO qo);

    int doData();

}
