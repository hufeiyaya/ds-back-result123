package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.kmmodel.KmZdryLxGxEntity;

/**
 * @Classname KmZdryLxGxService
 * @Description TODO
 * @Date 2020/1/10 22:54
 * @Created by duchaof
 * @Version 1.0
 */
public interface KmZdryLxGxService extends IService<KmZdryLxGxEntity> {
}
