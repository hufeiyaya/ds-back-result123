package com.mti.service;

import com.mti.dao.model.TkmPhonePositionEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.qo.LfZdryQO;
import org.springframework.web.server.ServerWebExchange;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 手机定位 服务类
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
public interface TKmPhonePositionService extends IService<TkmPhonePositionEntity> {

    /**
     * 获取手机定位
     * @param exchange
     * @param qo
     * @return
     */
    Object queryZdryPhonePosition(ServerWebExchange exchange, LfZdryQO qo);

    /**
     * 查询手机定位详情
     * @param sfzhs
     * @return
     */
    List<Map<String, Object>> queryPhoneLocationInfo(List<String> sfzhs);
}
