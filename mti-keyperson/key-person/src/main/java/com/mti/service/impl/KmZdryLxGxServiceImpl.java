package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.kmmodel.KmZdryLxGxEntity;
import com.mti.mapper.KmZdryLxGxMapper;
import com.mti.service.KmZdryLxGxService;
import org.springframework.stereotype.Service;

/**
 * @Classname KmZdryLxGxServiceImpl
 * @Description TODO
 * @Date 2020/1/10 22:55
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class KmZdryLxGxServiceImpl extends ServiceImpl<KmZdryLxGxMapper, KmZdryLxGxEntity> implements KmZdryLxGxService {
}
