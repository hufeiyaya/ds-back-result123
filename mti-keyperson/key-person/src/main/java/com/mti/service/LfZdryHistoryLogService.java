package com.mti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mti.dao.model.LfZdryHistoryLogEntity;
import com.mti.dao.qo.HistoryLogQO;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.vo.ZdryHistoryLogVo;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenf
 * @since 2020-03-24
 */
public interface LfZdryHistoryLogService extends IService<LfZdryHistoryLogEntity> {
	/**
	 * 5合1，通过类型查询历史记录
	 * @param logqo
	 * @return
	 */
	List<LfZdryHistoryLogEntity> getByType(HistoryLogQO logqo);

	/**
	 * 查询部门下重点人员的日志记录
	 * @param logqo
	 * @param exchange
	 * @return
	 */
    IPage<ZdryHistoryLogVo> getZdryHistiryLog(HistoryLogQO logqo, ServerWebExchange exchange);

	/**
	 * 导出重点人员日志记录
	 * @param logqo
	 * @param exchange
	 * @return
	 */
	Mono<DataBuffer> exportZdryHistoryLog(HistoryLogQO logqo, ServerWebExchange exchange);
}
