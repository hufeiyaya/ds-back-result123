package com.mti.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mti.component.snowflake.KeyWorker;
import com.mti.dao.kmmodel.KmZdryLxGxEntity;
import com.mti.dao.model.*;
import com.mti.enums.ZdryAssistType;
import com.mti.service.*;
import com.mti.vo.MessageText;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
@AllArgsConstructor
public class SpecialPersonComponent {
    private final IWebsocketService websocketService;
    private final IEventService eventService;
    private final ILfZdryModifyLogService logService;
    @Autowired
    private ISysDictService iSysDictService;

    public void insertEvent(LfZdryEntity zdryEntity){
        EventEntity eventEntity = new EventEntity();
        eventEntity.setEventId(String.valueOf(KeyWorker.nextId()));
        eventEntity.setPoliceMan(zdryEntity.getXm());
        eventEntity.setPhone(zdryEntity.getSjh());
        eventEntity.setAddress(zdryEntity.getHjd());
        eventEntity.setCreateTime(new Date());
        eventEntity.setStatus(0);
        eventEntity.setLatitude((double) 0);
        eventEntity.setLongitude((double) 0);
        eventEntity.setContent(zdryEntity.getHjd());
        eventEntity.setTitle(zdryEntity.getXm());
        eventEntity.setEventType("1");
        eventEntity.setWarning_id("");
        eventEntity.setWarning_type("失控报警");
        eventEntity.setSfzh(zdryEntity.getSfzh());
        eventEntity.setZrbm(zdryEntity.getZrbm());
        eventEntity.setMjId(zdryEntity.getMjid());
        eventEntity.setSendTime(LocalDateTime.now());
        eventService.saveOrUpdate(eventEntity);
        this.pushMessage(eventEntity);
        if(!StringUtils.isEmpty(zdryEntity.getMjid()))
            this.pushMessage2Mj(eventEntity,zdryEntity.getMjid());
    }


    private void pushMessage2Mj(EventEntity eventEntity, String mjID) {
        String jsonString = JSON.toJSONString(eventEntity);
        JSONObject jobj = JSON.parseObject(jsonString);
        jobj.put("create_time",eventEntity.getCreateTime().getTime());
        jobj.put("event_id",eventEntity.getEventId());
        jobj.put("event_type",eventEntity.getEventType());
        jobj.put("police_man",eventEntity.getPoliceMan());
        jobj.put("title_",eventEntity.getTitle());
        MessageText message = new MessageText();
        String out = jobj.toJSONString();
        log.info("send message===>{}",out);
        message.setContent(out);

        List<String> mjIDList = new ArrayList<>();
        mjIDList.add(mjID);

        websocketService.sendMessage2AppMj(message, mjIDList);
    }

    private void pushMessage(EventEntity eventEntity) {
        String jsonString = JSON.toJSONString(eventEntity);
        JSONObject jobj = JSON.parseObject(jsonString);
        jobj.put("create_time",eventEntity.getCreateTime().getTime());
        jobj.put("event_id",eventEntity.getEventId());
        jobj.put("event_type",eventEntity.getEventType());
        jobj.put("police_man",eventEntity.getPoliceMan());
        jobj.put("title_",eventEntity.getTitle());
        MessageText message = new MessageText();
        String out = jobj.toJSONString();
        log.info("send message===>{}",out);
        message.setContent(out);
        websocketService.sendMessageTextToAll(message);
    }

    /**
     * 姓名、手机号、户籍地、现住地、人员类型、
     * 人员细类、管控分局、责任单位、责任民警、民警联系方式、
     * 备注
     * @param modifiedEntity 变更后实体
     * @param entity 未变更实体
     */
    public void recordModifyColumnLog(LfZdryBaseEntity modifiedEntity,LfZdryBaseEntity entity){
        if(modifiedEntity==null || entity==null)
            return;
        	String log = getModifyInfo(modifiedEntity, entity);
            if(log.length()>0){
	            LfZdryModifyLogEntity modifyLogEntity = new LfZdryModifyLogEntity();
	            modifyLogEntity.setCreateTime(LocalDateTime.now());
	            modifyLogEntity.setModifyTime(LocalDateTime.now());
	            modifyLogEntity.setCreator(modifiedEntity.getModifier());
	            modifyLogEntity.setId(String.valueOf(KeyWorker.nextId()));
	            modifyLogEntity.setModifier(modifiedEntity.getModifier());
	            modifyLogEntity.setZdryId(modifiedEntity.getId());
	            modifyLogEntity.setLog(log);
	            //modifyLogEntity.setXgyy(modifiedEntity.getXgyy());
	            logService.save(modifyLogEntity);
            }
    }
            
        public String getModifyInfo(LfZdryBaseEntity modifiedEntity, LfZdryBaseEntity entity){
            StringBuilder sb = new StringBuilder();
            if(StringUtils.isEmpty(entity)){
                sb.append("新增");
            if(null!=modifiedEntity.getTx() && !modifiedEntity.getTx().equalsIgnoreCase(entity.getTx()))
            	sb.append(",头像新增"); 
            }else{
                // 姓名
                if(null!=modifiedEntity.getXm() && !modifiedEntity.getXm().equalsIgnoreCase(entity.getXm()))
                    sb.append("姓名由【").append(entity.getXm()).append("】变更为【").append(modifiedEntity.getXm()).append("】");
               //  头像
                if(null!=modifiedEntity.getTx() && !modifiedEntity.getTx().equalsIgnoreCase(entity.getTx()))
                    sb.append("替换头像");
                // 户籍省
                if(null != modifiedEntity.getHjs() && !modifiedEntity.getHjs().equalsIgnoreCase(entity.getHjs())){
                    if(sb.length()>0)
                        sb.append("，").append("人员户籍省由【").append(entity.getHjs()).append("】变更为【").append(modifiedEntity.getHjs()).append("】");
                    else
                        sb.append("人员户籍省由【").append(entity.getHjs()).append("】变更为【").append(modifiedEntity.getHjs()).append("】");
                }
                // 户籍市
                if(null != modifiedEntity.getHjss() && !modifiedEntity.getHjss().equalsIgnoreCase(entity.getHjss())){
                    if(sb.length()>0)
                        sb.append("，").append("人员户籍市由【").append(entity.getHjss()).append("】变更为【").append(modifiedEntity.getHjss()).append("】");
                    else
                        sb.append("人员户籍市由【").append(entity.getHjss()).append("】变更为【").append(modifiedEntity.getHjss()).append("】");
                }
                // 户籍县
                if(null != modifiedEntity.getHjx() && !modifiedEntity.getHjx().equalsIgnoreCase(entity.getHjx())){
                    if(sb.length()>0)
                        sb.append("，").append("人员户籍县由【").append(entity.getHjx()).append("】变更为【").append(modifiedEntity.getHjx()).append("】");
                    else
                        sb.append("人员户籍县由【").append(entity.getHjx()).append("】变更为【").append(modifiedEntity.getHjx()).append("】");
                }

                // 户籍地
                if (null!=modifiedEntity.getHjd()&&!modifiedEntity.getHjd().equalsIgnoreCase(entity.getHjd())){
                    if(sb.length()>0)
                        sb.append("，").append("户籍地由【").append(entity.getHjd()).append("】变更为【").append(modifiedEntity.getHjd()).append("】");
                    else
                        sb.append("户籍地由【").append(entity.getHjd()).append("】变更为【").append(modifiedEntity.getHjd()).append("】");
                }
                // 居住地
                if(null != modifiedEntity.getJzdList() && null != entity.getJzdList() ){
                    for(KmZdryHomeEntity zdry : modifiedEntity.getJzdList()){
                        if(null == zdry.getId()){ //新增地址
                            if(sb.length()>0)
                                sb.append(", ").append("新增居住地 【").append(zdry.getXzzs()).append(zdry.getXzzss())
                                        .append(zdry.getXzzx()).append(zdry.getXzz()).append("】");
                            else
                                sb.append("新增居住地 【").append(zdry.getXzzs()).append(zdry.getXzzss())
                                        .append(zdry.getXzzx()).append(zdry.getXzz()).append("】");
                        }else{ // 编辑
                            loop :for(KmZdryHomeEntity orZdry : entity.getJzdList()){
                                if(zdry.getId().equals(orZdry.getId())){
                                    //判断是否是删除居住地址
                                	if(StringUtils.isEmpty(zdry.getXzz()) && StringUtils.isEmpty(zdry.getXzzs()) || StringUtils.isEmpty(zdry.getXzzss()) || StringUtils.isEmpty(zdry.getXzzx()) ){
                                   // if(null == zdry.getXzz()  && null == zdry.getXzzs() || null == zdry.getXzzss() || null == zdry.getXzzx()){
                                        if(sb.length()>0) {
                                            sb.append(", ").append("删除居住地 【").append(orZdry.getXzzs()).append(orZdry.getXzzss())
                                                    .append(orZdry.getXzzx()).append(orZdry.getXzz()).append("】");
                                        }else {
                                            sb.append("删除居住地 【").append(orZdry.getXzzs()).append(orZdry.getXzzss())
                                                    .append(orZdry.getXzzx()).append(orZdry.getXzz()).append("】");
                                        }
                                        break loop;
                                    }
                                    if(!(zdry.getXzzs().equalsIgnoreCase(orZdry.getXzzs()) && zdry.getXzzss().equalsIgnoreCase(orZdry.getXzzss())
                                            && zdry.getXzzx().equalsIgnoreCase(orZdry.getXzzx()) && zdry.getXzz().equalsIgnoreCase(orZdry.getXzz()))) {
                                        if (sb.length() > 0)
                                            sb.append(", ").append("居住地由 【").append(orZdry.getXzzs()).append(orZdry.getXzzss())
                                                    .append(orZdry.getXzzx()).append(orZdry.getXzz()).append("】").append(" 修改为 【")
                                                    .append(zdry.getXzzs()).append(zdry.getXzzss()).append(zdry.getXzzx()).append(zdry.getXzz()).append("】");
                                        else
                                            sb.append("居住地由 【").append(orZdry.getXzzs()).append(orZdry.getXzzss())
                                                    .append(orZdry.getXzzx()).append(orZdry.getXzz()).append("】").append(" 修改为 【")
                                                    .append(zdry.getXzzs()).append(zdry.getXzzss()).append(zdry.getXzzx()).append(zdry.getXzz()).append("】");
                                    }

                                }
                            }
                        }
                    }
                }

                // 管控责任分局
                if (null!=modifiedEntity.getSsxq()&&!modifiedEntity.getSsxq().equalsIgnoreCase(entity.getSsxq())){
                    if(sb.length()>0)
                        sb.append("，").append("管控分局由【").append(entity.getSsxq()).append("】变更为【").append(modifiedEntity.getSsxq()).append("】");
                    else
                        sb.append("管控分局由【").append(entity.getSsxq()).append("】变更为【").append(modifiedEntity.getSsxq()).append("】");
                }
                // 责任派出所
                if (null!=modifiedEntity.getSspcs() && !modifiedEntity.getSspcs().equalsIgnoreCase(entity.getSspcs())){
                    if(sb.length()>0)
                        sb.append("，").append("责任派出所由【").append(entity.getSspcs()).append("】变更为【").append(modifiedEntity.getSspcs()).append("】");
                    else
                        sb.append("责任派出所由【").append(entity.getSspcs()).append("】变更为【").append(modifiedEntity.getSspcs()).append("】");
                }
                
                // 管控责任州市
                if (null!=modifiedEntity.getSszsmc()&&!modifiedEntity.getSszsmc().equalsIgnoreCase(entity.getSszsmc())){
                    if(sb.length()>0)
                        sb.append("，").append("管控责任州市由【").append(entity.getSszsmc()).append("】变更为【").append(modifiedEntity.getSszsmc()).append("】");
                    else
                        sb.append("管控责任州市由【").append(entity.getSszsmc()).append("】变更为【").append(modifiedEntity.getSszsmc()).append("】");
                }
                // 管控民警
                if (null!=modifiedEntity.getGkmj()&&!modifiedEntity.getGkmj().equalsIgnoreCase(entity.getGkmj())){
                    if(sb.length()>0)
                        sb.append("，").append("责任民警由【").append(entity.getGkmj()).append("】变更为【").append(modifiedEntity.getGkmj()).append("】");
                    else
                        sb.append("责任民警由【").append(entity.getGkmj()).append("】变更为【").append(modifiedEntity.getGkmj()).append("】");
                }
                // 民警联系电话
                if (null!=modifiedEntity.getMjlxdh()&&!modifiedEntity.getMjlxdh().equalsIgnoreCase(entity.getMjlxdh())){
                    if(sb.length()>0)
                        sb.append("，").append("民警联系方式由【").append(entity.getMjlxdh()).append("】变更为【").append(modifiedEntity.getMjlxdh()).append("】");
                    else
                        sb.append("民警联系方式由【").append(entity.getMjlxdh()).append("】变更为【").append(modifiedEntity.getMjlxdh()).append("】");
                }

                // 风险等级
                if(null != modifiedEntity.getGkjb() && !modifiedEntity.getGkjb().equalsIgnoreCase(entity.getGkjb())){
                    if(sb.length()>0)
                        sb.append("，").append("风险等级由【").append(entity.getGkjb()).append("】变更为【").append(modifiedEntity.getGkjb()).append("】");
                    else
                        sb.append("风险等级由【").append(entity.getGkjb()).append("】变更为【").append(modifiedEntity.getGkjb()).append("】");
                }
                // 人员属性
                if(null!= modifiedEntity.getRysx() && modifiedEntity.getRysx()!= entity.getRysx()){
                    SysDictEntity moRysx = iSysDictService.getEntityByKey("zdry_ryxx",String.valueOf(modifiedEntity.getRysx()));
                    SysDictEntity orRysx = iSysDictService.getEntityByKey("zdry_ryxx",String.valueOf(entity.getRysx()));
                    String orRysxVal = "空";
                    if(orRysx!=null) {
                    	orRysxVal=orRysx.getValue();
                    }
                    if(sb.length()>0)
                        sb.append("，").append("人员属性由【").append(orRysxVal).append("】变更为【").append(moRysx.getValue()).append("】");
                    else
                        sb.append("人员属性由【").append(orRysxVal).append("】变更为【").append(moRysx.getValue()).append("】");
                }
                // 预警处置级别
                if(null != modifiedEntity.getYjczlb() && !modifiedEntity.getYjczlb().equals(entity.getYjczlb())){
                    SysDictEntity moYjczlb = iSysDictService.getEntityByKey("zdry_yjczlb",String.valueOf(modifiedEntity.getYjczlb()));
                    SysDictEntity orYjczlb = iSysDictService.getEntityByKey("zdry_yjczlb",String.valueOf(entity.getYjczlb()));
                    String orYjczlbVal = "空";
                    if(orYjczlb!=null) {
                    	orYjczlbVal=orYjczlb.getValue();
                    }
                    if(sb.length()>0)
                        sb.append("，").append("人员预警处置类别由【").append(orYjczlbVal).append("】变更为【").append(moYjczlb.getValue()).append("】");
                    else
                        sb.append("人员预警处置类别由【").append(orYjczlbVal).append("】变更为【").append(moYjczlb.getValue()).append("】");
                }
                // 稳控状态/zdry_wkzt
                if(!StringUtils.isEmpty(modifiedEntity.getWkzt()) && !modifiedEntity.getWkzt().equalsIgnoreCase(entity.getWkzt())){
//                    SysDictEntity moWkzt = iSysDictService.getEntityByKey("zdry_wkzt",modifiedEntity.getWkzt());
//                    SysDictEntity orWkzt = iSysDictService.getEntityByKey("zdry_wkzt",entity.getWkzt());
                    if(sb.length()>0)
                        sb.append("，").append("稳控状态由【").append(entity.getWkzt()).append("】变更为【").append(modifiedEntity.getWkzt()).append("】");
                    else
                        sb.append("稳控状态由【").append(entity.getWkzt()).append("】变更为【").append(modifiedEntity.getWkzt()).append("】");
                }
                //附件历史记录
                if(null != modifiedEntity.getFileList()&& modifiedEntity.getFileList().size()>0) {
                	for(TTaskFileEntity file : modifiedEntity.getFileList()){
                		if(null == file.getId()){ // 新增
                			if(sb.length()>0)
                                sb.append("，").append("新增附件【").append(file.getFileName()).append("】");
                            else
                                sb.append("新增附件【").append(file.getFileName()).append("】");
                		}else {
                			for(TTaskFileEntity oldFile : entity.getFileList()){
                  			   if(file.getId().equals(oldFile.getId())){
                  				   if(!file.getFileUrl().equalsIgnoreCase(oldFile.getFileUrl())){
	                  					if(sb.length()>0) {
	                  						if(StringUtils.isEmpty(file.getFileName())) {
	                  							sb.append("，").append("删除附件【").append(oldFile.getFileName()).append("】");
	                  						}else {
	                  							sb.append("，").append("附件由【").append(oldFile.getFileName()).append("】变更为 【")
	                  							.append(file.getFileName()).append("】");
	                  						}
	                  						
	                  					}else {
	                  						if(StringUtils.isEmpty(file.getFileName())) {
	                  							sb.append("删除附件【").append(oldFile.getFileName()).append("】");
	                  						}else {
	                  							sb.append("附件由【").append(oldFile.getFileName()).append("】变更为 【")
	                  							.append(file.getFileName()).append("】");
	                  						}
	         						    }
                  				 }  
                  			  }
                  			}
                		}
                	}
                }
                
                //辅助信息
                if(null != modifiedEntity.getAssistList()&& modifiedEntity.getAssistList().size()>0){
                   for(KmZdryAssistEntity assist : modifiedEntity.getAssistList()){
                	   
                       if(null == assist.getId()){ // 新增
                           if(StringUtils.isEmpty(assist.getInfoValue())) continue;
                           switch (assist.getInfoType()){
                               case 1:{ //电话
                                   if(sb.length()>0)
                                       sb.append("，").append("新增电话号码【").append(assist.getInfoValue()).append("】");
                                   else
                                       sb.append("新增电话号码【").append(assist.getInfoValue()).append("】");
                               }break;
                               case 2 :{ // 微信号码
                                   if(sb.length()>0)
                                       sb.append("，").append("新增微信号码【").append(assist.getInfoValue()).append("】");
                                   else
                                       sb.append("新增微信号码【").append(assist.getInfoValue()).append("】");
                               }break;
                               case 3 :{ // qq号码
                                   if(sb.length()>0)
                                       sb.append("，").append("新增qq号码【").append(assist.getInfoValue()).append("】");
                                   else
                                       sb.append("新增qq号码【").append(assist.getInfoValue()).append("】");
                               }break;
                               case 4 :{ // 名下车辆号牌
                                   if(sb.length()>0)
                                       sb.append("，").append("新增名下车辆号牌【").append(assist.getInfoValue()).append("】");
                                   else
                                       sb.append("新增名下车辆号牌【").append(assist.getInfoValue()).append("】");
                               }break;
                               case 5 :{ // 实际车辆号牌
                                   if(sb.length()>0)
                                       sb.append("，").append("新增实际车辆号牌【").append(assist.getInfoValue()).append("】");
                                   else
                                       sb.append("新增实际车辆号牌【").append(assist.getInfoValue()).append("】");
                               }break;
                               case 6 :{ // 出入境证件
                                   if(sb.length()>0)
                                       sb.append("，").append("新增出入境证件【").append(assist.getInfoValue()).append("】");
                                   else
                                       sb.append("新增出入境证件【").append(assist.getInfoValue()).append("】");
                               }break;
                           }
                       }else{ // 编辑
                    	   if(null!=entity.getAssistList()) {
                    		   for(KmZdryAssistEntity orAssist : entity.getAssistList()){
                    			   if(assist.getId().equals(orAssist.getId())){
                    				   if(null != assist.getInfoType() && !StringUtils.isEmpty(assist.getInfoValue()) && !orAssist.getInfoValue().equalsIgnoreCase(assist.getInfoValue())){
                    					   switch (assist.getInfoType()){
                    					   case 1 :{ // 电话号码
                    						   if(sb.length()>0)
                    							   sb.append("，").append("电话号码由【").append(orAssist.getInfoValue()).append("】变更为 【")
                    							   .append(assist.getInfoValue()).append("】");
                    						   else
                    							   sb.append("电话号码由【").append(orAssist.getInfoValue()).append("】变更为 【")
                    							   .append(assist.getInfoValue()).append("】");
                    					   }break;
                    					   case 2 :{ //微信号码
                    						   if(sb.length()>0)
                    							   sb.append("，").append("微信号码由【").append(orAssist.getInfoValue()).append("】变更为 【")
                    							   .append(assist.getInfoValue()).append("】");
                    						   else
                    							   sb.append("微信号码由【").append(orAssist.getInfoValue()).append("】变更为 【")
                    							   .append(assist.getInfoValue()).append("】");
                    					   }break;
                    					   case 3 :{ // qq号码
                    						   if(sb.length()>0)
                    							   sb.append("，").append("qq号码由【").append(orAssist.getInfoValue()).append("】变更为 【")
                    							   .append(assist.getInfoValue()).append("】");
                    						   else
                    							   sb.append("qq号码由【").append(orAssist.getInfoValue()).append("】变更为 【")
                    							   .append(assist.getInfoValue()).append("】");
                    					   }break;
                    					   case 4 :{ //名下车辆号牌
                    						   if(sb.length()>0)
                    							   sb.append("，").append("名下车牌号由【").append(orAssist.getInfoValue()).append("】变更为 【")
                    							   .append(assist.getInfoValue()).append("】");
                    						   else
                    							   sb.append("名下车牌号由【").append(orAssist.getInfoValue()).append("】变更为 【")
                    							   .append(assist.getInfoValue()).append("】");
                    					   }break;
                    					   case 5 :{ // 实际车辆号牌
                    						   if(sb.length()>0)
                    							   sb.append("，").append("实际车辆号牌由【").append(orAssist.getInfoValue()).append("】变更为 【")
                    							   .append(assist.getInfoValue()).append("】");
                    						   else
                    							   sb.append("实际车辆号牌由【").append(orAssist.getInfoValue()).append("】变更为 【")
                    							   .append(assist.getInfoValue()).append("】");
                    					   }break;
                    					   case 6 :{ // 出入境证件
                                                   if(sb.length()>0)
                                                       sb.append("，").append("出入境证件由【").append(orAssist.getInfoValue()).append("】变更为 【")
                                                               .append(assist.getInfoValue()).append("】");
                                                   else
                                                       sb.append("出入境证件由【").append(orAssist.getInfoValue()).append("】变更为 【")
                                                               .append(assist.getInfoValue()).append("】");
                                               }break;
                    					   }
                    				   }
                    				   //辅助信息删除记录
                    				   if(null != assist.getInfoType() && StringUtils.isEmpty(assist.getInfoValue())){
                    					   switch (assist.getInfoType()){
                    					   case 1 :{ // 电话号码
                    						   if(sb.length()>0)
                    							   sb.append("，").append("删除电话号码 【").append(orAssist.getInfoValue()).append("】 ");
                    						   else
                    							   sb.append("删除电话号码 【").append(orAssist.getInfoValue()).append("】 ");

                    					   }break;
                    					   case 2 :{ //微信号码
                    						   if(sb.length()>0)
                    							   sb.append("，").append("删除微信号码【").append(orAssist.getInfoValue()).append("】");
                    						   else
                    							   sb.append("删除微信号码【").append(orAssist.getInfoValue()).append("】");

                    					   }break;
                    					   case 3 :{ // qq号码
                    						   if(sb.length()>0)
                    							   sb.append("，").append("删除qq号码【").append(orAssist.getInfoValue()).append("】");
                    						   else
                    							   sb.append("qq号码由【").append(orAssist.getInfoValue()).append("】变更为 【")
                    							   .append(assist.getInfoValue()).append("】");
                    					   }break;
                    					   case 4 :{ //名下车辆号牌
                    						   if(sb.length()>0)
                    							   sb.append("，").append("删除名下车牌号【").append(orAssist.getInfoValue()).append("】");
                    						   else
                    							   sb.append("删除名下车牌号【").append(orAssist.getInfoValue()).append("】");

                    					   }break;
                    					   case 5 :{ // 实际车辆号牌
                    						   if(sb.length()>0)
                    							   sb.append("，").append("删除实际车辆号牌【").append(orAssist.getInfoValue()).append("】");
                    						   else
                    							   sb.append("删除实际车辆号牌【").append(orAssist.getInfoValue()).append("】");

                    					   }break;
                                           case 6 :{ // 出入境证件
                                                   if(sb.length()>0)
                                                       sb.append("，").append("删除出入境证件【").append(orAssist.getInfoValue()).append("】");
                                                   else
                                                       sb.append("删除出入境证件【").append(orAssist.getInfoValue()).append("】");

                                            }break;
                    					   }
                    				   }
                    			   }
                    		   }
                    	   }
                       }
                   }
                }
                // 备注
                if (entity.getBz()==null && modifiedEntity.getBz()!=null){
                    if(sb.length()>0)
                        sb.append("，").append("新增备注【").append(modifiedEntity.getBz()).append("】");
                    else
                        sb.append("新增备注【").append(modifiedEntity.getBz()).append("】");
                }
               // 备注
                if (entity.getBz()!= null && !modifiedEntity.getBz().equalsIgnoreCase(entity.getBz())){
                    if(sb.length()>0)
                        sb.append("，").append("修改备注【").append(modifiedEntity.getBz()).append("】");
                    else
                        sb.append("修改备注【").append(modifiedEntity.getBz()).append("】");
                }
                // 大类 小类 和 对应警种
                if(null != modifiedEntity.getTypeList() && modifiedEntity.getTypeList().size()>0){
                    for(KmZdryTypeEntity type : modifiedEntity.getTypeList()){
                        if(null == type.getId()){
                            if(sb.length()>0)
                                sb.append("，").append("新增人员大类【").append(type.getRylb()).append("】 人员小类 【").append(type.getXl())
                                        .append("】 对应的警种为 【").append(type.getZrbm()).append("】");
                            else
                                sb.append("新增人员大类【").append(type.getRylb()).append("】 人员小类 【").append(type.getXl())
                                        .append("】 对应的警种为 【").append(type.getZrbm()).append("】");
                        }else{
                            if(null != entity.getTypeList() && entity.getTypeList().size() >0){
                                for(KmZdryTypeEntity orType : entity.getTypeList()){
                                    if(type.getZrbm()!=null) {
                                        if(type.getId().equals(orType.getId())){
                                            if(!type.getRylb().equalsIgnoreCase(orType.getRylb())){
                                                if(sb.length()>0)
                                                    sb.append("，").append("人员大类由【").append(orType.getRylb()).append("】变更为 【").append(type.getRylb()).append("】");
                                                else
                                                    sb.append("人员大类由【").append(orType.getRylb()).append("】变更为 【").append(type.getRylb()).append("】");
                                            }
                                            if(!type.getXl().equalsIgnoreCase(orType.getXl())){
                                                if(sb.length()>0)
                                                    sb.append("，").append("人员小类由【").append(orType.getXl()).append("】变更为 【").append(type.getXl()).append("】");
                                                else
                                                    sb.append("人员小类由【").append(orType.getXl()).append("】变更为 【").append(type.getXl()).append("】");
                                            }
                                            if(!type.getZrbm().equalsIgnoreCase(orType.getZrbm())){
                                                if(sb.length()>0)
                                                    sb.append("，").append("警种由【").append(orType.getZrbm()).append("】变更为 【").append(type.getZrbm()).append("】");
                                                else
                                                    sb.append("警种由【").append(orType.getZrbm()).append("】变更为 【").append(type.getZrbm()).append("】");
                                            }
                                        }
                                    }else {
                                        if(type.getId().equals(orType.getId())){
                                            if(sb.length()>0)
                                                sb.append("，").append("删除人员大类【").append(orType.getRylb()).append("】").append("人员小类由【").append(orType.getXl()).append("】对应警种【").append(orType.getZrbm()).append("】");
                                            else
                                                sb.append("删除人员大类【").append(orType.getRylb()).append("】").append("人员小类由【").append(orType.getXl()).append("】对应警种【").append(orType.getZrbm()).append("】");
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
                
             // 自定义类型
                if(null != modifiedEntity.getCustomList() && modifiedEntity.getCustomList().size()>0){
                    for(KmZdryCustomEntity custom : modifiedEntity.getCustomList()){
                        if(null == custom.getId()){
                            if(sb.length()>0)
                                sb.append("，").append("新增自定义字段【").append(custom.getCustomKey()).append("】值为: 【").append(custom.getCustomValue()).append("】");
                            else
                                sb.append("新增自定义字段【").append(custom.getCustomKey()).append("】值为: 【").append(custom.getCustomValue()).append("】");
                        }else{  //修改自定义字段记录
                        	  if(null!=entity.getCustomList()) {
                        		for(KmZdryCustomEntity oldCustom : entity.getCustomList()){
		                        	if(custom.getCustomKey()!=null) {
		                        		if(custom.getId().equals(oldCustom.getId())){
		                        			if(!(custom.getCustomKey()+custom.getCustomValue()).equalsIgnoreCase(oldCustom.getCustomKey()+oldCustom.getCustomValue())){
		                        				if(sb.length()>0)
		                        					sb.append("，").append("新增自定义字段【").append(custom.getCustomKey()).append("】值为: 【").append(custom.getCustomValue()).append("】").append("】变更为 【").append("字段【").append(oldCustom.getCustomKey()).append("】值为: 【").append(oldCustom.getCustomValue()).append("】");
		                        				else
		                        					sb.append("新增自定义字段【").append(custom.getCustomKey()).append("】值为: 【").append(custom.getCustomValue()).append("】").append("】变更为 【").append("字段【").append(oldCustom.getCustomKey()).append("】值为: 【").append(oldCustom.getCustomValue()).append("】");
		                        			}
		                        		}
		                        	}else {//删除自定义字段记录
		                        		if(sb.length()>0)
                        					sb.append("，").append("删除自定义字段【").append(oldCustom.getCustomKey()).append("】值为: 【").append(oldCustom.getCustomValue()).append("】");

                        				else
                        					sb.append("删除自定义字段【").append(oldCustom.getCustomKey()).append("】值为: 【").append(oldCustom.getCustomValue()).append("】");

 		                        	}
                        		}
                        	}	//---
                        }
                    }
                }
            }

            //诉求
            if(null != modifiedEntity.getAppeal() && !modifiedEntity.getAppeal().equals(entity.getAppeal())){
                if(sb.length()>0)
                    sb.append("，").append("诉求【").append(entity.getAppeal()).append("】变更为【").append(modifiedEntity.getAppeal()).append("】");
                else
                    sb.append("诉求由【").append(entity.getAppeal()).append("】变更为【").append(modifiedEntity.getAppeal()).append("】");
            }
            return sb.toString(); 
        }
        //移交修变更记录
        public String getTransferInfo(LfZdryBaseEntity modifiedEntity,LfZdryBaseEntity entity){
            StringBuilder sb = new StringBuilder();
            // 户籍省
            if(null != modifiedEntity.getHjs() && !modifiedEntity.getHjs().equalsIgnoreCase(entity.getHjs())){
                if(sb.length()>0)
                    sb.append("，").append("人员户籍省由【").append(entity.getHjs()).append("】变更为【").append(modifiedEntity.getHjs()).append("】");
                else
                    sb.append("人员户籍省由【").append(entity.getHjs()).append("】变更为【").append(modifiedEntity.getHjs()).append("】");
            }
            // 户籍市
            if(null != modifiedEntity.getHjss() && !modifiedEntity.getHjss().equalsIgnoreCase(entity.getHjss())){
                if(sb.length()>0)
                    sb.append("，").append("人员户籍市由【").append(entity.getHjss()).append("】变更为【").append(modifiedEntity.getHjss()).append("】");
                else
                    sb.append("人员户籍市由【").append(entity.getHjss()).append("】变更为【").append(modifiedEntity.getHjss()).append("】");
            }
            // 户籍县
            if(null != modifiedEntity.getHjx() && !modifiedEntity.getHjx().equalsIgnoreCase(entity.getHjx())){
                if(sb.length()>0)
                    sb.append("，").append("人员户籍县由【").append(entity.getHjx()).append("】变更为【").append(modifiedEntity.getHjx()).append("】");
                else
                    sb.append("人员户籍县由【").append(entity.getHjx()).append("】变更为【").append(modifiedEntity.getHjx()).append("】");
            }

            // 户籍地
            if (null!=modifiedEntity.getHjd()&&!modifiedEntity.getHjd().equalsIgnoreCase(entity.getHjd())){
                if(sb.length()>0)
                    sb.append("，").append("户籍地由【").append(entity.getHjd()).append("】变更为【").append(modifiedEntity.getHjd()).append("】");
                else
                    sb.append("户籍地由【").append(entity.getHjd()).append("】变更为【").append(modifiedEntity.getHjd()).append("】");
            }
            // 居住省
            if(null != modifiedEntity.getXzzs() && !modifiedEntity.getXzzs().equalsIgnoreCase(entity.getXzzs())){
                if(sb.length()>0)
                    sb.append("，").append("人员居住省由【").append(entity.getXzzs()).append("】变更为【").append(modifiedEntity.getXzzs()).append("】");
                else
                    sb.append("人员居住省由【").append(entity.getXzzs()).append("】变更为【").append(modifiedEntity.getXzzs()).append("】");
            }
            // 居住市
            if(null != modifiedEntity.getXzzss() && !modifiedEntity.getXzzss().equalsIgnoreCase(entity.getXzzss())){
                if(sb.length()>0)
                    sb.append("，").append("人员居住市由【").append(entity.getXzzss()).append("】变更为【").append(modifiedEntity.getXzzss()).append("】");
                else
                    sb.append("人员居住市由【").append(entity.getXzzss()).append("】变更为【").append(modifiedEntity.getXzzss()).append("】");
            }
            // 居住县
            if(null != modifiedEntity.getXzzx() && !modifiedEntity.getXzzx().equalsIgnoreCase(entity.getXzzx())){
                if(sb.length()>0)
                    sb.append("，").append("人员居住县由【").append(entity.getXzzx()).append("】变更为【").append(modifiedEntity.getXzzx()).append("】");
                else
                    sb.append("人员居住县由【").append(entity.getXzzx()).append("】变更为【").append(modifiedEntity.getXzzx()).append("】");
            }

            // 居住地
            if (null!=modifiedEntity.getXzz()&&!modifiedEntity.getXzz().equalsIgnoreCase(entity.getXzz())){
                if(sb.length()>0)
                    sb.append("，").append("居住地由【").append(entity.getXzz()).append("】变更为【").append(modifiedEntity.getXzz()).append("】");
                else
                    sb.append("居住地由【").append(entity.getXzz()).append("】变更为【").append(modifiedEntity.getXzz()).append("】");
            }
            if (null!=modifiedEntity.getSsxq()&&!modifiedEntity.getSsxq().equalsIgnoreCase(entity.getSsxq())){
                sb.append("管控分局由【").append(entity.getSsxq()).append("】移交至【").append(modifiedEntity.getSsxq()).append("】");
            }
            if (null!=modifiedEntity.getSspcs() && !(modifiedEntity.getSsxq()+modifiedEntity.getSspcs()).equalsIgnoreCase(entity.getSsxq()+entity.getSspcs())){
                if(sb.length()>0)
                    sb.append("，").append("责任派出所由【").append(entity.getSspcs()).append("】移交至【").append(modifiedEntity.getSspcs()).append("】");
                else
                    sb.append("责任派出所由【").append(entity.getSspcs()).append("】移交至【").append(modifiedEntity.getSspcs()).append("】");
            }
            // 管控民警
            if (null!=modifiedEntity.getGkmj()&&!modifiedEntity.getGkmj().equalsIgnoreCase(entity.getGkmj())){
                if(sb.length()>0)
                    sb.append("，").append("责任民警由【").append(entity.getGkmj()).append("】变更为【").append(modifiedEntity.getGkmj()).append("】");
                else
                    sb.append("责任民警由【").append(entity.getGkmj()).append("】变更为【").append(modifiedEntity.getGkmj()).append("】");
            }
            // 民警联系电话
            if (null!=modifiedEntity.getMjlxdh()&&!modifiedEntity.getMjlxdh().equalsIgnoreCase(entity.getMjlxdh())){
                if(sb.length()>0)
                    sb.append("，").append("民警联系方式由【").append(entity.getMjlxdh()).append("】变更为【").append(modifiedEntity.getMjlxdh()).append("】");
                else
                    sb.append("民警联系方式由【").append(entity.getMjlxdh()).append("】变更为【").append(modifiedEntity.getMjlxdh()).append("】");
            }
           /* if (modifiedEntity.getGkmj()!=entity.getGkmj()){
            	if(sb.length()>0)
                    sb.append("，").append("责任民警由【").append(entity.getGkmj()).append("】变更为【").append(modifiedEntity.getGkmj()).append("】");
                else
                    sb.append("责任民警由【").append(entity.getGkmj()).append("】变更为【").append(modifiedEntity.getGkmj()).append("】");
            }
            if (null!=modifiedEntity.getMjlxdh()&&!modifiedEntity.getMjlxdh().equalsIgnoreCase(entity.getMjlxdh())){
            	if(null==entity.getMjlxdh()) {
                 	entity.setMjlxdh("空");
                 }
            	if(sb.length()>0)
                    sb.append("，").append("民警联系方式由【").append(entity.getMjlxdh()).append("】变更为【").append(modifiedEntity.getMjlxdh()).append("】");
                else
                    sb.append("民警联系方式由【").append(entity.getMjlxdh()).append("】变更为【").append(modifiedEntity.getMjlxdh()).append("】");
            }*/
         // 居住地
            if(null != modifiedEntity.getJzdList() && null != entity.getJzdList() ){
                for(KmZdryHomeEntity zdry : modifiedEntity.getJzdList()){
                    if(null == zdry.getId()){ //新增地址
                        if(sb.length()>0)
                            sb.append(", ").append("新增居住地 【").append(zdry.getXzzs()).append(zdry.getXzzss())
                                    .append(zdry.getXzzx()).append(zdry.getXzz()).append("】");
                        else
                            sb.append("新增居住地 【").append(zdry.getXzzs()).append(zdry.getXzzss())
                                    .append(zdry.getXzzx()).append(zdry.getXzz()).append("】");
                    }else{ // 编辑
                        for(KmZdryHomeEntity orZdry : entity.getJzdList()){
                            if(zdry.getId() == orZdry.getId()){
                            	if(StringUtils.isEmpty( zdry.getZdryId())) {
                            		if (sb.length() > 0)
                        				sb.append(", ").append("删除居住地  【").append(zdry.getXzzs()).append(zdry.getXzzss()).append(zdry.getXzzx()).append(zdry.getXzz()).append("】");
                        			else
                        				sb.append("删除居住地  【").append(zdry.getXzzs()).append(zdry.getXzzss()).append(zdry.getXzzx()).append(zdry.getXzz()).append("】");
                            	}else {
                            		if(!(zdry.getXzzs().equalsIgnoreCase(orZdry.getXzzs()) && zdry.getXzzss().equalsIgnoreCase(orZdry.getXzzss())
                            				&& zdry.getXzzx().equalsIgnoreCase(orZdry.getXzzx()) && zdry.getXzz().equalsIgnoreCase(orZdry.getXzz()))) {
                            			if (sb.length() > 0)
                            				sb.append(", ").append("居住地由 【").append(orZdry.getXzzs()).append(orZdry.getXzzss())
                            				.append(orZdry.getXzzx()).append(orZdry.getXzz()).append("】").append(" 修改为 【")
                            				.append(zdry.getXzzs()).append(zdry.getXzzss()).append(zdry.getXzzx()).append(zdry.getXzz()).append("】");
                            			else
                            				sb.append("居住地由 【").append(orZdry.getXzzs()).append(orZdry.getXzzss())
                            				.append(orZdry.getXzzx()).append(orZdry.getXzz()).append("】").append(" 修改为 【")
                            				.append(zdry.getXzzs()).append(zdry.getXzzss()).append(zdry.getXzzx()).append(zdry.getXzz()).append("】");
                            		}
                            	}
                            }
                        }
                    }
                }
            }
            //诉求
            if(null != modifiedEntity.getAppeal() && !modifiedEntity.getAppeal().equals(entity.getAppeal())){
                if(sb.length()>0)
                    sb.append("，").append("诉求【").append(entity.getAppeal()).append("】变更为【").append(modifiedEntity.getAppeal()).append("】");
                else
                    sb.append("诉求由【").append(entity.getAppeal()).append("】变更为【").append(modifiedEntity.getAppeal()).append("】");
            }

        return sb.toString(); 
        }
        
        public List<String> getModifyList(LfZdryBaseEntity modifiedEntity,LfZdryBaseEntity entity){
            List<String> list = new ArrayList<String>();
    		//String[] baseInfos={"xm","hjs","hjss","hjx","hjd","gkmj","mjlxdh"};
            // 姓名
            if(null!=modifiedEntity.getXm() && !modifiedEntity.getXm().equalsIgnoreCase(entity.getXm()))
            	list.add("xm");
            // 户籍省
            if(null != modifiedEntity.getHjs() && !modifiedEntity.getHjs().equalsIgnoreCase(entity.getHjs())){
            	list.add("hjs");
            }
            // 户籍市
            if(null != modifiedEntity.getHjss() && !modifiedEntity.getHjss().equalsIgnoreCase(entity.getHjss())){
            	list.add("hjss");
            }
            // 户籍县
            if(null != modifiedEntity.getHjx() && !modifiedEntity.getHjx().equalsIgnoreCase(entity.getHjx())){
            	list.add("hjx");
            }

            // 户籍地
            if (null!=modifiedEntity.getHjd()&&!modifiedEntity.getHjd().equalsIgnoreCase(entity.getHjd())){
            	list.add("hjd");
            }
            // 居住地
           /* if(null != modifiedEntity.getJzdList() && null != entity.getJzdList() ){
                for(KmZdryHomeEntity zdry : modifiedEntity.getJzdList()){
                    if(null == zdry.getId()){ //新增地址
                    	list.add("jzd");
                    }else{ // 编辑
                        for(KmZdryHomeEntity orZdry : entity.getJzdList()){
                            if(zdry.getId() == orZdry.getId()){
                                if(!(zdry.getXzzs().equalsIgnoreCase(orZdry.getXzzs()) && zdry.getXzzss().equalsIgnoreCase(orZdry.getXzzss())
                                        && zdry.getXzzx().equalsIgnoreCase(orZdry.getXzzx()) && zdry.getXzz().equalsIgnoreCase(orZdry.getXzz()))) {
                                	list.add("jzd");
                                }
                            }
                        }
                    }
                }
            }

            // 管控责任分局
            if (null!=modifiedEntity.getSsxq()&&!modifiedEntity.getSsxq().equalsIgnoreCase(entity.getSsxq())){
                list.add("ssxq");
            }
            // 责任派出所
            if (null!=modifiedEntity.getSspcs() && !modifiedEntity.getSspcs().equalsIgnoreCase(entity.getSspcs())){
                list.add("sspcs");
            }*/
            // 管控民警
            if (null!=modifiedEntity.getGkmj()&&!modifiedEntity.getGkmj().equalsIgnoreCase(entity.getGkmj())){
//                if(null==entity.getGkmj()) {
//                    entity.setGkmj("空");
//                }
                list.add("gkmj");
            }
            // 民警联系电话
            if (null!=modifiedEntity.getMjlxdh()&&!modifiedEntity.getMjlxdh().equalsIgnoreCase(entity.getMjlxdh())){
//                if(null==entity.getMjlxdh()) {
//                    entity.setMjlxdh("空");
//                }
                list.add("mjlxdh");
            }

            // 风险等级
           if(null != modifiedEntity.getGkjb() && !modifiedEntity.getGkjb().equalsIgnoreCase(entity.getGkjb())){
                list.add("gkjb");
            }
            // 人员属性
            if(null!= modifiedEntity.getRysx() && modifiedEntity.getRysx()!= entity.getRysx()){
                list.add("rysx");
            }
            // 预警处置级别
            if(null != modifiedEntity.getYjczlb() && !modifiedEntity.getYjczlb().equals(entity.getYjczlb())){
                list.add("yjczlb");
            }
            // 稳控状态
            if(!StringUtils.isEmpty(modifiedEntity.getWkzt()) && !modifiedEntity.getWkzt().equalsIgnoreCase(entity.getWkzt())){
                list.add("wkzt");
            }
            //辅助信息
            /*  if(null != modifiedEntity.getAssistList()&& modifiedEntity.getAssistList().size()>0){
               for(KmZdryAssistEntity assist : modifiedEntity.getAssistList()){
                   if(null == assist.getId()){ // 新增
                       switch (assist.getInfoType()){
                           case 1:{ //电话
                               list.add("hjd");
                           }break;
                           case 2 :{ // 微信号码
                               list.add("hjd");
                           }break;
                           case 3 :{ // qq号码
                               list.add("hjd");
                           }break;
                           case 4 :{ // 名下车辆号牌
                               list.add("hjd");
                           }break;
                           case 5 :{ // 实际车辆号牌
                               list.add("hjd");
                           }break;
                       }
                   }else{ // 编辑
                       for(KmZdryAssistEntity orAssist : modifiedEntity.getAssistList()){
                                if(assist.getId() == orAssist.getId()){
                                    if(null != assist.getInfoType() && !assist.getInfoValue().equalsIgnoreCase(orAssist.getInfoValue())){
                                        switch (assist.getInfoType()){
                                            case 1 :{ // 电话号码
                                                list.add("hjd");
//                                                    sb.append("，").append("电话号码由【").append(orAssist.getInfoValue()).append("】变更为 【")
                                            }break;
                                            case 2 :{ //微信号码
                                                list.add("hjd");
//                                                    sb.append("，").append("微信号码由【").append(orAssist.getInfoValue()).append("】变更为 【")
                                            }break;
                                            case 3 :{ // qq号码
                                                list.add("hjd");
//                                                    sb.append("，").append("qq号码由【").append(orAssist.getInfoValue()).append("】变更为 【")
                                            }break;
                                            case 4 :{ //名下车辆号牌
                                                list.add("hjd");
//                                                    sb.append("，").append("名下车牌号由【").append(orAssist.getInfoValue()).append("】变更为 【")
                                            }break;
                                            case 5 :{ // 实际车辆号牌
                                                list.add("hjd");
//                                                    sb.append("，").append("实际车辆号牌由【").append(orAssist.getInfoValue()).append("】变更为 【")
                                            }break;
                                        }
                                    }
                                }
                            }
                   }
               }
            }
            // 备注
            if (entity.getBz()==null && modifiedEntity.getBz()!=null){
                list.add("bz");
            }
            // 大类 小类 和 对应警种
            if(null != modifiedEntity.getTypeList() && modifiedEntity.getTypeList().size()>0){
                for(KmZdryTypeEntity type : modifiedEntity.getTypeList()){
                    if(null == type.getId()){
                        list.add("rylb");
                    }else{
                        for(KmZdryTypeEntity orType : entity.getTypeList()){
                            if(type.getId() == orType.getId()){
                                if(!type.getRylb().equalsIgnoreCase(orType.getRylb())){
                                    list.add("rylb");
                                }
                                if(!type.getXl().equalsIgnoreCase(orType.getXl())){
                                    list.add("xl");
                                }
                                if(!type.getZrbm().equalsIgnoreCase(orType.getZrbm())){
                                    list.add("zrbm");
                                }
                            }
                        }
                    }
                }
            }
            */
			return list;
        }
        
        public List<String> getModifyList1(LfZdryEntity modifiedEntity,LfZdryEntity entity){
            List<String> list = new ArrayList<String>();
            if(null!=modifiedEntity.getXm() && !modifiedEntity.getXm().equalsIgnoreCase(entity.getXm())) {
            	list.add("xm");
            }
            if (null!=modifiedEntity.getHjd()&&!modifiedEntity.getHjd().equalsIgnoreCase(entity.getHjd())){
            	list.add("hjd");
            }
            if (null!=modifiedEntity.getXzz()&&!modifiedEntity.getXzz().equalsIgnoreCase(entity.getXzz())){
            	list.add("xzz");
            }
            if (null != modifiedEntity.getRydlName() && !modifiedEntity.getRydlName().equalsIgnoreCase(entity.getRylbx())){
            	list.add("rylbx");
            }
            if (null!=modifiedEntity.getSsxq()&&!modifiedEntity.getSsxq().equalsIgnoreCase(entity.getSsxq())){
            	list.add("ssxq");
            }
           /* if (null!=modifiedEntity.getZrdw() && !modifiedEntity.getZrdw().equalsIgnoreCase(entity.getZrdw())){
                if(sb.length()>0)
                    sb.append("，").append("责任单位由【").append(entity.getZrdw()).append("】变更为【").append(modifiedEntity.getZrdw()).append("】");
                else
                    sb.append("责任单位由【").append(entity.getZrdw()).append("】变更为【").append(modifiedEntity.getZrdw()).append("】");
            }*/
            if (null!=modifiedEntity.getGkmj()&&!modifiedEntity.getGkmj().equalsIgnoreCase(entity.getGkmj())){
            	list.add("gkmj");
            }
            if (null!=modifiedEntity.getMjlxdh()&&!modifiedEntity.getMjlxdh().equalsIgnoreCase(entity.getMjlxdh())){
            	list.add("mjlxdh");                
            }
            if (entity.getBz()!=null && modifiedEntity.getBz()!=null && !modifiedEntity.getBz().equalsIgnoreCase(entity.getBz())){
            	list.add("bz");
            }
            if (modifiedEntity.getSjh()!=null && entity.getSjh()!=null && !modifiedEntity.getSjh().equalsIgnoreCase(entity.getSjh())){
            	list.add("sjh");
            }
            if (entity.getXl() != null && modifiedEntity.getRyxlName()!=null && !modifiedEntity.getRyxlName().equalsIgnoreCase(entity.getXl())){
            	list.add("xl");
            }
            if(null!=modifiedEntity.getWx() && !modifiedEntity.getWx().equalsIgnoreCase(entity.getWx())){
            	list.add("wx");
            }
            if(null!=modifiedEntity.getQq() && !modifiedEntity.getQq().equalsIgnoreCase(entity.getQq())){
            	list.add("qq");
            }
            if(null!=modifiedEntity.getMxcph() && !modifiedEntity.getMxcph().equalsIgnoreCase(entity.getMxcph())){
            	list.add("mxcph");
            }
            if(null!=modifiedEntity.getSjsyclhp() && !modifiedEntity.getSjsyclhp().equalsIgnoreCase(entity.getSjsyclhp())){
            	list.add("sjsyclhp");
            }
            if(null!= modifiedEntity.getRysx() && modifiedEntity.getRysx()!= entity.getRysx()){
            	list.add("rysx");
            }
            if(null != modifiedEntity.getYjczlb() && modifiedEntity.getYjczlb() != entity.getYjczlb()){
            	list.add("yjczlb");
            }
            if(null != modifiedEntity.getHjs() && !modifiedEntity.getHjs().equalsIgnoreCase(entity.getHjs())){
            	list.add("hjs");
            }
            if(null != modifiedEntity.getHjsS() && !modifiedEntity.getHjsS().equalsIgnoreCase(entity.getHjsS())){
            	list.add("hjss");
            }
            if(null != modifiedEntity.getHjx() && !modifiedEntity.getHjx().equalsIgnoreCase(entity.getHjx())){
            	list.add("hjx");
            }
            if(null != modifiedEntity.getXzzs() && !modifiedEntity.getXzzs().equalsIgnoreCase(entity.getXzzs())){
            	list.add("xzzs");
            }
            if(null != modifiedEntity.getXzzsS() && !modifiedEntity.getXzzsS().equalsIgnoreCase(entity.getXzzsS())){
            	list.add("xzzss");
            }
            if(null != modifiedEntity.getXzzx() && !modifiedEntity.getXzzx().equalsIgnoreCase(entity.getXzzx())){
            	list.add("xzzx");
            }
          /*  if(null != modifiedEntity.getRydlName() && !modifiedEntity.getRydlName().equalsIgnoreCase(entity.getRylbx())){
            	list.add("rydlName");
            }
            if(null != modifiedEntity.getRyxlName() && !modifiedEntity.getRyxlName().equalsIgnoreCase(entity.getXl())){
            	list.add("ryxlName");
            }*/
            if(null != entity.getXgyy() &&entity.getXgyy().length()>0){
            	list.add("xgyy");
            }
			return list;
    }
}
