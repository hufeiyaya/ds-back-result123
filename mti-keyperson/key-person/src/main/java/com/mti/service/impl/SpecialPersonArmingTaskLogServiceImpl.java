package com.mti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mti.dao.model.LfZdryArmingTaskOperateLog;
import com.mti.mapper.ArmingTaskRelationLogMapper;
import com.mti.service.IArmingTaskRelationLogService;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor
public class SpecialPersonArmingTaskLogServiceImpl extends ServiceImpl<ArmingTaskRelationLogMapper, LfZdryArmingTaskOperateLog> implements IArmingTaskRelationLogService {

}
