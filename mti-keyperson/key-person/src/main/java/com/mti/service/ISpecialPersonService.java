package com.mti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.kmmodel.KmZdryDto;
import com.mti.dao.model.LfZdryApproveLogEntity;
import com.mti.dao.model.LfZdryBaseEntity;
import com.mti.dao.model.LfZdryHistoryEntity;
import com.mti.dao.qo.LfZdryQO;
import com.mti.vo.PredictStatisticsDataItem;
import com.mti.vo.SpecialPersonStatisticsDataVO;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/15
 * @change
 * @describe describe
 **/
public interface ISpecialPersonService extends IService<LfZdryHistoryEntity> {
    Mono<DataBuffer> export(
            String date,
            String exportName,
            LfZdryQO qo,
            ServerWebExchange exchange
    ) throws Exception;

    Mono<DataBuffer> export1(
            List<String> listNumber,
            String date,
            String exportName,
            ServerWebExchange exchange
    ) throws Exception;

    IPage<LfZdryHistoryEntity> pageData(
            Integer page,
            Integer size,
            String date,
            String rangeVal,
            String rangeType,
            String zrbm
    );

    IPage<LfZdryHistoryEntity> pageDataV2(Integer page, Integer size, String date, String rangeVal, String rangeType, String zrbm);
    int getIsControlCountByVersion(String date, String rangeVal, String rangeType, String isControl);
    int getIsControlCountByVersionV2(String date, String zrbm, String rangeVal, String rangeType, String isControl);


  /**
     * 获取重点人员统计数据
     * @return data
     */
//    List<SpecialPersonStatisticsDataVO> getSpecialPersonStatisticsData(String rangeType,String rangeVal,String zrdw);
    List<SpecialPersonStatisticsDataVO> getSpecialPersonStatisticsData(LfZdryQO qo);

    List<PredictStatisticsDataItem> getPredictSpecialPersonStatisticsData(String rangeType, String rangeVal, String start, String end);

    /**
     * 重点人员上报
     * @param personId personId
     */
    void updatePersonStatus(String personId);

    /**
     * 重点人员审批
     *
     */
    void approveSpecialPerson(LfZdryApproveLogEntity logEntity);

    /**
     * Excel上传重点人员
     * @param filePart file
     */
    Object uploadSp(FilePart filePart,ServerWebExchange exchange) throws Exception;

    /**
     * 四级查询经纬度
     * @param province
     * @param city
     * @param country
     * @param village
     * @return
     */
    Map<String,String> searchLngLat(String province,String city,String country,String  village) throws Exception;

    /**
     * 根据具体地址查询经纬度
     * @param address
     * @return
     */
    public Map<String,String> checkXY(String address) throws Exception;
    /**
     * 昆明重点人员审批
     *
     */
	void approveKMSpecialPerson(LfZdryApproveLogEntity logEntity);


}
