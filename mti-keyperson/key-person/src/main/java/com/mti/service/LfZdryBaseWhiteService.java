package com.mti.service;

import com.mti.dao.model.LfZdryBaseWhiteEntity;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * <p>
 * 重点人员表 服务类
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */

public interface LfZdryBaseWhiteService extends IService<LfZdryBaseWhiteEntity> {

   
}