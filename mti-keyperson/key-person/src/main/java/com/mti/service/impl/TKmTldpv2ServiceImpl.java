package com.mti.service.impl;

import com.mti.dao.model.TKmTldpEntity;
import com.mti.dao.model.TKmTldpv2Entity;
import com.mti.dao.qo.LfRyyjsjQO;
import com.mti.mapper.TKmTldpv2EntityMapper;
import com.mti.service.TKmTldpv2Service;
import com.mti.utils.ZdryQxzkUtilBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Classname TKmTldpv2ServiceImpl
 * @Description TODO
 * @Date 2020/3/5 14:05
 * @Created by duchaof
 * @Version 1.0
 */
@Service
public class TKmTldpv2ServiceImpl implements TKmTldpv2Service {
    @Resource
    private TKmTldpv2EntityMapper tKmTldpv2EntityMapper;

    @Override
    public Integer insert(TKmTldpv2Entity tKmTldpv2Entity) {
        return tKmTldpv2EntityMapper.insert(tKmTldpv2Entity);
    }

    @Override
    public Integer judgeInOutType(TKmTldpv2Entity tKmTldpv2Entity) {
        return tKmTldpv2EntityMapper.judgeInOutType(tKmTldpv2Entity);
    }

    @Override
    public TKmTldpv2Entity getByGlId(String glId) {
        return tKmTldpv2EntityMapper.selectBySystemid(glId);
    }

    @Override
    public List<TKmTldpv2Entity> getTldpListBySfzh(String sfzf) {
        return tKmTldpv2EntityMapper.getTldpListBySfzh(sfzf);
    }

    @Override
    public List<ZdryQxzkUtilBean> getTlOut(LfRyyjsjQO lfRyyjsjQO) {
        return tKmTldpv2EntityMapper.getTlOut(lfRyyjsjQO);
    }

    @Override
    public List<ZdryQxzkUtilBean> getTlIn(LfRyyjsjQO lfRyyjsjQO) {
        return tKmTldpv2EntityMapper.getTlIn(lfRyyjsjQO);
    }

    @Override
    public List<TKmTldpv2Entity> getTldpEntityList(List<String> tldpEntities) {
        return tKmTldpv2EntityMapper.getTldpEntityList(tldpEntities);
    }
}
