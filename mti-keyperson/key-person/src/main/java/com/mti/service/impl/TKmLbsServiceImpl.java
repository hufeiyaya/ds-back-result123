package com.mti.service.impl;

import com.mti.dao.model.TkmLbsEntity;
import com.mti.dao.qo.LfZdryQO;
import com.mti.enums.RoleType;
import com.mti.exception.BusinessException;
import com.mti.jwt.AccountBo;
import com.mti.jwt.RequestUtils;
import com.mti.mapper.TKmLbsMapper;
import com.mti.service.ILfZdryService;
import com.mti.service.TKmLbsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 微信/qq定位 服务实现类
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
@Slf4j
@Service
public class TKmLbsServiceImpl extends ServiceImpl<TKmLbsMapper, TkmLbsEntity> implements TKmLbsService {

    @Autowired
    private RequestUtils requestUtils;


    @Override
    public Object queryZdryLbsPosition(ServerWebExchange exchange, LfZdryQO qo) {
        // 将权限参数设置进去
        this.setUpAuthority(exchange,qo);
        return this.getBaseMapper().queryZdryLbsPosition(qo);
    }

    private void setUpAuthority(ServerWebExchange exchange, LfZdryQO qo) {
        try {
            // 权限相关代码
            AccountBo bo = requestUtils.getCurrentUser(exchange);
            if (bo == null) {
                throw new BusinessException(10019,"此账号已在别处登录，请查证");
            }
            String roleType = bo.getRoleList().get(0).getRoleType();
            String orgName = bo.getOrganizationBo().getShortName();
            // getOrgName(bo.getOrgId());
            // 1、管理员、市局情报中心,不加判断条件
            // 2、市局各警种
            // 角色类型："管理员", "0")"市局情报中心", "1"), "市局警种", "2"),"分局", "3"),"派出所", "4")
            // 审批状态：0：待分局审批,1：待警种审核,2：已驳回,3：已完成, 4：上报
            if (roleType.equals(RoleType.OWNER_ADMIN_ORG_DATA.getType())) { // 1:市局情报中心
                qo.setSsxqList(qo.getQueryList());
            } else if (roleType.equals(RoleType.OWNER_REVIEW_DATA.getType())
                    || roleType.equals(RoleType.OWNER_ORG_DATA.getType())) { // 5:市局警种和2:市局警种审核
                qo.setZrbm(orgName);
                qo.setSsxqList(qo.getQueryList());
            } else if (roleType.equals(RoleType.BREACH_REVIEW_DATA.getType())
                    || roleType.equals(RoleType.BREACH_DATA.getType())) { // 6:分局 和 3:分局审核
                qo.setSsxq(orgName);
                log.info("============qo=========:" + qo);
                /*
                 * if(qo.getQueryList()!=null&&!qo.getQueryList().contains("其他")) { }
                 */
                List<String> list = new ArrayList<String>();
                for (String pcs : qo.getQueryList()) {
                    list.add(pcs);
                }
                qo.setSspcsList(list);
            } else if (roleType.equals(RoleType.POLICE_STATION_DATA.getType())) { // 派出所4
                qo.setSspcs(orgName);
                qo.setMjList(qo.getQueryList());
            }
        } catch (BusinessException e) {
            log.error("权限参数不正确，或者筛选参数不正确：========>{}",e.getMessage());
            if("此账号已在别处登录，请查证".equals(e.getMessage())){
                throw new BusinessException(10019,"此账号已在别处登录，请查证");
            }
        }
    }


    @Override
    public List<Map<String, Object>> queryLbsLocationInfo(List<String> sfzhs) {
        return this.getBaseMapper().queryLbsLocationInfo(sfzhs);
    }
}
