package com.mti.service;


import com.mti.dao.model.TKmMhlgEntity;

import java.util.List;

/**
 * @Classname TKmMhlgService
 * @Description TODO
 * @Date 2020/1/18 11:48
 * @Created by duchaof
 * @Version 1.0
 */
public interface TKmMhlgService {
    Integer insert(TKmMhlgEntity tKmTldpEntity);

    TKmMhlgEntity getByGlId(String glId);

    List<TKmMhlgEntity> getMhlgListBySfzh(String sfzf);

    /**
     * 获取民航离岗导出数据
     * @param mhlgEntities
     * @return
     */
    List<TKmMhlgEntity> getMhlgList(List<String> mhlgEntities);
}
