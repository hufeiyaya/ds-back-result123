package com.mti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mti.dao.model.LfGpsGisEntity;
import com.mti.dao.model.LfZdryEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public interface ILfGpsGisService extends IService<LfGpsGisEntity> {

    int saveByHistory(String date);

}
