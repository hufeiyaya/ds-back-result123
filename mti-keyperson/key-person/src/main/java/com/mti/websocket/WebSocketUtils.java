package com.mti.websocket;

import com.mti.component.PushMessageIn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Classname WebSocketUtils
 * @Description TODO
 * @Date 2019/12/31 17:33
 * @Created by duchaof
 * @Version 1.0
 */
@Slf4j
@Component
public class WebSocketUtils {

    @Value("${zdry.websocket-ip}")
    private String webSocketIp;
    //private static final String URL="http://10.168.31.40:20020/websocket/push/message";

    @Resource
    private RestTemplate restTemplate;

    /**
     * 指定发送给某个用户 或者 某几个指定的用户
     * @param toUserIds
     * @param param
     * 模拟json格式 "{\"userRealName\": \"测试人员\", \"userName\": \"chujing2\",\"expiredTimestamp\": null,\"userId\": 9}"
     * @param contentType
     * @return
     */
    @Async
    public Object sendWebsockMessageToUser(List<String> toUserIds, List<String> param, String contentType){
        ResponseEntity<String> userResponseEntity = null;
        try {
            PushMessageIn pushMessageIn = new PushMessageIn( toUserIds,  param, contentType);
            HttpEntity entity = createHttpHeaders(pushMessageIn);
            userResponseEntity = restTemplate.postForEntity(webSocketIp+"websocket/push/message", entity, String.class);
        } catch (RestClientException e) {
           log.error("发送websocket失败！");
        }
        return new AsyncResult<>(userResponseEntity);
    }

    /**
     * 指定发送给某个组 或者 某几个组
     * @param groupIds
     * @param param
     * 模拟json格式 "{\"userRealName\": \"测试人员\", \"userName\": \"chujing2\",\"expiredTimestamp\": null,\"userId\": 9}"
     * @param contentType
     * @return
     */
    @Async
    public Object sendWebsockMessageToGroup(List<String> groupIds, List<String> param, String contentType){
        ResponseEntity<String> userResponseEntity = null;
        try {
            PushMessageIn pushMessageIn = new PushMessageIn( "*",groupIds,  param, contentType);
            HttpEntity entity = createHttpHeaders(pushMessageIn);
            userResponseEntity = restTemplate.postForEntity(webSocketIp+"websocket/push/message", entity, String.class);
        } catch (RestClientException e) {
            log.error("发送websocket失败！");
        }
        return new AsyncResult<>(userResponseEntity);
    }

    /**
     * 发给全部人
     * @param param
     * 模拟json格式 "{\"userRealName\": \"测试人员\", \"userName\": \"chujing2\",\"expiredTimestamp\": null,\"userId\": 9}"
     * @param contentType
     * @return
     */
    @Async
    public Object sendWebsockMessageToAll(List<String> param, String contentType){
        ResponseEntity<String> userResponseEntity = null;
        try {
            PushMessageIn pushMessageIn = new PushMessageIn(param, contentType);
            HttpEntity entity = createHttpHeaders(pushMessageIn);
            userResponseEntity = restTemplate.postForEntity(webSocketIp+"websocket/push/message", entity, String.class);
        } catch (RestClientException e) {
            log.error("发送websocket失败！");
        }
        return new AsyncResult<>(userResponseEntity);
    }

    private HttpEntity createHttpHeaders(PushMessageIn pushMessageIn){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //封装成一个请求对象
        HttpEntity entity = new HttpEntity(pushMessageIn, headers);
        return entity;
    }


}
