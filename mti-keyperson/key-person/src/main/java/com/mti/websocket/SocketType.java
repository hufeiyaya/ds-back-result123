package com.mti.websocket;

/**
 * @Classname SocketType
 * @Description TODO
 * @Date 2019/12/31 17:46
 * @Created by duchaof
 * @Version 1.0
 */
public enum SocketType {

    NORMAL("0","normal"),// 普通提示消息
    ZDRRYJXX("1","重点人员预警信息"),
    ZDRRYJJSXX("2","重点人员移交接收信息"),
    ZDRRPHXX("3","手机预警"),
	ZDRDCLXX("4","待处理信息");

    private String code;
    private String name;

    SocketType(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getName(String code) {
        for (SocketType a : SocketType.values()) {
            if (code.equals(a.getCode())) {
                return a.name;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
