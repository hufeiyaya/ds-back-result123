package com.mti.kafka.entity;

import lombok.Data;

import java.util.Date;

/**
 * <p>
 * 重点人员对象
 * </p>
 *
 * @author zhaichen
 * @since 2020-06-12 16:04
 */
@Data
public class ZdryEntity {

    /**
     * 主键
     */
    private String id;

    /**
     * 人员大类代码
     */
    private String rylb_code;

    /**
     * 人员小类代码
     */
    private String xl_code;

    /**
     * 姓名
     */
    private String xm;

    /**
     * 性别
     */
    private String xb;

    /**
     * 民族
     */
    private String mz;

    /**
     * 身份证
     */
    private String sfzh;

    /**
     * 户籍地详址
     */
    private String hjd;

    /**
     * 籍贯省市县代码
     */
    private String jg;

    /**
     * 手机号
     */
    private String sjh;

    /**
     * 更新时间
     */
    private String update_time;

    /**
     * 创建时间
     */
    private String create_time;

    /**
     * 风险等级代码 0:关注 1:核实 2:控制
     */
    private String gk_jb_code;

    /**
     * 居住地
     */
    private String xzz;

    /**
     * 更新类型；0.新增 1.修改 2.删除
     */
    private String gxlx;

    /**
     * 图片访问地址
     */
    private String photo;
    
    /**
     * 头像
     */
    private String tx;

}
