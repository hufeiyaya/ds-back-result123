package com.mti.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @Classname ZdryHistoryLogType
 * @Description TODO
 * @Date 2020/4/27 20:58
 * @Created by duchaof
 * @Version 1.0
 */
public interface ZdryHistoryLogType {
    //1 :处理，2:备注3：变更,4：移交,5:信访
    Map<Integer, String> ZdryHistoryLogType = new HashMap<Integer, String>() {
        {
            put(1, "处理");
            put(2, "备注");
            put(3, "变更");
            put(4, "移交");
            put(5, "维权");
        }
    };
}
