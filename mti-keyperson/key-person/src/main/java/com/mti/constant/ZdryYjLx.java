package com.mti.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @Classname ZdryYjLx
 * @Description TODO
 * @Date 2020/1/18 19:09
 * @Created by duchaof
 * @Version 1.0
 */
public interface ZdryYjLx {
    /**
     * 铁路购票
     */
    String TLDP="tldp";
    /**
     * 网吧上网
     */
     String WBSW="wbsw";
    /**
     * 旅馆住宿
     */
     String LGZS="lgzs";
    /**
     * 民航订票
     */
     String MHDZ="mhdz";
    /**
     * 民航离岗
     */
     String MHLG="mhlg";
    /**
     * 公路客运-购票
     */
     String GLKYGP="glky-gp";
    /**
     * 手机定位
     */
    String PHYJ = "phyj";
    /**
     * 人脸识别
     */
    String RLSB ="rlsb";

     Map<String, String> YJLX = new HashMap<String, String>() {
        {
            put("tldp", "铁路购票");
            put("wbsw", "网吧上网");
            put("lgzs", "旅馆住宿");
            put("mhdz", "民航订票");
            put("mhlg", "民航离岗");
            put("glky-gp", "公路旅客");
            put("phyj","手机预警");
            put("rlsb","人脸识别");
        }
    };

}
