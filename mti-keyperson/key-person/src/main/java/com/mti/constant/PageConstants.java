package com.mti.constant;

public interface PageConstants {
    String PAGE_RESULT_NAME = "list";
    String PAGE_RESULT_COUNT = "total";
    Integer PAGE_NUM_START = 0;
    Integer PAGE_NUM_SIZE = 20;
}

