package com.mti.constant;

import com.google.gson.Gson;
import com.mti.component.snowflake.KeyWorker;
import com.mti.dao.model.TSwitchTypeEntity;
import lombok.AllArgsConstructor;
import org.apache.commons.compress.utils.Lists;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
public enum AlarmFilterEnum {

    RLSB_SWITCH("人脸识别", "人脸识别开关", 0, "rlsb"),
    PHONE_SWITCH("手机预警", "手机预警开关", 0, "phyj"),


    DS_SWITCH("DS预警处置", "DS预警处置开关", 2, "zdry_yjcz"),

    TLGP_ALARM("tldp", "预警方式", 2, "zdry_yjway"),
    GLKY_ALARM("glky-gp", "预警方式", 2, "zdry_yjway"),
    WBSW_ALARM("wbsw", "预警方式", 2, "zdry_yjway"),
    LGZS_ALARM("lgzs", "预警方式", 2, "zdry_yjway"),
    MHDZ_ALARM("mhdz", "预警方式", 2, "zdry_yjway"),
    MHLZ_ALARM("mhlg", "预警方式", 2, "zdry_yjway"),
    SJYJ_ALARM("phyj", "预警方式", 2, "zdry_yjway"),
    RLSB_ALARM("rlsb", "预警方式", 2, "zdry_yjway"),

    WKZT_ALARM("", "稳控状态", 2, "zdry_wkzt"),

    YJCZLB_ALARM("", "预警处置类别", 2, "zdry_yjczlb"),

    FXDJ_ALARM("", "风险等级", 2, "kp_gkjb"),

    RYLX_ALARM("", "人员类型", 2, "kp_rylb"),

    DYJZ_ALARM("", "对应警种", 2, "kp_zrbm"),

    GKZRZS_ALARM("", "管控责任州市", 2, "yn_zs"),

    GKZRFJ_ALARM("", "管控责任分局", 2, "kp_zrgkfj"),

    //时间key值保证要唯一
    START_TIME("", "开始时间", 3, "start_time"),
    //时间key值保证要唯一
    END_TIME("", "结束时间", 3, "end_time");

    private String name;
    private String desc;
    private Integer type;
    private String key;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static String getByType(Integer type) {
        for (AlarmFilterEnum msgActionEnum : AlarmFilterEnum.values()) {
            if (msgActionEnum.getType().equals(type)) {
                return msgActionEnum.getName();
            }
        }
        return null;
    }

    //讲枚举转换成list格式，这样前台遍历的时候比较容易，列如 下拉框 后台调用toList方法，你就可以得到code 和name了
    public static List toList() {
        List list = Lists.newArrayList();
        for (AlarmFilterEnum alarmFilterEnum : AlarmFilterEnum.values()) {
            TSwitchTypeEntity entity = new TSwitchTypeEntity();
            entity.setType(alarmFilterEnum.getType());
            entity.setDescribe(alarmFilterEnum.getDesc());
            entity.setKey(alarmFilterEnum.getKey());
            entity.setParentKey(alarmFilterEnum.getKey());
            list.add(entity);
        }
        return list;
    }

    //获取预警类型、人脸识别、手机预警、DS 预警设置开关 list
    public static List toYjfsList() {
        List list = Lists.newArrayList();
        for (AlarmFilterEnum alarmFilterEnum : AlarmFilterEnum.values()) {
            if (alarmFilterEnum.getKey().equals(AlarmFilterEnum.TLGP_ALARM.getKey()) || alarmFilterEnum.getKey().equals(AlarmFilterEnum.RLSB_SWITCH.getKey()) || alarmFilterEnum.getKey().equals(AlarmFilterEnum.PHONE_SWITCH.getKey()) || alarmFilterEnum.getKey().equals(AlarmFilterEnum.DS_SWITCH.getKey())) {
                TSwitchTypeEntity entity = new TSwitchTypeEntity();
                entity.setId(String.valueOf(KeyWorker.nextId()));
                entity.setStatus(false);
                entity.setName(alarmFilterEnum.getName());
                entity.setType(alarmFilterEnum.getType());
                entity.setDescribe(alarmFilterEnum.getDesc());
                entity.setKey(alarmFilterEnum.getKey());
                entity.setParentKey(alarmFilterEnum.getKey());
                list.add(entity);
            }
        }
        return list;
    }


    public static void main(String[] args) {
        List<Map<String, Object>> list = toList();
        list.forEach(e -> {
            System.out.println(new Gson().toJson(e.get("aa")));
        });

    }
}
