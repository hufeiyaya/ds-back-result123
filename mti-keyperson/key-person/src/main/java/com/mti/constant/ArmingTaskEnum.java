package com.mti.constant;

import lombok.AllArgsConstructor;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/9/11
 * @change
 * @describe 上报状态
 **/
@AllArgsConstructor
public enum ArmingTaskEnum {
    REPORTING_TYPE("上报中",0),
    WAIT_SSXQ_POLICE_TYPE_TYPE("待区县警钟审核",1),
    WAIT_POLICE_TYPE("待市局警种审核",2),
    WAIT_SPY_TYPE("待情报审核",3),
    RECALL_TYPE("上报退回",4),
    FINISH_TYPE("审核完毕",5),
    REQUEST_REBACK("申请撤销",6),
    WAIT_SSXQ_POLICE_TYPE_REBACK("待区县警钟审核撤销",7),
    WAIT_POLICE_TYPE_REBACK("待市局警钟审核撤销",8),
    WAIT_SPY_TYPE_REBACK("待情报审核撤销",9),
    REBACK_RECALL_TYPE("撤销退回",10),
    REBACK_FINISH_TYPE("撤销布控",11);

    String type;
    Integer status;
}
