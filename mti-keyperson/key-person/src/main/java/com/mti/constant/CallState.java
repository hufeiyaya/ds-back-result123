package com.mti.constant;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by jingzhanwu
 * @date 2018-12-18
 * @change
 * @describe 电话呼叫状态
 **/
public class CallState {
    /**
     * 没有电话，无呼叫 无呼入
     */
    public static final int STATE_NULL = 0;
    /**
     * 呼叫
     */
    public static final int STATE_CALLING = 1;
    /**
     * 来电
     */
    public static final int STATE_INCOMING = 2;
    /**
     * 等待连接,振铃中
     */
    public static final int STATE_EARLY = 3;
    /**
     * 正在连接媒体中
     */
    public static final int STATE_CONNECTING = 4;
    /**
     * 已经连接确认
     */
    public static final int STATE_CONFIRMED = 5;
    /**
     * 挂断，断开
     */
    public static final int STATE_DISCONNECTED = 6;
}