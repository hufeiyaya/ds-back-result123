package com.mti.constant;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Classname YjHandleStatusCode
 * @Description TODO
 * @Date 2020/6/19 13:45
 * @Created by duchaof
 * @Version 1.0
 */

@AllArgsConstructor
public enum YjHandleStatusCode {
          /*  "1":"已收到"
            "2":"已查证->属实"
            "3":"已查证->误报"
            "4": "未查证->前期核实->属实"
            "5": "未查证->前期核实->误报"
            "6": "未查证->不需核实"
            "7": "未查证->其它"
            "8": "已控制属实"
            "9": "已控制误报"
            "10": "未控制"*/
          Accepted(1,"已收到"),
         Verified_True(2,"已查证->属实"),
         Verified_Misinformation(3,"已查证->误报"),
         Not_Verified_Prior_Verification_True(4,"未查证->前期核实->属实"),
         Not_Verified_Prior_Verification_Misinformation(5,"未查证->前期核实->误报"),
         Not_Verified_No_Need_To_Verify(6,"未查证->不需核实"),
         Not_Verified_Other(7,"未查证->其他"),
         Controlled_True(8,"已控制属实"),
         Controlled_Misinformation(9,"已控制属实"),
         Uncontrolled(10,"未控制")
    ;
    Integer status;
    String type;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
