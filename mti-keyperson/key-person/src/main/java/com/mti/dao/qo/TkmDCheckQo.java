package com.mti.dao.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Classname TkmDCheckQo
 * @Description TODO
 * @Date 2020/2/27 17:45
 * @Created by duchaof
 * @Version 1.0
 */
@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel(value = "日常核查", description = "日常核查")
@NoArgsConstructor
public class TkmDCheckQo extends PageQO {
    /**
     * 关键字 姓名/身份证
     */
    @ApiModelProperty(value = "关键字查询",name = "keyWords",example = "张三")
    private String keyWords;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间",name = "startTime",example = "2020-02-27")
    private String startTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间",name = "endTime",example = "2020-02-27")
    private String endTime;

    /**
     * 核查结果
     */
    @ApiModelProperty(value = "核查结果",name = "checkResult",example = "再次在控")
    private String checkResult;

    /**
     * 所属分局
     */
    @ApiModelProperty(value = "所属分局",name = "ssfj",example = "五华分局")
    private String ssfj;

    /**
     * 管辖派出所
     */
    @ApiModelProperty(value = "管辖派出所",name = "sspcs",example = "五华分局华山东路派出所")
    private String sspcs;



    /**
     * 警种
     */
    @ApiModelProperty(value = "警种",name = "jz",example = "治安支队")
    private String jz;

    public TkmDCheckQo(String keyWords,String startTime,String endTime,String checkResult,String ssfj,
                       String sspcs,String jz){
        this.keyWords=keyWords;
        this.startTime=startTime;
        this.endTime=endTime;
        this.checkResult=checkResult;
        this.ssfj=ssfj;
        this.sspcs=sspcs;
        this.jz=jz;
    }
}
