package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author duchao
 * @since 2020-05-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_jurisdiction")
public class KmJurisdictionEntity extends Model<KmJurisdictionEntity> {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 派出所代码
     */
    private String dm;

    /**
     * 派出所名称
     */
    private String mc;

    /**
     * 所属分局
     */
    private String ssfj;

    /**
     * 辖区范围
     */
    private String xqfw;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
