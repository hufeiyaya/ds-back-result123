package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_zdry_type")
@AllArgsConstructor
@NoArgsConstructor
public class KmZdryTypeEntity extends Model<KmZdryTypeEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId("id")
    @ApiModelProperty(value = "主键",name = "id")
    private String id;

    /**
     * 人员大类
     */
    @TableField("rylb")
    @ApiModelProperty(value = "人员大类",name = "rylb")
    private String rylb;
    /**
     * 人员大类（新）
     */
    @TableField("rylb_code")
    @ApiModelProperty(value = "人员大类（新）",name = "rylbCode")
    private String rylbCode;
    /**
     * 责任部门--对应警种
     */
    @TableField("zrbm")
    @ApiModelProperty(value = "任部门--对应警种",name = "zrbm")
    private String zrbm;
    
    /**
     * 人员小类
     */
    @TableField("xl")
    @ApiModelProperty(value = "人员小类",name = "xl")
    private String xl;
    
    @TableField("xl_code")
    @ApiModelProperty(value = "人员小类code",name = "xlCode")
    private String xlCode;

    /**
     * 1:主责警种  0:非主责警种
     */
    @TableField("zzjz")
    @ApiModelProperty(value = "主责警种",name = "0")
    private Integer zzjz;
    
    /**
     * 重点人员ID
     */
    @TableField("zdry_id")
    @ApiModelProperty(value = "重点人员ID",name = "zdryId")
    private String zdryId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
