package com.mti.dao.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Classname TkmProRiQo
 * @Description TODO
 * @Date 2020/2/20 17:17
 * @Created by duchaof
 * @Version 1.0
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class TkmProRiQo extends PageQO {

    /**
     * 自增主键
     */
    @ApiModelProperty(value = "id",name = "id",example = "1")
    private Long id;

    /**
     * 信访查询开始时间
     */
    @ApiModelProperty(value = "信访查询开始时间",name = "start_time",example = "2019-01-02 12:01:02")
    private String start_time;

    /**
     * 信访查询结束时间
     */
    @ApiModelProperty(value = "信访查询结束时间",name = "end_time",example = "2019-01-02 12:01:02")
    private String end_time;

    /**
     * 信访内容
     */
    @ApiModelProperty(value = "信访内容",name = "proRightContent",example = "信访")
    private String proRightContent;

    /**
     * 关联Id
     */
   @ApiModelProperty(value = "关联Id",name = "refId",example = "401")
    private String refId;
}
