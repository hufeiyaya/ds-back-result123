package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 预警区域表
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_alarm_area")
public class KmAlarmAreaEntity extends Model<KmAlarmAreaEntity> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    @ApiModelProperty(value = "主键", name = "id", hidden = true)
    private String id;

    @TableField("name")
    @ApiModelProperty(value = "区域名称", name = "name")
    private String name;

    @ApiModelProperty(value = "预警范围(json格式数据)")
    @TableField("alarm_range")
    private String alarmRange;

}
