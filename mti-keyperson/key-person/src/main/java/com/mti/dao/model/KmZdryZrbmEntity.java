/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author chenf
 * @since 2020-03-29
 */
@Data
@Accessors(chain = true)
@TableName("t_km_zdry_zrbm")
@ApiModel(value = "重点人员责任部门对象", description = "重点人员责任部门对象")
public class KmZdryZrbmEntity {

    /**
     * 序号
     */
    @ApiModelProperty(value = "序号")
    @TableField("id")
    private String id;

    /**
     * 人员类别
     */
    @ApiModelProperty(value = "人员大类")
    @TableField("rydl")
    private String rydl;

    /**
     * 责任部门
     */
    @ApiModelProperty(value = "责任部门")
    @TableField("zrbm")
    private String zrbm;

    /**
     * 人员小类
     */
    @ApiModelProperty(value = "人员小类")
    @TableField("ryxl")
    private String ryxl;

    /**
     * 人员大类编码
     */
    @ApiModelProperty(value = "人员大类编码")
    @TableField("rydl_code")
    private String rydlCode;

    /**
     * 人员小类编码
     */
    @ApiModelProperty(value = "人员小类编码")
    @TableField("ryxl_code")
    private String ryxlCode;


}
