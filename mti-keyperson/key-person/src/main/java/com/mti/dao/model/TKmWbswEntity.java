package com.mti.dao.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.constant.ZdryYjLx;
import com.mti.utils.ZdryYjUtilBean;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class TKmWbswEntity {
    private  static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String xxzjbh;

    private String id;

    private String loginAt;

    private String idType;

    private String idCode;

    private String idName;

    private String idSex;

    private String serviceCode;

    private String xzqh;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date rksj;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date inserttime;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date loginTm;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date logoutAt;

    private String clientIp;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date bzRksj;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date bzGxsj;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pgRksj;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pgGxsj;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date jlSccjsj;

    /**
     * 网吧名称
     */
    private String wbmc;
    /**
     * 网吧经度
     */
    private String wbjd;

    /**
     * 网吧维度
     */
    private String wbwd;




    public ZdryYjUtilBean transtoZdryYjUtilBean(){
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.idName);
        zdryYjUtilBean.setSfzh(this.idCode);
        zdryYjUtilBean.setYjlx(ZdryYjLx.WBSW);
        zdryYjUtilBean.setYjdz(this.wbmc);
        if(null != this.getLoginTm())
        zdryYjUtilBean.setYjsj(sdf.format(this.getLoginTm()));
        zdryYjUtilBean.setLng(this.wbjd);
        zdryYjUtilBean.setLat(this.wbwd);
        return zdryYjUtilBean;
    }


}