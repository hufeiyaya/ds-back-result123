/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.dao.kmmodel.KmZdryDto;
import com.mti.dao.kmmodel.KmZdryLxGxEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_zdry_temp")
//@KeySequence(value = "t_lf_zdry_xh_seq", clazz = String.class)
@ApiModel(value = "重点人员对象", description = "重点人员对象")
public class LfZdryTempEntity {

    /**
     * 序号
     */
    @ApiModelProperty(value = "序号")
    @TableField("xh")
    private String xh;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    @TableField("xm")
    private String xm;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    @TableField("xb")
    private String xb;

    /**
     * 民族
     */
    @ApiModelProperty(value = "民族")
    @TableField("mz")
    private String mz;

    /**
     * 身份证号
     */
    @ApiModelProperty(value = "身份证号")
    @TableField("sfzh")
    private String sfzh;

    /**
     * 户籍地
     */
    @ApiModelProperty(value = "户籍地")
    @TableField("hjd")
    private String hjd;

    /**
     * 现住地
     */
    @ApiModelProperty(value = "现住地")
    @TableField("xzz")
    private String xzz;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    @TableField("sjh")
    private String sjh;

    /**
     * 细类
     */
    @ApiModelProperty(value = "细类")
    @TableField("xl")
    private String xl;

    /**
     * 人员类别
     */
    @ApiModelProperty(value = "人员类别（未使用）")
    @TableField("rylb")
    private String rylb;

    /**
     * 是否落实管控措施
     */
    @ApiModelProperty(value = "是否落实管控措施")
    @TableField("sflsgkcs")
    private String sflsgkcs;

    /**
     * 具体管控措施或未控制原因
     */
    @ApiModelProperty(value = "具体管控措施或未控制原因")
    @TableField("jtgkcshwkzyy")
    private String jtgkcshwkzyy;

    /**
     * 责任单位
     */
    @ApiModelProperty(value = "责任单位")
    @TableField("zrdw")
    private String zrdw;

    /**
     * 管控民警
     */
    @ApiModelProperty(value = "管控民警")
    @TableField(value = "gkmj", strategy = FieldStrategy.IGNORED)
    private String gkmj;

    /**
     * 民警联系电话
     */
    @ApiModelProperty(value = "民警联系电话")
    @TableField(value = "mjlxdh", strategy = FieldStrategy.IGNORED)
    private String mjlxdh;

    /**
     * 管控级别
     */
    @ApiModelProperty(value = "管控级别（未使用）")
    @TableField("gkjb")
    private String gkjb;

    /**
     * update_time
     */
    @ApiModelProperty(value = "update_time（未使用）")
    @TableField("update_time")
    private String update_time;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    @TableField("bz")
    private String bz;

    /**
     * job_id
     */
    @ApiModelProperty(value = "job_id（未使用）")
    @TableField("job_id")
    private String job_id;

    /**
     * 人员类别（新）
     */
    @ApiModelProperty(value = "人员类别")
    @TableField("rylbx")
    private String rylbx;

    /**
     * 是否在控
     */
    @ApiModelProperty(value = "是否在控")
    @TableField("sfzk")
    private String sfzk;

    /**
     * 责任部门
     */
    @ApiModelProperty(value = "责任部门")
    @TableField("zrbm")
    private String zrbm;

    /**
     * 所属辖区
     */
    @ApiModelProperty(value = "所属辖区")
    @TableField("ssxq")
    private String ssxq;

    /**
     * 所属辖区ID
     */
    @ApiModelProperty(value = "所属辖区ID")
    @TableField("ssxq_id")
    private String ssxqId;

    /**
     * 管控级别（新）
     */
    @ApiModelProperty(value = "管控级别")
    @TableField("gkjbx")
    private String gkjbx;

    /**
     * 经度（现住址）
     */
    @ApiModelProperty(value = "经度（现住址）")
    @TableField("x")
    private String x;

    /**
     * 纬度（现住址）
     */
    @ApiModelProperty(value = "纬度（现住址）")
    @TableField("y")
    private String y;

    /**
     * 是否布控
     */
    @ApiModelProperty(value = "是否布控")
    @TableField("sfbk")
    private String sfbk;

    /**
     * 流水编号
     */
    @ApiModelProperty(value = "流水编号")
    @TableId("id")
    private String id;

    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    @TableId("tx")
    private String tx;

    /**
     * 民警ID
     */
    @ApiModelProperty(value = "民警ID")
    @TableId("mjid")
    private String mjid;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_timex")
    private Date create_timex;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("update_timex")
    private Date update_timex;
    /**
     * 所属派出所
     */
    @ApiModelProperty(value = "所属派出所")
    @TableField("sspcs")
    private String sspcs;

    /**
     * 派出所ID
     */
    @ApiModelProperty(value = "派出所ID")
    @TableField("oid")
    private String oid;
    /**
     * 是否自排
     */
    @ApiModelProperty(value = "是否自排")
    @TableField("sfzp")
    private String sfzp;

//    @TableField(exist = false)
//    @ApiModelProperty(value = "审核状态")
//    private String approveStatus;
    @TableField(exist = false)
    @ApiModelProperty(value = "关联ID")
    private String rid;
    @ApiModelProperty(value = "是否触网")
    @TableField(exist = false)
    private String isDrop;

    @ApiModelProperty(value = "责任部门联系人")
    @TableField(exist = false)
    private String zrbmlxr;
    @ApiModelProperty(value = "责任部门联系电话")
    @TableField(exist = false)
    private String zrbmlxdh;
    @ApiModelProperty(value = "重点人员审批状态")
    @TableField(value = "approve_status")
    private String approveStatus;

    @ApiModelProperty(value = "数据项操作人",hidden = true)
    @TableField(exist = false)
    private String modifier;

    @ApiModelProperty(value = "人员来源")
    @TableField("ryly")
    private String ryly;

    @ApiModelProperty(value = "微信号码")
    @TableField("wx")
    private String wx;

    @ApiModelProperty(value = "qq号码")
    @TableField("qq")
    private String qq;

    @ApiModelProperty(value = "名下车牌号")
    @TableField("mxcph")
    private String mxcph;

    @ApiModelProperty(value = "实际使用车辆号牌")
    @TableField("sjsyclhp")
    private String sjsyclhp;

    @ApiModelProperty(value = "对应人员属性类型,字典表ID")
    @TableField("rysx")
    private Integer rysx;

    @ApiModelProperty(value = "对应人员属性类型名称")
    @TableField(exist = false)
    private String rysxName;


    @ApiModelProperty(value = "预警处置类别，字典表ID")
    @TableField("yjczlb")
    private Integer yjczlb;

    @ApiModelProperty(value = "预警处置类别名称")
    @TableField(exist = false)
    private String yjczlbName;


    @ApiModelProperty(value = "户籍省")
    @TableField("hjs")
    private String hjs;

    @ApiModelProperty(value = "户籍市")
    @TableField("hjss")
    private String hjsS;

    @ApiModelProperty(value = "户籍县")
    @TableField("hjx")
    private String hjx;

    @ApiModelProperty(value = "实际住址省")
    @TableField("xzzs")
    private String xzzs;

    @ApiModelProperty(value = "实际住址市")
    @TableField("xzzss")
    private String xzzsS;

    @ApiModelProperty(value = "实际住址市")
    @TableField("xzzx")
    private String xzzx;

    @ApiModelProperty(value = "重点人员大类名称")
    @TableField(exist = false)
    private String rydlName;

    @ApiModelProperty(value = "重点人员小类名称")
    @TableField(exist = false)
    private String ryxlName;


    @ApiModelProperty(value = "户籍地经度")
    @TableField("hjd_x")
    private String hjdX;

    @ApiModelProperty(value = "户籍维度")
    @TableField("hjd_y")
    private String hjdY;

    @ApiModelProperty(value = "稳控状态")
    @TableField("wkzt")
    private String wkzt;

    @ApiModelProperty(value = "修改原因")
    @TableField(exist = false)
    private String xgyy;
    
    @ApiModelProperty(value = "审批意见")
    @TableField(exist = false)
    private String approvalOpinion;
    
    @ApiModelProperty(value = "最后一次操作")
    @TableField("last_operate")
    private String lastOperate;
    
    @ApiModelProperty(value = "提交人角色ID")
    @TableField("submitter_role_id")
    private String submitterRoleId;
    
    @ApiModelProperty(value = "流程类型")
    @TableField("process_type")
    private Integer processType;
    
    @ApiModelProperty(value = "上报状态")
    @TableField("up_status")
    private Integer upStatus;
    
    @ApiModelProperty(value = "提交人")
    @TableField("submitter")
    private String submitter;
    
    @ApiModelProperty(value = "人员大类小类关系")
    @TableField(exist = false)
    private List<KmZdryLxGxEntity> kmZdryLxGxEntityList;
    
    @ApiModelProperty(value = "审批状态")
    @TableField(exist = false)
    private String reviewState;

    public void addKmzdryLxGx(KmZdryLxGxEntity kmZdryLxGxEntity){
        if(null == this.getKmZdryLxGxEntityList()){
            this.setKmZdryLxGxEntityList(new ArrayList<>());
            this.getKmZdryLxGxEntityList().add(kmZdryLxGxEntity);
        }else{
            this.getKmZdryLxGxEntityList().add(kmZdryLxGxEntity);
        }
    }

    public KmZdryDto changeKmZdryEntity(){
        KmZdryDto km = new KmZdryDto();
        km.setXh(this.xh);
        km.setXm(this.xm);
        km.setXb(this.xb);
        km.setMz(this.mz);
        km.setSfzh(this.sfzh);
        km.setHjd(this.hjd);
        km.setXzz(this.xzz);
        km.setSjh(this.sjh);
        km.setXl(this.xl);
        km.setRylb(this.rylb);
        km.setSflsgkcs(this.sflsgkcs);
        km.setJtgkcshwkzyy(this.jtgkcshwkzyy);
        km.setZrdw(this.zrdw);
        km.setGkmj(this.gkmj);
        km.setMjlxdh(this.mjlxdh);
        km.setGkjb(this.gkjb);
        km.setUpdate_time(this.update_time);
        km.setBz(this.bz);
        km.setJob_id(this.job_id);
        km.setRylbx(this.rylbx);
        km.setSfzk(this.sfzk);
        km.setZrbm(this.zrbm);
        km.setSsxq(this.ssxq);
        km.setSsxqId(this.ssxqId);
        km.setGkjbx(this.gkjbx);
        km.setX(this.x);
        km.setY(this.y);
        km.setSfbk(this.sfbk);
        km.setId(this.id);
        km.setTx(this.tx);
        km.setMjid(this.mjid);
        km.setCreate_timex(this.create_timex);
        km.setUpdate_timex(this.update_timex);
        km.setSspcs(this.sspcs);
        km.setOid(this.oid);
        km.setSfzp(this.sfzp);
        km.setApproveStatus(this.approveStatus);
        km.setRid(this.rid);
        km.setIsDrop(this.isDrop);
        km.setZrbmlxr(this.zrbmlxr);
        km.setZrbmlxdh(this.zrbmlxdh);
//        km.setZdApproveStatus(this.zdApproveStatus);
        km.setModifier(this.modifier);
        km.setRyly(this.ryly);
        km.setWx(this.wx);
        km.setQq(this.qq);
        km.setMxcph(this.mxcph);
        km.setSjsyclhp(this.sjsyclhp);
        km.setRysx(this.rysx);
        km.setRysxName(this.rysxName);
        km.setYjczlb(this.yjczlb);
        km.setYjczlbName(this.yjczlbName);
        km.setHjs(this.hjs);
        km.setHjsS(this.hjsS);
        km.setHjx(this.hjx);
        km.setXzzs(this.xzzs);
        km.setXzzsS(this.xzzsS);
        km.setXzzx(this.xzzx);
        km.setRydlName(this.rydlName);
        km.setRyxlName(this.ryxlName);
        km.setHjdX(this.hjdX);
        km.setHjdY(this.hjdY);
        km.setWkzt(this.wkzt);
        km.setXgyy(this.getXgyy());
        return km;
    }
}
