package com.mti.dao.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chenf
 * @since 2020-03-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_lf_zdry_history_log")
public class LfZdryHistoryLogEntity extends Model<LfZdryHistoryLogEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableField("id")
    private String id;

    /**
     * 创建人/处理/备注人
     */
    @ApiModelProperty(value = "创建人/处理/备注人")
    @TableField("creator")
    private String creator;

    /**
     * 创建时间/处理/备注时间/维权时间
     */
    @ApiModelProperty(value = "创建时间/处理/备注时间/维权时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    @TableField("modifier")
    private String modifier;

    /**
     * 最后更新时间
     */
    @ApiModelProperty(value = "最后更新时间")
    @TableField("last_update_time")
    private LocalDateTime lastUpdateTime;
    /**
     * 重点人员ID
     */
    @ApiModelProperty(value = "重点人员ID")
    @TableField("sp_id")
    private String spId;

    /**
     * 操作名称
     */
    @ApiModelProperty(value = "操作名称")
    @TableField("operation")
    private String operation;

    /**
     * 操作结果
     */
    @ApiModelProperty(value = "操作结果")
    @TableField("approve_result")
    private String approveResult;

    /**
     * 操作部门
     */
    @ApiModelProperty(value = "操作部门")
    @TableField("operate_dept")
    private String operateDept;

    /**
     * 操作/处理/备注内容/维权内容
     */
    @ApiModelProperty(value = "操作/处理/备注内容/维权内容")
    @TableField("addition")
    private String addition;

    /**
     * 1 :处理，2:备注3：变更,4：移交,5:信访
     */
    @ApiModelProperty(value = "类型")
    @TableField("type")
    private Integer type;

    /**
     * 入库时间
     */
    @ApiModelProperty(value = "入库时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("insert_time")
    private Date insertTime;
    
    /**
     * 省
     */
    @ApiModelProperty(value = "省")
    @TableField("province")
    private String province;
    /**
     * 市
     */
    @ApiModelProperty(value = "市")
    @TableField("city")
    private String city;
    /**
     * 县/区
     */
    @ApiModelProperty(value = "县/区")
    @TableField("region")
    private String region;

    /**
     * 添加单位
     */
    @ApiModelProperty(value = "添加单位")
    @TableField("unit")
    private String unit;

    @ApiModelProperty(value = "诉求",name = "appeal",example = "拆迁问题")
    @TableField("appeal")
    private String appeal;

    @ApiModelProperty(value = "操作ip",name = "operateIp",example = "86.81.10.178")
    @TableField("operate_ip")
    private String operateIp;
    
    @ApiModelProperty(value = "功能模块",name = "functionModule")
    @TableField("function_module")
    private String functionModule;
    
    @ApiModelProperty(value = "功能模块",name = "functionModule")
    @TableField("creator_code")
    private String creatorCode;
    
    
    @ApiModelProperty(value = "附件地址")
    @TableField(exist = false)
	private List<TTaskFileEntity> fileList;
    
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
