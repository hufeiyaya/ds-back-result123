package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author duchaof
 * @since 2020-03-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_task_file")
public class TTaskFileEntity extends Model<TTaskFileEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 编号id
     */
    @ApiModelProperty(hidden = true)
    @TableId("id")
    private String id;

    /**
     * 附件名称
     */
    @ApiModelProperty(value = "附件名称",name = "fileName",example = "xxx图片")
    @TableField("file_name")
    private String fileName;

    /**
     * 附件地址
     */
    @ApiModelProperty(value = "附件路径",name = "fileUrl")
    @TableField("file_url")
    private String fileUrl;

    /**
     * 附件类型 1 通知 2 任务 3 反馈 4 预警处置
     */
    @ApiModelProperty(hidden = true)
    @TableField("buss_type")
    private String bussType;

    /**
     * 附件业务id
     */
    @ApiModelProperty(hidden = true)
    @TableField("buss_id")
    private String bussId;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
