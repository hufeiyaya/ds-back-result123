/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;

@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel(value = "预警数据", description = "预警数据")
public class TKmTrailQO extends PageQO{
	
	private String id;
    /**
     * @1：聚集，2：伴随
     */
    private String trailType;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;
    private String personCount;
    private String setId;
    private String setName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    private String isNew;
    private String trailInfo;
    private String setPersonType;
    private String areaId;
    private long sort;
    private List<String> personTypeList;
	 
}
