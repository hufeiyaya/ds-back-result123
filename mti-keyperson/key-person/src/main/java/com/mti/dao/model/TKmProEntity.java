package com.mti.dao.model;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.dao.qo.FileQO;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname TKmProEntity
 * @Description TODO
 * @Date 2020/2/20 16:37
 * @Created by duchaof
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_km_pro")
public class TKmProEntity {
    /**
     * 自增主键
     */
    @TableId(value = "id")
    @ApiModelProperty(value = "id",name = "id")
    private String id;

    /**
     * 信访时间
     */
    @ApiModelProperty(value = "信访时间",name = "proRightTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("pro_right_time")
    private Date proRightTime;

    /**
     * 信访内容
     */
    @ApiModelProperty(value = "信访内容",name = "proRightContent")
    @TableField("pro_right_con")
    private String proRightContent;
    /**
     * 记录插入时间
     */
    @ApiModelProperty(value = "插入时间",name = "insertTime",example = "2018-01-02 12:13:14")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("insert_time")
    private Date insertTime;
    /**
     * 删除标志
     */
    @ApiModelProperty(value = "删除标志",name = "deleted",example = "1")
    @TableField("deleted")
    @TableLogic
    private Integer deleted;
    /**
     * 关联Id
     */
    @ApiModelProperty(value = "关联Id",name = "refId",example = "1")
    @TableField("ref_id")
    private String refId;

    /**
     * 省
     */
    @ApiModelProperty(value = "省",name = "province",example = "云南省")
    @TableField("province")
    private String province;

    /**
     * 市
     */
    @ApiModelProperty(value = "市",name = "city",example = "昆明")
    @TableField("city")
    private String city;

    /**
     * 区/县
     */
    @ApiModelProperty(value = "区/县",name = "region",example = "五华")
    @TableField("region")
    private String region;

    /**
     * 单位
     */
    @ApiModelProperty(value = "单位",name = "unit",example = "昆明市信访局")
    @TableField("unit")
    private String unit;

    @ApiModelProperty(value = "附件地址")
    @TableField(exist = false)
	private List<TTaskFileEntity> fileList;
    
    @ApiModelProperty(value = "诉求",name = "appeal",example = "拆迁问题")
    @TableField("appeal")
    private String appeal;
    /**
     * 地址
     */
    @ApiModelProperty(hidden = true)
    @TableField(exist = false)
    private String address;

    public String getAddress() {
        return this.province+this.city+this.region;
    }
}
