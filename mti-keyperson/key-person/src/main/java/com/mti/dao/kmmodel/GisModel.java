package com.mti.dao.kmmodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangmingxin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GisModel {
    private String geom;
    private Double jd;
    private Double wd;
}
