package com.mti.dao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 信访/聚访维权处理或化解信息
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_pro_handle")
public class TKmProHandle extends Model<TKmProHandle> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    @ApiModelProperty(value = "主键",name = "id")
    private String id;

    /**
     * 处理/化解人
     */
    @TableField("op_person")
    @ApiModelProperty(value = "处理/化解人",name = "opPerson",example = "张三")
    private String opPerson;

    /**
     * 处理时间
     */
    @TableField("op_time")
    @ApiModelProperty(value = "处理时间",name = "opTime",example = "2020-03-06 11:21:20")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date opTime;

    /**
     * 处理方式
     */
    @TableField("op_type")
    @ApiModelProperty(value = "处理方式",name = "opType",example = "处理方式")
    private String opType;

    /**
     * 1 :处理，2：化解
     */
    @TableField("type")
    @ApiModelProperty(value = "1 :处理，2：化解",name = "type",example = "1")
    private Integer type;

    /**
     * 入库时间
     */
    @TableField("insert_time")
    @ApiModelProperty(hidden = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date insertTime;

    /**
     * 0 删除，1 未删除
     */
    @TableField("deleted")
    @ApiModelProperty(hidden = true)
    @TableLogic
    private Integer deleted;

    /**
     * 信访/聚访id
     */
    @TableField("ref_id")
    @ApiModelProperty(value = "关联id",name = "refId",example = "38")
    private String refId;

    @ApiModelProperty(value = "附件地址")
    @TableField(exist = false)
	private List<TTaskFileEntity> fileList;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
