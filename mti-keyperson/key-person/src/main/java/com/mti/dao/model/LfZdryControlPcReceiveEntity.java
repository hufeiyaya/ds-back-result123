package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * <p>
 * 重点人员布控pc接收单位信息
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-26
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_zdry_control_pc_receive")
@ApiModel(value = "重点人员布控pc接收单位信息", description = "重点人员布控pc接收单位信息")
public class LfZdryControlPcReceiveEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号", dataType = "String")
    @TableId(value = "id_", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 布控id
     */
    @ApiModelProperty(value = "布控id", dataType = "String")
    @TableField("control_id")
    private String controlId;

    /**
     * 接收单位名称
     */
    @ApiModelProperty(value = "接收单位名称", dataType = "String")
    @TableField("receive_unit_name")
    private String receiveUnitName;

    /**
     * 接收指令内容
     */
    @ApiModelProperty(value = "接收指令内容", dataType = "Integer")
    @TableField("receive_content")
    private String receiveContent;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", dataType = "Date", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time")
    private Date createTime;


    /**
     * 当前页码
     */
    @ApiModelProperty(value = "当前页码", dataType = "Long", example = "1")
    @TableField(exist = false)
    private Long page;

    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页条数", dataType = "Long", example = "15")
    @TableField(exist = false)
    private Long size;

    /**
     * 排序sql
     */
    @ApiModelProperty(value = "排序sql", dataType = "String")
    @TableField(exist = false)
    private String sortSql;

}
