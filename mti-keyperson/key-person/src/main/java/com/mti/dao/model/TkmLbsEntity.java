package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 微信/qq定位
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_lbs")
public class TkmLbsEntity extends Model<TkmLbsEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",name = "id",example = "12")
    @TableField("id")
    private String id;

    /**
     * 微信或QQ号
     */
    @ApiModelProperty(value = "账号",name = "account",example = "123456789")
    @TableField("account")
    private String account;

    /**
     * 经度
     */
    @ApiModelProperty(value = "经度",name = "lng")
    @TableField("lng")
    private String lng;

    /**
     * 维度
     */
    @ApiModelProperty(value = "维度",name = "lat")
    @TableField("lat")
    private String lat;

    /**
     * 关联重点人员的身份证号
     */
    @ApiModelProperty(value = "维度",name = "lat")
    @TableField("ref_sfzh")
    private String refSfzh;

    /**
     * 0:删除，1：未删除
     */
    @ApiModelProperty(value = "删除标志",name = "deleted")
    @TableField("deleted")
    @TableLogic
    private Integer deleted;

    @ApiModelProperty(value = "插入时间",name = "insertTime")
    @TableField("insert_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date insertTime;

    @ApiModelProperty(value = "预警类型",name = "yjlx")
    @TableField(exist = false)
    private String yjlx;


    @Override
    protected Serializable pkVal() {
        return null;
    }

    public String getYjlx() {
        return "lbs";
    }
}
