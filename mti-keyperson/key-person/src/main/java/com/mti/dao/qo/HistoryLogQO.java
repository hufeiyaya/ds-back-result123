/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel(value = "历史记录对象", description = "历史记录对象")
public class HistoryLogQO  extends PageQO{

	/**
	 * 开始时间
	 */
	@ApiModelProperty(value = "开始时间")
	private String startTime;

	/**
	 * 结束时间
	 */
	@ApiModelProperty(value = "结束时间")
	private String endTime;

	/**
	 * 关键字搜索：creator，operateDept，xm，sfzh
	 */
	private String keyWords;


	/**
	 * 操作
	 */
	@ApiModelProperty(value = "操作")
	private String operation;

	/**
	 * 记录类型
	 */
	@ApiModelProperty(value = "记录类型")
	private Integer type;

	/**
	 * 重点人员ID
	 */
	@ApiModelProperty(value = "重点人员ID")
	private String  zdryId;


	
	/**
	 * 记录条数
	 */
	@ApiModelProperty(value = "记录条数")
	private Integer limit;

	/**
	 * 警种
	 */
	@ApiModelProperty(value = "责任部门")
	private List<String> zrbmList;

	/**
	 * 分局
	 */
	@ApiModelProperty(value = "责任分局",name = "zrfj")
	private List<String> zrfj;

	/**
	 * 派出所
	 */
	@ApiModelProperty(value = "所属派出所")
	private List<String> sspcsList;

	/**
	 * 操作内容
	 */
	@ApiModelProperty(value = "操作内容")
	private String approveResult;
	



}
