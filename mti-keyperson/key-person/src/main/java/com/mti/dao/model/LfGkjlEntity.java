package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_gkjl")
public class LfGkjlEntity {

    /**
     * 流水号
     */
    @TableId("id")
    private String id;

    /**
     * 管控类型（大类）：1 访谈 2 协查
     */
    @ApiModelProperty(value = "管控类型", dataType = "String", example = "1 访谈 2 协查" )
    @TableField("lxdl")
    private String lxdl;

    /**
     * 类型
     */
    @ApiModelProperty(value = "类型", dataType = "String")
    @TableField("lx")
    private String lx;

    /**
     * 内容
     */
    @ApiModelProperty(value = "内容", dataType = "String")
    @TableField("nr")
    private String nr;

    /**
     * 图片 地址逗号隔开
     */
    @ApiModelProperty(value = "图片(多个地址逗号隔开)", dataType = "String", example = "/storage/emulated/0/Android/data/com.mti.lfkp/cache/luban_disk_cache/1559054129991989.jpg,/storage/emulated/0/Android/data/com.mti.lfkp/cache/luban_disk_cache/1559054136010147.jpg" )
    @TableField("tp")
    private String tp;

    /**
     * 视频 地址逗号隔开
     */
    @ApiModelProperty(value = "视频(多个地址逗号隔开)", dataType = "String", example = "/storage/emulated/0/daoshu/video/video_1559054172455.mp4" )
    @TableField("sp")
    private String sp;

    /**
     * 录音 地址逗号隔开
     */
    @ApiModelProperty(value = "录音(多个地址逗号隔开)", dataType = "String", example = "/storage/emulated/0/daoshu/audio/mti_1559053480342.wav,/storage/emulated/0/daoshu/audio/mti_1559053480342.wav," )
    @TableField("ly")
    private String ly;

    /**
     * 重点人员身份证号
     */
    @ApiModelProperty(value = "姓名", dataType = "String")
    @TableField("sfzh")
    private String sfzh;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名", dataType = "String")
    @TableField("xm")
    private String xm;

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址", dataType = "String")
    @TableField("dz")
    private String dz;

    /**
     * 是否在控
     */
    @ApiModelProperty(value = "是否在控", dataType = "String", example = "0 失控 1 在控" )
    @TableField("sfzk")
    private String sfzk;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time")
    private Date create_time;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("update_time")
    private Date update_time;
}
