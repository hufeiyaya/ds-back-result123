package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 卡点警力分布信息
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-26
 */
@Data
@Accessors(chain = true)
@TableName("t_contact_card_police_force")
@ApiModel(value = "卡点警力分布信息", description = "卡点警力分布信息")
public class ContactCardPoliceForceEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 单位名称
     */
    @ApiModelProperty(value = "单位名称", dataType = "String")
    @TableField("unit_name")
    private String unitName;

    /**
     * 卡口名称
     */
    @ApiModelProperty(value = "卡口名称", dataType = "String")
    @TableField("card_name")
    private String cardName;

    /**
     * 位置
     */
    @ApiModelProperty(value = "位置", dataType = "String")
    @TableField("location_")
    private String location;

    /**
     * 责任单位
     */
    @ApiModelProperty(value = "责任单位", dataType = "String")
    @TableField("responsible_unit")
    private String responsibleUnit;

    /**
     * 距离
     */
    @ApiModelProperty(value = "距离", dataType = "String")
    @TableField("distance_")
    private String distance;

    /**
     * 类型
     */
    @ApiModelProperty(value = "类型", dataType = "String")
    @TableField("type_")
    private String type;

    /**
     * 负责人
     */
    @ApiModelProperty(value = "负责人", dataType = "String")
    @TableField("principal_")
    private String principal;

}
