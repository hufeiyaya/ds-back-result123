package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("cp_event")
public class EventEntity {
	@TableId("event_id")
	private String eventId;

	@TableField("police_man")
	private String policeMan;

	@TableField("phone")
	private String phone;

	@TableField("case_type")
	private String caseType;

	@TableField("record")
	private String record;

	@TableField("unit")
	private String unit;

	@TableField("level")
	private String level;

	@TableField("address")
	private String address;

	@TableField("content")
	private String content;

	@TableField("status")
	private Integer status;

	@TableField("longitude")
	private Double longitude;

	@TableField("latitude")
	private Double latitude;

	@TableField("record_mp3_url")
	private String recordMp3Url;

	@TableField("record_mp3_length")
	private Integer recordMp3Length;

	@TableField("create_time")
	private Date createTime;

	/**
	 * 分局单位id
	 */
	@TableField("branch_unit_id")
	private String branchUnitId;

	/**
	 * 分局单位
	 */
	@TableField("branch_unit")
	private String branchUnit;

	/**
	 * 管辖单位警员id
	 */
	@TableField("unit_police_id")
	private String unitPoliceId;

	/**
	 * 管辖单位警员姓名
	 */
	@TableField("unit_police_name")
	private String unitPoliceName;

	/**
	 * 所属组织id
	 */
	@TableField("unit_id")
	private String unitId;

	/**
	 * 案件标题
	 */
	@TableField("title_")
	private String title;

	@TableField("event_type")
	private String eventType;

	@TableField("sfzh")
	private String sfzh;

	@TableField("send_time")
	private LocalDateTime sendTime;

	@TableField(exist = false)
	private String zrbm;
	@TableField("warning_type")
	private String warning_type;
	@TableField("warning_id")
	private String warning_id;
	@TableField(exist = false)
	private String mjId;

}
