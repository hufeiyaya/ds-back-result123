/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel(value = "被关注重点人员查询对象", description = "被关注重点人员查询对象")
public class TPfGroupZdryQo extends PageQO {
    @ApiModelProperty(value = "用户ID", name = "id", hidden = true)
    private String accId;
    @ApiModelProperty(value = "分组ID", name = "group_id")
    private String groupId;

}
