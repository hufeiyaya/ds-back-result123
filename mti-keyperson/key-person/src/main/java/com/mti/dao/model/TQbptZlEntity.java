package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yangsp
 * @since 2019-06-25
 */
@Data
@Accessors(chain = true)
@TableName("t_qbpt_zl")
public class TQbptZlEntity {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId("id")
    private String id;

    /**
     * 同步数据
     */
    @TableField("data")
    private String data;

    /**
     * 指令id
     */
    @TableField("zlid")
    private String zlid;

    /**
     * 指令标题
     */
    @TableField("zlbt")
    private String zlbt;

    /**
     * 指令时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("zlsj")
    private Date zlsj;

    /**
     * 指令类型 1: 情报指令预警 2：布控信息预警 3：其他预警
     */
    @TableField("zllx")
    private String zllx;

    /**
     * 是否重点人员：0 否 1 是
     */
    @TableField("is_zdry")
    private String is_zdry;

    /**
     * 成功标识
     */
    @TableField("success")
    private Boolean success;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time")
    private Date create_time;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("update_time")
    private Date update_time;

}
