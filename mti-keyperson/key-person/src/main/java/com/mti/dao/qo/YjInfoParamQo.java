package com.mti.dao.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Classname YjInfoParamQo
 * @Description TODO
 * @Date 2020/3/17 16:44
 * @Created by duchaof
 * @Version 1.0
 */
@Data
@ApiModel(value = "查询定位详细信息时的参数类")
public class YjInfoParamQo {
    @ApiModelProperty(value = "预警类型：wbsw,lgzs,lbs,sjdw",name="type",example = "wbsw")
    private String type;

    @ApiModelProperty(value = "预警唯一标识集合",name = "wybses")
    private List<String> wybses;
}
