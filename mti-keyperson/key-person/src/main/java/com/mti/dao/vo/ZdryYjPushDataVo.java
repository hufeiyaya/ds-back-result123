package com.mti.dao.vo;

import com.mti.constant.ZdryYjLx;
import com.mti.dao.model.KmZdryAssistEntity;
import lombok.Data;

import java.util.List;

/**
 * @Classname ZdryYjPushDataVo
 * @Description TODO
 * @Date 2020/5/4 13:59
 * @Created by duchaof
 * @Version 1.0
 */
@Data
public class ZdryYjPushDataVo {

    /**
     * 预警编号
     */
    private String alarmId;
    /**
     * 预警人员姓名
     */
    private String name;
    /**
     * 身份证号
     */
    private String idcard;

    /**
     * 重点人员手机号
     */
    private String phone;

    /**
     * 预警发生地分局
     */
    private String yjdSsfj;
    /**
     * 预警发生地派出所
     */
    private String yjdPcs;
    /**
     * 预警发生地派出所编码
     */
    private String policeStation;
    /**
     * 责任分局
     */
    private String ssxq;

    /**
     * 责任分局id
     */
    private String ssxqId;
    /**
     * 责任派出所
     */
    private String sspcs;
    /**
     * 责任派出所编码
     */
    private String controlUnit;
    /**
     * 管控民警
     */
    private String police;
    /**
     * 管控民警电话
     */
    private String policePhone;
    /**
     * 人员大类
     */
    private String type;
    /**
     * 人员小类
     */
    private String xl;
    /**
     * 管控级别
     */
    private String controlLevel;
    /**
     * 预警处置类别
     */
    private String measure;
    /**
     * 预警类型
     */
    private String alarmType;
    /**
     * 经度
     */
    private String loc;
    /**
     * 维度
     */
    private String lat;
    /**
     * 照片
     */
    private String idcardPic;
    /**
     * 预警时间
     */
    private String alarmTime;
    /**
     * 预警地址
     */
    private String alarmLocation;
    /**
     * 预警内容
     */
    private String alarmContent;
    /**
     * 性别
     */
    private String sex;
    /**
     * 民族
     */
    private String nationalName;

    /**
     * 诉求
     */
    private String appeal;

    /**
     * 进出标志
     */
    private Integer inOut;
    /**
     * 出发站
     */
    private String sendStation;
    /**
     * 终点站
     */
    private String endStation;

    /**
     * 所属州市名称
     */
    private String sszsmc;
    /**
     * 辅助信息
     */
    private List<KmZdryAssistEntity> assist;

//    private static final List<String> ssxqes = Arrays.asList("530113000000","530129000000","530127000000","530125000000",
//            "530126000000","530128000000","530124000000","530181000000","530122000000");
//
//    private static final Map<String, String> yjdFjNameMap = new HashMap<String, String>(){
//        {
//            put("东川分局","530113000000");
//            put("东川区公安局","530113000000");
//            put("寻甸县局","530129000000");
//            put("寻甸回族彝族自治县公安局","530129000000");
//            put("嵩明县局","530127000000");
//            put("嵩明县公安局","530127000000");
//            put("宜良县局","530125000000");
//            put("宜良县公安局","530125000000");
//            put("石林县局","530126000000");
//            put("石林彝族自治县公安局","530126000000");
//            put("禄劝县局","530128000000");
//            put("禄劝彝族苗族自治县公安局","530128000000");
//            put("富民县局","530124000000");
//            put("富民县公安局","530124000000");
//            put("安宁市局","530181000000");
//            put("安宁市公安局","530181000000");
//            put("晋宁分局","530122000000");
//        }
//    };

    public ZdryYjPushToDsUtil convertToZdryYjPushToDsUtil() {
        ZdryYjPushToDsUtil zdry = new ZdryYjPushToDsUtil();
        zdry.setAlarmId(this.alarmId == null ? "" : this.alarmId);
        zdry.setName(this.getName() == null ? "" : this.getName());
        zdry.setIdcard("");
        zdry.setPhone(this.getPhone() == null ? "" : this.getPhone());
        zdry.setControlUnit(this.controlUnit);
        zdry.setPoliceStation(this.policeStation);
        zdry.setPolice("");
        zdry.setPolicePhone("");
        zdry.setType("");
        zdry.setControlLevel("");
        zdry.setMeasure("");
        zdry.setAlarmType("");
        zdry.setLoc(this.getLoc() == null ? "" : this.getLoc());
        zdry.setLat(this.getLat() == null ? "" : this.getLat());
        zdry.setIdcardPic(this.getIdcardPic() == null ? "" : this.getIdcardPic());
        zdry.setAlarmTime(this.getAlarmTime() == null ? "" : this.getAlarmTime());
        zdry.setAlarmLocation(this.getAlarmLocation() == null ? "" : this.getAlarmLocation());
        StringBuilder sb = new StringBuilder();
        sb.append("预警类别:").append(ZdryYjLx.YJLX.get(this.getAlarmType())).append(",预警处置级别:").append(this.measure)
                .append(",").append(this.type).append("类").append(this.xl).append("重点人员：").append("姓名：").append(this.name)
                .append(",身份证号：").append(this.idcard).append(",");
        if (null != this.phone) {
            StringBuilder sbPhone = new StringBuilder();
            if (null != this.getAssist() && this.getAssist().size() > 0) {
                this.getAssist().forEach(e -> {
                    if (sbPhone.length() > 0) {
                        sbPhone.append("/").append(e.getInfoValue());
                    } else {
                        sbPhone.append(e.getInfoValue());
                    }
                });
            }
            sb.append("手机：").append(sbPhone.toString()).append(",");
        } else {
            sb.append("手机：").append("暂无").append(",");
        }
        sb.append("预警时间：").append(this.alarmTime).append(",");
        sb.append("人员诉求:");
        if (null != this.appeal) {
            sb.append(this.appeal);
        } else {
            sb.append("暂无");
        }
        sb.append(",管控责任分局:");

        sb.append(this.getSsxq() == null ? "暂无" : this.getSsxq());
//                if(null != this.getSszsmc() && (this.getSszsmc().equals("昆明")|| this.getSszsmc().equals("0")))
//                else sb.append("暂无");

        sb.append(",管控责任派出所:").append(this.getSspcs() == null ? "暂无" : this.getSspcs());
//                if(null != this.getSszsmc() && (this.getSszsmc().equals("昆明")|| this.getSszsmc().equals("0")))
//                else sb.append(",管控责任派出所:").append("暂无");

        if (this.getPolice() == null || this.getPolice().length() <= 0) {
            sb.append(",管控责任民警:").append("暂无");
        } else {
            sb.append(",管控责任民警:").append(this.getPolice());
        }
        if (this.getPolicePhone() == null || this.getPolicePhone().length() <= 0) {
            sb.append(",民警联系电话:").append("暂无");
        } else {
            sb.append(",民警联系电话:").append(this.policePhone);
        }
        sb.append(",在");
        if (this.getAlarmType().equals("tldp") || this.getAlarmType().equals("mhdz") || this.getAlarmType().equals("mhlg") ||
                this.getAlarmType().equals("glky-gp")) {
            if (this.inOut == 1) {
                sb.append(this.endStation).append("(").append(this.alarmLocation).append(")");
            } else if (this.inOut == 2) {
                sb.append(this.sendStation).append("(").append(this.alarmLocation).append(")");
            }
        } else {
            sb.append(this.alarmLocation);
        }
        sb.append("出现,");
        if (this.alarmType.equals("wbsw") || this.alarmType.equals("lgzs") || this.alarmType.equals("glky-gp") ||
                this.alarmType.equals("phyj")) {
            sb.append("预警地分局：");
            if (null != this.yjdSsfj) {
                sb.append(this.yjdSsfj);
            } else {
                sb.append("暂无");
            }
            sb.append(",预警地派出所:");
            if (null != this.yjdPcs) {
                sb.append(this.yjdPcs).append(",");
            } else {
                sb.append("暂无,");
            }
        }
        // 2020-11-04 客户王涛 要求删掉
               /* sb.append("请按照市局" +
                "《关于省市及全国“两会”安保期间对涉稳涉访重点人员开展大数据预警核查的通知》相关要求开展预警处置工作。注意事项：预警触发地" +
                        "单位接到预警后及时联系管控责任单位或管控责任民警按管控单位要求开展查找协控工作，并在30分钟内通过接处警系统反馈处理" +
                        "结果（找到、未找到）、核查处置情况。管控责任单位持续反馈后续工作情况。重大情况及时反馈警令部指挥中心。");*/
        //2020-12-03 客户要求追加以下内容
        sb.append("备注:详情请到昆明市公安局重点人员动态管控系统查看");

        zdry.setAlarmContent(sb.toString());
        zdry.setSex("");
        zdry.setNationalName("");
        return zdry;
    }


}
