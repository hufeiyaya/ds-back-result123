package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 重点人员分组表
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_pf_group")
public class TPfGroupEntity extends Model<TPfGroupEntity> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    @ApiModelProperty(value = "主键", name = "id", hidden = true)
    private String id;

    @TableField("account_id")
    @ApiModelProperty(value = "关联账户表id", name = "account_id", hidden = true)
    private String accountId;

    @TableField("group_name")
    @ApiModelProperty(value = "分组名称", name = "group_name")
    private String groupName;

    /**
     * true  默认
     * false 非默认
     */
    @TableField("is_default")
    @ApiModelProperty(value = "是否是默认分组", name = "is_default", hidden = true)
    private boolean isDefault;

}
