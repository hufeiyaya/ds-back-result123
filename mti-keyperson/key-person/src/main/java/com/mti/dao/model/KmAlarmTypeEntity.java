package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 预警设置过滤表
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_alarm_filter")
public class KmAlarmTypeEntity extends Model<KmAlarmTypeEntity> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    @ApiModelProperty(value = "主键", name = "id", hidden = true)
    private String id;

    @TableField("name")
    @ApiModelProperty(value = "预警名称", name = "name")
    private String name;

    @TableField("status")
    @ApiModelProperty(value = "预警过滤状态:true-开,false-关", name = "status", hidden = true)
    private boolean status;


}
