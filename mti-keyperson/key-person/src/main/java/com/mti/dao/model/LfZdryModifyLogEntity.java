package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_lf_zdry_operate_log")
public class LfZdryModifyLogEntity {
    @TableId
    private String id;
    @TableField(value = "creator")
    private String creator;
    @TableField(value = "create_time")
    private LocalDateTime createTime;
    @TableField(value = "modifier")
    private String modifier;
    @TableField(value = "last_update_time")
    private LocalDateTime modifyTime;
    @TableField(value = "log_data")
    private String log;
    @TableField(value = "zdry_id")
    private String zdryId;
    @TableField(value = "xgyy")
    private String xgyy;

}
