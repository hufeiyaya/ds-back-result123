package com.mti.dao.dto;

import lombok.Data;

/**
 * @Classname ZdryYjHandlePushDto
 * @Description TODO
 * @Date 2020/6/19 11:40
 * @Created by duchaof
 * @Version 1.0
 */
@Data
public class ZdryYjHandlePushDto {
    /**
     * 告警ID
     */
     private String NotificationID;
    /**
     * 告警反馈状态代码
     */
    private Integer AlarmStatus;
    /**
     * 反馈人
     */
     private String FeedbackPerson;
    /**
     * 反馈时间
     */
    private String FeedbackTime;
    /**
     * 反馈说明
     */
     private String FeedbackRemark;
}
