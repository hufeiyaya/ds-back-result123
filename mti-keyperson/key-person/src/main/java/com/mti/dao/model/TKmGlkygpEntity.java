package com.mti.dao.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.constant.ZdryYjLx;
import com.mti.utils.DateUtils;
import com.mti.utils.ZdryYjUtilBean;
import lombok.Data;

import java.util.Date;

@Data
public class TKmGlkygpEntity {
    private String pgPkid;

    private String zjlb;

    private String zjhm;

    private String xm;

    private String cpbh;

    private String zwh;

    private String gpsj;

    private String gaKyzid;

    private String czid;

    private String bc;

    private String xlmc;

    private String qdzMc;

    private String zdzMc;

    private String zdzXzqhbh;

    private String fbsj;

    private String gsmc;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date jssj;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pgRksj;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pgGxsj;



    public ZdryYjUtilBean transtoZdryYjUtilBean(){
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.getXm());
        zdryYjUtilBean.setSfzh(this.getZjhm());
        zdryYjUtilBean.setYjlx(ZdryYjLx.GLKYGP);
        zdryYjUtilBean.setYjdz(this.getGsmc());
        zdryYjUtilBean.setYjsj( DateUtils.convertStrDateToOtherStr(this.getGpsj(),"yyyyMMddHHmmss","yyyy-MM-dd HH:mm:ss"));
        return zdryYjUtilBean;
    }

}