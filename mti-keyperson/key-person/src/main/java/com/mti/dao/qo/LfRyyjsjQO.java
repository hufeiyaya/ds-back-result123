/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.qo;

import com.mti.dao.dto.TimeFlow;
import com.mti.dao.kmmodel.GisModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LfRyyjsjQO extends PageQO {

    @ApiModelProperty(value = "关键字")
    private String name;
    @ApiModelProperty(value = "预警标识")
    private String yjbs;
    @ApiModelProperty(value = "排序")
    private String orderBy;
    @ApiModelProperty(value = "布控任务id")
    private String bkrwId;
    @ApiModelProperty(value = "预警开始时间", name = "yjsj_start", example = "20200217 22:03:04")
    private String yjsj_start;
    @ApiModelProperty(value = "预警开始时间", name = "yjsj_end", example = "20200217 22:03:04")
    private String yjsj_end;
    @ApiModelProperty(value = "进出标志")
    private List<Map<String, Object>> inOut;
    @ApiModelProperty(value = "时间段集合")
    private List<Map<String, Object>> listTime;
    @ApiModelProperty(value = "人员类别")
    private List<String[]> rylbxList;
    @ApiModelProperty(value = "是否在控")
    private List<String> sfzkList;
    @ApiModelProperty(value = "责任部门")
    private List<String> zrbmList;
    @ApiModelProperty(value = "所属辖区")
    private List<String> ssxqList;
    @ApiModelProperty(value = "管控级别")
    private List<String> gkjbxList;
    @ApiModelProperty(value = "民警联系电话(责任民警)")
    private List<String> mjlxdhList;
    @ApiModelProperty(value = "所属派出所")
    private List<String> sspcsList;
    @ApiModelProperty(value = "是否布控预警(0:否，1：是)")
    private String isArming;
    @ApiModelProperty(value = "预警类别")
    private List<String> yjlbxs;
    @ApiModelProperty(value = "责任分局", name = "zrfj")
    private List<String> zrfj;
    @ApiModelProperty(value = "责任民警", name = "zrmj")
    private List<String> zrmj;
    @ApiModelProperty(value = "户籍省市县")
    private List<String[]> hjssxList;
    @ApiModelProperty(value = "预警处置类别")
    private List<String> yjczlbList;
    @ApiModelProperty(value = "稳控状态")
    private List<String> wkztList;
    @ApiModelProperty(value = "稳控状态")
    private List<String> ynzsList;
    @ApiModelProperty(value = "重点人员ID列表")
    private List<String> personIdList;
    @ApiModelProperty(value = "人员属性列表")
    private List<String> rysxList;
    @ApiModelProperty(value = "时间流数据", hidden = true)
    private List<TimeFlow> timeFlows;
    //情报中心用户进行预警人员查询时 需要对预警人员位置进行判断
    @ApiModelProperty(value = "位置数据", hidden = true)
    private List<GisModel> gisModels;
    @ApiModelProperty(value = "预警类型列表数据", hidden = true)
    private List<String> yjlxList;
    @ApiModelProperty(value = "预警类型控制(只包含手机预警和人脸识别)", hidden = true)
    private List<String> subYjlxList;
    public LfRyyjsjQO(String name, String yjsj_start, String yjsj_end, List<String[]> rylbxList, List<String> zrbmList,
                      List<String> gkjbxList, List<String> yjlbxs, List<String> zrfj, List<String> zrmj,
                      List<String[]> hjssxList, List<String> sspcsList, List<String> yjczlbList, List<String> wkztList) {
        this.name = name;
        this.yjsj_start = yjsj_start;
        this.yjsj_end = yjsj_end;
        this.rylbxList = rylbxList;
        this.zrbmList = zrbmList;
        this.gkjbxList = gkjbxList;
        this.yjlbxs = yjlbxs;
        this.zrfj = zrfj;
        this.zrmj = zrmj;
        this.hjssxList = hjssxList;
        this.sspcsList = sspcsList;
        this.yjczlbList = yjczlbList;
        this.wkztList = wkztList;
    }
}
