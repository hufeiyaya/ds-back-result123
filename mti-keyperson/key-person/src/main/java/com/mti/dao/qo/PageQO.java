package com.mti.dao.qo;

import com.mti.constant.PageConstants;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
public class PageQO{

	private Integer start = 1;
	
	private Integer size = PageConstants.PAGE_NUM_SIZE;
}
