package com.mti.dao.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 线索kafaka数据表 
 * </p>
 *
 * @author qiangxy
 * @since 2020-09-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_trail")
@NoArgsConstructor
@AllArgsConstructor
public class KmTrailEntity {

    /**
     * 主键id
     */
    @TableId("id")
    private String id;

    /**
     * 线索类型：1 聚集 2 伴随
     */
    @TableField("trail_type")
    private String trailType;

    /**
     * 开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField("start_time")
    private Date startTime;

    /**
     * 结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField("end_time")
    private Date endTime;
   
    /**
     * 人数
     */
    @TableField("person_count")
    private String personCount;

    /**
     * 配置id度
     */
    @TableField("set_id")
    private String setId;


    /**
     * 配置名称
     */
    @TableField("set_name")
    private String setName;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time")
    private Date createTime;

    /**
     * 是否新数据
     */
    @TableField("is_new")
    private String isNew;

    /**
     * 线索信息
     */
    @TableField("trail_info")
    private String trailInfo;
    
    /**
     * 配置人员类型
     */
    @TableField("set_person_type")
    private String setPersonType;

    /**
     * 区域id
     */
    @TableField("area_id")
    private String areaId;
    
    @TableField("is_read")
    private String isRead;
    
    
    @TableField("sort")
    private Integer sort;

  
}
