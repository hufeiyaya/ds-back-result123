package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_trail_set_rel")
@AllArgsConstructor
@NoArgsConstructor
public class TKmTrailSetRelEntity {

	@TableId("id")
	@ApiModelProperty(value = "主键",name = "id")
    private String id;

    /**
     * 小类
     */
    @TableField("small_class_name")
    @ApiModelProperty(value = "小类名称",name = "smallClassName")
    private String smallClassName;
    /**
        * 大类
     */
    @TableField("max_class_name")
    @ApiModelProperty(value = "大类名称",name = "maxClassName")
    private String maxClassName;
    

    /**
     * 小类code
     */
    @TableField("small_class_code")
    @ApiModelProperty(value = "小类code",name = "smallClassCode")
    private String smallClassCode;
    /**
        * 大类code
     */
    @TableField("max_class_code")
    @ApiModelProperty(value = "大类code",name = "maxClassCode")
    private String maxClassCode;
   
    /**
     * 关联ID
     */
	@TableField("set_id")
    @ApiModelProperty(value = "关联id",name = "setId")
    private String setId;
 

}
