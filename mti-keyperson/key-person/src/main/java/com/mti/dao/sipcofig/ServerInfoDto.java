package com.mti.dao.sipcofig;

import lombok.Data;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by xuebing
 * @date 2019/3/20
 * @change
 * @describe describe
 * 获取服务器配置信息
 **/
@Data
public class ServerInfoDto {
    /**
     * api接口地址
     */
    private String apiUrl;

    /**
     * 文件地址
     */
    private String fileBaseUrl;

    /**
     * imIP
     */
    private String imIp;

    /**
     * im端口
     */
    private String imPort;

    /**
     * sip服务器地址
     */
    private String sipIp;
    /**
     * sip服务器端口
     */
    private int sipPort;
    /**
     * sip账号
     */
    private String sipNumber;
    /**
     * 名称
     */
    private String sipName;

}