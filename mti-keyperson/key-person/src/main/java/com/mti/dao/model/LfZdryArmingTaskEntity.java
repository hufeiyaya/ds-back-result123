package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/21
 * @change
 * @describe describe
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_lf_zdry_arming_task")
public class LfZdryArmingTaskEntity extends BaseEntity implements Necessary {
    private static final long serialVersionUID = -9017592355387435381L;
    @TableField(value = "task_name")
    @ApiModelProperty(value = "任务名称")
    private String taskName;

    @TableField(value = "start_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间(yyyy-MM-dd)")
    private LocalDate startDate;

    @TableField(value = "end_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "结束日期(yyyy-MM-dd)")
    private LocalDate endDate;

    @TableField(value = "special_level")
    @ApiModelProperty(value = "重要程度")
    private Integer specialLevel;

    @TableField(value = "description")
    @ApiModelProperty(value = "描述")
    private String description;

    @TableField(value = "status")
    @ApiModelProperty(value = "状态(0：未开始，1：进行中，2：已结束)")
    private Integer status;

    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private LocalDate createTime;

    @TableField(exist = false)
    @ApiModelProperty(value = "重点人员列表",hidden = true)
    private List<String> specialPersonIds;

    @ApiModelProperty(value = "重点人群人数")
    @TableField(exist = false)
    private String specialPersonCount;

    @ApiModelProperty(value = "布防范围Id")
    @TableField(value = "arming_range_id")
    private String armingRangeId;

    @TableField(value = "arming_range")
    @ApiModelProperty(value = "布防范围")
    private String armingRange;
    @ApiModelProperty(value = "是否存在待处理(大于0:存在，等于0：不存在)")
    @TableField(exist = false)
    private Integer isUnDeal;
}
