package com.mti.dao.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.dao.model.KmZdryTypeEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Classname ZdryTxVo
 * @Description TODO
 * @Date 2020/4/9 23:22
 * @Created by duchaof
 * @Version 1.0
 */
@Data
public class ZdryTxVo {
    private String zdryId;
    private String wybs;
    private String yjsj;
    private String xm;
    private String sfzh;
    private String gkjb;
    private String rysx;
    private String yjczlb;
    private String wkzt;
    private String currentPosition;
    private String yjlx;
    private String yjdd;
    private String lng;
    private String lat;

    private String sendtime;
    private String sendPro;
    private String sendCity;
    private String sendStation;

    private String endname;
    private String endPro;
    private String endCity;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endStationTime;

    private String yjdPcs;
    private String yjdSsfj;
    //上机,入住时间
    private String startTime;
    // 下机，退房时间
    private String endTime;

    private String yjczlbName;

    private String rylbx;

    private String xl;

    private List<KmZdryTypeEntity> types;
}
