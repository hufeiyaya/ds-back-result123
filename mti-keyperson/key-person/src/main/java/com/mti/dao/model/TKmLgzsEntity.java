package com.mti.dao.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.constant.ZdryYjLx;
import com.mti.utils.ZdryYjUtilBean;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class TKmLgzsEntity {

    private  static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String xxzjbh;

    private String zdbh;

    private String lvkedaima;

    private String xingming;

    private String xingbie;

    private String minzu;

    private String chushengriqi;

    private String zhengjianmingcheng;

    private String zhengjianhaoma;

    private String yuanji;

    private String shengshixian;

    private String xiangzhi;

    private String timao;

    private String ruzhufanghao;

    private String ruzhushijian;

    private String tuifangshijian;

    private String leixing;

    private String tuanduidm;

    private String tuanduimc;

    private String lvguandaima;

    private String paichusuo;

    private String transfertime;

    private String receivetime;

    private String bz;

    private String hfbz;

    private String packname;

    private String rzbz;

    private String chcl;

    private String toLog;

    private String sfzh;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date rksj;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date rzsj;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date bzRksj;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date bzGxsj;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pgRksj;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pgGxsj;

    /**
     * 旅馆名称
     */
    private String lgmc;
    /**
     * 旅馆经度
     */
    private String lgjd;

    /**
     * 旅馆维度
     */
    private String lgwd;


    public ZdryYjUtilBean transtoZdryYjUtilBean(){
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.getXingming());
        zdryYjUtilBean.setSfzh(this.getSfzh());
        zdryYjUtilBean.setYjlx(ZdryYjLx.LGZS);
        if(null != this.getRzsj()){
        zdryYjUtilBean.setYjsj(sdf.format(this.getRzsj()));
          }
        zdryYjUtilBean.setYjdz(this.lgmc);
        zdryYjUtilBean.setLng(this.lgjd);
        zdryYjUtilBean.setLat(this.lgwd);
        return zdryYjUtilBean;
    }

}