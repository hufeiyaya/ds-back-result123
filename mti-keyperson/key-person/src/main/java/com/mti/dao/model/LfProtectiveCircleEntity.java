package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_protective_circle")
public class LfProtectiveCircleEntity {

    @TableId("pid")
    private String pid;

    @TableField("xh")
    private String xh;

    @TableField("cj")
    private String cj;

    @TableField("lx")
    private String lx;

    @TableField("zdmc")
    private String zdmc;

    @TableField("zdid")
    private String zdid;

    @TableField("zrdw")
    private String zrdw;

    @TableField("gklx")
    private String gklx;

    @TableField("xq")
    private String xq;

    @TableField("jwd")
    private String jwd;

}
