package com.mti.dao.vo;

import lombok.Data;

/**
 * @Classname ZdryYjPushToDsUtil
 * @Description TODO
 * @Date 2020/5/5 16:19
 * @Created by duchaof
 * @Version 1.0
 */
@Data
public class ZdryYjPushToDsUtil {
    /**
     * 预警编号
     */
    private String alarmId;
    /**
     *  预警人员姓名
     */
    private String name;
    /**
     * 身份证号
     */
    private String idcard;

    /**
     * 重点人员手机号
     */
    private String phone;
    /**
     * 预警发生地派出所编码
     */
    private String policeStation;
    /**
     * 责任派出所编码
     */
    private String controlUnit;
    /**
     * 管控民警
     */
    private String police;
    /**
     * 管控民警电话
     */
    private String policePhone;
    /**
     * 人员大类
     */
    private String type;
    /**
     * 管控级别
     */
    private String controlLevel;
    /**
     * 预警处置类别
     */
    private String measure;
    /**
     * 预警类型
     */
    private String alarmType;
    /**
     * 经度
     */
    private String loc;
    /**
     * 维度
     */
    private String lat;
    /**
     * 照片
     */
    private String idcardPic;
    /**
     * 预警时间
     */
    private String alarmTime;
    /**
     * 预警地址
     */
    private String alarmLocation;
    /**
     * 预警内容
     */
    private String alarmContent;
    /**
     * 性别
     */
    private String sex;
    /**
     * 民族
     */
    private String nationalName;
}
