package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 重点人员表
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_zdry_home")
@NoArgsConstructor
@AllArgsConstructor
public class KmZdryHomeEntity extends Model<KmZdryHomeEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId("id")
    @ApiModelProperty(value = "主键",name = "id")
    private String id;

    /**
     * 重点人员ID
     */
    @TableField("zdry_id")
    @ApiModelProperty(value = "重点人员ID",name = "zdryId")
    private String zdryId;

    /**
     * 实际住地省

     */
    @TableField("xzzs")
    @ApiModelProperty(value = "实际住地省",name = "xzzs")
    private String xzzs;

    /**
     * 实际住地市
     */
    @TableField("xzzss")
    @ApiModelProperty(value = "实际住地市",name = "xzzss")
    private String xzzss;

    /**
     * 实际住地县
     */
    @TableField("xzzx")
    @ApiModelProperty(value = "实际住地县",name = "xzzx")
    private String xzzx;

    /**
     * 实际居住地址
     */
    @TableField("xzz")
    @ApiModelProperty(value = "实际居住地址",name = "xzz")
    private String xzz;

    /**
     * 序号
     */
    @TableField("index")
    @ApiModelProperty(value = "序号",name = "index")
    private Integer index;

    /**
     * 实际居住地址经度
     */
    @TableField("x")
    @ApiModelProperty(value = "实际居住地址经度",name = "x")
    private String x;

    /**
     * 实际居住地址纬度
     */
    @TableField("y")
    @ApiModelProperty(value = "实际居住地址纬度",name = "y")
    private String y;

    /**
     * 主居住地标志
     */
    @TableField("zjzdbj")
    @ApiModelProperty(value = "主居住地标志(1主居住地 0辅居住地)",name = "zjzdbj")
    private Integer zjzdbj;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
