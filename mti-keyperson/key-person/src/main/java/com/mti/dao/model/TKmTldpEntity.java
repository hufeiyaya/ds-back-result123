package com.mti.dao.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.constant.ZdryYjLx;
import com.mti.utils.ZdryYjUtilBean;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class TKmTldpEntity {
    private  static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private String kind;

    private String sex;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pgGxsj;

    private String idNo;

    private String realName;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date reserveTime;

    private String seatTypeName;

    private String passengerIdNo;

    private String userName;

   // @JSONField(format = "yyyy-MM-dd HH:mm:ss")
   @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date trainDate;

    private String country;

    private String seatNo;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date jlSccjsj;

    private String coachNo;

    private String dpbh;

    private String ticketTypeName;

    private String state;

    private String xxzjbh;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date bornDate;

    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pgRksj;

    private String mobileNo;

    private String fromStationName;

    private String passengerName;

    private String toStationName;

    private String ticketPrice;

    private String boardTrainCode;

    private String idName;

    private String email;

    private String phoneNo;

    public ZdryYjUtilBean transtoZdryYjUtilBean(){
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.realName);
        zdryYjUtilBean.setSfzh(this.getIdNo());
        zdryYjUtilBean.setYjlx(ZdryYjLx.TLDP);
        if(null !=this.getTrainDate())
        zdryYjUtilBean.setYjsj(sdf.format(this.getTrainDate()));
        zdryYjUtilBean.setYjdz(this.getFromStationName());
        return zdryYjUtilBean;
    }

}