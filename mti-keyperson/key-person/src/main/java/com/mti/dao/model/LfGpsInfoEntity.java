package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_gps_info")
public class LfGpsInfoEntity {


    @TableId("gpsid")
    private String gpsid;

//    @TableField("policetypeid")
//    private String policetypeid;

//    @TableField("gpsstyleid")
//    private String gpsstyleid;

//    @TableField("orgid")
//    private String orgid;

    @TableField("callno")
    private String callno;

    @TableField("uim")
    private String uim;

//    @TableField("cartype")
//    private String cartype;

    @TableField("carno")
    private String carno;

//    @TableField("policeid")
//    private String policeid;
//
//    @TableField("policename")
//    private String policename;
//
//    @TableField("remark")
//    private String remark;
//
//    @TableField("loctype")
//    private String loctype;
//
//    @TableField("xindao")
//    private String xindao;
//
//    @TableField("havemonitor")
//    private String havemonitor;
//
//    @TableField("imagename")
//    private String imagename;
//
//    @TableField("remark1")
//    private String remark1;
//
//    @TableField("remark2")
//    private String remark2;
//
//    @TableField("remark3")
//    private String remark3;
//
//    @TableField("remark4")
//    private String remark4;
//
//    @TableField("remark5")
//    private String remark5;
//
//    @TableField("x")
//    private String x;
//
//    @TableField("y")
//    private String y;
//
//    @TableField("realtime")
//    private String realtime;
//
//    @TableField("oldorgid")
//    private String oldorgid;

    @TableField("picturetype")
    private String picturetype;

}
