package com.mti.dao.qo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
public class LfGpsInfoQO extends PageQO{

	/**
	 * 关键字
	 */
	private String name;

}
