/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel(value = "文件对象", description = "文件对象")
public class FileQO  {

	/**
	 * 文件名称
	 */
	@ApiModelProperty(value = "文件名称")
	private String fileName;

	/**
	 * 文件url
	 */
	@ApiModelProperty(value = "文件url")
	private String fileUrl;
}
