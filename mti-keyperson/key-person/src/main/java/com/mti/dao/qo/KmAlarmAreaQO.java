/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.qo;

import com.mti.dao.kmmodel.GisModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class KmAlarmAreaQO {
    @ApiModelProperty(value = "横坐标")
    private String x;
    @ApiModelProperty(value = "纵坐标")
    private String y;
    @ApiModelProperty(value = "位置数据", hidden = true)
    private List<GisModel> gisModels;

}
