package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 布控信息
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-15
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_zdry_control")
@ApiModel(value = "布控信息对象", description = "布控信息对象")
public class LfZdryControlEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号", dataType = "String")
    @TableId(value = "id_", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 布控人
     */
    @ApiModelProperty(value = "布控人", dataType = "String")
    @TableField("name_")
    private String name;

    /**
     * 布控原因
     */
    @ApiModelProperty(value = "布控原因", dataType = "String")
    @TableField("reason_")
    private String reason;

    /**
     * 布控时间
     */
    @ApiModelProperty(value = "布控时间", dataType = "String", example = "2019-07-15至2019-08-31")
    @TableField("control_time")
    private String controlTime;

    /**
     * 申请机构id
     */
    @ApiModelProperty(value = "申请机构id", dataType = "String")
    @TableField("org_id")
    private String orgId;

    /**
     * 申请机构名称
     */
    @ApiModelProperty(value = "申请机构名称", dataType = "String")
    @TableField("org_name")
    private String orgName;

    /**
     * 申请人id
     */
    @ApiModelProperty(value = "申请人id", dataType = "String")
    @TableField("police_id")
    private String policeId;

    /**
     * 申请人姓名
     */
    @ApiModelProperty(value = "申请人姓名", dataType = "String")
    @TableField("police_name")
    private String policeName;

    /**
     * 申请人警号
     */
    @ApiModelProperty(value = "申请人警号", dataType = "String")
    @TableField("police_code")
    private String policeCode;

    /**
     * 申请时间
     */
    @ApiModelProperty(value = "申请时间", dataType = "Date", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time")
    private Date createTime;

    /**
     * 状态：0.已申请 1.已审批 2.已退回
     */
    @ApiModelProperty(value = "状态", dataType = "Integer", notes = "0.已申请 1.已审批 2.已退回")
    @TableField("status_")
    private Integer status;

    /**
     * 退回原因
     */
    @ApiModelProperty(value = "退回原因", dataType = "String")
    @TableField("callback_reason")
    private String callbackReason;

    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("update_time")
    private Date updateTime;

    /**
     * 当前页码
     */
    @ApiModelProperty(value = "当前页码", dataType = "Long", example = "1")
    @TableField(exist = false)
    private Long page;

    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页条数", dataType = "Long", example = "15")
    @TableField(exist = false)
    private Long size;

    /**
     * 排序sql
     */
    @ApiModelProperty(value = "排序sql", dataType = "String")
    @TableField(exist = false)
    private String sortSql;

    /**
     * 状态列表
     */
    @ApiModelProperty(value = "状态列表", dataType = "List")
    @TableField(exist = false)
    private List<Integer> statusList;

}
