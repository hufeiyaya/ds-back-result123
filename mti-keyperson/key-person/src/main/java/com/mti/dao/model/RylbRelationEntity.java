package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author chenf
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_pf_lb_relation")
public class RylbRelationEntity {

    /**
     * 流水编号
     */
    @TableId("id")
    private String id;
	/**
	 * 昆明细类
	 */
    @TableField("km_ry_xl")
    private String kmryxl;
	/**
	 * 省厅大类代码
	 */
    @TableField("st_dl_code")
    private String stdlcode;
    /**
	 * 省厅大类名称
	 */
    @TableField("st_dl_name")
    private String stdlname;
    /**
	 * 省厅小类代码
	 */
    @TableField("st_xl_code")
    private String stxlcode;
    /**
	 * 省厅小类名称
	 */
    @TableField("st_xl_name")
    private String stxlname;
     

}
