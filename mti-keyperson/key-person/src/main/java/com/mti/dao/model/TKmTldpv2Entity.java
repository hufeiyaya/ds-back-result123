package com.mti.dao.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.mti.constant.ZdryYjLx;
import com.mti.utils.ZdryYjUtilBean;
import lombok.Data;

import java.util.Date;

@Data
public class TKmTldpv2Entity {
    private String systemid;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date bzkGxsj;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date bzkRksj;

    private String bzkScbz;

    private String bzkZzjg;

    private String ccBc;

    private String cfHcczmc;

    private String cxh;

    private String czLx;

    private String ddzMc;

    private String fcrqRqsj;

    private String hckSystemid;

    private String hiveRksj;

    private String lkcpzt;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgRksj;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date pgGxsj;

    private String xm;

    private String ywkSsxtmc;

    private String zjhm;

    private String zjlx;

    private String zjlxYwxt;

    private String zjlxZw;

    private String zwh;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date jlSccjsj;


    public ZdryYjUtilBean transtoZdryYjUtilBean() {
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.xm);
        zdryYjUtilBean.setSfzh(this.zjhm);
        zdryYjUtilBean.setYjlx(ZdryYjLx.TLDP);
        zdryYjUtilBean.setYjsj(this.fcrqRqsj);
        zdryYjUtilBean.setYjdz(this.cfHcczmc);
        return zdryYjUtilBean;
    }

}