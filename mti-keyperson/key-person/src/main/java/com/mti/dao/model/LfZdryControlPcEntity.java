package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 重点人员布控pc信息
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-26
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_zdry_control_pc")
@ApiModel(value = "重点人员布控pc信息", description = "重点人员布控pc信息")
public class LfZdryControlPcEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号", dataType = "String")
    @TableId(value = "id_", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 重点人员id
     */
    @ApiModelProperty(value = "重点人员id", dataType = "String")
    @TableField("zdry_id")
    private String zdryId;

    /**
     * 重点人员姓名
     */
    @ApiModelProperty(value = "重点人员姓名", dataType = "String")
    @TableField("zdry_name")
    private String zdryName;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", dataType = "Date", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time")
    private Date createTime;

    /**
     * 状态：0.未布控 1.已布控
     */
    @ApiModelProperty(value = "状态", dataType = "Integer", notes = "0.未布控 1.已布控")
    @TableField("status_")
    private Integer status;

    /**
     * 最后一次更新时间
     */
    @ApiModelProperty(value = "最后一次更新时间", dataType = "Date", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("update_time")
    private Date updateTime;


    /**
     * 重点人员布控pc接收单位信息
     */
    @ApiModelProperty(value = "重点人员布控pc接收单位信息", dataType = "List")
    @TableField(exist = false)
    private List<LfZdryControlPcReceiveEntity> zdryControlPcReceiveEntities;

    /**
     * 当前页码
     */
    @ApiModelProperty(value = "当前页码", dataType = "Long", example = "1")
    @TableField(exist = false)
    private Long page;

    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页条数", dataType = "Long", example = "15")
    @TableField(exist = false)
    private Long size;

    /**
     * 排序sql
     */
    @ApiModelProperty(value = "排序sql", dataType = "String")
    @TableField(exist = false)
    private String sortSql;

}
