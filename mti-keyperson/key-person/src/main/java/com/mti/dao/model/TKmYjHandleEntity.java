package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 预警处置表
 * </p>
 *
 * @author duchaof
 * @since 2020-03-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_yj_handle")
public class TKmYjHandleEntity extends Model<TKmYjHandleEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    @ApiModelProperty(value = "id",name = "id")
    private String id;

    /**
     * 处置结果：已化解 ，已控制，已遣返，无结果
     */
    @TableField("handle_result")
    @ApiModelProperty(value = "处置结果",name = "handleResult",example = "已化解")
    private String handleResult;

    /**
     * 处置内容
     */
    @ApiModelProperty(value = "处置内容",name = "handleContent",example = "已将重点人员带回派出所")
    @TableField("handle_content")
    private String handleContent;

    /**
     * 处置时间
     */
    @ApiModelProperty(value = "处置时间",name = "handleTime",example = "2020-03-11 11:57:02")
    @TableField("handle_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date handleTime;

    /**
     * 填报民警
     */
    @ApiModelProperty(value = "填报民警",name = "fillPolice",example = "张三")
    @TableField("fill_police")
    private String fillPolice;

    /**
     * 插入时间
     */
    @ApiModelProperty(hidden = true)
    @TableField("insert_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date insertTime;

    /**
     * 关联id，关联预警表t_if_ztryyjsj的主键
     */
    @ApiModelProperty(value = "关联id",name = "refId",example = "010203040506")
    @TableField("ref_id")
    private String refId;

    /**
     * 删除标志：0 删除，1 未删除
     */
    @ApiModelProperty(hidden = true)
    @TableField("deleted")
    @TableLogic
    private Integer deleted;

    @ApiModelProperty(value = "处置部门名称",name = "policeStation")
    @TableField("police_station")
    private String policeStation;

    @ApiModelProperty(value = "处置部门名称id",name = "policeStationId")
    @TableField("police_station_id")
    private String policeStationId;

    @ApiModelProperty(value = "处置人",name = "handlePolice")
    @TableField("handle_police")
    private String handlePolice;

    /**
     * 预警处置图片
     */
    @ApiModelProperty(value = "预警处置图片",name = "files")
    @TableField(exist = false)
    private List<TTaskFileEntity> files;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
