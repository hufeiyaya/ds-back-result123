package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "开关状态对象", description = "开关状态对象")
@TableName("t_switch_type")
public class TSwitchTypeEntity {
    @TableId("id")
    @ApiModelProperty(value = "ID", name = "id", hidden = true)
    private String id;
    @TableField("status")
    @ApiModelProperty(value = "状态", name = "status", required = false)
    private boolean status;
    @TableField("name")
    private String name;
    @ApiModelProperty(value = "描述", name = "describe", required = false)
    @TableField("describe")
    private String describe;
    @TableField("type")
    private int type;
    @ApiModelProperty(value = "小类Key", name = "key", required = false)
    @TableField("key")
    private String key;
    @ApiModelProperty(value = "大类Key", name = "parent_key", required = false)
    @TableField("parent_key")
    private String parentKey;
    @ApiModelProperty(value = "子类集合", name = "children", hidden = true)
    @TableField(exist = false)
    private List<TSwitchTypeEntity> children;
}
