package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/15
 * @change
 * @describe describe
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_lf_zdry_history")
public class LfZdryHistoryEntity {
    @TableId
    private String id;
    @TableField(value = "name")
    private String name;
    @TableField(value = "identity_id")
    private String identityId;
    @TableField(value = "resource_location")
    private String resourceLocation;
    @TableField(value = "person_type")
    private String personType;
    @TableField(value = "own_car_type")
    private String ownCarType;
    @TableField(value = "control_dep")
    private String controlDep;
    @TableField(value = "control_police")
    private String controlPolice;
    @TableField(value = "is_control")
    private String isControl;
    @TableField(value = "record_version")
    private String recordVersion;
    @TableField(value = "own_range")
    private String ownRange;
    @TableField(value = "own_range_id")
    private String ownRangeId;
    @TableField(value = "police_home_id")
    private String policeHomeId;
    @TableField(value = "zrbm")
    private String zrbm;

}
