package com.mti.dao.qo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @Classname TkmYjHanQo 预警处置查询参数类
 * @Description TODO
 * @Date 2020/3/11 14:55
 * @Created by duchaof
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class TkmYjHanQo extends PageQO {
    /**
     * 预警处置id
     */
    @ApiModelProperty(value = "预警处置id",name = "id")
    private String id;

    /**
     * 关键字 重点人员姓名 和 身份证
     */
    @ApiModelProperty(value = "关键字",name = "keyWords",example = "张三")
    private String keyWords;

    /**
     * 预警处置查询开始时间
     */
    @ApiModelProperty(value = "信访查询开始时间",name = "start_time",example = "2019-01-02 12:01:02")
    private String start_time;

    /**
     * 预警处置查询结束时间
     */
    @ApiModelProperty(value = "信访查询结束时间",name = "end_time",example = "2019-01-02 12:01:02")
    private String end_time;

    /**
     * 身份证号
     */
    @ApiModelProperty(value = "身份证号",name = "sfzh",example = "532224196110142726")
    private String sfzh;

    /**
     * 处置标志 1 为处置，2  超时，3 已处置
     */
    @ApiModelProperty(value = "处置标志",name = "handleFlag",example = "1")
    private Integer handleFlag;

    /**
     * 处置结果
     */
    @ApiModelProperty(value = "处置结果",name = "handleResult",example = "已化解")
    private String handleResult;

    /**
     * 处置内容
     */
    @ApiModelProperty(value = "处置内容",name = "handleContent",example = "已经人带回派出所")
    private String handleContent;

    /**
     * 关联id,关联预警表的id
     */
    @ApiModelProperty(value = "关联id",name = "refId")
    private String refId;


    /**
     * 所属分局
     */
    @ApiModelProperty(value = "所属分局",name = "ssfj",example = "五华分局")
    private String ssfj;

    /**
     * 管辖派出所
     */
    @ApiModelProperty(value = "管辖派出所",name = "sspcs",example = "五华分局华山东路派出所")
    private String sspcs;



    /**
     * 警种
     */
    @ApiModelProperty(value = "警种",name = "jz",example = "治安支队")
    private String jz;

    public TkmYjHanQo(String id,String keyWords,String start_time,String end_time,String sfzh,Integer handleFlag,String handleResult,
                      String handleContent,String refId,String ssfj,String sspcs,String jz){
        this.id=id;
        this.keyWords=keyWords;
        this.start_time=start_time;
        this.end_time=end_time;
        this.sfzh=sfzh;
        this.handleFlag=handleFlag;
        this.handleResult=handleResult;
        this.handleContent=handleContent;
        this.refId=refId;
        this.ssfj=ssfj;
        this.sspcs=sspcs;
        this.jz=jz;
    }
}
