package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 重点人员分组表
 * </p>
 *
 * @author hf
 * @since 2020-11-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_pf_group_zdry")
public class TPfGroupZdryEntity extends Model<TPfGroupZdryEntity> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    @ApiModelProperty(value = "主键", name = "id", hidden = true)
    private String id;

    @TableField("group_id")
    @ApiModelProperty(value = "分组ID", name = "group_id")
    private String groupId;

    @TableField("zdry_id")
    @ApiModelProperty(value = "重点人员ID", name = "zdry_id")
    private String zdryId;

}
