package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/9/12
 * @change
 * @describe describe
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_lf_zdry_arming_task_operate_log")
@ApiModel(description = "布防任务操作日志类")
public class LfZdryArmingTaskOperateLog extends BaseEntity implements Necessary {
    private static final long serialVersionUID = 3849951214015286931L;
    @TableField("operator")
    private String operator;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField("operate_date_time")
    private LocalDateTime operateDateTime;
    @TableField("operation")
    private String operation;
    @TableField("description")
    private String description;
    @TableField("task_id")
    private String taskId;
    @TableField("rid")
    private String rid;
    @TableField("sp_id")
    private String spId;
    @TableField(value = "operator_dept")
    private String operatorDept;
}
