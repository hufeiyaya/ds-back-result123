package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * Created by admin on 2020/8/24.
 */
@Data
@Accessors(chain = true)
@TableName("t_pf_phone_log")
public class PhoneLog {
        /**
         * 主键
         */
        @TableId("id")
        private String id;

        /**
         * 返参
         */
        @ApiModelProperty(value = "返参", dataType = "String")
        @TableField("response_phone")
        private String responsehone;

        /**
         * 创建时间
         */
        @ApiModelProperty(value = "创建时间", dataType = "String")
        @TableField("creat_time")
        private String creatTime;

        /**
         * 备注
         */
        @ApiModelProperty(value = "备注", dataType = "String")
        @TableField("remark")
        private String remark;

        /**
         * 接口返回状态
         */
        @ApiModelProperty(value = "接口返回状态", dataType = "String")
        @TableField("code_status")
        private String codeStatus;

}
