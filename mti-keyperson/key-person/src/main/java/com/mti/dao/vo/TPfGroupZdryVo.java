package com.mti.dao.vo;

import lombok.Data;


@Data
public class TPfGroupZdryVo {
    private String id;
    private boolean isDefault;
    private String name;
    private int count;
    private boolean in;
}
