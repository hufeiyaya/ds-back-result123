package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 手机定位
 * </p>
 *
 * @author duchaof
 * @since 2020-03-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_phone_position")
public class TkmPhonePositionEntity extends Model<TkmPhonePositionEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "id",name = "id")
    @TableId("id")
    private String id;

    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码",name = "phoneNum")
    @TableField("phone_num")
    private String phoneNum;

    /**
     * 经度
     */
    @ApiModelProperty(value = "经度",name = "lng")
    @TableField("lng")
    private String lng;

    /**
     * 维度
     */
    @ApiModelProperty(value = "维度",name = "lat")
    @TableField("lat")
    private String lat;

    /**
     * 关联重点人员的身份证
     */
    @ApiModelProperty(value = "身份证",name = "refSfzh")
    @TableField("ref_sfzh")
    private String refSfzh;

    /**
     * 删除标志：0 删除，1 未删除
     */
    @ApiModelProperty(value = "删除标志",name = "deleted")
    @TableField("ref_sfzh")
    private Integer deleted;

    @ApiModelProperty(value = "插入时间",name = "insertTime")
    @TableField("insert_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date insertTime;

    @ApiModelProperty(value = "预警类型",name = "yjlx")
    @TableField(exist = false)
    private String yjlx;


    @Override
    protected Serializable pkVal() {
        return null;
    }

    public String getYjlx() {
        return "sjdw";
    }
}
