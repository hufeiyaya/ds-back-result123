package com.mti.dao.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.constant.ZdryYjLx;
import com.mti.utils.DateUtils;
import com.mti.utils.ZdryYjUtilBean;
import lombok.Data;

import java.util.Date;

@Data
public class TKmMhdzEntity {
    private String xxzjbh;

    private String pnrRef;

    private String pnrCrDt;

    private String airCarrCd;

    private String airSegFltNbrSfx;

    private String airSegFltNbr;

    private String airSegDptDtLcl;

    private String airSegDptTmLcl;

    private String airSegArrvTmLcl;

    private String airSegArrvDtLcl;

    private String airSegDptAirptCd;

    private String airSegArrvAirptCd;

    private String subClsCd;

    private String offcCd;

    private String oprStatCd;

    private String rspAirlnCd;

    private String rspOfcCd;

    private String pasId;

    private String pasLstNm;

    private String pasFstNm;

    private String pasChnNm;

    private String pasIdType;

    private String pasIdNbr;

    private String ffpIdNbr;

    private String grpInd;

    private String grpNm;

    private String vipInd;

    private String pnSeat;

    private String operatedate;

    private String operatetime;

    private String filename;

    private String ctDt;
    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pgRksj;
    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pgGxsj;
    //@JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date jlSccjsj;
    /**
     * 等机机场名称
     */
    private String airSegDptAirptCdCn;
    /**
     * 到达机场名称
     */
    private String airSegArrvAirptCdCn;

    public ZdryYjUtilBean transtoZdryYjUtilBean(){
        ZdryYjUtilBean zdryYjUtilBean = new ZdryYjUtilBean();
        zdryYjUtilBean.setName(this.getPasChnNm());
        zdryYjUtilBean.setSfzh(this.getPasIdNbr());
        zdryYjUtilBean.setYjlx(ZdryYjLx.MHDZ);
        zdryYjUtilBean.setYjdz(this.airSegDptAirptCdCn);
        zdryYjUtilBean.setYjsj( DateUtils.convertStrDateToOtherStr(this.getOperatedate()+this.getOperatetime(),
                "yyyyMMddHH:mm:ss","yyyy-MM-dd HH:mm:ss"));
        return zdryYjUtilBean;
    }
}