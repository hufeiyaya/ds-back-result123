package com.mti.dao.qo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Classname TkmProHanQo
 * @Description TODO
 * @Date 2020/3/6 13:43
 * @Created by duchaof
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class TkmProHanQo  extends PageQO{

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键",name = "id")
    private String id;

    /**
     * 处理/化解人
     */
    @ApiModelProperty(value = "处理/化解人",name = "opPerson",example = "张三")
    private String opPerson;
    /**
     * 处理方式
     */
    @ApiModelProperty(value = "处理方式",name = "opType",example = "处理方式")
    private String opType;

    /**
     * 1 :处理，2：化解
     */
    @ApiModelProperty(value = "1 :处理，2：化解",name = "type",example = "1")
    private Integer type;

    /**
     * 信访/聚访id
     */
    @ApiModelProperty(value = "关联id",name = "refId",example = "38")
    private Integer refId;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间",name = "startTime",example = "2020-02-01 10:12:13")
    private String startTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间",name = "endTime",example = "2020-03-06 10:12:13")
    private String endTime;
}
