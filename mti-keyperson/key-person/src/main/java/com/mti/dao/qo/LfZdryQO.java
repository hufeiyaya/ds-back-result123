/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;

@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel(value = "重点人员查询对象", description = "重点人员查询对象")
public class LfZdryQO extends PageQO{

	/**
	 * 关键字
	 */
	@ApiModelProperty(value = "关键字：姓名、身份证号码、现住址、电话")
	private String name;

	/**
	 * 姓名
	 */
	@ApiModelProperty(value = "姓名")
	private String xm;

	/**
	 * 身份证号
	 */
	@ApiModelProperty(value = "身份证号")
	private String sfzh;

	/**
	 * 是否在控
	 */
	@ApiModelProperty(value = "是否在控")
	private String sfzk;

	@ApiModelProperty(value = "隐藏已审批(隐藏：1，不隐藏：0)")
	private String isApproved;

	@ApiModelProperty(value = "上报状态(查询所有状态：不传此参数，否则传入相关状态值)")
	private String approveStatus;

	/**
	 * 责任部门  -->昆明叫市局警种
	 */
	@ApiModelProperty(value = "责任部门")
	private String zrbm;
	
	/**
	 * 派出所   
	 */
	@ApiModelProperty(value = "所属派出所")
	private String sspcs;
	 
	/**
     * 所属辖区 ---分局
     */
    @ApiModelProperty(value = "所属辖区")
    private String ssxq;
	
    
    /**
     * 提交人Id
     */
    @ApiModelProperty(value = "提交人ID")
    private String submitter;
	/**
	 * 人员类别
	 */
//	private String rylbx;
	@ApiModelProperty(value = "人员类别")
	private List<String[]> rylbxList;

	@ApiModelProperty(value = "人员类别名称")
	private List<String> rylbxStrList;

	/**
	 * 是否在控
	 */
//	private String sfzk;
	@ApiModelProperty(value = "是否在控")
	private List<String> sfzkList;

	/**
	 * 人员属性
	 */
//	private String sfzk;
	@ApiModelProperty(value = "人员属性")
	private List<Integer> rysxList;

	/**
	 * 责任部门
	 */
//	private String zrbm;
	@ApiModelProperty(value = "责任部门")
	private List<String> zrbmList;

	/**
	 * 所属分局
	 */
//	private String ssxq;
	@ApiModelProperty(value = "所属分局")
	private List<String> ssxqList;
	
	/**
	 * 查询条件: 分局、派出所、个人  mjidList/sspcsList/ssxqList
	 */
//	private String queryList;
	@ApiModelProperty(value = "查询条件,放管控民警")
	private List<String> queryList;

	/**
	 * 所属辖区
	 */
	@ApiModelProperty(value = "所属辖区Id")
	private List<String> ssxqIdList;

	/**
	 * 管控级别
	 */
//	private String gkjbx;
	@ApiModelProperty(value = "管控级别")
	private List<String> gkjbxList;

	@ApiModelProperty(value = "民警ID(责任民警ID)")
	private List<String> mjidList;


	/**
	 * 所属派出所
	 */
//	private String sspcs;
	@ApiModelProperty(value = "所属派出所")
	private List<String> sspcsList;

	/**
	 * 人员来源
	 */
	@ApiModelProperty(value = "人员来源")
	private List<String> rylyList;

	/**
	 * 手机号
	 */
	@ApiModelProperty(value = "手机号")
	private String sjh;
	
	/**
	 * 预警处置类别
	 */
	@ApiModelProperty(value = "预警处置类别")
	private List<Integer>   yjczlbList ;
	
	/**
	 * 派出所列表
	 */
	@ApiModelProperty(value = "派出所列表")
	private List<String>   pcsList ;
	
	/**
	 * 民警列表
	 */
	@ApiModelProperty(value = "民警列表")
	private List<String>   mjList ;
	/**
	 * 户籍省
	 */
	@ApiModelProperty(value = "户籍省市县")
    private List<String[]> hjssxList;	
    /**
	 * 户籍省
	 */
    @ApiModelProperty(value = "户籍省")
    private List<String> hjsList;
	/**
	 * 户籍市
	 */
    @ApiModelProperty(value = "户籍市")
    private List<String> hjsSList;
    /**
	 * 户籍县
	 */
    @ApiModelProperty(value = "户籍县")
    private List<String> hjxList;

	/**
	 * 迁出地
	 */
	@ApiModelProperty(value = "迁出地",name = "areaOfOrigin",example = "海南/曲靖")
    private String areaOfOrigin;

	/**
	 * 迁入地
	 */
    @ApiModelProperty(value = "迁入地",name = "placeOfImmigration",example = "云南/昆明")
    private String placeOfImmigration;

	/**
	 * 迁徙标志：省 province; 市 city.
	 */
	@ApiModelProperty(value = "迁徙标志：省 province; 市 city",name = "qxSign",example = "city")
	private String qxSign;
	
	 /**
     * 是否移交市外
     */
    @ApiModelProperty(value = "是否移交市外(1:移交市外;0:默认)")
    private Integer yjsw;
    
    /**
     * 上报状态 0 未上报  1已上报	
     */
    @ApiModelProperty(value = "上报状态 0 未上报  1已上报",name = "upStatus")
    private Integer upStatus;

	/**
	 * 稳控状态
	 */
	@ApiModelProperty(value = "稳控状态")
	private List<String> wkztList;
	
	/**
	 * 所属州市
	 */
	@ApiModelProperty(value = "所属州市")
	private List<String> ynzsList;

	/**
	 * 开始时间
	 */
	@ApiModelProperty(value = "开始时间")
	private String startTime;

	/**
	 * 结束时间
	 */
	@ApiModelProperty("结束时间")
	private String endTime;

	/**
	 * 移交市外查询
	 */
	@ApiModelProperty("移交市外")
	private String qyjsw;

	/**
	 * 范围类型(1:市局,2:分局、派出所)
	 */
	@ApiModelProperty("范围类型(1:市局,2:分局、派出所)")
	private String rangeType;

	/**
	 * 范围值(多个派出所ID用英文逗号分隔)
	 */
	@ApiModelProperty("范围值(多个派出所ID用英文逗号分隔)")
	private String rangeVal;

	/**
	 * 警种(多个警种用英文逗号分隔)
	 */
	@ApiModelProperty("警种(多个警种用英文逗号分隔)")
	private String roleType;
	@ApiModelProperty("当前登陆人ID")
	private String currentUserId;
	@ApiModelProperty("是否关注(1：关注，0：未关注)")
	private Integer isFollow;
	@ApiModelProperty("重点人员id")
	private List<String> personIds;
	
	/**
	 * 根据组织Code查询
	 */
	@ApiModelProperty("根据组织Code查询")
	private String code;

	/**
	 * 默认派出所
	 */
	@ApiModelProperty("默认派出所")
	private String createOrg;

	/**
	 * 布控任务id
	 */
	@ApiModelProperty("布控任务id")
	private String bkrwId;

	/**
	 * 布控任务对应的人员集合
	 */
	@ApiModelProperty("布控任务对应的人员集合")
	private List<String> sfzhList;


}
