package com.mti.dao.qo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
public class TQbptQO extends PageQO{

	/**
	 * 关键字
	 */
	private String name;

}
