/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;

@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel(value = "模型配置", description = "模型配置")
public class TKmTrailSetQO extends PageQO{
	  /**
     * id
     */
    @ApiModelProperty(value = "主键",name = "id")
    private String id;

    /**
     * 线索类型 
     */
    @ApiModelProperty(value = "线索类型：1 聚集 2 伴随",name = "trailType")
    private String trailType;
    /**
     * 名称
     */
    @ApiModelProperty(value = "人员大类（新）",name = "name")
    private String name;
    /**
     * 数据来源
     */
    @ApiModelProperty(value = "数据来源",name = "warningType")
    private String warningType;
    
    /**
        * 距离宽容度
     */
    @ApiModelProperty(value = "距离宽容度",name = "distanceLimit")
    private String distanceLimit;
    
    @ApiModelProperty(value = "时间宽容度",name = "timeLimit")
    private String timeLimit;
    /**
     * 碰撞次数
     */
    @ApiModelProperty(value = "碰撞次数",name = "calculateTimes")
    private String calculateTimes;
    /**
     * 数据计算频率
     */
    @ApiModelProperty(value = "数据计算频率",name = "calculateFrequency")
    private String calculateFrequency;
    /**
     *备注
     */
    @ApiModelProperty(value = "备注",name = "description")
    private String description;
    /**
     * 配置状态：0 暂停 1启动
     */
    @ApiModelProperty(value = "配置状态",name = "isUse")
    private String isUse;
    /**
     * 删除标识：0 未删除 1已删除
     */
    @ApiModelProperty(value = "删除标识",name = "isDelete")
    private String isDelete;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",name = "createTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人",name = "creater")
    private String creater;
 
    /**
     *网格大小（按网格）
     */
    @ApiModelProperty(value = "网格大小",name = "gridFirst")
    private String gridFirst;
    
    /**
           * 网格大小（自定义）
     */
    @ApiModelProperty(value = "网格大小",name = "gridSecond")
    private String gridSecond;
    
	 /**
	     * 大类人员类型
	*/
	@ApiModelProperty(value = "网格大小",name = "personType")
	private String personType;
	
}
