/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel(value = "文件对象", description = "文件对象")
public class AddressQO  {

	/**
	 * 省
	 */
	@ApiModelProperty(value = "省")
	private String provice;

	/**
	 * 市
	 */
	@ApiModelProperty(value = "市")
	private String city;
	/**
	 * 县
	 */
	@ApiModelProperty(value = "县")
	private String region;
	/**
	 * 详址
	 */
	@ApiModelProperty(value = "详址")
	private String 	detailAddress;
}
