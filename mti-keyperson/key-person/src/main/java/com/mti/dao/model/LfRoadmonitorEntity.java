package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_roadmonitor")
public class LfRoadmonitorEntity {

    @TableId("id")
    private String id;

    @TableField("layerid")
    private Integer layerid;

    @TableField("name")
    private String name;

    @TableField("videostdid")
    private String videostdid;

    @TableField("videoselfid")
    private String videoselfid;

    @TableField("channelno")
    private String channelno;

    @TableField("coordinate")
    private String coordinate;

    @TableField("videotype")
    private String videotype;

    @TableField("status")
    private String status;

    @TableField("orgid")
    private String orgid;

    @TableField("orgname")
    private String orgname;

    @TableField("areacode")
    private String areacode;

    @TableField("areaname")
    private String areaname;

    @TableField("applyuser")
    private String applyuser;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("applytime")
    private String applytime;

    @TableField("systemid")
    private String systemid;

}
