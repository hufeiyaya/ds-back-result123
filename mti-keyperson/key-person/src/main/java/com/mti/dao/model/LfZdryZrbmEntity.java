/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_zdry_zrbm")
@ApiModel(value = "重点人员责任部门对象", description = "重点人员责任部门对象")
public class LfZdryZrbmEntity {

    /**
     * 序号
     */
    @ApiModelProperty(value = "序号")
    @TableField("id")
    private String id;

    /**
     * 人员大类编码
     */
    @ApiModelProperty(value = "人员大类编码")
    @TableField("rydl_code")
    private String rydlCode;
    
    /**
     * 人员大类
     */
    @ApiModelProperty(value = "人员大类")
    @TableField("rylb")
    private String rylb;

    /**
     * 责任部门
     */
    @ApiModelProperty(value = "责任部门")
    @TableField("zrbm")
    private String zrbm;

    /**
     * 联系人
     */
    @ApiModelProperty(value = "联系人")
    @TableField("lxr")
    private String lxr;

    /**
     * 联系电话
     */
    @ApiModelProperty(value = "联系电话")
    @TableField("lxdh")
    private String lxdh;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    @TableField("bz")
    private String bz;


}
