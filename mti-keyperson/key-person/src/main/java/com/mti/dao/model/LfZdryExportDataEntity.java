package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.security.Key;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.component.snowflake.KeyWorker;
import com.mti.jwt.AccountBo;
import com.mti.utils.DateUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author duchao
 * @since 2020-04-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_lf_zdry_export_data")
public class LfZdryExportDataEntity extends Model<LfZdryExportDataEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 流水号
     */
    @TableField("id")
    private String id;

    /**
     * 序号
     */
    @TableField("xh")
    private String xh;

    /**
     * 姓名
     */
    @TableField("xm")
    private String xm;

    /**
     * 性别
     */
    @TableField("xb")
    private String xb;

    /**
     * 民族
     */
    @TableField("mz")
    private String mz;

    /**
     * 身份证号
     */
    @TableField("sfzh")
    private String sfzh;

    /**
     * 户籍地
     */
    @TableField("hjd")
    private String hjd;

    /**
     * 实际居住地址
     */
    @TableField("xzz")
    private String xzz;

    /**
     * 手机号
     */
    @TableField("sjh")
    private String sjh;

    /**
     * 细类
     */
    @TableField("xl")
    private String xl;


    /**
     * 责任民警
     */
    @TableField("gkmj")
    private String gkmj;

    /**
     * 民警联系电话
     */
    @TableField("mjlxdh")
    private String mjlxdh;


    /**
     * 备注
     */
    @TableField("bz")
    private String bz;

    /**
     * 人员类别（新）
     */
    @TableField("rylbx")
    private String rylbx;



    /**
     * 所属辖区--对应昆明管控责任分局
     */
    @TableField("ssxq")
    private String ssxq;

    /**
     * 管控级别（新）  对应风险等级的字典数据
     */
    @TableField("gkjbx")
    private String gkjbx;


    /**
     * 创建时间
     */
    @TableField("create_timex")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTimex;

    /**
     * 更新时间
     */
    @TableField("update_timex")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTimex;


    /**
     * 责任派出所
     */
    @TableField("sspcs")
    private String sspcs;


    /**
     * 微信号码
     */
    @TableField("wx")
    private String wx;

    /**
     * qq号码
     */
    @TableField("qq")
    private String qq;

    /**
     * 名下车牌号
     */
    @TableField("mxcph")
    private String mxcph;

    /**
     * 实际使用车辆号牌
     */
    @TableField("sjsyclhp")
    private String sjsyclhp;

    /**
     * 字典表key，对应人员属性类型
     */
    @TableField("rysx")
    private String rysx;

    /**
     * 预警处置类别，字典表key
     */
    @TableField("yjczlb")
    private String yjczlb;

    /**
     * 户籍省
     */
    @TableField("hjs")
    private String hjs;

    /**
     * 户籍市
     */
    @TableField("hjss")
    private String hjss;

    /**
     * 户籍县
     */
    @TableField("hjx")
    private String hjx;

    /**
     * 实际住地省

     */
    @TableField("xzzs")
    private String xzzs;

    /**
     * 实际住地市
     */
    @TableField("xzzss")
    private String xzzss;

    /**
     * 实际住地县
     */
    @TableField("xzzx")
    private String xzzx;

    /**
     * 户籍所在地经度
     */
    @TableField("hjd_x")
    private String hjdX;

    /**
     * 户籍所在地维度
     */
    @TableField("hjd_y")
    private String hjdY;

    /**
     * 稳控状态
     */
    @TableField("wkzt")
    private String wkzt;

    /**
     * 导入时的版本，便于事务回滚
     */
    @TableField("version")
    private String version;

    /**
     * 0  未处理；1 已处理。
     */
    @TableField("han_sign")
    private Integer hanSign;

    @TableField("appeal")
    private String appeal;

    @TableField("sszsmc")
    private String sszsmc;

    /**
     * 行号
     */
    @TableField(exist = false)
    private Integer rowIndex;

    /**
     * 出入证号码
     */
    @TableField("pass_number")
    private String passNumber;


    @Override
    protected Serializable pkVal() {
        return null;
    }


    public LfZdryBaseEntity transToZdryBase(){
        LfZdryBaseEntity lfZdryBase = new LfZdryBaseEntity();
        lfZdryBase.setXm(this.getXm());
        lfZdryBase.setXb(this.getXb());
        lfZdryBase.setMz(this.getMz());
        lfZdryBase.setSfzh(this.getSfzh());
        lfZdryBase.setHjd(this.getHjd());
        lfZdryBase.setSjh(this.getSjh());
        lfZdryBase.setGkmj(this.getGkmj());
        lfZdryBase.setMjlxdh(this.getMjlxdh());
        lfZdryBase.setBz(this.getBz());
        lfZdryBase.setSsxq(this.getSsxq());
        lfZdryBase.setSspcs(this.getSspcs());
        lfZdryBase.setHjs(this.getHjs());
        lfZdryBase.setHjss(this.getHjss());
        lfZdryBase.setHjx(this.getHjx());
        lfZdryBase.setHjdX(this.getHjdX());
        lfZdryBase.setHjdY(this.getHjdY());
        lfZdryBase.setGkjb(this.getGkjbx());
        lfZdryBase.setWkzt(this.getWkzt());
        lfZdryBase.setRysxName(this.getRysx());
        lfZdryBase.setYjczlbName(this.getYjczlb());
        lfZdryBase.setXzz(this.getXzz());
        lfZdryBase.setXzzs(this.getXzzs());
        lfZdryBase.setXzzss(this.getXzzss());
        lfZdryBase.setXzzx(this.getXzzx());

        lfZdryBase.setAppeal(this.getAppeal());
        lfZdryBase.setSszsmc(this.getSszsmc());
        return lfZdryBase;
    }

    public KmZdryTypeEntity transToKmZdryType(String zdryId){
        KmZdryTypeEntity zt = new KmZdryTypeEntity();
        zt.setZdryId(zdryId);
        zt.setRylb(this.getRylbx());
        zt.setRylbCode(this.getRylbx());
        zt.setXl(this.getXl());
        return zt;
    }

    public List<KmZdryAssistEntity> transToKmZdryAssist(String zdryId){
        List<KmZdryAssistEntity> result = new ArrayList<>();
        if(null != this.getSjh() && this.getSjh().split(",").length>0){
            for(String sjh : this.getSjh().split(",")){
                if(sjh.length()>0){
                    KmZdryAssistEntity kmZdryAssist= new KmZdryAssistEntity();
                    kmZdryAssist.setZdryId(zdryId);
                    kmZdryAssist.setInfoType(1);
                    kmZdryAssist.setInfoValue(sjh);
                    result.add(kmZdryAssist);
                }
            }
        }
        if(null != this.getPassNumber() && this.getPassNumber().split(",").length>0){
            for(String passNumber : this.getPassNumber().split(",")){
                if(passNumber.length()>0){
                    KmZdryAssistEntity kmZdryAssist= new KmZdryAssistEntity();
                    kmZdryAssist.setZdryId(zdryId);
                    kmZdryAssist.setInfoType(6);
                    kmZdryAssist.setInfoValue(passNumber);
                    result.add(kmZdryAssist);
                }
            }
        }
        if(null != this.getWx() && this.getWx().length()>0){
            KmZdryAssistEntity wx= new KmZdryAssistEntity();
            wx.setZdryId(zdryId);
            wx.setInfoType(2);
            wx.setInfoValue(this.getWx());
            result.add(wx);
        }
        if(null != this.getQq() && this.getQq().length()>0){
            KmZdryAssistEntity qq= new KmZdryAssistEntity();
            qq.setZdryId(zdryId);
            qq.setInfoType(3);
            qq.setInfoValue(this.getQq());
            result.add(qq);
        }
        if(null != this.getMxcph() && this.getMxcph().length()>0){
            KmZdryAssistEntity mxcph= new KmZdryAssistEntity();
            mxcph.setZdryId(zdryId);
            mxcph.setInfoType(4);
            mxcph.setInfoValue(this.getMxcph());
            result.add(mxcph);
        }
        if(null != this.getSjsyclhp() && this.getSjsyclhp().length()>0){
            KmZdryAssistEntity sjsyclhp= new KmZdryAssistEntity();
            sjsyclhp.setZdryId(zdryId);
            sjsyclhp.setInfoType(5);
            sjsyclhp.setInfoValue(this.getSjsyclhp());
            result.add(sjsyclhp);
        }
        return result;
    }

    public KmZdryHomeEntity transToKmZdryHome(String zdryId){
        KmZdryHomeEntity xzz = new KmZdryHomeEntity();
        xzz.setZdryId(zdryId);
        if(null != this.getXzzs() && this.getXzzs().length()>0) xzz.setXzzs(this.getXzzs());
        if(null != this.getXzzss() && this.getXzzss().length()>0) xzz.setXzzss(this.getXzzss());
        if(null != this.getXzzx() && this.getXzzx().length()>0) xzz.setXzzx(this.getXzzx());
        if(null != this.getXzz() && this.getXzz().length()>0) xzz.setXzz(this.getXzz());
        return xzz;
    }


    public TKmProEntity transToKmPro(String zdryId) {
        TKmProEntity pro = new TKmProEntity();
        pro.setId(String.valueOf(KeyWorker.nextId()));
        pro.setProRightTime(new Date());
        pro.setInsertTime(new Date());
        pro.setDeleted(1);
        pro.setRefId(zdryId);
        pro.setAppeal(this.getAppeal());
        return pro;
    }

    public LfZdryHistoryLogEntity transToLfZdryHistoryLog(AccountBo bo, String id) {
        LfZdryHistoryLogEntity log = new LfZdryHistoryLogEntity();
        log.setId(String.valueOf(KeyWorker.nextId()));
        log.setCreator(bo.getPersonBo().getName());
        log.setCreateTime(new Date());
        log.setSpId(id);
        log.setOperation("维权");
        log.setOperateDept(bo.getOrganizationBo().getName());
        log.setAddition("数据导入");
        log.setType(5);
        log.setInsertTime(new Date());
        log.setAppeal(this.getAppeal());
        log.setFunctionModule("重点人员模块");
        return log;
    }
}
