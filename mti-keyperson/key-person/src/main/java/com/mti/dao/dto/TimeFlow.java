package com.mti.dao.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zhangmingxin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeFlow implements Serializable {
    private static final long serialVersionUID = 4011754955888159372L;
    /**
     * 布控任务ID
     */
    private String taskId;
    /**
     * 开始时间
     */
    private String startDateTime;
    /**
     * 结束时间
     */
    private String endDateTime;
    /**
     * 是否有效数据
     */
    private boolean available;
}
