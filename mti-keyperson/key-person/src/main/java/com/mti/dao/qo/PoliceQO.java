/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel(value = "文件对象", description = "文件对象")
public class PoliceQO  {

	/**
	 * 大类
	 */
	@ApiModelProperty(value = "大类")
	private String rydl;

	/**
	 * 小类
	 */
	@ApiModelProperty(value = "小类")
	private String ryxl;
}
