package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_lf_ztry")
public class LfZtryEntity {
    @TableId
    private String ztrybh;
    @TableField("xm")
    private String xm;
    @TableField("bmch")
    private String bmch;
    @TableField("xbdm")
    private String xbdm;
    @TableField("mzdm")
    private String mzdm;
    @TableField("csrq")
    private String csrq;
    @TableField("hjdz_xzqhdm")
    private String hjdzXzqhdm;
    @TableField("hjdz_dzmc")
    private String hjdzDzmc;
    @TableField("xzz_xzqhdm")
    private String xzzXzqhdm;
    @TableField("xzz_dzmc")
    private String xzzDzmc;
    @TableField("sfzh")
    private String sfzh;
    @TableField("cyzj")
    private String cyzj;
    @TableField("sgxx")
    private String sgxx;
    @TableField("sgsx")
    private String sgsx;
    @TableField("asjbh")
    private String asjbh;
    @TableField("tpsj")
    private String tpsj;
    @TableField("tpfx_jyqk")
    private String tpfxJyqk;
    @TableField("tjjbdm")
    private String tjjbdm;
    @TableField("ztjj")
    private String ztjj;
    @TableField("ztrylxdm")
    private String ztrylxdm;
    @Transient
    @TableField(exist = false)
    private String ztrylx;
    @TableField("ladw_gajgjgdm")
    private String ladwGajgjgdm;
    @TableField("zbr_xm")
    private String zbrXm;
    @TableField("zbr_lxdh")
    private String zbrLxdh;
    @TableField("dm_rybh")
    private String dmRybh;
    @TableField("gay_rksj")
    private String gayRksj;
    @TableField("gay_gxsj")
    private String gayGxsj;
    @TableField("is_problem")
    private String isProblem;
    @TableField("dmq1001")
    private String dmq1001;
    @TableField("py_xm")
    private String pyXm;
    @TableField("gxrq")
    private String gxrq;
    @TableField("flag")
    private String flag;
    @TableField("bdrksj")
    private String bdrksj;


}
