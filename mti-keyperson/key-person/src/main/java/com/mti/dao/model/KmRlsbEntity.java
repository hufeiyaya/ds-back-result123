package com.mti.dao.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.component.snowflake.KeyWorker;
import com.mti.utils.DateUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 人脸识别
 * </p>
 *
 * @author duchao
 * @since 2020-06-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_rlsb")
@NoArgsConstructor
@AllArgsConstructor
public class KmRlsbEntity extends Model<KmRlsbEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 人脸预警id
     */
    @TableId("notification_id")
    private String NotificationID;

    /**
     * 姓名
     */
    @TableField("name")
    private String Name;

    /**
     * 身份证号
     */
    @TableField("id_number")
    private String IdNumber;

    /**
     * 抓拍设备名称
     */
    @TableField("device_name")
    private String DeviceName;

    /**
     * 抓拍时间(yyyy-MM-dd HH:mm:ss)
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField("shot_time")
    private Date ShotTime;

    /**
     * 经度
     */
    @TableField("lng")
    private String Longitude;

    /**
     * 维度
     */
    @TableField("lat")
    private String Latitude;

    /**
     * 告警时间(yyyy-MM-dd HH:mm:ss)
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField("trigger_time")
    private Date TriggerTime;

    /**
     * 嫌疑人抓拍人脸图
     */
    @TableField("face_url")
    private String FaceUrl;

    /**
     * 嫌疑人抓拍场景图
     */
    @TableField("person_url")
    private String PersonUrl;

    /**
     * 嫌疑人布控库人脸图
     */
    @TableField("target_url")
    private String TargetUrl;

    /**
     * 管理单位
     */
    @TableField("department")
    private String Department;


    @Override
    protected Serializable pkVal() {
        return this.NotificationID;
    }

    public LfZtryyjEntity convertZdryYj() {
        LfZtryyjEntity y = new LfZtryyjEntity();
        y.setWybs(String.valueOf(KeyWorker.nextId()));
        y.setYjlx("rlsb");
        y.setRyxm(this.Name);
        y.setYjbs(this.IdNumber);
        y.setYjsj(DateUtils.dateToString(this.getTriggerTime(),"yyyy-MM-dd HH:mm:ss"));
        y.setTbsj(DateUtils.dateToString(new Date(),"yyyy-MM-dd HH:mm:ss"));
        y.setX(this.Longitude);
        y.setY(this.Latitude);
        y.setGlId(this.NotificationID);
        y.setDeviceName(this.DeviceName);
        y.setShotTime(this.ShotTime);
        y.setFaceUrl(this.FaceUrl);
        y.setPersonUrl(this.PersonUrl);
        y.setTargetUrl(this.TargetUrl);
        y.setDepartment(this.Department);
        y.setYjdd(this.DeviceName);
        return y;
    }
}
