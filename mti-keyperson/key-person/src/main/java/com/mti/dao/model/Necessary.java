package com.mti.dao.model;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/26
 * @change
 * @describe 此接口没有任何方法,只是区分是否需要自动进行相关基础字段赋值的标志位
 * 如果相应实体需要在保存时需要自动设置id则实现改接口即可
 **/
public interface Necessary {
}
