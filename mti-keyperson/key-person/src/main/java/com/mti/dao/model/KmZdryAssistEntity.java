package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 重点人员-辅助信息表
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_zdry_assist")
@AllArgsConstructor
@NoArgsConstructor
public class KmZdryAssistEntity extends Model<KmZdryAssistEntity> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    @ApiModelProperty(value = "主键",name = "id")
    private String id;

    /**
     * 重点人员ID
     */
    @TableField("zdry_id")
    @ApiModelProperty(value = "重点人员ID",name = "zdryId")
    private String zdryId;

    /**
     * 信息类型：1电话号码，2微信号码，3qq号码，4名下车辆号牌，5实际车辆号牌
     */
    @TableField("info_type")
    @ApiModelProperty(value = "信息类型：1电话号码，2微信号码，3qq号码，4名下车辆号牌，5实际车辆号牌,6出入境证件号",
                        name = "infoType")
    private Integer infoType;

    /**
     * 信息值
     */
    @TableField("info_value")
    @ApiModelProperty(value = "信息值",name = "infoValue")
    private String infoValue;

    /**
     * 信息序号
     */
    @TableField("index")
    @ApiModelProperty(value = "信息序号",name = "index")
    private Integer index;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
