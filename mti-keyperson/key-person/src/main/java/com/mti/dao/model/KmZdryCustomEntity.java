package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 重点人员-自定义字段表
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_zdry_custom")
public class KmZdryCustomEntity extends Model<KmZdryCustomEntity> {

    private static final long serialVersionUID = 1L;
    @TableId("id")
    @ApiModelProperty(value = "主键",name = "id")
    private String id;

    /**
     * 重点人员ID
     */
    @TableField("zdry_id")
    @ApiModelProperty(value = "重点人员ID",name = "zdryId")
    private String zdryId;

    /**
     * 自定义键
     */
    @TableField("custom_key")
    @ApiModelProperty(value = "自定义键",name = "customKey")
    private String customKey;

    /**
     * 自定义值
     */
    @TableField("custom_value")
    @ApiModelProperty(value = "自定义值",name = "customValue")
    private String customValue;

    /**
     * 序号
     */
    @TableField("index")
    @ApiModelProperty(value = "序号",name = "index")
    private Integer index;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
