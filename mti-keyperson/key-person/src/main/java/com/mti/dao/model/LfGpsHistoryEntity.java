package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_gps_history")
public class LfGpsHistoryEntity {

    @TableId("mobileid")
    private String mobileid;

    @TableField("gpsid")
    private String gpsid;

    @TableField("x")
    private String x;

    @TableField("y")
    private String y;

    @TableField("time")
    private String time;

    @TableField("speed")
    private String speed;

    @TableField("dir")
    private String dir;

}
