package com.mti.dao.model;

/**
 * @Classname TKmDailyCheckEntity
 * @Description TODO
 * @Date 2020/2/27 16:29
 * @Created by duchaof
 * @Version 1.0
 */

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.dao.dto.ZdryDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("t_km_daily_check")
@ApiModel(value = "日常核查",description = "日常核查")
public class TKmDailyCheckEntity {
    /**
     * 主键
     */
    @TableId("id")
    @ApiModelProperty(value = "id",name = "id")
    private String id;

    /**
     * 核查结果
     */
    @TableField("check_result")
    @ApiModelProperty(value = "核查结果",name = "checkResult",example = "再次在控")
    private String checkResult;

    /**
     * 核查内容
     */
    @TableField("check_content")
    @ApiModelProperty(value = "核查内容",name = "checkContent",example = "进行核查")
    private String checkContent;

    /**
     * 核查时间
     */
    @TableField("check_time")
    @ApiModelProperty(value = "核查结果",name = "checkTime",example = "2020-02-27")
    private String checkTime;

    /**
     * 插入时间
     */
    @TableField("insert_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(hidden = true)
    private Date insertTime;

    /**
     * 删除标志
     */
    @TableField("deleted")
    @TableLogic
    @ApiModelProperty(hidden = true)
    private Integer deleted;

    /**
     * 关联Id
     */
    @ApiModelProperty(value = "关联Id,重点人员Id",name = "refId",example = "401")
    @TableField("ref_id")
    private String refId;

    /**
     * 图片路径
     */
    @ApiModelProperty(value = "图片路径",name = "fileUrl")
    @TableField("file_url")
    private String fileUrl;

    @TableField(exist = false)
    @ApiModelProperty(hidden = true)
    private ZdryDto zdryDto;

}
