package com.mti.dao.dto;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.mti.dao.model.LfZdryBaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 重点人员表
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ZdryDto extends LfZdryBaseEntity {

    private static final long serialVersionUID = 1L;

	/**
	 * 人员大类
 	 */
	private String rylbx;

	/**
	 * 人员小类
	 */
	private String xl;

	/**
	 * qq号码
	 */
	private String qq;

	/**
	 * wx
	 */
	private String wx;

	/**
	 * 名下车牌号
	 */
	private String mxcph;

	/**
	 * 实际使用车牌号
	 */
	private String sjsyclhp;

	/**
	 * 人员属性名称
	 */
	private String rysxName;

	/**
	 * 人员预警处置类别名称
	 */
	private String yjczlbName;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
