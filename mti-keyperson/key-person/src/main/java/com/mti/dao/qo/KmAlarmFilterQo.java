/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.qo;

import com.mti.dao.kmmodel.GisModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author hf
 * @Title:
 * @Description: TODO (DS预警推送过滤)
 * @Param
 * @return
 * @Date 2020/11/24 19:39
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class KmAlarmFilterQo extends PageQO {

    @ApiModelProperty(value = "预警方式")
    private List<String> yjlxList;
    @ApiModelProperty(value = "预警类型控制(只包含手机预警和人脸识别)")
    private List<String> subYjlxList;
    @ApiModelProperty(value = "稳控状态")
    private List<String> wkztList;
    @ApiModelProperty(value = "预警处置类别")
    private List<String> yjczlbList;
    @ApiModelProperty(value = "风险等级")
    private List<String> fxdjList;
    @ApiModelProperty(value = "人员类型-大类")
    private List<String> rylxList;
    @ApiModelProperty(value = "人员类型-小类")
    private List<String> sonRylxList;
    @ApiModelProperty(value = "对应警种")
    private List<String> zrbmList;
    @ApiModelProperty(value = "管控责任州市")
    private List<String> sszsmcList;
    @ApiModelProperty(value = "管控责任分局")
    private List<String> ssxqList;
    @ApiModelProperty(value = "位置数据")
    private List<GisModel> gisModels;
    @ApiModelProperty(value = "开始时间")
    private String startTime;
    @ApiModelProperty(value = "结束时间")
    private String endTime;


}
