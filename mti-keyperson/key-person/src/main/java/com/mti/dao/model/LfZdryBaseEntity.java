package com.mti.dao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.mti.utils.ZdryYjUtilBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 重点人员表
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_lf_zdry_base")
public class LfZdryBaseEntity extends Model<LfZdryBaseEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    @ApiModelProperty(value = "主键",name = "id")
    private String id;

    /**
     * 姓名
     */
    @TableField("xm")
    @ApiModelProperty(value = "姓名",name="xm")
    private String xm;

    /**
     * 性别
     */
    @TableField("xb")
    @ApiModelProperty(value = "性别",name = "xb")
    private String xb;

    /**
     * 民族
     */
    @TableField("mz")
    @ApiModelProperty(value = "民族",name = "mz")
    private String mz;

    /**
     * 身份证号
     */
    @TableField("sfzh")
    @ApiModelProperty(value = "身份证号",name = "sfzh")
    private String sfzh;

    /**
     * 户籍地
     */
    @TableField("hjd")
    @ApiModelProperty(value = "户籍地",name = "hjd")
    private String hjd;

    /**
     * 手机号
     */
    @TableField("sjh")
    @ApiModelProperty(value = "手机号",name = "sjh")
    private String sjh;

    /**
     * 是否落实管控措施
     */
    @TableField("sflsgkcs")
    @ApiModelProperty(value = "是否落实管控措施",name = "sflsgkcs")
    private String sflsgkcs;

    /**
     * 具体管控措施或未控制原因
     */
    @TableField("jtgkcshwkzyy")
    @ApiModelProperty(value = "具体管控措施或未控制原因",name = "jtgkcshwkzyy")
    private String jtgkcshwkzyy;

    /**
     * 责任单位
     */
    @TableField("zrdw")
    @ApiModelProperty(value = "责任单位",name = "zrdw")
    private String zrdw;

    /**
     * 责任民警
     */
    @TableField(value = "gkmj", strategy = FieldStrategy.IGNORED)
    @ApiModelProperty(value = "责任民警",name = "gkmj")
    private String gkmj;

    /**
     * 民警联系电话
     */
    @TableField(value = "mjlxdh", strategy = FieldStrategy.IGNORED)
    @ApiModelProperty(value = "民警联系电话",name = "mjlxdh")
    private String mjlxdh;

    /**
     * update_time
     */
    @TableField("update_time")
    @ApiModelProperty(value = "更新时间",name = "updateTime")
    private String updateTime;

    /**
     * 备注
     */
    @TableField("bz")
    @ApiModelProperty(value = "备注",name = "bz")
    private String bz;

    /**
     * job_id
     */
    @TableField("job_id")
    @ApiModelProperty(value = "工号",name = "jobId")
    private String jobId;

    /**
     * 是否在控
     */
    @TableField("sfzk")
    @ApiModelProperty(value = "是否在控",name = "sfzk")
    private String sfzk;

    /**
     * 责任部门--对应警种
     */
    @TableField("zrbm")
    @ApiModelProperty(value = "任部门--对应警种",name = "zrbm")
    private String zrbm;

    /**
     * 所属辖区--对应昆明管控责任分局
     */
    @TableField("ssxq")
    @ApiModelProperty(value = "所属辖区--对应昆明管控责任分局",name = "ssxq")
    private String ssxq;

    /**
     * 是否布控
     */
    @TableField("sfbk")
    @ApiModelProperty(value = "是否布控",name = "sfbk")
    private String sfbk;

    /**
     * 创建时间
     */
    @TableField("create_timex")
    @ApiModelProperty(value = "创建时间",name = "createTimex")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTimex;

    /**
     * 更新时间
     */
    @TableField("update_timex")
    @ApiModelProperty(value = "更新时间",name = "updateTimex")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTimex;



    /**
     * 头像
     */
    @TableField("tx")
    @ApiModelProperty(value = "头像",name = "tx")
    private String tx;

    /**
     * 责任派出所
     */
    @TableField(value = "sspcs",strategy = FieldStrategy.IGNORED)
    @ApiModelProperty(value = "责任派出所",name = "sspcs")
    private String sspcs;

    /**
     * 派出所ID
     */
    @TableField(value = "sspcs_id",strategy = FieldStrategy.IGNORED)
    @ApiModelProperty(value = "派出所ID",name = "sspcsId")
    private String sspcsId;

    /**
     * 民警ID
     */
    @TableField(value = "mjid", strategy = FieldStrategy.IGNORED)
    @ApiModelProperty(value = "民警ID",name = "mjid")
    private String mjid;

    /**
     * 是否自排(0:默认，1：自排)
     */
    @TableField("sfzp")
    @ApiModelProperty(value = "是否自排(0:默认，1：自排)",name = "sfzp")
    private String sfzp;

    /**
     * 所属辖区ID
     */
    @TableField("ssxq_id")
    @ApiModelProperty(value = "所属辖区ID",name = "ssxqId")
    private String ssxqId;

    /**
     * 重点人员审批状态(0：待分局审批，1：待警种审核，2：已驳回，4：已完成)
     */
    @TableField("approve_status")
    @ApiModelProperty(value = "重点人员审批状态(0：待分局审批，1：待警种审核，2：已驳回，4：已完成)",
                      name ="approveStatus")
    private String approveStatus;

    /**
     * 户籍省
     */
    @TableField("hjs")
    @ApiModelProperty(value = "户籍省",name = "hjs")
    private String hjs;

    /**
     * 户籍市
     */
    @TableField("hjss")
    @ApiModelProperty(value = "户籍市",name = "hjss")
    private String hjss;

    /**
     * 户籍县
     */
    @TableField("hjx")
    @ApiModelProperty(value = "户籍县",name = "hjx")
    private String hjx;

    /**
     * 户籍所在地经度
     */
    @TableField("hjd_x")
    @ApiModelProperty(value = "户籍所在地经度",name = "hjdX")
    private String hjdX;

    /**
     * 户籍所在地维度
     */
    @TableField("hjd_y")
    @ApiModelProperty(value = "户籍所在地维度",name = "hjdY")
    private String hjdY;
    
    /**
     * 实际住地省

     */
    @TableField("xzzs")
    @ApiModelProperty(value = "实际住地省",name = "xzzs")
    private String xzzs;

    /**
     * 实际住地市
     */
    @TableField("xzzss")
    @ApiModelProperty(value = "实际住地市",name = "xzzss")
    private String xzzss;

    /**
     * 实际住地县
     */
    @TableField("xzzx")
    @ApiModelProperty(value = "实际住地县",name = "xzzx")
    private String xzzx;

    /**
     * 实际居住地址
     */
    @TableField("xzz")
    @ApiModelProperty(value = "实际居住地址",name = "xzz")
    private String xzz;
    
    /**
     * 实际居住地址经度
     */
    @TableField("x")
    @ApiModelProperty(value = "实际居住地址经度",name = "x")
    private String x;

    /**
     * 实际居住地址纬度
     */
    @TableField("y")
    @ApiModelProperty(value = "实际居住地址纬度",name = "y")
    private String y;
    
    /**
     * 管控级别 字典表的key
     */
    @TableField("gkjb")
    @ApiModelProperty(value = "管控级别 字典表的key",name = "gkjb")
    private String gkjb;

    /**
     * 管控级别（新）  对应风险等级的字典数据
     */

    @TableField("gkjb_code")
    @ApiModelProperty(value = "管控级别（新）  对应风险等级的字典数据",name = "gkjbCode")
    private String gkjbCode;


    /**
     * 字典表key，对应人员属性类型
     */
    @TableField("rysx")
    @ApiModelProperty(value = "字典表key，对应人员属性类型",name = "rysx")
    private Integer rysx;

    /**
     * 预警处置类别，字典表key
     */
    @TableField("yjczlb")
    @ApiModelProperty(value = "预警处置类别，字典表key",name = "yjczlb")
    private Integer yjczlb;

    
    /**
     * 稳控状态,字典表key
     */
    @TableField("wkzt")
    @ApiModelProperty(value = "稳控状态key",name = "wkzt")
    private String wkzt;

    /**
     * 流程类型(0：数据导入、基本信息变更，1:新增，2:风险等级变更,3:移交至分局，30：移交分局驳回，31：分局指定派出所，321：市局警种驳回，322市局警种指定分局，323市局警种上报
     */
    @TableField("process_type")
    @ApiModelProperty(value = "流程类型",name = "processType")
    private Integer processType;

    /**
     * 上报状态 0 未上报  1已上报	
     */
    @TableField("up_status")
    @ApiModelProperty(value = "上报状态 0 未上报  1已上报",name = "upStatus")
    private Integer upStatus;

    /**
     * 提交人：account_id账户ID即警号
     */
    @TableField("submitter")
    @ApiModelProperty(value = "提交人：account_id账户ID即警号",name = "submitter")
    private String submitter;

    /**
     * 最后一次操作： 驳回， 通过
     */
    @TableField("last_operate")
    @ApiModelProperty(value = "后一次操作： 驳回， 通过",name = "lastOperate")
    private String lastOperate;

    /**
     * 提交人角色Id
     */
    @TableField("submitter_role_id")
    @ApiModelProperty(value = "提交人角色Id",name = "submitterRoleId")
    private String submitterRoleId;
    /**
     * 是否移交市外
     */
    @ApiModelProperty(value = "是否移交市外(1:移交市外;0:默认)")
    @TableField(exist = false)
    private Integer yjsw;
    
    /**
   	 * 人员类型
   	 */
      @ApiModelProperty(value = "人员类型")
    @TableField(exist = false)
    private List<KmZdryTypeEntity> typeList;
    /**
   	 * 辅助信息
   	 */
      @ApiModelProperty(value = "辅助信息")
    @TableField(exist = false)
    private List<KmZdryAssistEntity> assistList;
       /**
	 * 居住地
	 */
         @ApiModelProperty(value = "居住地")
    @TableField(exist = false)
	private List<KmZdryHomeEntity> jzdList;
	  /**
	 * 自定义字段
	 */
		@ApiModelProperty(value = "自定义字段")
	@TableField(exist = false)
	private List<KmZdryCustomEntity> customList;

	 	@ApiModelProperty(value = "附件地址")
	@TableField(exist = false)
  	private List<TTaskFileEntity> fileList;

	
	@TableField("appeal")
    @ApiModelProperty(value = "最后一次诉求",name = "上访的问题")
    private String appeal;
	
	@TableField("receive_flag")
    @ApiModelProperty(value = "接收标记(1:未接收;0:默认已接收)",name = "接收标记")
    private Integer receiveFlag;
	
	@TableField("sszsmc")
    @ApiModelProperty(value = "所属州市名称（地州和地级市）",name = "所属州市名称（地州和地级市）")
    private String sszsmc;

	@TableField("yjbz")
    @ApiModelProperty(value = "移交备注",name = "移交备注")
	private String yjbz;

	@TableField(exist = false)
    @ApiModelProperty(value = "日志记录人",name = "日志记录人")
	private String modifier;

 	@ApiModelProperty(value = "轨迹信息")
	@TableField(exist = false)
  	private List<ZdryYjUtilBean> gjList;
	
 	@ApiModelProperty(value = "审批状态")
    @TableField(exist = false)
    private String reviewState;
 	
 	@ApiModelProperty(value = "审批意见/提交理由")
    @TableField(exist = false)
    private String approvalOpinion;

    @ApiModelProperty(value = "人员属性名称")
    @TableField(exist = false)
 	private String rysxName;

    @ApiModelProperty(value = "预警处置类别名称")
    @TableField(exist = false)
    private String yjczlbName;
 	
    @ApiModelProperty(value = "重新提交标志 0：第一次提交 ，1:重新提交")
    @TableField(exist = false)
    private Integer resubmitFlag;

    @TableField(exist = false)
    @ApiModelProperty(value = "关联ID")
    private String rid;
    @TableField(exist = false)
    @ApiModelProperty(value = "是否关注(1：关注，0：未关注)")
    private Integer isFollow;
    @TableField(exist = false)
    @ApiModelProperty(value = "是否已经删除(1：已删除)")
    private Integer isDelete;

    /**
     * 默认派出所
     */
    @TableField("create_org")
    @ApiModelProperty(value = "默认派出所",name="createOrg")
    private String createOrg;

    /**
     * 默认派出所ID
     */
    @TableField("create_org_id")
    @ApiModelProperty(value = "默认派出所",name="createOrgId")
    private String createOrgId;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
