package com.mti.dao.vo;

import lombok.Data;


/**
 * @Classname KmTrailVo
 * @Description TODO
 * @Date 2020/4/9 23:22
 * @Created by duchaof
 * @Version 1.0
 */
@Data
public class KmTrailVo {
    private String name;
}
