/*
 * Copyright (C) 2019 @MTI Ltd.
 *
 */
package com.mti.dao.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel(value = "警种查询大类小类对象", description = "警种查询大类小类对象")
public class TypeQO  {

	/**
	 * 字典类型
	 */
	@ApiModelProperty(value = "字典类型")
	private String type;
	
	/**
	 * 是否首次
	 */
	@ApiModelProperty(value = "是否首次(1首次  2多次)")
	private String first;
}
