package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_ryyjsj")
public class LfRyyjsjEntity {

    /**
     * 预警类型
     */
    @TableId("yjlx")
    private String yjlx;

    /**
     * 人员姓名
     */
    @TableField("ryxm")
    private String ryxm;

    /**
     * 预警标识，身份证号
     */
    @TableField("yjbs")
    private String yjbs;

    /**
     * 手机标识
     */
    @TableField("sjbs")
    private String sjbs;

    /**
     * 预警时间
     */
    @TableField("yjsj")
    private String yjsj;

    /**
     * 预警地点
     */
    @TableField("yjdd")
    private String yjdd;

    /**
     * 地点名称
     */
    @TableField("ddmc")
    private String ddmc;

    /**
     * 更新时间
     */
    @TableField("gxsj")
    private String gxsj;


    /**
     * 经度
     */
    @TableField("x")
    private String x;

    /**
     * 纬度
     */
    @TableField("y")
    private String y;

    /**
     * 唯一标识
     */
    @TableField("wybs")
    private String wybs;

    @TableField("xsd")
    private String xsd;

    @TableField("rlzp")
    private String rlzp;

    @TableField("zjzp")
    private String zjzp;

    @TableField("zjzp2")
    private String zjzp2;

    @TableField("zjzp3")
    private String zjzp3;

    /**
     * 盘查民警ID
     */
    @TableField("pcmjid")
    private String pcmjid;

    /**
     * 盘查民警姓名
     */
    @TableField("pcmjname")
    private String pcmjname;

    /**
     * 盘查民警电话
     */
    @TableField("pcmjdh")
    private String pcmjdh;

    /**
     * 盘查派出所
     */
    @TableField("pcpcs")
    private String pcpcs;

    /**
     * 盘查派出所电话
     */
    @TableField("pcpcsdh")
    private String pcpcsdh;

    /**
     * 盘查派出所电话
     */
    @TableField("ticketno")
    private String ticketno;

    /**
     * 座位号
     */
    @TableField("seatno")
    private String seatno;

    /**
     * 车牌号
     */
    @TableField("busno")
    private String busno;

    /**
     * 到达站
     */
    @TableField("prtname")
    private String prtname;

    /**
     * 发车时间
     */
    @TableField("sendtime")
    private String sendtime;

    /**
     * 终点站
     */
    @TableField("endname")
    private String endname;

}
