package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_code_device")
public class LfCodeDeviceEntity {

    @TableId("code")
    private String code;

    @TableField("name")
    private String name;

    @TableField("code_lev1")
    private String codeLev1;

    @TableField("dsc_lev1")
    private String dscLev1;

    @TableField("lev")
    private String lev;

    @TableField("child")
    private String child;

    @TableField("sort")
    private String sort;

    @TableField("x")
    private String x;

    @TableField("y")
    private String y;

    @TableField("total")
    private String total;

    @TableField("status")
    private String status;

    @TableField("djsj")
    private String djsj;

    @TableField("bz")
    private String bz;

    @TableField("wllx")
    private String wllx;

}
