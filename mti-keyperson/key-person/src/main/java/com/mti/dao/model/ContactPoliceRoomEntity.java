package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 警务室巡区信息
 * </p>
 *
 * @author zhaichen
 * @since 2019-07-26
 */
@Data
@Accessors(chain = true)
@TableName("t_contact_police_room")
@ApiModel(value = "警务室巡区信息", description = "警务室巡区信息")
public class ContactPoliceRoomEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 单位名称
     */
    @ApiModelProperty(value = "单位名称", dataType = "String")
    @TableField("unit_name")
    private String unitName;

    /**
     * 区域划分
     */
    @ApiModelProperty(value = "区域划分", dataType = "String")
    @TableField("regional_division")
    private String regionalDivision;

    /**
     * 屯警点名称
     */
    @ApiModelProperty(value = "屯警点名称", dataType = "String")
    @TableField("police_place")
    private String policePlace;

    /**
     * 屯警点位置
     */
    @ApiModelProperty(value = "屯警点位置", dataType = "String")
    @TableField("police_place_location")
    private String policePlaceLocation;

    /**
     * 警车主巡路线
     */
    @ApiModelProperty(value = "警车主巡路线", dataType = "String")
    @TableField("police_car_main_line")
    private String policeCarMainLine;

    /**
     * 重点目标任务
     */
    @ApiModelProperty(value = "重点目标任务", dataType = "String")
    @TableField("key_target_task")
    private String keyTargetTask;

}
