package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@ApiModel(description = "昆明重点人员流程类")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_km_process")
public class KmProcessEntity extends BaseEntity {
    private static final long serialVersionUID = -2747116569349623530L;
    /**
     * 流程编码
     */
    @ApiModelProperty(value = "流程编码")
    @TableField(value = "process_code")
    private Integer processCode;
    /**
     * 流程类型
     */
    @ApiModelProperty(value = "流程类型")
    @TableField(value = "process_type")
    private String processType;
    /**
     * 流程节点
     */
    @ApiModelProperty(value = "流程节点")
    @TableField(value = "process_node")
    private String processNode;
    /**
     * 流程描述
     */
    @ApiModelProperty(value = "流程描述")
    @TableField(value = "process_describe")
    private String processDescribe;
}
