package com.mti.dao.vo;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

/**
 * @Classname ZdryYjStatisticsVo
 * @Description TODO
 * @Date 2020/5/6 10:31
 * @Created by duchaof
 * @Version 1.0
 */
@Data
public class ZdryYjStatisticsVo {
    private String ssxqId;
    private String ssxq;
    private String sspcsId;
    private String sspcs;
    private String mjid;
    private String gkmj;

    private Long total=0l;
    private Long personCount=0l;
    private Long handleTotal=0l;

    private Long tldpCount=0l;
    private Long tldpPersonCount=0l;
    private Long tldpHandleCount=0l;

    private Long wbswCount=0l;
    private Long wbswPersonCount=0l;
    private Long wbswHandleCount=0l;

    private Long lgzsCount=0l;
    private Long lgzsPersonCount=0l;
    private Long lgzsHandleCount=0l;

    private Long mhdzCount=0l;
    private Long mhdzPersonCount=0l;
    private Long mhdzHandleCount=0l;

    private Long mhlgCount=0l;
    private Long mhlgPersonCount=0l;
    private Long mhlgHandleCount=0l;

    private Long glkyCount=0l;
    private Long glkyPersonCount=0l;
    private Long glkyHandleCount=0l;

    private Long phyjCount=0l;
    private Long phyjPersonCount=0l;
    private Long phyjHandleCount=0l;

    private Long rlsbCount=0l;
    private Long rlsbPersonCount=0l;
    private Long rlsbHandleCount=0l;

    private List<ZdryYjStatisticsVo> child;

}
