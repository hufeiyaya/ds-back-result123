package com.mti.dao.kmmodel;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mti.component.snowflake.KeyWorker;
import com.mti.dao.dto.LfZdryDto;
import com.mti.dao.model.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname KmZdryEntity
 * @Description TODO
 * @Date 2020/1/9 15:46
 * @Created by duchaof
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
//@TableName("t_lf_zdry")
@ApiModel(value = "昆明重点人员对象", description = "昆明重点人员对象")
public class KmZdryDto extends LfZdryDto {

   
   /* public LfZdryBaseEntity transToLfZdryBase(){
        LfZdryBaseEntity lfZdryBase = new LfZdryBaseEntity();
        lfZdryBase.setXm(this.getXm());
        lfZdryBase.setXb(this.getXb());
        lfZdryBase.setMz(this.getMz());
        lfZdryBase.setSfzh(this.getSfzh());
        lfZdryBase.setHjd(this.getHjd());
        lfZdryBase.setSjh(this.getSjh());
        lfZdryBase.setSflsgkcs(this.getSflsgkcs());
        lfZdryBase.setJtgkcshwkzyy(this.getJtgkcshwkzyy());
        lfZdryBase.setZrdw(this.getZrdw());
        lfZdryBase.setGkmj(this.getGkmj());
        lfZdryBase.setMjlxdh(this.getMjlxdh());
        lfZdryBase.setUpdateTime(this.getUpdate_time());
        lfZdryBase.setBz(this.getBz());
        lfZdryBase.setJobId(this.getJob_id());
        lfZdryBase.setSfzk(this.getSfzk());
        lfZdryBase.setZrbm(this.getZrbm());
        lfZdryBase.setSsxq(this.getSsxq());
        lfZdryBase.setX(this.getX());
        lfZdryBase.setY(this.getY());
        lfZdryBase.setSfbk(this.getSfbk());
        lfZdryBase.setCreateTimex(this.getCreate_timex());
        lfZdryBase.setUpdateTimex(this.getUpdate_timex());
        lfZdryBase.setTx(this.getTx());
        lfZdryBase.setSspcs(this.getSspcs());
        lfZdryBase.setSspcsId(this.getOid());
        lfZdryBase.setMjid(this.getMjid());
        lfZdryBase.setSfzp(this.getSfzp());
        lfZdryBase.setSsxqId(this.getSsxqId());
        lfZdryBase.setApproveStatus(this.getApproveStatus());
        lfZdryBase.setHjs(this.getHjs());
        lfZdryBase.setHjss(this.getHjsS());
        lfZdryBase.setHjx(this.getHjx());
        lfZdryBase.setHjdX(this.getHjdX());
        lfZdryBase.setHjdY(this.getHjdY());
        lfZdryBase.setProcessType(this.getProcessType());
        lfZdryBase.setUpStatus(this.getUpStatus());
        lfZdryBase.setSubmitter(this.getSubmitter());
        lfZdryBase.setLastOperate(this.getLastOperate());
        lfZdryBase.setSubmitterRoleId(this.getSubmitterRoleId());
        lfZdryBase.setGkjb(this.getGkjb());
        lfZdryBase.setGkjbCode(this.getGkjbx());
        lfZdryBase.setRysx(this.getRysx());
        lfZdryBase.setYjczlb(this.getYjczlb());
        lfZdryBase.setWkzt(this.getWkzt());
        return lfZdryBase;
     }*/
   

   /* public List<KmZdryAssistEntity> transToKmZdryAssist(String zdryId){
        List<KmZdryAssistEntity> result = new ArrayList<>();
        if(null != this.getSjh() && this.getSjh().split(",").length>0){
            for(String sjh : this.getSjh().split(",")){
                if(sjh.length()>0){
                    KmZdryAssistEntity kmZdryAssist= new KmZdryAssistEntity();
                    kmZdryAssist.setZdryId(zdryId);
                    kmZdryAssist.setInfoType(1);
                    kmZdryAssist.setInfoValue(sjh);
                    result.add(kmZdryAssist);
                }
            }
        }
        if(null != this.getWx() && this.getWx().length()>0){
            KmZdryAssistEntity wx= new KmZdryAssistEntity();
            wx.setZdryId(zdryId);
            wx.setInfoType(2);
            wx.setInfoValue(this.getWx());
            result.add(wx);
        }
        if(null != this.getQq() && this.getQq().length()>0){
            KmZdryAssistEntity qq= new KmZdryAssistEntity();
            qq.setZdryId(zdryId);
            qq.setInfoType(3);
            qq.setInfoValue(this.getWx());
            result.add(qq);
        }
        if(null != this.getMxcph() && this.getMxcph().length()>0){
            KmZdryAssistEntity mxcph= new KmZdryAssistEntity();
            mxcph.setZdryId(zdryId);
            mxcph.setInfoType(4);
            mxcph.setInfoValue(this.getMxcph());
            result.add(mxcph);
        }
        if(null != this.getSjsyclhp() && this.getSjsyclhp().length()>0){
            KmZdryAssistEntity sjsyclhp= new KmZdryAssistEntity();
            sjsyclhp.setZdryId(zdryId);
            sjsyclhp.setInfoType(5);
            sjsyclhp.setInfoValue(this.getSjsyclhp());
            result.add(sjsyclhp);
        }
        return result;
     }*/

    /* public KmZdryHomeEntity transToKmZdryHome(String zdryId){
         KmZdryHomeEntity xzz = new KmZdryHomeEntity();
        xzz.setZdryId(zdryId);
        xzz.setXzzs(this.getXzzs());
        xzz.setXzzss(this.getXzzsS());
        xzz.setXzzx(this.getXzzx());
        xzz.setXzz(this.getXzz());
        xzz.setX(this.getX());
        xzz.setY(this.getY());
        return xzz;
     }*/

   /*  public KmZdryTypeEntity transToKmZdryType(String zdryId){
         KmZdryTypeEntity zt = new KmZdryTypeEntity();
        zt.setZdryId(zdryId);
        zt.setRylb(this.getRylb());
        zt.setRylbCode(this.getRylbx());
        zt.setZrbm(this.getZrbm());
        zt.setXl(this.getXl());
        return zt;
     }*/
}
