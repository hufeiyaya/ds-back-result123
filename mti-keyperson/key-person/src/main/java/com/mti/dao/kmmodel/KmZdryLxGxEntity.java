package com.mti.dao.kmmodel;

import com.baomidou.mybatisplus.annotation.KeySequence;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * @Classname KmZdryLxGxEntity
 * @Description TODO
 * @Date 2020/1/9 17:02
 * @Created by duchaof
 * @Version 1.0
 */

@Data
@Accessors(chain = true)
@TableName("t_lf_zdry_lxgx")
@ApiModel(value = "昆明重点人员类型关系", description = "昆明重点人员类型关系")
@KeySequence(value = "t_lf_zdry_lxgx_seq", clazz = Integer.class)
public class KmZdryLxGxEntity {

    @ApiModelProperty(value = "id")
    @TableId("id")
    private Integer Id;

    @ApiModelProperty(value = "重点人员序号")
    @TableField("zdryxh")
    private String zdryxh;

    @ApiModelProperty(value = "重点人员大类")
    @TableField("zdry_dl")
    private Integer zdryDl;

    @ApiModelProperty(value = "重点人员大类类型")
    @TableField("zdry_dl_lx")
    private String zdrydllx;

    @ApiModelProperty(value = "重点人员大类名称")
    @TableField("zdry_dl_name")
    private String zdryDlName;


    @ApiModelProperty(value = "重点人员小类")
    @TableField("zdry_xl")
    private Integer zdryXl;

    @ApiModelProperty(value = "重点人员小类名称")
    @TableField("zdry_xl_name")
    private String zdryXlName;

    @ApiModelProperty(value = "插入时间")
    @TableField("inserttime")
    private Date insertTime;

    @ApiModelProperty(value = "更新时间")
    @TableField("updatetime")
    private Date updateTime;

    @ApiModelProperty(value = "删除标志")
    @TableField("deleted")
    private Integer deleted;



}
