package com.mti.dao.dto;

import lombok.Data;

/**
 * @Classname ResultDto
 * @Description TODO
 * @Date 2020/6/19 13:25
 * @Created by duchaof
 * @Version 1.0
 */
@Data
public class ResultDto {
     private String Id ;
     private Integer StatusCode = 0;
     private String StatusString = "OK";
}
