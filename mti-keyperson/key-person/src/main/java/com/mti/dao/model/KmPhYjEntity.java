package com.mti.dao.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mti.component.snowflake.KeyWorker;
import com.mti.utils.DateUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 手机预警
 * </p>
 *
 * @author duchao
 * @since 2020-04-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_ph_yj")
public class KmPhYjEntity extends Model<KmPhYjEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("alarm_id")
    private String alarmId;

    /**
     * 预警地址
     */
    @TableField("alarm_location")
    private String alarmLocation;

    /**
     * 预警时间
     */
    @TableField("alarm_time")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date alarmTime;

    /**
     * 预警类型
     */
    @TableField("alarm_type")
    private String alarmType;

    /**
     * 身份证号
     */
    @TableField("id_card")
    private String idCard;

    /**
     * 经度
     */
    @TableField("loc")
    private String loc;

    /**
     * 维度
     */
    @TableField("lat")
    private String lat;

    /**
     * 预警人姓名
     */
    @TableField("name")
    private String name;

    /**
     * 手机号码
     */
    @TableField("phone")
    private String phone;


    @Override
    protected Serializable pkVal() {
        return null;
    }

    public LfZtryyjEntity convertZdryYj(){
        LfZtryyjEntity lfZtryyjEntity = new LfZtryyjEntity();
        lfZtryyjEntity.setWybs(String.valueOf(KeyWorker.nextId()));
        lfZtryyjEntity.setYjlx("phyj");
        lfZtryyjEntity.setRyxm(this.getName());
        lfZtryyjEntity.setYjbs(this.getIdCard());
        lfZtryyjEntity.setYjsj(DateUtils.dateToString(this.getAlarmTime(),"yyyy-MM-dd HH:mm:ss"));
        lfZtryyjEntity.setYjdd(this.getAlarmLocation());
        lfZtryyjEntity.setTbsj(DateUtils.dateToString(new Date(),"yyyy-MM-dd HH:mm:ss"));
        //将坐标设置为固定值
        if(null != this.alarmType && "航班预警".equals(this.alarmType)){
            lfZtryyjEntity.setX("102.935615");
            lfZtryyjEntity.setY("25.102871");
        }else if(null != this.alarmType && "火车预警".equals(this.alarmType)){
            if(null != this.getAlarmLocation() && this.getAlarmLocation().substring(0,3).contains("D") || this.getAlarmLocation().substring(0,3).contains("G")){
                lfZtryyjEntity.setX("102.867943");
                lfZtryyjEntity.setY("24.876253");
            }else {
                lfZtryyjEntity.setX("102.728219");
                lfZtryyjEntity.setY("25.022114");
            }
        } else {
            lfZtryyjEntity.setX(this.loc);
            lfZtryyjEntity.setY(this.lat);
        }
        lfZtryyjEntity.setGlId(this.alarmId);
        lfZtryyjEntity.setSendFlag(0);
        lfZtryyjEntity.setSjbs(this.getPhone());
        return lfZtryyjEntity;
    }
}
