package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_sys_test")
public class SysTestEntity {

    /**
     * 流水编号
     */
    @TableId("id_")
    private String id;

    @TableField("name_")
    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time_")
    private Date createTime;


}
