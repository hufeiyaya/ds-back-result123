package com.mti.dao.vo;

import com.mti.dao.model.LfZdryHistoryLogEntity;
import lombok.Data;

/**
 * @Classname ZdryHistoryLogVo
 * @Description TODO
 * @Date 2020/4/27 16:32
 * @Created by duchaof
 * @Version 1.0
 */
@Data
public class ZdryHistoryLogVo extends LfZdryHistoryLogEntity {
    /**
     * 重点人员姓名
     */
    private String xm;

    /**
     * 重点人员身份证号
     */
    private String sfzh;

    /**
     * 记录名称
     */
    private String typeName;

    /**
     * 人员大类
     */
    private String dlName;

    /**
     * 人员小类
     */
    private String xlName;


}
