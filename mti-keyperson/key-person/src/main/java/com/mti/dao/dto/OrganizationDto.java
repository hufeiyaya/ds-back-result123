package com.mti.dao.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyanjiang
 * @since 2018-09-17
 */
@Data
@ApiModel(description = "组织机构实体类")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrganizationDto implements Serializable {

    private static final long serialVersionUID = 1L;


    private String id;


    private String number;


    private String code;


    private Integer type;


    private String name;


    private String shortName;


    private Integer sort;


    private String description;


    private String regionId;


    private String regionPid;


    private String businessPid;


    private String address;


    private String zipCode;


    private String concatPhone;


    private String firstManager;


    private String secondManager;


    private String version;


    private Integer available;


    private String creatorId;
    

    private Integer internal;


    private Integer delete;

    

    private Integer orgType;


    private String orgSort;
    /**
     * 所属区域
     */
    @TableField(value = "qu_")
    private String qu;
    

}
