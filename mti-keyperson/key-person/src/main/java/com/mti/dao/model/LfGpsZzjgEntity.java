package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Data
@Accessors(chain = true)
@TableName("t_lf_gps_zzjg")
public class LfGpsZzjgEntity {

    @TableField("orgid")
    private String orgid;

    @TableField("orgname")
    private String orgname;

    @TableField("parentid")
    private String parentid;

    @TableField("no")
    private String no;

    @TableField("orgcode")
    private String orgcode;

}
