package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chenf
 * @since 2020-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_km_trail_set")
@AllArgsConstructor
@NoArgsConstructor
public class TKmTrailSetEntity {

    /**
     * id
     */
    @TableId("id")
    @ApiModelProperty(value = "主键",name = "id")
    private String id;

    /**
     * 线索类型 
     */
    @TableField("trail_type")
    @ApiModelProperty(value = "线索类型：1 聚集 2 伴随",name = "trailType")
    private String trailType;
    /**
     * 名称
     */
    @TableField("name")
    @ApiModelProperty(value = "人员大类（新）",name = "name")
    private String name;
    /**
     * 数据来源
     */
    @TableField("warning_type")
    @ApiModelProperty(value = "数据来源",name = "warningType")
    private String warningType;
    
    /**
        * 距离宽容度
     */
    @TableField("distance_limit")
    @ApiModelProperty(value = "距离宽容度",name = "distanceLimit")
    private String distanceLimit;
    
    @TableField("time_limit")
    @ApiModelProperty(value = "时间宽容度",name = "timeLimit")
    private String timeLimit;

    /**
     * 碰撞次数
     */
    @TableField("calculate_times")
    @ApiModelProperty(value = "碰撞次数",name = "calculateTimes")
    private String calculateTimes;
    
    /**
     * 数据计算频率
     */
    @TableField("calculate_frequency")
    @ApiModelProperty(value = "数据计算频率",name = "calculateFrequency")
    private String calculateFrequency;
    
    /**
     *备注
     */
    @TableField("description")
    @ApiModelProperty(value = "备注",name = "description")
    private String description;
    
    /**
     * 配置状态：0 暂停 1启动
     */
    @TableField("is_use")
    @ApiModelProperty(value = "配置状态",name = "isUse")
    private String isUse;
    
    /**
     * 删除标识：0 未删除 1已删除
     */
    @TableField("is_delete")
    @ApiModelProperty(value = "删除标识",name = "isDelete")
    private String isDelete;
    
    /**
     * 创建时间
     */
    @TableField("create_time")
    @ApiModelProperty(value = "创建时间",name = "createTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    
    /**
     * 创建人
     */
    @TableField("creater")
    @ApiModelProperty(value = "创建人",name = "creater")
    private String creater;
 
    
    /**
     *网格大小（按网格）
     */
    @TableField("grid_first")
    @ApiModelProperty(value = "网格大小",name = "gridFirst")
    private String gridFirst;
    
    
    /**
           * 网格大小（自定义）
     */
    @TableField("grid_second")
    @ApiModelProperty(value = "网格大小",name = "gridSecond")
    private String gridSecond;
    
    
    /**
     * 大类人员类型
   */
    @TableField("person_type")
    @ApiModelProperty(value = "网格大小",name = "gridSecond")
    private String personType;
    
    /**
     * 大类人员类型
   */
    @TableField("warning_value")
    @ApiModelProperty(value = "预警阈值",name = "warningValue")
    private String warningValue;
    
    /**
   	 * 人员类型
   	 */
    @ApiModelProperty(value = "人员类型")
    @TableField(exist = false)
    private List<TKmTrailSetRelEntity> typeList;

 
}
