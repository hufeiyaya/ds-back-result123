package com.mti.dao.vo;

import com.mti.dao.model.LfZtryyjEntity;
import com.mti.dao.model.TKmYjHandleEntity;
import com.mti.dao.model.TTaskFileEntity;
import lombok.Data;

import java.util.List;

/**
 * @author Administrator
 * @Classname TKmYjHandleVo
 * @Description TODO
 * @Date 2020/3/11 15:25
 * @Created by duchaof
 * @Version 1.0
 */
@Data
public class TKmYjHandleVo extends LfZtryyjEntity {
    /**
     * 户籍地
     */
    private String hjd;

    /**
     * 人员大类
     */
    private String rylbx;

    /**
     * 人员小类
     */
    private String xl;

    /**
     * 所属派出所
     */
    private String sspcs;

    /**
     * 管控民警
     */
    private String gkmj;

    /**
     * 稳控状态
     */
    private String wkzt;

    /**
     * 风险等级
     */
    private String gkjb;

    /**
     * 对应的处境处置类别
     */
    private TKmYjHandleEntity tKmYjHandleEntity;


}
