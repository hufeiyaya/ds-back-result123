package com.mti.dao.kmmodel;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

/**
 * @author ： caoxx
 * @date ：2020/09/08 14:20
 * @description :布控类别值的json实体类
 * @path : com.mti.dao.kmmodel.KmArmingTaskJsonEntity
 * @modifiedBy ：
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "布控类别值对应的json实体类", description = "布控类别值对应的json实体类")
public class KmArmingTaskJsonEntity {
    private List<String[]> rylbxList;
    private List<String> zrbmList;
    private List<String> sfzkList;
    private List<String> gkjbxList;
    private List<String> queryList;
    private List<String> sspcsList;
    private List<String> mjidList;
    private List<String> rysxList;
    private List<String> yjczlbList;
    private List<String> queryLsit;
    private List<String> wkztList;
    private List<String> ynzsList;
    private List<String> personList;
    private List<String> hjssxList;
    private List<String> controlTypeList;
}
