package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/9/11
 * @change
 * @describe describe
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_lf_zdry_arming_task_relation")
public class ArmingTaskRelationEntity extends BaseEntity implements Necessary {
    private static final long serialVersionUID = -8170907511101557429L;
    @TableField("sp_id")
    private String spId;
    @TableField("at_id")
    private String atId;
    @TableField("report_org_id")
    private String reportOrgId;
    @TableField("approve_status")
    private Integer approveStatus;
    @TableField("description")
    private String description;

}
