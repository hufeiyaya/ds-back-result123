package com.mti.dao.kmmodel;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author ： caoxx
 * @date ：2020/09/01 15:29
 * @description :
 * @path : com.mti.dao.kmmodel.KmArmingTaskEntity
 * @modifiedBy ：zhangmx
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_km_arming_task")
@ApiModel(value = "布控任务实体")
public class KmArmingTaskEntity {
    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;
    @ApiModelProperty(value = "布控任务名称")
    @TableField("task_name")
    private String taskName;
    @ApiModelProperty(value = "布控范围(json格式数据)")
    @TableField("arming_range")
    private String armingRange;
    @ApiModelProperty(value = "预警控制开关(0：关，1：开)")
    @TableField("alarm_switch")
    private Integer controlAreaSwitch;
    @ApiModelProperty(value = "布控类别值(对应布控类别的json值)")
    @TableField("arming_value")
    private String armingValue;
    @ApiModelProperty(value = "布控原因")
    @TableField("arming_reason")
    private String armingReason;
    @ApiModelProperty(value = "布控单位")
    @TableField("arming_org_id")
    private String armingOrgId;
    @ApiModelProperty(value = "布控任务创建人Id")
    @TableField("arming_person_id")
    private String armingPersonId;
    @ApiModelProperty(value = "布控单位名称")
    @TableField("arming_org_name")
    private String armingOrgName;
    @ApiModelProperty(value = "布控任务创建人")
    @TableField("arming_person_name")
    private String armingPersonName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "开始时间")
    @TableField("start_date")
    private LocalDateTime startDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "结束时间")
    @TableField("end_date")
    private LocalDateTime endDate;
    @ApiModelProperty(value = "布控状态(0:基本信息状态，1：启动，2：暂停，3：已结束，-1：已删除)")
    @TableField("arming_status")
    private Integer armingStatus;
    @ApiModelProperty(value = "重要程度(0：一般，1：中等，2：重要)")
    @TableField("import_level")
    private Integer importLevel;
    @ApiModelProperty(value = "创建时间")
    @TableField("create_date_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDateTime;
    @ApiModelProperty(value = "更新时间")
    @TableField("last_update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime lastUpdateTime;
    @ApiModelProperty(value = "是否默认")
    @TableField("is_default")
    private String isDefault;
    
}
