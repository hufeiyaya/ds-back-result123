package com.mti.dao.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@ApiModel(description = "重点人员审批日志类")
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_lf_zdry_approve_log")
public class LfZdryApproveLogEntity extends BaseEntity {
    private static final long serialVersionUID = -2747116569349623530L;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人(每次操作人)")
    @TableField(value = "creator")
    private String creator;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_time")
    private LocalDateTime createTime = LocalDateTime.now();
    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    @TableField(value = "modifier")
    private String modifier;
    /**
     * 最后更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "last_update_time")
    private LocalDateTime lastUpdateTime = LocalDateTime.now();
    /**
     * 重点人员ID
     */
    @ApiModelProperty(value = "重点人员ID(多个用英文逗号拼接)")
    @TableField(value = "sp_id")
    private String spId;
    /**
     * 操作名称
     */
    @ApiModelProperty(value = "操作名称")
    @TableField(value = "operation")
    private String operation;
    /**
     * 操作结果
     */
    @ApiModelProperty(value = "操作结果")
    @TableField(value = "approve_result")
    private String approveResult;
    /**
     * 审核中标记 :0进行中，1已完成
     */
    @ApiModelProperty(value = "审核中标记")
    @TableField(value = "approving_flag")
    private String approvingFlag;
    /**
     * 审批状态
     */
    @ApiModelProperty(value = "审批状态值(0:上报中,1:待警种审核，2：待情报审核，3：退回，4：审核完毕)")
    @TableField(exist = false)
    private String approveStatus;
    /**
     * 操作部门
     */
    @ApiModelProperty(value = "操作部门")
    @TableField(value = "operate_dept")
    private String operateDept;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注(审批意见)")
    @TableField(value = "addition")
    private String addition;
}
