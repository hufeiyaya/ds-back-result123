package com.mti.jwt;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Setter
@Getter
@ToString
@ApiModel(description = "人员信息实体类")
public class PersonBo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String number;
    
    private String avator;

    private String name;

    private String policeCode;

    private Integer credentialsType;

    private String credentialsNumber;

    private Integer sex;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date birthDate;

    private String address;

    private Integer security;

    private String version;

    private Integer available;

    private String phone;

    private String email;

    private String mainDuty;
}
