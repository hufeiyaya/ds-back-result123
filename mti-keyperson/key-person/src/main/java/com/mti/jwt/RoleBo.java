package com.mti.jwt;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleBo implements Serializable {

	private String id;
    private String clientType;
    private String createTime;
    private String menuIds;
    private String name;
    private String roleType;
}
