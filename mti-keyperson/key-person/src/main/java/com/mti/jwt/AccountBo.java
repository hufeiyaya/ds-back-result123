package com.mti.jwt;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountBo  implements Serializable {

    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 1690891663910437828L;

	private String id;

    private String loginName;

    private String password;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    
    private String creatorId;

    private String personId;
    
    private String description;

    private String orgId;

    private Integer available;

    private Integer statu;
    /**
     * 人员信息
     */
    private PersonBo personBo;

    /**
     * 组织信息
     */
    private OrganizationBo organizationBo;

//    /**
//     * 角色权限(包含临时权限)
//     */
//    private List<MenuEntity> menus;
    
    /**
     * 菜单IDS
     */
    private List<String> menuIds;
    
    /**
     * 角色IDS
     */
    private List<RoleBo> roleList;
    
    /**
     * token
     */
    private String token;

    /**
     * 客户端类型
     * web/android ...
     */
    private String clientType;
    
    /**
     * 查找SIP账号是否
     */
    private Boolean sipFlag = false;
}
