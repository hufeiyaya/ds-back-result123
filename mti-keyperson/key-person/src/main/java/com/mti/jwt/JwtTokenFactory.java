package com.mti.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Base64Utils;

/**
 * <p>
 *
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public class JwtTokenFactory {
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenFactory.class);
    public static final long EXPIRATIONTIME = 432000000L;
    static final String SECRET_KEY = "Dk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjS";
    static final String TOKEN_PREFIX = "Bearer";
    public static final String HEADER_STRING = "Authorization";
    public static final String COOKIE = "Cookie";
    public static final String USER_AGENT = "user-agent";
    static final String TOKEN_ISSUER = "http://daoshu.com";
    SignatureAlgorithm signatureAlgorithm;

    public JwtTokenFactory() {
        this.signatureAlgorithm = SignatureAlgorithm.HS256;
    }

    public static String createAccessJwtToken(String name, String role) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Cannot create JWT Token without username");
        } else if (StringUtils.isBlank(role)) {
            throw new IllegalArgumentException("User doesn't have any privileges");
        } else {
            String token = Jwts.builder().setSubject(name).setId(role).setIssuedAt(new Date()).setIssuer("http://daoshu.com").signWith(SignatureAlgorithm.HS512, Base64Utils.decodeFromString("Dk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjS")).compact();
            return token;
        }
    }

    public static Jws<Claims> parseClaims(String token) {
        try {
            return Jwts.parser().setSigningKey(Base64Utils.decodeFromString("Dk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjS")).parseClaimsJws(token);
        } catch (MalformedJwtException | IllegalArgumentException | SignatureException | UnsupportedJwtException var2) {
            logger.error("Invalid JWT Token", var2);
            throw new BusinessException(10000, String.format("Invalid JWT token:{} ", var2));
        } catch (ExpiredJwtException var3) {
            logger.info("JWT Token is expired", var3);
            throw new BusinessException(10018, String.format("Invalid JWT token:{} ", var3));
        }
    }

    public static void main(String[] args) {
        String tt = createAccessJwtToken("admin", "1");
        String t2 = createAccessJwtToken("admin", "1");
        System.out.println(tt);
        System.out.println(t2);
        System.out.println(tt.equals(t2));
        Claims claim1 = (Claims)parseClaims(tt).getBody();
        System.out.println(claim1.toString());
        Claims claim2 = (Claims)parseClaims(t2).getBody();
        System.out.println(claim1.toString());
    }
}

