package com.mti.jwt;

/**
 * <p>
 *
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public enum ReturnStatus {
    SUCCESS(200, "成功"),
    INTERNAL_SERVER_ERROR(500, "请求的服务产生异常"),
    NOT_FOUND(404, "请求的服务不存在"),
    SERVICE_NOT_SUPPORT(408, "由于特殊原因，此服务已不支持"),
    FAULT_ACCESS_TOKEN(900, "您没有权限访问该接口"),
    NO_ACCESS_TOKEN(901, "缺少访问授权参数"),
    EXPIRE_ACCESS_TOKEN(902, "您授权已到期"),
    KEY_IN_USED(903, "该字典项已经被使用，不允许删除"),
    JPUSH_COLLEGE_FAIL(904, "专版优圈推送异常"),
    SMS_COUNT_LIMIT(905, "今天已到接受验证码次数上限（8次），请您明天再操作"),
    BUSINESS_ERROR(10001, "业务逻辑异常"),
    PARAMETER_EMPTY(10003, "参数不能为空"),
    PARAMETER_INCORRECT(10004, "参数错误"),
    CONFIG_KEY_REPEAT(10005, "字典项键重复"),
    CONFIG_DETAIL_REPEAT(10006, "字典项值重复"),
    NOT_ALLOW_PUBLISH(10007, "系统维护中，暂时不能使用！"),
    GLOBAL_CONFIG_USER_BEHAVIOR_VALID_ERROR(1000403, "用户有效操作记录时间不能大于60分钟"),
    GLOBAL_CONFIG_USER_BEHAVIOR_INVALID_ERROR(1000404, "用户无效操作记录时间不能小于24小时"),
    BUSINESS_DISTRICTID_NOT_EXIST(1000401, "districtId不能为空"),
    PAY_PREPAY_ID_NOT_FOUND(1000405, "预支付单获取失败"),
    NUM_TOO_LANG(1000406, "编号太长"),
    UPLOAD_ERROR(1002, "文件上传异常"),
    NULL_POINT_ERROR(2001, "空指针异常"),
    SQL_ERROR(2002, "SQL执行异常"),
    FILE_NOT_FOUND_ERROR(2003, "文件不存在"),
    OTHER_ERROR(9001, "其他异常"),
    ALARM_NOT_EXIST(10201, "警情不存在"),
    NO_TOKEN(10101, "缺少参数[tokenId]"),
    FAULT_TOKEN(10102, "错误的Token"),
    EXPIRE_TOKEN(10103, "Token已过期，请重新登录");

    private final int value;
    private String description;

    private ReturnStatus(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return this.value;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

