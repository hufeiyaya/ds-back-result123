package com.mti.jwt;

/**
 * <p>
 *
 * </p>
 *
 * @author yangsp
 * @since 2019-04-29
 */
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 8882366904034518521L;
    public static final int CODE_GLOCBAL_EXCEPTION = 12000;
    public static final int CODE_UN_AUTHORIZATION = 10000;
    public static final int CODE_NONEXISTENT_USER = 10001;
    public static final int CODE_INCORRECT_USER = 10002;
    public static final int CODE_DELETE_USER = 10017;
    public static final int CODE_DISTABLE_USER = 10016;
    public static final int CODE_VALIDATE_USER = 10018;//非法的用户
    public static final int CODE_ALREADY_LOGIN = 10019;
    public static final int CODE_ILLEGAL_SEAT = 10021;
    public static final int CODE_ILLEGAL_PARAMS = 10003;
    public static final int CODE_ILLEGAL_IMAGE_SIZE = 10004;
    public static final int CODE_ILLEGAL_NOTMAPPING = 10013;
    public static final int CODE_INSERT_ERROR = 10020;
    public static final int CODE_QUERY_ERROR = 10005;
    public static final int CODE_UPDATE_ERROR = 10006;
    public static final int CODE_DELETE_INEXISTENCE_ERROR = 10007;
    public static final int CODE_DELETE_INUSE_ERROR = 10008;
    public static final int CODE_REQUEST_DATA_INEXISTENCE = 10009;
    public static final int CODE_XML_PARSE_ERROR = 100010;
    public static final int CODE_REQUEST_IMAGE_INEXISTENCE = 100011;
    public static final int CODE_CRITERIA_DUPLICATE = 100012;
    public static final int CODE_REDIS_ERROR = 100013;
    public static final int CODE_CACHE_ERROR = 100014;
    public static final int CODE_COLLETION_INDEX_ERROR = 100015;
    public static final int CODE_USER_SESSION_ERROR = 100016;
    public static final int CODE_MISSING_SYSTENCONFIG_ERROR = 100017;
    public static final int CODE_THREADS_FINISH_ERROR = 100018;
    public static final int CODE_QUERY_OVERSIZE = 100019;
    public static final int CODE_RECORD_EXISTS = 100020;
    public static final int CODE_SYNCHRONIZATION_FAILURE = 100021;
    public static final int CODE_IMAGE_EXCEPTION = 100022;
    public static final int CODE_QUERY_EMPTY = 100023;
    public static final int CODE_ILLEGAL_OPERATION = 100024;
    public static final int CODE_IMAGE_COMPRESSION_FAILURE = 100025;
    public static final int CODE_DELETE_FILE_FAILURE = 100026;
    public static final int CODE_UPLOAD_FILE_FAILURE = 100027;
    public static final int CODE_FTP_NO_LOGIN = 100028;
    public static final int CODE_MQ_SEND_MESSAGE = 100029;
    public static final int CODE_SAME_PLATENUMBER = 100044;
    public static final int CODE_USER_LOGIN = 100045;
    public static final int CODE_MAIL_SEND_MESSAGE = 100046;
    public static final int CODE_FILE_DOWN = 100042;
    public static final int CODE_FILE_FORMAT = 100029;
    public static final int CODE_PLATENUMBER_ERROR = 100030;
    public static final int CODE_FILED_NOTNULL = 100031;
    public static final int CODE_FILE_COLUMN_NOTEQUALS = 100032;
    public static final int CODE_FILE_OVER_SIZE = 100033;
    public static final int CODE_FILE_RECORDS_EMPTY = 100034;
    public static final int CODE_FILE_ORGNAME_MISMATCHES = 100035;
    public static final int CODE_FILE_DATE_FORMAT_ERROR = 100036;
    public static final int CODE_FILE_DATE_LESS_THAN_NOW = 100037;
    public static final int CODE_FILE_DURATION_FORMATE_ERROR = 100038;
    public static final int CODE_FILED_STARTTIME_NOTNULL = 100040;
    public static final int CODE_FILE_DATE_ERROR = 100041;
    public static final int CODE_PLATENUMBER_FORMATE_ERROR = 100042;
    public static final int CODE_PRECISEPLATENUMBER_FORMATE_ERROR = 100043;
    public static final int CODE_ClientTEXT_NOMATCH_SERVICE = 100501;
    public static final int CODE_AUTH_FAILD = 100502;
    public static final int CODE_AUTH_FILE_NO_MATCH = 100503;
    public static final int CODE_AUTH_NONEXISTENT_USER = 100504;
    public static final int CODE_NO_DATE = 100601;
    public static final int CODE_GENERATE_FILE_ERROR = 100602;
    public static final int BEFORE_THAN_AUDIT = 100701;
    public static final int CACHE_NO_STATUS_EXIST = 100702;
    public static final int BEFORE_DEPLOY_CHECEK = 100703;
    public static final int CODE_SAME_DATE = 100704;
    public static final int CODE_DATE_ERROR = 100705;
    public static final int CODE_STARTTIME_MORETHAN_ENDTIME = 100706;
    public static final int CODE_PHONE_SAME = 100707;
    public static final int CODE_STATE_ERROR = 100708;
    public static final int CODE_REPEAT_INFO = 100709;
    public static final int CODE_SAME_INFO = 100710;
    public static final int HAS_USER_IN_ROLE = 100801;
    public static final int ROLENAME_REPEAT = 100802;
    public static final int ADMINROLE = 100803;
    public static final int ROLENAME_NULL = 100804;
    public static final int ROLEPERMISSION_NULL = 100805;
    public static final int CODE_PCC_RECOGNIZE_ERROR = 110001;
    public static final int CODE_PCC_RETURN_OVERTIME = 110006;
    public static final int CODE_PCC_RETURN_EMPTY_ERROR = 110007;
    public static final int CODE_REGION_ISNULL_ERROR = 110002;
    public static final int CODE_FEATURE_ISNULL_ERROR = 110003;
    public static final int CODE_NOT_CAR_ERROR = 110004;
    public static final int CODE_MATCH_SERVICE_ERROR = 110005;
    public static final int CODE_CTI_RETURN_OVERTIME = 120006;
    public static final int CODE_CTI_RETURN_EMPTY_ERROR = 120007;
    public static final int CODE_CTI_RESULT_ERROR = 120002;
    public static final int CODE_SHORT_MSG_SEND_FAILED = 130001;
    public static final int CODE_SHORT_MSG_INIT_FAILED = 130002;
    public static final int CODE_PLAN_STEP_ERROR = 100901;
    public static final int CODE_INSTANCE_NOT_CONTROL = 100902;
    private Integer code;
    private String message;
    private boolean customMsg = false;
    private ReturnStatus returnStatus;

    public BusinessException(ReturnStatus returnStatus) {
        this.code = returnStatus.getValue();
        this.message = returnStatus.getDescription();
    }

    public BusinessException(Integer code) {
        this.code = code;
        if (!"200".equals(code)) {
            this.message = "发生错误，调用失败";
        }

    }

    public BusinessException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public BusinessException(Integer code, String message, Throwable cause) {
        super(cause);
        this.code = code;
        this.message = message;
    }

    public BusinessException(Integer code, String message, boolean customMsg) {
        this.code = code;
        this.message = message;
        this.customMsg = customMsg;
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String toString() {
        return "code ..." + this.code;
    }

    public boolean isCustomMsg() {
        return this.customMsg;
    }

    public ReturnStatus getReturnStatus() {
        return this.returnStatus;
    }

    public void setReturnStatus(ReturnStatus returnStatus) {
        this.returnStatus = returnStatus;
    }
}
