package com.mti.utils;

import org.springframework.beans.factory.annotation.Value;

public class GpsUtils {

    @Value("${zdry.gpsUrl}")
    private String zdryGpsUrl; // 注入配置属性
    //private static String getgps = "http://localhost:10089/api/poi/getPoiInfo";

    public GpsUtils(){ }

    public String getgps(String address){
        String result = "";
        try {
            String url = this.zdryGpsUrl
                    + "?address=" + address;
            result = HttpClientUtil.sendGetRequest(url, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
