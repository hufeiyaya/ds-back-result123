package com.mti.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GeneratorIdCode {


    /**
     * 生成ID
     * @return
     */
    public static String getGeneratorById(String typeVal,String num) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int a = (int)(Math.random() * 9000.0D) + 1000;
        System.out.println(a);
        Date date = new Date();
        String str = sdf.format(date);
        String[] split = str.split("-");
        String s = split[0] + split[1] + split[2];
        String[] split1 = s.split(" ");
        String s1 = split1[0] + split1[1];
        String[] split2 = s1.split(":");
        //String s2 = split2[0] + split2[1] + split2[2] + a;
        String newStr = typeVal.toUpperCase();
        String fir = split2[0].substring(2,split2[0].length()).toString();
        String s2 = newStr+ fir + split2[1] + split2[2] + num;

//        System.out.println("*************************");
//        System.out.println(split2[0].substring(2,split2[0].length()));
//        System.out.println(split2[1]);
//        System.out.println(split2[2]);
//        System.out.println("*************************");


        return s2;
    }


    /***
     * 具体到毫秒数
     * @param typeVal
     * @param num
     * @return
     */
    public static String getGeneratorSec(String typeVal,String num) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        int a = (int)(Math.random() * 9000.0D) + 1000;
        System.out.println(a);
        Date date = new Date();
        String str = sdf.format(date);
        String[] split = str.split("-");
        String s = split[0] + split[1] + split[2];
        String[] split1 = s.split(" ");
        String s1 = split1[0] + split1[1];
        String[] split2 = s1.split(":");
        //String s2 = split2[0] + split2[1] + split2[2] + a;
        String newStr = typeVal.toUpperCase();
        String fir = split2[0].substring(2,split2[0].length()).toString();
        String s2 = newStr+ fir + split2[1] + split2[2] + split2[3]+ num;//精确到毫秒
        return s2;
    }

}
