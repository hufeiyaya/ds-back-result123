package com.mti.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mti.configuration.SystemConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * @Classname AddressOrLngLatConvertUtil
 * @Description TODO
 * @Date 2020/3/26 11:54
 * @Created by duchaof
 * @Version 1.0
 */
@Component
@Slf4j
public class AddressOrLngLatConvertUtil {
    @Autowired
    private SystemConfig systemConfig;

    public String checkAddress(String lng,String lat) {
        try {
            StringBuilder sb = new StringBuilder(this.systemConfig.getAddressIp());
            sb.append("/api/poi/searchByCoordinate").append("?").append("latitude=").append(Double.parseDouble(lat))
                    .append("&longitude=").append(Double.parseDouble(lng)).append("&radius=1")
                    .append("&pageNo=1&pageSize=10");
            String data = HttpClientUtil.sendGetRequest(sb.toString(), "UTF-8");
            JSONObject json = (JSONObject) JSON.parseObject(data).get("data");
            JSONArray array = (JSONArray) json.get("poiList");
            if (array.size() > 0) {
                JSONObject obj = (JSONObject) array.get(0);
                String address = String.valueOf(obj.get("search"));
                Map<String,String> mapList = AddressResolutionUtil.addressResolution(address).get(0);
                if(null != mapList)
                    return mapList.get("province")+mapList.get("city")+mapList.get("county");
            }
        } catch (Exception e) {
            log.info("转换坐标时发生异常=========>{}",e.getMessage());
            return null;
        }
        return null;
    }

    /**
     * 根据地址具体返回经纬度
     *
     * @return
     */
    public Map<String,String> checkXY(String address) throws Exception {
        try {
            Map<String,String> resultMap = new HashMap<>();
            StringBuilder sb = new StringBuilder(this.systemConfig.getAddressIp());
            sb.append("/api/poi/search").append("?address=").append(URLEncoder.encode(address, "UTF-8"))
                    .append("&pageNo=1&pageSize=10");
            String data = HttpClientUtil.sendGetRequest(sb.toString(), "UTF-8");
            JSONObject json = (JSONObject) JSON.parseObject(data).get("data");
            JSONArray array = (JSONArray) json.get("poiList");
            if (array.size() > 0) {
                for(int i=0;i<array.size();i++){
                JSONObject obj = (JSONObject) array.get(i);
                String lng = String.valueOf(obj.get("gcjlon"));
                resultMap.put("lng",lng == null?"":lng);
                String lat = String.valueOf(obj.get("gcjlat"));
                resultMap.put("lat",lat == null?"":lat);
                String search = String.valueOf(obj.get("search"));
                char firstChar = search.charAt(0);
                    if((firstChar >= 0x4e00)&&(firstChar <= 0x9fbb)) {
                       resultMap.put("search",search);
                    }
                }
                return resultMap;
            }
        } catch (UnsupportedEncodingException e) {
            log.error("根据地址查询具体的省市出现异常：=====>{}",e.getMessage());
            return null;
        }
        return null;
    }
}
