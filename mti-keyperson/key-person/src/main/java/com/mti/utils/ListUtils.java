package com.mti.utils;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {

	/**
	 * 删除列表中元素为null 的元素
	 * @param list
	 */
	public static <T> List<T> removeNullElement(List<T> list) {
		List<T> result =  new ArrayList<T>();
		if(list == null || list.isEmpty())
			return result;
		for(int i = list.size() - 1; i >= 0; i--) {
			if(list.get(i) != null && !"null".equalsIgnoreCase(list.get(i).toString().trim())) {
				result.add(list.get(i));
			}
		}
		return result;
	}

	/**
	 * 将集合等分
	 * @param list
	 * @param pageSize
	 * @param <T>
	 * @return
	 */
	public static <T> List<List<T>> splitList(List<T> list, int pageSize) {
		List<List<T>> listArray = new ArrayList<List<T>>();
		for (int i = 0; i < list.size(); i+=pageSize) {
			int toIndex = i + pageSize>list.size()?list.size():i+pageSize;
			listArray.add(list.subList(i, toIndex));
		}
		return listArray;
	}

}
