package com.mti.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Classname ExeclUtil
 * @Description TODO
 * @Date 2020/2/18 15:43
 * @Created by duchaof
 * @Version 1.0
 */
@Slf4j
public class ExeclUtil {
    private  static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void genExcel(SXSSFWorkbook wb, CellStyle cellStyleCenter,
                                 CellStyle cellStyleLeft, List<?> data, Class clazz,
                                 Map<String,String> proTitleMap,String sheetName) throws Exception{
        SXSSFSheet sh = wb.createSheet(sheetName);
        List<String> titles = new ArrayList<>(proTitleMap.values());
        List<String> properties = new ArrayList<>(proTitleMap.keySet());

        for(int i=0;i<data.size()+2;i++){
            Row row = sh.createRow(i);

            for(int colNum = 0; colNum < titles.size(); colNum++){
                Cell cell = row.createCell(colNum);
                if(i==1){
                    cell.setCellValue(titles.get(colNum));
                    cell.setCellStyle(cellStyleCenter);
                }
                if(i>1){
                    if(clazz.isAssignableFrom(Map.class)){
                        Map map = (Map) data.get(i-2);
                        if(map.containsKey(properties.get(colNum))){
                            if(map.get(properties.get(colNum))!=null){
                                cell.setCellValue(map.get(properties.get(colNum)).toString());
                            }
                        }
                    }else{
                        Field field= null;
                        try {
                            field = clazz.getDeclaredField(properties.get(colNum));
                        } catch (NoSuchFieldException e) {
                            log.error("本类没有找到。异常为："+e.getMessage());
                        }
                        if(null == field){ //本来中没有找到，在父类中去找
                            Class supClass = clazz.getSuperclass();
                            try {
                                field = supClass.getDeclaredField(properties.get(colNum));
                            } catch (NoSuchFieldException e) {
                                log.error("父类没有找到。异常为："+e.getMessage());
                            }
                        }
                        if(null != field){
                            field.setAccessible(true);
                            Object value = field.get(data.get(i-2));
                            if(null!=value){
                                cell.setCellValue(transferDataToString(value));
                            }
                        }
                    }
                    cell.setCellStyle(cellStyleLeft);
                }
            }
            if (i == 0) {
                sh.addMergedRegion(new CellRangeAddress(0, 0, 0, titles.size()-1));
                sh.getRow(i).getCell(0).setCellValue(sheetName);
                //设置单元格样式
                sh.getRow(i).getCell(0).setCellStyle(cellStyleCenter);
            }
        }
    }





    private static String transferDataToString(Object obj){
        try {
            if(obj instanceof Date){
                return sdf.format(obj);
            }
            return obj.toString();
        } catch (Exception e) {
            return "";
        }
    }
}
