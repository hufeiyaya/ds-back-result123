package com.mti.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;


import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

/**
 * @Classname JsonFileParseUtil
 * @Description TODO
 * @Date 2020/2/17 23:58
 * @Created by duchaof
 * @Version 1.0
 */
@Slf4j
public class JsonFileParseUtil {

    public static <T> T readJsonFromClassPath(String path, Type type) throws IOException {

        ClassPathResource resource = new ClassPathResource(path);
        if (resource.exists()) {
            return JSON.parseObject(resource.getInputStream(), StandardCharsets.UTF_8, type,
                    // 自动关闭流
                    Feature.AutoCloseSource,
                    // 允许注释
                    Feature.AllowComment,
                    // 允许单引号
                    Feature.AllowSingleQuotes,
                    // 使用 Big decimal
                    Feature.UseBigDecimal,
                    // 保持原来的json数据顺序
                    Feature.OrderedField);
        } else {
            throw new IOException();
        }
    }

}
