package com.mti.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Classname GetRemoteIpUtil
 * @Description TODO
 * @Date 2020/7/10 13:52
 * @Created by duchaof
 * @Version 1.0
 */
@Slf4j
@Component
public class GetRemoteIpUtil {

    public String getRemoteIP(ServerHttpRequest request) {
        // List<String> ipes1 = request.getHeaders().get("X-Real-IP");
        // List<String> ipes2 = request.getHeaders().get("x-forwarded-for");
        if (request.getHeaders().get("x-forwarded-for") == null) {
            return request.getHeaders().get("X-Real-IP").get(0);
        }
        log.info("---------------访问的ip为：======》{}",request.getHeaders().get("x-forwarded-for").get(0));
        System.out.println("---------------访问的ip为：======》"+request.getHeaders().get("x-forwarded-for").get(0));
        return request.getHeaders().get("x-forwarded-for").get(0);

    }
}
