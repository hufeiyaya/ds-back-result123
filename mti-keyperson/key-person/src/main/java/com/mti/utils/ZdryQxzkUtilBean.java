package com.mti.utils;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Classname ZdryQxzkUtilBean
 * @Description TODO
 * @Date 2020/2/13 15:54
 * @Created by duchaof
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ZdryQxzkUtilBean {

    @ApiModelProperty(hidden = true)
    private String yjlx;

    @ApiModelProperty(value = "出发省",name = "send_pro",example = "陕西省")
    private String sendPro;

    @ApiModelProperty(value = "出发市",name = "send_city",example = "西安")
    private String sendCity;

    @ApiModelProperty(value = "到达省",name = "end_pro",example = "云南省")
    private String endPro;

    @ApiModelProperty(value = "到达市",name = "end_city",example = "昆明")
    private String endCity;

    @ApiModelProperty(hidden = true)
    private Long number;

    @ApiModelProperty(value = "进出标志：1 进 2 出 null 进+出",name = "inOut",example = "1")
    private Integer inOut;

    @ApiModelProperty(value = "预警类型集合",name = "yjlxs")
    private List<String> yjlxs;

    public ZdryQxzkUtilBean(List<String> yjlxs,String sendPro,String sendCity,String endPro,String endCity){
        this.yjlxs=yjlxs;
        this.sendPro=sendPro;
        this.sendCity=sendCity;
        this.endPro=endPro;
        this.endCity=endCity;
    }

    public ZdryQxzkUtilBean(String sendPro,String sendCity,String endPro,String endCity){
        this.sendPro=sendPro;
        this.sendCity=sendCity;
        this.endPro=endPro;
        this.endCity=endCity;
    }

}
