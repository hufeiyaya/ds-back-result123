package com.mti.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Classname AddressResolutionUtil
 * @Description TODO
 * @Date 2020/3/17 18:12
 * @Created by duchaof
 * @Version 1.0
 */
@Slf4j
public class AddressResolutionUtil {

    /**
     *  地址坐标转换
     * @param addresses
     * @return
     */
    public static List<Map<String,String>> addressResolution(String...addresses){
        String regex="(?<province>[^省]+省|.+自治区)(?<city>[^自治州]+自治州|[^市]+市|[^盟]+盟|[^地区]+地区|.+区划)(?<county>[^市]+市|[^县]+县|[^旗]+旗|.+区)?(?<town>[^区]+区|.+镇)?(?<village>.*)";
        try {
            List<Map<String,String>> list=new ArrayList<Map<String,String>>();
            for(String address : addresses){
                Matcher m= Pattern.compile(regex).matcher(address);
                String province= null,city=null,county=null,town=null,village=null;
                Map<String,String> row= null;
                while(m.find()){
                    row=new LinkedHashMap<String,String>();
                    province=m.group("province");
                    row.put("province", province==null?"":province.trim());
                    city=m.group("city");
                    row.put("city", city==null?"":city.trim());
                    county=m.group("county");
                    row.put("county", county==null?"":county.trim());
                    town=m.group("town");
                    row.put("town", town==null?"":town.trim());
                    village=m.group("village");
                    row.put("village", village==null?"":village.trim());
                    row.put("detail",row.get("town")+row.get("village"));
                    list.add(row);
                }
            }
            return list;
        } catch (Exception e) {
            log.error("地址截取时发生异常：=====>{}",e.getMessage());
            //e.printStackTrace();
            return null;
        }
    }
}
