package com.mti.handler;

import com.alibaba.fastjson.JSON;
import com.mti.component.ApiResponse;
import com.mti.websocket.SocketSessionRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Flux;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 *  调度消息处理广播群发
 *
 * @author yangsp
 * @since 2019-03-22
 */
@Component
@Slf4j
public class DispatchBroadcast {

    @Autowired
    private SocketSessionRegistry sessionRegistry;

    /**
     * 给所有人发消息
     * @param excludeUserId
     * @param apiResponse
     */
    public void broadcastByAll(String excludeUserId, ApiResponse apiResponse) {
        // 获取用户session
        ConcurrentMap<String, WebSocketSession> sessions =  sessionRegistry.getAllSessionWebSocketInfos();
        for(Map.Entry<String, WebSocketSession> entry: sessions.entrySet() ){
            log.info("给所有人发消息,entry.getKey:{},entry.getValue:{},apiResponse:{}",entry.getKey(), entry.getValue(),JSON.toJSONString(apiResponse));
            WebSocketSession session = entry.getValue();
            String user = sessionRegistry.parseUserByURI(session).get("user");
            if(StringUtils.isEmpty(excludeUserId)) {
                // 返回消息
                session.send(Flux.just(session.textMessage(JSON.toJSONString(apiResponse)))).then().toProcessor();
            }else{
                if(!excludeUserId.equals(user)){
                    // 返回消息
                    session.send(Flux.just(session.textMessage(JSON.toJSONString(apiResponse)))).then().toProcessor();
                }
            }
        }
    }

    /**
     * 给会议成员发消息
     * @param userIds
     * @param apiResponse
     */
    public void broadcastByUserIds(List<String> userIds, ApiResponse apiResponse) {
        if(userIds.size() > 0){
            for (int i = 0; i < userIds.size(); i++) {

                log.info("给会议成员发消息---1,userId:{},apiResponse:{}",userIds.get(i),JSON.toJSONString(apiResponse));

                if(!StringUtils.isEmpty(userIds.get(i))){
                    // 获取用户session
                    Collection<WebSocketSession> sessions =  sessionRegistry.getSessionByUser(userIds.get(i));
                    if(!CollectionUtils.isEmpty(sessions)){
                        for (WebSocketSession session : sessions) {
                            log.info("给会议成员发消息,userId:{},apiResponse:{}",userIds.get(i),JSON.toJSONString(apiResponse));
                            // 返回消息
                            session.send(Flux.just(session.textMessage(JSON.toJSONString(apiResponse)))).then().toProcessor();
                        }
                    }
                }
            }
        }
    }
}

