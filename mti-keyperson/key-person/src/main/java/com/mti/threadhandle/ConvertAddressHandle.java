package com.mti.threadhandle;

import com.mti.dao.model.LfZdryBaseEntity;
import com.mti.service.ISpecialPersonService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @Classname ConvertAddressHandle
 * @Description TODO
 * @Date 2020/5/7 20:51
 * @Created by duchaof
 * @Version 1.0
 */
@Slf4j
@Data
@AllArgsConstructor
public class ConvertAddressHandle implements Runnable{

    private LfZdryBaseEntity zdry;

    private ISpecialPersonService iSpecialPersonService;


    @Override
    public void run() {
        try {
            //iSpecialPersonService.updateAddress(zdry);
        } catch (Exception e) {
            log.error("地址转换经纬度是发生异常："+e.getMessage());
        }
    }
}
