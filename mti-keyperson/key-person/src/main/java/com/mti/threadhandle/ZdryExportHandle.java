package com.mti.threadhandle;

import com.mti.dao.model.LfZdryEntity;
import com.mti.dao.model.SysDictEntity;
import com.mti.service.ISysDictService;
import com.mti.utils.DateUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @Classname ZdryExportHandle
 * @Description TODO
 * @Date 2020/5/14 16:08
 * @Created by duchaof
 * @Version 1.0
 */
@Data
@Slf4j
@AllArgsConstructor
public class ZdryExportHandle implements Runnable {

    private SXSSFSheet sh;

    private CellStyle cellStyleCenter;

    private CellStyle cellStyleLeft;

    private List<LfZdryEntity> list;

    private String date;

    private String exportName;

    private ISysDictService iSysDictService;

    private CountDownLatch downLatch;

    private CellStyle cellStyFont;

    @Override
    public void run() {
        try {
            LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            int totalSize = 4 + list.size();
            for (int rowNum = 0; rowNum < totalSize; rowNum++) {
                Row row = sh.createRow(rowNum);

                for (int colNum = 0; colNum < 29; colNum++) {
                    Cell cell1 = row.createCell(colNum);
                    //设置单元格样式
                    cell1.setCellStyle(cellStyleLeft);
                    String title = "";
                    if (rowNum == 3) {
                        switch (colNum) {
                            case 0: {
                                title = "序号";
                            }
                            break;
                            case 1: {
                                title = "姓名";
                            }
                            break;
                            case 2: {
                                title = "身份证号码";
                            }
                            break;
                            case 3: {
                                title = "电话号码";
                            }
                            break;
                            case 4: {
                                title = "微信号码";
                            }
                            break;
                            case 5: {
                                title = "QQ号码";
                            }
                            break;
                            case 6: {
                                title = "名下车辆号牌";
                            }
                            break;
                            case 7: {
                                title = "实际使用车辆号牌";
                            }
                            break;
                            case 8: {
                                title = "管控责任分局";
                            }
                            break;
                            case 9: {
                                title = "责任派出所";
                            }
                            break;
                            case 10: {
                                title = "责任民警";
                            }
                            break;
                            case 11: {
                                title = "责任民警电话";
                            }
                            break;
                            case 12: {
                                title = "户籍省";
                            }
                            break;
                            case 13: {
                                title = "户籍市";
                            }
                            break;
                            case 14: {
                                title = "户籍县";
                            }
                            break;
                            case 15: {
                                title = "户籍详址";
                            }
                            break;
                            case 16: {
                                title = "实际居住省";
                            }
                            break;
                            case 17: {
                                title = "实际居住市";
                            }
                            break;
                            case 18: {
                                title = "实际居住县";
                            }
                            break;
                            case 19: {
                                title = "实际居住详址";
                            }
                            break;
                            case 20: {
                                title = "人员大类";
                            }
                            break;
                            case 21: {
                                title = "人员细类";
                            }
                            break;
                            case 22: {
                                title = "人员属性";
                            }
                            break;
                            case 23: {
                                title = "风险等级";
                            }
                            break;
                            case 24: {
                                title = "预警处置类别";
                            }
                            break;
                            case 25: {
                                title = "稳控状态";
                            }
                            break;
                            case 26: {
                                title = "备注";
                            }
                            break;
                            case 27:{
                                title = "所属派出所编号";
                            }break;
                            case 28:{
                                title = "诉求";
                            }break;
                        }
                        cell1.setCellValue(title);
                    } else {
                        if (rowNum > 3) {

                            LfZdryEntity entity = list.get(rowNum - 4);
                            if (colNum == 0)
                                cell1.setCellValue(rowNum - 3);
                            else {
                                switch (colNum) {
                                    case 1: {
                                        cell1.setCellValue(entity.getXm());
                                    }
                                    break;
                                    case 2: {
                                        cell1.setCellValue(entity.getSfzh());
                                    }
                                    break;
                                    case 3: {
                                        cell1.setCellValue(entity.getSjh());
                                    }
                                    break;
                                    case 4: {
                                        cell1.setCellValue(entity.getWx());
                                    }
                                    break;
                                    case 5: {
                                        cell1.setCellValue(entity.getQq());
                                    }
                                    break;
                                    case 6: {
                                        cell1.setCellValue(entity.getMxcph());
                                    }
                                    break;
                                    case 7: {
                                        cell1.setCellValue(entity.getSjsyclhp());
                                    }
                                    break;
                                    case 8: {
                                        cell1.setCellValue(entity.getSsxq());
                                    }
                                    break;
                                    case 9: {
                                        cell1.setCellValue(entity.getSspcs());
                                    }
                                    break;
                                    case 10: {
                                        cell1.setCellValue(entity.getGkmj());
                                    }
                                    break;
                                    case 11: {
                                        cell1.setCellValue(entity.getMjlxdh());
                                    }
                                    break;
                                    case 12: {
                                        cell1.setCellValue(entity.getHjs());
                                    }
                                    break;
                                    case 13: {
                                        cell1.setCellValue(entity.getHjsS());
                                    }
                                    break;
                                    case 14: {
                                        cell1.setCellValue(entity.getHjx());
                                    }
                                    break;
                                    case 15: {
                                        cell1.setCellValue(entity.getHjd());
                                    }
                                    break;
                                    case 16: {
                                        cell1.setCellValue(entity.getXzzs());
                                    }
                                    break;
                                    case 17: {
                                        cell1.setCellValue(entity.getXzzsS());
                                    }
                                    break;
                                    case 18: {
                                        cell1.setCellValue(entity.getXzzx());
                                    }
                                    break;
                                    case 19: {
                                        cell1.setCellValue(entity.getXzz());
                                    }
                                    break;
                                    case 20: {
                                        cell1.setCellValue(entity.getRylbx());
                                    }
                                    break;
                                    case 21: {
                                        cell1.setCellValue(entity.getXl());
                                    }
                                    break;
                                    case 22: {
                                        SysDictEntity sysDictEntity = iSysDictService.getEntityByKey("zdry_ryxx", String.valueOf(entity.getRysx()));
                                        if (null != sysDictEntity) {
                                            cell1.setCellValue(sysDictEntity.getValue());
                                        } else
                                            cell1.setCellValue("");
                                    }
                                    break;
                                    case 23: {
                                        cell1.setCellValue(entity.getGkjbx());
                                    }
                                    break;
                                    case 24: {
                                        SysDictEntity sysDictEntity = iSysDictService.getEntityByKey("zdry_yjczlb", String.valueOf(entity.getYjczlb()));
                                        if (null != sysDictEntity) {
                                            cell1.setCellValue(sysDictEntity.getValue());
                                        } else
                                            cell1.setCellValue("");
                                    }
                                    break;
                                    case 25: {
                                        cell1.setCellValue(entity.getWkzt());
                                    }
                                    break;
                                    case 26: {
                                        cell1.setCellValue(entity.getBz());
                                    }
                                    break;
                                    case 27:{
                                        cell1.setCellValue(entity.getOid());
                                    }break;
                                    case 28:{
                                        cell1.setCellValue(entity.getAppeal());
                                    }break;
                                }
                            }
                        } else

                            cell1.setCellValue("");
                    }

                }

                if (rowNum == 0) {
                    sh.addMergedRegion(new CellRangeAddress(0, 0, 0, 28));
                    sh.getRow(rowNum).getCell(0).setCellValue(StringUtils.isEmpty(exportName) ? "重点人员(" + localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ")" : exportName);
                    //设置单元格样式
                    sh.getRow(rowNum).getCell(0).setCellStyle(cellStyleCenter);
                }
                if (rowNum == 1) {

                    sh.addMergedRegion(new CellRangeAddress(1, 2, 0, 3));
                    sh.getRow(rowNum).getCell(0).setCellValue("填报日期： " + DateUtils.parseStrDateToChineseDate(date));
                }
                if (rowNum == 2) {
                    //sh.addMergedRegion(new CellRangeAddress(2,3,0,3));
                    //sh.getRow(rowNum).getCell(0).setCellValue("本单位涉及人员" + list.size() + "名：在控 " + zkCount + " 人，失控 " + skCount + " 人，未上报 " + wsbCount + " 人");
                    sh.getRow(1).getCell(4).setCellValue("单位负责人签字：");
                    sh.addMergedRegion(new CellRangeAddress(1, 2, 4, 9));
                }
            }
            for (int i = 0; i < totalSize; i++) {
                sh.autoSizeColumn(i, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("给execl写入数据时发生异常：========》{}",e.getMessage());
        } finally {
            downLatch.countDown();
        }
    }
}
