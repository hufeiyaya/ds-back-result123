package com.mti.threadhandle;

import com.mti.dao.model.LfZdryExportDataEntity;
import com.mti.service.LfZdryExportDataService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 * @Classname SaveExeclHandle
 * @Description TODO
 * @Date 2020/4/8 20:03
 * @Created by duchaof
 * @Version 1.0
 */
@Slf4j
@Data
@AllArgsConstructor
public class SaveExeclHandle implements Callable<Boolean> {

    private CountDownLatch latch;

    private List<LfZdryExportDataEntity> tempData;

    private LfZdryExportDataService lfZdryExportDataService;

    @Override
    public Boolean call() throws Exception {
        try {
            boolean flag =lfZdryExportDataService.batchSaveZdry(tempData);
            return flag;
        } catch (Exception e) {
            throw e;
        }finally {
            latch.countDown();
        }
    }
}
