package com.mti.threadhandle;

import com.mti.service.impl.SpecialPersonServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Sheet;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 * @Classname CheckExeclHandle
 * @Description TODO
 * @Date 2020/4/8 19:48
 * @Created by duchaof
 * @Version 1.0
 */
@Slf4j
@Data
@AllArgsConstructor
public class CheckExeclHandle implements Callable<List<Map<String, Object>>> {

    private int start;

    private int end;

    private Sheet sheet;

    private CountDownLatch checkLatch;

    private SpecialPersonServiceImpl specialPersonService;

    private Map<String,List<String>> checkParam;


    @Override
    public List<Map<String, Object>> call() throws Exception {
        List<Map<String, Object>> returnList = new ArrayList<>();
        try {
            if(start<end){
                for(int i=start;i<end;i++){
                    //log.info("当前线程"+Thread.currentThread().getName()+"校验到第：【"+i+"】 行");
                    Map<String,Object> tempData =specialPersonService.checkData(sheet,i,checkParam);
                    returnList.add(tempData);
                }
            }
            return returnList;
        } catch (ParseException e) {
            throw e;
        } finally {
            checkLatch.countDown();
        }
     }
}


