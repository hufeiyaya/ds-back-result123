package com.mti.enums;

/**
 * 在线状态
 */
public enum OnLineStatus {
    /**
     * 在线
     */
    ONLINE("ONLINE","在线",1),
    /**
     * 离线
     */
    OFFLINE("OFFLINE","离线",2);
    OnLineStatus(String name, String desc, Integer status){
        this.name=name;
        this.desc=desc;
        this.status = status;
    }

    private String name;
    private String desc;
    private Integer status;
    public String getName() {
        return name;
    }
    public String getDesc() {
        return desc;
    }
    public Integer getStatus(){
        return status;
    }
}
