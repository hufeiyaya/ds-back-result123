package com.mti.enums;

/**
 * 接收标记
 */
public enum ReceiveFlag {

    /**
     * 未接收
     */
	NOT_RECEIVED("NOT_RECEIVED","未接收",1),
    /**
     * 已接收
     */
	RECEIVED("RECEIVED","已接收",0);
    ReceiveFlag(String name, String desc ,Integer status){
        this.name=name;
        this.desc=desc;
        this.status = status;
    }
    private String name;
    private String desc;
    private Integer status;
    public String getName() {
        return name;
    }
    public String getDesc() {
        return desc;
    }
    public Integer getStatus(){
        return status;
    }
}
