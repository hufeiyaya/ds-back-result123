package com.mti.enums;

/**
 * @Classname ZdryAssistType
 * @Description TODO
 * @Date 2020/3/31 18:29
 * @Created by duchaof
 * @Version 1.0
 * 1电话号码，2微信号码，3qq号码，4名下车辆号牌，5实际车辆号牌
 */
public enum ZdryAssistType {
    TEL("电话号码",1),
    WX("微信号码",2),
    QQ("qq号码",3),
    MXCPH("名下车牌号",4),
    SJCPH("实际车牌号",5)
    ;

    private String title;
    private Integer type;

    ZdryAssistType(String title, Integer type) {
        this.title = title;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
