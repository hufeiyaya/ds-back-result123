package com.mti.enums;

/**
 * 结果状态
 */
public enum ResultStatus {

    /**
     * 正常
     */
    OK("OK","获取结果正常",1),
    /**
     * 错误
     */
    ERROR("ERROR","获取结果错误",0);
    ResultStatus(String name, String desc ,Integer status){
        this.name=name;
        this.desc=desc;
        this.status = status;
    }
    private String name;
    private String desc;
    private Integer status;
    public String getName() {
        return name;
    }
    public String getDesc() {
        return desc;
    }
    public Integer getStatus(){
        return status;
    }
}
