package com.mti.enums;

public enum ProcessType
{
	//流程类型(0：数据导入、基本信息变更，1:新增，2:风险等级变更,3:移交至分局，30：移交分局驳回，31：分局指定派出所，
	//321：市局警种驳回，322市局警种指定分局，323市局警种上报
  DATA_IMPORT( 0 ,"数据导入/基本信息变更" ), 
  DATA_ADD(1,"派出所新增"  ), 
  DATA_CHANGE(2 ,"风险等级变更"), 
  DATA_TRANSFER_BRATCH( 3,"移交至分局"),
  DATA_REJECT_BRATCH( 30 ,"移交分局驳回"),
  DATA_BRATCH_TO_POLICE_STATION( 31,"分局指定派出所" ), 
  DATA_POLICE_CATEGORY(321,"市局警种驳回"), 
  DATA_POLICE_CATEGORY_TO_BRATCH(322,"市局警种指定分局"),
  DATA_EXPORT( 323,"市局警种撤控" );
  
  private String title;
  private Integer type;
  
  private ProcessType( Integer type,String title)
  {
    this.title = title;
    this.type = type;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public Integer getType()
  {
    return this.type;
  }
  
  public static String getTitle(Integer value) {  
	  ProcessType[] processTypes = values();  
        for (ProcessType processType : processTypes) {  
            if (processType.type.equals(value)) {  
                return processType.title;  
            }  
        }  
        return null;  
    } 
  
}

