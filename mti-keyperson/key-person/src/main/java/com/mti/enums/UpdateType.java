package com.mti.enums;
 
public enum UpdateType {
	//辅助信息修改、基本信息修改 、管控信息修改 
     
	ASSIST_INFO(1,"辅助信息修改"),
	BASE_INFO(2,"基本信息修改"),
	CONTROL_INFO(3,"管控信息修改");
	
	//可以看出这在枚举类型里定义变量和方法和在普通类里面定义方法和变量没有什么区别。
	//唯一要注意的只是变量和方法定义必须放在所有枚举值定义的后面，否则编译器会给出一个错误。
	private int code;
	private String desc;
	
	public int getCode() {
		return code;
	}
 
	public void setCode(int code) {
		this.code = code;
	}
 
	public String getDesc() {
		return desc;
	}
 
	public void setDesc(String desc) {
		this.desc = desc;
	}
 
	
	
	UpdateType(int code,String desc){ //加上public void 上面定义枚举会报错 The constructor Color(int, String) is undefined
		this.code=code;
		this.desc=desc;
		
	}
	
	
	/**
	 * 自己定义一个静态方法,通过code返回枚举常量对象
	 * @param code
	 * @return
	 */
	public static UpdateType getValue(int code){
		
		for (UpdateType updateType : values()) {
			if(updateType.getCode()== code){
				return updateType;
			}
		}
		return null;
	}
 
	
}