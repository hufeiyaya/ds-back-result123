package com.mti.enums;


public enum RoleType
{
  ADMIN_ORG_DATA("管理员", "0"),  
  OWNER_ADMIN_ORG_DATA("市局情报中心", "1"), 
  OWNER_REVIEW_DATA("市局警种审核", "2"),  
  BREACH_REVIEW_DATA("分局审核", "3"),
  POLICE_STATION_DATA("派出所", "4"),
  OWNER_ORG_DATA("市局警种", "5"),  
  BREACH_DATA("分局", "6");
  
  private String title;
  private String type;
  
  private RoleType(String title, String type)
  {
    this.title = title;
    this.type = type;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public String getType()
  {
    return this.type;
  }
}

