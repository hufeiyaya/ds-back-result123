package com.mti.enums;

/**
 * 审核状态
 */
public enum ApproveStatus {
    /**
     * 重点人员审批状态(0：已驳回 1：待分局审核 :2：待警种审核 3：待情报中心审核 4：审核完毕  ||0：待分局审批，1：待警种审核，2：已驳回，3：已完成, 4：上报)
     */
	REJECT_REVIEWED("REJECT_REVIEWED","已驳回","0"),
    /**
     * 待警种审核
     */
	BRANCH_REVIEWED("BRANCH_REVIEWED","待分局审核/已驳回至分局","1"),
    /**
     * 待警种审核
     */
	POLICE_REVIEWED("POLICE_REVIEWED","待警种审核","2"), 
    /**
     * 情报中心
     */
	INFO_CENTER_REVIEWED("INFO_CENTER_REVIEWED","情报中心审核","3"),
    /**
     * 审核完毕
     */
	COMPLETED("COMPLETED","审核完毕","4"),
	
	/**
     * 非完成
     */
	NOT_COMPLETED("NOT_COMPLETED","非完成","30");
    ApproveStatus(String name, String desc, String status){
        this.name=name;
        this.desc=desc;
        this.status = status;
    }

    private String name;
    private String desc;
    private String status;
    public String getName() {
        return name;
    }
    public String getDesc() {
        return desc;
    }
    public String getStatus(){
        return status;
    }
}
