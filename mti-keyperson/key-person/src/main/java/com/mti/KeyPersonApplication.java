package com.mti;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeyPersonApplication {

    public static void main(String[] args) {
        SpringApplication.run(KeyPersonApplication.class,args);
    }

}

