package com.mti.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ExceptionsHandler {

    @ExceptionHandler(value = Exception.class)
    public Exception exception(Exception e) {
        e.printStackTrace();
        log.error("发生了未知异常", e);
        return e;
    }
}
