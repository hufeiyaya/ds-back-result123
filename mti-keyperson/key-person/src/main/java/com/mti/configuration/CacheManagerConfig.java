//package com.mti.configuration;
//
//import org.springframework.boot.autoconfigure.cache.CacheManagerCustomizer;
//import org.springframework.cache.Cache;
//import org.springframework.cache.CacheManager;
//import org.springframework.cache.annotation.EnableCaching;
//import org.springframework.cache.concurrent.ConcurrentMapCache;
//import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
//import org.springframework.cache.support.SimpleCacheManager;
//import org.springframework.context.annotation.Bean;
//import org.springframework.stereotype.Component;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @ClassName CacheManagerConfig
// * @Description 配置缓存
// * @Author zhaoyj
// * @Date 2019/3/14 10:05
// */
//@Component
//@EnableCaching
//public class CacheManagerConfig {
//
//    @Bean
//    public CacheManager configCacheManager(){
//        List<Cache> caches = new ArrayList<>();
//        caches.add(new ConcurrentMapCache("user"));
//        SimpleCacheManager manager = new SimpleCacheManager();
//        manager.setCaches(caches);
//        return manager;
//    }
//
//    @Bean
//    public CacheManagerCustomizer<ConcurrentMapCacheManager> cacheManagerCustomizer() {
//        return cacheManager -> cacheManager.setAllowNullValues(false);
//    }
//}
