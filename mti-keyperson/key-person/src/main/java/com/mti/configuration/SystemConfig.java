package com.mti.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @company 上海道枢信息科技-->
 * @anthor created by zhangmingxin
 * @date 2019/8/15
 * @change
 * @describe describe
 **/
@Data
@Component
@AllArgsConstructor
@NoArgsConstructor
public class SystemConfig {
    @Value("${zdry.syncCron}")
    private String syncCon;
    @Value("${zdry.locationServer}")
    private String locationServer;
    @Value("${zdry.locationEnable}")
    private String locationEnable;
    @Value("${zdry.restTemplateTimeOut}")
    private Integer restTemplateTimeOut;
    @Value("${zdry.systemServer}")
    private String systemServer;
    @Value("${zdry.addressIp}")
    private String addressIp;
    @Value("${zdry.stAddressIp}")
    private String stAddressIp;
    @Value("${zdry.stUserName}")
    private String stUserName;
    @Value("${zdry.fileDownloadIp}")
    private String fileDownloadIp;
    @Value("${zdry.yjHandleFackUrl}")
    private String yjHandleFackUrl;
}
