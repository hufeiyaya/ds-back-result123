package com.mti.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.*;

/**
 * @Classname ExecutorConfig
 * @Description TODO
 * @Date 2020/3/10 14:54
 * @Created by duchaof
 * @Version 1.0
 */
@Configuration
@EnableAsync
public class ExecutorConfig {
    @Bean
    public ExecutorService executorService(){
        ExecutorService pool = new ThreadPoolExecutor(
               6,
               10,
                1000 * 60,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>());
        return pool;
    }

}
