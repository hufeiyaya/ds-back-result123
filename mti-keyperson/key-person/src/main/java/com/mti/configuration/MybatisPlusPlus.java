package com.mti.configuration;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.incrementer.IKeyGenerator;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.incrementer.PostgreKeyGenerator;
import com.baomidou.mybatisplus.extension.injector.LogicSqlInjector;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Statement;

/**
 * MybatisPlusPlus class
 *
 * @author zhaoyj
 * @date 2019/3/12
 */
@Configuration
@Slf4j
public class MybatisPlusPlus {

    @Bean
    public ISqlInjector sqlInjector() {
        return new LogicSqlInjector();
    }
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        paginationInterceptor.setDialectType(DbType.POSTGRE_SQL.getDb());
        return paginationInterceptor;
    }

    @Bean
    public IKeyGenerator getPostKeyGenerator(){
        return new PostgreKeyGenerator();
    }

    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer scannerConfigurer = new MapperScannerConfigurer();
        scannerConfigurer.setBasePackage("com.mti.**.mapper*");
        return scannerConfigurer;
    }
    @Bean
    public PerformanceInterceptor performanceInterceptor(){
        PerformanceInterceptor performanceInterceptor =  new SqlInterceptor();
        performanceInterceptor.setFormat(false);
        return performanceInterceptor;
    }

    @Intercepts({
            @Signature(type = StatementHandler.class, method = "query", args = {Statement.class, ResultHandler.class}),
            @Signature(type = StatementHandler.class, method = "update", args = {Statement.class}),
            @Signature(type = StatementHandler.class, method = "batch", args = {Statement.class})
    })
    class SqlInterceptor extends PerformanceInterceptor {

        @Override
        public Object intercept(Invocation invocation) throws Throwable {
            Object obj = super.intercept(invocation);
            Object o =invocation.getArgs()[0];
            com.alibaba.druid.pool.DruidPooledPreparedStatement druidPooledPreparedStatement = (com.alibaba.druid.pool.DruidPooledPreparedStatement) o;
            String sql = String.valueOf(druidPooledPreparedStatement.getStatement()).replaceAll("[\\s]+", StringPool.SPACE) + "";
            log.debug(sql);
            System.err.println(sql);
            return obj;
        }
    }

}
