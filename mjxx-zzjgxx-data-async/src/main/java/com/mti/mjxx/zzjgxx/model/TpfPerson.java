package com.mti.mjxx.zzjgxx.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * Created by admin on 2020/6/29.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_pf_person")
public class TpfPerson {
    @TableId
    @TableField("id_")
    private String id_;

    @TableField("number_")
    private String number_;

    @TableField("name_")
    private String name_;

    @TableField("police_code_")
    private String police_code_;

    @TableField("credentials_type_")
    private String credentials_type_;

    @TableField("credentials_number_")
    private String credentials_number_;

    @TableField("sex_")
    private int sex_;

    @TableField("birth_date_")
    private Timestamp birth_date_;

    @TableField("address_")
    private String address_;

    @TableField("security_")
    private String security_;

    @TableField("version_")
    private String version_;

    @TableField("available_")
    private int available_;

    @TableField("phone_")
    private String phone_;

    @TableField("email_")
    private String email_;

    @TableField("creator_id_")
    private String creator_id_;

    @TableField("main_duty_")
    private String main_duty_;

    @TableField("delete_")
    private int delete_;

    @TableField("avator_")
    private String avator_;

    @TableField("create_time_")
    private Timestamp create_time_;

    @TableField("update_time_")
    private Timestamp update_time_;

}
