package com.mti.mjxx.zzjgxx.component.consumer;


import java.util.List;
import java.util.Map;

/**
 * create by zhangmingxin
 * 数据消费接口类,主要处理消费数据逻辑
 */
public interface DataConsumerProcessor {
    void onConsume(List<Map<String,Object>> data,String tableName);
}
