
package com.mti.mjxx.zzjgxx.controller;


import com.daoshu.system.model.view.ResponseResult;
import com.mti.mjxx.zzjgxx.service.TpfPersonImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * test
 */
@Api(value = "mjxx-zzjgxx", description = "mjxx-zzjgxx")
@RestController
@RequestMapping("/api/mjxx")
@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
public class MjxxZzjgxxController {

    @Autowired
    private TpfPersonImpl tpfPerson;

    @ApiOperation(value = "民警信息同步", notes = "民警信息同步")
    @RequestMapping(value = "/mjxxUpdate", method = RequestMethod.GET)
    public ResponseResult<Object> getById() {
        try {
            tpfPerson.seleectAllPerson();
            return ResponseResult.success();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseResult.fail(99999, "民警信息同步失败！", null);
        }
    }

    @ApiOperation(value = "组织机构信息同步", notes = "组织机构信息同步")
    @RequestMapping(value = "/zzjgxxUpdate", method = RequestMethod.GET)
    public ResponseResult<Object> updateOrganization() {
        try {
            tpfPerson.updateZZJGXX();
            return ResponseResult.success();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseResult.fail(99999, "民警信息同步失败！", null);
        }

    }

}
