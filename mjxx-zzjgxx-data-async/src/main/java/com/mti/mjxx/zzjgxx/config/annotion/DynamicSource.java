package com.mti.mjxx.zzjgxx.config.annotion;

import java.lang.annotation.*;

/**
 * <p>
 *     动态配置多数据源元注解
 * </p>
 *
 * @author yangsp
 * @since 2018/3/19
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
@Documented
public @interface DynamicSource {

    String value() default "";
}
