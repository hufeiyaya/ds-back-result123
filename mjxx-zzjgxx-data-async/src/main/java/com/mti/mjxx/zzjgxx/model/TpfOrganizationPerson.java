package com.mti.mjxx.zzjgxx.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by admin on 2020/7/1.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_pf_organization_person")
public class TpfOrganizationPerson {

    @TableId
    @TableField("id_")
    private String id_;

    @TableField("org_id_")
    private String org_id_;

    @TableField("person_id_")
    private String person_id_;

    @TableField("position_name_")
    private String position_name_;

    @TableField("main_position_")
    private int main_position_;

    @TableField("leader_")
    private int leader_;

    @TableField("available_")
    private int available_;

    @TableField("sort_")
    private int sort_;

    @TableField("position_sort_")
    private int position_sort_;

    @TableField("delete_")
    private int delete_;

    @TableField("person_name_")
    private String person_name_;
}
