package com.mti.mjxx.zzjgxx.service;

import com.mti.mjxx.zzjgxx.config.annotion.DynamicSource;
import com.mti.mjxx.zzjgxx.mapper.SelectTGASTMjxxMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class TGASTMjxxServiceImpl {

    @Resource
    private SelectTGASTMjxxMapper selectTGASTMjxxMapper;

    @DynamicSource
    public List<Map<String,Object>> getTGASTMjxx( ) {
            List<Map<String,Object>> qgztryjbxx = selectTGASTMjxxMapper.selectTGASTMjxx();
        return qgztryjbxx;
    }
    @DynamicSource
    public List<Map<String,Object>> getTGASTZzjgxx( ) {
        List<Map<String,Object>> listTGASTZzjgxx = selectTGASTMjxxMapper.selectTGASTZzjgxx();
        return listTGASTZzjgxx;
    }

}
