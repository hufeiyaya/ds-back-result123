//package com.mti.ztry.controller;
//
//import ResourceOperate;
//import Constants;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import lombok.AllArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
////import com.mti.message.component.producer.ProducerOperator;
////import lombok.AllArgsConstructor;
//
//@Api
//@Slf4j
//@RestController
//@AllArgsConstructor
//@RequestMapping(value = "/async")
//public class AsyncDataController {
//    private final ResourceOperate resourceOperate;
////    @Autowired
////    private final ProducerOperator producer;
//
//    @ApiOperation(value = "数据同步接口")
//    @GetMapping
//    public ResponseEntity<String> aysncData(@RequestParam(value = "start")@ApiParam(value = "开始时间(yyyy-MM-dd HH:mm:ss)",name = "start") String start, @RequestParam(value = "end")@ApiParam(value = "结束时间(yyyy-MM-dd HH:mm:ss)",name = "end") String end, @RequestParam(value = "tableName")@ApiParam(value = "同步表名",name="tableName") String tableName){
//        try {
//            String[] tables = tableName.split(",");
//            for (String table : tables) {
//                resourceOperate.getResourceData(start,end, Constants.ORACLE_DATE_TIME_FORMAT,table);
//            }
//            return ResponseEntity.ok("Async Data Success");
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.ok("Async Data Failure");
//        }
//    }
//
////    @GetMapping(value = "/queue")
////    public ResponseEntity<String> send(@RequestParam(value = "msg")String msg){
////        producer.sendMsg("",msg);
////        return ResponseEntity.ok("success");
////    }
//
//
//
//
//
//}
