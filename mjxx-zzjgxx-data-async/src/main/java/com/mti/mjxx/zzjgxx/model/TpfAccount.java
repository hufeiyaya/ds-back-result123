package com.mti.mjxx.zzjgxx.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by admin on 2020/6/30.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_pf_account")
public class TpfAccount implements Serializable{
    @TableId
    @TableField("id_")
    private String id_;

    @TableField("login_name_")
    private String login_name_;

    @TableField("password_")
    private String password_;

    @TableField("create_time_")
    private Timestamp create_time_;

    @TableField("creator_id_")
    private String creator_id_;

    @TableField("person_id_")
    private String person_id_;

    @TableField("description_")
    private String description_;

    @TableField("org_id_")
    private String org_id_;

    @TableField("available_")
    private int available_;

    @TableField("statu_")
    private int statu_;

    @TableField("delete_")
    private int delete_;

}
