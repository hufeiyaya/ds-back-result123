//package com.mti.ztry.component.consumer;
//
//import WebApplicationContext;
//import DynamicSource;
//import Constants;
//import TransBaseMapper;
//import com.mti.ztry.service.IZtryService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.core.task.TaskExecutor;
//import org.springframework.stereotype.Component;
//import org.springframework.util.StringUtils;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
///**
// * create by zhangmingxin
// * 目标数据消费，各尽其职
// */
//@Slf4j
//@Component
//public class DataConsumer implements DataConsumerProcessor {
//    @Autowired
//    private WebApplicationContext context;
//    @Autowired
//    private IZtryService ztryService;
//    @Autowired
//    private TaskExecutor taskExecutor;
//    @Value("${dataRsync.async-tablename}")
//    private String masterTable;
//
//    private static final Object monitor = new Object();
//
//    @Override
//    @DynamicSource(value = "stationDataSource")
//    public void onConsume(List<Map<String, Object>> data, String tableName) {
//        synchronized (monitor){
//            TransBaseMapper transBaseMapper = (TransBaseMapper) context.getBeanByName(masterTable);
//            //将源数据入目标库
//            boolean isExist = transBaseMapper.getTableCount(masterTable)>0;
//            if(isExist){
//                //保存数据到target表
//                transBaseMapper.saveByBatch(data);
//            }else
//                log.warn("===>target table:{} is not exist",masterTable);
//        }
//    }
//
//
//
//    private Date toDate(String date){
//        if(StringUtils.isEmpty(date))
//            return new Date();
//        try {
//            return new SimpleDateFormat(Constants.DATE_TIME_FORMAT).parse(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//}
