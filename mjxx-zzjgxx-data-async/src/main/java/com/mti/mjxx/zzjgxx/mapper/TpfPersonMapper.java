package com.mti.mjxx.zzjgxx.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mti.mjxx.zzjgxx.model.TpfPerson;

import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2020/6/29.
 */
public interface TpfPersonMapper extends BaseMapper<TpfPerson>, TransBaseMapper {
    List<Map<String, Object>> selectAll();
}
