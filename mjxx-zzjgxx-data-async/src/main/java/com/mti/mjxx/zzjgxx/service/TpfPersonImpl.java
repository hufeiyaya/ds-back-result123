package com.mti.mjxx.zzjgxx.service;

import com.mti.mjxx.zzjgxx.mapper.TpfAccountMapper;
import com.mti.mjxx.zzjgxx.mapper.TpfOrganizationMapper;
import com.mti.mjxx.zzjgxx.mapper.TpfOrganizationPersonMapper;
import com.mti.mjxx.zzjgxx.mapper.TpfPersonMapper;
import com.mti.mjxx.zzjgxx.model.TpfAccount;
import com.mti.mjxx.zzjgxx.model.TpfOrganization;
import com.mti.mjxx.zzjgxx.model.TpfOrganizationPerson;
import com.mti.mjxx.zzjgxx.model.TpfPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2020/6/29.
 */
@Service
public class TpfPersonImpl {
    @Autowired
    private TpfPersonMapper tpfPersonMapper;

    @Autowired
    private TpfOrganizationMapper tpfOrganizationMapper;

    @Autowired
    private TpfAccountMapper tpfAccountMapper;

    @Autowired
    private TpfOrganizationPersonMapper tpfOrganizationPersonMapper;

    @Autowired
    private TGASTMjxxServiceImpl tgastMjxxServiceImpl;

    /**
     * 人员信息定时更新
     *
     * @return
     */
    public void seleectAllPerson() {
        //先查询公安的全部民警信息
        List<Map<String, Object>> remoteMjxx = tgastMjxxServiceImpl.getTGASTMjxx();

        //本地库中的全部民警信息
        List<Map<String, Object>> localMjxx = tpfPersonMapper.selectAll();

        TpfPerson tpfPerson = new TpfPerson();

        TpfOrganizationPerson tpfOrganizationPerson = new TpfOrganizationPerson();

        TpfAccount tpfAccount = new TpfAccount();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        boolean flag;
        //遍历公安民警信息与本地数据库中人员信息比对（无-添加，有-更新）
        for (Map remoteMap : remoteMjxx) {
            flag = false;
            loo:
            for (Map localMap : localMjxx) {
                //根据民警编号和姓名进行比对

                if (remoteMap.get("id_").equals(localMap.get("id_")) && remoteMap.get("name_").equals(localMap.get("name_"))) {
                    //将从远程库拿到的Map数据转换成Bean实体
                    TpfPerson myTpfPerson = (TpfPerson) this.objectToMap(tpfPerson, remoteMap);
                    myTpfPerson.setUpdate_time_(Timestamp.valueOf(sdf.format(new Date())));
                    //更新t_pf_person 数据
                    tpfPersonMapper.updateById(myTpfPerson);

                    //查询账号
                    //当人员已经存在 判断 t_pf_account是否存在，如果不存在则新增，存在则更新
                    if (localMap.get("police_code_") != null && !"null".equals(localMap.get("police_code_"))) {
                        List<Map<String, Object>> tpfAccountList = tpfAccountMapper.selectByIdTpfAccount((String) localMap.get("police_code_"));
                        if (tpfAccountList.size() > 0) {
                            Map<String, Object> accMap = tpfAccountList.get(0);
                            //判断人员对应的机构ID是否需要更新
                            tpfAccount.setId_((String) accMap.get("id_"));
                            tpfAccount.setOrg_id_((String) remoteMap.get("org_id_"));
                            tpfAccount.setLogin_name_((String) remoteMap.get("police_code_"));
                            tpfAccount.setDescription_((String) remoteMap.get("name_"));
                            tpfAccount.setPerson_id_((String) remoteMap.get("id_"));
                            //更新账号信息
                            tpfAccountMapper.updateById(tpfAccount);
                        } else {
                            TpfAccount tpfAccountNew = (TpfAccount) this.objectToMapAccount(tpfAccount, remoteMap);
                            tpfAccountMapper.insert(tpfAccountNew);
                        }
                    }

                    //查询人员和组织机构对应关系
                    //当人员已经存在 判断 t_pf_organization_person关系是否存在，如果不存在则新增，存在则更新
                    List<Map<String, Object>> tpfOrganizationPersonList = tpfOrganizationPersonMapper.selectByPersonId((String) localMap.get("id_"));
                    if (tpfOrganizationPersonList.size() > 0) {
                        Map<String, Object> orgPersonMap = tpfOrganizationPersonList.get(0);
                        //判断人员的组织机构是否变更
                        if (!orgPersonMap.get("org_id_").equals(remoteMap.get("org_id_"))) {
                            tpfOrganizationPerson.setId_((String) orgPersonMap.get("id_"));
                            tpfOrganizationPerson.setOrg_id_((String) remoteMap.get("org_id_"));
                            //更新组织机构对应关系
                            tpfOrganizationPersonMapper.updateById(tpfOrganizationPerson);
                        }
                    } else {
                        TpfOrganizationPerson tpfOrganizationPersonNew = (TpfOrganizationPerson) this.objectToMapTpfOrganizationPerson(tpfOrganizationPerson, remoteMap);
                        tpfOrganizationPersonNew.setId_((String) localMap.get("id_"));
                        tpfOrganizationPersonMapper.insert(tpfOrganizationPersonNew);
                    }
                    flag = true;
                    break loo;
                }
            }
            //如果不存在则新增
            if (!flag) {
                //新增人员信息
                TpfPerson tpfPersonNew = (TpfPerson) this.objectToMap(tpfPerson, remoteMap);
                tpfPersonNew.setCreate_time_(Timestamp.valueOf(sdf.format(new Date())));
                tpfPersonMapper.insert(tpfPersonNew);

                //新增人员和组织机构对应关系
                TpfOrganizationPerson tpfOrganizationPersonNew = (TpfOrganizationPerson) this.objectToMapTpfOrganizationPerson(tpfOrganizationPerson, remoteMap);
                tpfOrganizationPersonMapper.insert(tpfOrganizationPersonNew);

                //账号新增
                TpfAccount tpfAccountNew = (TpfAccount) this.objectToMapAccount(tpfAccount, remoteMap);
                tpfAccountMapper.insert(tpfAccountNew);

            }

        }
    }

    /**
     * 组织机构信息定时更新
     *
     * @return
     */
    public void updateZZJGXX() {

        //查询公安所有的组织机构信息
        List<Map<String, Object>> remoteOrgList = tgastMjxxServiceImpl.getTGASTZzjgxx();

        //查询本地组织机构信息
        List<TpfOrganization> localOrgList = tpfOrganizationMapper.selectAllZzjgxx();

        TpfOrganization tpfOrganization = new TpfOrganization();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //遍历公安组织机构信息与本地数据库中组织机构信息比对（无-添加，有-更新）
        boolean flag;
        for (Map map : remoteOrgList) {
            flag = false;
            loo:
            for (TpfOrganization m : localOrgList) {
                //根据组织机构id和姓名进行比对
                if (map.get("region_id_").equals(m.getId_())) {
                    TpfOrganization tpfOrganization1 = (TpfOrganization) this.objectToMapOrganization(m, map);
                    tpfOrganization1.setUpdate_time_(Timestamp.valueOf(sdf.format(new Date())));
                    tpfOrganizationMapper.updateById(tpfOrganization1);
                    flag = true;
                    break loo;
                }
            }
            //如果不存在则新增
            if (!flag) {
                tpfOrganization.setId_((String) map.get("region_id_"));
                TpfOrganization tpfOrganization1 = (TpfOrganization) this.objectToMapOrganization(tpfOrganization, map);
                tpfOrganization1.setCreate_time_(Timestamp.valueOf(sdf.format(new Date())));
                tpfOrganizationMapper.insert(tpfOrganization1);
            }
        }
    }

    private Object objectToMap(TpfPerson tpfPerson, Map map) {
        tpfPerson.setId_((String) map.get("id_"));
        tpfPerson.setPolice_code_((String) map.get("police_code_"));
        tpfPerson.setNumber_((String) map.get("police_code_"));
        tpfPerson.setName_((String) map.get("name_"));
        tpfPerson.setSex_(Integer.valueOf((String) map.get("sex_")));
        tpfPerson.setBirth_date_(Timestamp.valueOf((String) map.get("birth_date_")));
        tpfPerson.setAvailable_(Integer.valueOf((String) map.get("available_")));
        tpfPerson.setAddress_((String) map.get("address_"));
        tpfPerson.setDelete_(0);
        tpfPerson.setPhone_((String) map.get("phone_"));
        tpfPerson.setMain_duty_((String) map.get("main_duty_"));
        return tpfPerson;
    }

    private Object objectToMapOrganization(TpfOrganization tpfOrganization, Map map) {
        tpfOrganization.setName_((String) map.get("name_"));
        tpfOrganization.setRegion_id_((String) map.get("region_id_"));
        tpfOrganization.setRegion_pid_((String) map.get("region_pid_"));
        tpfOrganization.setDelete_(0);
        tpfOrganization.setAddress_((String) map.get("name_"));
        return tpfOrganization;
    }

    private Object objectToMapAccount(TpfAccount tpfAccount, Map map) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        tpfAccount.setId_((String) map.get("police_code_"));
        tpfAccount.setLogin_name_((String) map.get("police_code_"));
        tpfAccount.setDescription_((String) map.get("name_"));
        tpfAccount.setPassword_("e10adc3949ba59abbe56e057f20f883e");
        tpfAccount.setAvailable_(1);
        tpfAccount.setStatu_(1);
        tpfAccount.setDelete_(0);
        tpfAccount.setPerson_id_((String) map.get("id_"));
        tpfAccount.setOrg_id_((String) map.get("org_id_"));
        tpfAccount.setCreate_time_(Timestamp.valueOf((String) sdf.format(new Date())));
        return tpfAccount;
    }

    private Object objectToMapTpfOrganizationPerson(TpfOrganizationPerson tpfOrganizationPerson, Map map) {
        tpfOrganizationPerson.setId_("2020" + (String) map.get("id_"));
        tpfOrganizationPerson.setOrg_id_((String) map.get("org_id_"));
        tpfOrganizationPerson.setPerson_id_((String) map.get("id_"));
        tpfOrganizationPerson.setMain_position_(1);
        tpfOrganizationPerson.setAvailable_(1);
        tpfOrganizationPerson.setDelete_(0);
        tpfOrganizationPerson.setPosition_name_((String) map.get("main_duty_"));
        tpfOrganizationPerson.setPerson_name_((String) map.get("name_"));
        return tpfOrganizationPerson;
    }
}
