package com.mti.mjxx.zzjgxx.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * Created by admin on 2020/6/30.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_pf_organization")
public class TpfOrganization {
    @TableId
    @TableField("id_")
    private String id_;

    @TableField("number_")
    private String number_;

    @TableField("code_")
    private String code_;

    @TableField("type_")
    private int type_;

    @TableField("name_")
    private String  name_;

    @TableField("short_name_")
    private String short_name_;

    @TableField("sort_")
    private int sort_;

    @TableField("description_")
    private String description_;

    @TableField("region_id_")
    private String region_id_;

    @TableField("region_pid_")
    private String region_pid_;

    @TableField("business_pid_")
    private String business_pid_;

    @TableField("address_")
    private String address_;

    @TableField("zip_code_")
    private String zip_code_;

    @TableField("concat_phone_")
    private String concat_phone_;

    @TableField("first_manager_")
    private String first_manager_;

    @TableField("second_manager_")
    private String second_manager_;

    @TableField("version_")
    private String version_;

    @TableField("available_")
    private int available_;

    @TableField("creator_id_")
    private String creator_id_;

    @TableField("delete_")
    private int delete_;

    @TableField("internal_")
    private int internal_;

    @TableField("qu_")
    private String qu_;

    @TableField("level")
    private String level;

    @TableField("org_type_")
    private int org_type_;

    @TableField("org_sort_")
    private String org_sort_;

    @TableField("create_time_")
    private Timestamp create_time_;

    @TableField("update_time_")
    private Timestamp update_time_;

}
