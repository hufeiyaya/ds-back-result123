package com.mti.mjxx.zzjgxx.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mti.mjxx.zzjgxx.model.TpfOrganization;

import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2020/6/29.
 */
public interface TpfOrganizationMapper extends BaseMapper<TpfOrganization>,TransBaseMapper {
    List<TpfOrganization> selectAllZzjgxx();
}
