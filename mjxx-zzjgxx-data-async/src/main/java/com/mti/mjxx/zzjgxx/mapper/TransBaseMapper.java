package com.mti.mjxx.zzjgxx.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TransBaseMapper {
    List<Map<String,Object>> selectTGASTMjxx();
    List<Map<String,Object>> selectTGASTZzjgxx();
}
