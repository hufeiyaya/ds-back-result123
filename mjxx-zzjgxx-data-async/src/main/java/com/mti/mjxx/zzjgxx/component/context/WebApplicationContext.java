package com.mti.mjxx.zzjgxx.component.context;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class WebApplicationContext implements ApplicationContextAware {
    private ApplicationContext context;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    public Object getBeanByName(String beanName){
        return this.context==null?null:this.context.getBean(beanName);
    }

    public Object getBeanByName(String beanName,Class cls){
        return this.context==null?null:this.context.getBean(beanName,cls);
    }
}
