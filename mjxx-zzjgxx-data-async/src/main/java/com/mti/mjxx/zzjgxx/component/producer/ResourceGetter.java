//package com.mti.ztry.component.producer;
//
//import DataAdapter;
//import WebApplicationContext;
//import DynamicSource;
//import TransBaseMapper;
//import lombok.AllArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Service;
//import java.util.List;
//import java.util.Map;
//
///**
// * create by zhangmingxin
// * 源表数据获取
// */
//@Slf4j
//@Service
//@AllArgsConstructor
//public class ResourceGetter implements ResourceOperate{
//    private final DataAdapter adapter;
//    private final WebApplicationContext context;
//    private static final Object monitor = new Object();
//    @Override
//    @DynamicSource(value = "subBureau")
//    public void getResourceData(String start,String end,String format,String tableName) {
//        synchronized (monitor){
//            //开始获取源数据
//            TransBaseMapper transBaseMapper = (TransBaseMapper) context.getBeanByName(tableName);
//            boolean isExist = transBaseMapper.tableExist(tableName)>0;
//            if(isExist){
//                List<Map<String,Object>> resourceData = transBaseMapper.selectAppendDataByDateTime(start,end,format);
//                adapter.onResourceDataProcess(resourceData,tableName);
//            }else
//                log.warn("===>resource table:{} is not exist",tableName);
//        }
//    }
//}
