package com.mti.mjxx.zzjgxx.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mti.mjxx.zzjgxx.model.TpfOrganizationPerson;

import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2020/7/1.
 */
public interface TpfOrganizationPersonMapper extends BaseMapper<TpfOrganizationPerson>, TransBaseMapper {
    List<Map<String, Object>> selectByPersonId(String person_id_);
}
