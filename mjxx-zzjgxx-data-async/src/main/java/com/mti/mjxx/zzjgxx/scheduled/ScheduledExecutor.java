package com.mti.mjxx.zzjgxx.scheduled;

import com.mti.mjxx.zzjgxx.service.TpfPersonImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
@EnableScheduling
@PropertySource("classpath:/application.yml")
public class ScheduledExecutor {

    @Autowired
    private TpfPersonImpl tpfPersonSImpl;

    /**
     * 民警数据同步服务
     *
     * 每天凌晨4点执行一次
     */
    @Scheduled(cron = "${schedules.mjxx}")
    public void reportCurrentByCron() {
        tpfPersonSImpl.seleectAllPerson();
    }


    /**
     * 组织机构同步
     *
     * 每天凌晨2点执行一次
     */
    @Scheduled(cron = "${schedules.zzjgxx}")
    public void reportCurrentByCron1() {
        tpfPersonSImpl.updateZZJGXX();
    }

}
