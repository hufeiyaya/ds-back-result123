package com.mti.mjxx.zzjgxx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages="com.mti.mjxx.zzjgxx.mapper")
public class MjxxAndZzjgxxDataAsyncApplication {

    public static void main(String[] args) {
        SpringApplication.run(MjxxAndZzjgxxDataAsyncApplication.class, args);
    }

}
