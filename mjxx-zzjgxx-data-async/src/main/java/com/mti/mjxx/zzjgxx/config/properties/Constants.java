package com.mti.mjxx.zzjgxx.config.properties;

public class Constants {
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String ORACLE_DATE_TIME_FORMAT = "yyyy-mm-dd hh24:mi:ss";
    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final String MICRO_TIME_FORMAT = "HH:mm:ss.S";
    public static final String DATE_TIME_MICRO_FORMAT = "yyyy-MM-dd HH:mm:ss.S";
}
