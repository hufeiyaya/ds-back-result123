package com.mti.mjxx.zzjgxx.component.adapter;

import java.util.List;
import java.util.Map;

/**
 * create by zhangmingxin
 * 数据适配器接口！用于原数据与目标数据处理器解耦
 */
public interface DataAdapter {
    void onResourceDataProcess(List<Map<String,Object>> resourceData,String resourceTableName);
}
