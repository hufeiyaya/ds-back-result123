//package com.mti.ztry.component.adapter;
//
//import DataConsumerProcessor;
//import lombok.AllArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import oracle.sql.TIMESTAMP;
//import org.springframework.beans.factory.annotation.Configurable;
//import org.springframework.stereotype.Component;
//
//import java.sql.Timestamp;
//import java.text.SimpleDateFormat;
//import java.time.LocalDateTime;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.stream.Collectors;
//
///**
// * 数据适配器
// * create by zhangmingxin
// * 对接生产数据类与消费数据类，也可在此处加入数据中间处理
// */
//@Slf4j
//@Component
//@Configurable
//@AllArgsConstructor
//public class DataAdapterImpl implements DataAdapter {
//    private final DataConsumerProcessor consumerAdapter;
//
//    @Override
//    public void onResourceDataProcess(List<Map<String, Object>> resourceData, String resourceTableName) {
//        consumerAdapter.onConsume(resourceData.parallelStream().map(DataAdapterImpl::transformUpperCase).collect(Collectors.toList()),resourceTableName);
//    }
//
//    /**
//     * map key转小写
//     * @param orgMap 源map
//     * @return transMap
//     */
//    private static Map<String, Object> transformUpperCase(Map<String, Object> orgMap) {
//        Map<String, Object> resultMap = new HashMap<>();
//        if (orgMap == null || orgMap.isEmpty()) {
//            return resultMap;
//        }
//        Set<String> keySet = orgMap.keySet();
//        for (String key : keySet) {
//            String newKey = key.toLowerCase();
////            newKey = newKey.replace("_", "");
//            resultMap.put(newKey, timestampToStr(orgMap.get(key)));
//        }
//        return resultMap;
//    }
//
//    /**
//     * timestamp转str
//     * @param obj obj
//     * @return str
//     */
//    private static String timestampToStr(Object obj){
//        if(obj instanceof TIMESTAMP)
//            return ((TIMESTAMP)obj).stringValue();
//        else
//            return obj.toString();
//    }
//}
//
