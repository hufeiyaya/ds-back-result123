package com.mti.mjxx.zzjgxx.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mti.mjxx.zzjgxx.model.TpfAccount;

import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2020/6/29.
 */
public interface TpfAccountMapper extends BaseMapper<TpfAccount>, TransBaseMapper {
    /**
    * 方法描述 根据账id_查询账号数据
    * @author hf
    * @date 2020/11/12
    * @param id_
    * @return List<Map<String, Object>>
    */
    List<Map<String, Object>> selectByIdTpfAccount(String id_);
}
