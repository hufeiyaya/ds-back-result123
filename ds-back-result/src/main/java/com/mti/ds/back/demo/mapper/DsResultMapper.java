package com.mti.ds.back.demo.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mti.ds.back.demo.model.DsResultEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author hf
 * @Title:
 * @Description: TODO (DS处置结果mapper)
 * @Param
 * @return
 * @Date 2020/12/4 14:15
 */
public interface DsResultMapper extends BaseMapper<DsResultEntity> {
    List<DsResultEntity> getDsResult(@Param("alarmId") String alarmId, @Param("bjsj") String bjsj);
}
