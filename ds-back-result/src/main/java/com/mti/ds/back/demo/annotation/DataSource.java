package com.mti.ds.back.demo.annotation;


import com.mti.ds.back.demo.enums.DataSourceEnum;

import java.lang.annotation.*;

/**
 * @Author hf
 * @Title:
 * @Description: TODO (数据源注解)
 * @Param
 * @return
 * @Date 2020/12/4 14:11
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {

    DataSourceEnum value() default DataSourceEnum.DB1;
}
