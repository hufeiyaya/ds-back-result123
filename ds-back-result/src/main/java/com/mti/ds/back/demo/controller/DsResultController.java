
package com.mti.ds.back.demo.controller;


import com.daoshu.system.model.view.ResponseResult;
import com.mti.ds.back.demo.service.DsResultService;
import com.mti.ds.back.demo.service.LfZtryyjsjService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@Api(value = "ds-result", description = "Ds反馈处警结果")
@RestController
@RequestMapping("/api/getDsResult")
@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
public class DsResultController {

    @Autowired
    private DsResultService dsResultService;
    @Autowired
    private LfZtryyjsjService lfZtryyjsjService;

    @PostMapping("/getDsResultByAlarmId")
    @ApiOperation("根据alarmId获取DS处理结果")
    public ResponseResult<Object> getDsResultByAlarmId(@RequestParam(required = true) String alarmId, @RequestParam(required = false) String bjsj) {
        try {
            Object obj = dsResultService.getDsResult(alarmId, bjsj);
            return ResponseResult.success(obj);
        } catch (Exception e) {
            log.error("查询失败", e);
            return ResponseResult.fail(99999, "无接处警反馈信息！", null);
        }
    }
}

