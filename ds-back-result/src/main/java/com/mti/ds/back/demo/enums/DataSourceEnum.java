package com.mti.ds.back.demo.enums;

/**
 * @Author hf
 * @Title:
 * @Description: TODO (数据源名称枚举类)
 * @Param
 * @return
 * @Date 2020/12/4 14:15
 */
public enum DataSourceEnum {

    DB1("db1"),DB2("db2");

    private String value;

    DataSourceEnum(String value){this.value=value;}

    public String getValue() {
        return value;
    }
}
