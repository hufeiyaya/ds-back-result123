package com.mti.ds.back.demo.service;

import com.baomidou.mybatisplus.service.IService;
import com.mti.ds.back.demo.model.DsResultEntity;

import java.util.List;

public interface DsResultService extends IService<DsResultEntity> {
    List<DsResultEntity> getDsResult(String alarmId, String bjsj);

}
