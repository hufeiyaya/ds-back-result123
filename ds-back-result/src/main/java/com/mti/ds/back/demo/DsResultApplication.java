package com.mti.ds.back.demo;

import lombok.extern.log4j.Log4j2;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@MapperScan(basePackages="com.mti.ds.back.demo.mapper")
@Log4j2
public class DsResultApplication {

    public static void main(String[] args) {
        SpringApplication.run(DsResultApplication.class, args);
        log.info("启动完毕");
    }

}
