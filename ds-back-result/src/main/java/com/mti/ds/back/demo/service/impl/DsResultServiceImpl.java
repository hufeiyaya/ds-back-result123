package com.mti.ds.back.demo.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mti.ds.back.demo.annotation.DataSource;
import com.mti.ds.back.demo.enums.DataSourceEnum;
import com.mti.ds.back.demo.mapper.DsResultMapper;
import com.mti.ds.back.demo.model.DsResultEntity;
import com.mti.ds.back.demo.service.DsResultService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author hf
 * @Title:
 * @Description: TODO (DS预警处置)
 * @Param
 * @return
 * @Date 2020/12/4 14:16
 */
@Service
public class DsResultServiceImpl extends ServiceImpl<DsResultMapper, DsResultEntity> implements DsResultService {

    @Override
    @DataSource(DataSourceEnum.DB2)
    public List<DsResultEntity> getDsResult(String alarmId, String bjsj) {
        return this.baseMapper.getDsResult(alarmId, bjsj);
    }
}
