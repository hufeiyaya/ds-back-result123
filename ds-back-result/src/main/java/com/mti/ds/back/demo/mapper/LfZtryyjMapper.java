package com.mti.ds.back.demo.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mti.ds.back.demo.model.LfZtryyjEntity;

import java.util.List;


public interface LfZtryyjMapper extends BaseMapper<LfZtryyjEntity> {
    List<LfZtryyjEntity> getAlarmId();
}
