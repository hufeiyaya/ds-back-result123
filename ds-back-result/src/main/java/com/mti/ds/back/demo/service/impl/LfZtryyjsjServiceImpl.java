package com.mti.ds.back.demo.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mti.ds.back.demo.annotation.DataSource;
import com.mti.ds.back.demo.enums.DataSourceEnum;
import com.mti.ds.back.demo.mapper.DsResultMapper;
import com.mti.ds.back.demo.mapper.LfZtryyjMapper;
import com.mti.ds.back.demo.model.DsResultEntity;
import com.mti.ds.back.demo.model.LfZtryyjEntity;
import com.mti.ds.back.demo.service.DsResultService;
import com.mti.ds.back.demo.service.LfZtryyjsjService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author hf
 * @Title:
 * @Description: TODO (DS预警处置)
 * @Param
 * @return
 * @Date 2020/12/4 14:16
 */
@Service
public class LfZtryyjsjServiceImpl extends ServiceImpl<LfZtryyjMapper, LfZtryyjEntity> implements LfZtryyjsjService {

    @Override
    public List<LfZtryyjEntity> getAlarmId() {
        return this.baseMapper.getAlarmId();
    }
}
