package com.mti.ds.back.demo.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_lf_ztryyjsj")
public class LfZtryyjEntity {
    @TableId("wybs")
    private String wybs;
    @TableField("yjlx")
    private String yjlx;
    @TableField("ryxm")
    private String ryxm;
    @TableField("yjbs")
    private String yjbs;
    @TableField("sjbs")
    private String sjbs;
    @TableField("yjsj")
    private String yjsj;
    @TableField("yjdd")
    private String yjdd;
    @TableField("ddmc")
    private String ddmc;
    @TableField("gxsj")
    private String gxsj;
    @TableField("x")
    private String x;
    @TableField("y")
    private String y;
    @TableField("tbsj")
    private String tbsj;
    @TableField("xsd")
    private String xsd;
    @TableField("rlzp")
    private String rlzp;
    @TableField("zjzp")
    private String zjzp;
    @TableField("zjzp2")
    private String zjzp2;
    @TableField("zjzp3")
    private String zjzp3;

    /**
     * 盘查民警ID
     */
    @TableField("pcmjid")
    private String pcmjid;

    /**
     * 盘查民警姓名
     */
    @TableField("pcmjname")
    private String pcmjname;

    /**
     * 盘查民警电话
     */
    @TableField("pcmjdh")
    private String pcmjdh;

    /**
     * 盘查派出所
     */
    @TableField("pcpcs")
    private String pcpcs;

    /**
     * 盘查派出所电话
     */
    @TableField("pcpcsdh")
    private String pcpcsdh;

    /**
     * 盘查派出所电话
     */
    @TableField("ticketno")
    private String ticketno;

    /**
     * 座位号
     */
    @TableField("seatno")
    private String seatno;

    /**
     * 车牌号
     */
    @TableField("busno")
    private String busno;

    /**
     * 到达站
     */
    @TableField("prtname")
    private String prtname;

    /**
     * 发车时间
     */
    @TableField("sendtime")
    private String sendtime;

    /**
     * 终点站
     */
    @TableField("endname")
    private String endname;

    /**
     * 关联Id
     */
    @TableField("gl_id")
    private String glId;

    /**
     * 公路客运 铁路订票 民航订座 进出标志
     */
    @TableField("in_out")
    private Integer inOut;

    /**
     * 预警处置标志：1 为处置，2  超时，3 已处置
     */
    @TableField("handle_flag")
    private Integer handleFlag;

    /**
     * 发车点，对应 铁路，客运和民航
     */
    @TableField("send_station")
    private String sendStation;
    /**
     * 发车点唯一标识，对应 铁路，客运和民航
     */
    @TableField("send_station_iden")
    private String sendStationIden;

    /**
     * 终点点唯一标识，对应 铁路，客运和民航
     */
    @TableField("end_station_iden")
    private String endStationIden;

    /**
     * 到达终点时间，对应 铁路，客运和民航
     */
    @TableField("end_station_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endStationTime;

    /**
     * 旅馆/网吧名称， 对应网吧和旅馆
     */
    @TableField("service_name")
    private String serviceName;

    /**
     * 旅馆/网吧的标识，对应网吧和旅馆
     */
    @TableField("service_code")
    private String serviceCode;

    /**
     * 出发城市
     */
    @TableField("send_city")
    private String sendCity;
    /**
     * 目的地城市
     */
    @TableField("end_city")
    private String endCity;

    /**
     * 出发省份
     */
    @TableField("send_pro")
    private String sendPro;

    /**
     * 到达省份
     */
    @TableField("end_pro")
    private String endPro;

    /**
     * 目前辖区
     */
    @TableField("current_jur")
    private String currentJur;

    /**
     * 出发站经度
     */
    @TableField("send_lng")
    private String sendLng;

    /**
     * 出发站维度
     */
    @TableField("send_lat")
    private String sendLat;

    /**
     * 终点站经度
     */
    @TableField("end_lng")
    private String endLng;

    /**
     * 终点站维度
     */
    @TableField("end_lat")
    private String endLat;

    /**
     * 预警发生地派出所
     */
    @TableField("yjd_pcs")
    private String yjdPcs;

    /**
     * 预警发生地派出所编码
     */
    @TableField("yjd_pcs_code")
    private String yjdPcsCode;

    /**
     * 预警发送标志：0 未发送，1 已发送
     */
    @TableField("send_flag")
    private Integer sendFlag;
    /**
     * 所属分局名称
     */
    @TableField("yjd_ssfj")
    private String yjdSsfj;

    /**
     * 所属分局编码
     */
    @TableField("yjd_ssfj_code")
    private String yjdSsfjCode;

    /**
     * 抓拍设备（人脸识别）
     */
    @TableField("device_name")
    private String deviceName;

    /**
     * 抓拍时间
     */
    @TableField("shot_time")
    private Date shotTime;

    /**
     * 嫌疑人抓拍人脸图（人脸识别）
     */
    @TableField("face_url")
    private String faceUrl;

    /**
     * 嫌疑人抓拍场景图（人脸识别）
     */
    @TableField("person_url")
    private String personUrl;

    /**
     * 嫌疑人布控库人脸图（人脸识别）
     */
    @TableField("target_url")
    private String targetUrl;

    /**
     * 管理部门
     */
    @TableField("department")
    private String department;

    /**
     * 关联数据
     */
    @TableField(exist = false)
    private Object glData;

    //返回数据
    private String tx;

}
