package com.mti.ds.back.demo.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *  Ds反馈处警结果
 * </p>
 *
 * @author hf
 * @since 2020-03-25
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "Ds反馈处警结果", description = "Ds反馈处警结果")
public class DsResultEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 处理结果内容
 	 */
	@ApiModelProperty(value = "处理结果内容")
	private String cljgnr;

	/**
	 * 处理时间
	 */
	@ApiModelProperty(value = "处理时间")
	private String fksj;

	/**
	 * 处理单位
	 */
	@ApiModelProperty(value = "处理单位")
	private String cjdwdm;


}
